<?php
sercurityCheck();

function toonPath($basisdir, $bestandlocatie, $bedrijfsid, $userid)
{
    global $pdo;

    $poespas = poespas();

    $toonPlaatsFactuur = 0;
    $toonPlaatsOfferte = 0;
    $toonPlaatsKlanten = 0;

    $basisdir = url2urlBack($basisdir);

    if (substr($basisdir, 0, strlen("$bestandlocatie/openruimte")) == "$bestandlocatie/openruimte") {
        $basisdir = str_replace("$bestandlocatie/openruimte", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/openruimte";
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/offerte")) == "$bestandlocatie/offerte") {
        $basisdir = str_replace("$bestandlocatie/offerte", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/offerte";
        $toonPlaatsOfferte = 1;
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/projecten")) == "$bestandlocatie/projecten") {
        $basisdir = str_replace("$bestandlocatie/projecten", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/projecten";
        $toonPlaatsOfferte = 1;
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/mail")) == "$bestandlocatie/mail") {
        $basisdir = str_replace("$bestandlocatie/mail", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/mail";
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/factuur")) == "$bestandlocatie/factuur") {
        $basisdir = str_replace("$bestandlocatie/factuur", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/factuur";
        $toonPlaatsFactuur = 1;
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/logo")) == "$bestandlocatie/logo") {
        $basisdir = str_replace("$bestandlocatie/logo", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/logo";
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/klanten")) == "$bestandlocatie/klanten") {
        $basisdir = str_replace("$bestandlocatie/klanten", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/klanten";
        $toonPlaatsKlanten = 1;
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/openruimte")) == "$bestandlocatie/openruimte") {
        $basisdir = str_replace("$bestandlocatie/reports", "", $basisdir);
        $pathFromRoot = "$bestandlocatie/reports";
    } elseif (substr($basisdir, 0, strlen("$bestandlocatie/werknemers")) == "$bestandlocatie/werknemers") {
        $basisdir = str_replace("$bestandlocatie/werknemers/" . $userid, "", $basisdir);
        $pathFromRoot = "$bestandlocatie/werknemers/" . $userid;
    } else {
        $basisdir = str_replace("$bestandlocatie", "", $basisdir);
        $pathFromRoot = $bestandlocatie;
    }

    $pathFromRoot = url2url($pathFromRoot);

    $path = tl("Locatie: ") . "<a href=\"content.php?SITE=ea&BID=$bedrijfsid&plaats=$pathFromRoot" . $poespas . "\">Start</a>";
    $mappen = explode("/", $basisdir);
    $pos = 0;
    for ($m = 0; $m < count($mappen); $m++) {
        if ($mappen[$m] != "") {
            $mapNaam = $mappen[$m];

            if ($m == 1) {
//                doesn't work because file is undefined -Martijn Smit
//                if ($toonPlaatsFactuur == 1) {
//                    $idFac = substr($mapNaam, 4, strlen($file));
//                    $queryRecht = $pdo->prepare('SELECT * FROM facturen WHERE ID = :id limit 1;');
//                    $queryRecht->bindValue('id', $idFac);
//                    $queryRecht->execute();
//                    $data_recht = $queryRecht->fetch(PDO::FETCH_ASSOC);
//
//                    if ($data_recht !== false) {
//                        $mapNaam = $data_recht["OFFERTENR"] . " - " . $data_recht["NAAM"];
//                    }
//                } else
                if ($toonPlaatsOfferte == 1) {
                    $queryRecht = $pdo->prepare('SELECT * FROM offerte WHERE ID = :id limit 1;');
                    $queryRecht->bindValue('id', $mapNaam);
                    $queryRecht->execute();
                    $data_recht = $queryRecht->fetch(PDO::FETCH_ASSOC);

                    if ($data_recht !== false) {
                        $mapNaam = $data_recht["OFFERTENR"] . " - " . $data_recht["NAAM"];
                    }
                } elseif ($toonPlaatsKlanten == 1) {
                    $query = $pdo->prepare('SELECT ID FROM klanten WHERE ID = :id limit 1;');
                    $query->bindValue('id', $mapNaam);
                    $query->execute();
                    $data_recht = $query->fetch(PDO::FETCH_ASSOC);

                    if ($data_recht !== false) {
                        $mapNaam = getNaamRelatie($data_recht["ID"]);
                    }

                    //TODO: check of correct
                    /*
                    $pathFromRoot = url2url($pathFromRoot . "/". $mappen[$m].$poespas);
                    $path .= " &#187; <a href=\"content.php?SITE=ea&BID=$bedrijfsid&plaats=$pathFromRoot\">".lb($mapNaam)."</a>";
                    $pos++;
                    */
                }
            }

            $pathFromRoot = url2url($pathFromRoot . "/" . $mappen[$m] . $poespas);
            $path .= " &#187; <a href=\"content.php?SITE=ea&BID=$bedrijfsid&plaats=$pathFromRoot\">" . lb($mapNaam) . "</a>";
            $pos++;
        }
    }
    if ($pos == 0) {
        $path = "";
    }

    return $path;
}

function bestandsnaam($bestandsnaam)
{
    $expl = explode("/", $bestandsnaam);
    $laatsteDeel = $expl[count($expl) - 1];
    if (is_numeric($laatsteDeel)) {
        $bestandsnaam = "document.php?DID=" . $laatsteDeel;
    }

    return $bestandsnaam;
}

function maakHTAccessBestand($path)
{
    $url = $_SERVER["DOCUMENT_ROOT"];
    $bestandlocatie = bestandLocatie();
    $LOCATIE = str_replace("//", "/", "$url/$bestandlocatie");
    $htaccess = "AuthType Basic\nAuthName \"Voer hier nogmaals je gebruikrsnaam en wachtwoord in.\"\nAuthUserFile $LOCATIE/.htpasswd\nRequire valid-user\nOptions -Indexes";

    $file = $path . "/.htaccess";

    if (!file_exists($file)) {
        if (!$file_handle = @fopen($file, "a")) {
            echo "<br/>Cannot open file: $file";
        }
        if (!fwrite($file_handle, $htaccess)) {
            echo "<br/>Cannot write to file: $file";
        }
        //echo "You have successfully written data to $file";
        @fclose($file_handle);
    }
}

function getPathFromCatID($ID, $bedrijfsid, $path)
{
    global $pdo;

    $query = $pdo->prepare('SELECT * FROM documentcatagorie WHERE (ID = :id) limit 1;');
    $query->bindValue('id', $ID);
    $query->execute();
    $data_2 = $query->fetch(PDO::FETCH_ASSOC);

    if ($data_2 !== false) {
        $path = $data_2["NAAM"] . "/" . $path;
        if ($data_2["HOGERNIVO"] != "" && $data_2["HOGERNIVO"] != "0" && $data_2["HOGERNIVO"] != null) {
            $path = getPathFromCatID($data_2["HOGERNIVO"], $bedrijfsid, $path);
        }
    }

    return $path;
}

function getCategorieIDFile($locatie, $bedrijfsid)
{
    global $pdo;

    if (substr($locatie, (strlen($locatie) - 1), strlen($locatie)) == "/") {
        $locatie = substr($locatie, 0, (strlen($locatie) - 1));
    }
    if (substr($locatie, 0, 1) == "/") {
        $locatie = substr($locatie, 1, strlen($locatie));
    }

    $mappen = explode("/", $locatie);
    $hogernivo = 0;

    if ($locatie != "" && $locatie != "/") {
        for ($i = 0; $i < count($mappen); $i++) {
            $sql_zoek = "";
            //echo "<br/>".$sql_zoek;
            $queryZoek = $pdo->prepare('SELECT * FROM documentcatagorie WHERE HOGERNIVO = :hogernivo and NAAM = :naam AND BEDRIJFSID = :bedrijfsid limit 1;');
            $queryZoek->bindValue('hogernivo', $hogernivo);
            $queryZoek->bindValue('naam', $mappen[$i]);
            $queryZoek->bindValue('bedrijfsid', $bedrijfsid);
            $queryZoek->execute();
            $data = $queryZoek->fetch(PDO::FETCH_ASSOC);

            if ($data !== false) {
                $hogernivo = $data["ID"];
            } else {
                //insert into documentcatagorie
                $query = $pdo->prepare('insert into documentcatagorie set HOGERNIVO = :hogernivo, NAAM = :naam, BEDRIJFSID = :bedrijfsid, ISSTANDAARD = "0";');
                $query->bindValue('hogernivo', $hogernivo);
                $query->bindValue('naam', $mappen[$i]);
                $query->bindValue('bedrijfsid', $bedrijfsid);

                if ($query->execute()) {
                    $hogernivo = getLastID('documentcatagorie');
                } else {
                    exit;
                }
            }
        }
    }
    return $hogernivo;
}

function uploadBestandInDB_2($files, $bedrijfsid, $userid, $doctype, $table, $table_id, $file_id, $JAAR)
{
    global $pdo;
    $lastid = 0;
    if (!isset($files['name']) || !$files['name']) {
        return false;
    }

    if (!is_array($files['name'])) {
        foreach ($files as $key => $value) {
            $files[$key] = [$value];
        }
    }
    foreach ($files['name'] as $key => $name) {
        $lastid = 0;
        $file = ps($name);
        $file_tmp = $files['tmp_name'][$key];

        if ($file == "" || !checkAllowedFileType($file, $file_tmp)) {
            continue;
//            header("HTTP/1.0 400 Bad Request");
//            echo "File not found or not allowed extension. Please go back and try again.";
//            die;
        }

        $error_message = "";
        switch ($files['error'][$key]) {
            case 0:
                break;
            case 1: //uploaded file exceeds the upload_max_filesize directive in php.ini
                $error_message = "uploaded file exceeds the upload maximum filesize";
                break;
            case 2: //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
                $error_message = "Uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form";
                break;
            case 3: //uploaded file was only partially uploaded
                $error_message = "Uploaded file was only partially uploaded";
                break;
            case 4: //no file was uploaded
                $error_message = "No file was uploaded";
                break;
            default: //a default error, just in case!  :)
                $error_message = "Default error with uploading file!";
                break;
        }
        if ($error_message != "") {
            echo $error_message;
            exit;
        }

        $file_type = $files['type'][$key];
        $file_size = $files['size'][$key];

        /*
        if(hasVirus($bestand_tmp)) {
          echo "A virus has been detected in your file!";
          $bestand = ""; exit;
        }
        */

        if ($file_size > 0) {
            $fp = fopen($file_tmp, 'r');
            $file_content = fread($fp, $file_size);
            $file_content = db_real_escape_string($file_content);
            fclose($fp);

            $file_name = str_replace(',', ' ', $file); // een komma in de naam geeft een error bij downloaden

            $statement = 'insert into';
            if ($file_id) {
                $statement = 'update';
            }
            $statement .= ' documentbeheer set
                            UPDATEDATUM    = "' . date("Y-m-d") . '",
                            TABEL          = :tabel,
                            TABELID        = :tabelid,
                            NAAM           = :naam,
                            PERSONEELSLID  = :personeelslid,
                            BESTAND        = :bestand,
                            BESTANDSNAAM   = :bestandsnaam,
                            BESTANDTYPE    = :bestandstype,
                            BESTANDSIZE    = :bestandsize,
                            BESTANDCONTENT = "' . $file_content . '"';
            if ($file_id) {
                $statement .= ' WHERE ID = :id';
            } else {
                $statement .= ',BEDRIJFSID  = :bedrijfsid,
                                DOCTYPE     = :doctype,
                                JAAR        = :jaar,
                                CATAGORIEID = "0",
                                DELETED     = "Nee",
                                VERSIENR    = "1",
                                BEKEKEN     = "0",
                                DATUM       = "' . date("Y-m-d") . '"';
            }

            $query = $pdo->prepare($statement);
            $query->bindValue('tabel', $table);
            $query->bindValue('tabelid', $table_id);
            $query->bindValue('naam', $file);
            $query->bindValue('bestand', $file);
            $query->bindValue('bestandsnaam', $file_name);
            $query->bindValue('bestandstype', $file_type);
            $query->bindValue('bestandsize', $file_size);
            $query->bindValue('personeelslid', $userid);

            if ($file_id) {
                $query->bindValue('id', $file_id);
            } else {
                $query->bindValue('doctype', $doctype);
                $query->bindValue('jaar', $JAAR);
                $query->bindValue('bedrijfsid', $bedrijfsid);
            }

            if ($query->execute()) {
                if ($file_id) {
                    $lastid = $file_id;
                } else {
                    $lastid = $pdo->lastInsertId();
                }

                @watisergebeurtEdit("Uploaded file: " . ps($file) . " / " . $file_type . "", $bedrijfsid,
                    "ADD", "documentbeheer", $lastid);
            }
        }

    }
    return $lastid;
}

function updateVirusDatabase()
{
    shell_exec('"C:\Program Files\ClamAV-x64\freshclam" --datadir="C:\database"');
}

function hasVirus($filename)
{
    $result = false;
    $match = array();
    $scanResult = shell_exec('"C:\Program Files\ClamAV-x64\clamscan"' . '  ' . $filename . ' --database="C:\database" 2>&1');

    preg_match('~Infected\sfiles\:[\s]?([0-9])~', $scanResult, $match);

    if (isset($match[1])) {
        if ($match[1] > 0) {
            $result = true;
        }
    } else {
        echo 'Virus scan is not completed.';
    }

    return $result;
}

function checkAllowedFileType($bestandsnaam, $file)
{
    $extention = strtolower(pathinfo($bestandsnaam, PATHINFO_EXTENSION));
    if (check('DIF', 'Nee', getMyBedrijfsID()) == 'Ja') {
        $allowedExtentions = array("pdf", "png", "jpg");
    } else {
        $allowedExtentions = array("csv", "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "txt", "odf", "odt");
    }

    if ($file == "") {
        return array();
    }

    $mimetype = mime_content_type($file);
    $allowedMimeTypes = array(
        'application/pdf',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'text/plain',
        'application/vnd.oasis.opendocument.formula',
        'application/vnd.oasis.opendocument.text'
    );

    return in_array($mimetype, $allowedMimeTypes) && in_array($extention, $allowedExtentions);
}

function uploadBestandInDB($FILES, $bedrijfsid, $userid, $locatie, $overRullCATId = 0, $typeFileAllowed = "")
{
    global $pdo;

    $lastid = 0;
    $bestand = ps($FILES['name']);

    if (($overRullCATId * 1) > 0) {
        $categorieId = $overRullCATId;
    } else {
        if ($locatie == "") {
            $categorieId = 0;
        } else {
            $categorieId = getCategorieIDFile($locatie, $bedrijfsid);
        }
    }

    switch ($FILES['error']) {
        case 0: //no error; possible file attack!
            break;
        case 1: //uploaded file exceeds the upload_max_filesize directive in php.ini
            echo "uploaded file exceeds the upload maximum filesize";
            $bestand = "";
            exit;
            break;
        case 2: //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
            echo "Uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form";
            $bestand = "";
            exit;
            break;
        case 3: //uploaded file was only partially uploaded
            echo "Uploaded file was only partially uploaded";
            $bestand = "";
            exit;
            break;
        case 4: //no file was uploaded
            echo "No file was uploaded";
            $bestand = "";
            exit;
            break;
        default: //a default error, just in case!  :)
            echo "Default error with uploading file!";
            $bestand = "";
            exit;
            break;
    }

    if ($bestand != "") {
        $bestand_tmp = $FILES['tmp_name'];
        $bestand_type = $FILES['type'];
        $bestand_size = $FILES['size'];

        /*
            if($typeFileAllowed == "pdf")
            {
              if (!(strtolower($FILES['type']) == "pdf" || strtolower($FILES['type']) == "application/pdf" || strtolower($FILES['type']) == "application/force-download"))
              {
                echo "This is no a correct pdf-file. (".$FILES['type'].")"; exit;
                $bestand = "";
              }
            }
        */


        if ($bestand_size > 0) {
            $fp = fopen($bestand_tmp, 'r');
            $bestand_content = fread($fp, $bestand_size);
            $bestand_content = db_real_escape_string($bestand_content);
            fclose($fp);

            $bestandsnaam = str_replace(',', ' ', $bestand);

            //check Allowed File Type
            if (checkAllowedFileType($bestandsnaam, $bestand_tmp)) {

                $doctype = "Other";
                if (isset($_REQUEST["documentbeheer_DOCTYPE"]) && $_REQUEST["documentbeheer_DOCTYPE"] != '') {
                    $doctype = $_REQUEST["documentbeheer_DOCTYPE"];
                }
                if (isset($_REQUEST["DOCTYPE"]) && $_REQUEST["DOCTYPE"] != '') {
                    $doctype = $_REQUEST["DOCTYPE"];
                }
                $BID = $bedrijfsid;
                if (isset($_REQUEST["BID"])) {
                    $BID = $_REQUEST["BID"];
                }

                $DATUM = date("Y-m-d");
                if (isset($_REQUEST["documentbeheer_DATUM"])) {
                    $DATUM = ps($_REQUEST["documentbeheer_DATUM"], "date");
                }
                if (isset($_REQUEST["DATUM"])) {
                    $DATUM = ps($_REQUEST["DATUM"], "date");
                } elseif (isset($_REQUEST["DOCDATUM"])) {
                    $DATUM = ps($_REQUEST["DOCDATUM"], "date");
                }
                $DATUM = date('Y-m-d', strtotime($DATUM));

                $JAAR = isset($_REQUEST["documentbeheer_JAAR"]) ? $_REQUEST["documentbeheer_JAAR"] : getJaar($DATUM);
                $JAAR = isset($_REQUEST["JAAR"]) ? $_REQUEST["JAAR"] : $JAAR;

//                $MONTH = isset($_REQUEST["documentbeheer_MONTH"]) ? $_REQUEST["documentbeheer_MONTH"] : getMaand($DATUM);

                $tags = isset($_REQUEST["documentbeheer_TAGS"]) ? $_REQUEST["documentbeheer_TAGS"] : "";

                $STOPDATUM = isset($_REQUEST["documentbeheer_STOPDATUM"]) ? $_REQUEST["documentbeheer_STOPDATUM"] : "";
                $STOPDATUM = date('Y-m-d', strtotime($STOPDATUM));

                $SQL_doc_insert = "";
                $query = $pdo->prepare('insert into documentbeheer set 
            UPDATEDATUM    = :updatedatum,
            DELETED        = "Nee",
            DOCTYPE        = :doctype,
            CATAGORIEID    = :categorieid,
            TAGS           = :tags,
            NAAM           = :naam,
            PERSONEELSLID  = :personeelslid,
            BESTAND        = :bestand,
            BESTANDSNAAM   = :bestandsnaam,
            BESTANDTYPE    = :bestandtype,
            BESTANDSIZE    = :bestandsize,
            BESTANDCONTENT = :bestandcontent,
            VERSIENR       = "1",
            BEKEKEN        = "0",
            DATUM          = :datum,
            STOPDATUM      = :stopdatum,
            JAAR           = :jaar,
            BEDRIJFSID     = :bedrijfsid;');
                $query->bindValue('updatedatum', date("Y-m-d"));
                $query->bindValue('doctype', $doctype);
                $query->bindValue('categorieid', $categorieId);
                $query->bindValue('tags', $tags);
                $query->bindValue('naam', $bestand);
                $query->bindValue('personeelslid', $userid);
                $query->bindValue('bestand', $bestand);
                $query->bindValue('bestandsnaam', $bestandsnaam);
                $query->bindValue('bestandtype', $bestand_type);
                $query->bindValue('bestandsize', $bestand_size);
                $query->bindValue('bestandcontent', $bestand_content);
                $query->bindValue('datum', $DATUM);
                $query->bindValue('stopdatum', $STOPDATUM);
                $query->bindValue('jaar', $JAAR);
//                $query->bindValue('month', $MONTH);
                $query->bindValue('bedrijfsid', $BID);

                if ($query->execute()) {
                    $lastid = $pdo->lastInsertId();
                    @watisergebeurtEdit("Uploaded file: " . ps($bestand) . " / " . $bestand_type . "", $bedrijfsid,
                        "ADD", "documentbeheer", $lastid);
                } else {
//            echo $query->queryString;
//            print_r($query->errorInfo());
                    echo "sql fout";
                    exit;
                }
            } else {
                echo "This file format is not allowed";
                exit;
                return 0;
            }
        }
    }
    return $lastid;
}

function uploadBestand($UPLOAD_BASE_DIR, $bestand_tmp, $bestand, $FILES)
{
    $return = false;

    if ($bestand != "") {
        $savefile = $UPLOAD_BASE_DIR . $bestand;
        @mkdir($UPLOAD_BASE_DIR, 0777);
        @chmod($UPLOAD_BASE_DIR, 0777);

        //kopieer bestand naar goede locatie.
        //if (is_uploaded_file($bestand))
        //{
        if (move_uploaded_file($bestand_tmp, $savefile)) {
            chmod($savefile, 0777);
            $return = true;
        } else {
            switch ($FILES['error']) {
                case 0: //no error; possible file attack!
                    echo "Er is een probleem met uploaden opgetreden met het uploaden naar $savefile.<BR>";
                    exit;
                    break;
                case 1: //uploaded file exceeds the upload_max_filesize directive in php.ini
                    echo "Het bestand dat u probeert te plaatsen is te groot.<BR>";
                    exit;
                    break;
                case 2: //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
                    echo "Het bestand dat u probeert te plaatsen is te groot.<BR>";
                    exit;
                    break;
                case 3: //uploaded file was only partially uploaded
                    echo "Het bestand dat u probeert te plaatsen is gedeeltelijk geplaatst.<BR>";
                    exit;
                    break;
                case 4: //no file was uploaded
                    echo "U heeft geen bestand geselecteerd om te uploaden.<BR>";
                    exit;
                    break;
                default: //a default error, just in case!  :)
                    echo "Er was een probleem bij het plaatsen.<BR>";
                    exit;
                    break;
            }
        }
        //}
    }
    return $return;
}

function RecursiveMkdir($path)
{
    if (!file_exists($path)) {
        //echo "<br/>".$path;
        RecursiveMkdir(dirname($path));
        @mkdir($path, 0755);
        //maakHTAccessBestand($path);
    }
}

function algemeneBestandsbeheer()
{
    global $pdo;

    $isRoot = false;
    $loginid = getLoginID();
    $userid = getUserID($loginid) * 1;
    if (isset($_REQUEST["BID"])) {
        $bedrijfsid = ps($_REQUEST["BID"], "nr");
    } else {
        $bedrijfsid = getBedrijfsID($loginid);
    }
    $bestandlocatie = bestandLocatieBedrijf($bedrijfsid);
    $poespas = poespas();

    if (isset($_REQUEST["plaats"])) {
        $plaats = $_REQUEST["plaats"];
    } else {
        $plaats = "$bestandlocatie/openruimte";
        $isRoot = true;
    }
    $plaats = url2urlBack($plaats);

    //check of je wel in de map mag zijn.
    if (substr($plaats, 0, strlen($bestandlocatie)) != $bestandlocatie) {
        $plaats = "$bestandlocatie/openruimte";
    }

    if (isKlant()) {
        $checkPlaats = explode("$bestandlocatie/klanten", $plaats);
        $klantid = getKlantId() * 1;
        if (count($checkPlaats) == 1) {
            $plaats = "$bestandlocatie/klanten/$klantid";
            $isRoot = true;
        }
    }

    $allreadyChecked = true;
    //Haal eerste gedeelte ervan af. (Dus geen /bestanden/openruimte)
    $toonPlaats = explode("$bestandlocatie/openruimte", $plaats);
    if (count($toonPlaats) > 1) {
        $toonPlaats = $toonPlaats[1];
        $allreadyChecked = false;
    } else {
        $toonPlaats = $toonPlaats[0];
    }

    //Bij projecten
    if ($allreadyChecked) {
        $toonPlaats = explode("$bestandlocatie/projecten", $plaats);
        if (count($toonPlaats) > 1) {
            $query = $pdo->prepare('SELECT * FROM offerte WHERE ID = (:id*1) limit 1;');
            $id = substr($toonPlaats[1], 1, 5);
            $query->bindValue('id', $id);
            $query->execute();
            $data_recht = $query->fetch(PDO::FETCH_ASSOC);

            if ($data_recht !== false) {
                $toonPlaats = $data_recht["OFFERTENR"] . " - " . $data_recht["NAAM"];
            } else {
                $toonPlaats = $toonPlaats[1];
            }
            $allreadyChecked = false;
        } else {
            $toonPlaats = $toonPlaats[0];
        }
    }
    //Bij klanten
    if ($allreadyChecked) {
        $toonPlaats = explode("$bestandlocatie/klanten", $plaats);
        if (count($toonPlaats) > 1) {
            $getNaamRelatie = getNaamRelatie(substr($toonPlaats[1], 1, 5));
            if ($getNaamRelatie != "N/A") {
                $toonPlaats = $getNaamRelatie;
            } else {
                $toonPlaats = $toonPlaats[1];
            }
            $allreadyChecked = false;
        } else {
            $toonPlaats = $toonPlaats[0];
        }
    }

    //Bij users
    if ($allreadyChecked) {
        $toonPlaats = explode("$bestandlocatie/werknemers", $plaats);
        if (count($toonPlaats) > 1) {
            $query = $pdo->prepare('SELECT * FROM personeel WHERE ID = (:id*1) limit 1;');
            $id = substr($toonPlaats[1], 1, 5);
            $query->bindValue('id', $id);
            $query->execute();
            $data_recht = $query->fetch(PDO::FETCH_ASSOC);

            if ($data_recht !== false) {
                $toonPlaats = tl("DOCUMENTEN") . ": " . $data_recht["VOORNAAM"] . " " . $data_recht["TUSSENVOEGSEL"] . " " . $data_recht["ACHTERNAAM"];
            } else {
                $toonPlaats = $toonPlaats[1];
            }
            $allreadyChecked = false;
        } else {
            $toonPlaats = $toonPlaats[0];
        }
    }

    if ($plaats == "$bestandlocatie/openruimte") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/offerte") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/projecten") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/mail") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/factuur") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/logo") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/klanten") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/reports") {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/werknemers/" . $userid) {
        $isRoot = true;
    } elseif ($plaats == "$bestandlocatie/capitalaccountstatment") {
        $isRoot = true;
    }

    if ($bedrijfsid > 0) {
        bestandBeheer("", $plaats, "ea", $isRoot, $toonPlaats, $bedrijfsid, $bestandlocatie);
    } else {
        rd("content.php?SITE=algBestbeheer" . $poespas);
    }
}

function walk_dir($path, $bedrijfsid)
{
    global $pdo;

    $teller = 0;
    $retval[][] = 0;
    if (file_exists($path)) {
        if ($dir = opendir($path)) {
            while (false !== ($file = readdir($dir))) {
                if (strlen($file) > 1) {
                    if ($file[0] == "." || $file[0] == "..") {
                        continue;
                    }
                    if (is_dir($path . "/" . $file)) {
                        //behandel als mappen
                        getCategorieIDFile($path . "/" . $file,
                            $bedrijfsid); //maakt automatisch aan in db als niet bestaat
                    } elseif (is_file($path . "/" . $file)) {
                        //Behandel als file
                        $ext = explode(".", $file);
                        $file = "";
                        for ($i = 0; $i < (count($ext) - 1); $i++) { //Maak filename weer compleet.
                            if ($i > 0) {
                                $file .= ".";
                            }
                            $file .= $ext[$i];
                        }
                        $ext = $ext[count($ext) - 1];
                        $retval[$teller][0] = $path . "/" . $file;
                        $retval[$teller][1] = $ext;
                        $retval[$teller][2] = "file";
                        $retval[$teller][3] = "0";
                        $teller++;
                    }
                }
            }
            closedir($dir);
        }
    }

    $WaarHangtHetOnder = getCategorieIDFile($path, $bedrijfsid);
    //echo $WaarHangtHetOnder;
    //Voeg mappen uit db toe
    $queryCat = $pdo->prepare('SELECT ID, NAAM
              FROM documentcatagorie
             WHERE HOGERNIVO = :hogernivo
               AND NAAM NOT IN (".", "..")
               AND BEDRIJFSID = :bedrijfsid
             ORDER BY NAAM;');
    $queryCat->bindValue('hogernivo', $WaarHangtHetOnder);
    $queryCat->bindValue('bedrijfsid', $bedrijfsid);
    $queryCat->execute();

    foreach ($queryCat->fetchAll() as $data_files) {
        $retval[$teller][0] = $path . "/" . $data_files["NAAM"];
        $retval[$teller][1] = "-MAP-";
        $retval[$teller][2] = "map";
        $retval[$teller][3] = 0;
        $teller++;
    }

    //Voeg bestanden uit db eraan toe EN kijk in goede map
    $queryDocu = $pdo->prepare('SELECT documentbeheer.ID, documentbeheer.BESTANDSNAAM, documentbeheer.BESTANDTYPE FROM documentbeheer where (CATAGORIEID*1) = (:categorieid*1) and not(BESTANDCONTENT = "") and not (BESTANDCONTENT is null) and BEDRIJFSID = :bedrijfsid;');
    $queryDocu->bindValue('categorieid', $WaarHangtHetOnder);
    $queryDocu->bindValue('bedrijfsid', $bedrijfsid);
    $queryDocu->execute();

    foreach ($queryDocu->fetchAll() as $data_files) {
        $retval[$teller][0] = $data_files["BESTANDSNAAM"];
        $retval[$teller][1] = str_replace("application/", "", $data_files["BESTANDTYPE"]);
        $retval[$teller][2] = "documentbeheer";
        $retval[$teller][3] = ($data_files["ID"] * 1);
        $teller++;
    }

    //Voeg bestanden uit db eraan toe EN kijk in goede map
    $queryDocumenten = $pdo->prepare('SELECT * FROM documenten where (HANGTONDER*1) = (:hangtonder*1) and not(TEKST = "") and not (TEKST is null) and BEDRIJFSID = :bedrijfsid;');
    $queryDocumenten->bindValue('hangtonder', $WaarHangtHetOnder);
    $queryDocumenten->bindValue('bedrijfsid', $bedrijfsid);
    $queryDocumenten->execute();

    foreach ($queryDocumenten->fetchAll() as $data_files) {
        $retval[$teller][0] = $data_files["NAAM"];
        $retval[$teller][1] = "dock";
        $retval[$teller][2] = "documenten";
        $retval[$teller][3] = ($data_files["ID"] * 1);
        $teller++;
    }

    return $retval;
}

//Id heb je nodig voor de project, om te weten op welk project je aan het werken bent.
function bestandBeheer($id, $basisdir, $typePage, $isRoot, $toonPlaats, $bedrijfsid, $bestandlocatie)
{
    global $pdo;
    $IMAGES_BASE_DIR = $basisdir;//getenv("DOCUMENT_ROOT").$IMAGES_BASE_URL;
    $UPLOAD_BASE_URL = $basisdir . "/";
    $UPLOAD_BASE_DIR = $UPLOAD_BASE_URL; //getenv("DOCUMENT_ROOT").$UPLOAD_BASE_URL;

    //Kopbalk
    $toonPlaats = str_replace("$bestandlocatie/", "", $toonPlaats);
    $toonPlaats = str_replace("$bestandlocatie", "", $toonPlaats);
    if (strlen($toonPlaats) == 0) {
        $toonPlaats = tl("Details");
    }
    $poespas = poespas();

    //Naam declaraties
    $j = "";
    $totalsize = "";
    $exceptions = "";
    $html_img_lst = "";
    $prevdir = "";
    $loginid = getLoginID();
    $userid = getUserID($loginid) * 1;

    $rechten = "lees";
    $aantalVakjesOpEenrij = 4;
    $totaleBreette = 930;
    if ((isOrangeField() && isMaster()) || !isOrangeField()) {
        $rechten = "schrijf";
        $aantalVakjesOpEenrij = 3;
        $totaleBreette = 740;
    }

//    $breettePerVakje = floor(($totaleBreette / $aantalVakjesOpEenrij));

    if (!file_exists($IMAGES_BASE_DIR)) {
        RecursiveMkdir($IMAGES_BASE_DIR);
    }

    if (isset($_POST["upload"]) && isset($_FILES['X_File']['name'])) {
//        $bestand = $_FILES['X_File']['name'];
//        $savefile = $UPLOAD_BASE_DIR . $bestand;
        $typeFileAllowed = "";
        if (isOrangeField()) {
            $typeFileAllowed = "pdf";
        }
        if (uploadBestandInDB($_FILES['X_File'], $bedrijfsid, $userid, $UPLOAD_BASE_DIR, 0, $typeFileAllowed) > 0) {
            $plaatss = substr($UPLOAD_BASE_URL, 0, (strlen($UPLOAD_BASE_URL) - 1));
            $plaatss = url2url($plaatss);
            rd("content.php?SITE=ea&ID=$id&plaats=$plaatss&BID=$bedrijfsid" . $poespas);
        }
    } elseif (isset($_POST["delete"])) {
        $totaantbest = $_POST["totaantbest"];
        for ($i = 0; $i <= $totaantbest; $i++) {
            if (isset($_REQUEST["BEST_" . $i])) {
                if (substr($_REQUEST["BEST_" . $i], 0, 4) == "dbid") {
                    $itemId = substr($_REQUEST["BEST_" . $i], 4, strlen($_REQUEST["BEST_" . $i]));
                    watisergebeurtEdit("Remove file", $bedrijfsid, "REMOVE", "documentbeheer", $itemId);
                    $query = $pdo->prepare('delete from documentbeheer where ID = :id;');
                    $query->bindValue('id', $itemId);
                    $query->execute();
                } elseif (substr($_REQUEST["BEST_" . $i], 0, 4) == "dcid") {
                    $itemId = substr($_REQUEST["BEST_" . $i], 4, strlen($_REQUEST["BEST_" . $i]));
                    watisergebeurtEdit("Remove document", $bedrijfsid, "REMOVE", "documenten", $itemId);
                    $query = $pdo->prepare('delete from documenten where ID = :id;');
                    $query->bindValue('id', $itemId);
                    $query->execute();
                } else {
                    $bestand = $_REQUEST["BEST_" . $i];
                    @unlink($bestand);
                }
            }
            if (isset($_REQUEST["MAP_" . $i])) {
                $directory = $_REQUEST["MAP_" . $i];
                watisergebeurtEdit("Remove directory", $bedrijfsid, "REMOVE", "documentcatagorie", $directory);
                recursive_remove_directory($directory, $bedrijfsid, $empty = false);
                recursive_remove_directory_db($directory, $bedrijfsid);
            }
        }
    } elseif (isset($_POST["mailfile"])) {
        $totaantbest = $_POST["totaantbest"];
        $bestandenlist = "";
        $documentenlist = "";
        for ($i = 0; $i <= $totaantbest; $i++) {
            if (isset($_REQUEST["BEST_" . $i])) {
                if (substr($_REQUEST["BEST_" . $i], 0, 4) == "dbid") {
                    //bestand in db
                    $itemId = substr($_REQUEST["BEST_" . $i], 4, strlen($_REQUEST["BEST_" . $i]));
                    $bestandenlist .= ";;;$bestandlocatie/mail/send/" . date("Ymd") . "/" . getNaam("documentbeheer",
                            $itemId, "NAAM");

                    if (!file_exists("$bestandlocatie/mail/send/" . date("Ymd") . "/")) {
                        RecursiveMkdir("$bestandlocatie/mail/send/" . date("Ymd") . "/");
                    }

                    saveDocumentDocumentBeheer($itemId,
                        "$bestandlocatie/mail/send/" . date("Ymd") . "/" . getNaam("documentbeheer", $itemId,
                            "NAAM"));
                } elseif (substr($_REQUEST["BEST_" . $i], 0, 4) == "dcid") {
                    //document
                    $itemId = substr($_REQUEST["BEST_" . $i], 4, strlen($_REQUEST["BEST_" . $i])) * 1;
                    $documentenlist .= ";;;" . $itemId . ";;" . getNaam("documenten", $itemId, "NAAM");

                    if (!file_exists("$bestandlocatie/mail/send/" . date("Ymd") . "/")) {
                        RecursiveMkdir("$bestandlocatie/mail/send/" . date("Ymd") . "/");
                    }

                    saveDocument($itemId,
                        "$bestandlocatie/mail/send/" . date("Ymd") . "/document_" . $itemId . ".pdf");
                } else {
                    //hardcopy
                    $bestandenlist .= ";;;" . $_REQUEST["BEST_" . $i];
                }
            }
        }
        setcookie("MAILBESTANDEN", $bestandenlist, (time() + 3600 * 24), "/");
        setcookie("MAILDOCS", $documentenlist, (time() + 3600 * 24), "/");
        rd("content.php?SITE=mywebmail&TYPE=SCHRIJF" . $poespas);
    } elseif (isset($_POST["Maak"])) {
        if ($_POST["map"] != "" && $_POST["map"] != " " && $_POST["map"] != $basisdir) {
            $dir = $basisdir . "/" . $_POST["map"];
            //@mkdir ($dir, 0755);
            //@chmod($dir, 0755);
            //maakHTAccessBestand($dir);
            rd(url2url("content.php?SITE=ea&BID=$bedrijfsid&ID=&plaats=$dir" . $poespas));
        }
    } elseif (isset($_REQUEST["copyfile"])) {
        $clipboard = "";
        $totaantbest = $_POST["totaantbest"];
        for ($i = 0; $i <= $totaantbest; $i++) {
            if (isset($_REQUEST["BEST_" . $i])) {
                $bestand = $_REQUEST["BEST_" . $i];
                $clipboard .= ";;FILE:" . $bestand;
            } elseif (isset($_REQUEST["MAP_" . $i])) {
                $directory = $_REQUEST["MAP_" . $i];
                $clipboard .= ";;MAP:" . $directory;
            }
        }
        $queryKlembord = $pdo->prepare('delete from klembord where PERSONEELSLID = :personeelslid;');
        $queryKlembord->bindValue('personeelslid', $userid);
        $queryKlembord->execute();
        $queryKlembordInsert = $pdo->prepare('insert into klembord set KLEMBORD = :klembord, PERSONEELSLID = :personeelslid;');
        $queryKlembordInsert->bindValue('klembord', $clipboard);
        $queryKlembordInsert->bindValue('personeelslid', $userid);
        $queryKlembordInsert->execute();
    } elseif (isset($_REQUEST["paste"]) || isset($_REQUEST["cut"])) {
        $query = $pdo->prepare('SELECT * FROM klembord WHERE (PERSONEELSLID*1) = :personeelslid limit 1;');
        $query->bindValue('personeelslid', $userid);
        $query->execute();
        $data_recht = $query->fetch(PDO::FETCH_ASSOC);

        if ($data_recht !== false) {
            $klembord = explode(";;", $data_recht["KLEMBORD"]);
            for ($i = 1; $i < count($klembord); $i++) {
                if (substr($klembord[$i], 0, 9) == "FILE:dbid") {
                    $dKlembordID = substr($klembord[$i], 9, strlen($klembord[$i]));
                    $WaarHangtHetOnder = getCategorieIDFile($basisdir, $bedrijfsid);
                    if (isset($_REQUEST["cut"])) {
                        $query = $pdo->prepare('update documentbeheer set CATAGORIEID = :categorieid, BEDRIJFSID = :bedrijfsid WHERE ID = :id limit 1;');
                        $query->bindValue('categorieid', $WaarHangtHetOnder);
                        $query->bindValue('bedrijfsid', $bedrijfsid);
                        $query->bindValue('id', $dKlembordID);
                        $query->execute();
                    } else {
                        $query = $pdo->prepare('SELECT * FROM documentbeheer WHERE ID = :id limit 1;');
                        $query->bindValue('id', $dKlembordID);
                        $query->execute();

                        foreach ($query->fetchAll() as $data_docbeh) {
                            $bestandsnaam = str_replace(',', ' ',
                                $data_docbeh["BESTANDSNAAM"]); // een komma in de naam geeft een error bij downloaden
                            $query = $pdo->prepare('insert into documentbeheer set
                                                      CATAGORIEID    = :categorieid,
                                                      TAGS           = :tags,
                                                      NAAM           = :naam,
                                                      PERSONEELSLID  = :personeelslid,
                                                      BESTAND        = :bestand,
                                                      BESTANDSNAAM   = :bestandsnaam,
                                                      BESTANDTYPE    = :bestandtype,
                                                      BESTANDSIZE    = :bestandsize,
                                                      BESTANDCONTENT = "' . addslashes($data_docbeh["BESTANDCONTENT"]) . '",
                                                      VERSIENR       = "1",
                                                      BEKEKEN        = "0",
                                                      DATUM          = "' . date("Y-m-d") . '",
                                                      BEDRIJFSID     = :bedrijfsid;');
                            $query->bindValue('categorieid', $WaarHangtHetOnder);
                            $query->bindValue('tags', $data_docbeh["TAGS"]);
                            $query->bindValue('naam', $data_docbeh["NAAM"]);
                            $query->bindValue('personeelslid', $userid);
                            $query->bindValue('bestand', $data_docbeh["BESTAND"]);
                            $query->bindValue('bestandsnaam', $bestandsnaam);
                            $query->bindValue('bestandtype', $data_docbeh["BESTANDTYPE"]);
                            $query->bindValue('bestandsize', $data_docbeh["BESTANDSIZE"]);
                            $query->bindValue('bedrijfsid', $bedrijfsid);
                            $query->execute();
                        }
                    }
                } else {
                    $filename = explode("/", $klembord[$i]);
                    $filename = $filename[(count($filename) - 1)];
                    $niewLocatie = $basisdir . "/" . $filename;
                    if (substr($klembord[$i], 0, 5) == "FILE:") {
                        $oudeLocatie = substr($klembord[$i], 5, strlen($klembord[$i]));

                        if (isset($_REQUEST["cut"])) {
                            if (!rename($oudeLocatie, $niewLocatie)) {
                                echo "failed to copy $oudeLocatie.\n";
                            }
                        } else {
                            if (!copy($oudeLocatie, $niewLocatie)) {
                                echo "failed to copy $oudeLocatie.\n";
                            }
                        }
                    } elseif (substr($klembord[$i], 0, 4) == "MAP:") {
                        $oudeLocatie = substr($klembord[$i], 4, strlen($klembord[$i]));
                        //Maak nieuwe locatie aan
                        RecursiveMkdir($niewLocatie);

                        //doorloop oude locatie
                        $directory = walk_dir($oudeLocatie, $bedrijfsid);
                        for ($j = 0; $j < count($directory); $j++) {
                            if ($directory[$j][1] != "-MAP-") {
                                $oudeLocatieFile = $directory[$j][0] . "." . $directory[$j][1];
                                $filename = explode("/", $oudeLocatieFile);
                                $filename = $filename[(count($filename) - 1)];
                                $niewLocatieFile = $niewLocatie . "/" . $filename;
                                if (isset($_REQUEST["cut"])) {
                                    if (!rename($oudeLocatieFile, $niewLocatieFile)) {
                                        echo "failed to copy $oudeLocatie.\n";
                                    }
                                } else {
                                    if (!copy($oudeLocatieFile, $niewLocatieFile)) {
                                        echo "failed to copy $oudeLocatie.\n";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $query = $pdo->prepare('delete FROM klembord WHERE (PERSONEELSLID*1) = (:personeelslid*1);');
            $query->bindValue('personeelslid', $userid);
            $query->execute();
        }
    }

    //==================================================
    $tel_array = 0;
    $tel_best = 0;
    $tel_map = 0;
    $checkAll = "";

    $pdfPreview = true;
    //http://hasin.wordpress.com/2008/02/06/installing-imagick-extension-for-php-in-ubuntu-710/

    //Doorloop een directory en toon files
    $directory = walk_dir($IMAGES_BASE_DIR, $bedrijfsid);
    if ($directory[0][0] == "0") {
        $stop = 0;
    } else {
        $stop = count($directory);
    }

    //zoek uit wat de boven liggende dir is.
    $tmp_dir = $UPLOAD_BASE_URL;
    $tmp_dirr = explode("/", $tmp_dir);
    for ($j == 0; $j < (count($tmp_dirr) - 2); $j++) {
        @$prevdir .= $tmp_dirr[$j] . "/";
    }
    $prevdir = $tmp_dirr[0] . $prevdir;
    $prevdir = substr($prevdir, 0, strlen($prevdir) - 1);

    //==================================================

    $aantalMappen = 0;
    $aantalBestanden = 0;

    $ordners = "";
    $query = $pdo->prepare('SELECT * FROM stam_ordners ORDER BY NAAM;');
    $query->execute();

    foreach ($query->fetchAll() as $data_ordners) {
        $ordners .= "<option value=\"" . ($data_ordners["ID"] * 1) . "\">" . $data_ordners["NAAM"] . "</option>";
    }

    if ($stop == 0) {
        $html_img_lst .= "<tr><td colspan=\"10\"><i>" . tl("Deze map is leeg") . "</i></td></tr>";
    } else {
        $telBestanden = 1;

        //Toonmappen
        while ($tel_array < $stop) {
            $extentie = "";
            if (isset($directory[$tel_array][1])) {
                $extentie = $directory[$tel_array][1];
            }
            $file = "";
            $fileCompleetPath = "";
            if (isset($directory[$tel_array][0])) {
                $file = $directory[$tel_array][0];
                $fileCompleetPath = $file;
            }
            $file = preg_replace("#//+#", '/', $file);
            $IMAGES_BASE_DIR = preg_replace("#//+#", '/', $IMAGES_BASE_DIR);
            $file = preg_replace("#$IMAGES_BASE_DIR#", '', $file);
            $file = substr($file, 1, strlen($file));
            $download = "";
            $sizeUitdruk = "";
            $bestandMap = "";

            $substr = substr($fileCompleetPath, 0, 20);
            $toonPlaatsFactuur = explode("$bestandlocatie/factuur", $fileCompleetPath);
            $toonPlaatsOfferte = explode("$bestandlocatie/projecten", $fileCompleetPath);
            $toonPlaatsKlanten = explode("$bestandlocatie/klanten", $fileCompleetPath);

            if (count($toonPlaatsFactuur) > 1) {
                $idFac = substr($file, 4, strlen($file));
                $query = $pdo->prepare('SELECT * FROM facturen WHERE ID = (:id*1) limit 1;');
                $query->bindValue('id', $idFac);
                $query->execute();
                $data_recht = $query->fetch(PDO::FETCH_ASSOC);

                if ($data_recht !== false) {
                    $bestandsNaamm = $data_recht["OFFERTENR"] . " - " . $data_recht["NAAM"];
                } else {
                    $bestandsNaamm = $file . "." . $extentie;
                }
            } elseif (count($toonPlaatsOfferte) > 1) {
                $query = $pdo->prepare('SELECT * FROM offerte WHERE ID = (:id*1) limit 1;');
                $query->bindValue('id', $file);
                $query->execute();
                $data_recht = $query->fetch(PDO::FETCH_ASSOC);

                if ($data_recht !== false) {
                    $bestandsNaamm = $data_recht["OFFERTENR"] . " - " . $data_recht["NAAM"];
                } else {
                    $bestandsNaamm = $file . "." . $extentie;
                }
            } elseif (count($toonPlaatsKlanten) > 1) {
                $getNaamRelatie = getNaamRelatie($file);
                if ($getNaamRelatie != "N/A") {
                    $bestandsNaamm = $getNaamRelatie;
                } else {
                    $bestandsNaamm = $file . "." . $extentie;
                }
            } elseif ($extentie == "-MAP-") {
                $bestandsNaamm = $file;
            } else {
                $bestandsNaamm = $file . "." . $extentie;
            }

            if ($extentie == "-MAP-") {
                //Als het gaat om een map
                $aantalMappen++;
                $sizeFile = "Folder";
                $extentie = "";
                $filedatum = "1970-01-01";
                if (file_exists($directory[$tel_array][0])) {
                    $filedatum = date("Y-m-d", filemtime($directory[$tel_array][0]));
                }
                $locatie = url2url($UPLOAD_BASE_URL . $file);
                $urlNaarBestand = "content.php?SITE=$typePage&BID=$bedrijfsid" . $poespas . "&ID=$id&plaats=$locatie";
                $download = "<img src=\"images/" . getExtentionIcon("-MAP-") . "\" border=\"0\" />";
                $checkAll = $checkAll . "MAP_$tel_map,";

                $bestandMap = "<tr onMouseOver=\"this.style.background='#cccccc';\" onMouseOut=\"this.style.background='#FFFFFF';\">";

                $WaarHangtHetOnder = getCategorieIDFile($UPLOAD_BASE_URL . "" . $file, $bedrijfsid);
                $query = $pdo->prepare('SELECT * FROM documentcatagorie WHERE ID = :id limit 1;');
                $query->bindValue('id', $WaarHangtHetOnder);
                $query->execute();
                $dataBestand = $query->fetch(PDO::FETCH_ASSOC);

                if ($dataBestand !== false) {
                    $bestandMap .= "\n<td><input type=\"checkbox\" id=\"MAP_$tel_map\" name=\"MAP_$tel_map\" value=\"" . $directory[$tel_array][0] . "\"></td>";
                } else {
                    $bestandMap .= "\n<td></td>";
                }
                $bestandMap .= "\n<TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\">$download</TD>
                                           <TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\">" . str_replace(".-MAP-",
                        "", $bestandsNaamm) . "</TD>
                                           <TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\">$sizeFile</TD>
                                           <TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\"><nobr>" . datumconversieTotal($filedatum) . "</nobr></TD>
                                         </tr>";
                $tel_map++;

                $html_img_lst .= $bestandMap;
                $telBestanden++;
            }

            $tel_array++;
        }

        $tel_array = 0;

        //Toon bestanden
        while ($tel_array < $stop) {
            $extentie = "";
            if (isset($directory[$tel_array][1])) {
                $extentie = $directory[$tel_array][1];
            }
            $file = "";
            $fileCompleetPath = "";
            if (isset($directory[$tel_array][0])) {
                $file = $directory[$tel_array][0];
                $fileCompleetPath = $file;
            }
            $bestandsType = "";
            if (isset($directory[$tel_array][2])) {
                $bestandsType = $directory[$tel_array][2];
            }

            $file = preg_replace("#//+#", '/', $file);
            $IMAGES_BASE_DIR = preg_replace("#//+#", '/', $IMAGES_BASE_DIR);
            $file = preg_replace("#$IMAGES_BASE_DIR#", '', $file);
            $file = substr($file, 1, strlen($file));
            $download = "";
            $sizeFile = "";
//            $sizeUitdruk = "";
//            $bestandMap = "";

            $substr = substr($fileCompleetPath, 0, (strlen($bestandlocatie) + 8));

            $tags = "";
            $filedatum = "1970-01-01";
            $VERSIENR = "1";
            $bekeken = "1";
            $owner = $bedrijfsid;
            $selectedOrdner = "";
            $urlNaarBestand = "";
            if ($bestandsType == "documentbeheer") { //Toon bestanden in DB
                $query = $pdo->prepare('SELECT * FROM documentbeheer WHERE ID = :id limit 1;');
                $query->bindValue('id', $directory[$tel_array][3]);
                $query->execute();
                $dataBestand = $query->fetch(PDO::FETCH_ASSOC);

                if ($dataBestand !== false) {
                    $extentie = strtolower(str_replace("application/", "", $dataBestand["BESTANDTYPE"]));
                    $extentie = str_replace("vnd.ms-excel", "xls", $extentie);

                    $bestandsNaamm = $dataBestand["NAAM"];
                    if ($bestandsNaamm == "") {
                        $bestandsNaamm = $dataBestand["BESTANDSNAAM"];
                    }

                    $urlNaarBestand = "document.php?DID=" . $directory[$tel_array][3];
                    $download = "<img src=\"images/" . getExtentionIcon($extentie) . "\" border=\"0\" />";
                    $sizeFile = $dataBestand["BESTANDSIZE"];
                    $tags = $dataBestand["TAGS"];
                    $filedatum = $dataBestand["DATUM"];
                    $VERSIENR = $dataBestand["VERSIENR"];
                    $bekeken = $dataBestand["BEKEKEN"];
                    $owner = $dataBestand["BEDRIJFSID"];
                    $selectedOrdner = "<option value=\"" . $dataBestand["ORDNER"] . "\">" . getNaam("stam_ordners",
                            $dataBestand["ORDNER"], "NAAM") . "</option>";
                }
            } elseif ($bestandsType == "documenten") { //Toon bestanden in DB
                $query = $pdo->prepare('SELECT * FROM documenten WHERE ID = :id limit 1;');
                $query->bindValue('id', $directory[$tel_array][3]);
                $query->execute();
                $dataBestand = $query->fetch(PDO::FETCH_ASSOC);

                if ($dataBestand !== false) {
                    $bestandsNaamm = $dataBestand["NAAM"];
                    if ($bestandsNaamm == "") {
                        $bestandsNaamm = $dataBestand["BESTANDSNAAM"];
                    }
                    $urlNaarBestand = "document.php?ID=" . $directory[$tel_array][3];
                    $download = "<img src=\"images/" . getExtentionIcon("pdf") . "\" border=\"0\" />";
                    $filedatum = "";
                    if (isset($data_recht["DATUMTIJD"])) {
                        $filedatum = $data_recht["DATUMTIJD"];
                    }
                    $sizeFile = 0;
                }
            } elseif ($substr == "$bestandlocatie/projecten/" || $substr == "$bestandlocatie/projecten") {
                $query = $pdo->prepare('SELECT * FROM offerte WHERE ID = ("' . $file . '"*1);');
                $query->execute();
                $data_recht = $query->fetch(PDO::FETCH_ASSOC);

                if ($data_recht !== false) {
                    $bestandsNaamm = $data_recht["OFFERTENR"] . " -- " . $data_recht["NAAM"];
                    $filedatum = $data_recht["STARTDATUM"];
                } else {
                    $bestandsNaamm = $file . "." . $extentie;
                }
            } elseif ($substr == "$bestandlocatie/klanten/" || $substr == "$bestandlocatie/klanten") {
                $query = $pdo->prepare('SELECT * FROM klanten WHERE ID = ("' . addslashes($file) . '"*1) limit 1;');
                $query->execute();
                $data_recht = $query->fetch(PDO::FETCH_ASSOC);

                if ($data_recht !== false) {
                    if ($data_recht["BEDRIJFSNAAM"] != "") {
                        $bestandsNaamm = stripslashes($data_recht["BEDRIJFSNAAM"]);
                    } else {
                        $bestandsNaamm = $data_recht["VOORNAAM"] . " " . $data_recht["TUSSENVOEGSEL"] . " " . $data_recht["ACHTERNAAM"];
                    }
                } else {
                    $bestandsNaamm = $file . "." . $extentie;
                }
            } else {
                $bestandsNaamm = $file . "." . $extentie;

                if (isset($directory[$tel_array][0]) && isset($directory[$tel_array][1]) && $extentie != "-MAP-") {
                    if (filemtime(($directory[$tel_array][0] . "." . $directory[$tel_array][1]))) {
                        $filedatum = date("Y-m-d",
                            filemtime(($directory[$tel_array][0] . "." . $directory[$tel_array][1])));
                    }
                }
            }

            if ($extentie != "-MAP-") { //Toon bestanden
                //Als het gaat om een file

                if ($sizeFile == "") {
                    $sizeFile = @filesize($IMAGES_BASE_DIR . "/" . $file . "." . $extentie);
                }
                $sizeFile = (int)($sizeFile / 1024);
                if ($download == "") {
                    $urlNaarBestand = str_replace("\\", "/", $IMAGES_BASE_DIR) . "/$file.$extentie";
                    $download = "<img src=\"images/" . getExtentionIcon($extentie) . "\" border=\"0\" title=\"$bestandsNaamm\" alt=\"$bestandsNaamm\" />";
                }
                $aantalBestanden++;
                $sizeUitdruk = "kb";
                $checkAll = $checkAll . "BEST_$tel_best,";

                $best_id = "";
                if ($bestandsType == "documentbeheer") {
                    $best_id = "dbid" . $directory[$tel_array][3];
                } elseif ($bestandsType == "documenten") {
                    $best_id = "dcid" . $directory[$tel_array][3];
                } elseif (isset($directory[$tel_array][0])) {
                    $best_id = $directory[$tel_array][0] . "." . @$directory[$tel_array][1];
                }

                $bestandsnaamKort = $bestandsNaamm;
                if (strlen($bestandsnaamKort) > 40) {
                    $bestandsnaamKort = substr($bestandsNaamm, 0, 49) . "..";
                }

                $bestand = "<TR onMouseOver=\"this.style.background='#cccccc';\" onMouseOut=\"this.style.background='#FFFFFF';\">";

                if ($rechten == "schrijf") {
                    $bestand .= "<TD><input type=\"checkbox\" id=\"BEST_$tel_best\" name=\"BEST_$tel_best\" value=\"" . $best_id . "\"></TD>";
                } else {
                    $bestand .= "<TD></TD>";
                }

                $bestand .= "<TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\">&nbsp;$download&nbsp;</TD>
                                          <td class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\"><span title=\"$bestandsNaamm\">$bestandsnaamKort</span></td>";
                if ($sizeFile > 0) {
                    $bestand .= "<TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\" class=\"windowsFileButtons\">$sizeFile $sizeUitdruk</TD>";
                } else {
                    $bestand .= "<TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\"></TD>";
                }

                $bestand .= "<TD class=\"cursor\" onClick=\"window.location.replace('$urlNaarBestand');\"><nobr>" . datumconversieTotal($filedatum) . "</nobr></TD>";

                if ($bestandsType == "documentbeheer" && $rechten == "schrijf") {
                    $bestand .= "<TD align=\"right\"><div class=\"cursor\" onClick=\"toggle('EXTR_$tel_best');\">edit &#187;</div>

                                             <div id=\"EXTR_$tel_best\" class=\"selectionboxAjaxproject\" style=\"background: #FFFFFF; padding: 5px; z-index: 100; position: absolute; display:none; left: 190px; width: 300px; height: 300px; border: 1px solid #000000;\">
                                               <div class=\"ajaxBalkSluiten\"><span style=\"float: left;\">" . verkort($bestandsNaamm,
                            20) . "</span><span class=\"cursor\" onClick=\"toggle('EXTR_$tel_best');\">" . tl("Sluiten") . "&nbsp;&nbsp;</span></div>
                                               <table class=\"mainfield\">
                                                  <tr><td width=\"150\">" . tl("Naam") . ":</td><td><input type=\"text\" id=\"BESTANDSNAAM$tel_best\" value=\"" . ($bestandsNaamm) . "\" class=\"fieldHalf\" /></td></tr>
                                                  <tr><td>" . tl("Ordner") . ":</td><td><select id=\"ORDNER$tel_best\" class=\"fieldHalf\">{$selectedOrdner}{$ordners}</select></td></tr>
                                                  <tr><td>" . tl("Datum") . ":</td><td><input type=\"text\" id=\"DATUM$tel_best\" value=\"" . $filedatum . "\" class=\"fieldHalf\" /></td></tr>
                                                  <tr><td>" . tl("Tags") . ":</td><td><textarea id=\"TAGS$tel_best\" class=\"fieldHalf\" style=\"height:75px;\">" . $tags . "</textarea></td></tr>
                                                  <tr><td>" . tl("Versie") . ":</td><td><input type=\"text\" id=\"VERSIONNR$tel_best\" value=\"" . $VERSIENR . "\" class=\"fieldHalf\" /></td></tr>
                                                  <tr><td>" . tl("Eigenaar") . ":</td><td>" . getBedrijfsnaam($owner) . "</td></tr>
                                                  <tr><td>" . tl("Bekeken") . ":</td><td>$bekeken</td></tr>
                                                  <tr><td></td><td><div id=\"setBestandInformatie$tel_best\"><input type=\"button\" class=\"button2\" value=\"" . tl("Opslaan") . "\" onClick=\"getPageContent2('content.php?SITE=setBestandInformatie&ID=$best_id&BESTANDSNAAM='+escape(document.getElementById('BESTANDSNAAM$tel_best').value)+'&ORDNER='+escape(document.getElementById('ORDNER$tel_best').value)+'&DATUM='+escape(document.getElementById('DATUM$tel_best').value)+'&TAGS='+escape(document.getElementById('TAGS$tel_best').value)+'&VERSIONNR='+escape(document.getElementById('VERSIONNR$tel_best').value), 'setBestandInformatie$tel_best', searchReq2);\" /></div></td></tr>
                                               </table>
                                             </div></td>";
                } else {
                    $bestand .= "<td></td>";
                }

                $bestand .= "</tr>";

                $tel_best++;

                $html_img_lst .= $bestand;

                $telBestanden++;
            }

            $tel_array++;
        }

        $html_img_lst .= "<input type=\"hidden\" name=\"totaantbest\" value=\"$tel_array\">";
    }

    /*
            $size = 0;
            //echo $basisdir;
            if($basisdir != "")
            {
                $size = get_folder_size($basisdir, false);
                // $size has the value of bytes found in directory
                // output size in kb
                $size =  prijsconversie(($size/1024))."kb";
            }
           echo("<BR />
           <div class=\"windowsMenuVakHeader\">".tl("Details")."</div>
           <div class=\"windowsMenuVak\">
           ".tl("GEBRUIKTERUIMTE").": $size
           <BR>".tl("AANTALMAPPEN").": $aantalMappen
           <BR>".tl("AANTALBESTANDEN").": $aantalBestanden
           </div>
    */

    echo("<TABLE width=100% height=\"100%\" border=\"0\" cellSpacing=\"0\" cellPadding=\"0\" class=\"mainfield\">
            <TR><TD colspan=\"120\" height=\"15\">");

    $kopBalk = $toonPlaats;
    if (isset($_REQUEST["BID"])) {
        $query = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id');
        $query->bindValue('id', $_REQUEST["BID"]);
        $query->execute();
        $databedr = $query->fetch(PDO::FETCH_ASSOC);

        if ($databedr !== false) {
            $kopBalk = $databedr["BEDRIJFSNAAM"] . " | " . $toonPlaats;
        }
    }

    updateDashboardVakje($userid, $bedrijfsid);

    if (!isKlant() && $poespas == "") {
        $button = "Start";
        if (strpos($basisdir, "werknemer") > 0) {
            $button = "Mijn documenten";
        }
        if (strpos($basisdir, "projecten") > 0) {
            $button = "Projecten";
        }
        if (strpos($basisdir, "klanten") > 0) {
            $button = "Klanten";
        }
        if (strpos($basisdir, "facturen") > 0) {
            $button = "Facturen";
        }

        kopBalk($kopBalk, "bestandsBeheer");
        bestandenButtons($button);
    }

    if (!isKlant() && !isOrangeField()) { // && $poespas == ""
        $toonPath = toonPath($basisdir, $bestandlocatie, $bedrijfsid, $userid);
        if ($toonPath != "") {
            echo("<tr>");
            echo("<td colspan=\"10\" height=\"20\">$toonPath</td>");
            echo("</tr>");
        }
    }

    echo("</TD></tr>
            <tr>
            <TD valign=\"top\" colspan=\"100\">
            <div class=\"contentclass\" style=\"overflow:auto;\">
                <!-- files -->
                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" class=\"mainfield\">");

    if ($rechten == "schrijf") {
        echo("<tr>
                            <td rowspan=\"10000\" valign=\"top\" align=\"left\">
                              <img src=\"./images/px.gif\" width=\"150\" height=\"1\" />");

        FunctiesBestanden($typePage, $basisdir, $id, $bedrijfsid, $userid);
        echo("</td>
                            <td colspan=\"6\" height=\"1\" width=\"$totaleBreette\"></td>
                        </tr>");
    }

    $locatie = url2url($basisdir . $poespas);
    echo("<form action=\"content.php?SITE=$typePage&ID=$id&BID=$bedrijfsid&plaats=$locatie\" method=\"Post\">
                <tr><td valign=\"top\" align=\"left\">
                  <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" class=\"sortable\">
                  <tr><th width=\"20\"></th><th width=\"30\"></th><th width=\"100%\" style=\"text-align: left;\">" . tl("Naam") . "</th><th width=\"75\">" . tl("Grootte") . "</th><th width=\"100\">" . tl("Datum") . "</th><th width=\"40\" style=\"text-align: right;\">" . tl("Actie") . "</th></tr>
                  " . $html_img_lst . "
                  </table>");

    if ($rechten == "schrijf") {
        echo("<br/><hr size=\"1\" />
                           <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mainfield\">
                           <TR>");
        if ($html_img_lst != "<tr><td><i>" . tl("Deze map is leeg") . "</i></td></tr>") {
            echo("<TD valign=\"top\" width=\"20\"><input type=\"checkbox\" id=\"selectAll\" onclick=\"checkUncheckSome('selectAll', '$checkAll');\" /></TD>");
            echo("<TD valign=\"top\"><input class=\"button2\" type=\"submit\" name=\"delete\" value=\"" . tl("Selectie verwijderen") . "\" onClick=\"return confirm('" . tl("Weet u het zeker?") . "')\" /></TD>");
            echo("<TD valign=\"top\"><input class=\"button2\" type=\"submit\" name=\"copyfile\" value=\"" . tl("Selectie kopieren") . "\" /></TD>");
            if (isRechten("WEBMAIL")) {
                echo("<td valign=\"top\"><input class=\"button2\" type=\"submit\" value=\"" . tl("Selectie mailen") . "\" name=\"mailfile\" /></td>");
            }
        }

        $query = $pdo->prepare('SELECT ID FROM klembord WHERE (PERSONEELSLID*1) = (:personeelslid*1) limit 1;');
        $query->bindValue('personeelslid', $userid);
        $query->execute();
        $data_recht = $query->fetch(PDO::FETCH_ASSOC);

        if ($data_recht !== false) {
            $locatie = url2url($basisdir);
            echo("<TD valign=\"top\"><a class=\"button2\" href=\"content.php?SITE=ea&BID=$bedrijfsid&cut=true" . $poespas . "&plaats=$locatie\">" . tl("Verplaatsen") . "</a></TD>");
            echo("<TD valign=\"top\"><a class=\"button2\" href=\"content.php?SITE=ea&BID=$bedrijfsid&paste=true" . $poespas . "&plaats=$locatie\">" . tl("Plakken") . "</a></TD>");
        }

        echo("</TR>
                           </TABLE>");
    }
    echo("</td></tr>
                </TABLE>
            </div>
            </TD></tr>
            </table>
            </form>");

}

function locationSelection($bedrijfsid, $userid)
{
    global $pdo;

    //if aantal bedrijven == 1 dan direct naar EA
    $query = $pdo->prepare('SELECT * FROM bedrijf WHERE NOT (MANAGEDFROM = "" OR MANAGEDFROM = " " OR MANAGEDFROM = "0" OR MANAGEDFROM IS NULL) limit 2;');
    $query->execute();
    $aantalBVS = $query->rowCount();


    $query->execute();
    $data_bedrijf = $query->fetch(PDO::FETCH_ASSOC);

    if ($aantalBVS < 2 || isToegangEigenAdmin()) {
        algemeneBestandsbeheer();
    } else {
        echo("<TABLE width=100% height=\"100%\" border=\"0\" cellSpacing=\"0\" cellPadding=\"0\" class=\"mainfield\">
                  <TR><TD colspan=\"120\" height=\"15\">");

        kopBalk("Electronic Archive", "bestandsBeheer");

        echo("</TD></tr>
                  <tr>
                  <TD valign=\"top\" colspan=\"100\">
                  <div class=\"contentclass\" style=\"overflow:auto;\">
                      <!-- files -->
                      <TABLE border=\"0\" cellspacing=\"5\" cellpadding=\"0\" width=\"100%\" class=\"mainfield\">
                      <tr><td rowspan=\"10\" align=\"right\" valign=\"top\" width=\"150\">
                      <img src=\"./images/px.gif\" width=\"150\" height=\"1\" />");
        persoonsGegevens($userid);
        echo("</td>
                      <td colspan=\"6\" width=\"750\"><img src=\"./images/px.gif\" border=\"0\" width=\"750\" height=\"2\" /></td></tr>

                      <tr><td valign=\"top\" align=\"left\" >");

        if (isset($_REQUEST["GID"]) && (@$_REQUEST["GID"] * 1) != 0) {
            $GID = ps($_REQUEST["GID"], "nr");
            $query = $pdo->prepare('SELECT * FROM bedrijf where (MANAGEDFROM*1) = :form AND ACTIVE = "Ja" order by BEDRIJFSNAAM;');
            $query->bindValue('form', $GID);
            $query->execute();

            foreach ($query->fetchAll() as $data_bedrijf) {
                echo("\n <TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class=\"mainfield\" style=\"float: left; width: 160px; height: 100px;\">
                                         <TR>
                                         <TD valign=\"middle\" align=\"center\" rowspan=\"10\">
                                         <a href=\"content.php?SITE=ea&BID=" . ($data_bedrijf["ID"] * 1) . "\"><img src=\"./icoontjes/Folder-close.jpg\" border=\"0\" /></a>
                                         </TD></TR>
                                         <TR><TD class=\"windowsFileButtonsNorm\">" . stripslashes($data_bedrijf["BEDRIJFSNAAM"]) . "</TD></TR>
                                         </TABLE>\n");
            }
        } else {
            $query = $pdo->prepare('SELECT distinct stam_bedrijfsoffices.* FROM stam_bedrijfsoffices, bedrijf where (stam_bedrijfsoffices.ID*1) = (bedrijf.MANAGEDFROM*1) AND bedrijf.ACTIVE = "Ja" order by NAAM;');
            $query->execute();

            foreach ($query->fetchAll() as $data_groep) {
                if (db_num_rows($data_groep) == 1) {
                    rd("content.php?SITE=algBestbeheer&GID=" . ($data_groep["ID"] * 1));
                }
                echo("\n <TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class=\"mainfield\" style=\"float: left; width: 160px; height: 100px;\">
                                           <TR>
                                           <TD valign=\"middle\" align=\"center\" rowspan=\"10\">
                                           <a href=\"content.php?SITE=algBestbeheer&GID=" . ($data_groep["ID"] * 1) . "\"><img src=\"./icoontjes/Folder-close.jpg\" border=\"0\" /></a>
                                           </TD></TR>
                                           <TR><TD class=\"windowsFileButtonsNorm\">" . $data_groep["NAAM"] . "</TD></TR>
                                           </TABLE>\n");
            }
        }

        echo("</td></tr>
                      </TABLE>
                  </div>
                  </TD></tr>
                </TABLE>");
    }
}

function FunctiesBestanden($typePage, $basisdir, $id, $BID, $userid)
{
    global $db;

    $bedrijfsid = getMyBedrijfsID();

    echo("<div class=\"windowsZijkantClean\">");

    $poespas = poespas();

    if (toonDashboardVakje("bestandsbeheer", "Nieuwe map", $userid, $bedrijfsid, 'Aan')) {
        echo("<div class=\"dashboardVakje\">");
        echo dashboardVakjeHead("bestandsbeheer", "Nieuwe map", "Nieuwe map", "clock.png",
            "content.php?SITE=$typePage&ID=$id&BID=$BID" . $poespas, "", $userid, $bedrijfsid);
        if (minMaxDashboardVakje("bestandsbeheer", "Nieuwe map", $userid, $bedrijfsid)) {
            echo("<form method='post' action=\"content.php?SITE=$typePage&ID=$id&BID=$BID" . $poespas . "\" enctype=\"multipart/form-data\">
                      \n<input type=\"hidden\" name=\"plaats\" value=\"$basisdir\">
                      \n<input class=\"field3\" type=\"text\" name=\"map\" value=\"\" style=\"width: 146px\">
                      \n<input type='submit' class='button2' value='" . tl("Maak") . "' name='Maak' style=\"width: 146px\">
                  </form>");
        }
        echo("</div>");
        witregel(2);
    }

    if (toonDashboardVakje("bestandsbeheer", "Upload file", $userid, $bedrijfsid, 'Aan')) {
        echo("<div class=\"dashboardVakje\">");
        echo dashboardVakjeHead("bestandsbeheer", "Upload file", "Upload file", "clock.png",
            "content.php?SITE=$typePage&ID=$id&BID=$BID" . $poespas, "", $userid, $bedrijfsid);
        if (minMaxDashboardVakje("bestandsbeheer", "Upload file", $userid, $bedrijfsid)) {
            echo("<form method='post' action=\"content.php?SITE=$typePage&ID=$id&BID=$BID" . $poespas . "\" enctype=\"multipart/form-data\">
                  \n<input type=\"hidden\" name=\"plaats\" value=\"$basisdir\">
                  \n<input class=\"file\" type=\"file\" name=\"X_File\" value=\"\" size=\"11\">
                  \n<br/><input type='submit' class='button2' value='Upload' name='upload' style=\"width: 146px\" />
                  </form>");
            if (isOrangeField()) {
                echo "<i>only pdf files are allowed</i>";
                echo("<br/><br/><br/><br/><br/>");
            }
        }
        echo("</div>");
        witregel(2);
    }


    if (false) { //isset($_SERVER['HTTP_USER_AGENT']) && strpos("mac", $_SERVER['HTTP_USER_AGENT']) == 0)
        if (toonDashboardVakje("bestandsbeheer", "Upload files", $userid, $bedrijfsid, 'Aan')) {
            echo("<div class=\"dashboardVakje\">");
            echo dashboardVakjeHead("bestandsbeheer", "Upload files", "Upload files", "clock.png",
                "content.php?SITE=$typePage&ID=$id&BID=$BID" . $poespas, "", $userid, $bedrijfsid);
            if (minMaxDashboardVakje("bestandsbeheer", "Upload files", $userid, $bedrijfsid)) {
                echo "<applet type=\"application/x-java-applet\" name=\"uploadApplet\" id=\"uploadApplet\" code=\"dndapplet/applet/DNDApplet.class\" archive=\"dndapplet/signed_dndapplet.jar\" mayscript=\"true\" scriptable=\"true\" width=150px height=100px>
                      <param name=\"uploadPath\" value=\"?SITE=draganddropupload&BID=$BID&dir=" . url2url($basisdir) . "\">
                      <param name=\"funcNameHandleCurrentUpload\" value=\"handleCurrentUpload\">
                      </applet>";
            }
            echo("</div>");
            witregel(2);
        }
    }

    toonGatgets($userid, "content.php?SITE=$typePage&ID=$id&BID=$BID" . $poespas, "bestandsbeheer");

    echo("</div>");
}

function url2url($url)
{
    $url = ps($url);
    $url = str_replace("/", "XSLASHX", $url);
    $url = str_replace(" ", "XSPACEX", $url);
    return $url;
}

function url2urlBack($url)
{
    $url = ps($url);
    $url = str_replace("XSLASHX", "/", $url);
    $url = str_replace("XSPACEX", " ", $url);
    return $url;
}

function draganddropupload($userid, $loginid, $bedrijfsid)
{
    $message = "Failed";

    if (isset($_REQUEST['dir'])) {
        $basisdir = url2urlBack($_REQUEST['dir']);
        if (isset($_REQUEST['BID']) && isset($_FILES['uploadfile'])) {
            $BID = ps($_REQUEST['BID'], "nr");
            $IMAGES_BASE_DIR = $basisdir;//getenv("DOCUMENT_ROOT").$IMAGES_BASE_URL;
            $UPLOAD_BASE_URL = $basisdir;//."/";
            $UPLOAD_BASE_DIR = $UPLOAD_BASE_URL; //getenv("DOCUMENT_ROOT").$UPLOAD_BASE_URL;
            $message = "";
            $typeFileAllowed = "";

            if (isOrangeField()) {
                $typeFileAllowed = "pdf";
            }

            if (uploadBestandInDB($_FILES['uploadfile'], $BID, $userid, $UPLOAD_BASE_DIR, 0,
                    $typeFileAllowed) > 0) {
                $message = "Upload succesful " . $UPLOAD_BASE_URL;
            } else {
                $message = "Upload failed";
            }
        }
    }
    echo $message;
}

function bestandselecteertool($userid, $bedrijfsid)
{
    global $pdo;

    kopBalk("File search", "File search");

    $INPUT = "";
    if (@$_REQUEST["INPUT"] != "") {
        $INPUT = "AND documentbeheer.TAGS like '%" . ps($_REQUEST["INPUT"]) . "%' OR documentbeheer.BESTANDSNAAM like '%" . ps($_REQUEST["INPUT"]) . "%'";
    }
    $DATUM = "";
    if (@$_REQUEST["DATUM"] != "") {
        $DATUM = "AND documentbeheer.DATUM = '" . ps($_REQUEST["DATUM"]) . "'";
    }
    $BID = "";
    if (@$_REQUEST["BID"] != "") {
        $BID = "AND documentbeheer.BEDRIJFSID = '" . ps($_REQUEST["BID"], "nr") . "'";
    }
    $ORDNER = "";
    if (@$_REQUEST["ORDNER"] != "") {
        if ($_REQUEST["ORDNER"] == "NA" || ps($_REQUEST["ORDNER"], "nr") == "0") {
            $ORDNER = "AND documentbeheer.ORDNER = '' OR documentbeheer.ORDNER is null";
        } else {
            $ORDNER = "AND documentbeheer.ORDNER = '" . ps($_REQUEST["ORDNER"], "nr") . "'";
        }
    }

    $aantaldocs = 35;
    $pagina = 1;
    if (isset($_REQUEST["PAGNR"])) {
        $pagina = $_REQUEST["PAGNR"];
    }

    echo("<h2>" . tl("Zoeken") . "</h2><form action=\"\" method=\"Post\">
          <table border=\"0\" class=\"mainfield\">
          <tr><td>Select Fund</td><td><select name=\"BID\" class=\"field\">");

    if (isToegangEigenAdmin()) {
        $query = $pdo->prepare('SELECT * FROM bedrijf where ID = :bedrijfsid order by BEDRIJFSNAAM;');
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $query->execute();

        foreach ($query->fetchAll() as $data) {
            echo("<option value=\"" . ($data["BEDRIJFSID"] * 1) . "\">" . lb($data["BEDRIJFSNAAM"]) . "</option>");
        }
    } else {
        echo("<option value=\"\"></option>");

        $query = $pdo->prepare('SELECT * FROM bedrijf where ACTIVE = "Ja" order by BEDRIJFSNAAM;');
        $query->execute();

        foreach ($query->fetchAll() as $data) {
            $selected = "";
            if (isset($_REQUEST["BID"])) {
                if ($_REQUEST["BID"] == ($data["BEDRIJFSID"] * 1)) {
                    $selected = "selected=\"true\"";
                }
            }
            echo("<option value=\"" . ($data["BEDRIJFSID"] * 1) . "\" $selected>" . lb($data["BEDRIJFSNAAM"]) . "</option>");
        }
    }

    echo("</select></td>
          <td>Select Date</td><td><select name=\"DATUM\" class=\"fieldHalf\"><option value=\"\"></option>");
    $query = $pdo->prepare('SELECT DATUM FROM documentbeheer GROUP BY DATUM order by DATUM DESC limit 20;');
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        $selected = "";
        if (isset($_REQUEST["DATUM"])) {
            if ($_REQUEST["DATUM"] == $data["DATUM"]) {
                $selected = "selected=\"true\"";
            }
        }
        echo("<option value=\"" . $data["DATUM"] . "\" $selected>" . datumconversiePresentatie($data["DATUM"]) . "</option>");
    }
    echo("</select></td>
          <td>Free input</td><td><input type=\"text\" class=\"fieldHalf\" name=\"INPUT\" value=\"" . @$_REQUEST["INPUT"] . "\" /></td>
          <td><input type=\"submit\" class=\"button2\" name=\"ZOEKEN_STAP1\" value=\"View\" /></td></tr>");
    echo("</table>
          </form>");

    if (isset($_REQUEST["ZOEKEN_STAP1"])) {
        echo("<br/><h2>Choose type</h2>");

        $urlToev = "";
        if (@$_REQUEST["INPUT"] != "") {
            $urlToev .= "&INPUT=" . ps($_REQUEST["INPUT"]);
        }
        if (@$_REQUEST["DATUM"] != "") {
            $urlToev .= "&DATUM=" . ps($_REQUEST["DATUM"]);
        }
        if (@$_REQUEST["BID"] != "") {
            $urlToev .= "&BID=" . ps($_REQUEST["BID"]);
        }

        $query = $pdo->prepare('SELECT stam_ordners.* FROM documentbeheer, stam_ordners where documentbeheer.ORDNER = stam_ordners.ID $INPUT $DATUM $BID GROUP BY documentbeheer.ORDNER ORDER BY stam_ordners.NAAM;');
        $query->execute();

        foreach ($query->fetchAll() as $data_res) {
            echo("<li type=\"square\"><a href=\"content.php?SITE=bestandselecteertool&ZOEKEN=true&PAGNR=0{$urlToev}&ORDNER=" . ($data_res["ID"] * 1) . "\">" . $data_res["NAAM"] . "</a></li>");
        }

        $query = $pdo->prepare('SELECT * FROM documentbeheer where documentbeheer.ORDNER is null or documentbeheer.ORDNER = "" limit 1;');
        $query->execute();
        $data_res = $query->fetch(PDO::FETCH_ASSOC);

        if ($data_res !== false) {
            echo("<li type=\"square\"><a href=\"content.php?SITE=bestandselecteertool&ZOEKEN=true&PAGNR=0{$urlToev}&ORDNER=NA\">N/A</a></li>");
        }
    } elseif (isset($_REQUEST["ZOEKEN"])) {
        $bestandslocatie = bestandLocatie();

        $urlToev = "";
        if (@$_REQUEST["INPUT"] != "") {
            $urlToev .= "&INPUT=" . ps($_REQUEST["INPUT"]);
        }
        if (@$_REQUEST["DATUM"] != "") {
            $urlToev .= "&DATUM=" . ps($_REQUEST["DATUM"]);
        }
        if (@$_REQUEST["BID"] != "") {
            $urlToev .= "&BID=" . ps($_REQUEST["BID"]);
        }
        if (@$_REQUEST["ORDNER"] != "") {
            $urlToev .= "&ORDNER=" . ps($_REQUEST["ORDNER"]);
        }

        $ordners = "";
        $query = $pdo->prepare('SELECT * FROM stam_ordners ORDER BY NAAM;');
        $query->execute();

        foreach ($query->fetchAll() as $data_ordners) {
            $ordners .= "<option value=\"" . ($data_ordners["ID"] * 1) . "\">" . $data_ordners["NAAM"] . "</option>";
        }

        echo("<br/><h2>Results</h2>");

        //Paginanummering
        $resultaten = 0;
        $query = $pdo->prepare('SELECT COUNT(*) As AANTAL FROM documentbeheer where true ' . $INPUT . ' ' . $DATUM . ' ' . $BID . ' ' . $ORDNER . ';');
        $query->execute();
        $data_res = $query->fetch(PDO::FETCH_ASSOC);

        if ($data_res !== false) {
            $resultaten = $data_res["AANTAL"];
        }
        if ($resultaten > 0) {
            $paginas = floor(($resultaten / $aantaldocs));
            if ($paginas > 0) {
                echo("<center><a href=\"content.php?SITE=bestandselecteertool&ZOEKEN=true&PAGNR=0$urlToev\">&lt;&lt;</a>&nbsp;");

                for ($p = 1; $p <= $paginas; $p++) {
                    echo("<a href=\"content.php?SITE=bestandselecteertool&ZOEKEN=true&PAGNR=$p{$urlToev}\">$p</a>&nbsp;");
                }

                echo("<a href=\"content.php?SITE=bestandselecteertool&ZOEKEN=true&PAGNR=$paginas{$urlToev}\">&gt;&gt;</a></center><br/>");
            }
        }

        //Toon resultaten
        echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"sortable\" width=\"100%\">
              <tr>
                  <th>&nbsp;</th>
                  <th style=\"text-align: left;\">Filename</th>
                  <th style=\"text-align: right;\">Date</th>
                  <th style=\"text-align: right;\">Filesize</th>
                  <th style=\"text-align: left;\">Location</th>
                  <th style=\"text-align: center;\">Viewed</th>
                  <th style=\"text-align: left;\">Tags</th>
                  <th style=\"text-align: center;\">Edit</th>
              </tr>");

        $SQL = "SELECT * FROM documentbeheer where true $INPUT $DATUM $BID $ORDNER order by DATUM limit " . ($aantaldocs * $pagina) . ", $aantaldocs;";
        //echo $SQL;
        $query = $pdo->prepare($SQL);
        $query->execute();

        foreach ($query->fetchAll() as $data) {
            $tel_best = $data["ID"] * 1;
            $selectedOrdner = "<option value=\"" . $data["ORDNER"] . "\">" . getNaam("stam_ordners",
                    $data["ORDNER"],
                    "NAAM") . "</option>";
            $bestand = "<div id=\"EXTR_$tel_best\" style=\"display:none;\">
                           <div style=\"background:#e3e3e3; height:20px;\" align=\"right\"><a onClick=\"expandDiscussion('EXTR_$tel_best');\">Close&nbsp;</a></div>
                           <table class=\"mainfield\">
                              <tr><td width=\"130\">&nbsp;</td><td><b>More information:</b></td></tr>
                              <tr><td>File:</td><td><input type=\"text\" id=\"BESTANDSNAAM$tel_best\" value=\"" . $data["BESTANDSNAAM"] . "\" class=\"fieldHalf\" /></td></tr>
                              <tr><td>Type:</td><td><select id=\"ORDNER$tel_best\" class=\"fieldHalf\">{$selectedOrdner}{$ordners}</select></td></tr>
                              <tr><td>Date:</td><td><input type=\"text\" id=\"DATUM$tel_best\" value=\"" . $data["DATUM"] . "\" class=\"fieldHalf\" /></td></tr>
                              <tr><td>Tags:</td><td><textarea id=\"TAGS$tel_best\" class=\"fieldHalf\" style=\"height:75px;\">" . $data["TAGS"] . "</textarea></td></tr>
                              <tr><td></td><td><div id=\"setBestandInformatie$tel_best\"><input type=\"button\" class=\"button2\" value=\"" . tl("Opslaan") . "\" onClick=\"getPageContent2('content.php?SITE=setBestandInformatie&ID=" . $data["ID"] . "&BESTANDSNAAM='+escape(document.getElementById('BESTANDSNAAM$tel_best').value)+'&ORDNER='+escape(document.getElementById('ORDNER$tel_best').value)+'&DATUM='+escape(document.getElementById('DATUM$tel_best').value)+'&TAGS='+escape(document.getElementById('TAGS$tel_best').value)+'&VERSIONNR=1', 'setBestandInformatie$tel_best', searchReq2);\" /></div></td></tr>
                           </table>
                        </div>";

            echo("<tr>
                    <td align=\"center\" width=\"20\">");
            echo("<a target=\"_blank\" href=\"document.php?DID=" . ($data["ID"] * 1) . "\"><img src=\"images/" . getExtentionIcon($data["BESTANDTYPE"]) . "\" border=\"0\" width=\"16\" /></a>");
            echo("</td>
                    <td align=\"left\"><a target=\"_blank\" href=\"document.php?DID=" . ($data["ID"] * 1) . "\">" . $data["BESTANDSNAAM"] . "</a></td>
                    <td align=\"right\">" . datumconversiePresentatie($data["DATUM"]) . "</td>
                    <td align=\"right\">" . prijsconversiePuntenKommas(($data["BESTANDSIZE"] / 1024)) . " Kb.</td>
                    <td align=\"left\">" . str_replace("$bestandslocatie/openruimte", "",
                    getPathFromCatID($data["CATAGORIEID"], $data["BEDRIJFSID"], "")) . "</td>
                    <td align=\"center\">" . rondAf($data["BEKEKEN"]) . "</td>
                    <td align=\"left\">" . substr($data["TAGS"], 0, 40) . "</td>
                    <td align=\"left\"><div onClick=\"expandDiscussion('EXTR_$tel_best');\">edit</div></td>
                 </tr>
                 <tr><td colspan=\"10\" align=\"left\">$bestand</td></tr>");
        }
        echo("</table>");
    }
}

function getExtentionIcon($extentie)
{
    $extentie = strtolower($extentie);

    if ($extentie == "-map-") {
        $plaatje = "ongewensteemail.jpg";
    } elseif ($extentie == "jpg" || $extentie == "gif" || $extentie == "jpeg" || $extentie == "bmp" || $extentie == "psd" || $extentie == "tif" || $extentie == "tiff" || $extentie == "esp" || $extentie == "eps") {
        $plaatje = "zoeken-klein.png";
    } elseif ($extentie == "pdf" || $extentie == "application/pdf") {
        $plaatje = "pdf2icon.gif";
        //$img             = "<img src=\"preview.php?FILE=./$urlNaarBestand\" border=\"0\" height=\"40\" />";
    } elseif ($extentie == "txt" || $extentie == "doc" || $extentie == "docx" || $extentie == "msword" || $extentie == "application/vnd.ms-word" || $extentie == "text/plain") {
        $plaatje = "page_white_word.png";
    } elseif ($extentie == "xls" || $extentie == "xlsx" || $extentie == "msexcel" || $extentie == "application/vnd.ms-excel") {
        $plaatje = "page_white_excel.png";
    } else {
        $plaatje = "peperclip.jpg";
    }

    return $plaatje;
}


function bestandenInsertVeld($veldNaam, $value, $type)
{
    global $pdo;

    $output = "";

    $bestandlocatie = bestandLocatie();
    $path = "$bestandlocatie/openruimte/";

    if ($type == "cv" || $type == "personeel" || $type == "klanten" || $type == "contactpersoon" || $type == "nieuws" || $type == "doctemplate") {
        $path = "$bestandlocatie/achtergrond/";
    } elseif ($type == "bedrijf" || $type == "offerteteksten") {
        $path = "$bestandlocatie/logo/";
    } elseif ($type == "kostenintern") {
        $path = "$bestandlocatie/inkoop/";
    } elseif ($type == "documentbeheer") {
        if (isset($_REQUEST["CATAGORIEID"])) {
            $query = $pdo->prepare('SELECT * FROM documentcatagorie WHERE ID = :id LIMIT 1;');
            $query->bindValue('id', $_REQUEST["CATAGORIEID"]);
            $query->execute();
            $data_XX1 = $query->fetch(PDO::FETCH_ASSOC);

            if ($data_XX1 !== false) {
                $catagorielocatie = lb($data_XX1["NAAM"]) . "/";
                $path = "$bestandlocatie/openruimte/$catagorielocatie";
            }
        }
    }
    $bnaam = $value;
    $bestandsnaam = $path . $bnaam;
    if (is_numeric($bnaam)) {
        $bestandsnaam = "document.php?DID=$bnaam";
        $bnaam = "document";
    }

    if ($value != "" && $value != "0") {
        $output .= "<input type=\"radio\" name=\"BEHOUD" . $veldNaam . "\" value=\"BEHOUD\" checked=\"true\" /> <a target=\"_blank\" href=\"$bestandsnaam\">Open: $bnaam</a>
                   <br/><input type=\"radio\" name=\"BEHOUD" . $veldNaam . "\" value=\"DELETE\" /> " . tl("Verwijder bestand");
    } else {
        $output .= "<input type=\"file\" class=\"field\" name=\"" . $veldNaam . "\" value=\"\" />";
    }
    return $output;
}
