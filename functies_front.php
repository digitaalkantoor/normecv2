<?php

include("functies_xml.php");
include("functies_security.php");

function homepage()
{
    headHomePage();

    middenveld();

    writeFooter();
}

function headWelkomPage($location)
{
    $pakketTitel = pakketTitel($location);

    echo("<DOCTYPE html>
\n<html>
\n<head>
\n  <meta http-equiv=\"author\" content=\"Bob Salm @ www.minded.nl\">
\n  <meta-http-equiv=\"description\" content=\"\">
\n  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">
\n  <meta name=\"category\" content=\"$pakketTitel\">
\n  <meta name=\"subject\" content=\"$pakketTitel\">
\n  <meta name=\"robots\" content=\"none\">
\n  <meta name=\"Content-Language\" content=\"nl\">
\n  <link rel=\"SHORTCUT ICON\" href=\"favicon.ico\">
\n  <link rel=\"icon\" type=\"image/x-icon\" href=\"favicon.ico\">
\n  <title>Digital Office: " . config("environment") . "</title>");
}

function homepagepda()
{
    $location = "..";
    headWelkomPage($location);

    $toevoeging = ""; //date("dhym");
    $administraties = getAdministraties();

    echo("<link href=\"../style/styleFront.css\" type=text/css rel=stylesheet>
      </head>
      <body leftmargin=\"0\" rightmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");

    if (loginMetSMS($location) == "Ja") {
        if (isset($_POST["Login"])) {
            loginSMSStap2($toevoeging, $administraties, $location);
        } else {
            loginSMSStap1($toevoeging, $administraties);
        }
    } else {
        loginNaamWachtwoord($toevoeging, $administraties, $location);
    }

    writeFooter();
}

function headHomePage()
{
    headWelkomPage(".");

    echo("<link href=\"./style/styleFront.css\" type=text/css rel=stylesheet>
     </head>
     <body leftmargin=\"0\" rightmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");

}

function setCompanyDatabaseFront($hash)
{
    if (!isset($_SESSION)) {
        session_start();
    }
    $_SESSION['DB'] = $hash;
    //setcookie("LID", $loginid, time()+3600);
}

function middenveld()
{
    global $db;

    //Reset elke cookie
    //if(!isset($_SESSION)) { session_start(); }
    //setcookie ("USERNAME", "", time()-36000, "/");
    //setcookie ("PASSWORD", "", time()-36000, "/");
    //setcookie ("LID", "", time()-36000, "/");
    //setcookie ("DB", "", time()-36000, "/");
    //setcookie ("PDA", "", time()-36000, "/");
    //setcookie ("IP", "", time()-36000, "/");
    //session_destroy();

    $toevoeging = ""; //date("dhym");
    $administraties = getAdministraties();
    $pakketTitel = pakketTitel();
    $footerLogin = FooterLogin();

    echo("<div id=sides>");

    echo "<div id=\"side_left\">";
    if (config("environment") == "Orangefield") {
        echo "<img src=\"./images/oft2/blocks.png\" id=\"blocks\" />";
    }

    echo("<div id=\"side_left_content\">");
//  echo "<img src=\"./images/oft2/logo_orangefield.png\" id=\"logo\" />";
    echo "<span id=\"welkomsttekst\">Welcome to Digital Office,<br/>your tool for managing<br/>corporate data.</span>";
    echo "<img src=\"./images/oft2/icon_chair.png\" id=\"chair\" />";
    echo "</div>";
    echo("</div>");


    if (config("environment") == "Vistra") {
        echo("<div id=\"side_right\" style=\"background: url('./images/oft2/bg_office_right.png');\">");
    } else {
        echo("<div id=\"side_right\">");
    }
    echo("<div id=\"side_right_content\">");
    if (isset($_REQUEST['ISSUE']) && $_REQUEST['ISSUE'] == 'forgot_password') {
        echo "<span id=\"login_text\">Fill in your e-mailaddress or User ID <br />to re-send your password</span>";
    }
    elseif(isset($_REQUEST['ISSUE']) && $_REQUEST['ISSUE'] == 'check_email') {
        echo "<span id=\"login_text\">Checking email...</span>";
    }
    else {
        echo "<span id=\"login_text\">Log in to your account</span>";
    }
    echo("<div id=\"content_index\">");

    if (isset($_REQUEST["ERROR"])) {
        echo "<div id=\"error\">" . errorMeldingen($_REQUEST["ERROR"]) . "</div>";
    } else {
        echo "<br/>";
    }

    if (loginMetSMS() == "Ja") {
        if (!isset($_POST["Login"])) {
            loginSMSStap1($toevoeging, $administraties);
        } else {
            loginSMSStap2($toevoeging, $administraties);
        }
    } elseif (loginMetSMS() == "extracode") {
        loginNaamWachtwoord2($toevoeging, $administraties);
    } elseif (isset($_REQUEST['ISSUE']) && $_REQUEST['ISSUE'] == 'forgot_password') {
        forgotPassword();
    }
    elseif(isset($_REQUEST['ISSUE']) && $_REQUEST['ISSUE'] == 'check_email') {
        checkEmail();
    }
    else {
        loginNaamWachtwoord($toevoeging, $administraties);
    }
    echo("</div>");
    echo("</div>");

    echo("</div>");
    echo("</div>");
}

function loginSMSStap1($toevoeging, $administraties)
{
    global $db;

    $next = "Next";
    $pakketTitel = pakketTitel();
    if ($pakketTitel != "OrangeField Online") {
        $next = "";
    }

    echo("<form method=\"Post\" action=\"index.php\" name=\"blogger\" id=\"blogger\">
     User id:
     <br/><input class=\"loginRegel\" type=\"text\" name=\"UserName$toevoeging\" value=\"\">");

    if (count($administraties) == 1) {
        foreach ($administraties as $key => $value) {
            echo("<input class=\"loginRegel\" type=\"hidden\" name=\"Database$toevoeging\" value=\"$key\">");
            setCompanyDatabaseFront($key);
        }
    } else {
        echo("<br/>Database:
      <br/><input type=\"password\" class=\"loginRegel\" name=\"Database$toevoeging\" value=\"\" />");
    }
    echo("<br/><input class=\"loginButton\" type=\"submit\" name=\"Login\" value=\"$next\">
    </form>");
}

function sendNewPass($lId)
{
    global $pdo;

    $nieuweCode = "";
    $query = $pdo->prepare('SELECT personeel.*, login.ID as LoginId FROM login, personeel WHERE login.ID = :loginid AND NOT (login.SUSPEND = "Ja") AND login.PERSONEELSLID = personeel.ID;');
    $query->bindValue('loginid', $lId);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        //Maak nieuwe code
        $nieuweCode = nieuweCodeGen();
        $query = $pdo->prepare('update login set PASSWORD = :password WHERE ID = :id;');
        $query->bindValue('password', $nieuweCode);
        $query->bindValue('id', $data["LoginId"]);
        $query->execute();

        //SMS deze code
        $SMSUsername = SMSUsername();
        $SMSPassword = SMSPassword();
        $mob = $data["MOB"];

        if ($mob != "") {
            $mob = str_replace(" ", "", $mob);
            $mob = str_replace("-", "", $mob);
            $mob = str_replace("(", "", $mob);
            $mob = str_replace(")", "", $mob);
            $mob = str_replace("+", "00", $mob);
            $nieuweCode = urlencode($nieuweCode);

            // Afzender moet een ander nummer zijn dan de ontvanger (bij voorkeur een +44 nummer)! Anders kan het geweigerd worden (bv in België)
            // Ik weet alleen niet of een niet-bestaand nummer altijd geaccepteerd wordt.
            // Onderstaande werkt in ieder geval wel voor België (waar zelfde afzender als ontvanger dus niet geaccepteerd wordt)
            if (substr($mob * 1, 0, 2) == "32") {
                $from = "0031610000000";
                //$from = "0031614251453";
            } else {
                $from = "0031610000000";
                //$from = $mob; // want voor andere landen werkt het ook niet...
            }

            $url = "http://sms.digitaalkantoor.nl/api_send_sms_get.php?user=$SMSUsername&pass=$SMSPassword&recnr=$mob&sendernr=$from&msg=Your%20one%20time%20login%20token%20$nieuweCode";
            //echo $url;
            @fopen($url, 'rb');
            /*$cred = sprintf('Authorization: Basic %s', base64_encode($SMSUsername.":".$SMSPassword) );
            $params = array('http' => array(
                'method' => 'POST',
                'header' => $cred,
                'content' => $output
                 )); //Basic auth
            $ctx = stream_context_create($params);
            $fp = fopen($url, 'rb', false, $ctx);
            $response = stream_get_contents($fp);
            */
        } else {
            //echo("Your mobile number is not valid.");
        }
    }

    return $nieuweCode;
}

function loginSMSStap2($toevoeging, $administratiesm, $location = ".")
{

    global $pdo;
    $database = $_POST["Database$toevoeging"];
    setCompanyDatabaseFront($database);

    require_once("dbConnecti.php");

    mayAccessSystem();

    $USERNAAM = $_POST["UserName$toevoeging"];
    if (strlen($USERNAAM) > 30) {
        $USERNAAM = "";
    }
    $PASSWOORD = "";

    $USERNAAM = checkInvoer($USERNAAM);

    $data = false;
    if ($pdo !== null) {
        $query = $pdo->prepare('SELECT personeel.*, login.ID as LoginId FROM login, personeel WHERE login.USERNAME = :username AND NOT (login.SUSPEND = "Ja") AND login.PERSONEELSLID = personeel.ID;');
        $query->bindValue('username', $USERNAAM);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
    }
    if ($data !== false) {
        $nieuweCode = sendNewPass($data["LoginId"]);
        addTokenRequest($_SERVER["REMOTE_ADDR"], $USERNAAM);

        $next = "Login";
        $pakketTitel = pakketTitel();
        if ($pakketTitel != "OrangeField Online") {
            $next = "";
        }

        echo("<form method=\"Post\" action=\"$location/admin/login.php\" name=\"blogger\" id=\"blogger\">
       	Code:</br>
    <input class=\"loginRegel\" type=\"text\" name=\"Password$toevoeging\" value=\"\">
    <input type=\"hidden\" name=\"UserName$toevoeging\" value=\"$USERNAAM\">
    <input type=\"hidden\" name=\"Database$toevoeging\" value=\"$database\">
    </br>
    <input class=\"loginButton\" type=\"submit\" name=\"Login\" value=\"$next\"></td></tr>
    </form>");

    } else {
        $next = "";
        //Toon geen foutmelding
        echo("<form method=\"Post\" action=\"$location/admin/login.php\" name=\"blogger\" id=\"blogger\">
       	Code :</br>
    <input class=\"loginRegel\" type=\"text\" name=\"Password$toevoeging\" value=\"\">
    <input type=\"hidden\" name=\"UserName$toevoeging\" value=\"$USERNAAM\">
    <input type=\"hidden\" name=\"Database$toevoeging\" value=\"$database\">
    </br>
    <input class=\"loginButton\" type=\"submit\" name=\"Login\" value=\"$next\"></td></tr>
    </form>");
    }
}

function loginNaamWachtwoord($toevoeging, $administraties, $location = ".")
{
    global $db;

    $gebruikersnaam = "Gebruikersnaam";
    $wachtwoord = "Wachtwoord";
    $database = "Database";

    if (taal($location) == "EN") {
        $gebruikersnaam = "User ID";
        $wachtwoord = "Password";
        $database = "Database";
    }

    $userValue = "";
    $passValue = "";
    $dbValue = "";
    $rememberValue = "";
    if (isset($_COOKIE['REMEMBER'])) {
        if ($_COOKIE['REMEMBER'] != "") {
            $tmpRem = explode(";;;", $_COOKIE['REMEMBER']);
            $userValue = $tmpRem[0];
            $passValue = $tmpRem[1];
            $dbValue = $tmpRem[2];
            $rememberValue = "checked=\"true\"";
        }
    }

    $next = "Login";
    $pakketTitel = pakketTitel($location);
    if ($pakketTitel != "OrangeField Online") {
        $next = "";
    }

    echo("<form method=\"Post\" action=\"$location/admin/login.php\" name=\"blogger\" id=\"blogger\">
    <br/>$gebruikersnaam
    <br/><input class=\"loginRegel\" type=\"text\" name=\"UserName$toevoeging\" value=\"$userValue\">
    <br/>
    <br/>$wachtwoord
    <br/><input class=\"loginRegel\" type=\"password\" name=\"Password$toevoeging\" value=\"$passValue\">");

    if (count($administraties) == 1) {
        foreach ($administraties as $key => $value) {
            echo("<input class=\"loginRegel\" type=\"hidden\" name=\"Database$toevoeging\" value=\"$key\">");
            setCompanyDatabaseFront($key);
        }
    } else {
        echo("<br/>$database
        <br/><input type=\"password\" class=\"loginRegel\" name=\"Database$toevoeging\" value=\"$dbValue\" />");
        /*
      <select class=\"loginRegel\" name=\"Database$toevoeging\">");
      if($dbValue != "" && $dbValue != null) {
        echo("<option value=\"$dbValue\">".$administraties[$dbValue]["bedrijf"]."</option>");
      }

      foreach($administraties as $key => $value) { echo("<option value=\"$key\">".$value["bedrijf"]."</option>"); }
      echo("</select>
        */
    }

    echo "<br /><span><a href='index.php?ISSUE=forgot_password' style='color: white;'>Forgot password?</a></span>";
    echo("<br/><br/>
    <input type=\"checkbox\" name=\"rememberMe\" $rememberValue /> Remember me
    <div id=\"loginButtonDiv\">
    <input class=\"loginButton\" type=\"submit\" value=\"$next\">
    </div>
    </form>");
}

function loginNaamWachtwoord2($toevoeging, $administraties, $location = ".")
{
    global $db;

    $gebruikersnaam = "Gebruikersnaam";
    $wachtwoord = "Wachtwoord";
    $database = "Database";

    if (taal() == "EN") {
        $gebruikersnaam = "User id";
        $wachtwoord = "Password";
        $database = "Database";
    }

    $userValue = "";
    $passValue = "";
    $dbValue = "";
    $rememberValue = "";
    if (isset($_COOKIE['REMEMBER'])) {
        if ($_COOKIE['REMEMBER'] != "") {
            $tmpRem = explode(";;;", $_COOKIE['REMEMBER']);
            $userValue = $tmpRem[0];
            $passValue = $tmpRem[1];
            $dbValue = $tmpRem[2];
            $rememberValue = "checked=\"true\"";
        }
    }

    $next = "Login";
    $pakketTitel = pakketTitel();
    if ($pakketTitel != "OrangeField Online") {
        $next = "";
    }

    echo("<form method=\"Post\" action=\"$location/admin/login.php\" name=\"blogger\" id=\"blogger\">
    <table>
      <tr><td height=\"12\"></td></tr>
      <tr><td>$gebruikersnaam:</td></tr>
      <tr><td><input class=\"loginRegel\" type=\"text\" name=\"UserName$toevoeging\" value=\"$userValue\" /></td></tr>
      <tr><td>$wachtwoord:</td></tr>
      <tr><td><input class=\"loginRegel\" type=\"password\" name=\"Password$toevoeging\" value=\"$passValue\" /></td></tr>");

    if (count($administraties) == 1) {
        foreach ($administraties as $key => $value) {
            echo("<input class=\"loginRegel\" type=\"hidden\" name=\"Database$toevoeging\" value=\"$key\" />");
            setCompanyDatabaseFront($key);
        }
    } else {
        echo("<tr><td>$database:</td></tr>
      <tr><td><input type=\"password\" class=\"loginRegel\" name=\"Database$toevoeging\" value=\"$dbValue\" /></td></tr>");
    }
    echo("<tr>
      <td>
      <div id=\"rememberMe\">
      <input type=\"checkbox\" name=\"rememberMe\" $rememberValue /> Remember me
      </div>
      <div id=\"loginButtonDiv\">
      <input class=\"loginButton\" type=\"submit\" value=\"$next\">
      </div>
      </td></tr>
      <tr><td height=25></td></tr>
    </table>
    </form>");
}

function writeFooter()
{

    echo "\n</body>";
    echo "\n</html>";
}

function forgotPassword()
{
    echo "<form method='post' action='index.php?ISSUE=check_email'>";
    echo "<label>E-mail / User ID: </label><br /><br />";
    echo "<input type='text' name='user_tag'>";
    echo "<div id=\"loginButtonDiv\">";
    echo "<input class=\"loginButton\" type=\"submit\" value=''>";
    echo "</div>";
    echo "</form>";
}

function checkEmail() {
    require_once("dbConnecti.php");
    global $db;

    if(isset($_REQUEST['user_tag'])) {
        $query = "SELECT CONCAT(personeel.VOORNAAM, ' ', personeel.TUSSENVOEGSEL, ' ', personeel.ACHTERNAAM) AS NAME, login.USERNAME, personeel.EMAIL, login.PASSWORD FROM login 
                    JOIN personeel ON personeel.ID = login.PERSONEELSLID
                    WHERE login.USERNAME = '".$_REQUEST['user_tag']."'
                    OR personeel.EMAIL  = '".$_REQUEST['user_tag']."' LIMIT 1";

        $stmt = $db->prepare($query);
        $stmt->execute();
        $stmt->bind_result($name, $userName, $email, $password);
        while($stmt->fetch()) {
            $message = "Hi --NAAM--,
            <br/>
            <br/>We're sorry to hear you've lost your password.
            <br/>
            <br/>Username: --USERNAME--
            <br/>Password: --PASSWORD--
            <br/>Don't lose it this time!
            <br/>
            <br/>Best regards,
            <br/>
            <br/>Normec Group";

            $message = str_replace("--USERNAME--", trim($userName), $message);
            $message = str_replace("--PASSWORD--", trim($password), $message);
            $message = str_replace("--NAAM--", trim($name), $message);

            sendMail('info@normec-cm.com', $email, "Login Recovery", $message);

            echo $message;
            exit;
        }
    }

    echo 'failed';
    exit;
}

function sendMail($Van, $aan, $onderwerp, $inhoud)
{
    require("./phpMailer/class.phpmailer.php");
    //Versturen met SMTP
    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP();             // telling the class to use SMTP

    try {
        $mail->Host       = "normec-cm.com"; // $dataInstellingen["SMTP"];           // SMTP server
        $mail->SMTPDebug  = 0;                // enables SMTP debug information (for testing)
        $mail->SMTPAuth   = true;             // enable SMTP authentication
        $mail->Host       = "normec-cm.com"; // $dataInstellingen["SMTP"];           // sets the SMTP server
        $mail->Port       = 825;              // set the SMTP port for the GMAIL server
        $mail->Username   = "normeccm";     // $dataInstellingen["GEBRUIKERSNAAM"]; // SMTP account username
        $mail->Password   = "DI5wKDtx2NuVY";     // $dataInstellingen["WACHTWOORD"];     // SMTP account password
        $mail->AddReplyTo($Van, $Van);
        $mail->AddAddress($aan, $aan);
        //$mail->AddBCC("support@digitaalkantoor.nl", "support@digitaalkantoor.nl");
//      $mail->AddBCC("bob.salm@minded.nl", "bob.salm@minded.nl");
        $mail->SetFrom($Van, $Van);
        $mail->AddReplyTo($Van, $Van);
        $mail->Subject    = $onderwerp;
        //$mail->AltBody    = $inhoud; // optional - MsgHTML will create an alternate automatically
        $mail->MsgHTML($inhoud);

        if($mail->Send())
        {
            $send = true;
        }
    } catch (phpmailerException $e) {
        echo $e->errorMessage();
        $send = false;
        //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage();
        $send = false;
        //Boring error messages from anything else!
    }
    return $send;
}
