<?php
sercurityCheck();

function voorloopTekens($waarde, $veldgrootte, $teken)
{
    $overigeGrootte = $veldgrootte - strlen($waarde);

    for ($i = 1; $i <= $overigeGrootte; $i++) {
        $waarde = $teken . $waarde;
    }
    return $waarde;
}

function naloopTekens($waarde, $veldgrootte, $teken)
{
    $overigeGrootte = $veldgrootte - strlen($waarde);

    for ($i = 1; $i <= $overigeGrootte; $i++) {
        $waarde = $waarde . $teken;
    }
    return $waarde;
}

function parseToXMLValue($string, $length = 0)
{
    $string = basisHtml2UTF($string);
    $string = str_replace("&", "&amp;", $string);
    $string = trim($string);

    if ($length > 0) {
        $string = substr($string, 0, $length);
    }

    return $string;
}

function exportToExcel($html)
{
    global $db;

    $toonExcel = true;
    $doc = array();

    if ($toonExcel) {
        if (true) {
            require_once "Spreadsheet/Excel/Writer.php";

            $bestandlocatie = bestandLocatie();
            $file = "export.xls";//$bestandlocatie."/temporary/export.xls";
            //
            $xls = new Spreadsheet_Excel_Writer();

            $xls->send($file);
            $sheet =& $xls->addWorksheet("Export");
            $sheet->_xls_strmax = 255; // default string is 255 tekens, lange teksten worden dan dus afgebroken

            $formatBold =& $xls->addFormat();
            $formatBold->setBold();

            $colorHeader =& $xls->addFormat();
            $colorHeader->setFgColor('orange');

            $boldPlusUnderline =& $xls->addFormat();
            $boldPlusUnderline->setBottom(1);
            $boldPlusUnderline->setBottomColor('black');
            $boldPlusUnderline->setBold();

            $boldPlusColor =& $xls->addFormat();
            $boldPlusColor->setBold();
            $boldPlusColor->setFgColor('orange');

            $colorEvenRow =& $xls->addFormat();
            $colorEvenRow->setFgColor(22);

            $formatOmschrijving =& $xls->addFormat();
        } else {
            require_once("./excel-2/class-excel-xml.inc.php");
        }
    }

    $html = str_replace("<TABLE", "<table", $html);
    $html = str_replace("</TABLE", "</table", $html);
    $html = str_replace("<thead>", "", $html);
    $html = str_replace("</thead>", "", $html);
    $html = str_replace("<THEAD>", "", $html);
    $html = str_replace("</THEAD>", "", $html);

    $html = str_replace("<TH", "<th", $html);
    $html = str_replace("<TR", "<tr", $html);
    $html = str_replace("<TD", "<td", $html);
    $html = str_replace("TH>", "th>", $html);
    $html = str_replace("TR>", "tr>", $html);
    $html = str_replace("TD>", "td>", $html);
    $html = preg_replace("/<th.*?>/i", "<th>", $html);
    $html = preg_replace("/<tr.*?>/i", "<tr>", $html);
    $html = preg_replace("/<td.*?>/i", "<td>", $html);
    $html = str_replace("<th>", "<td>", $html);
    $html = str_replace("</th>", "</td>", $html);
    $html = str_replace("  ", " ", $html);//Dubbele spaties eruit
    $html = str_replace("  ", " ", $html);
    $html = str_replace("  ", " ", $html);
    $html = str_replace("  ", " ", $html);
    $html = str_replace("  ", " ", $html);

    $html = str_replace("&nbsp;", " ", $html);
    $html = str_replace("&euro; ", "", $html);
    $html = str_replace("&#128; ", "", $html);
    $html = str_replace("� ", "", $html);
    $html = str_replace("&euro;", "", $html);
    $html = str_replace("&#128;", "", $html);
    $html = str_replace("�", "", $html);

//    $html = str_replace("<table", "XTABLEX", $html); //Haal alle tags eruit, behalve de table tags.
    $html = preg_replace("/<table/", "XTABLEX", $html);
    $html = str_replace("</table>", "XSLASHTABLEX", $html);
    $html = str_replace("<td>", "XTDX", $html);
    $html = str_replace("</td>", "XSLASHTDX", $html);
    $html = str_replace("<tr>", "XTRX", $html);
    $html = str_replace("</tr>", "XSLASHTRX", $html);

    $html = strip_tags($html);

    $html = str_replace("'", "&apos;", $html);
    $html = str_replace("XTABLEX", "<table", $html);
    $html = str_replace("XSLASHTABLEX", "</table>", $html);
    $html = str_replace("XTDX", "<td>", $html);
    $html = str_replace("XSLASHTDX", "</td>", $html);
    $html = str_replace("XTRX", "<tr>", $html);
    $html = str_replace("XSLASHTRX", "</tr>", $html);
    $html = str_replace("</tr><tr>", "</tr>\n<tr>", $html);
    $html = str_replace("&gt;", ">", $html);
    $html = str_replace("&lt;", "<", $html);
    $html = str_replace("�", "�", $html);
//    dpm($html, true);

    $html = str_replace("&Uuml;", "Ü", $html);

    if (!$toonExcel) {
        echo "<h1>stap 1</h1>";
        echo $html;
    }

    preg_match_all("/<table name=\"exportexcel\".*?>.*?<\/[\s]*table>/s", $html, $matches,
        PREG_PATTERN_ORDER); //Kan niet teveel data aan..

    if (!$toonExcel) {
        echo "<h1>stap 2</h1>";
        dpm($matches);
    }

    foreach ($matches as &$tabelToExport) {
        if (!$toonExcel) {
            echo "<h1>stap 2b</h1>";
            dpm(implode("", $tabelToExport));
        }

        preg_match_all("/<tr.*?>.*?<\/tr>/s", implode("", $tabelToExport), $rows, PREG_PATTERN_ORDER);

        if (!$toonExcel) {
            echo "<h1>stap 3</h1>";
            dpm($rows);
        }

        foreach ($rows as &$row) {
            $y = 0;
            foreach ($row as &$roww) {
                preg_match_all("/<td.*?>.*?<\/td>/s", $roww, $cells, PREG_PATTERN_ORDER);

                if (!$toonExcel) {
                    echo "<h1>stap 4</h1>";
                    dpm($roww);
                }

                foreach ($cells as &$cel) {
                    if (!$toonExcel) {
                        echo "<h1>stap 5</h1>";
                        dpm($cel);
                    }
                    $x = 0;
                    unset($orangeRow);
                    foreach ($cel as &$sel) {
                        if (!$toonExcel) {
                            echo "<h1>stap 6</h1>";
                            dpm($sel);
                        }
                        $sel = str_replace("<td>", "", $sel);
                        $sel = str_replace("</td>", "", $sel);
                        $sel = str_replace("&nbsp;", " ", $sel);
                        $sel = str_replace("&#128;", "�", $sel);
                        $sel = str_replace("&#9650;", "", $sel);
                        $sel = str_replace("&amp;", "", $sel);
                        $sel = trim(html_entity_decode($sel));
                        $sel = htmlentities($sel);

                        //if($sel != "" && $sel != " ")
                        //{
                        $doc[$y][$x] = $sel;
                        if ($toonExcel) {
                            if (true) {
                                if(isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "entity_favorites") {
                                    if ($y == 0) {
                                        $sheet->write($y, $x, $sel, $boldPlusUnderline);
                                    }
                                    elseif(isset($orangeRow)) {
                                        $sheet->write($y, $x, $sel, $boldPlusColor);
                                    }
                                    elseif(trim(strtolower($sel)) == "total:") {
                                        $orangeRow = "";
                                        $sheet->write($y, $x, $sel, $boldPlusColor);
                                    }
                                    elseif($y % 2 != 0) {
                                        $sheet->write($y, $x, $sel, $colorEvenRow);
                                    }
                                    else {
                                        $sheet->write($y, $x, $sel, $formatOmschrijving);
                                    }
                                }
                                else {
                                    if ($y == 0) {
                                        $sheet->write($y, $x, $sel, $boldPlusColor);
                                    }
                                    elseif($y % 2 == 0) {
                                        $sheet->write($y, $x, $sel, $colorEvenRow);
                                    }
                                    else {
                                        $sheet->write($y, $x, $sel, $formatOmschrijving);
                                    }
                                }

                            }
                        } else {
                            echo "stap 7: " . $sel;
                        }
                        //}
                        $x++;
                    }
                }

                $y++;
                if (!$toonExcel) {
                    echo "<br/>";
                }
            }
        }
    }

    if ($toonExcel) {
        if (true) {
            $xls->close();
        } else {
            // generate excel file
            $xls = new Excel_XML;
            $xls->addArray($doc);
            $xls->generateXML("export");
        }
    } else {
        echo "<h1>Inhoud doc</h1>";
        dpm($doc);
        echo "<h1>Einde</h1>";
    }
    exit;
}

