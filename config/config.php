<?php

function config($type)
{
    $config = [
        "config_sms" => "Nee", //<!-- opties: Nee, Ja, extracode, CONTROLID -->
        "config_taal" => "EN",
        "config_title" => "Normec Group",
        "environment" => "Normec Group",
        "config_idcontrol_sharedsecret" => "Minded",
        "config_idcontrol_ip" => "oft-vp-idc1.p",
        "config_idcontrol_port" => "1812",
        "config_host_beheer" => "oft-p-db1.p",
        "config_user_beheer" => "dkbeheer",
        "config_pass_beheer" => "dr7chacA",
        "config_db_beheer" => "dkbeheer",
        "config_debug" => "true",
        "config_loggin_location" => "../logfile_dk.log",
        "config_logging" => "strong", // <!-- normal, strong -->
        "config_footer_login" => "",
        "config_firstPageAdmin" => "false",
        "config_ssl" => "false",
        "config_smsUser" => "orange",
        "config_smsPass" => "orange"
    ];
    if (isset($config[$type])) {
        return $config[$type];
    }
    return false;
}
