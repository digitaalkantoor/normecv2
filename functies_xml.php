<?php
function config_host_beheer($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_host_beheer");
}

function config_db_beheer($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_db_beheer");
}

function config_user_beheer($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_user_beheer");
}

function config_pass_beheer($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_pass_beheer");
}

function TypeLogin($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_sms");
}

function IPIDControlPort($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_idcontrol_port");
}

function IDControlSharedSecret($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_idcontrol_sharedsecret");
}

function IDControlIP($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_idcontrol_ip");
}

function taal($location = ".")
{
  global $config;
  
  if(isset($config["LANGUAGE"]) && $config["LANGUAGE"] != "")
  {
    $taal = $config["LANGUAGE"];
  } else {
    if(function_exists("getMySelf") && function_exists("getUserTaal")) {
      $userid = getMySelf();
      $taal   = getUserTaal($userid);
    } else {
      require_once("$location/config/config.php");
      $taal = config("config_taal");
    }

    $config["LANGUAGE"] = $taal;
  }

  if(trim($taal) == "") {
     $taal = "NL";
  }
  $taal = trim($taal);

  return $taal;
}

function config_smsUser($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_smsUser");
}

function config_smsPass($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_smsPass");
}

function loginMetSMS($location = ".")
{
  require_once("$location/config/config.php");
  $return = config("config_sms");

  return $return;
}

function pakketTitel($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_title");
}

function FooterLogin($location = ".")
{
  require_once("$location/config/config.php");
   return config("config_footer_login");
}

function config_debug($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_debug");
}

function config_logging($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_loggin_location");
}

function config_loggin_location($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_loggin_location");
}

function config_firstPageAdmin($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_firstPageAdmin");
}

function config_ssl($location = ".")
{
  require_once("$location/config/config.php");
  return config("config_ssl");
}

function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();
    
    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }
    
    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

/*function TypeLogin($location = ".")
{
  $return                   = "Nee";
  $xml                      = file_get_contents($location."/config/config.xml");
  $parser                   = new XMLParser($xml);
  $parser->Parse();

  if($parser->document->sms[0]->tagData)
  {
    $return = $parser->document->sms[0]->tagData;
  }

  return $return;
}

function IPIDControlPort($location = ".")
{
  $return                   = "8112";
  $xml                      = file_get_contents($location."/config/config.xml");
  $parser                   = new XMLParser($xml);
  $parser->Parse();

  if($parser->document->idcontrol_port[0]->tagData)
  {
    $return = $parser->document->idcontrol_port[0]->tagData;
  }

  return $return;
}

function IDControlSharedSecret($location = ".")
{
  $return                   = "geheim";
  $xml                      = file_get_contents($location."/config/config.xml");
  $parser                   = new XMLParser($xml);

  $parser->Parse();
  if($parser->document->idcontrol_sharedsecret[0]->tagData)
  {
    $return = $parser->document->idcontrol_sharedsecret[0]->tagData;
  }

  return $return;
}

function IDControlIP($location = ".")
{
  $return                   = "127.0.0.1";
  $xml                      = file_get_contents($location."/config/config.xml");
  $parser                   = new XMLParser($xml);

  $parser->Parse();
  if($parser->document->idcontrol_ip[0]->tagData)
  {
    $return = $parser->document->idcontrol_ip[0]->tagData;
  }

  return $return;
}

function taal($location = ".")
{
   $return = "EN";

  //Get the XML document loaded into a variable
  $xml = file_get_contents($location.'/config/config.xml');
  $parser = new XMLParser($xml);
  $parser->Parse();

  if($parser->document->taal[0]->tagData)
  {
    $return = $parser->document->taal[0]->tagData;
  }

  return $return;
}

function loginMetSMS($location = ".")
{
   $return = false;

  //Get XML String
  //$xmlstr = file_get_contents($location.'/config/config.xml');

  //Generate XML
  //$xml = new SimpleXMLElement($xmlstr);

  //Get element
  //$sms = $xml->sms;
  
    $xml = file_get_contents($location.'/config/config.xml');
  //Set up the parser object
  $parser = new XMLParser($xml);
  
  //Work the magic...
  $parser->Parse();

  $sms = $parser->document->sms[0]->tagData;

  if($sms == "Ja")
  {
     $return = true;
  }

  return $return;
}

function pakketTitel($location = ".")
{
   $return = false;

   $xml = file_get_contents($location.'/config/config.xml');
   //Set up the parser object
   $parser = new XMLParser($xml);

   //Work the magic...
   $parser->Parse();

   $titel = $parser->document->titel[0]->tagData;

   return $titel;
}

function FooterLogin($location = ".")
{
   $return = false;

   $xml = file_get_contents($location.'/config/config.xml');
   //Set up the parser object
   $parser = new XMLParser($xml);

   //Work the magic...
   $parser->Parse();

   $titel = $parser->document->footer_login[0]->tagData;

   return $titel;
}

function config_debug($location = ".")
{
  $return                   = "false";
  $xml                      = file_get_contents($location."/config/config.xml");
  $parser                   = new XMLParser($xml);
  $parser->Parse();

  if($parser->document->debug[0]->tagData)
  {
    $return = $parser->document->debug[0]->tagData;
  }

  return $return;
}

function config_logging($location = ".")
{
  $return                   = "false";
  $xml                      = file_get_contents($location."/config/config.xml");
  $parser                   = new XMLParser($xml);
  $parser->Parse();

  if($parser->document->logging[0]->tagData)
  {
    $return = $parser->document->logging[0]->tagData;
  }

  return $return;
}

function config_loggin_location($location = ".")
{
  $return                   = "false";
  $xml                      = file_get_contents($location."/config/config.xml");
  $parser                   = new XMLParser($xml);
  $parser->Parse();

  if($parser->document->loggin_location[0]->tagData)
  {
    $return = $parser->document->loggin_location[0]->tagData;
  }

  return $return;
}
*/

?>