<?php
define( 'ALLOWED_INCLUDE_FILETYPES_REGEX', '/\.(php)$/', true );

require_once("functies_algemeen.php");
require_once("xmlparser.php");
require_once("functies_functies.php");

sercurityCheck();

require_once "dbConnecti.php";
require_once("functies_bestanden.php");
require_once("site.php");
require_once("functies_xml.php");
require_once("functies_export.php");
require_once("functies_notificaties.php");
require_once("functies_updates.php");

$path = "./components/";
$count = includeFolder( $path, '/^functies_/' );

if(isset($_REQUEST["APP_ACTIVE"])) {
  //Apps
  include_all_files("./apps/", false);
  //echo "<Br/>done (apps)";
}

function incluse_all_files( $path, $extraCheck ) {
	return include_all_files( $path, $extraCheck );
}

function include_all_files( $path, $extraCheck ) {
	if( $extraCheck ) {
		return includeFolder( $path, null, true );	
	} else {
		return includeFolder( $path, '/^functies_/', true );
	}
}

function includeFolder( $path, $filter_regex = null, $recursive = false ) {
	pathNormalise( $path, true );
	$count = 0;
	if( is_dir( $path ) ) {
		if( $dirhandle = opendir( $path ) ) {
			while( ( $file = readdir( $dirhandle ) ) !== false ) {
				if( !is_file( $file ) && $recursive && $file != '.' && $file != '..' ) {
					$count += includeFolder( $path.$file, $filter, $recursive );
				}
				if( preg_match( ALLOWED_INCLUDE_FILETYPES_REGEX, $file ) ) {
					if( is_null( $filter_regex ) ) {
						require_once $path.$file;
						$count++;
					} elseif( preg_match( $filter_regex, $file ) ) {
						require_once $path.$file;
						$count++;
					}
				}
			}
			closedir( $dirhandle );
		}
		return $count;
	}
	throw new Exception( "Path [$path] is not a valid folder." );
}

function pathNormalise( &$path, $postfix_slash = false ) {
	str_replace( '\\', '/', $path );
	if( $postfix_slash ) {
		$path = ( substr( $path, -1 ) == '/' ? $path : $path. '/' );
	}
}


