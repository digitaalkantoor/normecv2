<?php

$timeStart = microtime(true);
header("Content-Type: text/html; charset=UTF-8");
date_default_timezone_set('Europe/Amsterdam');
ini_set("default_charset", "UTF-8");

// Change the session timeout value to 30 minutes  // 8*60*60 = 8 hours
//ini_set('session.gc_maxlifetime', 30*60*2); //1 hour
// This is now set in the Custom TimeOut in securityDoubleCheck()

//ini_set("display_errors","On");
//ini_set("memory_limit","1024M");
//error_reporting (E_ALL);

//Log alle requests
/*
$REQ = "";
foreach($_REQUEST As $key => $value) {
  $REQ .= "\n$key";
}
if($REQ != "") {
    $file = "./bestanden/requests.txt";
    if (!$file_handle = @fopen($file,"a+")) { echo "Cannot open file"; }
    if (!fwrite($file_handle, "$REQ")) { echo "Cannot write to file"; }
    @fclose($file_handle);
}
*/

if (isset($_REQUEST["PDF"])) {
    require_once("functies_functies.php");

    headpdf();
} elseif (isset($_REQUEST["EXPORTEXCEL"])) {
    require_once("functies_functies.php");

    headExcel();

}
include "includes.php";
sercurityDoubleCheck();

if (false) {
    set_error_handler('dk_error_handler');
}

$loginid = getLoginID();
$site = getSite();

if ($loginid != 0 && $loginid != "" && $loginid != "0") {
    //Check of login bekend is, ander alsnog break.
    $query = $pdo->prepare('SELECT * FROM login WHERE ID = :id limit 1;');
    $query->bindValue('id', $loginid);
    $query->execute();
    $aantallogin = $query->rowCount();

    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($aantallogin >= 1) {
        if ($data["SUSPEND"] == "Ja") {
            gaNaarLogin("6");
        } else {
            $query = $pdo->prepare('UPDATE personeel SET LAATSTINGELOGT = NOW() WHERE ID = :id;');
            $query->bindValue('id', $data["PERSONEELSLID"]);
            $query->execute();

            $config = getConfiguratie($data["PERSONEELSLID"], $data["BEDRIJFSID"]);
            site($site, ps($loginid, "nr"));
        }
    } else {
        gaNaarLogin("1");
    }
} elseif ($site == "loginError" || $site == "loginErrorEngels") {
    site(ps($site, ""), ps($loginid, "nr"));
} else {
    gaNaarLogin("1");
}

//Leegmaken van de variabelen
if (!isset($_SESSION)) {
    session_start();
}
$_SESSION['tmp_LID'] = null;
$_SESSION['tmp_PLID'] = null;
$_SESSION['tmp_MYSELF'] = null;
$_SESSION['tmp_BEP'] = null;
$_SESSION['tmp_BID'] = null;
$_SESSION['tmp_KID'] = null;
$_SESSION['tmp_UID'] = null;
$_SESSION['tmp_ISBESTAND'] = null;

db_close($db);

//if(!isset($_REQUEST["EXPORTEXCEL"])) {
//  echo "\n<!-- delta time: ".(microtime(true) - $timeStart)." -->";
//}
