<?php
  date_default_timezone_set('Europe/Amsterdam');

  include("functies_front.php");
  require_once("toegang.php");

  if(config_firstPageAdmin(".") == "true")
  {
    rd("admin.php");
  } 
  
  checkSSL();
  homepage();
?>