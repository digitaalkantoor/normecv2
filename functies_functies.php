<?php
require_once "functies_security.php";
sercurityCheck();

function headAlgemeen()
{
    global $db;

    $pakketTitel = pakketTitel();

    echo("<html>
<head>
<title>$pakketTitel - " . getCurrentBedrijfsNaam() . "</title>
<meta http-equiv=\"author\" content=\"Bob Salm @ www.minded.nl\" />
<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />
<meta http-equiv=\"PRAGMA\" content=\"NO-CACHE\" />
<meta name=\"category\" content=\"$pakketTitel\" />
<meta name=\"subject\" content=\"$pakketTitel\" />
<meta name=\"robots\" content=\"none\" />");

    $taal = taal();
    if ($taal == "EN") {
        echo "\n<meta name=\"Content-Language\" content=\"en\" />";
        echo "\n<meta http-equiv=\"description\" content=\"Your online bookkeeping solution!\" />";
    } else {
        echo "\n<meta name=\"Content-Language\" content=\"" . strtolower($taal) . "\" />";
        echo "\n<meta http-equiv=\"description\" content=\"De online oplossing voor al uw administratie!\" />";
    }

    printFaviconLinks();

    echo("\n<script src=\"./scripts/global_scripts.js\"></script>");

    if (isset($_REQUEST["SITE"])
        && $_REQUEST["SITE"] != "digi"
        && $_REQUEST["SITE"] != "generateMailInhoud"
        && $_REQUEST["SITE"] != "checkMail") {

        //FunsionCharts
        //echo ("\r<script type=\"text/javascript\" src=\"./fusion/FusionCharts.js\"></script>");

        //JQuery
        echo "\r<link rel=\"stylesheet\" href=\"./style/jquery-ui.css\" type=\"text/css\" media=\"all\" />";
        //echo "\r<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\"></script>";
        //echo "\r<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js\"></script>";
        //echo "\r<script type=\"text/javascript\" src=\"./scripts/jquery-1.9.1.min.js\"></script>";
        if(isset($_REQUEST['SITE']) && ($_REQUEST['SITE'] == 'entity_integration_checklist' || $_REQUEST['SITE'] == 'entity_integration_checklist2')) {
            echo '<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>';
            echo '<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>';
            echo '<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"></script>';

            echo '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">';
            echo '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.3.0/css/fixedColumns.dataTables.min.css">';
        } else {
            echo "\r<script type=\"text/javascript\" src=\"./scripts/jquery-1.7.2.min.js\"></script>";
            echo "\r<script type=\"text/javascript\" src=\"./scripts/jquery-ui-1.8.20.custom.min.js\"></script>";
        }

        echo "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.1/css/all.css\" integrity=\"sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP\" crossorigin=\"anonymous\">";


        echo "\r<script type=\"text/javascript\">";
        echo "\r$(document).ready(";
        echo "\rfunction() {";
        if ($_REQUEST["SITE"] == "bi_output") {
            echo "\r$( 'input[name=\"zoekVeld\"]' ).autocomplete({ source: \"./bic/search.php\" });";
        }

        if (taal() == "EN") {
            $format = "yy-mm-dd";
        } else {
            $format = "dd-mm-yy";
        }

        echo "\r$('.date-picker').datepicker({dateFormat: '" . $format . "'});";
        echo "\r}";
        echo "\r);";
        echo "</script>";

        //get all js files
        $path = "./components/";
        if ($dir = opendir($path)) {
            while (false !== ($file = readdir($dir))) {
                if ($file[0] == ".") {
                    continue;
                } else {
                    if (is_file($path . $file) && substr($file, -3) == ".js") {
                        if (file_exists($path . $file)) {
                            echo "\n<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . $path . $file . "\"></script>";
                        }
                    }
                }
            }
            closedir($dir);
        }
    }

    if (!getPDA()) {
        echo("\n<script src=\"./scripts/ajax_search.js\"></script>
                \n<script src=\"./scripts/calendar.js\"></script>");

        //if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] != "completementsrapport" && $_REQUEST["SITE"] != "whitebook") {
        //    echo("\n<script language=\"JavaScript\" type=\"text/javascript\" src=\"./scripts/sorttable.js\"></script>");
        //}
    }

    if (isset($_REQUEST["CHART"])) {
        echo("\n<script>AC_FL_RunContent = 0;</script>
            \n<script> DetectFlashVer = 0; </script>
            \n<script src=\"./scripts/AC_RunActiveContent.js\"></script>
            \n<script>
            \n<!--
            \nvar requiredMajorVersion = 9;
            \nvar requiredMinorVersion = 0;
            \nvar requiredRevision = 45;
            \n-->
            \n</script>");
    }
}

function headcron()
{
    $pakketTitel = pakketTitel();
    $locatiedk = getLocatie();

    echo("<DOCTYPE html>
\r\n<html>
\r\n<head>
\r\n<title>$pakketTitel - " . getCurrentBedrijfsNaam() . "</title>
\r\n<meta http-equiv=\"author\" content=\"Bob Salm @ www.digitaalkantoor.nl\" />
\r\n<meta http-equiv=\"description\" content=\"De online oplossing voor al uw administratie!\" />
\r\n<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />
\r\n<meta http-equiv=\"PRAGMA\" content=\"NO-CACHE\" />
\r\n<meta name=\"category\" content=\"$pakketTitel\" />
\r\n<meta name=\"subject\" content=\"$pakketTitel\" />
\r\n<meta name=\"Content-Language\" content=\"nl\" />
\r\n<link href=\"./style/global_style.css\" type=\"text/css\" rel=\"stylesheet\">
\r\n</head>
\r\n<body>");
}

function head_start()
{
    if (!isset($_REQUEST["PDF"]) && !isset($_REQUEST["EXPORTEXCEL"])) {
        headAlgemeen();

        echo("\n<link href=\"./style/global_style.css\" type=text/css rel=stylesheet />");
        echo("\n</head>");
        echo("\n<body leftmargin=\"0\" rightmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">"); // onLoad=\"setWindowHeight();\"
    }
}

function head1()
{
    if (!isset($_REQUEST["PDF"]) && !isset($_REQUEST["EXPORTEXCEL"])) {
        headAlgemeen();

        echo("\n<link href=\"./style/global_style.css\" type=text/css rel=stylesheet />");
        echo("\n<link href=\"./style/calendar.css\" type=text/css rel=stylesheet />");
        echo("\n</head>");
        echo("\n<body>");

        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] != "setViewMsg") {
            echo html_invoerscherm();
        }
    }
}

function head_validatie($titel, $koloms1, $koloms2, $koloms3)
{
    global $db;

    headAlgemeen();

    $_REQUEST["VALIDATIEFORM"] = "Ja";

    echo("\n<link href=\"./style/global_style.css\" type=\"text/css\" rel=\"stylesheet\">
\n<link href=\"./style/calendar.css\" type=\"text/css\" rel=\"stylesheet\">
\n<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
\n</head>
\n<body>");

    if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] != "setViewMsg") {
        echo html_invoerscherm();
    }
}

function alertFoutmeldingValidatie($veld, $naam)
{
    if ($veld == "") {
        $veld = $naam;
    }

    return "\n     alert(\"Het veld `$veld` is nog leeg!\");
\n     return (false);";
//\n     theForm.$naam.focus();
}

function head2()
{
    $tabeldbstruct = "";
    if (isset($_REQUEST["SITE"])) {
        $tabeldbstruct = str_replace("bew", "", str_replace("toev", "", $_REQUEST["SITE"]));
    }
    if ($tabeldbstruct != "" && tabellenInDeDatabase($tabeldbstruct)) {
        $kolom1 = site("get$tabeldbstruct", 1);
        $kolom2 = site("get$tabeldbstruct", 2);
        $kolom3 = site("get$tabeldbstruct", 3);
        head_validatie($tabeldbstruct, $kolom1, $kolom2, $kolom3);
    } else {

        if (!isset($_REQUEST["PDF"]) && !isset($_REQUEST["EXPORTEXCEL"])) {
            headAlgemeen();

            $sessionTime = ini_get('session.gc_maxlifetime') * 1000; // Seconds * 1000 miliseconds
            //$onLoad = "onLoad=\"setTimeout('alert(\'Probably you are logged out!\')', $sessionTime );\"";
            //onLoad=\"window.top.document.getElementById('scherm".getScherm()."woord').innerHTML='&nbsp;&nbsp;&nbsp;'+document.getElementById('paginakop').innerHTML;\"";
            $onLoad = "";
            if (isset($_REQUEST["SITE"])) {
                if ($_REQUEST["SITE"] == "mededelingen") {
                    $onLoad = "openSubDiv();\"";
                } else {
                    if ($_REQUEST["SITE"] == "overzichtklant" && isset($_REQUEST["PROJECTNR"])) {
                        if (isDirectie() || isAdministratie()) {
                            $onLoad = "onLoad=\"openStatistieken(" . $_REQUEST["PROJECTNR"] . ", 'admin')\"";
                        } else {
                            $onLoad = "onLoad=\"openStatistieken(" . $_REQUEST["PROJECTNR"] . ", 'public')\"";
                        }
                    } else {
                        if ($_REQUEST["SITE"] == "crmrekenen") {
                            $onLoad = "onLoad=\"document.forms.zoekform.VRIJEINVOER.focus();\"";
                        } else {
                            if (isset($_REQUEST["SESSIE"])) {
                                if ($_REQUEST["SESSIE"] == "Alice") {
                                    $onLoad = "onload=\"start();\"";
                                }
                            }
                        }
                    }
                }
            }

            echo "\n<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300' rel='stylesheet' type='text/css'>";

            if (isset($_GET["PRINTEN"])) {
                echo("\n<link href=\"./style/global_style.css\" type=\"text/css\" rel=\"stylesheet\" onLoad=\"javascript:window.print();\">
                        \n<link href=\"./style/calendar.css\" type=\"text/css\" rel=\"stylesheet\">
                        \n</head>
                        \n<body $onLoad>");
            } else {
                if (!getPDA()) {
                    echo("\n<link href=\"./style/global_style.css\" type=\"text/css\" rel=\"stylesheet\">
                            \n<link href=\"./style/calendar.css\" type=\"text/css\" rel=\"stylesheet\">
                            \n</head>
                            \n<body $onLoad>");
                    if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] != "setViewMsg") {
                        echo html_invoerscherm();
                    }
                } else {
                    echo("\n<link href=\"./style/global_style.css\" type=\"text/css\" rel=\"stylesheet\">
                             \n<link href=\"./style/calendar.css\" type=\"text/css\" rel=\"stylesheet\">
                             \n</head>
                             \n<body>");
                }
            }
        }
    }
}

function head3()
{
    if (!isset($_REQUEST["PDF"]) && !isset($_REQUEST["EXPORTEXCEL"])) {
        headAlgemeen();

        if (isset($_GET["PRINTEN"])) {
            echo("\r\n<link href=\"../style/global_style.css\" type=\"text/css\" rel=\"stylesheet\" onLoad=\"javascript:window.print();\">
                  \r\n<link href=\"../style/calendar.css\" type=\"text/css\" rel=\"stylesheet\">
                  \r\n</head>
                  \r\n<body leftmargin=\"0\" rightmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
        } else {
            echo("\r\n<link href=\"../style/global_style.css\" type=\"text/css\" rel=\"stylesheet\">
                   \r\n<link href=\"../style/calendar.css\" type=\"text/css\" rel=\"stylesheet\">
                   \r\n</head>
                   \r\n<body leftmargin=\"0\" rightmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
            echo html_invoerscherm();
        }
    }
}

function headpdf()
{
    require_once './tcpdf/config/lang/eng.php';
    require_once './tcpdf/tcpdf.php';

    // activate Output-Buffer:
    ob_start();
}

function footerpdf()
{
    require_once './tcpdf/config/lang/eng.php';
    require_once './tcpdf/tcpdf.php';

    // Output-Buffer in variable:

    $html = ob_get_contents();

    $html = str_replace("</a>", "</x>", $html);
    $html = str_replace("<a ", "<x ", $html);
    $array = explode("<center style=\"PAGE-BREAK-AFTER: always\"></center>", $html);
    $tel = count($array);
    // delete Output-Buffer
    ob_end_clean();

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // ---------------------------------------------------------

    // set font
    $pdf->SetFont('helvetica', '', 8);

    $tel2 = 0;
    while ($tel2 < $tel) {
        $teksttmp = $array[$tel2];
        if (substr($array[$tel2], 0, 2) == "LX") {
            $pdf->AddPage('L');
            $teksttmp = substr($teksttmp, 2, strlen($teksttmp));
        } else {
            $pdf->AddPage();
        }
        $pdf->WriteHTML($teksttmp, true, false, false, false, '');
        $tel2++;
    }

    $pdf->Output('doc.pdf', 'I');
}

function headExcel()
{
    // activate Output-Buffer:
    ob_start();
}

function footerExcel()
{
    // Output-Buffer in variable:

    $html = ob_get_contents();

    dpm($html);

    // delete Output-Buffer
    ob_end_clean();

    //echo $html;
    exportToExcel($html);
}

function headChat()
{
    echo("<DOCTYPE html>
  \r\n<html>
  \r\n<head>
  \r\n<meta http-equiv=\"refresh\" content=\"10\">
  \r\n<LINK href=\"./style/global_style.css\" type=text/css rel=stylesheet>
  \r\n<LINK href=\"style/calendar.css\" type=text/css rel=stylesheet>
  \r\n</head>
  \r\n<body leftmargin=\"0\" rightmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
}

function get_rows($table)
{
    //TODO: verwijderen : wordt niet gebruikt en gevaarlijk!!

    /*
    global $db;
    $temp = db_ query("SELECT SQL_CALC_FOUND_ROWS ID FROM " . ps($table) . " LIMIT 1", $db);
    $result = db_ query("SELECT FOUND_ROWS()", $db);
    $total = db_fetch_row($result);
    return $total[0];
     
     */

    return 0;
}

function getAantal($tabel, $conditie = '')
{
    global $db;

    //return checkAantal(1, $tabel, $conditie);

    $aantalResult = 0;
    $sql = "SELECT COUNT(*) As Aantal from " . $tabel . " $conditie limit 1;";
    //echo $sql;
    $result = db_query($sql, $db);
    if ($data = db_fetch_array($result)) {
        $aantalResult = $data["Aantal"];
    }

    return $aantalResult;
}

function setLeavePage()
{
    return "document.getElementById('leave_page').innerHTML='0';";
}

function footer0()
{
    echo("\r\n</body>\r\n</html>");
}

function footer1()
{
    if (isset($_REQUEST["PDF"])) {
        //sfooterpdf();
    } else {
        if (isset($_REQUEST["EXPORTEXCEL"])) {
            footerExcel();
        } else {
            if (isset($_REQUEST["SITE"])) {
                if (check("check_op_voortijdig_paginaverlaten", "Ja", '1') == "Ja") {
                    if (
                        //substr($_REQUEST["SITE"],0,4) == "toev"
                        //|| substr($_REQUEST["SITE"],0,3) == "bew"
                        $_REQUEST["SITE"] == "toevklanten"
                        || $_REQUEST["SITE"] == "urenRegistratieTwee"
                        || $_REQUEST["SITE"] == "urenRegistratieDrie"
                        || $_REQUEST["SITE"] == "urenRegistratieVier"
                        || $_REQUEST["SITE"] == "urenRegistratieVijf"
                        || $_REQUEST["SITE"] == "urenmedewerkers"
                        || (isset($_REQUEST["TYPE"]) && $_REQUEST["TYPE"] == "SCHRIJF" && $_REQUEST["SITE"] == "mywebmail")
                    ) {
                        echo("\n<span id=\"leave_page\" style=\"display: none;\">1</span>");
                    }
                }
            }

            echo("\r\n</body>\r\n</html>");
        }
    }
}

function getSite()
{
    $site = "leeg";
    if (isset($_GET["SITE"])) {
        $site = $_GET["SITE"];
    }
    return $site;
}

function getUserID($loginid = false)
{
    if (!$loginid) {
        $loginid = getLoginID();
    }
    global $pdo;
    if (!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['tmp_UID']) || $_SESSION['tmp_UID'] == "0") {
        global $db;

        $userid = 0;

        $query = $pdo->prepare('SELECT PERSONEELSLID FROM login WHERE ID = :loginid limit 1;');
        $query->bindValue('loginid', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $userid = $data["PERSONEELSLID"] * 1;
            $_SESSION['tmp_UID'] = $userid;
        }
        return $userid;
    } else {
        return $_SESSION['tmp_UID'];
    }

}

function getLastID($tabel)
{
    global $pdo;

    /*
    $id = "";
    $result = db_query("SELECT ID FROM " . ps($tabel, "tabel") . " ORDER BY ID DESC limit 1;", $db);
    $query = $pdo->prepare('SELECT * FROM table WHERE ID=?');
    $query->bindValue('username', $USERNAME);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if($data !== false) {
    
    if ($data = db_fetch_array($result)) {$id = ($data["ID"] * 1);}

    return $id;
    */

    return $pdo->lastInsertId();

}

function getNaam($tabel, $id, $veld, $length = "1000", $leegWaarde = "N/A")
{
    global $pdo;

    $naam = $leegWaarde;


    if (!empty($tabel)) {
        $tableData = getTableData($tabel);
        if (!isset($tableData[$veld])) {
            return;
        }
    }

    if ($id != 0) {
        $query = $pdo->prepare('SELECT ' . $veld . ' FROM ' . $tabel . ' WHERE ID = :id limit 1;');
        $query->bindValue('id', $id);
        $query->execute();

        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $naam = stripslashes($data[$veld]);
        }
    }

    if (strlen($naam) >= $length) {
        $naam = substr($naam, 0, ($length - 2)) . "..";
    }

    $naam = stripslashes($naam);

    return $naam;
}

function getId($tabel, $veld, $id)
{
    global $pdo;

    if (!empty($tabel)) {
        $tableData = getTableData($tabel);
        if (!isset($tableData[$veld])) {
            return;
        }
    }

    $naam = "";
    if ($id != "") {
        $query = $pdo->prepare('SELECT ID FROM ' . ps($tabel, "tabel") . ' WHERE ' . ps($veld) . ' = :value limit 1;');
        $query->bindValue('value', $id);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $naam = $data["ID"] * 1;
        }
    }

    return lb($naam);
}

function getKlantId()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['tmp_KID']) || $_SESSION['tmp_KID'] == "0") {
        global $pdo;

        $loginid = getLoginID();

        $klantid = 0;
        $query = $pdo->prepare('SELECT PERSONEELSLID FROM login WHERE ID = :id limit 1;');
        $query->bindValue('id', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $klantid = $data["PERSONEELSLID"];
            $_SESSION['tmp_KID'] = $klantid;
        }
        return $klantid;
    } else {
        return $_SESSION['tmp_KID'];
    }
}

function getKlantIdVanOrder($bonnr, $bedrijfsid)
{
    global $pdo;

    $klantId = 0;
    $query = $pdo->prepare('SELECT KLANTID FROM verkocht WHERE BONNR = :bonnr AND BEDRIJFSID = :bedrijfsid limit 1;');
    $query->bindValue('bonnr', $bonnr);
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        $klantId = $data["KLANTID"];
    }
    return $klantId;
}

function getBeheerEigenProfiel()
{
    global $pdo;

    if (!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['tmp_BEP']) || $_SESSION['tmp_BEP'] == "0") {
        $return = "Nee";
        $query = $pdo->prepare('SELECT * FROM login where ID = :id AND KLANT = "Nee" AND NOT BEHEEREIGENPROFIEL = "Nee" limit 1;');
        $loginId = getLoginID();
        $query->bindValue('id', $loginId);
        $query->execute();
        $data_login = $query->fetch(PDO::FETCH_ASSOC);

        if ($data_login !== false) {
            $return = "Ja";
        }
        return $return;
    } else {
        return $_SESSION['tmp_BEP'];
    }
}

function getPDA()
{
    $pda = 0;
    if (!isset($_SESSION)) {
        session_start();
    }
    if (isset($_SESSION['PDA'])) {
        $pda = $_SESSION['PDA'];
    }
    return $pda * 1;
}

function getSessionIP()
{
    return lb($_SESSION['IP']);
}

function getBedrijfsID($loginid)
{
    if (!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['tmp_BID']) || $_SESSION['tmp_BID'] == "0") {
        global $pdo;
        $bedrijfsid = 1;

        $query = $pdo->prepare('SELECT BEDRIJFSID FROM login WHERE (ID = :id)');
        $query->bindValue('id', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $bedrijfsid = $data["BEDRIJFSID"];
        }
        $_SESSION['tmp_BID'] = $bedrijfsid;
        return ($bedrijfsid * 1);
    } else {
        return ($_SESSION['tmp_BID'] * 1);
    }
}

function getMyBedrijfsID()
{
    return getBedrijfsID(getLoginID());
}

/*
function getBedrijfsLogo()
{
    global $db;

    $logo = "";
    $bedrijfid = getMyBedrijfsID();
    $result = db_query("SELECT * FROM bedrijf WHERE ID = '" . ps($bedrijfid, "nr") . "' LIMIT 1;", $db);
    if ($data = db_fetch_array($result)) {
        $logo = lb($data["LOGO"]);
    }
    return $logo;
}
*/

function getCurrentBedrijfsNaam()
{
    $loginid = getLoginID();
    $bedrijfid = getBedrijfsID($loginid);
    return stripslashes(getBedrijfsnaam($bedrijfid));
}

function getMySelf()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['tmp_MYSELF']) || $_SESSION['tmp_MYSELF'] == "0") {
        global $pdo;
        $userid = 0;
        $loginid = getLoginID();
        $query = $pdo->prepare('SELECT PERSONEELSLID FROM login WHERE ID = :id LIMIT 1;');
        $query->bindValue('id', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $userid = $data["PERSONEELSLID"] * 1;
            $_SESSION['tmp_MYSELF'] = $userid;
        }
        return lb($userid);
    } else {
        return lb($_SESSION['tmp_MYSELF']);
    }
}

function getNaamRelatie($id, $length = -1)
{
    global $pdo;

    $tmpKlant = "";
    $query = $pdo->prepare('SELECT BEDRIJFSNAAM, INITIALEN, VOORNAAM, TUSSENVOEGSEL, ACHTERNAAM FROM klanten WHERE ID = :id LIMIT 1;');
    $query->bindValue('id', $id);
    $query->execute();
    $data_klant = $query->fetch(PDO::FETCH_ASSOC);

    if ($data_klant["BEDRIJFSNAAM"] != "") {
        $tmpKlant = $data_klant["BEDRIJFSNAAM"];
    } else {
        $initialen = $data_klant["INITIALEN"];
        if ($initialen == "") {
            $initialen = $data_klant["VOORNAAM"];
        }
        if ($data_klant["TUSSENVOEGSEL"] != "" || $data_klant["ACHTERNAAM"] != "") {
            if ($initialen != "") {
                $initialen = ", " . $initialen;
            }
            $tmpKlant = $data_klant["TUSSENVOEGSEL"] . " " . $data_klant["ACHTERNAAM"] . $initialen;
        }
    }
    if (str_replace(" ", "", $tmpKlant) == "") {
        $tmpKlant = "N/A";
    }

    if (isset($_REQUEST["SITE"])) {
        if ($_REQUEST["SITE"] != "facturen_pdf") {
            //Doe dit niet bij de facturen
            $tmpKlant = basisutf2html($tmpKlant);
        }
    }

    if (($length * 1) > 0) {
        if (strlen($tmpKlant) >= $length) {
            $tmpKlant = substr($tmpKlant, 0, ($length - 2)) . "..";
        }
    }

    return lb(stripslashes($tmpKlant));
}

function getTabbladLink($bedrijfsid, $userid, $tabblad)
{
    global $pdo;

    if (isAccountent()) {
        $response = "content.php?SITE=BoekhouderDashboard";
    } else {
        $response = "content.php?SITE=" . watWordStartScherm();
    }
    $query = $pdo->prepare('SELECT menuitems.* FROM tabbladen, menuitems WHERE menuitems.ID = tabbladen.FUNCTIE AND tabbladen.PERSONEELSLID = :personeelslid and tabbladen.BEDRIJFSID = :bedrijfsid and tabbladen.TABBLAD = :tabblad limit 1;');
    $query->bindValue('personeelslid', $userid);
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->bindValue('tabblad', $tabblad);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        $response = lb($data["LINK"]);
    }
    return $response;
}

/*
function getTabbladNaam($bedrijfsid, $userid, $tabblad, $verkort = 17)
{
  
    global $db;

    $response = tl("tabblad"); //"SCHERM$tabblad"
    if (getNaam("login", getLoginId(), "STYLE") == "Zonder tabbbladen") {
        $response = "Welkom " . getPersoneelsLidNaam($userid);
    }

    $resultBedrijf = db_query("SELECT menuitems.* FROM tabbladen, menuitems WHERE menuitems.ID = tabbladen.FUNCTIE AND tabbladen.PERSONEELSLID = '" . ps($userid, "nr") . "' and tabbladen.BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' and tabbladen.TABBLAD = '" . ps($tabblad, "nr") . "' limit 1;", $db);
    if ($data = db_fetch_array($resultBedrijf)) {
        //En bij EN Naam
        $response = lb(tl($data["NAAM"]));
        $response = str_replace("<BR>", " ", $response);
        $response = str_replace("<BR/>", " ", $response);
        $response = str_replace("<br>", " ", $response);
        $response = str_replace("<br/>", " ", $response);
        $response = strip_tags($response);
        $response = verkort($response, $verkort);
    }
    return $response;
   
   
}
*/
/*  
function veldTabelTonenJaNee($tabel, $kolom)
{
  
  global $db;

  $return = "Nee";
  $res = db_query("SELECT * FROM dbstructuur where TABEL = '" . ps($tabel) . "' AND KOLOM = '" . ps($kolom) . "' AND TONEN = 'Ja' LIMIT 1;", $db);
  if (db_num_rows($res) > 0) {
      $return = "Ja";
  }

  return $return;

 
}
*/
function getBedrijfsnaam($bedrijfsid)
{
    global $pdo;

    $bedrijfsnaam = "N/A";
    $query = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id limit 1;');
    $query->bindValue('id', $bedrijfsid);
    $query->execute();
    $bedrijf = $query->fetch(PDO::FETCH_ASSOC);

    if ($bedrijf !== false) {
        $bedrijfsnaam = lb($bedrijf["BEDRIJFSNAAM"]);
    }
    return $bedrijfsnaam;
}

/*
function getFactuur($fid)
{
  
    global $db;

    $facturen = "";
    $resultBedrijf = db_query("SELECT * FROM facturen WHERE ID = '" . ps($fid, "nr") . "' limit 1;", $db);
    if ($data = db_fetch_array($resultBedrijf)) {
        $facturen = "<a target=\"_blank\" href=\"content.php?SITE=facturen_pdf&ID=$fid\">" . lb($data["FACTUURNR"]) . "</a>";
    }
    return $facturen;
  
   
}
*/
/* 
function getStamNaam($stamtabel, $ID)
{
 
  global $db;

  $naam = "N/A";
  $resultBedrijf = db_query("SELECT * FROM " . ps($stamtabel) . " WHERE ID = '" . ps($ID, "nr") . "' limit 1;", $db);
  if ($bedrijf = db_fetch_array($resultBedrijf)) {
      $naam = lb($bedrijf["NAAM"]);
  }
  return $naam;

  
}
*/
/*
function getPersoneelsLeden($userid, $bedrijfsid, $adminSpecifiek = "eigen")
{

 global $db;

 $personeelslidLijstje = "";

 if ($adminSpecifiek == "eigen") {
     $sqlPersleden = "SELECT ID FROM personeel WHERE (TOTWANNEERCONTRACT >= '" . date("Y-m-d") . "' OR TOTWANNEERCONTRACT IS NULL OR TOTWANNEERCONTRACT = '0000-00-00') AND BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' ORDER BY ACHTERNAAM;";
 } else {
     $sqlPersleden = "SELECT ID FROM personeel WHERE (TOTWANNEERCONTRACT >= '" . date("Y-m-d") . "' OR TOTWANNEERCONTRACT IS NULL OR TOTWANNEERCONTRACT = '0000-00-00') ORDER BY ACHTERNAAM;";
 }

 $result_personeel = db_query($sqlPersleden, $db);
 while ($data_personeel = db_fetch_array($result_personeel)) {
     $selected = "";
     if (($userid * 1) == ($data_personeel["ID"] * 1)) {
         $selected = "selected=\"true\"";
     }
     $personeelslidLijstje .= "<option value=\"" . ($data_personeel["ID"] * 1) . "\" $selected>" . getPersoneelslidNaam($data_personeel["ID"]) . "</option>";
 }

 return $personeelslidLijstje;
  
}
*/
function getPersoneelsLidNaam($ID, $length = 0)
{
    global $pdo;

    $naam = "N/A";
    $query = $pdo->prepare('SELECT VOORNAAM, TUSSENVOEGSEL, ACHTERNAAM, INITIALEN FROM personeel WHERE ID = :id limit 1;');
    $query->bindValue('id', $ID);
    $query->execute();
    $bedrijf = $query->fetch(PDO::FETCH_ASSOC);

    if ($bedrijf !== false) {
        $initialen = "";
        if (trim($bedrijf["INITIALEN"]) != "") {
            $initialen = " (" . trim($bedrijf["INITIALEN"]) . ")";
        }

        $voornaam = "";
        if ($bedrijf["VOORNAAM"] != "") {
            $voornaam = $bedrijf["VOORNAAM"] . " ";
        }

        $tussenvoegsel = "";
        if ($bedrijf["TUSSENVOEGSEL"] != "") {
            $tussenvoegsel = $bedrijf["TUSSENVOEGSEL"] . " ";
        }

        $achternaam = "";
        if ($bedrijf["ACHTERNAAM"] != "") {
            $achternaam = $bedrijf["ACHTERNAAM"] . " ";
        }

        $naam = stripslashes($voornaam . $tussenvoegsel . $achternaam . $initialen);

        if ($naam == "") {
            $naam = "N/A";
        }
    }

    if (($length * 1) > 0) {
        if (strlen($naam) >= $length) {
            $naam = substr($naam, 0, ($length - 2)) . "..";
        }
    }

    return lb($naam);
}

function getPersoneelsid($loginid)
{
    if (!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['tmp_PLID']) || $_SESSION['tmp_PLID'] == "0") {
        global $pdo;
        $userid = 0;
        $query = $pdo->prepare('SELECT PERSONEELSLID FROM login WHERE (ID = :id) LIMIT 1;');
        $query->bindValue('id', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $userid = $data["PERSONEELSLID"] * 1;
            $_SESSION['tmp_PLID'] = $userid;
        }
        return $userid;
    } else {
        return $_SESSION['tmp_PLID'];
    }
}

/*
function getPersoneelslidLoginId($perslid)
{
    global $db;

    $lid = 0;
    $result = db_query("SELECT ID FROM login WHERE PERSONEELSLID* = '" . ps($perslid, "nr") . "' LIMIT 1;", $db);
    if ($data = db_fetch_array($result)) {
        $lid = $data["ID"] * 1;
    }

    return $lid;
}
*/

function isKlant()
{
    return isRechten("KLANT");
}

function isBalans()
{
    return isRechten("BALANS");
}

function isVerlofPerMedewerker()
{
    return isRechten("VERLOFMEDEWERKER");
}

function isVerlofPerUur()
{
    return isRechten("VERLOFPERUUR");
}

function isBestandsrechten()
{
    return isRechten("BESTANDEN");
}

function isMailRechten()
{
    return isRechten("WEBMAIL");
}

function isTicketingRechten()
{
    return isRechten("TICKETS");
}

function isMaster()
{
    return isRechten("MASTER");
}

function isToegangEigenAdmin()
{
    return isRechten("ALLEENEIGENADMIN");
}

function isAdministratie()
{
    return isRechten("ADMINISTRATOR");
}

function isDirectie()
{
    return isRechten("DIRECTIE");
}

function isRechten($RECHTEN, $typeRechten = "Ja")
{
    global $pdo, $config;

    if (isset($config["RECHTEN"][$RECHTEN]) && $config["RECHTEN"][$RECHTEN] != "") {
        if ($config["RECHTEN"][$RECHTEN] == $typeRechten) {
            return true;
        } else {
            return false;
        }
    } else {
        $loginid = getLoginID();
        $tableData = getTableData('login', array(), array('login', 'ingelogt'), $RECHTEN);
        if (!isset($tableData[$RECHTEN])) {
            return false;
        }

        $query = $pdo->prepare('SELECT `' . $RECHTEN . '` FROM login WHERE (ID = :id) LIMIT 1;');
        $query->bindValue('id', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            if ($data[$RECHTEN] == $typeRechten) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

function dpm($array, $die = false)
{
    echo "<pre>";
    var_dump($array);
    echo "</pre>";

    if ($die) {
        exit;
    }
}

function isRittenadministratie()
{
    return isRechten("RITTENADMIN");
}

function bestandLocatie($loginid = "", $bedrijfsid = "")
{
    global $pdo, $config;

    $return = "bestanden";

    if (isset($config["CONFIG"]["DEFAULTDIR"]) && $config["CONFIG"]["DEFAULTDIR"] != "") {
        $return = $config["CONFIG"]["DEFAULTDIR"];
    } else {
        if ($loginid == "") {
            $loginid = getLoginID();
        }
        if ($bedrijfsid == "") {
            $bedrijfsid = getBedrijfsID($loginid);
        }

        $query = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id LIMIT 1;');
        $query->bindValue('id', $bedrijfsid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $return = $data["DEFAULTDIR"];
        }
    }

    return $return;
}

function bestandLocatieBedrijf($bedrijfsid)
{
    global $pdo;

    $return = "bestanden";

    $query = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id LIMIT 1;');
    $query->bindValue('id', $bedrijfsid);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        $return = $data["DEFAULTDIR"];
    }

    return $return;
}

/*
function typePlanning()
{
    global $db, $config;

    $return = "Capaciteit"; //Detail, Capaciteit, Beide

    if (isset($config["CONFIG"]["PLANNING"]) && $config["CONFIG"]["PLANNING"] != "") {
        $return = $config["CONFIG"]["PLANNING"];
    } else {
        $result = db_query("SELECT * FROM standaardwaarde WHERE (NAAM = 'PLANNING') LIMIT 1;", $db);
        if ($data = db_fetch_array($result)) {
            $return = lb($data["WAARDE"]);
        }
    }

    return $return;
}
*/
/*
function afboekenHandmatig()
{
    global $db, $config;

    $return = "Nee";

    if (isset($config["CONFIG"]["AFBOEKEN"]) && $config["CONFIG"]["AFBOEKEN"] != "") {
        $return = $config["CONFIG"]["AFBOEKEN"];
    } else {
        $result = db_query("SELECT * FROM standaardwaarde WHERE (NAAM = 'AFBOEKEN') LIMIT 1;", $db);
        if ($data = db_fetch_array($result)) {
            $return = lb($data["WAARDE"]);
        }
    }

    return $return;
}
*/
function locatieDk()
{
    //global $db, $config;

    $return = "https://www.orangefieldonline.com/";

    //if (isset($config["CONFIG"]["LOCATIEDK"]) && $config["CONFIG"]["LOCATIEDK"] != "") {
    //    $return = $config["CONFIG"]["LOCATIEDK"];
    //} else {
    //    return getLocatie();
    //}

    return $return;
}

function beginschermOverslaan()
{
    global $pdo;

    $return = "Nee";
    $query = $pdo->prepare('SELECT * FROM standaardwaarde WHERE (NAAM = "BEGINSCHERMOVERSLAAN") LIMIT 1;');
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        $return = lb($data["WAARDE"]);
    }

    return $return;
}

function isAccountent()
{
    global $pdo, $config;

    if (isset($config["RECHTEN"]["ACCOUNTENT"]) && $config["RECHTEN"]["ACCOUNTENT"] != "") {
        if ($config["RECHTEN"]["ACCOUNTENT"] == "Ja" || $config["RECHTEN"]["ACCOUNTENT"] == "Lees") {
            return true;
        } else {
            return false;
        }
    } else {
        $loginid = getLoginID();
        $query = $pdo->prepare('SELECT ACCOUNTENT FROM login WHERE ID = :id LIMIT 1;');
        $query->bindValue('id', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            if ($data["ACCOUNTENT"] == "Ja" || $data["ACCOUNTENT"] == "Lees") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

function getNietTonenKlanten()
{
    global $pdo;

    $niettonen = "TRUE";
    $query = $pdo->prepare('SELECT * FROM typerelatie WHERE TONENBIJRELATIES = "Nee";');
    $query->execute();

    foreach ($query->fetchAll() as $data_niettonen) {
        $niettonen .= " AND TYPERELATIE != '" . $data_niettonen["NAAM"] . "'";
    }
    if ($niettonen != "TRUE") {
        $niettonen = "WHERE $niettonen";
    } else {
        $niettonen = "";
    }

    return $niettonen;
}

function tl($ref)
{
    // TIJDELIJK: handmatig vertalen... waarom wordt er niet vertaald?
    $vertalen = array(
        'Ja' => 'Yes',
        'Nee' => 'No',
    );

    if (isset($vertalen[$ref])) {
        return $vertalen[$ref];
    }

    return $ref;
}

function vertaal($ref)
{
    return $ref;
}

function encode_msg($msg)
{
    global $image_dir;
    if ($msg) {
        $msg = str_replace("\r", "", $msg); // Replace carrige return
        $msg = str_replace("\n", "<BR>", $msg); // Replace newline with <br>
    }
    return $msg;
}

function urlcode_msg($msg)
{
    if (get_magic_quotes_gpc() != 1) {
        $msg = addslashes($msg);
    }
    $msg = nl2br($msg);
    $msg = ereg_replace("javascript", "", $msg);
    $msg = eregi_replace(quotemeta("[b]"), quotemeta("<b>"), $msg);
    $msg = eregi_replace(quotemeta("[/b]"), quotemeta("</b>"), $msg);
    $msg = eregi_replace(quotemeta("[i]"), quotemeta("<i>"), $msg);
    $msg = eregi_replace(quotemeta("[/i]"), quotemeta("</i>"), $msg);
    $msg = eregi_replace(quotemeta("[u]"), quotemeta("<u>"), $msg);
    $msg = eregi_replace(quotemeta("[/u]"), quotemeta("</u>"), $msg);
    $msg = eregi_replace("\\[url\\]www.([^\\[]*)\\[/url\\]", "<a href=\"http://www.\\1\" target=_blank>\\1</a>", $msg);
    $msg = eregi_replace("\\[url\\]([^\\[]*)\\[/url\\]", "<a href=\"\\1\" target=_blank>\\1</a>", $msg);
    $msg = eregi_replace("\\[url=([^\\[]*)\\]([^\\[]*)\\[/url\\]", "<a href=\"\\1\" target=_blank>\\2</a>", $msg);
    $msg = eregi_replace("\\[email\\]([^\\[]*)\\[/email\\]", "<a href=\"mailto:\\1\">\\1</a>", $msg);
    $msg = eregi_replace("\\[img\\]([^\\[]*)\\[/img\\]", "<img src=\"\\1\" border=0>", $msg);
    $msg = eregi_replace("\\[swf width=([^\\[]*) height=([^\\[]*)\\]([^\\[]*)\\[/swf\\]",
        "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=4\,0\,2\,0\" width=\"\\1\" height=\"\\2\"><param name=quality value=high><param name=\"SRC\" value=\"\\3\"><embed src=\"\\3\" quality=high pluginspage=\"http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash\" type=\"application/x-shockwave-flash\" width=\"\\1\" height=\"\\2\"></embed></object>",
        $msg);
    return $msg;
}

function died($message)
{
    //when we die, than with a nice screen ;-)                                              // tbd. NOT nice yet :-) btw. maybe next releases
    echo $message;
    exit;
}

function poespas()
{
    $poespas = "";
    if (isset($_REQUEST["POESPAS"])) {
        $poespas = "&POESPAS=no";
    }
    return $poespas;
}

function printen()
{
    $poespas = "";
    if (isset($_REQUEST["PRINTEN"])) {
        $poespas = "&PRINTEN=true";
    }
    return $poespas;
}

/*
function getHuidigMenuId()
{
    global $db;

    $site = $_GET["SITE"];

    $type = "";
    if (isset($_GET["TYPE"])) {$type = "&TYPE=" . $_GET["TYPE"];}
    $BEDRIJF = "";
    if (isset($_GET["BEDRIJF"])) {$type = "&BEDRIJF=" . $_GET["BEDRIJF"];}
    $STATUS = "";
    if (isset($_GET["STATUS"])) {$type = "&STATUS=" . $_GET["STATUS"];}

    $sql = "SELECT * FROM menuitems WHERE LINK LIKE 'content.php?SITE=$site" . $type . "" . $BEDRIJF . "" . $STATUS . "%' ORDER BY KLANT DESC;"; //Eerst de links niet van een klant, en daarna wel.
    //echo $sql;
    $res = db_query($sql, $db);
    if ($data = db_fetch_array($res)) {
        return ($data["ID"] * 1);
    }
    return "0";
}
 */
/*
function target()
{
    $target = "MAIN";
    $getScherm = getScherm();
    if ($getScherm > 1) {
        $target .= $getScherm;
    }
    return $target;
}
*/

function getScherm()
{
    if (isset($_REQUEST["SCHERM"])) {
        $scherm = $_REQUEST["SCHERM"];
    } else {
        if (!isset($_SESSION)) {
            session_start();
        }
        $scherm = '1';
        if (isset($_SESSION["SCHERM"])) {
            $scherm = $_SESSION["SCHERM"];
        }
    }
    return $scherm;
}

function setScherm($scherm)
{
    if ($scherm != "0" && $scherm != "") {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['SCHERM'] = $scherm;
    }
}

/*
function getZoekActieOp($userid, $waar)
{
    global $db;

    $output = "";

    $SQL = "SELECT WAT FROM zoekenrelbeheer WHERE ((PERSONEELSLID*1) = ('" . ps($userid, "nr") . "')) AND (WAAR = '" . ps($waar) . "');";
    $result = db_query($SQL, $db);
    if ($data = db_fetch_array($result)) {
        $output = $data["WAT"];
    }
    return $output;
}
*/
function emptyTemporaryMap()
{
    //Verwijder alle temp bestanden.
    $bestandslocatie = bestandLocatie();
    $tempDir = "$bestandslocatie/temporary";
    if (is_dir($tempDir)) {
        $dh = @opendir($tempDir);
        while (false !== ($obj = @readdir($dh))) {
            if ($obj != '.' && $obj != '..') {
                @unlink($tempDir . '/' . $obj);
            }
        }
        @closedir($dh);
    }
    $tempDir = "$bestandslocatie/bankbestanden";
    if (is_dir($tempDir)) {
        $dh = @opendir($tempDir);
        while (false !== ($obj = @readdir($dh))) {
            if ($obj != '.' && $obj != '..') {
                @unlink($tempDir . '/' . $obj);
            }
        }
        @closedir($dh);
    }
}

/*
function emptyMailMap()
{
    global $db;

    //Verwijder alle temp bestanden.
    $bestandslocatie = bestandLocatie();
    $tempDir = "$bestandslocatie/mail/_attachments/" . date("Ymd") . "/";
    if (is_dir($tempDir)) {
        $dh = @opendir($tempDir);
        while (false !== ($obj = @readdir($dh))) {
            if ($obj != '.' && $obj != '..') {
                $SQL = "select * from mail where ATTACHMENT like '%" . ps($obj) . "%' limit 1;";
                $result = db_query($SQL, $db);
                if (db_num_rows($result) == 0) {
                    $filename = $tempDir . '/' . $obj;
                    $filename = str_replace("//", "/", $filename);
                    if (!file_exists($filename)) {
                        unlink($filename);
                    }
                }
            }
        }
        @closedir($dh);
    }
}
*/
function stopDigitaalKantoor($userid, $bedrijfsid)
{
    global $pdo;
    //Empty klembord
    $queryKlembord = $pdo->prepare('delete from klembord where PERSONEELSLID = :personeelslid;');
    $queryKlembord->bindValue('personeelslid', $userid);
    $queryKlembord->execute();

    $queryIngelogd = $pdo->prepare('update ingelogt set SESSIONID = "' . date("Y-m-d H:i:s") . '" where SESSIONID = :sessionid;');
    $queryIngelogd->bindValue('sessionid', $_SESSION["LID"]);
    $queryIngelogd->execute();

    //empty temp files
    emptyTemporaryMap();

    if (!isset($_SESSION)) {
        session_start();
    }
    $_SESSION['DB'] = null;
    $_SESSION['LID'] = null;
    setcookie("USERNAME", "", time() - 36000, "/");
    setcookie("PASSWORD", "", time() - 36000, "/");
    setcookie("LID", "", time() - 36000, "/");
    setcookie("DB", "", time() - 36000, "/");
    setcookie("PDA", "", time() - 36000, "/");
    setcookie("IP", "", time() - 36000, "/");
    session_destroy();

    $url = "index.php";
    if (config_firstPageAdmin(".") == "true") {
        $url = "admin.php?DASHBOARD=true";
    }

    echo("<script type=\"text/javascript\">\n");
    echo("window.top.location.replace('" . $url . "');\n");
    echo("</script>");
    //rd("$url");
}

function isOrangeField()
{
    return true;
}

function extraFooter($metRuimte = "Nee")
{
    $pakketTitel = pakketTitel();
    $FooterLogin = FooterLogin();
    $output = "";

    if (isOrangeField() && !isset($_REQUEST["PDF"])) {
        $pakketTitel = pakketTitel();
        if ($metRuimte == "Ja") {
            $output .= "<tr><td class=\"mainmenuachter\" style=\"height: 65px;\" valign=\"top\" ><img alt=\"\" src=\"./images/px.gif\" style=\"border-top: 1px solid #ff5800;\" border=\"0\" height=\"2\" width=\"100%\" /></td></tr>";
        }
        $output .= "<tr><td id=\"balk\" colspan=\"10\">&nbsp;&nbsp;$pakketTitel</td></tr>
                      <tr><td id=\"balk1\" colspan=\"10\">&nbsp;&nbsp;$FooterLogin</td></tr>
                      <tr><td height=\"20px\" colspan=\"10\">&nbsp;</td></tr>";
    }
    return $output;
}

function enkeleSite($loginid, $titel1)
{
    global $pdo;

    if (getPDA() == "0") {
        $query = $pdo->prepare('SELECT * FROM login WHERE ID = :id limit 1;');
        $query->bindValue('id', $loginid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            if ($data["STYLE"] == "Empty") {
                echo("<table BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"0\" WIDTH=\"1000\" HEIGHT=\"100%\" align=\"center\" class=\"mainfield\">
                     <tr>
                     <td height=\"100%\">
                       <div id=\"scherm1\" style=\"height: 100%;\"><IFRAME NAME=\"MAIN\" WIDTH=\"100%\" id=\"MAIN\" HEIGHT=\"100%\" src=\"" . $titel1 . "\" frameborder=\"No\" onload=\"setHighLight();\"></IFRAME></div>
                     </td></tr>
                     </table>");
            } else {
                //check($kolom, $defaultValue, $bedrijfsid)

                echo "<table border=\"0\" width=\"100%\" height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #f1f1f2;\">"; //id=\"wrapper\" class=\"wrapper\"
                topbalk();

                // id=\"iframes\"
                echo("<tr><td align=\"center\" height=\"100%\">
                       <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"1000\" height=\"100%\" align=\"center\">
                       <tr>
                       <td width=\"23\" valign=\"top\" style=\"background: url(./images/linkerschaduw.png);\" border=\"0\" id=\"linkerschaduw\" width=\"23\" height=\"100%\" /></td>
                       <td height=\"100%\">");
                echo("<div id=\"scherm1\" style=\"height: 100%;\"><iframe NAME=\"MAIN\" WIDTH=\"100%\" id=\"MAIN\" HEIGHT=\"100%\" src=\"" . $titel1 . "\" frameborder=\"No\" onload=\"setHighLight();\"></iframe></div>");
                if ($data["STYLE"] != "Zonder tabbladen" && $data["STYLE"] != "Zonder tabbladen - custom") {
                    echo("<div id=\"scherm2\" style=\"display: none; height: 0px;\"><iframe NAME=\"MAIN2\" WIDTH=\"100%\" id=\"MAIN2\" HEIGHT=\"100%\" src=\"content.php?SITE=leegScherm\" frameborder=\"No\"></iframe></div>");
                    echo("<div id=\"scherm3\" style=\"display: none; height: 0px;\"><iframe NAME=\"MAIN3\" WIDTH=\"100%\" id=\"MAIN3\" HEIGHT=\"100%\" src=\"content.php?SITE=leegScherm\" frameborder=\"No\"></iframe></div>");
                    echo("<div id=\"scherm4\" style=\"display: none; height: 0px;\"><iframe NAME=\"MAIN4\" WIDTH=\"100%\" id=\"MAIN4\" HEIGHT=\"100%\" src=\"content.php?SITE=leegScherm\" frameborder=\"No\"></iframe></div>");
                }
                echo("</td>
                       <td width=\"23\" valign=\"top\" style=\"background: url(./images/rechterschaduw.png);\" border=\"0\" id=\"rechterschaduw\" width=\"23\" height=\"100%\" /></td>
                       </tr>
                       </table>
                     </td></tr>
                     </table>");
            }
        }
    } else {
        rd("$titel1");
    }
}

function leegScherm()
{
    $watWordStartScherm = getTabbladLink(getMyBedrijfsID(), getMySelf(), getScherm());

    echo("<br/><br/><center><a href=\"$watWordStartScherm\">" . tl("Klik hier om verder te gaan.") . "</a></center>");
}

function spiegelscherm($titel1, $titel2)
{
    echo("<frameset rows=\"*\" frameborder=\"NO\" border=\"0\" framespacing=\"0\" framepadding=\"0\" scrolling=\"NO\">
       <frameset cols=\"500,500\" name=\"mainframe\" frameborder=\"No\" border=\"0\" framespacing=\"0\" scrolling=\"NO\">
         <frame src=\"" . $titel1 . "\" noresize name=\"overzicht\">
         <frame src=\"" . $titel2 . "\" noresize name=\"main\">
       </frameset>
  </frameset>
  <noframes>
  <body leftmargin=\"0\" rightmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");

    counter("Niet bekend");

    echo("<center>
    Uw browser ondersteunt geen frames.
  </center>
  </body>
  </noframes>");
}

function counter($titel)
{
    $bedrijfsid = 1;

    //TO DO
}

/*
function dbTekst($titel)
{
    global $db;
    $result = db_query("SELECT * FROM teksten WHERE NAAM='" . ps($titel) . "'", $db);
    while ($Naam = db_fetch_array($result)) {echo (lb($Naam["TEKST"]));}
}
*/
function berekenVerschilInMinuten($tijdVan, $tijdTot)
{

    /*
    $verschil1 = substr($tijdTot,0,2);
    $verschil2 = substr(substr($tijdTot,3,5),0,2);
    $verschil3 = substr($tijdVan,0,2);
    $verschil4 = substr(substr($tijdVan,3,5),0,2);

    $minuten   = $verschil2 - $verschil4;
    $uren      = ($verschil1 - $verschil3)*60;
    $verschil  = $uren + $minuten;
     */

    if ($tijdTot == '24:00:00') {
        $tijdTot = '23:59:59';
    }
    if ($tijdVan == '24:00:00') {
        $tijdVan = '23:59:59';
    }

    $tijdVan = strtotime($tijdVan);
    $tijdTot = strtotime($tijdTot);
    if ($tijdTot < $tijdVan) {
        $tijdtmp = $tijdVan;
        $tijdTot = $tijdVan;
        $tijdVan = $tijdtmp;
    }
    $verschil = ($tijdTot - $tijdVan) / 60;

    $verschil = ceil($verschil);

    return $verschil;
}

function errorPage($kop, $error, $link = "Login: try again")
{
    echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"1000\" class=\"mainfield\" align=\"center\">
    <tr>
    <td>");
    kopBalk($kop, "Error");
    echo("</td></tr>
    <tr><td>
        <BR>
        <h1>Error: </h1>
        " . $error . "
        <BR><br><a href=\"index.php\" target=\"_top\">$link &#187;</a>
        </td>
    </tr>
    </table>");
}

/*
function voorkomeKenmerk($kenmerk, $bedrijfsid)
{
    global $db;

    $bedrijfsid = 1;

    $res = db_query("SELECT * FROM helpitems WHERE KENMERK = '" . ps($kenmerk) . "' AND BEDRIJFSID = ('" . ps($bedrijfsid, "nr") . "')", $db);
    $counter = 0;
    while ($temp = db_fetch_array($res)) {$counter = $counter + 1;}
    if (($counter * 1) == 0) {
        $insert = "INSERT INTO helpitems VALUES ('0', '" . ps($bedrijfsid, "nr") . "', '" . ps($kenmerk) . "', 'Dit onderdeel is nog niet beschreven. Voor vragen: Neem contact op met uw service provider (<a target=\"blank\" href=\"http://www.bobsoft.biz/\">BobSoft</a>)','" . ps($kenmerk) . "', '1')";
        $res = db_query($insert, $db) or die("Fout in script! " . db_error());
    } else {
        $insert = "UPDATE helpitems SET BEZOCHT = (BEZOCHT+1) WHERE (KENMERK = '" . ps($kenmerk) . "') AND (BEDRIJFSID = ('" . ps($bedrijfsid, "nr") . "'))";
        $res = db_query($insert, $db) or die("Fout in script! " . db_error());
    }
}
*/

function watWordStartScherm()
{
    $return = "mededelingen";
    $result = getValueFromStandaardWaarde('STARTSCHERM');
    if ($result != "") {
        $return = $result;
    }

    return $return;
}

function getValueFromStandaardWaarde($value)
{
    global $pdo, $config;

    $return = "";
    if (isset($config["CONFIG"][$value]) && $config["CONFIG"][$value] != "") {
        $return = lb($config["CONFIG"][$value]);
    } else {
        $query = $pdo->prepare('SELECT * FROM standaardwaarde WHERE NAAM = :naam LIMIT 1;');
        $query->bindValue('naam', $value);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $return = lb($data["WAARDE"]);
        }
    }

    return $return;
}

function opslaanStandaardWaarde($kolom, $value, $bedrijfsid)
{
    global $pdo;

    $querySelect = $pdo->prepare('select ID from standaardwaarde where NAAM = :naam limit 1;');
    $querySelect->bindValue('naam', $kolom);
    $querySelect->execute();

    if ($querySelect->rowCount() > 0) {
        //Update
        $query = $pdo->prepare('update standaardwaarde set `WAARDE` = :waarde where NAAM = :naam;');
        $query->bindValue('waarde', $value);
        $query->bindValue('naam', $kolom);
        $query->execute();
    } else {
        $query = $pdo->prepare('insert into standaardwaarde set `WAARDE` = :waarde, NAAM = :naam, BEDRIJFSID =  :bedrijfsid;');
        $query->bindValue('waarde', $value);
        $query->bindValue('naam', $kolom);
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $query->execute();
    }
}

function check($kolom, $defaultValue, $bedrijfsid)
{
    global $pdo;

    $return = $defaultValue;

    $query = $pdo->prepare('SELECT WAARDE FROM standaardwaarde WHERE NAAM = :naam LIMIT 1;');
    $query->bindValue('naam', $kolom);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if (!$data) {
        opslaanStandaardWaarde($kolom, $defaultValue, $bedrijfsid);
    } else {
        $return = lb($data["WAARDE"]);
    }

    return $return;
}

function afterlogin($loginid)
{
    setScherm(1);

    //Als wizzard nog niet is ingevuld, dan eerst wizzard tonen
    if (getValueFromStandaardWaarde("WIZZARD") != "Ingevuld") {
        site("bedrijfWizzard", $loginid);
    } else {
        if (beginschermOverslaan() == "Ja") {
            site("kantoormenu", $loginid);
        } else {
            $watWordStartScherm = getTabbladLink(getMyBedrijfsID(), getMySelf(), 1);
            $watWordStartScherm = str_replace("content.php?SITE=", "", $watWordStartScherm);
            site($watWordStartScherm, $loginid);
        }
    }
}

function toUTF($tekst)
{
    $tekst = basisHtml2UTF(utf2html(stripslashes($tekst)));
    $tekst = str_replace("&#128;", "�", $tekst);
    return $tekst;
}

function basisHtml2UTF($str)
{
    $str = str_replace("&#128;", "�", $str);
    $str = str_replace("&#649;", "�", $str);
    $str = str_replace("&#650;", "�", $str);
    $str = str_replace("&#632;", "�", $str);
    $str = str_replace("&#192;", "�", $str);
    $str = str_replace("&#193;", "�", $str);
    $str = str_replace("&#194;", "�", $str);
    $str = str_replace("&#195;", "�", $str);
    $str = str_replace("&#196;", "�", $str);
    $str = str_replace("&#197;", "�", $str);
    $str = str_replace("&#198;", "�", $str);
    $str = str_replace("&#199;", "�", $str);
    $str = str_replace("&#200;", "�", $str);
    $str = str_replace("&#201;", "�", $str);
    $str = str_replace("&#202;", "�", $str);
    $str = str_replace("&#203;", "�", $str);
    $str = str_replace("&#204;", "�", $str);
    $str = str_replace("&#205;", "�", $str);
    $str = str_replace("&#206;", "�", $str);
    $str = str_replace("&#207;", "�", $str);
    $str = str_replace("&#208;", "�", $str);
    $str = str_replace("&#209;", "�", $str);
    $str = str_replace("&#210;", "�", $str);
    $str = str_replace("&#211;", "�", $str);
    $str = str_replace("&#212;", "�", $str);
    $str = str_replace("&#213;", "�", $str);
    $str = str_replace("&#214;", "�", $str);
    $str = str_replace("&#216;", "�", $str);
    $str = str_replace("&#217;", "�", $str);
    $str = str_replace("&#218;", "�", $str);
    $str = str_replace("&#219;", "�", $str);
    $str = str_replace("&#220;", "�", $str);
    $str = str_replace("&#221;", "�", $str);
    $str = str_replace("&#222;", "�", $str);
    $str = str_replace("&#223;", "�", $str);
    $str = str_replace("&#224;", "�", $str);
    $str = str_replace("&#225;", "�", $str);
    $str = str_replace("&#226;", "�", $str);
    $str = str_replace("&#227;", "�", $str);
    $str = str_replace("&#228;", "�", $str);
    $str = str_replace("&#229;", "�", $str);
    $str = str_replace("&#230;", "�", $str);
    $str = str_replace("&#231;", "�", $str);
    $str = str_replace("&#232;", "�", $str);
    $str = str_replace("&#233;", "�", $str);
    $str = str_replace("&#234;", "�", $str);
    $str = str_replace("&#235;", "�", $str);
    $str = str_replace("&#236;", "�", $str);
    $str = str_replace("&#237;", "�", $str);
    $str = str_replace("&#238;", "�", $str);
    $str = str_replace("&#239;", "�", $str);
    $str = str_replace("&#240;", "�", $str);
    $str = str_replace("&#241;", "�", $str);
    $str = str_replace("&#242;", "�", $str);
    $str = str_replace("&#243;", "�", $str);
    $str = str_replace("&#244;", "�", $str);
    $str = str_replace("&#245;", "�", $str);
    $str = str_replace("&#246;", "�", $str);
    $str = str_replace("&#248;", "�", $str);
    $str = str_replace("&#249;", "�", $str);
    $str = str_replace("&#250;", "�", $str);
    $str = str_replace("&#251;", "�", $str);
    $str = str_replace("&#252;", "�", $str);
    $str = str_replace("&#253;", "�", $str);
    $str = str_replace("&#254;", "�", $str);
    $str = str_replace("&#255;", "�", $str);

    $str = str_replace("&Agrave;", "�", $str);
    $str = str_replace("&Aacute;", "�", $str);
    $str = str_replace("&Acirc;", "� ", $str);
    $str = str_replace("&Atilde;", "�", $str);
    $str = str_replace("&Auml;", "�", $str);
    $str = str_replace("&Aring;", "�", $str);
    $str = str_replace("&AElig;", "�", $str);
    $str = str_replace("&Ccedil;", "�", $str);
    $str = str_replace("&Egrave;", "�", $str);
    $str = str_replace("&Eacute;", "�", $str);
    $str = str_replace("&Ecirc;", "�", $str);
    $str = str_replace("&Euml;", "�", $str);
    $str = str_replace("&Igrave;", "�", $str);
    $str = str_replace("&Iacute;", "�", $str);
    $str = str_replace("&Icirc;", "�", $str);
    $str = str_replace("&Iuml;", "�", $str);
    $str = str_replace("&ETH;", "�", $str);
    $str = str_replace("&Ntilde;", "�", $str);
    $str = str_replace("&Ograve;", "�", $str);
    $str = str_replace("&Oacute;", "�", $str);
    $str = str_replace("&Ocirc;", "�", $str);
    $str = str_replace("&Otilde;", "�", $str);
    $str = str_replace("&Ouml;", "�", $str);
    $str = str_replace("&Oslash;", "�", $str);
    $str = str_replace("&Ugrave;", "�", $str);
    $str = str_replace("&Uacute;", "�", $str);
    $str = str_replace("&Ucirc;", "�", $str);
    $str = str_replace("&Uuml;", "�", $str);
    $str = str_replace("&Yacute;", "�", $str);
    $str = str_replace("&THORN;", "�", $str);
    $str = str_replace("&szlig;", "�", $str);
    $str = str_replace("&agrave;", "�", $str);
    $str = str_replace("&aacute;", "�", $str);
    $str = str_replace("&acirc;", "�", $str);
    $str = str_replace("&atilde;", "�", $str);
    $str = str_replace("&auml;", "�", $str);
    $str = str_replace("&aring;", "�", $str);
    $str = str_replace("&aelig;", "�", $str);
    $str = str_replace("&ccedil;", "�", $str);
    $str = str_replace("&egrave;", "�", $str);
    $str = str_replace("&eacute;", "�", $str);
    $str = str_replace("&ecirc;", "�", $str);
    $str = str_replace("&euml;", "�", $str);
    $str = str_replace("&igrave;", "�", $str);
    $str = str_replace("&iacute;", "�", $str);
    $str = str_replace("&icirc;", "�", $str);
    $str = str_replace("&iuml;", "�", $str);
    $str = str_replace("&eth;", "�", $str);
    $str = str_replace("&ntilde;", "�", $str);
    $str = str_replace("&ograve;", "�", $str);
    $str = str_replace("&oacute;", "�", $str);
    $str = str_replace("&ocirc;", "�", $str);
    $str = str_replace("&otilde;", "�", $str);
    $str = str_replace("&ouml;", "�", $str);
    $str = str_replace("&oslash;", "�", $str);
    $str = str_replace("&ugrave;", "�", $str);
    $str = str_replace("&uacute;", "�", $str);
    $str = str_replace("&ucirc;", "�", $str);
    $str = str_replace("&uuml;", "�", $str);
    $str = str_replace("&yacute;", "�", $str);
    $str = str_replace("&thorn;", "�", $str);
    $str = str_replace("&yuml;", "�", $str);

    $str = str_replace("ë", "�", $str);
    return $str;
}

function basisutf2html($str)
{
    $str = str_replace("�", "&Agrave;", $str);
    $str = str_replace("�", "&Aacute;", $str);
    $str = str_replace("�", "&Acirc;", $str);
    $str = str_replace("�", "&Atilde;", $str);
    $str = str_replace("�", "&Auml;", $str);
    $str = str_replace("�", "&Aring;", $str);
    $str = str_replace("�", "&AElig;", $str);
    $str = str_replace("�", "&Ccedil;", $str);
    $str = str_replace("�", "&Egrave;", $str);
    $str = str_replace("�", "&Eacute;", $str);
    $str = str_replace("�", "&Ecirc;", $str);
    $str = str_replace("�", "&Euml;", $str);
    $str = str_replace("�", "&Igrave;", $str);
    $str = str_replace("�", "&Iacute;", $str);
    $str = str_replace("�", "&Icirc;", $str);
    $str = str_replace("�", "&Iuml;", $str);
    $str = str_replace("�", "&ETH;", $str);
    $str = str_replace("�", "&Ntilde;", $str);
    $str = str_replace("�", "&Ograve;", $str);
    $str = str_replace("�", "&Oacute;", $str);
    $str = str_replace("�", "&Ocirc;", $str);
    $str = str_replace("�", "&Otilde;", $str);
    $str = str_replace("�", "&Ouml;", $str);
    $str = str_replace("�", "&Oslash;", $str);
    $str = str_replace("�", "&Ugrave;", $str);
    $str = str_replace("�", "&Uacute;", $str);
    $str = str_replace("�", "&Ucirc;", $str);
    $str = str_replace("�", "&Uuml;", $str);
    $str = str_replace("�", "&Yacute;", $str);
    $str = str_replace("�", "&THORN;", $str);
    $str = str_replace("�", "&szlig;", $str);
    $str = str_replace("�", "&agrave;", $str);
    $str = str_replace("�", "&aacute;", $str);
    $str = str_replace("�", "&acirc;", $str);
    $str = str_replace("�", "&atilde;", $str);
    $str = str_replace("�", "&auml;", $str);
    $str = str_replace("�", "&aring;", $str);
    $str = str_replace("�", "&aelig;", $str);
    $str = str_replace("�", "&ccedil;", $str);
    $str = str_replace("�", "&egrave;", $str);
    $str = str_replace("�", "&eacute;", $str);
    $str = str_replace("�", "&ecirc;", $str);
    $str = str_replace("�", "&euml;", $str);
    $str = str_replace("�", "&igrave;", $str);
    $str = str_replace("�", "&iacute;", $str);
    $str = str_replace("�", "&icirc;", $str);
    $str = str_replace("�", "&iuml;", $str);
    $str = str_replace("�", "&eth;", $str);
    $str = str_replace("�", "&ntilde;", $str);
    $str = str_replace("�", "&ograve;", $str);
    $str = str_replace("�", "&oacute;", $str);
    $str = str_replace("�", "&ocirc;", $str);
    $str = str_replace("�", "&otilde;", $str);
    $str = str_replace("�", "&ouml;", $str);
    $str = str_replace("�", "&oslash;", $str);
    $str = str_replace("�", "&ugrave;", $str);
    $str = str_replace("�", "&uacute;", $str);
    $str = str_replace("�", "&ucirc;", $str);
    $str = str_replace("�", "&uuml;", $str);
    $str = str_replace("�", "&yacute;", $str);
    $str = str_replace("�", "&thorn;", $str);
    $str = str_replace("�", "&yuml;", $str);
    $str = str_replace("&#192;", "&Agrave;", $str);
    $str = str_replace("&#193;", "&Aacute;", $str);
    $str = str_replace("&#194;", "&Acirc;", $str);
    $str = str_replace("&#195;", "&Atilde;", $str);
    $str = str_replace("&#196;", "&Auml;", $str);
    $str = str_replace("&#197;", "&Aring;", $str);
    $str = str_replace("&#198;", "&AElig;", $str);
    $str = str_replace("&#199;", "&Ccedil;", $str);
    $str = str_replace("&#200;", "&Egrave;", $str);
    $str = str_replace("&#201;", "&Eacute;", $str);
    $str = str_replace("&#202;", "&Ecirc;", $str);
    $str = str_replace("&#203;", "&Euml;", $str);
    $str = str_replace("&#204;", "&Igrave;", $str);
    $str = str_replace("&#205;", "&Iacute;", $str);
    $str = str_replace("&#206;", "&Icirc;", $str);
    $str = str_replace("&#207;", "&Iuml;", $str);
    $str = str_replace("&#208;", "&ETH;", $str);
    $str = str_replace("&#209;", "&Ntilde;", $str);
    $str = str_replace("&#210;", "&Ograve;", $str);
    $str = str_replace("&#211;", "&Oacute;", $str);
    $str = str_replace("&#212;", "&Ocirc;", $str);
    $str = str_replace("&#213;", "&Otilde;", $str);
    $str = str_replace("&#214;", "&Ouml;", $str);
    $str = str_replace("&#216;", "&Oslash", $str);
    $str = str_replace("&#217;", "&Ugrave;", $str);
    $str = str_replace("&#218;", "&Uacute;", $str);
    $str = str_replace("&#219;", "&Ucirc;", $str);
    $str = str_replace("&#220;", "&Uuml;", $str);
    $str = str_replace("&#221;", "&Yacute;", $str);
    $str = str_replace("&#222;", "&THORN;", $str);
    $str = str_replace("&#223;", "&szlig;", $str);
    $str = str_replace("&#224;", "&agrave;", $str);
    $str = str_replace("&#225;", "&aacute;", $str);
    $str = str_replace("&#226;", "&acirc;", $str);
    $str = str_replace("&#227;", "&atilde;", $str);
    $str = str_replace("&#228;", "&auml;", $str);
    $str = str_replace("&#229;", "&aring;", $str);
    $str = str_replace("&#230;", "&aelig;", $str);
    $str = str_replace("&#231;", "&ccedil;", $str);
    $str = str_replace("&#232;", "&egrave;", $str);
    $str = str_replace("&#233;", "&eacute;", $str);
    $str = str_replace("&#234;", "&ecirc;", $str);
    $str = str_replace("&#235;", "&euml;", $str);
    $str = str_replace("&#236;", "&igrave;", $str);
    $str = str_replace("&#237;", "&iacute;", $str);
    $str = str_replace("&#238;", "&icirc;", $str);
    $str = str_replace("&#239;", "&iuml;", $str);
    $str = str_replace("&#240;", "&eth;", $str);
    $str = str_replace("&#241;", "&ntilde;", $str);
    $str = str_replace("&#242;", "&ograve;", $str);
    $str = str_replace("&#243;", "&oacute;", $str);
    $str = str_replace("&#244;", "&ocirc;", $str);
    $str = str_replace("&#245;", "&otilde;", $str);
    $str = str_replace("&#246;", "&ouml;", $str);
    $str = str_replace("&#248;", "&oslash;", $str);
    $str = str_replace("&#249;", "&ugrave;", $str);
    $str = str_replace("&#250;", "&uacute;", $str);
    $str = str_replace("&#251;", "&ucirc;", $str);
    $str = str_replace("&#252;", "&uuml;", $str);
    $str = str_replace("&#253;", "&yacute;", $str);
    $str = str_replace("&#254;", "&thorn;", $str);
    $str = str_replace("&#255;", "&yuml;", $str);

    return $str;
}

function utf2html($str)
{

    $search = array(chr(128));
    $replace = array("&#128;");
    $str = str_replace($search[0], $replace[0], $str);

    $str = str_replace("“", "\"", $str);
    $str = str_replace("�", "&#128;", $str);
    $str = str_replace("�?�", "&#128;", $str);
    $str = str_replace("€", "&#128;", $str);
    $str = str_replace("&#226;?&#172;", "&#128;", $str);
    $str = str_replace("&amp;#226;?&amp;#172;", "&#128;", $str);
    $str = str_replace("�&#128;�", "\"", $str);
    $str = str_replace("�&#128;", "\"", $str);

    $ret = "";
    $max = strlen($str);
    $last = 0; // keeps the index of the last regular character
    for ($i = 0; $i < $max; $i++) {
        $c = $str{$i};
        $c1 = ord($c);
        if ($c1 >> 5 == 6) {
            // 110x xxxx, 110 prefix for 2 bytes unicode
            $ret .= substr($str, $last, $i - $last); // append all the regular characters we've passed
            $c1 &= 31; // remove the 3 bit two bytes prefix
            $c2 = ord($str{++$i}); // the next byte
            $c2 &= 63; // remove the 2 bit trailing byte prefix
            $c2 |= (($c1 & 3) << 6); // last 2 bits of c1 become first 2 of c2
            $c1 >>= 2; // c1 shifts 2 to the right
            $ret .= "&#" . ($c1 * 100 + $c2) . ";"; // this is the fastest string concatenation
            $last = $i + 1;
        }
    }
    $str = str_replace("--&gt;", "-->", $str);
    $str = str_replace("&lt;--", "<--", $str);
    return $ret . substr($str, $last, $i); // append the last batch of regular characters

}

/*
function medewerkerBenaming()
{
    global $db;

    $result_stand = db_query("SELECT * FROM standaardwaarde WHERE ID = ('11'*1) LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    $waarde = $data_stand["WAARDE"];
    return $waarde;
}
*/

function btwHoog()
{
    global $pdo;

    $query = $pdo->prepare('SELECT * FROM standaardwaarde WHERE ID = ("1"*1) LIMIT 1;');
    $query->execute();
    $data_stand = $query->fetch(PDO::FETCH_ASSOC);
    $bedrag = ($data_stand["WAARDE"] / 100);

    return $bedrag;
}

/*
function btwLaag()
{
    global $db;

    $result_stand = db_query("SELECT * FROM standaardwaarde WHERE ID = ('2'*1) LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    $bedrag = ($data_stand["WAARDE"] / 100);
    return $bedrag;
}
*/
/*
function btwDefault()
{
    global $db;

    $result_stand = db_query("SELECT * FROM standaardwaarde WHERE NAAM = 'DEFAULTBTWTARIEF' LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    $bedrag = ($data_stand["WAARDE"] / 100);

    if ($bedrag == null || $bedrag == "") {$bedrag = "0.19";}
    return lb($bedrag);
}
*/
/*
function verlofJaNee()
{
    global $db;

    $loginid = getLoginID();
    $result_stand = db_query("SELECT * FROM login WHERE ID = '" . ps($loginid, "nr") . "' LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    $ant = $data_stand["VERLOF"];
    if ($ant == "") {$ant = "Nee";}

    return lb($ant);
}
*/
/*
function formulierJaNee()
{
    global $db;

    $ant = false;
    $loginid = getLoginID();
    $result_stand = db_query("SELECT * FROM login WHERE ID = '" . ps($loginid, "nr") . "' LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    $ant = $data_stand["OPDRACHTEN"];
    if ($ant == "") {$ant = false;} elseif ($ant == "") {$ant = true;} elseif ($ant == "Nee") {$ant = false;}

    return lb($ant);
}
*/
function getLocatie()
{
    $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $url = explode("content.", $url);
    $url = trim($url[0]);

    $url = explode("app_index.", $url);
    $return = trim($url[0]);

    if ($return == '') {
        return check("LOCATIEDK", "http://www.orangefieldonline.com/", 1);
    }

    return lb($return);
}

function isSecurity($loginid)
{
    global $pdo;

    $query = $pdo->prepare('SELECT * FROM login WHERE ID = :id LIMIT 1;');
    $query->bindValue('id', $loginid);
    $query->execute();
    $data_stand = $query->fetch(PDO::FETCH_ASSOC);

    $ant = $data_stand["SECURITYMOD"];
    if ($ant == "") {
        $ant = false;
    } else {
        if ($ant == "Ja") {
            $ant = true;
        } else {
            if ($ant == "Nee") {
                $ant = false;
            }
        }
    }

    return $ant;
}

/*
function aantalItems()
{
    global $db;

    $result_stand = db_query("SELECT * FROM standaardwaarde WHERE ID = '5' LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    $bedrag = lb($data_stand["WAARDE"]);
    return $bedrag;
}
*/
/*
function standaardChecked()
{
    global $db;

    $return = "Nee";
    $result_stand = db_query("SELECT * FROM standaardwaarde WHERE ID = '6' LIMIT 1;", $db);
    if ($data_stand = db_fetch_array($result_stand)) {
        $return = lb($data_stand["WAARDE"]);
    }
    return $return;
}
*/
/*
function prijsPerPerslidApart()
{
    global $db;

    $result_stand = db_query("SELECT * FROM standaardwaarde WHERE ID = ('7'*1) LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);

    return $data_stand["WAARDE"];
}
*/
/*
function nieuwNummer($tabel, $veld, $bedrijfsid)
{
    global $db;

    $nr = 1;
    $result = db_query("SELECT * FROM `".ps($tabel, "tabel")."` WHERE BEDRIJFSID = ('" . ps($bedrijfsid, "nr") . "') ORDER BY ($veld*1) DESC;", $db);
    if ($data = db_fetch_array($result)) {
        if (isset($data[$veld])) {
            $nr = $data[$veld];
            $nr = $nr + 1;}
    }
    return $nr;
}
*/
/*
function ifIsCreditDebetWenV($grootboekid, $creditdebet)
{
    global $db;

    $return = true;

    $result_stand = db_query("SELECT * FROM grootboekitems WHERE ID = '" . ps($grootboekid, "nr") . "' LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    if ($data_stand["BALANSKOSTEN"] == "Kosten of opbrengst") {
        if ($data_stand["CREDITDEBET"] == $creditdebet) {$return = true;} else { $return = false;}
    } else { $return = false;}

    return $return;
}
*/
/*
function positieOpDeBalans($grootboekid)
{
    global $db;

    $result_stand = db_query("SELECT * FROM grootboekitems WHERE ID = '" . ps($grootboekid, "nr") . "' LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);

    return $data_stand["CREDITDEBET"];
}
*/
/*
function positieOpDeBalansDraaiOm($grootboekid)
{
    global $db;

    $result_stand = db_query("SELECT * FROM grootboekitems WHERE ID = '" . ps($grootboekid, "nr") . "' LIMIT 1;", $db);
    $data_stand = db_fetch_array($result_stand);
    $pos = $data_stand["CREDITDEBET"];

    if ($pos == "Credit") {$pos = "Debet";} elseif ($pos == "Debet") {$pos = "Credit";}

    return $pos;
}
*/
/*
function vorigeDag($dagorg, $maandorg, $jaarorg, $min)
{
    $dag = date("d", strtotime("-$min days", strtotime("$jaarorg-$maandorg-$dagorg")));
    $maand = date("m", strtotime("-$min days", strtotime("$jaarorg-$maandorg-$dagorg")));
    $jaar = date("Y", strtotime("-$min  days", strtotime("$jaarorg-$maandorg-$dagorg")));

    return "$dag-$maand-$jaar";
}
*/
function volgendeDag($dagorg, $maandorg, $jaarorg, $plus)
{
    return date("d-m-y", strtotime("+$plus days", strtotime("$jaarorg-$maandorg-$dagorg")));
}

function nlnaarvsDatumVorige($datum, $maand, $jaar)
{
    $deel1 = substr($datum, 0, 2);
    $deel2 = substr($datum, 3, 2);
    $deel3 = substr($datum, 6, 4);

    //Deze maand - aantal maanden eraf.
    $vorigemaand = $deel2 - $maand;

    if ($vorigemaand == 0) {
        $vorigemaand = 12;
        $jaar++;
    } else {
        if ($vorigemaand < 0) {
            $vorigemaand *= -1;
            $vorigemaand = 12 - ($vorigemaand + 1);
            $jaar++;
        }
    }

    //Dit jaar - aantal jaren eraf.
    return ($deel3 - $jaar) . "-" . $vorigemaand . "-" . $deel1;
}

function nlnaarvsDatum($datum)
{
    $deel1 = substr($datum, 0, 2);
    $deel2 = substr($datum, 3, 2);
    $deel3 = substr($datum, 6, 4);
    return $deel3 . "-" . $deel2 . "-" . $deel1;
}

function get_folder_size($target, $output = false)
{
    $totalsize = "";
    if (file_exists($target)) {
        $sourcedir = opendir($target);
        $exceptions = "";
        if ($sourcedir != "") {
            while (false !== ($filename = readdir($sourcedir))) {
                if ($output) {
                    echo "Processing: " . $target . "/" . $filename . "<br>";
                }
                if ($filename != "." && $filename != "..") {
                    if (is_dir($target . "/" . $filename)) {
                        // recurse subdirectory; call of function recursive
                        $totalsize += get_folder_size($target . "/" . $filename, $exceptions);
                    } else {
                        if (is_file($target . "/" . $filename)) {
                            $totalsize += filesize($target . "/" . $filename);
                        }
                    }
                }
            }
        }
        closedir($sourcedir);
    }
    return $totalsize;
}

function datumNummer($dformat, $datum)
{
    $date_parts1 = explode($dformat, $datum);
    $dateNummer = gregoriantojd($date_parts1[1], $date_parts1[0], $date_parts1[2]);
    return $dateNummer;
}

/*
function offerteNaam($offertenr, $bedrijfsid, $toonOokKlant = false, $maxStrLen = 1000)
{
    global $db;
    $naam = "";

    //Offertenaam
    if ($offertenr == "-intern-" || $offertenr == "-ziekte-" || $offertenr == "(Geen)") {
        $naam = $offertenr;
    } else {
        $resultbis = db_query("SELECT * FROM offerte WHERE (OFFERTENR*1) = '" . ps($offertenr, "nr") . "' AND (BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "') LIMIT 1", $db);
        if ($data_offerte = db_fetch_array($resultbis)) {
            $naam = stripslashes($data_offerte["NAAM"]);
            if ($toonOokKlant) {
                $naam = getNaamRelatie($data_offerte["KLANTID"]) . " - " . $naam;
            }
        }
    }

    if (strlen($naam) >= $maxStrLen) {
        $naam = substr($naam, 0, ($maxStrLen - 2)) . "..";
    }

    return $naam;
}
*/
function boekjaaropen($datum, $BEDRIJFSID)
{
    global $pdo;

    $query = $pdo->prepare('select ID from afsluitenboekjaar where DATUMVAN <= :datumvan AND DATUMTOT >= :datumtot AND AFGESLOTEN = "Ja" AND BEDRIJFSID = :bedrijfsid limit 1;');
    $query->bindValue('datumvan', $datum);
    $query->bindValue('datumtot', $datum);
    $query->bindValue('bedrijfsid', $BEDRIJFSID);
    $query->execute();

    return $query->rowCount() <= 0;
}

function dateDiff($dformat, $endDate, $beginDate)
{
    $date_parts1 = explode($dformat, $beginDate);
    $date_parts2 = explode($dformat, $endDate);
    if ($date_parts1[0] != null && $date_parts2[0] != null) {
        $start_date = gregoriantojd($date_parts1[1], $date_parts1[0], $date_parts1[2]);
        $end_date = gregoriantojd($date_parts2[1], $date_parts2[0], $date_parts2[2]);
    }
    return $end_date - $start_date;
}

function dateDiffVS($dformat, $endDate, $beginDate)
{
    if ($endDate == "" || $endDate == "0000-00-00" || $endDate == "00-00-0000" || $beginDate == "" || $beginDate == "0000-00-00" || $beginDate == "00-00-0000") {
        return "0";
    } else {
        $date_parts1 = explode($dformat, $beginDate);
        $date_parts2 = explode($dformat, $endDate);
        $start_date = @gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
        $end_date = @gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
        $diff = $end_date - $start_date;
        return $diff;
    }
}

function isFactuurBuitenland($factNr, $bedrijfsid)
{
    global $pdo;

    $return = false;
    $query = $pdo->prepare('SELECT KLANTID FROM facturen WHERE (FACTUURNR = :factuurnr) AND (BEDRIJFSID = :bedrijfsid) LIMIT 1');
    $query->bindValue('factuurnr', $factNr);
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();
    $data_offerte = $query->fetch(PDO::FETCH_ASSOC);
    if ($data_offerte !== false) {
        $query = $pdo->prepare('SELECT LAND FROM klanten WHERE ID = :id AND NOT BTWNR = "" AND (BEDRIJFSID = :bedrijfsid) LIMIT 1');
        $query->bindValue('id', $data_offerte["KLANTID"]);
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            if ($data["LAND"] != "Nederland" && $data["LAND"] != ""
                && $data["LAND"] != "NEDERLAND"
                && $data["LAND"] != "NL" && $data["LAND"] != "Nl"
                && $data["LAND"] != "nl" && $data["LAND"] != "Netherlands"
                && $data["LAND"] != "The Netherlands") {
                $return = true;
            }
        }
    }

    return $return;
}

/*
function isKlantBuitenland($klantid, $bedrijfsid)
{
    global $db;
    $return = false;

    $resultbis = db_query("SELECT LAND FROM klanten WHERE ID = '" . ps($klantid, "nr") . "' AND NOT BTWNR = '' AND (BEDRIJFSID = ('" . ps($bedrijfsid, "nr") . "')) LIMIT 1", $db);
    if ($data = db_fetch_array($resultbis)) {
        if ($data["LAND"] != "Nederland" && $data["LAND"] != "" && $data["LAND"] != "NEDERLAND" && $data["LAND"] != "NL" && $data["LAND"] != "Nl" && $data["LAND"] != "nl" && $data["LAND"] != "The Netherlands" && $data["LAND"] != "Netherlands") {
            $return = true;
        }
    }
    return $return;
}
*/
/*
function getFactuurIdFromFacNr($factNr, $bedrijfsid)
{
    global $db;
    $return = "";
    $resultbis = db_query("SELECT * FROM facturen WHERE (FACTUURNR = '" . ps($factNr) . "') AND (BEDRIJFSID = ('" . ps($bedrijfsid, "nr") . "')) LIMIT 1", $db);
    if ($data_offerte = db_fetch_array($resultbis)) {
        $return = $data_offerte["ID"] * 1;
    }

    return $return;
}
*/
function toonArray($array)
{
    foreach ($array as $x => $y) {
        echo("<br/>$x: $y");
    }
}

function maakVakjeButton($naam, $link, $plaatje, $aantalVakjesOpEenrij, $target)
{
    global $db;
    echo("\n<td valign=\"middle\" align=\"center\" width=\"" . (100 / $aantalVakjesOpEenrij) . "%\">");

    echo("\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class=\"mainfield\">
              \n<tr>
              \n<td valign=\"middle\" align=\"center\">
              \n<a $target $link>");
    echo("<img alt=\"\" src=\"$plaatje\" border=\"0\" alt=\"$naam\" width=\"35\">");
    echo("\n</a></td></tr>
              \n<tr><td class=\"windowsFileButtonsNorm\" align=\"center\"><a $target $link>$naam</a></td></tr>
              \n</table>");
    echo("</td>");
}

/*
function meestVoorkomendeJaNee($tabel, $veld, $conditie, $bedrijfsid)
{
    global $db;

    $meestvoorkomende = "Ja";

    $result_teller = db_query("SELECT COUNT(" . ps($veld) . ") as TELLER, " . ps($veld) . " FROM " . ps($tabel, "tabel") . " WHERE BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' " . ps($conditie) . " GROUP BY " . ps($veld) . " ORDER BY TELLER DESC LIMIT 1;", $db);
    if (db_num_rows($result_teller) != 0) {
        if ($data_personeel = db_fetch_array($result_teller)) {
            $meestvoorkomende = lb($data_personeel[$veld]);
        }
    }

    return $meestvoorkomende;
}
*/
/*
function meestVoorkomendeWaarde($tabel, $veld, $conditie, $bedrijfsid)
{
    global $db;

    $meestvoorkomende = "";

    $SQL = "SELECT COUNT(" . ps($veld) . ") as TELLER, " . ps($veld) . " FROM " . ps($tabel, "tabel") . " WHERE BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' $conditie GROUP BY " . ps($veld) . " ORDER BY TELLER DESC LIMIT 1;";
    //echo $SQL;
    $result_teller = db_query($SQL, $db);
    if (db_num_rows($result_teller) != 0) {
        if ($data_personeel = db_fetch_array($result_teller)) {
            $meestvoorkomende = lb($data_personeel[$veld]);
        }
    }

    return $meestvoorkomende;
}
*/
/*
function convertUrenToDagen($userid, $invoerUren)
{
    global $db;

    $result5 = db_query("SELECT * FROM personeel WHERE ID = ('" . ps($userid, "nr") . "');", $db);
    $data5 = db_fetch_array($result5);

    $uurPerDaginMinuten = uurPerDaginMinuten(($data5["UURPERWEEK"] * 1), $data5["DAGENPERWEEK"]);
    //echo $uurPerDaginMinuten;

    $dagenDouble = @($invoerUren / $uurPerDaginMinuten);
    $dagen = explode(".", $dagenDouble);

    $output = $dagen[0] . " " . tl("dag(en)");
    if (count($dagen) == 2) {
        $overigdeel = $dagen[1];
        $overigdeel = "0.$overigdeel";
        $overigeuren = rondAf($uurPerDaginMinuten * $overigdeel);
        $uur = floor($overigeuren / 60);
        $min = $overigeuren - ($uur * 60);

        if (strlen($min) == 1) {$min = "0" . $min;}
        $overigeuren = "$uur:$min";

        $output .= " $overigeuren uur";
    }

    return $output;
}
*/
function recursive_remove_directory_db($directory, $bedrijfsid)
{
    global $pdo;

    $hogernivo = getCategorieIDFile($directory, $bedrijfsid);
    if ($hogernivo > 0) {
        //Get Children
        $querySelectCat = $pdo->prepare('SELECT * FROM documentcatagorie WHERE HOGERNIVO = :hogernivo AND BEDRIJFSID = :bedrijfsid limit 1;');
        $querySelectCat->bindValue('hogernivo', $hogernivo);
        $querySelectCat->bindValue('bedrijfsid', $bedrijfsid);
        $querySelectCat->execute();
        $data = $querySelectCat->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            recursive_remove_directory_db($directory . "/" . $data["NAAM"], $bedrijfsid);
        }

        //TODO
        //Verwijder map
        $queryDeleteCat = $pdo->prepare('DELETE FROM documentcatagorie WHERE ID = :id limit 1;');
        $queryDeleteCat->bindValue('id', $hogernivo);
        if ($queryDeleteCat->execute()) {
            $queryDeleteBeheer = $pdo->prepare('DELETE FROM documentbeheer WHERE CATAGORIEID = :catid limit 1;');
            $queryDeleteBeheer->bindValue('catid', $hogernivo);
            $queryDeleteBeheer->execute();
        }
    }
}

function recursive_remove_directory($directory, $bedrijfsid, $empty = false)
{
    // if the path has a slash at the end we remove it here
    if (substr($directory, -1) == '/') {
        $directory = substr($directory, 0, -1);
    }
    // if the path is not valid or is not a directory ...
    if (!file_exists($directory) || !is_dir($directory)) {
        // ... we return false and exit the function
        return false;
        // ... if the path is not readable
    } elseif (!is_readable($directory)) {
        // ... we return false and exit the function
        return false;

        // ... elseif the path is readable
    } else {

        // we open the directory
        $handle = opendir($directory);

        // and scan through the items inside
        while (false !== ($item = readdir($handle))) {
            // if the filepointer is not the current directory
            // or the parent directory
            if ($item != '.' && $item != '..') {
                // we build the new path to delete
                $path = $directory . '/' . $item;

                // if the new path is a directory
                if (is_dir($path)) {
                    // we call this function with the new path
                    recursive_remove_directory($path, $bedrijfsid);

                    // if the new path is a file
                } else {
                    // we remove the file
                    unlink($path);
                }
            }
        }
        // close the directory
        closedir($handle);

        // if the option to empty is not set to true
        if ($empty == false) {
            // try to delete the now empty directory
            if (!rmdir($directory)) {
                // return false if not possible
                return false;
            }
        }
        // return success
        return true;
    }
}

// ------------------------------------------------------------

function witregel($teller)
{
    for ($i = 1; $i <= $teller; $i++) {
        echo("<table border=\"0\"><tr><td height=\"10\"></td></tr></table>");
    }
}

function watisergebeurtEdit($omschrijving, $bedrijfsid, $actie = "", $tabel = "", $itemid = "")
{
    global $pdo;

    $logging = config_logging(".");

    if ($logging == "normal") {
        $queryInsert = $pdo->prepare('INSERT INTO watisergebeurt SET DATUM = "' . date("Y-m-d H:i:s") . '",
                                                    OMSCHRIJVING  = :omschrijving,
                                                    ACTIE         = :actie,
                                                    TABEL         = :tabel,
                                                    ITEMID        = :itemid,
                                                    PERSONEELSLID = :personeelslid,
                                                    IPADRES       = :ipadres,
                                                    BEDRIJFSID    = :bedrijfsid;');
        $self = getMySelf();
        $queryInsert->bindValue('omschrijving', $omschrijving);
        $queryInsert->bindValue('actie', $actie);
        $queryInsert->bindValue('tabel', $tabel);
        $queryInsert->bindValue('itemid', $itemid);
        $queryInsert->bindValue('personeelslid', $self);
        $queryInsert->bindValue('ipadres', $_SERVER["REMOTE_ADDR"]);
        $queryInsert->bindValue('bedrijfsid', $bedrijfsid);
        $queryInsert->execute();

        //Verwijder alles ouder dan 2 maanden
        $pdo->query('delete from watisergebeurt WHERE DATUM < "' . date("Y-m-d",
                strtotime("-2 month", strtotime(date("Y-m-d")))) . '";');
    } else {
        if ($logging == "strong") {
            $loggin_location = config_loggin_location(".");

            $logRegel = "\n['" . date("Y-m-d H:i:s") . "'] '" . ps($actie, "") . "', '" . ps($tabel,
                    "tabel") . "', item: '" . ps($itemid, "nr") . "', Omschrijving: '" . ps($omschrijving,
                    "") . "', PERSONEELSLID: '" . ps(getMySelf(),
                    "nr") . "', IPADRES: '" . ps($_SERVER["REMOTE_ADDR"]) . "', BEDRIJFSID: '" . ps($bedrijfsid,
                    "nr") . "'";

            $file_handle = @fopen($loggin_location, "a+");
            if (!fwrite($file_handle, $logRegel)) {
                echo "Cannot write to file";
            }
            @fclose($file_handle);
        }
    }
}

function verschiltijd($tijdbegin, $tijdeind)
{
    $verschil1 = substr($tijdeind, 0, 2);
    $verschil2 = substr(substr($tijdeind, 3, 5), 0, 2);
    $verschil3 = substr($tijdbegin, 0, 2);
    $verschil4 = substr(substr($tijdbegin, 3, 5), 0, 2);

    $minuten = $verschil2 - $verschil4;
    $uren = ($verschil1 - $verschil3) * 60;
    $verschil = $uren + $minuten;

    return $verschil;
}

function getKolomFromSites($kolom1, $kolom2, $kolom3, $welke)
{
    if ($welke == 1) {
        return $kolom1;
    } else {
        if ($welke == 2) {
            return $kolom2;
        } else {
            if ($welke == 3) {
                return $kolom3;
            }
        }
    }
}

/*
function inkoopbonbedragRef($ref, $bedrijfsid, $metBTW)
{
    global $db;

    $bedrag = 0;
    $sql = "SELECT * FROM kostenintern WHERE BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' AND REFERENTIENR = '" . ps($ref) . "';";
    $result = db_query($sql, $db);
    while ($data = db_fetch_array($result)) {
        $kosten = $data["KOSTEN"];
        if ($metBTW == "Ja") {
            if (ps($data["BTWTARIEF"], "nr") > 0) {
                $kosten *= ($data["BTWTARIEF"] / 100) + 1;
            }
        }
        $bedrag += $kosten;
    }
    return $bedrag;
}
*/
function inkoopfactuurberekenen($KOSTEN, $BTWTARIEF)
{
    $KOSTEN = ps($KOSTEN, "amount");
    $KOSTEN *= 1;
    if (ps($BTWTARIEF, "nr") > 0) {
        $KOSTEN *= ($BTWTARIEF / 100) + 1;
    }
    return prijsconversie($KOSTEN);
}

/*
function inkoopbonbedragFactPlusRef($REFERENTIENR, $FACTUURNRCREDITEUR, $bedrijfsid)
{
    global $db;

    $bedrag = 0;
    $sql = "SELECT * FROM kostenintern WHERE BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' AND REFERENTIENR = '" . ps($REFERENTIENR) . "' AND FACTUURNRCREDITEUR = '" . ps($FACTUURNRCREDITEUR) . "';";
    $result = db_query($sql, $db);
    while ($data = db_fetch_array($result)) {
        $kosten = $data["KOSTEN"];
        if (ps($data["BTWTARIEF"], "nr") > 0) {
            $kosten *= ($data["BTWTARIEF"] / 100) + 1;
        }
        $bedrag += $kosten;
    }
    return $bedrag;
}
*/

function getDocumentKoppelingen($KOPPELING)
{
    $return = "";
    $tmpKoppel = explode("_", $KOPPELING);

    $geenKoppel = "";
    $klanten = "";
    $orderbevestiging = "";
    $offerte = "";
    $polissen = "";
    $contracten = "";
    $dossiers = '';

    if (isset($tmpKoppel[1])) {
        if ($tmpKoppel[1] == "") {
            $geenKoppel = "selected=\"true\"";
        } else {
            if ($tmpKoppel[1] == "klanten") {
                $klanten = "selected=\"true\"";
            } else {
                if ($tmpKoppel[1] == "orderbevestiging") {
                    $orderbevestiging = "selected=\"true\"";
                } else {
                    if ($tmpKoppel[1] == "offerte") {
                        $offerte = "selected=\"true\"";
                    } else {
                        if ($tmpKoppel[1] == "dossiers") {
                            $dossiers = "selected=\"true\"";
                        } else {
                            if ($tmpKoppel[1] == "contracten") {
                                $contracten = "selected=\"true\"";
                            } else {
                                if ($tmpKoppel[1] == "polissen") {
                                    $polissen = "selected=\"true\"";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    $return .= "<option value=\"\" $geenKoppel>Geen koppeling</option>";
    if (isRechten("CONTRACTEN") || $contracten != "") {
        $return .= "<option value=\"contracten\" $contracten>Contracten</option>";
    }
    if (isRechten("ADMINISTRATOR") || $dossiers != "") {
        $return .= "<option value=\"dossiers\" $dossiers>Dossiers</option>";
    }
    if (isRechten("RELATIEBEHEER") || $klanten != "") {
        $return .= "<option value=\"klanten\" $klanten>" . tl("Relatie") . "</option>";
    }
    if (isRechten("VERKOOP") || $orderbevestiging != "") {
        $return .= "<option value=\"orderbevestiging\" $orderbevestiging>Order</option>";
    }
    if (isRechten("VERHUUR") || $polissen != "") {
        $return .= "<option value=\"polissen\" $polissen>Polissen</option>";
    }
    if (isRechten("PROJECTEN") || $offerte != "") {
        $return .= "<option value=\"offerte\" $offerte>" . tl("Project") . "</option>";
    }

    return $return;
}

/*
function landen()
{
    global $db;

    if (taal() == "NL") {
        $output = "\n<option value=\"Albania\">Albania</option><option value=\"Andorra\">Andorra</option>
                \n<option value=\"Angola\">Angola</option><option value=\"Antigua en Barbuda\">Antigua en Barbuda</option>
                \n<option value=\"Argentini�\">Argentini�</option><option value=\"Aruba\">Aruba</option>
                \n<option value=\"Australia\">Australia</option><option value=\"Bahamas\">Bahamas</option>
                \n<option value=\"Barbados\">Barbados</option><option value=\"Belgi�\">Belgi�</option>
                \n<option value=\"Belize\">Belize</option><option value=\"Benin\">Benin</option>
                \n<option value=\"Bermuda\">Bermuda</option><option value=\"Bolivia\">Bolivia</option>
                \n<option value=\"Bosni� en Herzegovina\">Bosni� en Herzegovina</option><option value=\"Brazili�\">Brazili�</option>
                \n<option value=\"Bulgarije\">Bulgarije</option><option value=\"Burkina Faso\">Burkina Faso</option>
                \n<option value=\"Canada\">Canada</option><option value=\"Centraal Afrikaanse Republiek\">Centraal Afrikaanse Republiek</option>
                \n<option value=\"Chad\">Chad</option><option value=\"Chili�\">Chili�</option>
                \n<option value=\"China\">China</option><option value=\"Colombia\">Colombia</option>
                \n<option value=\"Costa Rica\">Costa Rica</option><option value=\"Cyprus\">Cyprus</option>
                \n<option value=\"Denemarken\">Denemarken</option><option value=\"Dominica\">Dominica</option>
                \n<option value=\"Dominicaanse Republiek\">Dominicaanse Republiek</option><option value=\"Duitsland\">Duitsland</option>
                \n<option value=\"Ecuador\">Ecuador</option><option value=\"El Salvador\">El Salvador</option>
                \n<option value=\"Equatorisch Guinea\">Equatorisch Guinea</option><option value=\"Estonia\">Estonia</option>
                \n<option value=\"Finland\">Finland</option><option value=\"Frankrijk\">Frankrijk</option>
                \n<option value=\"Frans Guinea\">Frans Guinea</option><option value=\"Gibraltar\">Gibraltar</option>
                \n<option value=\"Greenland\">Greenland</option><option value=\"Grenada\">Grenada</option>
                \n<option value=\"Griekenland\">Griekenland</option><option value=\"Guadeloupe\">Guadeloupe</option>
                \n<option value=\"Guam\">Guam</option><option value=\"Guatemala\">Guatemala</option>
                \n<option value=\"Guinea\">Guinea</option><option value=\"Guinnee-Bissau\">Guinnee-Bissau</option>
                \n<option value=\"Guyana\">Guyana</option><option value=\"Haiti\">Haiti</option>
                \n<option value=\"Honduras\">Honduras</option><option value=\"Hong Kong\">Hong Kong</option>
                \n<option value=\"Hongarije\">Hongarije</option><option value=\"Ierland\">Ierland</option>
                \n<option value=\"Hong Kong\">Hong Kong</option>
                \n<option value=\"Ijsland\">Ijsland</option><option value=\"India\">India</option>
                \n<option value=\"Indonesia\">Indonesia</option><option value=\"Israel\">Israel</option>
                \n<option value=\"Itali�\">Itali�</option><option value=\"Jamaica\">Jamaica</option>
                \n<option value=\"Japan\">Japan</option><option value=\"Kaaiman Eilanden\">Kaaiman Eilanden</option>
                \n<option value=\"Kaapverdi�\">Kaapverdi�</option><option value=\"Kroati� (Hrvatska)\">Kroati� (Hrvatska)</option>
                \n<option value=\"Lesotho\">Lesotho</option><option value=\"Liberi�\">Liberi�</option>
                \n<option value=\"Liechtenstein\">Liechtenstein</option><option value=\"Lithuani�\">Lithuani�</option>
                \n<option value=\"Litouwen\">Litouwen</option><option value=\"Luxembourg\">Luxembourg</option>
                \n<option value=\"Macedoni�\">Macedoni�</option><option value=\"Madagascar\">Madagascar</option>
                \n<option value=\"Malawi\">Malawi</option><option value=\"Maleisi�\">Maleisi�</option>
                \n<option value=\"Mali\">Mali</option><option value=\"Malta\">Malta</option>
                \n<option value=\"Marshall Eilanden\">Marshall Eilanden</option><option value=\"Martinique\">Martinique</option>
                \n<option value=\"Mauretani�\">Mauretani�</option><option value=\"Mauritius\">Mauritius</option>
                \n<option value=\"Mexico\">Mexico</option><option value=\"Micronesia, Federated States of\">Micronesia, Federated States of</option>
                \n<option value=\"Moldavi�\">Moldavi�</option><option value=\"Monaco\">Monaco</option>
                \n<option value=\"Montserrat\">Montserrat</option><option value=\"Mozambique\">Mozambique</option>
                \n<option value=\"Namibia\">Namibia</option><option value=\"Nederland\" SELECTED>Nederland</option>
                \n<option value=\"Nederlandse Antillen\">Nederlandse Antillen</option><option value=\"Nieuw Zeeland\">Nieuw Zeeland</option>
                \n<option value=\"Noordelijke Mariana Eilanden\">Noordelijke Mariana Eilanden</option><option value=\"Noorwegen\">Noorwegen</option>
                \n<option value=\"Oostenrijk\">Oostenrijk</option><option value=\"Palau\">Palau</option>
                \n<option value=\"Panama\">Panama</option><option value=\"Paraguay\">Paraguay</option>
                \n<option value=\"Peru\">Peru</option><option value=\"Poland\">Poland</option>
                \n<option value=\"Portugal\">Portugal</option><option value=\"Puerto Rico\">Puerto Rico</option>
                \n<option value=\"Russische Federatie\">Russische Federatie</option><option value=\"Saint Lucia\">Saint Lucia</option>
                \n<option value=\"Saint Vincent and The Grenadines\">Saint Vincent and The Grenadines</option><option value=\"Samoa\">Samoa</option>
                \n<option value=\"San Marino\">San Marino</option><option value=\"Seychelles\">Seychelles</option>
                \n<option value=\"Singapore\">Singapore</option><option value=\"Slovakije\">Slovakije</option>
                \n<option value=\"Slovenia\">Slovenia</option><option value=\"Spanje\">Spanje</option>
                \n<option value=\"Spanje - Canarias\">Spanje - Canarias</option><option value=\"St. Kitts en Nevis\">St. Kitts en Nevis</option>
                \n<option value=\"Suriname\">Suriname</option><option value=\"Taiwan, Prov. of China\">Taiwan, Prov. of China</option>
                \n<option value=\"Togo\">Togo</option><option value=\"Trinidad en Tobago\">Trinidad en Tobago</option>
                \n<option value=\"Tsjechi�\">Tsjechi�</option><option value=\"Turkije\">Turkije</option>
                \n<option value=\"Uganda\">Uganda</option><option value=\"Ukraine\">Ukraine</option>
                \n<option value=\"Uruguay\">Uruguay</option><option value=\"Vaticaan\">Vaticaan</option>
                \n<option value=\"Venezuela\">Venezuela</option><option value=\"Verenigd Koninkrijk\">Verenigd Koninkrijk</option>
                \n<option value=\"Verenigde Staten van Amerika\">Verenigde Staten van Amerika</option><option value=\"Vietnam\">Vietnam</option>
                \n<option value=\"Virgin Islands (British)\">Virgin Islands (British)</option><option value=\"Virgin Islands (US)\">Virgin Islands (US)</option>
                \n<option value=\"Zambia\">Zambia</option><option value=\"Zimbabwe\">Zimbabwe</option>
                \n<option value=\"Zuid Afrika\">Zuid Afrika</option><option value=\"Zweden\">Zweden</option>
                \n<option value=\"Zwitzerland\">Zwitzerland</option>";

    } else {
        $result_2 = db_query("SELECT * FROM country ORDER BY printable_name;", $db);
        while ($data_2 = db_fetch_array($result_2)) {
            $output .= "\n<option value=\"" . $data_2["printable_name"] . "\">" . lb($data_2["printable_name"]) . "</option>";
        }
    }

    return $output;

}
*/
function eenheden()
{
    echo("<option value=\"Per stuk\">Per stuk</option>
           <option value=\"Per paar\">Per paar</option>
           <option value=\"Meter\">Meter</option>
           <option value=\"Vierkante meter\">Vierkante meter</option>
           <option value=\"Kubieke meter\">Kubieke meter</option>
           <option value=\"Kilo\">Kilo</option>
           <option value=\"Pond\">Pond</option>
           <option value=\"Liter\">Liter</option>");
}

function browser_info($agent = null)
{
    // Declare known browsers to look for
    $known = array(
        'msie',
        'firefox',
        'safari',
        'webkit',
        'opera',
        'netscape',
        'konqueror',
        'gecko'
    );

    // Clean up agent and build regex that matches phrases for known browsers
    // (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
    // version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
    $agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
    $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';

    // Find all phrases (or return empty array if none found)
    if (!preg_match_all($pattern, $agent, $matches)) {
        return array();
    }

    // Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
    // Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
    // in the UA).  That's usually the most correct.
    $i = count($matches['browser']) - 1;
    return array($matches['browser'][$i] => $matches['version'][$i]);
}

function html2text($str)
{
    return unhtmlentities(preg_replace(
        array(
            "'<(SCRIPT|STYLE)[^>]*?>.*?</(SCRIPT|STYLE)[^>]*?>'si",
            "'(\r|\n)'",
            "'<BR[^>]*?>'i",
            "'<P[^>]*?>'i",
            "'<\/?\w+[^>]*>'e",
        ),
        array(
            "",
            "",
            "\r\n",
            "\r\n\r\n",
            ""
        ),
        $str));
}

function unhtmlentities($string)
{
    $trans_tbl = get_html_translation_table(HTML_ENTITIES);
    $trans_tbl = array_flip($trans_tbl);
    return strtr($string, $trans_tbl);
}

function CheckOfHTMLIs($txt)
{
    $return = false;

    //check of de mail al HTML is
    $mailHTML = str_replace("<br", "", $txt);
    $mailHTML = str_replace("<BR", "", $mailHTML);
    $mailHTML = str_replace("<body", "", $mailHTML);
    $mailHTML = str_replace("<BODY", "", $mailHTML);
    $mailHTML = str_replace("<p", "", $mailHTML);
    $mailHTML = str_replace("<P", "", $mailHTML);
    if (strlen($txt) == strlen($mailHTML)) {
        $return = true;
    }

    return $return;

}

function percentageVDkleur($percentage)
{
    if ($percentage > 100) {
        $percentage = 100;
    } else {
        if ($percentage < 0) {
            $percentage = 0;
        }
    }

    $kleur = "#ffffff"; //wit
    if ($percentage < 5) {
        $kleur = "#fb5656";
    } else {
        if ($percentage < 10) {
            $kleur = "#fb5a5a";
        } else {
            if ($percentage < 20) {
                $kleur = "#fc7474";
            } else {
                if ($percentage < 30) {
                    $kleur = "#fc8d8d";
                } else {
                    if ($percentage < 40) {
                        $kleur = "#fda0a0";
                    } else {
                        if ($percentage < 50) {
                            $kleur = "#fdabab";
                        } else {
                            if ($percentage < 60) {
                                $kleur = "#fdc0c0";
                            } else {
                                if ($percentage < 70) {
                                    $kleur = "#fecccc";
                                } else {
                                    if ($percentage < 80) {
                                        $kleur = "#fed5d5";
                                    } else {
                                        if ($percentage < 90) {
                                            $kleur = "#feeaea"; //rood
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $kleur;
}

function opdehoogtebrengen($tabel, $ID)
{
    $headers = "From: mijn@digitaalkantoor. \nReply-to: $replyto <$replyto>\r\nDisposition-notification-to:" . $replyto . "\r\nContent-type:text/html; charset=iso-8859-1\r\n";
    $inhoud = stripslashes($inhoud);
    @mail($mailto, $subject, $inhoud, $headers); //To customer
}

function checkSelectedItem($WAARDE, $VALUE)
{
    if ($WAARDE == $VALUE) {
        return "style=\"background:#cccccc;\"";
    }
    return "";
}

/*
function getInCache($sleutel, $bedrijfsid)
{
    global $db;

    $return = "";
    $result_2 = db_query("SELECT * from cache where SLEUTEL = '" . ps($sleutel) . "' limit 1;", $db);
    if ($data_2 = db_fetch_array($result_2)) {
        $return = $data_2["WAARDE"];
    }
    return $return;
}
*/
/*
function setInCache($sleutel, $waarde, $bedrijfsid)
{
    global $db;

    $return = "";
    $result_2 = db_query("SELECT * from cache where SLEUTEL = '" . ps($sleutel) . "' limit 1;", $db);
    if ($data_2 = db_fetch_array($result_2)) {
        db_query("update cache set WAARDE = '" . ps($waarde) . "'  where SLEUTEL = '" . ps($sleutel) . "' limit 1;", $db);
    } else {
        db_query("insert into cache set WAARDE = '" . ps($waarde) . "', SLEUTEL = '" . ps($sleutel) . "', BEDRIJFSID = '" . ps($bedrijfsid) . "';", $db);
    }
    return $return;
}
*/
function tegenovergestelde($woord)
{
    if ($woord == "Ja") {
        $woord = "Nee";
    } else {
        if ($woord == "Nee") {
            $woord = "Ja";
        }
    }

    return $woord;
}

function getDag($vanDatum)
{
    $return = "00";
    if ($vanDatum != "") {
        $return = substr($vanDatum, 8, 2);
    }
    return $return;
}

function getMaand($vanDatum)
{
    $return = "00";
    if ($vanDatum != "") {
        $return = substr($vanDatum, 5, 2);
    }
    return $return;
}

function getJaar($vanDatum)
{
    $return = "00";
    if ($vanDatum != "") {
        $return = substr($vanDatum, 0, 4);
    }
    return $return;
}

/**
 * @param string $url       Base url
 * @param int    $results   Total results
 * @param int    $snippet   Results per page
 * @param int    $cur_page  Active page
 * @param array  $options   [ajax|table] ajax is used for shwoing a loader & table is used to paginate a specific table
 *
 * @return string with pagination
 */
function paginate($url = "", $results = 0, $snippet = 0, $cur_page = 0, $options = array())
{
    if (!$url || !$results || $results <= 0 || $results <= $snippet) {
        return false;
    }

    $marge = 5;
    $ajax = false;
    $table = "";
    $waiter = "";

    if (isset($options['ajax']) && isset($options['table']) && $options['table']) {
        $ajax = true;
        $table = $options['table'];
        $waiter = "document.getElementById('contentTable_$table').innerHTML='<center><br/><br/><br/><img src=./images/wait30trans.gif /></center>';";
    }

    $total_pages = ceil(($results / $snippet));

    $paginate = "<span class='paginate " . (($ajax) ? "paginate-ajax" : "paginate-normal") . "'>";

    // Previous
    if ($cur_page > 1) {
        $button_url = $url . "&PAGNR=" . ($cur_page - 1);
        $button_text = "&#60;";
        if (!$ajax) {
            $paginate .= "<a class='cursor' href='$button_url'>$button_text</a>";
        } else {
            $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$button_text</a>";
        }
        $paginate .= "&nbsp;";
    }

    $first_page = $cur_page - $marge;
    if ($first_page < 1) {
        $first_page = 1;
    }

    $last_page = $cur_page + $marge;
    if ($last_page > $total_pages) {
        $last_page = $total_pages;
    }


    //Start
    if ($first_page > 1) {
        for ($p = 1; $p < $first_page; $p += 10) {
//            $p = ($p == 0) ? 1 : $p;
            $button_url = $url . "&PAGNR=$p";
            if (!$ajax) {
                $paginate .= "<a class='cursor' href='$button_url'>$p</a>";
            } else {
                $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$p</a>";
            }
            $paginate .= "&nbsp;";
        }
        $paginate .= " .. ";
    }
    for ($p = $first_page; $p < $cur_page; $p++) {
        $button_url = $url . "&PAGNR=$p";
        if (!$ajax) {
            $paginate .= "<a class='cursor' href='$button_url'>$p</a>";
        } else {
            $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$p</a>";
        }
        $paginate .= "&nbsp;";
    }

    // Midden
    $button_text = ($cur_page > 0) ? "<font color='#f79910' size='3'>" . $cur_page . "</font>" : "1";
    $button_url = $url . "&PAGNR=" . (($cur_page == 0) ? "1" : $cur_page);
    if (!$ajax) {
        $paginate .= "<a class='cursor' href='$button_url'>$button_text</a>";
    } else {
        $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$button_text</a>";
    }
    $paginate .= "&nbsp;";


    //Eind
    for ($p = ($cur_page + 1); $p <= $last_page; $p++) {

        $button_text = ($cur_page == $p) ? "<font color='#f79910' size='3'>" . $p . "</font>" : $p;
        $button_url = $url . "&PAGNR=" . $p;
        if (!$ajax) {
            $paginate .= "<a class='cursor' href='$button_url'>$button_text</a>";
        } else {
            $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$button_text</a>";
        }
        $paginate .= "&nbsp;";
    }

    if ($last_page && $last_page != $total_pages) {
        $deling = ceil($last_page / 10);
        $last_page = $deling * 10;
        $paginate .= " .. ";
        for ($p = $last_page; $p <= $total_pages; $p += 10) {
            $button_url = $url . "&PAGNR=$p";
            if (!$ajax) {
                $paginate .= "<a class='cursor' href='$button_url'>$p</a>";
            } else {
                $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$p</a>";
            }
            $paginate .= "&nbsp;";
        }
    }

    // Next
    if ($cur_page < $total_pages) {
        $button_text = "&#62;";
        $button_url = $url . "&PAGNR=" . ($cur_page + 1);
        if (!$ajax) {
            $paginate .= "<a class='cursor' href='$button_url'>$button_text</a>";
        } else {
            $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$button_text</a>";
        }
        $paginate .= "&nbsp;";
    }

    // Show all
    if ($total_pages > 1) {

        $button_text = ($cur_page == 0) ? "<b><font color=\"#ff5800\" size=\"3\">" . tl("Show all") . "</font></b>" : tl("Show all");
        $button_url = $url . "&PAGNR=0";
        if (!$ajax) {
            $paginate .= "<a class='cursor' href='$button_url'>$button_text</a>";
        } else {
            $paginate .= "<a class='button' onClick=\"$waiter getPageContent2('$button_url', 'contentTable_$table', searchReq2);\">$button_text</a>";
        }
        $paginate .= "&nbsp;";
    }


    return $paginate . "</span>";
}

function algemeneURLToevoegingen()
{
    $algemeneURLToevoegingen = "";

    if (isset($_REQUEST["AUTOCLOSE"])) {
        $algemeneURLToevoegingen .= "&AUTOCLOSE=" . $_REQUEST["AUTOCLOSE"];
    }
    if (isset($_REQUEST["AUTOCLOSE_ID"])) {
        $algemeneURLToevoegingen .= "&AUTOCLOSE_ID=" . $_REQUEST["AUTOCLOSE_ID"];
    }

    if (isset($_REQUEST["POESPAS"])) {
        $algemeneURLToevoegingen .= "&POESPAS=" . $_REQUEST["POESPAS"];
    }

    return $algemeneURLToevoegingen;
}

function toevoegenKlantenSnel()
{
    return toevoegenViaAjax("KLANTID", "content.php?SITE=toevklantensnel");
}

function html_invoerscherm()
{
    return "";
    /*return "<div id=\"achtergrond\" onClick=\"sluiten('invoerscherm');sluiten('achtergrond');document.getElementById('invoerschermContent').innerHTML='';document.body.style.overflow='visible';\" style=\"display:none;\" ></div>
<div id=\"invoerscherm\" class=\"invoerscherm\" style=\"display: none;\">
<div class=\"ajaxBalkSluiten\" align=\"right\"><span class=\"cursor\" onClick=\"sluiten('invoerscherm');sluiten('achtergrond');document.getElementById('invoerschermContent').innerHTML='';document.body.style.overflow='visible';\">".tl("Sluiten")."&nbsp;&nbsp;</span></div><br/>
<div id=\"invoerschermContent\" class=\"invoerschermContent\"></div>
</div>
<div id=\"achtergrond2\" onClick=\"sluiten('invoerscherm2');sluiten('achtergrond2');document.getElementById('invoerschermIframe').src='';document.body.style.overflow='visible';\" style=\"display:none;\" ></div>
<div id=\"invoerscherm2\" class=\"invoerscherm\" style=\"display: none;\">
<div class=\"ajaxBalkSluiten\" align=\"right\"><span class=\"cursor\" onClick=\"sluiten('invoerscherm2');sluiten('achtergrond2');document.getElementById('invoerschermIframe').src='';document.body.style.overflow='visible';\">".tl("Sluiten")."&nbsp;&nbsp;</span></div><br/>
<iframe allowtransparency=\"true\" id=\"invoerschermIframe\" width=\"100%\" height=\"365\" src=\"\" frameborder=\"0\" name=\"invoerschermIframe\"></iframe>
</div>";
 */
}

function toevoegenViaAjax($veld, $url)
{
    $output = "";

    if (!isKlant() && !isset($_REQUEST["AUTOCLOSE"])) {
        if (getPDA()) {
            $output = "<a href=\"$url\"><img src=\"./images/plus.gif\" border=\"0\" /></a>";
        } else {
            $rand = rand();
            $plus = $rand . "plus";
            $divid = $rand . "divid";
            $iframeid = $rand . "iframeid";
            $achtergrond = "achtergrond";

            $titel = "";
            if ($veld == "KLANTID") {
                $titel = "Nieuwe relatie";
            } else {
                if ($veld == "KOSTENPLAATS") {
                    $titel = "Nieuwe kostenplaats";
                } else {
                    if ($veld == "AANMANINGID" || $veld == "AANMANINGIDNEXT") {
                        $titel = "Nieuwe factuurtekst";
                    }
                }
            }

            /*$output = "<a class=\"cursor\" onClick=\"if(document.getElementById('$divid').style.display='none') {
            window.scrollTo(0,0);
            document.getElementById('$iframeid').src='$url&POESPAS=Nee&AUTOCLOSE=$divid&AUTOCLOSE_ID=$veld';
            }
            toggle('$achtergrond');
            togglePlus('$divid', '$plus');
            document.body.style.overflow='hidden';
            if(document.getElementById('".$veld."_name')) {
            document.getElementById('".$veld."_name').value='';
            }\"><img src=\"./images/plus.gif\" id=\"$plus\" border=\"0\" /></a>
            \n<div class=\"selectionboxAjaxproject\" style=\"background: #FFFFFF; padding: 5px; z-index: 10; position: absolute; display:none; top: 70px; left: 50%; margin-left:-450px; width: 900px; height: 400px; border: 1px solid #000000;\" id=\"$divid\">
            \n<div class=\"ajaxBalkSluiten\"><span style=\"float: left;\">$titel</span><span class=\"cursor\" onClick=\"toggle('$achtergrond'); togglePlus('$divid', '$plus');document.body.style.overflow='visible';\">".tl("Sluiten")."&nbsp;&nbsp;</span></div>
            \n<iframe allowtransparency=\"true\" id=\"$iframeid\" width=\"100%\" height=\"365\" src=\"\" frameborder=\"0\" name=\"$iframeid\"></iframe>
            \n</div>";
             */
            $output = "<a class=\"cursor\" onClick=\"window.scrollTo(0,0);
                                                       document.getElementById('invoerschermIframe').src='$url&POESPAS=Nee&AUTOCLOSE=invoerscherm2&AUTOCLOSE_ID=$veld';
                                                       tonen('achtergrond2');
                                                       tonen('invoerscherm2');
                                                       document.body.style.overflow='hidden';
                                                       if(document.getElementById('" . $veld . "_name')) {
                                                         document.getElementById('" . $veld . "_name').value='';
                                                       }\"><img src=\"./images/plus.gif\" id=\"$plus\" border=\"0\" /></a>";

        }
    }
    return $output;
}

function searchSelectionBox(
    $naam,
    $function,
    $userid,
    $bedrijfsid,
    $type,
    $defaultId,
    $defaultValue,
    $extra = "",
    $class = "field searchselect",
    $return = false
) {
    global $pdo;

    if ($defaultValue == "N/A") {
        $defaultValue = "";
    }

    $tabel = "";
    $keyKolom = "";
    $valueKolom = "";
    $aantalResult = 0;

    $basistekst = "";
    if ($type == "land") {
        $basistekst = tl("..type hier het landnaam..");
    } else {
        if ($type == "generateProjectenLijstNormaal" || $type == "projecten") {
            $basistekst = tl("..zoek op project..");
        } else {
            if ($type == "relaties" || $type == "crediteuren") {
                $basistekst = tl("..zoek op relatie..");
            } else {
                if ($type == "contract") {
                    $basistekst = tl("..zoek op contract..");
                } else {
                    if ($type == "producten") {
                        $basistekst = tl("..zoek op product..");
                        $tabel = "producten";
                        $keyKolom = "ID";
                        $valueKolom = "NAAM";
                    } else {
                        if ($type == "stamtabel") {
                            $tabel = $function;
                            $keyKolom = "ID";
                            $valueKolom = "NAAM";
                            $basistekst = tl("..zoek op naam..");
                            $function = "generateSTAMTABELLijst&param=" . $function;
                        } else {
                            if ($type == "grootboekrekeningen" || $type == "grootboekitems") {
                                $basistekst = tl("..zoek op rubriek..");
                            }
                        }
                    }
                }
            }
        }
    }

    if ($defaultValue == "") {
        $defaultValue = $basistekst;
    }

    if ($tabel != "") {
        $tableData = getTableData($tabel);
        if (!empty($tableData)) {

            $query = $pdo->prepare('SELECT COUNT(*) As Aantal from ' . $tabel . ' limit 1;');
            $query->execute();
            $data = $query->fetch(PDO::FETCH_ASSOC);

            if ($data !== false) {
                $aantalResult = $data["Aantal"];
            }
        }
    }

    if ($tabel != "" && $keyKolom != "" && $valueKolom != "" && $aantalResult < 20) {
        $output = "<select class=\"field\" name=\"" . $naam . "\">" . selectbox_tabel($tabel, $keyKolom, $valueKolom,
                $defaultId, true, '') . "</select>";
    } else {
        $classSearchSelection = "selectionboxAjaxproject";
        if ($type == "relaties") {
            if (isset($_REQUEST["SITE"]) && ($_REQUEST["SITE"] == "inkoopboekinvoer" || $_REQUEST["SITE"] == "inkoopboek" || $_REQUEST["SITE"] == "verkoopboek" || $_REQUEST["SITE"] == "toevfacturen")) {
            } else {
                $classSearchSelection = "selectionboxAjax";
            }
        }

        $output = "<input type=\"hidden\" id=\"" . $naam . "\" name=\"" . $naam . "\" value=\"$defaultId\">
                <input type=\"text\" id=\"" . $naam . "_name\" $extra onFocusOut=\"if(document.getElementById('" . $naam . "_name').value.length == 0) { document.getElementById('" . $naam . "_name').value = '$defaultValue'; }\" onClick=\"document.getElementById('" . $naam . "_name').value='';\" name=\"" . $naam . "_name\" value=\"$defaultValue\" class=\"$class\" alt=\"" . tl("Zoeken") . "\" onKeyDown=\"selectWithKeyDown('$naam', document.getElementById('" . $naam . "_name'), '$function', '$basistekst', event);\" onKeyUp=\"selectWithKeyDown('$naam', document.getElementById('" . $naam . "_name'), '$function', '$basistekst', event);\" autocomplete=\"off\" />
                <div id=\"hidden_" . $naam . "\" style=\"display: none;\">0</div>
                <div id=\"" . $naam . "_search_suggest\" class=\"$classSearchSelection\" style=\"display: none;\">";
        if ($type == "land") {
            $output .= generateStamtabelLijstNormaal($naam, "SELECT DISTINCT * FROM country ORDER BY NAAM;", "NAAM",
                "NAAM");
        } else {
            if ($type == "generateProjectenLijstNormaal") {
                $output .= generateProjectenLijstNormaal($userid, $bedrijfsid);
            } else {
                if ($type == "") {

                }
            }
        }
        $output .= "</div>";
    }

    if (!$return) {
        echo $output;
    } else {
        return $output;
    }
}

function createEditor($naam, $value, $width = "", $height = "", $barType = "default")
{
    //echo "\n<script src=\"./ckeditor4.3.1/ckeditor.js\"></script>";
    ob_start();

    $algemeen_editor_gebruiken = check("algemeen_editor_gebruiken", "CKEditor4.1.2", 1);

    if ($height != "") {
        $hoogte = "height: '$height',";
    }

    if ($width != "") {
        $width = "width: '$width',";
    }

    echo "<textarea class=\"ckeditor\" id=\"$naam\" name=\"$naam\" rows=\"5\">" . stripslashes($value) . "</textarea>";

    echo "<script>
  	CKEDITOR.replace( '$naam', {
  	uiColor: '#cccccc',
  	removePlugins: 'elementspath',
  	resize_enabled:false,
  	allowedContent: true,
  	enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P,
	toolbar: [";
    /*
    if ($barType == "small") {
    echo "[ 'Source', 'Bold', 'Italic', 'Underline', 'Strike', 'Link', 'Unlink' ],
    [ 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote' ],
    [ 'SpecialChar', 'FontSize', 'Font', 'TextColor' ]";
    } elseif ($barType == "xssmall") {
    echo "[ 'Bold', 'Italic', 'Underline' ],
    [ 'NumberedList', 'BulletedList', 'Outdent', 'Indent' ],
    [ 'TextColor' ]";

    } elseif ($barType == "full") {
    echo "[ 'Source', '-', 'Cut', 'Copy', 'Paste'],
    [ 'Undo','Redo' ],
    [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript' ],
    [ 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote'],
    [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ],
    [ 'BidiLtr', 'BidiRtl' ],
    [ 'Link','Unlink', 'Image','Table','HorizontalRule','SpecialChar','PageBreak' ],
    [ 'Format','Font','FontSize','TextColor','BGColor' ],
    [ 'Maximize', 'ShowBlocks' ]";

    } else {

    echo "[ 'Source', '-', 'Cut', 'Copy', 'Paste'],
    [ 'Undo','Redo' ],
    [ 'Bold', 'Italic', 'Underline', 'Strike' ],
    [ 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote'],
    [ 'JustifyLeft','JustifyCenter','JustifyRight' ],
    [ 'Link','Unlink', 'Image','Table','HorizontalRule','SpecialChar','PageBreak' ],
    [ 'Format','FontSize','TextColor','BGColor' ],
    [ 'Maximize', 'ShowBlocks' ]";
    }
     */

    echo "[ 'Bold', 'Italic', 'Underline' ],
	  [ 'NumberedList', 'BulletedList', 'Outdent', 'Indent' ],
	  [ 'TextColor' ]";

    echo " ],
    $width
    $hoogte
  });
  </script>";

    $html = ob_get_contents();
    ob_end_clean();

    return $html;
}

function activeKleur()
{
    //#cccccc
    return "#206bb4";
}

function kleuren()
{
    $kleuren = array(
        "oranje" => getColor("oranje"),
        "rood" => getColor("rood"),
        "groen" => getColor("groen"),
        "grijs" => getColor("grijs"),
        "blauw" => getColor("blauw")
    );
    return $kleuren;
}

function getColor($actie)
{
    if ($actie == "On") {
        return "#f0f0f0";
    } else {
        if ($actie == "Off") {
            return "#ffffff";
        } else {
            if ($actie == "Aangemaakt") {
                return "#fc4d08";
            } else {
                if ($actie == "Doorgestuurd") {
                    return "#ffd9a5";
                } else {
                    if ($actie == "Goedgekeurd") {
                        return "#c5ecb3";
                    } else {
                        if ($actie == "oranje") {
                            return "#ffd9a5";
                        } else {
                            if ($actie == "rood") {
                                return "#fc4d08";
                            } else {
                                if ($actie == "groen") {
                                    return "#c5ecb3";
                                } else {
                                    if ($actie == "grijs") {
                                        return "#979797";
                                    } else {
                                        if ($actie == "blauw") {
                                            return "#c7e1fb";
                                        } else {
                                            if ($actie == "bug_Afgesloten") {
                                                return "#fc4d08";
                                            } else {
                                                if ($actie == "bug_Nieuw") {
                                                    return "#c5ecb3";
                                                } else {
                                                    if ($actie == "bug_Opgelost") {
                                                        return "#b87eff";
                                                    } else {
                                                        if ($actie == "bug_Feedback") {
                                                            return "#f1fab4";
                                                        } else {
                                                            if ($actie == "bug_Wens") {
                                                                return "#ffb0ac";
                                                            } else {
                                                                if ($actie == "bug_Toegewezen") {
                                                                    return "#75e5ff";
                                                                } else {
                                                                    if ($actie == "bug_Geescaleerd") {
                                                                        return "#dec3cd";
                                                                    } else {
                                                                        if ($actie == "nivo2") {
                                                                            return "#4c5165";
                                                                        } else {
                                                                            if ($actie == "nivo2_selected") {
                                                                                return "#9e9dab";
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return "#ffffff";
}

function getTRAchtergrondkleur($teller)
{
    if (($teller % 2) == 0) {
        return getColor("On");
    } else {
        return getColor("Off");
    }
}

function verkort($naam, $lengte)
{
    if (isset($_REQUEST["EXPORTEXCEL"])) {
    } else {
        if (strlen($naam) > $lengte) {
            $naam = substr($naam, 0, ($lengte - 2)) . "..";
        }
    }
    return lb($naam);
}

function getUserTaal($userid)
{
    global $db, $config;

    $taal = "NL";
    $result = db_query("SELECT * FROM personeel where ID = '" . ps($userid, "nr") . "' limit 1;", $db);
    if ($data = db_fetch_array($result)) {
        if (isset($data["TAAL"])) {
            $config["LANGUAGE"] = $data["TAAL"];
            $taal = $data["TAAL"];
        }
    }

    return $taal;
}

function getConfiguratie($userid, $bedrijfsid)
{
    global $pdo, $config;

    $query = $pdo->prepare('SELECT * FROM standaardwaarde;');
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        $config["CONFIG"][$data["NAAM"]] = $data["WAARDE"];
    }

    $queryBedrijf = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id LIMIT 1;');
    $queryBedrijf->bindValue('id', intval($bedrijfsid));
    $queryBedrijf->execute();
    $data = $queryBedrijf->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        $config["CONFIG"]["DEFAULTDIR"] = $data["DEFAULTDIR"];
    }

    $queryTaal = $pdo->prepare('SELECT * FROM taal;');
    $queryTaal->execute();

    foreach ($queryTaal->fetchAll() as $data) {
        $config["TAAL"][$data["REF"]] = $data[taal()];
    }

    getUserTaal($userid, $bedrijfsid);

    /*$result = db_query("SELECT * FROM dashboard where PERSONEELSLID = '".ps($userid, "nr")."' AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."';", $db);
    while($data= db_fetch_array($result))
    {
    $config["DASHBOARD"][$data["DASHBOARD"]][$data["FUCNTIE"]]["AANUIT"] = $data["AANUIT"];
    $config["DASHBOARD"][$data["DASHBOARD"]][$data["FUCNTIE"]]["GROOTKLEIN"] = $data["GROOTKLEIN"];
    }*/
    $queryLogin = $pdo->prepare('SELECT * FROM login where PERSONEELSLID = :personeelslid AND BEDRIJFSID = :bedrijfsid limit 1;');
    $queryLogin->bindValue('personeelslid', $userid);
    $queryLogin->bindValue('bedrijfsid', $bedrijfsid);
    $queryLogin->execute();
    $dataLogin = $queryLogin->fetch(PDO::FETCH_ASSOC);

    if ($dataLogin !== false) {
        foreach ($dataLogin as $k => $v) {
            if ($k != "ID" || $k != "USERNAME" || $k != "PASSWORD" || $k != "PROFIEL" || $k != "BEDRIJFSID") {
                $config["RECHTEN"][$k] = $v;
            }
        }
    }

    return $config;
}

function stringToTime($value)
{
    $value = str_replace(" ", "", $value);
    $tmpExpl = explode(":", $value);
    if (strlen($value) == 1) {
        $value = "0$value:00";
    } else {
        if (count($tmpExpl) > 1) {
            if (strlen($tmpExpl[0]) == 0) {
                $tmpExpl[0] = "00";
            } else {
                if (strlen($tmpExpl[0]) == 1) {
                    $tmpExpl[0] = "0" . $tmpExpl[0];
                }
            }

            if (strlen($tmpExpl[1]) == 0) {
                $tmpExpl[1] = "00";
            } else {
                if (strlen($tmpExpl[1]) == 1) {
                    $tmpExpl[1] = $tmpExpl[1] . "0";
                } else {
                    if (strlen($tmpExpl[1]) > 2) {
                        $tmpExpl[1] = substr($tmpExpl[1], 0, 2);
                    }
                }
            }

            $value = $tmpExpl[0] . ":" . $tmpExpl[1];
        } else {
            $value = "$value:00";
        }
    }

    return $value;
}

function getKolomNaamContactPersoon($tabel)
{
    $naam = "CONTACTPERS";
    return $naam;
}

/*
function toonAdres($klantId, $contactId)
{
    global $db;

    $output = "";
    $dhrMevr = "dhr/mevr";
    $titeletuur = "";

    $result = db_query("SELECT * FROM klanten WHERE ID = '" . ps($klantId, "nr") . "' limit 1;", $db);
    if ($data_klant = db_fetch_array($result)) {
        $telvast = $data_klant["TELVAST"];
        $mobiel = $data_klant["MOB"];
        $email = $data_klant["EMAIL"];

        if ($contactId > 0) {
            $result_contact = db_query("SELECT * FROM contactpersoon WHERE ID = '" . $contactId . "' limit 1;", $db);
            if ($data_contact = db_fetch_array($result_contact)) {
                $telvast = $data_contact["TELVAST"];
                $mobiel = $data_contact["MOB"];
                $email = $data_contact["EMAIL"];

                //titeletuur
                $titeletuur = $data_contact["TITEL"];
                $dhrMevr = "dhr/mevr";
                if ($data_contact["MANVROUW"] == "Vrouw") {
                    $dhrMevr = "mevr.";
                } elseif ($data_contact["MANVROUW"] == "Man") {
                    $dhrMevr = "dhr.";
                }
            }
        } else {
            $titeletuur = $data_klant["TITEL"];
            if ($data_klant["MANVROUW"] == "Vrouw") {
                $dhrMevr = "mevr.";
            } elseif ($data_klant["MANVROUW"] == "Man") {
                $dhrMevr = "dhr.";
            }
        }

        $titeletuurVoor = "";
        $titeletuurNa = "";
        if ($titeletuur == "Ir." || $titeletuur == "Ds." || $titeletuur == "Dr." || $titeletuur == "Drs." || $titeletuur == "Ing." || $titeletuur == "Prof.") {
            $titeletuurVoor = "$titeletuur ";
        } else {
            $titeletuurNa = " " . $titeletuur;
        }

        $contactpersoonnaam = "";
        if ($contactId > 0) {
            $tussenvoegsell = "";
            if ($data_contact["TUSSENVOEGSEL"] != "") {$tussenvoegsell = " " . $data_contact["TUSSENVOEGSEL"];}
            $contactpersoonnaam = " $titeletuurVoor" . $data_contact["VOORNAAM"] . $tussenvoegsell . " " . $data_contact["ACHTERNAAM"] . $titeletuurNa;
        } elseif ($data_klant["ACHTERNAAM"] != "") {
            $tussenvoegsell = "";
            if ($data_klant["TUSSENVOEGSEL"] != "") {$tussenvoegsell = " " . $data_klant["TUSSENVOEGSEL"];}
            $contactpersoonnaam = " $titeletuurVoor" . $data_klant["VOORNAAM"] . $tussenvoegsell . " " . $data_klant["ACHTERNAAM"] . $titeletuurNa;
        }
        $contactpersoonnaam = str_replace("  ", " ", $contactpersoonnaam);

        $postcode = "";
        if ($contactId > 0 && $data_contact["ADRES"] != "") {
            $adres = $data_contact["ADRES"] . " " . $data_contact["HUISNR"] . " " . $data_contact["TOEVOEGING"];
            $postcodePlaats = $data_contact["POSTCODE"] . " " . $data_contact["PLAATS"];
            $postcode = $data_contact["POSTCODE"];
        } elseif ($data_klant["ADRES"] != "") {
            $adres = $data_klant["ADRES"] . " " . $data_klant["HUISNR"] . " " . $data_klant["TOEVOEGING"];
            $postcodePlaats = $data_klant["POSTCODE"] . " " . $data_klant["PLAATS"];
            $postcode = $data_klant["POSTCODE"];
        } else {
            $adres = $data_klant["KANTOORADRES"] . " " . $data_klant["KANTOORHUISNR"] . " " . $data_klant["KANTOORTOEVOEGING"];
            $postcodePlaats = $data_klant["KANTOORPOSTCODE"] . " " . $data_klant["KANTOORPLAATS"];
            $postcode = $data_klant["KANTOORPOSTCODE"];
        }

        if ($telvast != "") {$telvast = "T: $telvast";}
        if ($mobiel != "") {$mobiel = " M: $mobiel";}
        if ($email != "") {$email = " E: $email";}

        if ($telvast != "" || $mobiel != "" || $email != "") {
            $telContact = "<br/>" . $telvast . $mobiel . $email;
        }

        $route = "";
        if ($postcode != "") {
            $result_bedrijf = db_query("SELECT POSTCODE FROM bedrijf WHERE ID = '" . ps(getBedrijfsid(getLoginId()), "nr") . "' AND NOT POSTCODE = '' limit 1;", $db);
            if ($data_bedrijf = db_fetch_array($result_bedrijf)) {
                $route = " <a target=\"_blank\" href=\"http://maps.google.nl/maps?hl=nl&amp;q=van " . str_replace(" ", "", $data_bedrijf["POSTCODE"]) . " naar " . str_replace(" ", "", $postcode) . "\"><i>" . tl("Toon routebeschrijving") . " &#187;</i></a>";
            }
        }

        $klantenkaart = "";
        if ($_REQUEST["SITE"] != "activiteitToev") {
            $klantenkaart = "<a href=\"content.php?SITE=klantenweb&TYPE=Contact&KLANTID=$klantId\"><i>Toon klantenkaart &#187;</a>";
        }

        if ($klantenkaart != "" || $route != "") {
            $klantenkaart = "<br/><br/>" . $klantenkaart . $route;
        }

        if (trim($adres) != "") {
            $adres = "<br/>" . trim($adres);
        }
        if (trim($postcodePlaats) != "") {
            $postcodePlaats = "<br/>" . trim($postcodePlaats);
        }

        $output = $contactpersoonnaam . $adres . $postcodePlaats . $telContact . $klantenkaart;

    }

    return $output;
}
*/
function grootboek_types()
{
    $types = array(
        10 => "Cash",
        12 => "Bank",
        20 => "Accounts receivable",
        22 => "Accounts payable",
        24 => "VAT",
        25 => "Employees payable",
        26 => "Prepaid expenses",
        27 => "Accrued expenses",
        29 => "Income tax payable",
        30 => "Fixed assets",
        32 => "Other assets",
        35 => "Accumulated depreciations",
        40 => "Inventory",
        50 => "Capital stock",
        52 => "Retained earnings",
        55 => "Long term debt",
        60 => "Current portion of debt",
        90 => "General",
        110 => "Revenue",
        111 => "Cost of goods",
        120 => "Other costs",
        121 => "Sales, marketing general expenses",
        122 => "Depreciation costs",
        123 => "Research and development",
        125 => "Employee costs",
        130 => "Exceptional costs",
        140 => "Exceptional income",
        150 => "Income taxes",
        160 => "Interest income"
    );
    return $types;
}

/*
function getContactPersoon($klantid, $contactId)
{
    global $db;

    $contactPersoon = "";

    if (($contactId * 1) > 0) {
        $sqlContactPers = "select VOORNAAM, TUSSENVOEGSEL, ACHTERNAAM from contactpersoon WHERE ID = '" . ps($contactId, "nr") . "' limit 1;";
        $resultContactPers = db_query($sqlContactPers, $db);
        if ($dataContactPers = db_fetch_array($resultContactPers)) {
            $contactPersoon = $dataContactPers["VOORNAAM"] . " " . $dataContactPers["TUSSENVOEGSEL"] . " " . $dataContactPers["ACHTERNAAM"];
        }
    } else {
        $sqlContactPers = "select VOORNAAM, TUSSENVOEGSEL, ACHTERNAAM from klanten WHERE ID = '" . ps($klantid, "nr") . "' limit 1;";
        $resultContactPers = db_query($sqlContactPers, $db);
        if ($dataContactPers = db_fetch_array($resultContactPers)) {
            $contactPersoon = $dataContactPers["VOORNAAM"] . " " . $dataContactPers["TUSSENVOEGSEL"] . " " . $dataContactPers["ACHTERNAAM"];
        }
    }

    return $contactPersoon;
}
*/
/*
function opslaanValue($tabel, $kolom, $value, $ID)
{
    global $db;

    $sql = "update " . ps($tabel, "tabel") . " set `" . ps($kolom) . "` = '" . ps($value) . "' where ID = '" . ps($ID, "nr") . "';";
    db_query($sql, $db);
}
*/
/*
function checkAantal($bedrijfsid, $tabel, $voorwaarde)
{
    global $db;

    db_query("SELECT SQL_CALC_FOUND_ROWS DISTINCT " . ps($tabel, "tabel") . ".ID FROM " . ps($tabel) . " $voorwaarde LIMIT 1", $db);
    $result = db_query("SELECT FOUND_ROWS()", $db);
    $totaalAantalRegels = db_fetch_row($result);
    return $totaalAantalRegels[0];
}
*/

function getNieuwNr($tabel, $kolom, $bedrijfsid)
{
    global $pdo;

    $nr = date("Y") . "001";

    $tableData = getTableData($tabel);
    if (!isset($tableData[$kolom])) {
        return;
    }

    $query = $pdo->prepare('SELECT `' . $kolom . '` FROM `' . $tabel . '` WHERE BEDRIJFSID = :bedrijfsid ORDER BY (' . $kolom . '*1) DESC;');
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();
    $data_bon = $query->fetch(PDO::FETCH_ASSOC);

    if ($data_bon !== false) {
        if (isset($data_bon[$kolom])) {
            $nr = $data_bon[$kolom];
            $nr = $nr + 1;
        }
    }
    return $nr;
}

/*
function checkBestaatAl($tabel, $kolom, $waarde)
{
    global $db;

    $Id = 0;
    $SQL = "SELECT ID FROM `" . ps($tabel, "tabel") . "` WHERE `" . ps($kolom) . "` = '" . ps($waarde) . "' limit 1;";
    $result = db_query($SQL, $db);
    if ($row = db_fetch_array($result)) {
        $Id = $row["ID"];
    }
    return $Id;
}
*/
function leeftijd($verjaardag)
{
    return floor((dateDiffVS("-", date("Y-m-d"), $verjaardag) / 365));
}

function selectRadioOptions($naam, $default, $waarde, $options)
{

    if ($waarde != '') {
        $default = $waarde;
    }

    $return = "";
    foreach ($options as $kolom => $value) {
        $checked = "";
        if ($default == $value) {
            $checked = "checked";
        }
        $return .= "<input type=\"radio\" name=\"" . $naam . "\" value=\"$value\" $checked> " . lb($value) . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    }

    return $return;
}

function selectjanee($naam, $default)
{
    $checkedJa = "";
    $checkedNee = "";
    if ($default == "Ja") {
        $checkedJa = "checked";
    } else {
        $checkedNee = "checked";
    }

    return "<input type=\"radio\" name=\"" . $naam . "\" value=\"Ja\" $checkedJa> " . tl("Ja") . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"radio\" name=\"" . $naam . "\" value=\"Nee\" $checkedNee> " . tl("Nee");
}

function selectbox($array, $selectedItem, $emptyItem)
{
    $content = "";

    if ($emptyItem && !isset($_REQUEST["PRINT"])) {
        $content .= "\n<option value=\"\"></option>";
    }

    foreach ($array as $key => $value) {
        $selected = "";
        if ($selectedItem == $key) {
            $selected = " selected=\"selected\"";
        }
        if (isset($_REQUEST["PRINT"])) {
            if ($selected != "") {
                $content .= $value;
            }
        } else {
            $content .= "<option value=\"$key\"$selected>" . lb($value) . "</option>";
        }
    }

    return $content;
}

function selectbox_tabel($tabel, $keyKolom, $valueKolom, $selectedItem, $emptyItem, $where)
{
    global $pdo;

    $content = "";
    $tableData = getTableData($tabel);

    if ($emptyItem) {
        $content .= "\n<option value=\"\"></option>";
    }

    if ($where == "N/A") {
        $where = "";
    }

    // check if keyKolom exists
    if (!isset($tableData[$keyKolom])) {
        return;
    }

    // check for multiple fields in $valueKolom exists
    $columns = explode(",", $valueKolom);
    foreach ($columns as $col) {
        if (!isset($tableData[trim($col)])) {
            return;
        }
    }

    //TODO : make save solution
    $query = $pdo->prepare('SELECT ' . $keyKolom . ', ' . $valueKolom . ', BEDRIJFSID FROM ' . $tabel . ' ' . $where . ' ORDER BY ' . $valueKolom . ';');
    $query->execute();

    foreach ($query->fetchAll() as $data_2) {
        if (($tabel == "bedrijf" && isRechtenOFT($data_2["BEDRIJFSID"], "CRM")) || $tabel != 'bedrijf') {
            $res = "";
            $kols = explode(", ", $valueKolom);
            foreach ($kols as $key => $kol) {
                if (trim($data_2[$kol]) != "") {
                    if ($kol == "USERID") {
                        $res .= getPersoneelslidNaam($data_2[$kol]) . " ";
                    } else {
                        $res .= lb($data_2[$kol]) . " ";
                    }
                }
            }

            if ($res == "") {
                $res = tl("&hellip;zonder naam&hellip;");
            }

            $selected = "";
            if ($keyKolom == "ID") {
                if (($selectedItem * 1) == ($data_2[$keyKolom] * 1)) {
                    $selected = "selected=\"true\"";
                }
            } else {
                if ($selectedItem == $data_2[$keyKolom]) {
                    $selected = "selected=\"true\"";
                }
            }
            $content .= "\n<option value=\"" . $data_2[$keyKolom] . "\" $selected>" . lb($res) . "</option>";
        }
    }
    return $content;
}

function label($tekst)
{
    return ucfirst(stripslashes(tl($tekst)));
}

function getBestandExtentie($bestandsnaam)
{
    $name = explode(".", $bestandsnaam);
    $ext = "";
    if (isset($name[(count($name) - 1)])) {
        $ext = $name[(count($name) - 1)];
    }
    return strtolower($ext);
}

function uniformFileName($table, $DId)
{
    global $pdo;

    $name = "";

    $query = $pdo->prepare('SELECT BESTANDSNAAM, BESTANDTYPE, BESTANDSIZE, BESTANDCONTENT, BEDRIJFSID, JAAR, DATUM, DOCTYPE FROM ' . $table . ' WHERE ID = :id limit 1;');
    $query->bindValue('id', $DId);
    $query->execute();
    $result = $query->fetch(PDO::FETCH_NUM);

    if ($result !== false) {
        list($name, $type, $size, $content, $bedrijfsid, $JAAR, $DATUM, $DOCTYPE) = $result;

        //Uniform naming of the files
        if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
            if ($JAAR == '') {
                $JAAR = substr($DATUM, 0, 4);
            }

            $query = $pdo->prepare('SELECT BEDRIJFSNAAM FROM bedrijf WHERE ID = :bedrijfsid limit 1;');
            $query->bindValue('bedrijfsid', $bedrijfsid);
            $query->execute();
            $bedrijf = $query->fetch(PDO::FETCH_ASSOC);

            $BEDRIJFSNAAM = $bedrijf["BEDRIJFSNAAM"];

            $EXT = getBestandExtentie($name);

            $name = $JAAR . "_" . $BEDRIJFSNAAM . "_" . $DOCTYPE;
            $name = str_replace(".", "_", $name);
            $name = bestandsnaam_clean($name) . "." . $EXT;
            $name = str_replace("__", "_", $name);
            $name = str_replace("__", "_", $name);
        }
    }

    return $name;
}

function bestandsnaam_clean($bestandnaam)
{
    $bestandnaam = lb($bestandnaam);
    $bestandnaam = strtolower($bestandnaam);

    $bestandnaam = str_replace("  ", " ", $bestandnaam);
    $bestandnaam = str_replace(" ", "_", $bestandnaam);
    $bestandnaam = str_replace(array('"', "'", ' ', ','), '_', $bestandnaam);

    $bestandnaam = str_replace("\\", "", $bestandnaam);
    $bestandnaam = str_replace("\"", "", $bestandnaam);

    $bestandnaam = str_replace("'", "", $bestandnaam);
    $bestandnaam = str_replace("`", "", $bestandnaam);
    $bestandnaam = str_replace("~", "", $bestandnaam);
    $bestandnaam = str_replace(" ", "_", $bestandnaam);
    $bestandnaam = str_replace(":", "", $bestandnaam);
    $bestandnaam = str_replace(";", "", $bestandnaam);
    $bestandnaam = str_replace("[", "", $bestandnaam);
    $bestandnaam = str_replace("]", "", $bestandnaam);
    $bestandnaam = str_replace("{", "", $bestandnaam);
    $bestandnaam = str_replace("}", "", $bestandnaam);
    $bestandnaam = str_replace(")", "", $bestandnaam);
    $bestandnaam = str_replace("(", "", $bestandnaam);
    $bestandnaam = str_replace("(", "", $bestandnaam);
    $bestandnaam = str_replace("*", "", $bestandnaam);
    $bestandnaam = str_replace("&", "", $bestandnaam);
    $bestandnaam = str_replace("^", "", $bestandnaam);
    $bestandnaam = str_replace("%", "", $bestandnaam);
    $bestandnaam = str_replace("$", "", $bestandnaam);
    $bestandnaam = str_replace("@", "", $bestandnaam);
    $bestandnaam = str_replace("#", "", $bestandnaam);

    $bestandnaam = str_replace("�", "e", $bestandnaam);
    $bestandnaam = str_replace("�", "a", $bestandnaam);
    $bestandnaam = str_replace("�", "u", $bestandnaam);
    $bestandnaam = str_replace("�", "i", $bestandnaam);
    $bestandnaam = str_replace("�", "o", $bestandnaam);

    if ($bestandnaam == '') {
        $bestandnaam = "document";
    }

    return $bestandnaam;
}


/**
 * @param $pk            //source id (pk)
 * @param $primary_table //source table
 * @param $fk_field      //source foreign id (fk)
 * @param $foreign_table //foreign table
 * @param $field         //foreign table field
 *
 * @return bool|mixed|string
 */
function getPivotValue($pk, $primary_table, $fk_field, $foreign_table, $field)
{
    if (!$pk || !$primary_table || !$fk_field || !$foreign_table || !$field) {
        return false;
    }
    global $pdo;

    $pk = $pdo->quote($pk);
    $primary_table = preg_replace('/[^A-Za-z0-9_]+/', '', $primary_table);
    $fk_field = preg_replace('/[^A-Za-z0-9_]+/', '', $fk_field);
    $foreign_table = preg_replace('/[^A-Za-z0-9_]+/', '', $foreign_table);
    $field = preg_replace('/[^A-Za-z0-9_]+/', '', $field);

    $statement = "SELECT f.$field FROM $primary_table AS p JOIN $foreign_table AS f ON f.ID = p.$fk_field WHERE p.ID = " . $pk;
    if(isset($_REQUEST['SITE']) && ($_REQUEST['SITE'] == "entity_addressbook" || $_REQUEST['SITE'] == "entity_addressbook_ajax") && $fk_field == "ID") {
        $statement = "SELECT f.$field FROM $primary_table AS p JOIN $foreign_table AS f ON f.ENTITY_ID = p.$fk_field WHERE p.ID = " . $pk;
    }
    $query = $pdo->prepare($statement);
    if ($query->execute()) {
        $results = $query->fetch(PDO::FETCH_ASSOC);
        if ($results && $results != array($field => '')) {
            return $results;
        }
    }
    return '';
}

function getPivotValueFromLinkTable($pk, $primary_table, $fk_field, $foreign_table, $field, $extraKey)
{
    if (!$pk || !$primary_table || !$fk_field || !$foreign_table || !$field) {
        return false;
    }
    global $pdo;

    $pk = $pdo->quote($pk);
    $primary_table = preg_replace('/[^A-Za-z0-9_]+/', '', $primary_table);
    $fk_field = preg_replace('/[^A-Za-z0-9_]+/', '', $fk_field);
    $foreign_table = preg_replace('/[^A-Za-z0-9_]+/', '', $foreign_table);
    $field = preg_replace('/[^A-Za-z0-9_]+/', '', $field);
    $extraKey = preg_replace('/[^A-Za-z0-9_]+/', '', $extraKey);

    $statement = "SELECT p.NAME FROM $primary_table AS p JOIN $foreign_table AS f ON f.$fk_field = p.ID WHERE f.$extraKey = " . $pk;
    $query = $pdo->prepare($statement);
    if ($query->execute()) {
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($results && $results != array($field => '')) {
            return $results;
        }
    }
    return '';
}


function getNotificatieStatus($args = array('status', 'due_date', 'table', 'table_id'))
{
    global $pdo;

    $default_status = isset($args['status']) ? strtolower($args['status']) : "";
    $status = '';
    $table = isset($args['table']) ? ps($args['table']) : false;
    $table_id = isset($args['table_id']) ? ps($args['table_id']) : false;

    $file_exists = 0;
    $file_upload_date = null;
    $due_date = isset($args['due_date']) ? $args['due_date'] : null;
    $today = date("Y-m-d");
    $days_left = dateDiffVS("-", $due_date, $today);
    if ($table && $table_id) {
        $statement = "SELECT * FROM " . ps($table) . " WHERE ID = " . ps($table_id,
                'nr') . " AND DELETED != 'Ja' LIMIT 1";
        $getRow = $pdo->prepare($statement);
        $getRow->execute();
        $row = $getRow->fetch();

        $status = isset($row['STATUS']) ? strtolower($row['STATUS']) : '';

        $year = $row['FINANCIALYEAR'];
        $company = $row['BEDRIJFSID'];

        $sql = " AND DELETED != 'Ja'";

        if ($year) {
            $sql .= " AND JAAR = '$year'";
        }
        $sql .= " AND TABEL = '" . ps($table) . "'";

        $doctype = false;
        if ($table == "oft_tax") {
            $doctype = 'Tax return';
        }
        if ($table == "oft_annual_accounts") {
            $doctype = 'Annual accounts';
        }
        if ($doctype) {
            $update_sql = "WHERE DELETED != 'Ja' AND BEDRIJFSID = $company AND JAAR = $year";
            $update_sql .= " AND DOCTYPE = '$doctype'";

            $update_sql .= " AND (TABELID IS NULL OR TABELID = 0 OR TABELID = '' OR TABELID = '-ID-')";
            if (getAantal("documentbeheer", $update_sql)) {
                $statement = "UPDATE documentbeheer SET TABEL = '$table', TABELID = $table_id " . $update_sql;
                $query = $pdo->prepare($statement);
                $query->execute();
            }
        }

        // quantity of uploaded files
        $file_exists = getAantal("documentbeheer", "WHERE TABELID = $table_id" . $sql);

        if (!$file_upload_date = $row['FILINGDATE']) {
            // Date of updating file row
            $file_upload_date = getValue("documentbeheer", "TABELID", $table_id, "UPDATEDATUM", $sql);
        }
    }

    $file_required = ($status !== "not required" && $default_status !== "not required");

    // File not required
    // Due date longer than 30 days
    $color = "white";
    if ($file_required) {
        if ($days_left <= 30) {
            // File must be uploaded within 30 days
            $color = "orange";
        }
        if ($days_left <= 14) {
            // File must be uploaded within 14 days
            $color = "red";
        }
        if ($due_date < $today) {
            // Too late
            $color = "black";
        }
        if ($file_exists) {
            // File uploaded in time
            $color = "green";
            if ($due_date < $file_upload_date) {
                // Didn't make due date in time.
                $color = "black";
            }
        }
    }

    return '<img src="./images/oft/oft_' . $color . '_circle.png" border="0" />';
}

function getUser($id = false)
{
    global $pdo;

    if (!$id) {
        $id = getValidatedLoginID();
    }

    $statement = "SELECT * FROM login JOIN personeel on login.PERSONEELSLID = personeel.ID WHERE login.ID = :id LIMIT 1";
    $prepare = $pdo->prepare($statement);
    $prepare->bindValue('id', $id);

    if (!$prepare->execute()) {
        return 'No results';
    }

    $user = $prepare->fetch(PDO::FETCH_NAMED);

    if(!$user) {
        return $user;
    }
    foreach ($user as $key => $value) {
        if (!is_array($value)){
            continue;
        }
        $new_key = 'PERSONEEL_'.$key;
        if (key_exists($new_key, $user)) {
            continue;
        }
        $user[$key] = $value[0];
        $user[$new_key] = $value[1];
    }

    return $user;
}

function hasRights($role = '', $userId = false)
{
    $availableRoles = ['master', 'administrator', 'directie', 'relationship manager'];

    if (!$role || !in_array(strtolower($role), $availableRoles)) {
        return false;
    }

    $user = getUser($userId);
    if (!is_array($user)) {
        return false;
    }

    $role = ucfirst($role);
    if ($user['PROFIEL'] == $role) {
        return true;
    }

    $role = strtoupper($role);
    if (key_exists($role, $user) && $user[$role] == 'Ja') {
        return true;
    }

    return false;
}

function checkVisualText($textArray) {
    if($textArray['DELETED'] == "1" || $textArray['DELETED'] == 1) {
        return "Element: ".$textArray['ID']." has been deleted.";
    }
    return $textArray['VALUE'];
}

function getCountryColor($id) {
    global $pdo;
    $query = "SELECT COLOR FROM countries WHERE ID = ".$id." AND DELETED = 0";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $color = $stmt->fetchColumn();

    return $color;
}