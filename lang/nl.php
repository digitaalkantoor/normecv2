<?php

function getSiteKolom($site, $tabel = "", $vakTekts = "")
{
    global $db;

  //Bugs
  if ($site == "bugs") {
      $koloms = array(0 => "Bug Nr", 1 => "Beschrijvende naam", 2 => "Categorie", 3 => "Type bug", 4 => "Status", 5 => "Prioriteit");
  }
  else if ($site == "bugsLeeg") {
      $koloms = array(0 => "Bug Nr", 1 => "Beschrijvende naam", 2 => "Categorie", 3 => "Type bug", 4 => "Status", 5 => "Prioriteit");
  }
  else if($site == "toevbugs" || $site == "bewbugs" || $site == "overzbugs" || $site == "bugsKlant" || $site == "getbugs")
  {
      $koloms = array(0 => "ID", 1 => "Bug Nr", 2 => "Beschrijvende naam", 3 => "Project", 4 => "Klant", 5 => "Categorie", 6 => "Type bug", 7 => "Status", 8 => "Prioriteit", 9 => "", 10 => "Geraporteerd door", 11 => "Toegewezen aan", 12 => "Invoerdatum", 13 => "Laatste update", 14 => "BEDRIJFSID");
  }
  else if ($site == "offerteKlant") {
      $koloms = array(0 => "Project Nr", 1 => "Naam", 2 => "Status", 3 => "Opleverdatum", 4 => "Bugs");
  }
  else if($site == "facturenKlant") {
      $koloms = array(0 => "Factuur Nr", 1 => "Klant", 2 => "Datum", 3 => "Bedrag");
  }
  else if($site == "aanmaningenKlant") {
      $koloms = array(0 => "Factuur Nr", 1 => "Project Nr", 2 => "Naam", 3 => "Datum", 4 => "Bedrag");
  }
  else if($site == "vakantiesKlant") {
      $koloms = array(0 => "Medewerker", 1 => "Klant", 2 => "Datum", 3 => "Bedrag", 4 => "BTW");
  }
  else if($site == "opdrachtenKlant") {
      $koloms = array(0 => "Opdracht nr", 1 => "Omschrijving", 2 => "Klant", 3 => "Project", 4 => "Template", 5 => "Medewerker");
  }
  else if ($site == "offerteSpecifiekeKlant") {
      $koloms = array(0 => "Project Nr", 1 => "Naam", 2 => "Status", 3 => "Opleverdatum", 4 => "Bugs");
  }
  else if($site == "facturenSpecifiekeKlant") {
      $koloms = array(0 => "Factuur Nr", 1 => "Naam", 2 => "Datum", 3 => "Bedrag");
  }
  else if($site == "bugsSpecifiekeKlant") {
      $koloms = array(0 => "Bug Nr", 1 => "Beschrijvende naam", 2 => "Categorie", 3 => "Type bug", 4 => "Status", 5 => "Prioriteit");
  }
  else if($site == "opdrachtenSpecifiekeKlant") {
      $koloms = array(0 => "Opdracht nr", 1 => "Omschrijving", 2 => "Klant", 3 => "Project", 4 => "Template", 5 => "Medewerker");
  }
  else if($site == "verkochtSpecifiekeKlant") {
      $koloms = array(0 => "Bon Nr", 1 => "Datum", 2 => "Artikel Nr", 3 => "Factuur Nr",  4 => "Omschrijving");
  }
  else if ($site == "agendaonderwerp") {
      $koloms = array(0 => "Genre", 1 => "Kleur");
  }
  else if($site == "toevagendaonderwerp" || $site == "bewagendaonderwerp" || $site == "overzagendaonderwerp" || $site == "getagendaonderwerp")
  {
      $koloms = array(0 => "ID", 1 => "Genre", 2 => "Kleur", 3 => "BEDRIJFSID");
  }
  else if ($site == "menuitems") {
      $koloms = array(0 => "Description", 1 => "Url");
  }
  else if($site == "toevmenuitems" || $site == "bewmenuitems" || $site == "overzmenuitems" || $site == "getmenuitems")
  {
      $koloms = array(0 => "ID",    1 => "Name",  2 => "EN-name", 3 => "Link", 4 => "Description", 5 => "Role", 6 => "Customer", 7 => "Accounten", 8 => "PDA", 9 => "Picture", 10 => "Rollover", 11 => "BEDRIJFSID");
  }
  else if ($site == "faq_handleiding") {
      $koloms = array(0 => "Vraag", 1 => "Antwoord", 2 => "Bezocht");
  }
  else if($site == "toevfaq_handleiding" || $site == "bewfaq_handleiding" || $site == "overzfaq_handleiding" || $site == "getfaq_handleiding")
  {
      $koloms = array(0 => "ID",     1 => "Onderdeel van", 2 => "Vraag",  3 => "Antwoord",   4 => "Rating", 5 => "Rechten", 6 => "Bezocht", 7 => "BEDRIJFSID");
  }
  else if ($site == "faq") {
      $koloms = array(0 => "Categorie", 1 => "Vraag", 2 => "Antwoord", 3 => "Bezocht");
  }
  else if($site == "toevfaq" || $site == "bewfaq" || $site == "overzfaq" || $site == "getfaq")
  {
      $koloms = array(0 => "ID", 1 => "Categorie", 2 => "Onderdeel van", 3 => "Vraag",  4 => "Antwoord",   5 => "Rating", 6 => "Bezocht", 7 => "BEDRIJFSID");
  }
  else if ($site == "doctemplate") {
      $koloms = array(0 => "Tekst", 1 => "Type", 2 => "Rechten");
  }
  else if($site == "toevdoctemplate" || $site == "bewdoctemplate" || $site == "overzdoctemplate" || $site == "getdoctemplate")
  {
      $koloms = array(0 => "ID",     1 => "Naam", 2 => "Tekst",  3 => "Type",   4 => "Bron map", 5 => "Rechten", 6 => "BEDRIJFSID", 7 => "Achtergrondafbeelding", 8 => "Font size", 9 => "Font");
  }
  else if ($site == "rssfeeds") {
      $koloms = array(0 => "Omschrijving", 1 => "Url");
  }
  else if($site == "toevrssfeeds" || $site == "bewrssfeeds" || $site == "overzrssfeeds" || $site == "getrssfeeds")
  {
      $koloms = array(0 => "ID",    1 => "Omschrijving", 2 => "RSS Url", 3 => "Onderwerp", 4 => "Zichtbaar in mededelingenscherm?", 5 => "Medewerker", 6 => "Download items in kennisbank", 7 => "BEDRIJFSID");
  }
  else if ($site == "pallets") {
      $koloms = array(0 => "Eenheid batch", 1 => "Batch nr", 2 => "Inkoopwaarde", 3 => "Verkoopwaarde", 4 => "Aantal eenheden", 5 => "Locatie");
  }
  else if($site == "toevpallets" || $site == "bewpallets" || $site == "overzpallets" || $site == "getpallets")
  {
      $koloms = array(0 => "ID", 1 => "BEDRIJFSID", 2 => "USERID", 3 => "Inkooporder", 4 => "Verkooporder", 5 => "Eenheid batch", 6 => "Batch nr.", 7 => "Inkoopwaarde", 8 => "Verkoopwaarde", 9 => "Aantal eenheden per batch", 10 => "Eenheid op de batch (doos)", 11 => "Doos nr.", 12 => "Locatie");
  }
  else if ($site == "klantenparameters") {
      $koloms = array(0 => "Klant", 1 => "Datum", 2 => "Parameter", 3 => "Waarde");
  }
  else if($site == "toevklantenparameters" || $site == "bewklantenparameters" || $site == "overzklantenparameters" || $site == "getklantenparameters")
  {
      $koloms = array(0 => "ID",    1 => "Klant", 2 => "Datum", 3 => "Parameter", 4 => "Waarde", 5 => "Opmerking", 6 => "Bestand", 7 => "BEDRIJFSID");
  }
  else if($site == "shareholders") {
    $koloms = array(0 => "Shareholder", 1 => "Ownership %");
  }
  else if($site == "toevshareholders" || $site == "bewshareholders" || $site == "overzshareholders" || $site == "getshareholders")
  {
      $koloms = array(0 => "ID",    1 => "Aandeelhouder", 2 => "Aandelen percentage", 3 => "Type", 4 => "Bedrijf");
  }
  else if ($site == "favourite") {
      $koloms = array(0 => "menuitem", 1 => "Medewerker", 2 => "Top scherm");
  }
  else if($site == "toevfavourite" || $site == "bewfavourite" || $site == "overzfavourite" || $site == "getfavourite")
  {
      $koloms = array(0 => "ID",    1 => "Menuitem", 2 => "Medewerker", 3 => "In de top van het scherm", 4 => "BEDRIJFSID");
  }
  else if ($site == "mijncontacten") {
      $koloms = array(0 => "Type", 1 => "Relatie", 2 => "Medewerker");
  }
  else if($site == "toevmijncontacten" || $site == "bewmijncontacten" || $site == "overzmijncontacten" || $site == "getmijncontacten")
  {
      $koloms = array(0 => "ID",    1 => "Tabel",        2 => "Relatie",      3 => "Medewerker",         4 => "BEDRIJFSID");
  }
  else if ($site == "vakanties") {
      $koloms = array(0 => "Medewerker", 1 => "Periode (Van)", 2 => "Periode (Tot)", 3 => "Aantal uur", 4 => "Naar");
  }
  else if($site == "toevvakanties" || $site == "bewvakanties" || $site == "overzvakanties" || $site == "getvakanties")
  {
      $koloms = array(0 => "ID",    1 => "Medewerker",2 => "Klant",             3 => "Ik ga weg op", 4 => "Ik ben terug op", 5 => "Naar",  6 => "$vakTekts", 7 => "Te bereiken op",       8 => "Dit is betaaldverlof",       9 => "Dit is buitengewoonverlof", 10 => "BEDRIJFSID");
  }
  else if ($site == "declaraties") {
      $koloms = array(0 => "Declaratie Nr", 1 => "Omschrijving", 2 => "Datum", 3 => "Bedrag", 4 => "BTW");
  }
  else if($site == "toevdeclaraties" || $site == "bewdeclaraties" || $site == "overzdeclaraties" || $site == "getdeclaraties")
  {
      $koloms = array(0 => "ID",    1 => "Declaratie nr.",2 => "Factuurnr", 3 => "Omschrijving", 4 => "Medewerker",  5 => "Datum", 6 => "Bedrag (excl BTW)", 7 => "BTW percentage", 8 => "Accepteren", 9 => "BEDRIJFSID");
  }
  else if ($site == "cronjob") {
      $koloms = array(0 => "Naam (voor eigen gebruik)", 1 => "Notificatie mailen naar", 2 => "Laatst uitgevoerd");
  }
  else if($site == "toevcronjob" || $site == "bewcronjob" || $site == "overzcronjob" || $site == "getcronjob")
  {
      $koloms = array(0 => "ID",    1 => "Naam (voor eigen gebruik)",2 => "Notificatie mailen naar", 3 => "User", 4 => "LoginId",  5 => "Datum", 6 => "Actief", 7 => "Toon rapport", 8 => "Opnieuw uitvoeren na", 9 => "BEDRIJFSID");
  }
  else if ($site == "bankrekeningen") {
      $koloms = array(0 => "Naam", 1 => "Rekening Nr.", 2 => "T.N.V.", 3 => "Plaats");
  }
  else if($site == "toevbankrekeningen" || $site == "toevkasboek" || $site == "bewbankrekeningen" || $site == "overzbankrekeningen" || $site == "getbankrekeningen")
  {
      $koloms = array(0 => "ID",    1 => "Naam",2 => "Rekening Nr.", 3 => "T.N.V", 4 => "Plaats",  5 => "Beginsaldo", 6 => "Medewerker", 7 => "Rubriek", 8 => "Bank", 9 => "Krediet", 10 => "BEDRIJFSID");
  }

  else if ($site == "saldoinformatie") {
      $koloms = array(0 => "Bank", 1 => "Datum", 2 => "Bij/Af", 3 => "Bedrag");
  }
  else if($site == "toevsaldoinformatie" || $site == "bewsaldoinformatie" || $site == "overzsaldoinformatie" || $site == "getsaldoinformatie")
  {
      $koloms = array(0 => "ID",    1 => "Kasboek / Rekening",2 => "Datum", 3 => "Referentie", 4 => "Relatie", 5 => "Verkoopboek", 6 => "Inkoopboek", 7 => "Bank of Giro",  8 => "Bankrekeningnummer", 9 => "Omschrijving", 10 => "Mededeling", 11 => "Bij /Af", 12 => "Bedrag", 13 => "Valuta", 14 => "Grootboekrekening", 15 => "(Grootboek-) tegenrekening", 16 => "BEDRIJFSID", 17 => "Nog te boeken", 18 => "Direct geboekt", 19 => "Eigen opmerkingen");
  }
  else if ($site == "saldoinformatieboekingen") {
      $koloms = array(0 => "Bedrag", 1 => "Verkoopboek id", 2 => "Inkoopboek id", 3 => "Grootboekid");
  }
  else if($site == "toevsaldoinformatieboekingen" || $site == "bewsaldoinformatieboekingen" || $site == "overzsaldoinformatieboekingen" || $site == "getsaldoinformatieboekingen")
  {
      $koloms = array(0 => "ID",    1 => "mutatie",2 => "Bedrag", 3 => "Verkoopboek", 4 => "Inkoopboek", 5 => "Grootboekrekening", 6 => "Tegenrekening", 7 => "BEDRIJFSID");
  }
  else if ($site == "opdrachttemplate") {
      $koloms = array(0 => "Omschrijving");
  }
  else if ($site == "verwopdrachttemplate") { head2(); verwijderen("opdrachttemplate"); footer1();}
  else if($site == "toevopdrachttemplate" || $site == "bewopdrachttemplate" || $site == "overzopdrachttemplate" || $site == "getopdrachttemplate")
  {
      $koloms = array(0 => "ID", 1 => "Omschrijving", 2 => "Formuliernaam", 3 => "Koppel aan project", 4 => "Koppel aan relatie", 5 => "Datum / status", 6 => "Koppel aan medewerker", 7 => "Koppel aan administratie", 8 => "Koppel aan contract", 9 => "Koppel aan dossiers", 10 => "Koppel aan artikelen", 11 => "Koppeling", 12 => "Koppel aan bestandslocatie", 13 => "Veldnamen", 14 => "BEDRIJFSID");
  }
  else if ($site == "opdrachten") {
      if(isset($_GET["TEMPLATEID"]))
      {
         $r2 = mysql_query("SELECT * FROM opdrachttemplate WHERE (ID*1) = ('".ps($_GET["TEMPLATEID"], "nr")."'*1) AND KLANT = 'Nee';", $db);
         if($datar2 = mysql_fetch_array($r2))
         {
           $koloms = array(0 => "Naam", 1 => "Verantwoordelijke", 2 => "Status", 3 => "Deadline");
         } else {
           $koloms = array(0 => "Naam", 1 => "Relatie", 2 => "Verantwoordelijke", 3 => "Status", 4 => "Deadline");
         }
      } else {
         $koloms = array(0 => "Naam", 1 => "Relatie", 2 => "Verantwoordelijke", 3 => "Status", 4 => "Deadline");
      }
  }
  else if($site == "toevopdrachten" || $site == "bewopdrachten" || $site == "overzopdrachten" || $site == "getopdrachten")
  {
      $koloms = array(0 => "ID",1 => "Regel Nr.", 2 => "Naam", 3 => "Betreft", 4 => "Omschrijving", 5 => "Relatie", 6 => "Project", 7 => "Template", 8 => "Verantwoordelijke", 9 => "Administratie", 10 => "Contract", 11 => "Dossier", 12 => "Ordernr", 13 => "Koppeling", 14 => "Deadline", 15 => "Status", 16 => "Opdrachtregels", 17 => "BEDRIJFSID");
  }
  else if ($site == "linksonderwerp") {
      $koloms = array(0 => "Omschrijving");
  }
  else if($site == "toevlinksonderwerp" || $site == "bewlinksonderwerp" || $site == "overzlinksonderwerp" || $site == "getlinksonderwerp")
  {
      $koloms = array(0 => "ID", 1 => "Categorie", 2 => "BEDRIJFSID");
  }
  else if ($site == "linken") {
      $koloms = array(0 => "Omschrijving", 1 => "Link", 2 => "Categorie");
  }
  else if ($site == "linkenLeeg") {
      $koloms = array(0 => "Omschrijving", 1 => "Link", 2 => "Categorie");
  }
  else if($site == "toevlinken" || $site == "bewlinken" || $site == "overzlinken" || $site == "getlinken")
  {
      $koloms = array(0 => "ID", 1 => "Beschrijvende naam", 2 => "Link / FTP adres", 3 => "Extra omschrijving", 4 => "Gebruikersnaam", 5 => "Wachtwoord", 6 => "Voor iedereen beschikbaar", 7 => "Categorie", 8 => "BEDRIJFSID", 9 => "Ordner");
  }
  else if ($site == "todolist") {
      $koloms = array(0 => "Omschrijving kort", 1 => "Status");
  }
  else if($site == "toevtodolist" || $site == "bewtodolist" || $site == "overztodolist" || $site == "gettodolist")
  {
      $koloms = array(0 => "ID", 1 => "Categorie",     2 => "Omschrijving kort", 3 => "Omschrijving lang", 4 => "Status", 5 => "Medewerker",         5 => "Datum", 6 => "Tijd",      7 => "BEDRIJFSID");
  }
  else if ($site == "helpitems") {
      $koloms = array(0 => "Onderwerp", 1 => "Kenmerk", 2 => "Bezocht");
  }
  else if($site == "toevhelpitems" || $site == "bewhelpitems" || $site == "overzhelpitems" || $site == "gethelpitems")
  {
      $koloms = array(0 => "ID", 1 => "hidden", 2 => "field", 3 => "editor", 4 => "field");
  }
  else if ($site == "maillinglist") {
      $koloms = array(0 => "Maillinglist", 1 => "Naam", 2 => "Aangemeld");
  }
  else if($site == "toevmaillinglist" || $site == "bewmaillinglist" || $site == "overzmaillinglist" || $site == "getmaillinglist")
  {
      $koloms = array(0 => "ID", 1 => "Mailinglist", 2 => "Klant", 3 => "Aangemeld", 4 => "Datum", 5 => "BEDRIJFSID");
  }
  else if ($site == "nieuwsbrief") {
      $koloms = array(0 => "Datum", 1 => "Titel", 2 => "Categorie");
  }
  else if($site == "toevnieuwsbrief" || $site == "bewnieuwsbrief" || $site == "overznieuwsbrief" || $site == "getnieuwsbrief")
  {
      $koloms = array(0 => "ID", 1 => "Mailinglist", 2 => "Afzender (e-mail)", 3 => "Titel", 4 => "Medewerker", 5 => "Getoond op website", 6 => "BEDRIJFSID", 7 => "Tekst", 8 => "Mailtekst voor mail als link", 9 => "Zet de mail in een onzichtbaar kader", 10 => "Ordner", 11 => "Datum");
  }
  else if ($site == "sms") {
      $koloms = array(0 => "Datum", 1 => "Verzender", 2 => "Tekst", 3 => "Ontvanger");
  }
  else if($site == "toevsms" || $site == "bewsms" || $site == "overzsms" || $site == "getsms")
  {
      $koloms = array(0 => "ID", 1 => "Aan", 2 => "Van", 3 => "Tekst", 4 => "Datum", 5 => "BEDRIJFSID", 6 => "Status");
  }
  else if ($site == "personeel") {
      $koloms = array(0 => "Voorletters", 1 => "Achternaam", 2 => "Adres", 3 => "Plaats", 4 => "Mobiel", 5 => "Email");
  }
  else if($site == "toevpersoneel" || $site == "bewpersoneel" || $site == "bewpersoneelID" || $site == "overzpersoneel" || $site == "overzpersoneelNAWLeeg" || $site == "getpersoneel")
  {
      $koloms = array(0 => "ID", 1 => "Bedrijfsnaam", 2 => "Man / Vrouw", 3 => "Voorletters", 4 => "Voornaam", 5 => "Tussenvoegsel", 6 => "Achternaam", 7 => "Adres", 8 => "Postcode", 9 => "Plaats", 10 => "Land", 11 => "Postbus", 12 => "Postcode van de postbus", 13 => "Homepage", 14 => "Email adres", 15 => "Telefoon vast", 16 => "Mobiel", 17 => "Fax", 18 => "Rekening Nr", 19 => "Ten name van", 20 => "Rekening plaats", 21 => "Bedrijfsadministratie", 22 => "Type werknemer", 23 => "Kosten per uur", 24 => "Aantal verlofdagen p.j.", 25 => "Werkuren per week", 26 => "Geboortedatum", 27 => "Werkdagen per week", 28 => "Wilt u per dag een automatische reminder", 29 => "Functie", 30 => "Afdeling", 31 => "Wanneer eerste werkdag?", 32 => "Wanneer laatste werkdag?<br/><i>Bij vast contract datum in de toekomst invullen.", 33 => "Kenteken van de auto", 34 => "Publiceer mijn gegevens online", 35 => "Verlofsaldo begin dit jaar (in uren)", 36 => "Laatste login", 37 => "Memo", 38 => "Rol", 39 => "Foto", 40 => "Taal", 41 => "Is werkzaam op");
  }
  else if ($site == "profiel") {
      $koloms = array(0 => "Medewerker", 1 => "Geboorteplaats", 2 => "Beroep", 3 => "Burgenlijke staat");
  }
  else if($site == "toevprofiel" || $site == "bewprofiel" || $site == "overzprofiel" || $site == "getprofiel")
  {
      $koloms = array(0 => "ID",    1 => "PERSONEELSLID", 2 => "Pasfoto", 3 => "Achtergrondfoto menu", 4 => "Burgerlijke staat", 5 => "Geboorteplaats", 6 => "Beroep", 7 => "Humor",  8 => "Mode", 9 => "Muziek", 10 => "Interesses", 11 => "Hobbies", 12 => "Huisdieren", 13 => "Msn-adres", 14 => "Skype-adres", 15 => "Favourite citaat", 16 => "Overige informatie", 17 => "BEDRIJFSID");
  }
  else if($site == "afdeling") {
      $koloms = array(0 => "Naam", 1 => "Positie in lijn tot de top", 2 => "Verantwoordelijke");
  }
  else if($site == "toevafdeling" || $site == "bewafdeling" || $site == "overzafdeling" || $site == "getafdeling")
  {
      $koloms = array(0 => "ID", 1 => "Naam",    2 => "Positie in lijn tot de top", 3 => "Verantwoordelijke", 4 => "BEDRIJFSID");
  }
  else if($site == "taal") {
      $koloms = array(0 => "Nederlands", 1 => "Engels");
  }
  else if($site == "toevtaal" || $site == "bewtaal" || $site == "overztaal" || $site == "gettaal")
  {
      $koloms = array(0 => "ID", 1 => "Referentie",    2 => "Nederlands", 3 => "Engels", 4 => "BEDRIJFSID");
  }
  else if ($site == "CVSpecifiekeKlant") {
      $koloms = array(0 => "Van", 1 => "Datum binnenkomst", 2 => "Studie 1", 3 => "TINK datum", 4 => "PINK datum", 5 => "Gereageerd?", 6 => "Status");
  }
  else if($site == "cv") {
      $koloms = array(0 => "Van", 1 => "Datum binnenkomst", 2 => "Studie 1", 3 => "TINK datum", 4 => "PINK datum", 5 => "Gereageerd?", 6 => "Status");
  }
  else if($site == "toevcv" || $site == "toevcvpers" || $site == "bewcv" || $site == "bewcvpers" || $site == "overzcv" || $site == "getcv")
  {
      $koloms = array(0 => "ID", 1 => "Medewerker",                2 => "Sollicitant",     3 => "Titel CV", 4 => "Datum cv ontvangen", 5 => "Kwam binnen via", 6 => "Rijbewijs", 7 => "Dienstensector",    8 => "Locatie werk",   9 => "Expertise", 10 => "Studie 1",           11 => "Overige studies", 12 => "Eigenschappen", 13 => "Ambities",  14 => "Werkervaring", 15 => "Bijzondere ervaring", 16 => "Vorige werkgevers", 17 => "Referenties",18 => "Beschikbaar per",  19 => "Aantal uur per week", 20 => "TINK datum",  21 => "TINK door",     22 => "TINK",   23 => "PINK datum",  24 => "PINK door",          25 => "PINK",   26 => "Wie heeft er gereageerd?", 27 => "Terugkoppeldatum", 28 => "Opmerking", 29 => "Status",   30 => "Video", 31 => "C.v. bestand", 32 => "BEDRIJFSID");
  }
  else if($site == "persdossier") {
      $koloms = array(0 => "Medewerker", 1 => "Datum", 2 => "Type gesprek");
  }
  else if($site == "toevpersdossier" || $site == "bewpersdossier" || $site == "overzpersdossier" || $site == "getpersdossier")
  {
      $koloms = array(0 => "ID", 1 => "Medewerker",       2 => "Datum",  3 => "Type gesprek", 4 => "Tekst beoordelaar", 5 => "Tekst beoordeelde", 6 => "BEDRIJFSID");
  }
  else if ($site == "ongewenstemail") {
      $koloms = array(0 => "Woord", 1 => "Emailadres");
  }
  else if($site == "toevongewenstemail" || $site == "bewongewenstemail" || $site == "overzongewenstemail" || $site == "getongewenstemail")
  {
      $koloms = array(0 => "ID", 1 => "Ongewenst woord", 2 => "Ongewenst emailadres", 3 => "Medewerker", 4 => "BEDRIJFSID");
  }
  else if ($site == "mailinstellingen") {
      $koloms = array(0 => "Email", 1 => "Type", 2 => "Standaard");
  }
  else if($site == "toevmailinstellingen" || $site == "bewmailinstellingen" || $site == "overzmailinstellingen" || $site == "getmailinstellingen")
  {
      $koloms = array(0 => "ID", 1 => "Type", 2 => "Email", 3 => "Gebruikersnaam", 4 => "Wachtwoord", 5 => "Inkomende mailserver", 6 => "Uitgaangde mailserver", 7 => "Host", 8 => "Bewaar berichten op server", 9 => "Hoeveel dagen bewaren op server?", 10 => "Employee", 11 => "Handtekening", 12 => "BEDRIJFSID", 13 => "Mailbox gedeeld met anderen", 14 => "Is standaard mailadres");
  }
  else if ($site == "nieuws") {
      $koloms = array(0 => "Titel", 1 => "Datum", 2 => "Zichtbaar");
  }
  else if($site == "toevnieuws" || $site == "bewnieuws" || $site == "overznieuws" || $site == "getnieuws")
  {
      $koloms = array(0 => "ID",    1 => "Titel", 2 => "Nieuws item",   3 => "Datum",  4 => "Zichtbaar",       5 => "BEDRIJFSID", 6 => "Medewerker", 7 => "Bestand koppelen", 8 => "Plaatje");
  }
  else if ($site == "offerteteksten") {
      $koloms = array(0 => "Type", 1 => "Kenmerk");
  }
  else if($site == "toevofferteteksten" || $site == "bewofferteteksten" || $site == "overzofferteteksten" || $site == "getofferteteksten")
  {
      $koloms = array(0 => "ID", 1 => "Titel op de factuur zelf", 2 => "Titel als geheugensteuntje", 3 => "Tekst boven de factuur (voor format 20)", 4 => "Opmaak factuur tekst", 5 => "Wat is de volgende tekst, na verstrijken vervaldatum", 6 => "Factuurtaal", 7 => "Type tekst", 8 => "Factuurlogo", 9 => "BEDRIJFSID");
  }
  else if ($site == "conceptofferte" || $site == "alleoffertes") {
      $koloms = array(0 => "Offerte Nr.", 1 => "Klant", 2 => "Beschrijvende naam", 3 => "Start op", 4 => "Stopt op", 5 => "Bedrag", 6 => "Status", 7 => "Offerte geldig tot");
  }
  else if($site == "toevconceptofferte" || $site == "bewconceptofferte" || $site == "overzconceptofferte" || $site == "conceptofferte_pdf" || $site == "getconceptofferte")
  {
      $koloms = array(0 => "ID",    1 => "Offerte Nr.",2 => "Beschrijvende naam", 3 => "Klant", 4 => "Leverancier",        5 => "Medewerker", 6 => "Start op", 7 => "Stopt op", 8 => "Invoerdatum", 9 => "Tot wanneer is deze offerte geldig?", 10 => "Hoeveel tijd", 11 => "Hoeveel euro per (tijds)eenheid",  12 => "Eenheid", 13 => "Vast bedrag", 14 => "Status", 15 => "Aantal (type 1)", 16 => "Aantal (type 2)", 17 => "Aantal (type 3)", 18 => "Aantal (type 4)", 19 => "Tekst voor de klant (1)", 20 => "Tekst voor de klant (2)", 21 => "Tekst voor de leverancier (1)", 22 => "Tekst voor de leverancier (2)", 23 => "Welke acties zijn er ondernomen", 24 => "Opmerking", 25 => "BEDRIJFSID");
  }
  else if ($site == "bugcatagory") {
      $koloms = array(0 => "Categorie", 1 => "Beschrijvende tekst");
  }
  else if($site == "toevbugcatagory" || $site == "bewbugcatagory" || $site == "overzbugcatagory" || $site == "getbugcatagory")
  {
      $koloms = array(0 => "ID", 1 => "BEDRIJFSID", 2 => "Beschrijvende naam", 3 => "Extra tekst");
  }
  else if ($site == "buglist") {
      $koloms = array(0 => "Ticket Nr.", 1 => "Omschrijvende naam");
  }
  else if($site == "toevbuglist" || $site == "bewbuglist" || $site == "overzbuglist" || $site == "getbuglist")
  {
      $koloms = array(0 => "ID", 1 => "Ticket Id.", 2 => "Beschrijvende naam", 3 => "Omschrijving", 4 => "Additional info", 5 => "BESTAND", 6 => "Prive of publiek", 7 => "Medewerker", 8 => "BEDRIJFSID", 9 => "Datum", 10 => "Tijd");
  }
  else if ($site == "offerteleden") {
      $koloms = array(0 => "Project Nr", 1 => "Medewerker", 2 => "Taak");
  }
  else if($site == "toevofferteleden" || $site == "bewofferteleden" || $site == "overzofferteleden" || $site == "getofferteleden")
  {
      $koloms = array(0 => "ID", 1 => "Project", 2 => "Taak", 3 => "Medewerker", 4 => "Uurloon", 5 => "BEDRIJFSID");
  }
  else if ($site == "planningtijd") {
      $koloms = array(0 => "Project Nr", 1 => "Medewerker", 2 => "Taak", 3 => "Beschikbare tijd");
  }
  else if($site == "toevplanningtijd" || $site == "bewplanningtijd" || $site == "overzplanningtijd" || $site == "getplanningtijd")
  {
      $koloms = array(0 => "ID", 1 => "Project", 2 => "Taak", 3 => "Medewerker", 4 => "Beschikbare tijd", 5 => "Opmerking", 6 => "Datum van", 7 => "Datum tot", 8 => "BEDRIJFSID");
  }
  else if ($site == "productgroepkorting") {
      $koloms = array(0 => "Categorie", 1 => "Klant", 2 => "Korting");
  }
  else if($site == "toevproductgroepkorting" || $site == "bewproductgroepkorting" || $site == "overzproductgroepkorting" || $site == "getproductgroepkorting")
  {
      $koloms = array(0 => "ID",    1 => "Categorie", 2 => "Klant",   3 => "Kortings %",   4 => "BEDRIJFSID");
  }
  else if ($site == "producten") {
      $koloms = array(0 => "Artikel nr.", 1 => "Categorie", 2 => "Naam", 3 => "Leverancier", 4 => "Verkoopsprijs", 5 => "BTW %", 6 => "Inkoopsprijs", 7 => "BTW %");
  }
  else if($site == "toevproducten" || $site == "bewproducten" || $site == "overzproducten" || $site == "getproducten")
  {
      $koloms = array(0 => "ID",    1 => "Artikel nr.",    2 => "Categorie",       3 => "Omschrijving", 4 => "Beschrijvende naam" , 5 => "Merk",  6 => "Type",  7 => "Verkoopbaar vanaf (datum)", 8 => "Verkoopbaar vanaf (tijd)", 9 => "Verkoopbaar tot (datum)", 10 => "Verkoopbaar tot (tijd)", 11 => "Leverancier", 12 => "Maximale prijs", 13 => "Minimale prijs", 14 => "Verkoopsprijs", 15 => "Prijs per maand",16 => "Prijs per kwartaal", 17 => "Prijs per jaar", 18 => "In voorraad", 19 => "In het assortiment", 20 => "Inkoopsprijs",   21 => "Inkoopsprijs p.m.", 22 => "Inkoopsprijs p. kwartaal", 23 => "Inkoopsprijs p.j.", 24 => "Beschikbaar voor online verkoop", 25 => "Aantal keer bekeken", 26 => "Plaatje",  27 => "BEDRIJFSID", 28 => "Minimale voorraad", 29 => "Besteladvies", 30 => "BTW %", 31 => "Eenheid inkoop", 32 => "Eenheid verkoop", 33 => "Omrekeningsfactor", 34 => "TAGS", 35 => "PRIJSVAN", 36 => "LEVERTIJD", 37 => "VERZENDKOSTEN");
  }
  else if ($site == "verkocht") {
      $koloms = array(0 => "Bon Nr", 1 => "Klant", 2 => "Datum", 3 => "Aantal", 4 => "Artikel", 5 => "Omschrijving", 6 => "Loopt het abonnement?", 7 => "Factuurnr");
  }
  else if($site == "toevverkocht" || $site == "bewverkocht" || $site == "overzverkocht" || $site == "getverkocht")
  {
      $koloms = array(0 => "ID",    1 => "BEDRIJFSID",2 => "Medewerker",3 => "Relatie",4 => "Bon Nr",5 => "Artikel",6 => "Datum",7 => "Omschrijving",8 => "Extra commentaar",9 => "Afwijkende totaalprijs",10 => "Korting",11 => "Aantal",12 => "Geleverd",13 => "Backorder",14 => "Pakbon",15 => "Loopt het abonnement nog steeds",16 => "Dit abonnement loopt tot",17 => "Pallet nr",18 => "Provisie",19 => "Reseller",20 => "Project",21 => "Behoort bij factuur",22 => "");
  }
  else if($site == "toevrelatievan" || $site == "bewrelatievan" || $site == "overzrelatievan" || $site == "getrelatievan")
  {
      $koloms = array(0 => "ID", 1 => "Relatie", 2 => "Contactpersoon", 3 => "Relatie", 4 => "Contactpersoon", 5 => "Omschrijving", 6 => "BEDRIJFSID");
  }
  else if ($site == "inkooporder") {
      $koloms = array(0 => "Bon Nr", 1 => "Verkooporder", 2 => "Leverancier", 3 => "Datum", 4 => "Besteld", 5 => "Geleverd", 6 => "Backorder", 7 => "Artikel", 8 => "Omschrijving", 9 => "Factuurnr");
  }
  else if($site == "toevinkooporder" || $site == "bewinkooporder" || $site == "overzinkooporder" || $site == "getinkooporder")
  {
      $koloms = array(0 => "ID",    1 => "Bonnr",  2 => "Verkoop bonnr",    3 => "Artikel",  4 => "Leverancier", 5 => "Datum bestelling",        6 => "Verwachte ontvangstdatum", 7 => "Omschrijving", 8 => "Aantal besteld", 9 => "Geleverd", 10 => "Backorder", 11 => "Totaalprijs", 12 => "Korting", 13 => "Medewerker", 14 => "Levering", 15 => "Project", 16 => "Productvariant", 17 => "Extra omschrijving", 18 => "Inkoop factuur", 19 => "BEDRIJFSID");
  }
  else if ($site == "offerte") {
      $koloms = array(0 => "Klant", 1 => "Projectnaam",2 => "Project Nr.", 3 => "Status", 4 => "Opleverdatum", 5 => "Projectleider"); //, 5 => "Alles gefactureerd");
  }
  else if($site == "toevofferte" || $site == "bewofferte" || $site == "overzofferte" || $site == "getofferte")
  {
      $koloms = array(0 => "ID",    1 => "Project Nr", 2 => "Offerte", 3 => "Omschrijving",4 => "Opmerkingen", 5 => "Beschrijvende naam",  6 => "Internet adres", 7 => "Dit project is", 8 => "Totaal order bedrag", 9 => "Vaste prijsafspraak met de klant?",10 => "BEDRIJFSID",11 => "Klant",   12 => "Status", 13 => "Startdatum", 14 => "Opleverdatum", 15 => "Projectleider", 16 => "Aanspreekpunt", 17 => "Regio", 18 => "Standaard aantal kilometers", 19 => "Externe uitvoerder", 20 => "PO-nummer");
  }
  else if ($site == "projectIntern") {
      $koloms = array(0 => "Projectnaam",1 => "Project Nr.", 2 => "Soort project", 3 => "Projectleider", 4 => "Aanspreekpunt", 5 => "Opmerkingen");
  }
  else if ($site == "uren") {
      $koloms = array(0 => "Project Nr", 1 => "DATUM", 1 => "Datum", 2 => "Tijd (start)", 3 => "Tijd (stop)", 4 => "Taak");
  }
  else if ($site == "urenLeeg") {
     $koloms = array(0 => "Project Nr", 1 => "Datum", 2 => "Tijd (start)", 3 => "Tijd (stop)", 4 => "Taak");
  }
  else if($site == "toevuren" || $site == "bewuren" || $site == "overzuren" || $site == "geturen")
  {
      $koloms = array(0 => "ID",    1 => "Project",    2 => "Activiteit",   3 => "Datum",  4 => "Tijd (start)",    5 => "Tijd (stop)", 6 => "Omschrijving", 7 => "BEDRIJFSID", 8 => "Factuur",        9 => "Medewerker");
  }
  else if ($site == "urenopplan") {
      $koloms = array(0 => "Project Nr", 1 => "Datum", 2 => "Tijd (start)", 3 => "Tijd (stop)", 4 => "Is nog oke?");
  }
  else if($site == "toevurenopplan" || $site == "bewurenopplan" || $site == "overzurenopplan" || $site == "geturenopplan")
  {
      $koloms = array(0 => "ID",    1 => "Project",    2 => "Taak",   3 => "Datum",  4 => "Tijd (start)",    5 => "Tijd (stop)", 6 => "Omschrijving", 7 => "BEDRIJFSID", 8 => "Medewerker", 9 => "Zijn deze uren nog steeds volgens afspraak?", 10 => "WIJZIGDATUM", 11 => "WIJZIGPERSLID");
  }
  else if($site == "klanten") {
      $koloms = array(0 => "Bedrijfsnaam", 1 => "Naam", 2 => "Adres", 3 => "Postcode", 4 => "Plaats", 5 => "Telefoon", 6 => "Mobiel");
  }
  else if($site == "toevklanten" || $site == "toevklantensnel" || $site == "bewklanten" || $site == "overzklanten" || $site == "getklanten" || $site == "getklanten_dbstructuur")
  {
      $koloms = array(0 => "ID",
                      1 => "Relatie Nr.",
                      2 => "Type relatie",
                      3 => "Deze relatie is tevens",
                      4 => "Deze relatie is tevens",
                      5 => "Soort bedrijf",
                      6 => "Vakgebied",      
                      7 => "Bedrijfsnaam",  
                      8 => "Man / Vrouw (Contactpersoon)",  
                      9 => "T.n.v. (Contactpersoon)",
                      10 => "Titel",                     
                      11 => "Voorletters (Contactpersoon)",
                      12 => "Voornaam (Contactpersoon)",
                      13 => "Tussenvoegsel (Contactpersoon)",
                      14 => "Achternaam (Contactpersoon)",
                      15 => "Afdeling",
                      16 => "Functie (Contactpersoon)",  
                      17 => "Email (Contactpersoon)", 
                      18 => "Telefoon (Contactpersoon)",
                      19 => "Mobiel (Contactpersoon)", 
                      20 => "Adres (Bezoekadres)",
                      21 => "Huis Nr (Bezoekadres)",
                      22 => "Toevoeging (Bezoekadres)",    
                      23 => "Postcode (Bezoekadres)",    
                      24 => "Plaats (Bezoekadres)",    
                      25 => "Land (Postadres)",
                      26 => "evt. Bedrijfsnaam (Postadres)",
                      27 => "evt. Contactpersoon (Postadres)",
                      28 => "Adres (Postadres)",
                      29 => "Huis Nr (Postadres)",
                      30 => "Toevoeging (Postadres)",
                      31 => "Postcode (Postadres)",
                      32 => "Plaats (Postadres)",
                      33 => "Telefoon (Postadres)",
                      34 => "Fax (Postadres)",
                      35 => "Homepage",
                      36 => "Email (Kantoor)",
                      37 => "Postbus",
                      38 => "Postcode van de postbus",
                      39 => "Kamer van Koophandel Nr",
                      40 => "BTW Nr",
                      41 => "Verzendwijze",
                      42 => "Rekening Nr",
                      43 => "Ten name van",
                      44 => "Rekening plaats",
                      45 => "Creditcard Nr",
                      46 => "Type kaart",
                      47 => "Vervaldatum kaart",
                      48 => "Bedrijfsomschrijving",
                      49 => "Geboortedatum",
                      50 => "Wanneer klant is geworden",
                      51 => "Wie is verantwoordelijk voor deze relatie",
                      52 => "Welk cijfer <b>krijgt</b> u van de relatie",
                      53 => "Welk cijfer <b>geeft</b> u van de relatie",
                      54 => "Is deze relatie exit?",
                      55 => "Hoe heeft de klant ons gevonden?",
                      56 => "Aangebracht via",
                      57 => "Hoe sterk is de relatie met de klant",
                      58 => "Hoofdtaal",
                      59 => "Ethnische achtergrond",
                      60 => "Hoogstgenoten opleiding",
                      61 => "Inkomstenbron",
                      62 => "Skills",
                      63 => "Bijzonderheden",
                      64 => "BEDRIJFSID",
                      65 => "Selecteer standaard grootboekrek. debiteur",
                      66 => "Selecteer standaard grootboekrek. crediteur",
                      67 => "In hoeveel dagen dienen facturen (verkoop) te worden betaald",
                      68 => "In hoeveel dagen dienen facturen (inkoop) te worden betaald",
                      69 => "Kredietlimiet",
                      70 => "Dossier",
                      71 => "Status nieuwsbrief",
                      72 => "Inkomstenbron",
                      73 => "Jaarinkomen",
                      74 => "Jaarinkomen is van welk jaar",
                      75 => "Waarde eigen woning",
                      76 => "Vrij vermogen",
                      77 => "Aantal kinderen",
                      78 => "Relatie van",
                      79 => "BSN nummer",
                      80 => "Burgerlijke staat",
                      81 => "Afwijkend mailadres voor facturen",
                      82 => "Afwijkend mailadres voor nieuwsbrief",
                      83 => "Provincie",
                      85 => "Betalingsconditie",
                      86 => "Foto");
  }
  else if($site == "toonveldenklanten") {
      $koloms = array(0 => "Bedrijfsnaam", 1 => "Naam", 2 => "Adres", 3 => "Postcode", 4 => "Plaats", 5 => "Telefoon", 6 => "Mobiel");
  }
  else if($site == "toevtoonveldenklanten" || $site == "bewtoonveldenklanten" || $site == "overztoonveldenklanten" || $site == "gettoonveldenklanten")
  {
      $koloms = array(0 => "ID", 1 => "Relatie Nr.",   2 => "Type relatie",   3 => "Deze relatie is tevens", 4 => "Deze relatie is tevens", 5 => "Soort bedrijf", 6 => "Vakgebied",      7 => "Bedrijfsnaam",  8 => "Man / Vrouw (Contactpersoon)",  9 => "T.n.v. (Contactpersoon)", 10 => "Titel",                     11 => "Voorletters (Contactpersoon)",12 => "Voornaam (Contactpersoon)", 13 => "Tussenvoegsel (Contactpersoon)", 14 => "Achternaam (Contactpersoon)", 15 => "Functie (Contactpersoon)",  16 => "Email (Contactpersoon)", 17 => "Telefoon (Contactpersoon)", 18 => "Mobiel (Contactpersoon)", 19 => "Adres (Bezoekadres)", 20 => "Huis Nr (Bezoekadres)",    21 => "Toevoeging (Bezoekadres)",    22 => "Postcode (Bezoekadres)",    23 => "Plaats (Bezoekadres)",    24 => "Land (Postadres)", 25 => "Adres (Postadres)", 26 => "Huis Nr (Postadres)", 27 => "Toevoeging (Postadres)", 28 => "Postcode (Postadres)", 29 => "Plaats (Postadres)", 30 => "Telefoon (Postadres)", 31 => "Fax (Postadres)", 32 => "Homepage",    33 => "Email (Kantoor)", 34 => "Postbus",     35 => "Postcode van de postbus", 36 => "Kamer van Koophandel Nr", 37 => "BTW Nr",      38 => "Factuur per mail?", 39 => "Rekening Nr",  40 => "Ten name van",  41 => "Rekening plaats",  42 => "Creditcard Nr", 43 => "Type kaart", 44 => "Vervaldatum kaart", 45 => "Bedrijfsomschrijving", 46 => "Geboortedatum",  47 => "Wanneer klant is geworden", 48 => "Wie is verantwoordelijk voor deze relatie", 49 => "Welk cijfer <b>krijgt</b> u van de relatie", 50 => "Welk cijfer <b>geeft</b> u van de relatie",  51 => "Is deze relatie exit?", 52 => "Hoe heeft de klant ons gevonden?", 53 => "Wie heeft de klant gevonden", 54 => "Hoe sterk is de relatie met de klant", 55 => "Hoofdtaal",          56 => "Ethnische achtergrond", 57 => "Hoogstgenoten opleiding", 58 => "Inkomstenbron", 59 => "Skills",      60 => "Bijzonderheden", 61 => "BEDRIJFSID", 62 => "Selecteer standaard grootboekrek.", 63 => "In hoeveel dagen dienen facturen te worden betaald", 64 => "Dossiers", 65 => "Status", 66 => "Inkomstenbron", 67 => "Jaarinkomen", 68 => "Jaarinkomen jaartal", 69 => "Waarde eigen woning",70 => "Vrij vermogen", 71 => "Kinderen", 72 => "Relatie van", 73 => "BSN nummer", 74 => "Huwelijkse staat", 75 => "Email factuur", 76 => "Email nieuwsbrief", 77 => "Provincie", 78 => "Kenteken");
  }
  else if($site == "contactpersoon") {
      $koloms = array(0 => "Klant", 1 => "Naam", 2 => "Email", 3 => "Mobiel", 4 => "Functie");
  }
  else if($site == "toevcontactpersoon" || $site == "bewcontactpersoon" || $site == "overzcontactpersoon" || $site == "getcontactpersoon")
  {
      $koloms = array(0 => "ID", 1 => "Klant",  2 => "Man / Vrouw",  3 => "T.n.v.", 4 => "Titel", 5 => "Voorletters", 6 => "Voornaam",  7 => "Tussenvoegsel", 8 => "Achternaam", 9 => "Email",10 => "Telefoon", 11 => "Mobiel", 12 => "Adres",13 => "Huis Nr.", 14 => "Toevoeging", 15 => "Postcode", 16 => "Plaats", 17 => "Land",  18 => "Fax", 19 => "Homepage", 20 => "Postbus", 21 => "Postcode van de postbus", 22 => "Functie", 23 => "Geboortedatum", 24 => "Bijzonderheden", 25 => "BEDRIJFSID", 26 => "Foto");
  }
  else if($site == "facturen") {
      $koloms = array(0 => "Factuur Nr", 1 => "Relatie", 2 => "Datum", 3 => "Bedrag");
  }
  else if($site == "toevfacturen" || $site == "bewfacturen" || $site == "overzfacturen" || $site == "facturen_pdf" || $site == "getfacturen")
  {
    $koloms = array(0 => "ID",
                     1 => "Algemene factuurinformatie",
                     2 => "Factuur Nr",
                     3 => "Project",
                     4 => "Datum",
                     5 => "Relatie",
                     6 => "Kostenplaats",
                     7 => "Betreft",

                     8 => "Financiele informatie",
                     9 => "Grootboekrekening",
                     10 => "Tegenrekening (BTW hoog)",
                     11 => "NAAM",
                     12 => "BTW percentage (BTW hoog)",
                     13 => "Totaal bedrag (BTW hoog)",
                     14 => "Tegenrekening (BTW laag)",
                     15 => "BTW percentage (BTW laag)",
                     16 => "Totaal bedrag (BTW laag)",
                     17 => "Tegenrekening (BTW nul)",
                     18 => "Totaal bedrag (BTW nul)",

                     19 => "Korting",
                     20 => "Boete",
                     21 => "Administratiekosten",
                     22 => "Verzendkosten",

                     23 => "BEDRIJFSID",
                     24 => "Is verzonden",
                     25 => "Is betaald",
                     26 => "Betaaldatum",
                     27 => "CONTROL1",
                     28 => "CONTROL2",
                     29 => "CONTROL3",

                     30 => "Factuur opmaak",
                     31 => "Template",
                     32 => "Factuurtekst",
                     33 => "Aanmaningstekst",
                     34 => "Tekst boven of onder de factuurspecificaties",
                     35 => "Uren op factuur tonen",
                     36 => "Extra kosten op factuur tonen",
                     37 => "Producten op factuur tonen",
                     38 => "Logo tonen",
                     39 => "Footer tonen",

                     40 => "PERSONEELSLID",
                     41 => "MEMO",

                     42 => "Reseller",
                     43 => "Afgerekend",
                     44 => "Met reseller is afgerekend",
                     45 => "Bedrag",
                     46 => "BESTAND");

  }
  else if($site == "facturenaanmanen") {
      $koloms = array(0 => "Aanmanings Nr", 1 => "Factuur Nr", 2 => "Klant", 3 => "Datum", 4 => "Bedrag");
  }
  else if($site == "toevfacturenaanmanen" || $site == "bewfacturenaanmanen" || $site == "overzfacturenaanmanen" || $site == "facturenaanmanen_pdf" || $site == "getfacturenaanmanen")
  {
      $koloms = array(0 => "ID", 1 => "Aanmanings Nr", 2 => "Factuur Nr",   3 => "Project",   4 => "Beschrijvende naam", 5 => "Totaal bedrag", 6 => "Extra tekst", 7 => "Datum", 8 => "BEDRIJFSID", 9 => "Verzonden factuur", 10 => "Klant", 11 => "Factuur tekst", 12 => "BTW percentage", 13 => "Korting (euro)", 14 => "Boete", 15 => "Administratiekosten", 16 => "Verzendkosten", 17 => "Uren op factuur tonen", 18 => "Extra kosten op factuur tonen", 19 => "Producten op factuur tonen");
  }
  else if ($site == "factuurregels") {
      $koloms = array(0 => "Regel", 1 => "Prijs");
  }
  else if($site == "toevfactuurregels" || $site == "bewfactuurregels" || $site == "overzfactuurregels" || $site == "getfactuurregels")
  {
      $koloms = array(0 => "ID", 1 => "Factuurid", 2 => "Regel", 3 => "Extra tekst/Astrix", 4 => "Onderwerp", 5 => "Prijs", 6=> "BTW", 7 => "BEDRIJFSID");
  }
  else if ($site == "aanmaningsregels") {
      $koloms = array(0 => "Regel", 1 => "Prijs");
  }
  else if($site == "toevaanmaningsregels" || $site == "bewaanmaningsregels" || $site == "overzaanmaningsregels" || $site == "getaanmaningsregels")
  {
      $koloms = array(0 => "ID", 1 => "Factuurid", 2 => "Regel", 3 => "Prijs", 4 => "Extra tekst/Astrix", 5 => "Onderwerp", 6 => "BEDRIJFSID");
  }
  else if($site == "bedrijf") {
      $koloms = array(0 => "Bedrijfsnaam", 1 => "Stad", 2 => "Land");
  }
  else if($site == "toevbedrijf" || $site == "bewbedrijf" || $site == "overzbedrijf" || $site == "getbedrijf")
  {
      $koloms = array(0 => "ID", 1 => "Bedrijfsnaam", 2 => "Man / Vrouw", 3 => "Voornaam", 4 => "Tussenvoegsel", 5 => "Achternaam", 6 => "Adres", 7 => "Postcode", 8 => "Plaats", 9 => "Land", 10 => "Postbus", 11 => "Postcode van de postbus", 12 => "Homepage", 13 => "Email", 14 => "Telefoon vast", 15 => "Mobiel", 16 => "Fax", 17 => "Kamer van Koophandel Nr", 18 => "BTW Nr", 19 => "Kopie paspoort", 20 => "Betaalwijze", 21 => "Rekening Nr", 22 => "Ten name van", 23 => "Rekening plaats", 24 => "Creditcard Nr", 25 => "Type Card", 26 => "Administratie", 27 => "Bestandsnaam van logo", 28 => "Serverruimte", 29 => "SMS Bundel", 30 => "IBAN", 31 => "BIC", 32 => "Emailadres van het incassobureau", 33 => "Gevestigd in", 34 => "Is onderdeel van", 35 => "default dir", 36 => "Startdatum", 37 =>"Rechtsvorm", 38 => "Eerste boekjaar", 39 => "Actief", 40 => "Handelsnaam", 41 => "End of Financial Year", 42 => "Bestuurd vanuit", 43 => "Intern bedrijf", 44 => "Omschrijving", 45 => "Rating", 46 => "Credits", 47 => "Branche", 48 => "Type", 49 => "Slapende entiteit");
  }
  else if ($site == "postenvast") {
      $koloms = array(0 => "Beschrijvende naam", 1 => "Omschrijving", 2 => "Standaard uurloon", 3 => "Standaard bij elk nieuw project");
  }
  else if($site == "toevpostenvast" || $site == "bewpostenvast" || $site == "overzpostenvast" || $site == "getpostenvast")
  {
      $koloms = array(0 => "ID", 1 => "Beschrijvende naam", 2 => "Omschrijving", 3 => "Uurloon (default)", 4 => "Standaard taak bij elk nieuw project", 5 => "Standaard beschikbare tijd", 6 => "BEDRIJFSID");
  }
  else if ($site == "kostenintern") {
      $koloms = array(0 => "Grootboek nr", 1 => "Omschrijving", 2 => "Kosten (excl BTW)");
  }
  else if($site == "toevkostenintern" || $site == "bewkostenintern" || $site == "overzkostenintern" || $site == "getkostenintern")
  {
      $koloms = array(0 => "ID", 1 => "Referentienr", 2 => "Datum", 3 => "Vervaldatum", 4 => "Project", 5 => "Grootboek nr", 6 => "Tegenrekening", 7 => "Beschrijvende naam", 8 => "Leverancier", 9 => "Kosten (excl btw)", 10 => "Btw tarief", 11 => "Bedrag wat wordt doorberekend aan klant", 12 => "Omschrijving", 13 => "BEDRIJFSID",  14 => "Factuurnr / bonnr (Crediteur)", 15 => "Factuur nr (Debiteur)", 16 => "Medewerker", 17 => "Betaald",     18 => "Horen deze kosten in het inkoopboek?", 19 => "Voeg scan van factuur toe", 20 => "Kostenplaats", 21 => "Kostensoort", 22 => "Aantal");
  }
  else if ($site == "login") {
      $koloms = array(0 => "Gebruikersnaam", 1 => "Medewerker", 2 => "Afgesloten account");
  } else if ($site == "login_klant" || $site == "login_account") {
      $koloms = array(0 => "Gebruikersnaam", 1 => "Wachtwoord", 2 => "Relatie", 3 => "Afgesloten account");
  }
  else if($site == "toevlogin" || $site == "bewlogin" || $site == "overzlogin" || $site == "getlogin")
  {
      $koloms = array(0 => "ID",
                      1 => "Gebruikersnaam",
                      2 => "Wachtwoord", 
                      3 => "Medewerker",
                      4 => "Beheerdersrechten",
                      5 => "Administratierechten",
                      6 => "Managementrapportages",
                      7 => "Urenregistratie",
                      8 => "Urenregistratie (methode 2)",
                      9 => "Handel",
                      10 => "Is deze persoon een klant?", 
                      11 => "E-mail",
                      12 => "SMS",
                      13 => "Formulier-templates",
                      14 => "Agenda",
                      15 => "In- en verkoopfacturen",
                      16 => "Projecten",
                      17 => "Bestanden", 
                      18 => "Kansen",
                      19 => "Nieuwsbrieven",
                      20 => "Is deze persoon een accountant?",
                      21 => "Relatiebeheer", 
                      22 => "Finance and Control",
                      23 => "Prikbord", 
                      24 => "Koppenklapper", 
                      25 => "Belangrijke FTP en internetadressen",
                      26 => "Offertegenerator", 
                      27 => "Forum / Chatten",
                      28 => "CV database",
                      29 => "Eigen personeelsdossier",
                      30 => "Indienen declaraties",
                      31 => "Taken",
                      32 => "Verlofadministratie", 
                      33 => "Boekhouding",
                      34 => "Capaciteitsplanning",
                      35 => "Polissen",
                      36 => "Personeelsbeheer",
                      37 => "Rittenadministratie", 
                      38 => "Planning", 
                      39 => "Website",
                      40 => "Security module", 
                      41 => "Kennisbank", 
                      42 => "Tekstverwerker", 
                      43 => "Telemarketing",
                      44 => "Detailplanning", 
                      45 => "Chatten",
                      46 => "Nieuwslezer (RSS)",
                      47 => "Notities", 
                      48 => "Verlof invoeren per uur", 
                      49 => "Verlof invoeren voor medewerker", 
                      50 => "Urenregistratie voor medewerker", 
                      51 => "Is deze gebruiker afgesloten?", 
                      52 => "Bedrijfsadministratie",
                      53 => "Thermometer",
                      54 => "Sitemap",
                      55 => "Contracten",
                      56 => "Alleen leesrechten",
                      57 => "Alleen toegang eigen administratie",
                      58 => "Style Digitaal Kantoor",
                      59 => "Profiel",
                      60 => "Importeren / Exporteren",
                      61 => "Zoekvragen",
                      62 => "Beheer over eigen profiel",
                      );
  }
  else if($site == "toevlogin_klant" || $site == "bewlogin_klant" || $site == "overzlogin_klant" || $site == "getlogin_klant")
  {
      $koloms = array(0 => "ID", 
                      1 => "Gebruikersnaam",
                      2 => "Wachtwoord", 
                      3 => "Relatie", 
                      4 => "Configuratierechten",
                      5 => "Administratierechten",
                      6 => "Managementrapportages",
                      7 => "Urenregistratie",
                      8 => "Urenregistratie (methode 2)",
                      9 => "Verkoop / verhuur goederen",
                      10 => "Is deze persoon een klant?", 
                      11 => "Webmail", 
                      12 => "SMS versturen",
                      13 => "Formulier-templates",
                      14 => "Agenda",
                      15 => "Facturen", 
                      16 => "Projecten",
                      17 => "Bestanden", 
                      18 => "Klantbeheer",
                      19 => "Nieuwsbrieven",
                      20 => "Is deze persoon een accountant?", 
                      21 => "Relatiebeheer", 
                      22 => "Finance and Control",
                      23 => "Prikbord", 
                      24 => "Koppenklapper", 
                      25 => "Belangrijke FTP en internetadressen", 
                      26 => "Aanmaken nieuwe offertes", 
                      27 => "Samenwerken",
                      28 => "Sollicitanten",
                      29 => "Eigen personeelsdossier",
                      30 => "Persoonlijke declaraties",
                      31 => "Taken",
                      32 => "Verlofadministratie", 
                      33 => "Boekhouding",
                      34 => "Volloop-leegloopschema", 
                      35 => "Polissen",
                      36 => "Personeelsbeheer",
                      37 => "Rittenadministratie", 
                      38 => "Planning", 
                      39 => "Website",
                      40 => "Security module",
                      41 => "Kennisbank", 
                      42 => "Tekstverwerker",
                      43 => "Telemarketing",
                      44 => "Deadlines", 
                      45 => "Chatten", 
                      46 => "Nieuwslezer (RSS)",
                      47 => "Notities", 
                      48 => "Verlof invoeren per uur", 
                      49 => "Verlof invoeren voor medewerker", 
                      50 => "Urenregistratie voor medewerker", 
                      51 => "Is deze gebruiker afgesloten?", 
                      52 => "Bedrijfsadministratie",
                      53 => "Thermometer",
                      54 => "Sitemap",
                      55 => "Contracten",
                      56 => "Alleen leesrechten",
                      57 => "Alleen toegang eigen administratie",
                      58 => "Style Digitaal Kantoor",
                      59 => "Profiel",
                      60 => "Importeren / Exporteren",
                      61 => "Zoekvragen",
                      62 => "Beheer over eigen profiel",
                      );
  }
  else if ($site == "ingelogt") {
      $koloms = array(0 => "Gebruiker", 1 => "Ip adres", 2 => "Datum", 3 => "Tijd");
  }
  else if($site == "toevingelogt" || $site == "bewingelogt" || $site == "overzingelogt" || $site == "getingelogt")
  {
      $koloms = array(0 => "ID", 1 => "Gebruiker", 2 => "Ip adres", 3 => "Datum", 4 => "Tijd", 5 => "User", 6 => "User", 6 => "Session", 7 => "BEDRIJFSID");
  }
  else if ($site == "deadlinetype") {
      $koloms = array(0 => "Beschrijvende naam", 1 => "Omschrijving");
  }
  else if($site == "toevdeadlinetype" || $site == "bewdeadlinetype" || $site == "overzdeadlinetype" || $site == "getdeadlinetype")
  {
      $koloms = array(0 => "ID", 1 => "Beschrijvende naam", 2 => "Volgorde in deadlines", 3 => "Omschrijving", 4 => "Standaard meenemen bij elk project", 5 => "BEDRIJFSID");
  }
  else if ($site == "documentflow") {
      $koloms = array(0 => "Document template", 1 => "Volgend document", 2 => "Dagen voordat wordt verzonden");
  }
  else if($site == "toevdocumentflow" || $site == "bewdocumentflow" || $site == "overzdocumentflow" || $site == "getdocumentflow")
  {
      $koloms = array(0 => "ID", 1 => "Document template", 2 => "Volgend document", 3 => "Dagen voordat wordt verzonden", 4 => "BEDRIJFSID");
  }
  else if ($site == "deadlines") {
      $koloms = array(0 => "Project", 1 => "Omschrijving", 2 => "Datum", 3 => "Tijd", 4 => "Medewerker", 5 => "Opmerking");
  }
  else if($site == "toevdeadlines" || $site == "bewdeadlines" || $site == "overzdeadlines" || $site == "getdeadlines")
  {
      $koloms = array(0 => "ID", 1 => "Project", 2 => "Deadlineomschrijving", 3 => "Startdatum (planning)", 4 => "Einddatum (planning)", 5 => "TIJD",    6 => "Deadline (werkenlijk)", 7 => "Medewerker",             8 => "Opmerking", 9 => "Afgerond",    10 => "BEDRIJFSID");
  }
  else if ($site == "tabbladen") {
      $koloms = array(0 => "Tabblad", 1 => "Functie");
  }
  else if($site == "toevtabbladen" || $site == "bewtabbladen" || $site == "overztabbladen" || $site == "gettabbladen")
  {
      $koloms = array(0 => "ID", 1 => "Medewerker", 2 => "Tabblad", 3 => "Functie", 4 => "BEDRIJFSID");
  }
  else if ($site == "mrktinglead") {
      $koloms = array(0 => "Klant", 1 => "Naam", 2 => "Status", 3 => "Bedrag");
  }
  else if($site == "toevmrktinglead" || $site == "bewmrktinglead" || $site == "overzmrktinglead" || $site == "getmrktinglead")
  {
      $koloms = array(0 => "ID",    1 => "Klant",  2 => "Naam", 3 => "Status", 4 => "Status uitleg", 5 => "Bedrag", 6 => "Bron", 7 => "Bron uitleg", 8 => "Campagne", 9 => "Aangebracht via", 10 => "Wil niet gebeld worden", 11 => "Omschrijving", 12 => "Medewerker", 13 => "Datum", 14 => "Aangepast", 15 => "BEDRIJFSID");
  }
  else if ($site == "grootboekitems") {
      $koloms = array(0 => "Rubriek", 1 => "Nummer", 2 => "Beschrijvende naam", 3 => "Waar?", 4 => "Credit / Debet");
  }
  else if($site == "toevgrootboekitems" || $site == "bewgrootboekitems" || $site == "overzgrootboekitems" || $site == "getgrootboekitems")
  {
      $koloms = array(0 => "ID", 1 => "Nummer", 2 => "Balans?",     3=> "Credit of Debet", 4 => "Beschrijvende naam", 5 => "Omschrijving", 6 => "Aanpasbaar", 7 => "Hoofdrubriek", 8 => "Type", 9 => "BTW Percentage", 10 => "Kleur", 11 => "Belastingpost", 12 => "Toon in grafiek", 13 => "Is salarispost", 14 => "Project", 15 => "BEDRIJFSID", 16 => "Geblokkeerd", 17 => "Niet beschikbaar in administratie", 18 => "Dagboek");
  }
  else if ($site == "balans") {
      $koloms = array(0 => "Grootboek nr", 1 => "Tegenrekening", 2 => "Bedrag", 3 => "Boekdatum");
  }
  else if($site == "toevbalans" || $site == "bewbalans" || $site == "overzbalans" || $site == "getbalans")
  {
      $koloms = array(0 => "ID", 1 => "Rubriek / Grootboekrekening", 2 => "Tegenrekening", 3 => "Relatie", 4 => "Inkoopboek", 5 => "Verkoopboek", 6 => "Bedrag", 7 => "Boek jaar", 8 => "Boekdatum", 9 => "Omschrijving", 10 => "Type dagboek", 11 => "BEDRIJFSID");
  }
  else if ($site == "afsluitenboekjaar") {
      $koloms = array(0 => "Vanaf", 1 => "Tot", 2 => "Afgesloten");
  }
  else if($site == "toevafsluitenboekjaar" || $site == "bewafsluitenboekjaar" || $site == "overzafsluitenboekjaar" || $site == "getafsluitenboekjaar")
  {
      $koloms = array(0 => "ID", 1 => "Vanaf", 2 => "Tot", 3 => "Afgesloten", 4 => "BEDRIJFSID");
  }
  else if ($site == "balansrubriek") {
      $koloms = array(0 => "Naam", 1 => "Omschrijving");
  }
  else if($site == "toevbalansrubriek" || $site == "bewbalansrubriek" || $site == "overzbalansrubriek" || $site == "getbalansrubriek")
  {
      $koloms = array(0 => "ID", 1 => "Naam", 2 => "Omschrijving", 3 => "BEDRIJFSID");
  }
  else if ($site == "openingsbalans") {
      $koloms = array(0 => "Grootboekrekening", 1 => "Boekjaar", 2 => "Debet", 3 => "Credit");
  }
  else if($site == "toevopeningsbalans" || $site == "bewopeningsbalans" || $site == "overzopeningsbalans" || $site == "getopeningsbalans")
  {
      $koloms = array(0 => "ID", 1 => "Grootboekrekening", 2 => "Boekjaar", 3 => "Debet", 4 => "Credit", 5 => "Opmerking", 6 => "BEDRIJFSID");
  }
  else if ($site == "journaal") {
      $koloms = array(0 => "Grootboek nr", 1 => "Bedrag", 2 => "Datum");
  }
  else if($site == "toevjournaal" || $site == "bewjournaal" || $site == "overzjournaal" || $site == "getjournaal")
  {
      $koloms = array(0 => "ID",    1 => "Datum", 2 => "Grootboekrekening", 3 => "Tegenrekeneing", 4 => "Verkoopboek", 5 => "Inkoopboek",     6 => "Bedrag", 7 => "Omschrijving", 8 => "BEDRIJFSID");
  }
  else if($site == "btwaangifte") {
      $koloms = array(0 => "Datum", 1 => "Omzet hoog", 2 => "Omzet laag", 3 => "Omzet 0%", 4 => "Omzet vrij");
  }
  else if($site == "toevbtwaangifte" || $site == "bewbtwaangifte" || $site == "overzbtwaangifte" || $site == "getbtwaangifte")
  {
      $koloms = array(0 => "ID", 1 => "Datum", 2 => "Binnenland hoog", 3 => "Binnenland laag", 4 => "Aan mij geleverde diensten binnenland hoog", 5 => "Aan mij geleverde diensten Binnenland laag", 6 => "Buitenland hoog", 7 => "Buitenland laag", 8 => "Aan mij geleverde diensten Buitenland hoog", 9 => "Aan mij geleverde diensten Buitenland laag", 10 => "Binnenland omzet hoog", 11 => "Binnenland omzet laag", 12 => "Binnenland omzet nul", 13 => "Binnenland omzet vrij", 14 => "Binnenland inkoop hoog", 15 => "Binnenland inkoop laag", 16 => "Binnenland inkoop nul", 17 => "Binnenland inkoop vrij", 18 => "Buitenland omzet hoog", 19 => "Buitenland omzet laag", 20 => "Buitenland omzet nul", 21 => "Buitenland omzet vrij", 22 => "Buitenland inkoop hoog", 23 => "BUIinkoop laag", 24 => "Buitenland inkoop nul", 25 => "Buitenland inkoop vrij", 26 => "BEDRIJFSID");
  }
  else if ($site == "rittenadmin") {
      $koloms = array(0 => "Datum", 1 => "Beginstand", 2 => "Eindstand",  3 => "Klant",  4 => "Van",  5 => "Naar");
  }
  else if($site == "toevrittenadmin" || $site == "bewrittenadmin" || $site == "overzrittenadmin" || $site == "getrittenadmin")
  {
      $koloms = array(0 => "ID",        1 => "Beginstand teller", 2 => "Eindstand teller", 3 => "Datum",          4 => "Van",    5 => "Naar",  6 => "Omschrijving", 7 => "Klant",        8 => "Project",   9 => "Personeelslid",      10 => "Kenteken", 11 => "Prijs per km", 12 => "Behoort bij factuur", 13 => "Is dit uw prive auto?", 14 => "BEDRIJFSID");
  }
  else if ($site == "brainstorm") {
      $koloms = array(0 => "Type", 1 => "Naam",2 => "Datum", 3 => "Gekoppelt aan project");
  }
  else if($site == "toevbrainstorm" || $site == "bewbrainstorm" || $site == "overzbrainstorm" || $site == "getbrainstorm")
  {
      $koloms = array(0 => "ID",    1 => "Naam",  2 => "Type", 3 => "Datum",      4 => "Personeelslid", 5 => "Gekoppelt aan project", 6 => "BEDRIJFSID");
  }
  else if ($site == "discussion") {
      $koloms = array(0 => "Type", 1 => "Stelling",2 => "Datum");
  }
  else if($site == "toevdiscussion" || $site == "bewdiscussion" || $site == "overzdiscussion" || $site == "getdiscussion")
  {
      $koloms = array(0 => "ID",    1 => "Type",  2 => "Omschrijving", 3 => "Onderdeel van",   4 => "Personeelslid", 5 => "Datum", 6 => "BEDRIJFSID");
  }
  else if ($site == "reacties") {
      $koloms = array(0 => "Opmerking", 1 => "Datum",2 => "Tijd");
  }
  else if($site == "toevreacties" || $site == "bewreacties" || $site == "overzreacties" || $site == "getreacties")
  {
      $koloms = array(0 => "ID",    1 => "Opmerking",  2 => "Eens / Voor", 3 => "Oneens / Tegen",  4 => "Geen mening / Onthouding", 5 => "Anders", 6 => "Anders", 7 => "BEDRIJFSID", 8 => "Datum",    9 => "Tijdstip", 10 => "Stelling / Vraag / Poll", 11 => "Anderereacties", 12 => "BEDRIJFSID");
  }
  else if($site == "toevpoll" || $site == "getpoll")
  {
      $koloms = array(0 => "ID",    1 => "Opmerking",  2 => "Eens / Voor", 3 => "Oneens / Tegen",  4 => "Geen mening / Onthouding", 5 => "Anders", 6 => "Anders", 7 => "BEDRIJFSID", 8 => "Datum",    9 => "Tijdstip", 10 => "Stelling / Vraag / Poll", 11 => "Anderereacties", 12 => "BEDRIJFSID");
  }
  else if ($site == "agendanationaal") {
      $koloms = array(0 => "Naam", 1 => "Van", 2 => "Tot", 3 => "Iedereen vrij");
  }
  else if($site == "toevagendanationaal" || $site == "bewagendanationaal" || $site == "overzagendanationaal" || $site == "getagendanationaal")
  {
      $koloms = array(0 => "ID", 1 => "Naam", 2 => "Omschrijving", 3 => "Datum (van)", 4 => "Datum (tot)", 5 => "Iederjaar op dezelfde dag", 6 => "Hebben alle werknemers een dag vrij?", 7 => "BEDRIJFSID");
  }
  else if ($site == "corve") {
      $koloms = array(0 => "Naam", 1 => "Van", 2 => "Tot", 3 => "Omschrijving");
  }
  else if($site == "toevcorve" || $site == "bewcorve" || $site == "overzcorve" || $site == "getcorve")
  {
      $koloms = array(0 => "ID", 1 => "Dag van de week", 2 => "Tijdstip", 3 => "Medewerker", 4 => "Omschrijving", 5 => "BEDRIJFSID");
  }
  else if ($site == "leads") {
      $koloms = array(0 => "Naam", 1 => "Datum", 2 => "Reporter");
  }
  else if($site == "toevleads" || $site == "bewleads" || $site == "overzleads" || $site == "getleads")
  {
      $koloms = array(0 => "ID", 1 => "Beschrijvende naam", 2 => "Datum", 3 => "Medewerker", 4 => "Alle relaties", 5 => "Omschrijving", 6 => "BEDRIJFSID");
  }
  else if ($site == "abcdinet") {
      $koloms = array(0 => "Typerend woord", 1 => "Tekst");
  }
  else if($site == "toevabcdinet" || $site == "bewabcdinet" || $site == "overzabcdinet" || $site == "getabcdinet")
  {
      $koloms = array(0 => "ID",    1 => "Kenmerkend woord", 2 => "Tekst", 3 => "Medewerker", 4 => "BEDRIJFSID");
  }
  else if ($site == "documentbeheer") {
      $koloms = array(0 => "Naam", 1 => "Bestand");
  }
  else if($site == "toevdocumentbeheer" || $site == "bewdocumentbeheer" || $site == "overzdocumentbeheer" || $site == "getdocumentbeheer")
  {
      $koloms = array(0 => "ID",    1 => "Naam", 2 => "Omschrijving", 3 => "Categorie",    4 => "Rechten", 5 => "Personeelslid", 6 => "Bestand", 7 => "Filename", 8 => "Type", 9 => "Size", 10 => "Content", 11 => "TAGS", 12 => "Datum", 13 => "BEDRIJFSID");
  }
  else if ($site == "documentcatagorie") {
      $koloms = array(0 => "Omschrijving");
  }
  else if($site == "toevdocumentcatagorie" || $site == "bewdocumentcatagorie" || $site == "overzdocumentcatagorie" || $site == "getdocumentcatagorie")
  {
      $koloms = array(0 => "ID", 1 => "Is onderdeel van", 2 => "Naam", 3 => "BEDRIJFSID");
  }
  else if ($site == "liquiditeitsbegroting") {
      $koloms = array(0 => "Omschrijving");
  }
  else if($site == "toevliquiditeitsbegroting" || $site == "bewliquiditeitsbegroting" || $site == "overzliquiditeitsbegroting" || $site == "getliquiditeitsbegroting")
  {
      $koloms = array(0 => "ID", 1 => "Kosten / Opbrengsten", 2 => "Naam", 3 => "Bedrag", 4 => "Omschrijving", 5 => "Datum", 6 => "BEDRIJFSID");
  }
  else if ($site == "projectbegroting") {
      $koloms = array(0 => "Omschrijving");
  }
  else if($site == "toevprojectbegroting" || $site == "bewprojectbegroting" || $site == "overzprojectbegroting" || $site == "getprojectbegroting")
  {
      $koloms = array(0 => "ID", 1 => "Kosten / Opbrengsten", 2 => "Naam", 3 => "Bedrag", 4 => "Omschrijving", 5 => "Datum", 6 => "BEDRIJFSID");
  }
  else if ($site == "text") {
    $koloms = array(0 => "Niveau", 1 => "Pagina titel", 2 => "Volgorde", 3 => "Online", 4 => "Url");
  }
  else if($site == "toevtext" || $site == "bewtext" || $site == "overztext" || $site == "gettext")
  {
    $koloms = array(0 => "ID", 1 => "URL", 2 => "Niveau", 3 => "Onderdeel van", 4 => "Type website", 5 => "Pagina titel", 6 => "Soort", 7 => "Positie in menu", 8 => "Online zichtbaar", 9 => "Tekst", 10 => "Hits", 11 => "Pagina beschrijving (metadata)", 12 => "Pagina tags (metadata)", 13 => "Pagina titel (metadata)", 14 => "BEDRIJFSID");
  }
  else if ($site == "intranet") {
    $koloms = array(0 => "Niveau", 1 => "Pagina titel", 2 => "Type tekst", 3 => "Volgorde", 4 => "Online");
  }
  else if($site == "toevintranet" || $site == "bewintranet" || $site == "overzintranet" || $site == "getintranet")
  {
    $koloms = array(0 => "ID", 1 => "Niveau", 2 => "Onderdeel van", 3 => "Type website", 4 => "Pagina titel", 5 => "Soort", 6 => "Positie in menu", 7 => "Online zichtbaar", 8 => "Tekst", 9 => "Hits", 10 => "BEDRIJFSID");
  }
  else if ($site == "trash") {
    $koloms = array(0 => "Niveau", 1 => "Pagina titel", 2 => "Type tekst", 3 => "Online");
  }
  else if ($site == "sitestyle") {
    $koloms = array(0 => "Url", 1 => "Template", 2 => "field", 3 => "Basislocatie");
  }
  else if($site == "toevsitestyle" || $site == "bewsitestyle" || $site == "overzsitestyle" || $site == "getsitestyle")
  {
    $koloms = array(0 => "ID", 1 => "Url", 2 => "Template", 3 => "Titel", 4 => "Basislocatie", 5 => "BEDRIJFSID");
  }
  else if ($site == "extramodules") {
      $koloms = array(0 => "Naam", 1 => "Link", 2 => "Rechten");
  }
  else if($site == "toevextramodules" || $site == "bewextramodules" || $site == "overzextramodules" || $site == "getextramodules")
  {
      $koloms = array(0 => "ID", 1 => "Naam", 2 => "Link", 3 => "Doel", 4 => "Plaatje", 5 => "Volgorde", 6 => "Rechten", 7 => "BEDRIJFSID");
  }
  else if ($site == "logboek") {
      $koloms = array(0 => "Categorie", 1 => "Datum", 2 => "Medewerker", 2 => "Omschrijving");
  }
  else if($site == "toevlogboek" || $site == "bewlogboek" || $site == "overzlogboek" || $site == "getlogboek")
  {
      $koloms = array(0 => "ID", 1 => "Categorie", 2 => "Beschrijvende naam", 3 => "Omschrijving", 4 => "Datum", 5 => "Medewerker", 6 => "BEDRIJFSID");
  }
  else if ($site == "stam_titels") {
      $koloms = array(0 => "Titel", 1 => "Voor of achter de naam");
  }
  else if($site == "toevstam_titels" || $site == "bewstam_titels" || $site == "overzstam_titels" || $site == "getstam_titels")
  {
      $koloms = array(0 => "ID", 1 => "Titel", 2 => "Voor of achter de naam", 3 => "BEDRIJFSID");
  }
  else if ($site == "stam_productcat") {
      $koloms = array(0 => "Naam", 1 => "Standaard omzetrekening", 2 => "Standaard kostenrekening", 3 => "Standaard voorraadrekening");
  }
  else if($site == "toevstam_productcat" || $site == "bewstam_productcat" || $site == "overzstam_productcat" || $site == "getstam_productcat")
  {
      $koloms = array(0 => "ID", 1 => "Is subcategorie van", 2 => "Naam", 3 => "Opmerking", 4 => "Standaard omzetrekening", 5 => "Standaard voorraadrekening", 6 => "Standaard kostenrekening", 7 => "BEDRIJFSID", 8 => "Volgordenummer");
  }
  else if ($site == "stam_ordners") {
      $koloms = array(0 => "Naam", 1 => "Hangt onder");
  } 
  else if($site == "toevstam_ordners" || $site == "bewstam_ordners" || $site == "overzstam_ordners" || $site == "getstam_ordners")
  {
      $koloms = array(0 => "ID", 1 => "Is subcategorie van", 2 => "Name", 3 => "Description", 4  => "BEDRIJFSID");
  }
  else if ($site == "stam_mrtkstatus") {
      $koloms = array(0 => "Naam", 1 => "Valt onder");
  }
  else if($site == "toevstam_mrtkstatus" || $site == "bewstam_mrtkstatus" || $site == "overzstam_mrtkstatus" || $site == "getstam_mrtkstatus")
  {
      $koloms = array(0 => "ID", 1 => "Is subcategorie van", 2 => "Naam", 3 => "Kans", 4 => "BEDRIJFSID");
  }
  else if(substr($site,0,5) == "stam_" || substr($site,0,9) == "verwstam_" || substr($site,0,9) == "toevstam_" || substr($site,0,8) == "bewstam_" || substr($site,0,10) == "overzstam_" || substr($site,0,8) == "getstam_")
  {
      if ($site == $tabel) { $koloms = array(0 => "Naam", 1 => "Omschrijving"); }
      if ($site == "toev$tabel" || $site == "bew$tabel" || $site == "overz$tabel" || $site == "get$tabel") { $koloms = array(0 => "ID", 1 => "Naam", 2 => "Omschrijving", 3 => "BEDRIJFSID"); }
  }
  else if ($site == "typerelatie") {
      $koloms = array(0 => "Naam", 1 => "Opgenomen relaties");
  }
  else if($site == "toevtyperelatie" || $site == "bewtyperelatie" || $site == "overztyperelatie" || $site == "gettyperelatie")
  {
      $koloms = array(0 => "ID", 1 => "Naam", 2 => "Deze relaties opnemen in alle uitvalmenutjes", 3 => "Opmerking", 4 => "BEDRIJFSID");
  }
  else if($site == "formulieren") {
      $koloms = array(0 => "Formuliernaam", 1 => "Formuliertype");
  }
  else if($site == "toevformulieren" || $site == "bewformulieren" || $site == "overzformulieren" || $site == "getformulieren")
  {
      $koloms = array(0 => "ID", 1 => "Formuliernaam", 2 => "Type formulier", 3 => "Bedrijfsnaam", 4 => "Geslacht",   5 => "T.n.v. (Contactpersoon)",  6 => "Voorletters (Contactpersoon)", 7 => "Contactpersoon",  8 => "Email (Contactpersoon)", 9 => "Telefoon (Contactpersoon)", 10 => "Mobiel (Contactpersoon)", 11 => "Adres",       12 => "Land (Kantoor)", 13 => "Adres (Kantoor)", 14 => "Telefooon (Kantoor)", 15 => "Fax (Kantoor)", 16 => "Homepage (Kantoor)", 17 => "Email (Kantoor)", 18 => "Postbus",     19 => "Kamer van Koophandel Nr", 20 => "BTW Nr",      21 => "Verzendwijze", 22 => "Rekening Nr", 23 => "Ten name van", 24 => "Rekening plaats", 25 => "Creditcard Nr", 26 => "Type kaart", 27 => "Vervaldatum kaart", 28 => "Marktsegment", 29 => "Geboortedatum", 30 => "Wie is verantwoordelijk voor deze relatie", 31 => "Start-datum", 32 => "Eind-datum", 33 => "Aantal (type 1)", 34 => "Aantal (type 2)", 35 => "Aantal (type 3)", 36 => "Aantal (type 4)", 37 => "Hoe heeft u ons gevonden", 38 => "Bijzonderheden", 39 => "BEDRIJFSID");
  }
  else if($site == "formuliereningevult") {
      $koloms = array(0 => "Invuldatum", 1 => "Formulier", 2 => "Naam", 3 => "Bijzonderheden");
  }
  else if($site == "toevformuliereningevult" || $site == "bewformuliereningevult" || $site == "overzformuliereningevult" || $site == "getformuliereningevult")
  {
      $koloms = array(0 => "ID", 1 => "Invuldatum", 2 => "Formulier",    3 => "Bedrijfsnaam", 4 => "Geslacht",       5 => "T.n.v.",     6 => "Voorletters", 7 => "Voornaam", 8 => "Tussenvoegsel", 9 => "Achternaam", 10 => "Email", 11 => "Telefoon", 12 => "Mobiel", 13 => "Adres", 14 => "Huis Nr", 15 => "Toevoeging", 16 => "Postcode", 17 => "Plaats", 18 => "Land", 19 => "Adres (Kantoor)", 20 => "Huis Nr (Kantoor)", 21 => "Toevoeging (Kantoor)", 22 => "Postcode (Kantoor)", 23 => "Vestigingsplaats (Kantoor)", 24 => "Telefooon (Kantoor)", 25 => "Fax (Kantoor)", 26 => "Homepage (Kantoor)", 27 => "Email (Kantoor)", 28 => "Postbus", 29 => "Postcode van de postbus", 30 => "Kamer van Koophandel Nr", 31 => "BTW Nr", 32 => "Factuur per mail?", 33 => "Rekening Nr", 34 => "Ten name van", 35 => "Rekening plaats", 36 => "Creditcard Nr", 37 => "Type kaart", 38 => "Vervaldatum kaart", 39 => "Marktsegment", 40 => "Geboortedatum", 41 => "Wie is verantwoordelijk voor deze relatie", 42 => "Start-datum", 43 => "Eind-datum",    44 => "Aantal (type 1)", 45 => "Aantal (type 2)", 46 => "Aantal (type 3)", 47 => "Aantal (type 4)", 48 => "Hoe heeft de klant ons gevonden?", 49 => "Wie heeft hem gevonden?", 50 => "Bijzonderheden", 51 => "BEDRIJFSID");
  }
  else if($site == "telemarkt") {
      $koloms = array(0 => "Formuliernaam", 1 => "Formuliertype");
  }
  else if($site == "toevtelemarkt" || $site == "bewtelemarkt" || $site == "overztelemarkt" || $site == "gettelemarkt")
  {
      $koloms = array(0 => "ID", 1 => "Formuliernaam", 2 => "Hangt onder sessie",3 => "Type",         4 => "Tekst",   5 => "Volgorde",  6 => "Stats", 7 => "BEDRIJFSID");
  }
  else if ($site == "telemarktform") {
      $koloms = array(0 => "Naam", 1 => "Opmerking");
  }
  else if($site == "toevtelemarktform" || $site == "bewtelemarktform" || $site == "overztelemarktform" || $site == "gettelemarktform")
  {
      $koloms = array(0 => "ID", 1 => "Naam", 2 => "Welke klanten zijn gebelt?", 3 => "Opmerking", 4 => "BEDRIJFSID");
  }
  else if ($site == "watisergebeurt") {
      $koloms = array(0 => "Datum", 1 => "Omschijving");
  }
  else if($site == "toevwatisergebeurt" || $site == "bewwatisergebeurt" || $site == "overzwatisergebeurt" || $site == "getwatisergebeurt")
  {
      $koloms = array(0 => "ID", 1 => "Datum", 2 => "Omschijving", 3 => "Actie", 4 => "Tabel", 5 => "ItemId", 6 => "BEDRIJFSID");
  }
  else if ($site == "cursus") {
      $koloms = array(0 => "Naam", 1 => "Instelling", 2 => "Medewerker");
  }
  else if($site == "toevcursus" || $site == "bewcursus" || $site == "overzcursus" || $site == "getcursus")
  {
      $koloms = array(0 => "ID", 1 => "Naam cursus", 2 => "Zoekterm van de cursus", 3 => "Instelling die de cursus verzorgt", 4 => "Kosten van de cursus", 5 => "Cursus inhoud &  programma", 6 => "Hoeveel tijd neemt het in beslag", 7 => "Cursus start op", 8 => "Cursus eindigd op", 9 => "Waar is meer informatie te vinden", 10 => "Waardering uitgedrukt in getal", 11 => "Ervaring en waardering opmerkingen", 12 => "Medewerker", 13 => "BEDRIJFSID");
  }
  else if ($site == "pers_loonstrook") {
      $koloms = array(0 => "Medewerker", 1 => "Datum", 2 => "Bestand");
  }
  else if($site == "toevpers_loonstrook" || $site == "bewpers_loonstrook" || $site == "overzpers_loonstrook" || $site == "getpers_loonstrook")
  {
      $koloms = array(0 => "ID", 1 => "Medewerker", 2 => "Datum", 3 => "Bestand", 4 => "Omschrijving", 5 => "BEDRIJFSID");
  }
  else if ($site == "selection") {
      $koloms = array(0 => "Naam", 1 => "Velden", 2 => "Zoeken in");
  }
  else if($site == "bewselection" || $site == "overzselection" || $site == "overzselectionExcel" || $site == "getselection")
  {
      $koloms = array(0 => "ID", 1 => "Beschrijvende naam", 2 => "Toon velden", 3 => "Uit de tabel", 4 => "Onder voorwaarde", 5 => "Order op", 6 => "Limiteer uitkomst", 7 => "Medewerker", 8 => "Administratie");
  }
  else if ($site == "attenderingen") {
      $koloms = array(0 => "Onderwerp", 1 => "Datum", 2 => "Tijd", 3 => "Medewerker");
  }
  else if($site == "toevattenderingen" || $site == "bewattenderingen" || $site == "overzattenderingen" || $site == "getattenderingen")
  {
      $koloms = array(0 => "ID", 1 => "Herinner mij aan", 2 => "Tekst", 3 => "Datum", 4 => "Tijd", 5 => "PERSMS", 6 => "PERSONEELSLID", 7 => "BEDRIJFSID");
  }
  else if ($site == "verkochtderving") {
      $koloms = array(0 => "Datum", 1 => "Artikel", 2 => "Onverklaarbaar", 3 => "Derving", 4 => "Retour");
  }
  else if($site == "toevverkochtderving" || $site == "bewverkochtderving" || $site == "overzverkochtderving" || $site == "getverkochtderving")
  {
      $koloms = array(0 => "ID",    1 => "Datum",        2 => "Artikel",  3 => "Onverklaarbaar",  4 => "Derving", 5 => "Retour", 6 => "Varanten", 7 => "BEDRIJFSID");
  }
  else if ($site == "kosteninternspecs") {
      $koloms = array(0 => "Referentie", 1 => "Project", 2 => "Aantal", 3 => "Eenheid", 4 => "Omschrijving");
  }
  else if($site == "toevkosteninternspecs" || $site == "bewkosteninternspecs" || $site == "overzkosteninternspecs" || $site == "getkosteninternspecs")
  {
      $koloms = array(0 => "ID", 1 => "Referentie", 2 => "Datum", 3 => "Project", 4 => "Omschrijving", 5 => "Inkoopprijs", 6 => "Verkoopprijs", 7 => "Kostenplaats", 8 => "Kostensoort", 9 => "Aantal", 10 => "Eenheid", 11 => "BEDRIJFSID");
  }
  else if ($site == "orderbevestiging") {
      $koloms = array(0 => "BONNR", 1 => "BESTELDATUM", 2 => "WANNEERUITGELEVERD", 3 => "OFFERTEAANGEMAAKT", 4 => "STATUS");
  }
  else if($site == "toevorderbevestiging" || $site == "beworderbevestiging" || $site == "overzorderbevestiging" || $site == "getorderbevestiging")
  {
      $koloms = array(0 => "ID", 1 => "BONNR", 2 => "BESTELDATUM", 3 => "WANNEERUITGELEVERD", 4 => "OFFERTEAANGEMAAKT", 5 => "OFFERTEGOEDGEKEURD", 6 => "OFFERTEDOORGESTUURD", 7 => "ORDERBEVAANGEMAAKT", 8 => "ORDERBEVGOEDGEKEURD", 9 => "ORDERBEVDOORGESTUURD", 10 => "DRUKPROEFAANGEMAAKT", 11 => "DRUKPROEFGOEDGEKEURD", 12 => "DRUKPROEFDOORGESTUURD", 13 => "INGEKOCHT", 14 => "AFGEBROKEN", 15 => "WENSONTVANGDATUM", 16 => "TEXTID", 17 => "OFFERTE", 18 => "OPDRACHTBEVESTIGING", 19 => "DRUKPROEF", 20 => "VERSTUURD", 21 => "ISUITGELEVERD", 22 => "OPMERKING", 23 => "GEFACTUREERD", 24 => "STATUS", 25 => "BEDRIJFSID");
  }
  else if ($site == "dossiers") {
      $koloms = array(0 => "Naam", 1 => "Datum tijd", 2 => "Medewerker", 3 => "Gekoppelt aan");
  }
  else if($site == "toevdossiers" || $site == "bewdossiers" || $site == "overzdossiers" || $site == "getdossiers")
  {
      $koloms = array(0 => "ID", 1 => "Dossier nr.", 2 => "Naam", 3 => "Startdatum", 4 => "Medewerker", 5 => "Opmerking", 6 => "Koppeling", 7 => "BEDRIJFSID");
  }
  else if ($site == "factuurtemplate") {
      $koloms = array(0 => "Logo op factuur", 1 => "Uren op factuur", 2 => "Footer op factuur");
  }
  else if($site == "toevfactuurtemplate" || $site == "bewfactuurtemplate" || $site == "overzfactuurtemplate" || $site == "getfactuurtemplate")
  {
      $koloms = array(0 => "ID", 1 => "Logo op factuur", 2 => "Uren op factuur", 3 => "Factuuropmaak", 4 => "Factuurtekst", 5 => "Eventueel andere factuuropmaak voor incasso facturen", 6 => "Tekst boven of onder specificatie tonen", 7 => "Footer op factuur", 8 => "Standaard tekst voor de mail", 9 => "Tekst voor de mail na vervaldatum factuur", 10 => "BEDRIJFSID", 11 => "Standaard tekst voor de mail bij automatisch incasso facturen");
  }
  else if ($site == "polissen") {
      $koloms = array(0 => "Polisnummer", 1 => "Status", 2 => "Product", 3 => "Maatschappij", 4 => "Klant", 5 => "Adviseur", 6 => "Ingangsdatum", 7 => "Einddatum");
  }
  else if($site == "toevpolissen" || $site == "bewpolissen" || $site == "overzpolissen" || $site == "getpolissen")
  {
      $koloms = array(0 => "ID", 1 => "Polisnummer", 2 => "Status", 3 => "Product", 4 => "Maatschappij", 5 => "Klant", 6 => "Mutatiedatum", 7 => "Ingangsdatum", 8 => "Einddatum", 9 => "Adviseur", 10 => "Soort verzekering", 11 => "Koopsom", 12 => "Premie", 13 => "Premie frequentie", 14 => "Rente", 15 => "Looptijd rente", 16 => "Clausulering", 17 => "Toelichting", 18 => "IPBEDRAG", 19 => "IPAANTALKEER", 20 => "IPLOOPTIJD", 21 => "IPFREQUENTIE", 22 => "APBEDRAG", 23 => "APAANTALKEER", 24 => "APLOOPTIJD", 25 => "APFREQUENTIE", 26 => "Ingevoerd door", 27 => "BEDRIJFSID");
  }
  else if ($site == "polis_soortverzekering") {
      $koloms = array(0 => "Naam");
  }
  else if($site == "toevpolis_soortverzekering" || $site == "bewpolis_soortverzekering" || $site == "overzpolis_soortverzekering" || $site == "getpolis_soortverzekering")
  {
      $koloms = array(0 => "ID", 1 => "Naam", 2 => "STARTDATUM", 3 => "EINDDATUM", 4 => "EINDDATUMPREMIEBET", 5 => "KOOPSOM", 6 => "PREMIE", 7 => "PREMIEFREQUENTIE", 8 => "RENTE", 9 => "LOOPTIJDRENTE", 10 => "CLAUSULERING", 11 => "BELEGGEN", 12 => "BEDRIJFSID");
  }
  else if ($site == "contracten") {
      $koloms = array(0 => "Referentie", 1 => "Type", 2 => "Betrekking op", 3 => "Naam", 4 => "Ingangsdatum", 5 => "Einddatum", 6 => "Contract waarde");
  }
  else if($site == "toevcontracten" || $site == "bewcontracten" || $site == "overzcontracten" || $site == "getcontracten")
  {
      $koloms = array(0 => "ID", 1 => "Reference", 2 => "Type", 3 => "Items", 4 => "Name", 5 => "Description", 6 => "Supplier", 7 => "External Reference", 8 => "Signed on", 9 => "Signed By", 10 => "Start Date", 11 => "Expiration", 12 => "Approved", 13 => "Valid until", 14 => "Renewal Frequancy", 15 => "Renewal Period", 16 => "Renewal manually", 17 => "Experation Frequency", 18 => "Experation Period", 19 => "Billing Frequency", 20 => "Cost per period", 21 => "Connections", 22 => "Tags", 23 => "Total value", 24 => "Entity", 25 => "Last modified");
  } else if($site == "bewideal")
  {
      $koloms = array(0 => "ID", 1 => "Sleutel", 2 => "iDeal URL van uw bank", 3 => "Acceptant ID", 4 => "MERCHANTRETURNURL", 5 => "Certificaat", 6 => "Private Key", 7 => "BEDRIJFSID");
  } else if($site == "bewmsp")
  {
      $koloms = array(0 => "ID", 1 => "Account Id", 2 => "Site Id", 3 => "Site code", 4 => "MultiSafepay grootboekrekening", 5 => "BEDRIJFSID");
  } else if ($site == "mrktingcampagne") {
      $koloms = array(0 => "Naam", 1 => "Status", 2 => "Start op", 3 => "Loopt tot", 4 => "Verwachte omzet", 5 => "Verwachte kosten");
  }
  else if($site == "toevmrktingcampagne" || $site == "bewmrktingcampagne" || $site == "overzmrktingcampagne" || $site == "getmrktingcampagne")
  {
      $koloms = array(0 => "ID",    1 => "Naam",  2 => "Omschrijving", 3 => "Type", 4 => "Status", 5 => "Budget", 6 => "Verwachte omzet", 7 => "Verwachte kosten", 8 => "Start op", 9 => "Loopt tot", 10 => "BEDRIJFSID");
  } else {
      $koloms = array(0 => "Naam");
  }

  return $koloms;
}
?>