<?php

function getSiteKolom($site, $tabel = "", $vakTekts = "")
{
    global $db;

    //Bugs
    if ($site == "bugs") {
        $koloms = array(0 => "Bug No.", 1 => "Name", 2 => "Category", 3 => "Type", 4 => "Status", 5 => "Priority");
    }
    else if ($site == "bugsLeeg") {
        $koloms = array(0 => "Bug No.", 1 => "Name", 2 => "Category", 3 => "Type", 4 => "Status", 5 => "Priority");
    }
    else if($site == "toevbugs" || $site == "bewbugs" || $site == "overzbugs" || $site == "bugsCustomer" || $site == "getbugs")
    {
        $koloms = array(0 => "ID", 1 => "Bug No.", 2 => "Name", 3 => "Project", 4 => "Customer", 5 => "Category", 6 => "Type", 7 => "Status", 8 => "Priority", 9 => "", 10 => "Reported by", 11 => "Assigned to", 12 => "Invoerdate", 13 => "Last update", 14 => "BEDRIJFSID");
    }
    else if ($site == "offerteCustomer") {
        $koloms = array(0 => "Project No.", 1 => "Name", 2 => "Status", 3 => "Deadline", 4 => "Bugs");
    }
    else if($site == "facturenCustomer") {
        $koloms = array(0 => "Invoice No.", 1 => "Project No.", 2 => "Name", 3 => "Date", 4 => "Amount");
    }
    else if($site == "aanmaningenCustomer") {
        $koloms = array(0 => "Invoice No.", 1 => "Customer", 2 => "Date", 3 => "Amount");
    }
    else if($site == "vakantiesCustomer") {
        $koloms = array(0 => "Employee", 1 => "Customer", 2 => "Date", 3 => "Amount", 4 => "VAT");
    }
    else if($site == "opdrachtenCustomer") {
        $koloms = array(0 => "Number", 1 => "Description", 2 => "Customer", 3 => "Project", 4 => "Template", 5 => "Employee");
    }
    else if ($site == "offerteSpecifiekeCustomer") {
        $koloms = array(0 => "Project No.", 1 => "Name", 2 => "Status", 3 => "Deadline", 4 => "Bugs");
    }
    else if($site == "facturenSpecifiekeCustomer") {
        $koloms = array(0 => "Invoice No.", 1 => "Name", 2 => "Date", 3 => "Amount");
    }
    else if($site == "bugsSpecifiekeCustomer") {
        $koloms = array(0 => "Bug No.", 1 => "Name", 2 => "Category", 3 => "Type", 4 => "Status", 5 => "Priority");
    }
    else if($site == "opdrachtenSpecifiekeCustomer") {
        $koloms = array(0 => "Number", 1 => "Description", 2 => "Customer", 3 => "Project", 4 => "Template", 5 => "Employee");
    }
    else if($site == "verkochtSpecifiekeCustomer") {
        $koloms = array(0 => "Bon Nr", 1 => "Date", 2 => "Product no.", 3 => "Invoice No.",  4 => "Description");
    }
  else if ($site == "agendaonderwerp") {
      $koloms = array(0 => "Category", 1 => "Color");
  }
  else if($site == "toevagendaonderwerp" || $site == "bewagendaonderwerp" || $site == "overzagendaonderwerp" || $site == "getagendaonderwerp")
  {
      $koloms = array(0 => "ID", 1 => "Category", 2 => "Color", 3 => "BEDRIJFSID");
  }
  else if ($site == "menuitems") {
      $koloms = array(0 => "Description", 1 => "Url");
  }
  else if($site == "toevmenuitems" || $site == "bewmenuitems" || $site == "overzmenuitems" || $site == "getmenuitems")
  {
      $koloms = array(0 => "ID",    1 => "Name",  2 => "EN-name", 3 => "Link", 4 => "Description", 5 => "Role", 6 => "Customer", 7 => "Accountant", 8 => "PDA", 9 => "Picture", 10 => "Rollover", 11 => "BEDRIJFSID");
  }
  else if ($site == "doctemplate") {
      $koloms = array(0 => "Text", 1 => "Type", 2 => "Rules");
  }
  else if($site == "toevdoctemplate" || $site == "bewdoctemplate" || $site == "overzdoctemplate" || $site == "getdoctemplate")
  {
      $koloms = array(0 => "ID",     1 => "Name", 2 => "Text",  3 => "Type",   4 => "Source map", 5 => "Rights", 6 => "BEDRIJFSID", 7 => "Achtergrondafbeelding", 8 => "Font size", 9 => "Font");
  }
  else if ($site == "rssfeeds") {
      $koloms = array(0 => "Description", 1 => "Url");
  }
  else if($site == "toevrssfeeds" || $site == "bewrssfeeds" || $site == "overzrssfeeds" || $site == "getrssfeeds")
  {
      $koloms = array(0 => "ID",    1 => "Omschrijving", 2 => "RSS Url", 3 => "Onderwerp", 4 => "Zichtbaar in mededelingenscherm?", 5 => "Medewerker", 6 => "Download items in kennisbank", 7 => "BEDRIJFSID");
  }
  else if ($site == "klantenparameters") {
      $koloms = array(0 => "Customer", 1 => "Date", 2 => "Parameter", 3 => "Value");
  }
  else if($site == "toevklantenparameters" || $site == "bewklantenparameters" || $site == "overzklantenparameters" || $site == "getklantenparameters")
  {
      $koloms = array(0 => "ID",    1 => "Customer", 2 => "Date", 3 => "Parameter", 4 => "Value", 5 => "Additional info", 6 => "File", 7 => "BEDRIJFSID");
  }
  else if ($site == "afsluitenboekjaar") {
      $koloms = array(0 => "Vanaf", 1 => "Tot", 2 => "Afgesloten");
  }
  else if($site == "toevafsluitenboekjaar" || $site == "bewafsluitenboekjaar" || $site == "overzafsluitenboekjaar" || $site == "getafsluitenboekjaar")
  {
      $koloms = array(0 => "ID", 1 => "Vanaf", 2 => "Tot", 3 => "Afgesloten", 4 => "BEDRIJFSID");
  }
  else if($site == "shareholders") {
    $koloms = array(0 => "Shareholder", 1 => "Ownership %");
  }
  else if($site == "toevshareholders" || $site == "bewshareholders" || $site == "overzshareholders" || $site == "getshareholders")
  {
      $koloms = array(0 => "ID",    1 => "Shareholder", 2 => "Ownership", 3 => "Class Type", 4 => "Entity");
  } else if ($site == "pallets") {
      $koloms = array(0 => "Eenheid batch", 1 => "Batch nr", 2 => "Inkoopwaarde", 3 => "Verkoopwaarde", 4 => "Aantal eenheden", 5 => "Locatie");
  }
  else if($site == "toevpallets" || $site == "bewpallets" || $site == "overzpallets" || $site == "getpallets")
  {
      $koloms = array(0 => "ID", 1 => "BEDRIJFSID", 2 => "USERID", 3 => "Inkooporder", 4 => "Verkooporder", 5 => "Eenheid batch", 6 => "Batch nr.", 7 => "Inkoopwaarde", 8 => "Verkoopwaarde", 9 => "Aantal eenheden per batch", 10 => "Eenheid op de batch (doos)", 11 => "Doos nr.", 12 => "Locatie");
  }
  else if ($site == "favourite") {
      $koloms = array(0 => "menuitem", 1 => "Employee", 2 => "Top window");
  }
  else if($site == "toevfavourite" || $site == "bewfavourite" || $site == "overzfavourite" || $site == "getfavourite")
  {
      $koloms = array(0 => "ID",    1 => "Menu item", 2 => "Employee", 3 => "In de top van het scherm", 4 => "BEDRIJFSID");
  }
  else if ($site == "mijncontacten") {
      $koloms = array(0 => "Type", 1 => "Relation", 2 => "Employee");
  }
  else if($site == "toevmijncontacten" || $site == "bewmijncontacten" || $site == "overzmijncontacten" || $site == "getmijncontacten")
  {
      $koloms = array(0 => "ID",    1 => "Tabel",        2 => "Relation",      3 => "Employee",         4 => "BEDRIJFSID");
  }
  else if ($site == "vakanties") {
      $koloms = array(0 => "Employee", 1 => "From (date)", 2 => "Till (date)", 3 => "Hours", 4 => "To");
  }
  else if($site == "toevvakanties" || $site == "bewvakanties" || $site == "overzvakanties" || $site == "getvakanties")
  {
      $koloms = array(0 => "ID",    1 => "Employee",2 => "Customer",             3 => "I leave", 4 => "I am back on", 5 => "To",  6 => "$vakTekts", 7 => "Te bereiken op",       8 => "Dit is betaaldverlof",       9 => "Dit is buitengewoonverlof", 10 => "BEDRIJFSID");
  }
  else if ($site == "declaraties") {
      $koloms = array(0 => "Declaratie Nr", 1 => "Description", 2 => "Date", 3 => "Amount", 4 => "VAT");
  }
  else if($site == "toevdeclaraties" || $site == "bewdeclaraties" || $site == "overzdeclaraties" || $site == "getdeclaraties")
  {
      $koloms = array(0 => "ID",    1 => "Declaratie nr.",2 => "Invoice nr.", 3 => "Description", 4 => "Employee",  5 => "Date", 6 => "Amount (excl VAT)", 7 => "VAT percentage", 8 => "Accept", 9 => "BEDRIJFSID");
  }
  else if ($site == "cronjob") {
      $koloms = array(0 => "Naam (voor eigen gebruik)", 1 => "Notificatie mailen naar", 2 => "Laatst uitgevoerd");
  }
  else if($site == "toevcronjob" || $site == "bewcronjob" || $site == "overzcronjob" || $site == "getcronjob")
  {
      $koloms = array(0 => "ID",    1 => "Naam (voor eigen gebruik)",2 => "Notificatie mailen naar", 3 => "User", 4 => "LoginId",  5 => "Datum", 6 => "Actief", 7 => "Toon rapport", 8 => "Opnieuw uitvoeren na", 9 => "BEDRIJFSID");
  }
  else if ($site == "bankrekeningen") {
      $koloms = array(0 => "Name", 1 => "Bank account nr..", 2 => "T.N.V.", 3 => "City");
  }
  else if($site == "toevbankrekeningen" || $site == "toevkasboek" || $site == "bewbankrekeningen" || $site == "overzbankrekeningen" || $site == "getbankrekeningen")
  {
      $koloms = array(0 => "ID",    1 => "Name",2 => "Bank account nr..", 3 => "T.N.V", 4 => "City",  5 => "Beginning Balance", 6 => "Employee", 7 => "General ledger", 9 => "Credit", 10 => "BEDRIJFSID");
  }
  else if ($site == "saldoinformatie") {
      $koloms = array(0 => "Bank", 1 => "Date", 2 => "Bij/Af", 3 => "Amount");
  }
  else if($site == "toevsaldoinformatie" || $site == "bewsaldoinformatie" || $site == "overzsaldoinformatie" || $site == "getsaldoinformatie")
  {
      $koloms = array(0 => "ID",    1 => "Kasboek / Rekening",2 => "Datum", 3 => "Referentie", 4 => "Relatie", 5 => "Verkoopboek", 6 => "Inkoopboek", 7 => "Bank of Giro",  8 => "Bankrekeningnummer", 9 => "Omschrijving", 10 => "Mededeling", 11 => "Bij /Af", 12 => "Bedrag", 13 => "Valuta", 14 => "Grootboekrekening", 15 => "(Grootboek-) tegenrekening", 16 => "BEDRIJFSID", 17 => "Nog te boeken", 18 => "Direct geboekt", 19 => "Memo");
  }
  else if ($site == "saldoinformatieboekingen") {
      $koloms = array(0 => "Amount", 1 => "Selling Book id", 2 => "Purchase book id", 3 => "General letter account");
  }
  else if($site == "toevsaldoinformatieboekingen" || $site == "bewsaldoinformatieboekingen" || $site == "overzsaldoinformatieboekingen" || $site == "getsaldoinformatieboekingen")
  {
      $koloms = array(0 => "ID",    1 => "Mutation",2 => "Amount", 3 => "Selling Book", 4 => "Purchase book", 5 => "General Ledger", 6 => "General Ledger", 7 => "BEDRIJFSID");
  }
  else if ($site == "opdrachttemplate") {
      $koloms = array(0 => "Description");
  }
  else if ($site == "verwopdrachttemplate") { head2(); verwijderen("opdrachttemplate"); footer1();}
  else if($site == "toevopdrachttemplate" || $site == "bewopdrachttemplate" || $site == "overzopdrachttemplate" || $site == "getopdrachttemplate")
  {
      $koloms = array(0 => "ID", 1 => "Description", 2 => "Add relation", 3 => "Add project", 4 => "Add description", 5 => "Edit date / status", 6 => "Responsible", 7 => "Administration", 8 => "Contracts", 8 => "Files", 10 => "Products", 11 => "Connections", 12 => "File location", 13 => "Fieldnames", 14 => "BEDRIJFSID");
  }
  else if ($site == "opdrachten") {
      if(isset($_GET["TEMPLATEID"]))
      {
         $r2 = mysql_query("SELECT * FROM opdrachttemplate WHERE (ID*1) = ('".ps($_GET["TEMPLATEID"], "nr")."'*1) AND KLANT = 'Nee';", $db);
         if($datar2 = mysql_fetch_array($r2))
         {
           $koloms = array(0 => "Name", 1 => "Responsible", 2 => "Status", 3 => "Deadline");
         } else {
           $koloms = array(0 => "Name", 1 => "Relation", 2 => "Responsible", 3 => "Status", 4 => "Deadline");
         }
      } else {
         $koloms = array(0 => "Name", 1 => "Relation", 2 => "Responsible", 3 => "Status", 4 => "Deadline");
      }
  }
  else if($site == "toevopdrachten" || $site == "bewopdrachten" || $site == "overzopdrachten" || $site == "getopdrachten")
  {
      $koloms = array(0 => "ID",1 => "Number", 2 => "Name", 3 => "Subject", 4 => "Description", 5 => "Relation", 6 => "Project", 7 => "Template", 8 => "Responsible", 9 => "Administration", 10 => "Contracts", 11 => "Files", 12 => "Order", 13 => "Connection", 14 => "Deadline", 15 => "Status", 16 => "Opdrachtregels", 17 => "BEDRIJFSID");
  }
  else if ($site == "linksonderwerp") {
      $koloms = array(0 => "Description");
  }
  else if($site == "toevlinksonderwerp" || $site == "bewlinksonderwerp" || $site == "overzlinksonderwerp" || $site == "getlinksonderwerp")
  {
      $koloms = array(0 => "ID", 1 => "Category", 2 => "BEDRIJFSID");
  }
  else if ($site == "linken") {
      $koloms = array(0 => "Description", 1 => "Link", 2 => "Category");
  }
  else if ($site == "linkenLeeg") {
      $koloms = array(0 => "Description", 1 => "Link", 2 => "Category");
  }
  else if($site == "toevlinken" || $site == "bewlinken" || $site == "overzlinken" || $site == "getlinken")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Link / FTP address", 3 => "Extra specification", 4 => "Username", 5 => "Password", 6 => "Availeble for everyone", 7 => "Category", 8 => "BEDRIJFSID", 9 => "Ordner");
  }
  else if ($site == "todolist") {
      $koloms = array(0 => "Description kort", 1 => "Status");
  }
  else if($site == "toevtodolist" || $site == "bewtodolist" || $site == "overztodolist" || $site == "gettodolist")
  {
      $koloms = array(0 => "ID", 1 => "Category",     2 => "Description kort", 3 => "Description lang", 4 => "Status", 5 => "Employee",         5 => "Date", 6 => "Time",      7 => "BEDRIJFSID");
  }
  else if ($site == "helpitems") {
      $koloms = array(0 => "Subject", 1 => "Kenmerk", 2 => "Bezocht");
  }
  else if($site == "toevhelpitems" || $site == "bewhelpitems" || $site == "overzhelpitems" || $site == "gethelpitems")
  {
      $koloms = array(0 => "ID", 1 => "hidden", 2 => "field", 3 => "editor", 4 => "field");
  }
  else if ($site == "maillinglist") {
      $koloms = array(0 => "Maillinglist", 1 => "Name", 2 => "Registered");
  }
  else if($site == "toevmaillinglist" || $site == "bewmaillinglist" || $site == "overzmaillinglist" || $site == "getmaillinglist")
  {
      $koloms = array(0 => "ID", 1 => "Mailinglist", 2 => "Customer", 3 => "Registered", 4 => "Date", 5 => "BEDRIJFSID");
  }
  else if ($site == "nieuwsbrief") {
      $koloms = array(0 => "Date", 1 => "Title", 2 => "Category");
  }
  else if($site == "toevnieuwsbrief" || $site == "bewnieuwsbrief" || $site == "overznieuwsbrief" || $site == "getnieuwsbrief")
  {
      $koloms = array(0 => "ID", 1 => "Mailinglist", 2 => "Afzender", 3 => "Titel", 4 => "Medewerker", 5 => "Getoond op website", 6 => "BEDRIJFSID", 7 => "Tekst", 8 => "Mailtekst voor mail als link", 9 => "Zet de mail in een onzichtbaar kader", 10 => "Ordner", 11 => "Date");
  }
  else if ($site == "sms") {
      $koloms = array(0 => "Date", 1 => "Send from", 2 => "Text", 3 => "Receiver");
  }
  else if($site == "toevsms" || $site == "bewsms" || $site == "overzsms" || $site == "getsms")
  {
      $koloms = array(0 => "ID", 1 => "To", 2 => "From", 3 => "Text", 4 => "Date", 5 => "BEDRIJFSID", 6 => "Status");
  }
  else if ($site == "personeel") {
      $koloms = array(0 => "Initials", 1 => "Employee", 2 => "Address", 3 => "City", 4 => "Mobile", 5 => "Email address");
  }
  else if($site == "toevpersoneel" || $site == "bewpersoneel" || $site == "bewpersoneelID" || $site == "overzpersoneel" || $site == "overzpersoneelNAWLeeg" || $site == "getpersoneel")
  {
      $koloms = array(0 => "ID", 1 => "Company name", 2 => "Gender", 3 => "Initials", 4 => "First name", 5 => "Inner name", 6 => "Last name", 7 => "Address", 8 => "Zip", 9 => "City", 10 => "Country", 11 => "PO Box", 12 => "Zip of the PO Box", 13 => "Homepage", 14 => "Email", 15 => "Phone", 16 => "Mobile", 17 => "Fax", 18 => "Bank account nr.", 19 => "In particular", 20 => "Location of the bank (city)", 21 => "Client File", 22 => "Type werknemer", 23 => "Kosten per uur", 24 => "Holidays a year", 25 => "Working hours per week", 26 => "Birthday", 27 => "Werkdagen per week", 28 => "Activate daily email reminder", 29 => "Job title", 30 => "Business line", 31 => "First day in the company?", 32 => "Wanneer laatste werkdag?<br/><i>Bij vast contract niet invullen.", 33 => "Characteristic", 34 => "Give my identity free", 35 => "Not spend holidays (in hours)", 36 => "Last login", 37 => "Memo", 38 => "Role", 39 => "Picture", 40 => "Language");
  }
  else if ($site == "profiel") {
      $koloms = array(0 => "Employee", 1 => "Geboorteplaats", 2 => "Beroep", 3 => "Burgenlijke staat");
  }
  else if($site == "toevprofiel" || $site == "bewprofiel" || $site == "overzprofiel" || $site == "getprofiel")
  {
      $koloms = array(0 => "ID",    1 => "PERSONEELSLID", 2 => "Picuture", 3 => "Achtergrondfoto menu", 4 => "Burgerlijke staat", 5 => "Birth place", 6 => "Appeal", 7 => "Sence of humour",  8 => "Mode", 9 => "Music", 10 => "Interesses", 11 => "Hobbies", 12 => "Huisdieren", 13 => "Msn-address", 14 => "Skype-address", 15 => "Favourite citaat", 16 => "Overige informatie", 17 => "BEDRIJFSID");
  }
  else if($site == "afdeling") {
      $koloms = array(0 => "Name", 1 => "Parent node", 2 => "Responsible");
  }
  else if($site == "toevafdeling" || $site == "bewafdeling" || $site == "overzafdeling" || $site == "getafdeling")
  {
      $koloms = array(0 => "ID", 1 => "Name",    2 => "Parent node", 3 => "Responsible", 4 => "BEDRIJFSID");
  }
  else if($site == "taal") {
      $koloms = array(0 => "Netherlands", 1 => "Englisch");
  }
  else if($site == "toevtaal" || $site == "bewtaal" || $site == "overztaal" || $site == "gettaal")
  {
      $koloms = array(0 => "ID", 1 => "Reference",    2 => "Netherlands", 3 => "Englisch", 4 => "BEDRIJFSID");
  }
  else if ($site == "CVSpecifiekeCustomer") {
      $koloms = array(0 => "From", 1 => "Date", 2 => "Education 1", 3 => "TINK date", 4 => "PINK date", 5 => "Response?", 6 => "Status");
  }
  else if($site == "cv") {
      $koloms = array(0 => "From", 1 => "Date", 2 => "Education 1", 3 => "TINK date", 4 => "PINK date", 5 => "Response?", 6 => "Status");
  }
  else if($site == "toevcv" || $site == "toevcvpers" || $site == "bewcv" || $site == "bewcvpers" || $site == "overzcv" || $site == "getcv")
  {
      $koloms = array(0 => "ID", 1 => "Employee",                2 => "Applicant",     3 => "Title", 4 => "Date", 5 => "Kwam binnen via", 6 => "Driving license", 7 => "Dienstensector",    8 => "Locatie werk",   9 => "Expertise", 10 => "Education",           11 => "Other education", 12 => "Properties", 13 => "Ambitions",  14 => "Werkervaring", 15 => "Bijzondere ervaring", 16 => "Vorige werkgevers", 17 => "References",18 => "Beschikbaar per",  19 => "Total uur per week", 20 => "TINK date",  21 => "TINK door",     22 => "TINK",   23 => "PINK date",  24 => "PINK door",          25 => "PINK",   26 => "Wie heeft er gereageerd?", 27 => "Terugkoppeldate", 28 => "Additional info", 29 => "Status",   30 => "Video", 31 => "File", 32 => "BEDRIJFSID");
  }
  else if($site == "persdossier") {
      $koloms = array(0 => "Employee", 1 => "Date", 2 => "Type gesprek");
  }
  else if($site == "toevpersdossier" || $site == "bewpersdossier" || $site == "overzpersdossier" || $site == "getpersdossier")
  {
      $koloms = array(0 => "ID", 1 => "Employee",       2 => "Date",  3 => "Type gesprek", 4 => "Text beoordelaar", 5 => "Text beoordeelde", 6 => "BEDRIJFSID");
  }
  else if ($site == "ongewenstemail") {
      $koloms = array(0 => "Woord", 1 => "Email addressaddress");
  }
  else if($site == "toevongewenstemail" || $site == "bewongewenstemail" || $site == "overzongewenstemail" || $site == "getongewenstemail")
  {
      $koloms = array(0 => "ID", 1 => "Ongewenst woord", 2 => "Ongewenst emailaddress", 3 => "Employee", 4 => "BEDRIJFSID");
  }
  else if ($site == "mailinstellingen") {
      $koloms = array(0 => "Email", 1 => "Type", 2 => "Standaard");
  }
  else if($site == "toevmailinstellingen" || $site == "bewmailinstellingen" || $site == "overzmailinstellingen" || $site == "getmailinstellingen")
  {
      $koloms = array(0 => "ID", 1 => "Type", 2 => "Email address", 3 => "Username", 4 => "Password", 5 => "Pop3", 6 => "SMTP", 7 => "Host", 8 => "Leave message on server", 9 => "How many days on server", 10 => "Employee", 11 => "Sign", 12 => "BEDRIJFSID", 13 => "Mailbox gedeeld met anderen", 14 => "Is standaard mailadres");
  }
  else if ($site == "nieuws") {
      $koloms = array(0 => "Title", 1 => "Date", 2 => "Visible");
  }
  else if($site == "toevnieuws" || $site == "bewnieuws" || $site == "overznieuws" || $site == "getnieuws")
  {
      $koloms = array(0 => "ID",    1 => "Title", 2 => "News item",   3 => "Date",  4 => "Visible",       5 => "BEDRIJFSID", 6 => "Employee", 7 => "File koppelen", 8 => "Image");
  }
  else if ($site == "offerteteksten") {
      $koloms = array(0 => "Type", 1 => "Kenmerk");
  }
  else if($site == "toevofferteteksten" || $site == "bewofferteteksten" || $site == "overzofferteteksten" || $site == "getofferteteksten")
  {
      $koloms = array(0 => "ID", 1 => "Type tekst", 2 => "Geheugensteuntje", 3 => "Tekst boven de factuur (voor format 20)", 4 => "Opmaak factuur tekst", 5 => "Aanmaningsvolgorde", 6 => "Taal van de factuur", 7 => "Type tekst", 8 => "Logo", 9 => "BEDRIJFSID");
  }
  else if ($site == "conceptofferte" || $site == "alleoffertes") {
      $koloms = array(0 => "Proposal No.", 1 => "Customer", 2 => "Name", 3 => "Start op", 4 => "Stopt op", 5 => "Amount", 6 => "Status", 7 => "Offerte geldig tot");
  }
  else if($site == "toevconceptofferte" || $site == "bewconceptofferte" || $site == "overzconceptofferte" || $site == "conceptofferte_pdf" || $site == "getconceptofferte")
  {
      $koloms = array(0 => "ID",    1 => "Proposal No.",2 => "Name", 3 => "Customer", 4 => "Supplier",        5 => "Employee", 6 => "".tl("DATUMSTART")."", 7 => "Afloopdate", 8 => "Invoerdate", 9 => "Tot wanneer is deze offerte geldig?", 10 => "Hoeveel tijd", 11 => "Hoeveel euro per (tijds)eenheid",  12 => "Eenheid", 13 => "Vast bedrag", 14 => "Status", 15 => "Total (type 1)", 16 => "Total (type 2)", 17 => "Total (type 3)", 18 => "Total (type 4)", 19 => "Text voor de klant (1)", 20 => "Text voor de klant (2)", 21 => "Text voor de leverancier (1)", 22 => "Text voor de leverancier (2)", 23 => "Welke acties zijn er ondernomen", 24 => "Additional info", 25 => "BEDRIJFSID");
  }
  else if ($site == "bugcatagory") {
      $koloms = array(0 => "Category", 1 => "Beschrijvende tekst");
  }
  else if($site == "toevbugcatagory" || $site == "bewbugcatagory" || $site == "overzbugcatagory" || $site == "getbugcatagory")
  {
      $koloms = array(0 => "ID", 1 => "BEDRIJFSID", 2 => "Name", 3 => "Extra text");
  }
  else if ($site == "buglist") {
      $koloms = array(0 => "Ticket Nr.", 1 => "Name");
  }
  else if($site == "toevbuglist" || $site == "bewbuglist" || $site == "overzbuglist" || $site == "getbuglist")
  {
      $koloms = array(0 => "ID", 1 => "Ticket Id.", 2 => "Name", 3 => "Description", 4 => "Additional info", 5 => "BESTAND", 6 => "Prive of publiek", 7 => "Employee", 8 => "BEDRIJFSID", 9 => "Date", 10 => "Time");
  }
  else if ($site == "offerteleden") {
      $koloms = array(0 => "Project No.", 1 => "Employee", 2 => "Task");
  }
  else if($site == "toevofferteleden" || $site == "bewofferteleden" || $site == "overzofferteleden" || $site == "getofferteleden")
  {
      $koloms = array(0 => "ID", 1 => "Project", 2 => "Task", 3 => "Employee", 4 => "Tarrif", 5 => "BEDRIJFSID");
  }
  else if ($site == "planningtijd") {
      $koloms = array(0 => "Project No.", 1 => "Employee", 2 => "Task", 3 => "Availeble time");
  }
  else if($site == "toevplanningtijd" || $site == "bewplanningtijd" || $site == "overzplanningtijd" || $site == "getplanningtijd")
  {
      $koloms = array(0 => "ID", 1 => "Project", 2 => "Task", 3 => "Employee", 4 => "Availeble time", 5 => "Additional info", 6 => "From", 7 => "Till", 8 => "BEDRIJFSID");
  }
  else if ($site == "productgroepkorting") {
      $koloms = array(0 => "Categorie", 1 => "Klant", 2 => "Korting");
  }
  else if($site == "toevproductgroepkorting" || $site == "bewproductgroepkorting" || $site == "overzproductgroepkorting" || $site == "getproductgroepkorting")
  {
      $koloms = array(0 => "ID",    1 => "Categorie", 2 => "Klant",   3 => "Korting",   4 => "BEDRIJFSID");
  }
  else if ($site == "producten") {
      $koloms = array(0 => "Product no.", 1 => "Category", 2 => "Name", 3 => "Supplier", 4 => "Amount", 5 => "BTW %", 6 => "Purchase price", 7 => "VAT %");
  }
  else if($site == "toevproducten" || $site == "bewproducten" || $site == "overzproducten" || $site == "getproducten")
  {
      $koloms = array(0 => "ID",    1 => "Product no.",    2 => "Category",       3 => "Description", 4 => "Name" , 5 => "Line",  6 => "Type",  7 => "Selleble from (date)", 8 => "Selleble from (time)", 9 => "Selleble till (date)", 10 => "Selleble till (time)", 11 => "Supplier", 12 => "Max price", 13 => "Min. price", 14 => "Price", 15 => "Price a month",16 => "Price a kwartaal", 17 => "Price a year", 18 => "Stock", 19 => "In het assortiment", 20 => "Inkoopsprijs",   21 => "Inkoopsprijs p.m.", 22 => "Inkoopsprijs p. kwartaal", 23 => "Inkoopsprijs p.j.", 24 => "Beschikbaar voor online verkoop", 25 => "Total keer bekeken", 26 => "Picture",  27 => "BEDRIJFSID", 28 => "Minimale voorraad", 29 => "Besteladvies", 30 => "BTW %", 31 => "Eenheid inkoop", 32 => "Eenheid verkoop", 33 => "Omrekeningsfactor", 34 => "TAGS", 35 => "PRIJSVAN", 36 => "LEVERTIJD", 37 => "VERZENDKOSTEN");
  }
  else if ($site == "verkocht") {
      $koloms = array(0 => "Reference", 1 => "Customer", 2 => "Date", 3 => "Number", 4 => "Article", 5 => "Description", 6 => "Is subscription active?", 7 => "Invoice nr.");
  }
  else if($site == "toevverkocht" || $site == "bewverkocht" || $site == "overzverkocht" || $site == "getverkocht")
  {
      $koloms = array(0 => "ID",    1 => "BEDRIJFSID",2 => "Employee",3 => "Customer",4 => "Number",5 => "Artikel",6 => "Date",7 => "Description",8 => "Comments",9 => "Total amount",10 => "Discount",11 => "Count",12 => "Delivered",13 => "Backorder",14 => "Pakbon",15 => "Is subscription active?",16 => "Dit abonnement loopt tot",17 => "Pallet nr",18 => "Provisie",19 => "Reseller",20 => "Project",21 => "Behoort bij factuur",22 => "");
  }
  else if($site == "toevrelatievan" || $site == "bewrelatievan" || $site == "overzrelatievan" || $site == "getrelatievan")
  {
      $koloms = array(0 => "ID", 1 => "Relatie", 2 => "Contactpersoon", 3 => "Relatie", 4 => "Contactpersoon", 5 => "Omschrijving", 6 => "BEDRIJFSID");
  }
  else if ($site == "inkooporder") {
      $koloms = array(0 => "Reference", 1 => "Sales reference", 2 => "Supplier", 3 => "Date", 4 => "Ordered", 5 => "Delivered", 6 => "Backorder", 7 => "Article", 8 => "Description", 9 => "Invoice Nr.");
  }
  else if($site == "toevinkooporder" || $site == "bewinkooporder" || $site == "overzinkooporder" || $site == "getinkooporder")
  {
      $koloms = array(0 => "ID",    1 => "Reference",  2 => "Sales reference",    3 => "Article",  4 => "Supplier", 5 => "Order date",        6 => "Expected delivery date", 7 => "Description", 8 => "Ordered", 9 => "Delivered", 10 => "Backorder", 11 => "Total price", 12 => "Discount", 13 => "Employee", 14 => "Levering", 15 => "Project", 16 => "Productvariant", 17 => "Extra description", 18 => "Invoice", 19 => "BEDRIJFSID");
  }
  else if ($site == "faq") {
      $koloms = array(0 => "Categorie", 1 => "Vraag", 2 => "Antwoord", 3 => "Bezocht");
  }
  else if($site == "toevfaq" || $site == "bewfaq" || $site == "overzfaq" || $site == "getfaq")
  {
      $koloms = array(0 => "ID", 1 => "Categorie", 2 => "Onderdeel van", 3 => "Vraag",  4 => "Antwoord",   5 => "Rating", 6 => "Bezocht", 7 => "BEDRIJFSID");
  }
  else if ($site == "faq_handleiding") {
      $koloms = array(0 => "Vraag", 1 => "Antwoord", 2 => "Bezocht");
  }
  else if($site == "toevfaq_handleiding" || $site == "bewfaq_handleiding" || $site == "overzfaq_handleiding" || $site == "getfaq_handleiding")
  {
      $koloms = array(0 => "ID",     1 => "Onderdeel van", 2 => "Vraag",  3 => "Antwoord",   4 => "Rating", 5 => "Rechten", 6 => "Bezocht", 7 => "BEDRIJFSID");
  }
  else if ($site == "offerte") {
      $koloms = array(0 => "Customer", 1 => "Project name",2 => "Project No..", 3 => "Status", 4 => "Deadline", 5 => "Project leader"); //, 5 => "Alles gefactureerd");
  }
  else if($site == "toevofferte" || $site == "bewofferte" || $site == "overzofferte" || $site == "getofferte")
  {
      $koloms = array(0 => "ID",    1 => "Project No.", 2 => "Proposal", 3 => "Description",4 => "Additional information", 5 => "Project name",  6 => "Internet address", 7 => "This project is", 8 => "Totaal order amount", 9 => "Fixed fee?",10 => "BEDRIJFSID",11 => "Customer",   12 => "Status", 13 => tl("DATUMSTART"), 14 => "Deadline", 15 => "Project leader", 16 => "Contact", 17 => "Region", 18 => "Default kilometers", 19 => "Factory", 20 => "PO number");
  }
  else if ($site == "projectIntern") {
      $koloms = array(0 => "Project name",1 => "Project No..", 2 => "Type project", 3 => "Project leader", 4 => "Contact", 5 => "Additional infoen");
  }
  else if ($site == "uren") {
      $koloms = array(0 => "Project No.", 1 => "DATUM", 1 => "Date", 2 => "Time (start)", 3 => "Time (stop)", 4 => "Task");
  }
  else if ($site == "urenLeeg") {
     $koloms = array(0 => "Project No.", 1 => "Date", 2 => "Time (start)", 3 => "Time (stop)", 4 => "Task");
  }
  else if($site == "toevuren" || $site == "bewuren" || $site == "overzuren" || $site == "geturen")
  {
      $koloms = array(0 => "ID",    1 => "Project",    2 => "Task",   3 => "Date",  4 => "Time (start)",    5 => "Time (stop)", 6 => "Description", 7 => "BEDRIJFSID", 8 => "Is declarabel",        9 => "Employee");
  }
  else if ($site == "urenopplan") {
      $koloms = array(0 => "Project No.", 1 => "Date", 2 => "Time (start)", 3 => "Time (stop)", 4 => "Still okay?");
  }
  else if($site == "toevurenopplan" || $site == "bewurenopplan" || $site == "overzurenopplan" || $site == "geturenopplan")
  {
      $koloms = array(0 => "ID",    1 => "Project",    2 => "Task",   3 => "Date",  4 => "Time (start)",    5 => "Time (stop)", 6 => "Description", 7 => "BEDRIJFSID", 8 => "Employee", 9 => "Zijn deze uren nog steeds volgens afspraak?", 10 => "WIJZIGDATUM", 11 => "WIJZIGPERSLID");
  }
  else if($site == "klanten") {
      $koloms = array(0 => "Company name", 1 => "Name", 2 => "Address", 3 => "Zip", 4 => "City", 5 => "Phone", 6 => "Mobile");
  }
  else if($site == "toevklanten" || $site == "toevklantensnel" || $site == "bewklanten" || $site == "overzklanten" || $site == "getklanten" || $site == "getklanten_dbstructuur")
  {
      $koloms = array(0 => "ID",
                      1 => "Relation Nr.",
                      2 => "Type contact",   
                      3 => "Deze relatie is tevens", 
                      4 => "Deze relatie is tevens", 
                      5 => "Soort bedrijf", 
                      6 => "Vakgebied",      
                      7 => "Company name",
                      8 => "Man / Vrouw (Contactpersoon)",  
                      9 => "T.n.v. (Contactpersoon)",
                      10 => "Titel",
                      11 => "Voorletters (Contactpersoon)",
                      12 => "Voornaam (Contactpersoon)",
                      13 => "Tussenvoegsel (Contactpersoon)",
                      14 => "Achternaam (Contactpersoon)", 
                      15 => "Afdeling",
                      16 => "Functie (Contactpersoon)",
                      17 => "Email (Contactpersoon)", 
                      18 => "Telefoon (Contactpersoon)",
                      19 => "Mobiel (Contactpersoon)",
                      20 => "Address (Bezoekadres)",
                      21 => "Huis Nr (Bezoekadres)",    
                      22 => "Addition (Bezoekadres)",
                      23 => "Zibcode (Bezoekadres)",
                      24 => "City (Bezoekadres)",
                      25 => "Country (Postadres)",
                      26 => "evt. Bedrijfsnaam (Postadres)",
                      27 => "evt. Contactpersoon (Postadres)",
                      28 => "Adres (Postadres)",
                      29 => "Huis Nr (Postadres)",
                      30 => "Toevoeging (Postadres)",
                      31 => "Postcode (Postadres)",
                      32 => "Plaats (Postadres)",
                      33 => "Telefoon (Postadres)",
                      34 => "Fax (Postadres)",
                      35 => "Homepage",
                      36 => "Email (Kantoor)",
                      37 => "Postbus",
                      38 => "Postcode van de postbus",
                      39 => "Kamer van Koophandel Nr",
                      40 => "BTW Nr",
                      41 => "Betaalwijze",
                      42 => "Rekening Nr",
                      43 => "Ten name van",
                      44 => "Rekening plaats",
                      45 => "Creditcard Nr",
                      46 => "Type kaart",
                      47 => "Vervaldatum kaart",
                      48 => "Bedrijfsomschrijving",
                      49 => "Geboortedatum",
                      50 => "Wanneer klant is geworden",
                      51 => "Wie is verantwoordelijk voor deze relatie",
                      52 => "Welk cijfer <b>krijgt</b> u van de relatie",
                      53 => "Welk cijfer <b>geeft</b> u van de relatie",
                      54 => "Is deze relatie exit?",
                      55 => "Hoe heeft de klant ons gevonden?",
                      56 => "Wie heeft de klant gevonden",
                      57 => "Hoe sterk is de relatie met de klant",
                      58 => "Hoofdtaal",
                      59 => "Ethnische achtergrond",
                      60 => "Hoogstgenoten opleiding",
                      61 => "Inkomstenbron",
                      62 => "Skills",
                      63 => "Bijzonderheden",
                      64 => "BEDRIJFSID",
                      65 => "Selecteer standaard grootboekrek. debiteur",
                      66 => "Selecteer standaard grootboekrek. crediteur",
                      67 => "In hoeveel dagen dienen facturen (verkoop) te worden betaald",
                      68 => "In hoeveel dagen dienen facturen (inkoop) te worden betaald",
                      69 => "Kredietlimiet",
                      70 => "Dossier",
                      71 => "Status nieuwsbrief",
                      72 => "Inkomstenbron",
                      73 => "Jaarinkomen",
                      74 => "Jaarinkomen is van welk jaar",
                      75 => "Waarde eigen woning",
                      76 => "Vrij vermogen",
                      77 => "Aantal kinderen",
                      78 => "Relatie van",
                      79 => "BSN nummer",
                      80 => "Burgerlijke staat",
                      81 => "Afwijkend mailadres voor facturen",
                      82 => "Afwijkend mailadres voor nieuwsbrief",
                      83 => "Provincie",
                      84 => "Kenteken",
                      85 => "Betalingsconditie",
                      86 => "Foto");
  }
  else if($site == "toonveldenklanten") {
      $koloms = array(0 => "Company name", 1 => "Name", 2 => "Address", 3 => "Zip", 4 => "City", 5 => "Phone", 6 => "Mobile");
  }
  else if($site == "toevtoonveldenklanten" || $site == "bewtoonveldenklanten" || $site == "overztoonveldenklanten" || $site == "gettoonveldenklanten")
  {
      $koloms = array(0 => "ID", 1 => "Relation Nr.",   2 => "Type relation",   3 => "Deze relation is tevens", 4 => "Deze relation is tevens", 5 => "Type bedrijf", 6 => "Vakgebied",      7 => "Company name",  8 => "Gender (Contactpersoon)",  9 => "T.n.v. (Contactpersoon)", 10 => "Title",                     11 => "Initials (Contactpersoon)",12 => "First name (Contactpersoon)", 13 => "Inner name (Contactpersoon)", 14 => "Last name (Contactpersoon)", 15 => "Job title contactpersoon",  16 => "Email address (Contactpersoon)", 17 => "Phone (Contactpersoon)", 18 => "Mobile (Contactpersoon)", 19 => "Address (Visiting)", 20 => "Number of the house (Visiting)",    21 => "Addition (Visiting)",    22 => "Zip (Visiting)",    23 => "City (Visiting)",    24 => "Land (Postal address)", 25 => "Address (Postal address)", 26 => "Number of the house (Postal address)", 27 => "Addition (Postal address)", 28 => "Zip (Postal address)", 29 => "City (Postal address)", 30 => "Phone (Postal address)", 31 => "Fax (Postal address)", 32 => "Homepage",    33 => "Email address (Kantoor)", 34 => "PO Box",     35 => "Zip of the PO Box", 36 => "Kamer van Koophandel Nr", 37 => "VAT Nr",      38 => "Factuur per mail?", 39 => "Bank account nr.",  40 => "In particular",  41 => "Location of the bank (city)",  42 => "Creditcard Nr", 43 => "Type kaart", 44 => "Vervaldate kaart", 45 => "Company description", 46 => "Birthday",  47 => "Relation starts", 48 => "Who is responsible", 49 => "Welk cijfer <b>krijgt</b> u van de relation", 50 => "Welk cijfer <b>geeft</b> u van de relation",  51 => "Relation exit?", 52 => "Lead?", 53 => "Who makes the first contact", 54 => "How strong is the relation", 55 => "Hoofdtaal",          56 => "Ethnische achtergrond", 57 => "Hoogstgenoten opleiding", 58 => "Salarisniveau", 59 => "Skills",      60 => "Special information", 61 => "BEDRIJFSID", 62 => "Default general letter account", 63 => "Expiration date invoices", 64 => "Dossiers", 65 => "Status", 66 => "Inkomstenbron", 67 => "Jaarinkomen", 68 => "Jaarinkomen jaartal", 69 => "Waarde eigen woning",70 => "Vrij vermogen", 71 => "Kinderen", 72 => "Onderdeel van", 73 => "BSN nummer", 74 => "Huwelijkse staat", 75 => "Email factuur", 76 => "Email nieuwsbrief", 77 => "Provincie", 78 => "Kenteken");
  }
  else if($site == "contactpersoon") {
      $koloms = array(0 => "Customer", 1 => "Name", 2 => "Email address", 3 => "Mobile", 4 => "Job title");
  }
  else if($site == "toevcontactpersoon" || $site == "bewcontactpersoon" || $site == "overzcontactpersoon" || $site == "getcontactpersoon")
  {
      $koloms = array(0 => "ID", 1 => "Customer",  2 => "Gender (Contactpersoon)",  3 => "T.n.v. (Contactpersoon)", 4 => "Title", 5 => "Initials (Contactpersoon)", 6 => "First name (Contactpersoon)",  7 => "Inner name (Contactpersoon)", 8 => "Last name (Contactpersoon)", 9 => "Email address (Contactpersoon)",10 => "Phone (Contactpersoon)", 11 => "Mobile (Contactpersoon)", 12 => "Address (Contactpersoon)",13 => "Number of the house (Contactpersoon)", 14 => "Addition (Contactpersoon)", 15 => "Zip (Contactpersoon)", 16 => "City (Contactpersoon)", 17 => "Land (Kantoor)",  18 => "Fax (Kantoor)", 19 => "Homepage (Kantoor)", 20 => "PO Box", 21 => "Zip of the PO Box", 22 => "Job title", 23 => "Birthday", 24 => "Special information", 25 => "BEDRIJFSID", 26 => "Foto");
  }
  else if($site == "facturen") {
      $koloms = array(0 => "Invoice No.", 1 => "Customer", 2 => "Date", 3 => "Amount");
  }
  else if($site == "toevfacturen" || $site == "bewfacturen" || $site == "overzfacturen" || $site == "facturen_pdf" || $site == "getfacturen")
  {
      $koloms = array(0 => "ID",
                     1 => "Algemene factuurinformatie",
                     2 => "Factuur Nr",
                     3 => "Project",
                     4 => "Datum",
                     5 => "Relatie",
                     6 => "Kostenplaats",
                     7 => "Betreft",

                     8 => "Financiele informatie",
                     9 => "Grootboekrekening",
                     10 => "Tegenrekening (BTW hoog)",
                     11 => "NAAM",
                     12 => "BTW percentage (BTW hoog)",
                     13 => "Totaal bedrag (BTW hoog)",
                     14 => "Tegenrekening (BTW laag)",
                     15 => "BTW percentage (BTW laag)",
                     16 => "Totaal bedrag (BTW laag)",
                     17 => "Tegenrekening (BTW nul)",
                     18 => "Totaal bedrag (BTW nul)",

                     19 => "Korting",
                     20 => "Boete",
                     21 => "Administratiekosten",
                     22 => "Verzendkosten",

                     23 => "BEDRIJFSID",
                     24 => "Is verzonden",
                     25 => "Is betaald",
                     26 => "Betaaldatum",
                     27 => "CONTROL1",
                     28 => "CONTROL2",
                     29 => "CONTROL3",

                     30 => "Factuur opmaak",
                     31 => "Template",
                     32 => "Factuurtekst",
                     33 => "Aanmaningstekst",
                     34 => "Tekst boven of onder de factuurspecificaties",
                     35 => "Uren op factuur tonen",
                     36 => "Extra kosten op factuur tonen",
                     37 => "Producten op factuur tonen",
                     38 => "Logo tonen",
                     39 => "Footer tonen",

                     40 => "PERSONEELSLID",
                     41 => "MEMO",

                     42 => "Reseller",
                     43 => "Afgerekend",
                     44 => "Met reseller is afgerekend",
                     45 => "Bedrag",
                     46 => "BESTAND");
  }
  else if($site == "facturenaanmanen") {
      $koloms = array(0 => "Reprimand no.", 1 => "Invoice No.", 2 => "Customer", 3 => "Date", 4 => "Amount");
  }
  else if($site == "toevfacturenaanmanen" || $site == "bewfacturenaanmanen" || $site == "overzfacturenaanmanen" || $site == "facturenaanmanen_pdf" || $site == "getfacturenaanmanen")
  {
      $koloms = array(0 => "ID", 1 => "Reprimand no.", 2 => "Invoice No.",   3 => "Project",   4 => "Name", 5 => "Total amount", 6 => "Extra text", 7 => "Date", 8 => "BEDRIJFSID", 9 => "Verzonden factuur", 10 => "Customer", 11 => "Text", 12 => "VAT percentage", 13 => "Korting (euro)", 14 => "Fine", 15 => "Administratiekosten", 16 => "Verzendkosten", 17 => "Uren op factuur tonen", 18 => "Extra kosten op factuur tonen", 19 => "Producten op factuur tonen");
  }
  else if ($site == "factuurregels") {
      $koloms = array(0 => "Rule", 1 => "Price");
  }
  else if($site == "toevfactuurregels" || $site == "bewfactuurregels" || $site == "overzfactuurregels" || $site == "getfactuurregels")
  {
      $koloms = array(0 => "ID", 1 => "Invoice no.", 2 => "Rule", 3 => "Extra text/Astrix", 4 => "Subject", 5 => "Price", 6 => "VAT", 7 => "BEDRIJFSID");
  }
  else if ($site == "aanmaningsregels") {
      $koloms = array(0 => "Rule", 1 => "Price");
  }
  else if($site == "toevaanmaningsregels" || $site == "bewaanmaningsregels" || $site == "overzaanmaningsregels" || $site == "getaanmaningsregels")
  {
      $koloms = array(0 => "ID", 1 => "Invoice no.", 2 => "Rule", 3 => "Price", 4 => "Extra text/Astrix", 5 => "Subject", 6 => "BEDRIJFSID");
  }
  else if($site == "bedrijf") {
      $koloms = array(0 => "Company name", 1 => "City", 2 => "Country");
  }
  else if($site == "toevbedrijf" || $site == "bewbedrijf" || $site == "overzbedrijf" || $site == "getbedrijf")
  {
      $koloms = array(0 => "ID", 1 => "Company name", 2 => "Gender", 3 => "First name", 4 => "Inner name", 5 => "Last name", 6 => "Address", 7 => "Zip", 8 => "City", 9 => "Country", 10 => "PO Box", 11 => "Zip of the PO Box", 12 => "Homepage", 13 => "Email address", 14 => "Phone", 15 => "Mobile", 16 => "Fax", 17 => "Kamer van Koophandel Nr", 18 => "VAT Nr", 19 => "Kopie paspoort", 20 => "Betaalwijze", 21 => "Bank account nr.", 22 => "In particular", 23 => "Location of the bank (city)", 24 => "Creditcard Nr", 25 => "Type Card", 26 => "Entity", 27 => "Filesname", 28 => "Server space", 29 => "SMS packet", 30 => "IBAN", 31 => "BIC", 32 => "Email addressaddress van het incassobureau", 33 => "Statutory seat", 34 => "Parent company", 35 => "default dir", 36 => "Incorporation date", 37 =>"Legal form", 38 => "First Financial Year OFT", 39 => "Active", 40 => "Commercial Name", 41 => "End of Financial Year", 42 => "Managed from", 43 => "Group company", 44 => "Description", 45 => "Rating", 46 => "Credits", 47 => "Branche", 48 => "Type", 49 => "Dormant company");
  }
  else if ($site == "postenvast") {
      $koloms = array(0 => "Name", 1 => "Description", 2 => "Tarrif", 3 => "Standaard bij elk project");
  }
  else if($site == "toevpostenvast" || $site == "bewpostenvast" || $site == "overzpostenvast" || $site == "getpostenvast")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Description", 3 => "Tarrif (default)", 4 => "Default project task", 5 => "Default availeble time", 6 => "BEDRIJFSID");
  }
  else if ($site == "kostenintern") {
      $koloms = array(0 => "General ledger account", 1 => "Description", 2 => "Kosten (excl VAT)");
  }
  else if($site == "toevkostenintern" || $site == "bewkostenintern" || $site == "overzkostenintern" || $site == "getkostenintern")
  {
      $koloms = array(0 => "ID",     1 => "Referencenr", 2 => "Date", 3 => "Vervaldatum", 4 => "Project",   5=> "General ledger no.",   6 => "Tegenrekening",        7 => "Name", 8 => "Supplier", 9 => "Kosten (excl btw)", 10 => "Btw tarief", 11 => "Amount wat wordt doorberekend aan klant", 12 => "Description", 13 => "BEDRIJFSID",  14 => "Invoice nr. / bonnr (Crediteur)", 15 => "Factuur nr (Debiteur)", 16 => "Employee", 17 => "Betaald", 18 => "Horen deze kosten in het inkoopboek?", 19 => "Voeg scan van factuur toe", 20 => "Cost place", 21 => "Cost type", 22 => "Count");
  }
  else if ($site == "login" || $site == "login_klant") {
      $koloms = array(0 => "Username", 1 => "Relation", 2 => "Account locked");
  }
  else if($site == "toevlogin" || $site == "bewlogin" || $site == "overzlogin" || $site == "getlogin" || $site == "toevlogin_klant" || $site == "bewlogin_klant" || $site == "overzlogin_klant" || $site == "getlogin_klant")
  {
      $koloms = array(0 => "ID", 1 => "Username", 2 => "Password", 3 => "Employee", 4 => "Master", 5 => "Administrator", 6 => "Managing director", 7 => "Time writing 1", 8 => "Time writing 2", 9 => "Trading", 10 => "Is customer account", 11 => "Webmail", 12 => "SMS", 13 => "Forms", 14 => "Diary", 15 => "Invoices", 16 => "Projects", 17 => "Electronic archive", 18 => "Salesforce", 19 => "Newsletters", 20 => "Accountent account", 21 => "Customeren relation management", 22 => "CRM", 23 => "Notice Board", 24 => "Service team", 25 => "Knowledge: FTP and url", 26 => "Create new proposals", 27 => "Brainstorm", 28 => "Cv", 29 => "Employee file", 30 => "Declarations", 31 => "Tickets", 32 => "Leave Administration", 33 => "Finance", 34 => "Volloop-leegloopschema", 35 => "Rental", 36 => "Human Resource Management", 37 => "Rittenadministratoe", 38 => "Planning", 39 => "CMS", 40 => "Security module", 41 => "Knowledge base", 42 => "Documents", 43 => "Telemarketing", 44 => "Deadlines", 45 => "Chatten", 46 => "News reader", 47 => "Notes", 48 => "Verlof invoeren per uur", 49 => "Verlof invoeren voor medewerker", 50 => "Urenregistratie voor medewerker", 51 => "Is this account locked?", 52 => "Client File", 53 => "Hot", 54 => "Sitemap", 55 => "Contracting", 56 => "Only read access", 57 => "Only access to this entity", 58 => "Office style", 59 => "Profile");
  }
  else if ($site == "ingelogt") {
      $koloms = array(0 => "Employee", 1 => "IP-Address", 2 => "Date", 3 => "Time");
  }
  else if($site == "toevingelogt" || $site == "bewingelogt" || $site == "overzingelogt" || $site == "getingelogt")
  {
      $koloms = array(0 => "ID", 1 => "Employee", 2 => "IP-Address", 3 => "Date", 4 => "Time", 5 => "User", 6 => "User", 6 => "Session", 7 => "BEDRIJFSID");
  }
  else if ($site == "deadlinetype") {
      $koloms = array(0 => "Name", 1 => "Description");
  }
  else if($site == "toevdeadlinetype" || $site == "bewdeadlinetype" || $site == "overzdeadlinetype" || $site == "getdeadlinetype")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Deadlines sequence", 3 => "Description", 4 => "Standaard meenemen bij elk project", 5 => "BEDRIJFSID");
  }
  else if ($site == "deadlines") {
      $koloms = array(0 => "Project", 1 => "Description", 2 => "Date", 3 => "Time", 4 => "Employee", 5 => "Additional info");
  }
  else if($site == "toevdeadlines" || $site == "bewdeadlines" || $site == "overzdeadlines" || $site == "getdeadlines")
  {
      $koloms = array(0 => "ID", 1 => "Project", 2 => "Description", 3 => "".tl("DATUMSTART")." (planning)", 4 => "".tl("DATUMSTOP")." (planning)", 5 => "TIJD",    6 => "Deadline (werkenlijk)", 7 => "Medewerker",             8 => "Description", 9 => "Afgerond",    10 => "BEDRIJFSID");
  }
  else if ($site == "documentflow") {
      $koloms = array(0 => "Document template", 1 => "Volgend document", 2 => "Dagen voordat wordt verzonden");
  }
  else if($site == "toevdocumentflow" || $site == "bewdocumentflow" || $site == "overzdocumentflow" || $site == "getdocumentflow")
  {
      $koloms = array(0 => "ID", 1 => "Document template", 2 => "Volgend document", 3 => "Dagen voordat wordt verzonden", 4 => "BEDRIJFSID");
  }
  else if ($site == "tabbladen") {
      $koloms = array(0 => "Tab", 1 => "Function");
  }
  else if($site == "toevtabbladen" || $site == "bewtabbladen" || $site == "overztabbladen" || $site == "gettabbladen")
  {
      $koloms = array(0 => "ID", 1 => "Employee", 2 => "Tab", 3 => "Function", 4 => "BEDRIJFSID");
  }
  else if ($site == "mrktinglead") {
      $koloms = array(0 => "Klant", 1 => "Naam", 2 => "Status", 3 => "Bedrag");
  }
  else if($site == "toevmrktinglead" || $site == "bewmrktinglead" || $site == "overzmrktinglead" || $site == "getmrktinglead")
  {
      $koloms = array(0 => "ID",    1 => "Klant",  2 => "Naam", 3 => "Status", 4 => "Status uitleg", 5 => "Bedrag", 6 => "Bron", 7 => "Bron uitleg", 8 => "Campagne", 9 => "Aangebracht via", 10 => "Wil niet gebeld worden", 11 => "Omschrijving", 12 => "Medewerker", 13 => "Datum", 14 => "Aangepast", 15 => "BEDRIJFSID");
  }
  else if ($site == "grootboekitems") {
      $koloms = array(0 => "Rubriek", 1 => "General ledger no.", 2 => "Name", 3 => "True?", 4 => "Credit / Debet");
  }
  else if($site == "toevgrootboekitems" || $site == "bewgrootboekitems" || $site == "overzgrootboekitems" || $site == "getgrootboekitems")
  {
      $koloms = array(0 => "ID", 1 => "General ledger no.", 2 => "Balance-sheet?",     3=> "Credit of Debet", 4 => "Name", 5 => "Description", 6 => "Editable", 7 => "Rubriek", 8 => "Type", 9 => "VAT %", 10 => "Kleur", 11 => "Belastingpost", 12 => "Toon in grafiek", 13 => "Is salarispost", 14 => "Project", 15 => "BEDRIJFSID", 16 => "Geblokkeerd", 17 => "Niet beschikbaar in administratie", 18 => "Dagboek");
  }
  else if ($site == "balans") {
      $koloms = array(0 => "General ledger no.", 1 => "Tegenrekening", 2 => "Amount", 3 => "Boekdate");
  }
  else if($site == "toevbalans" || $site == "bewbalans" || $site == "overzbalans" || $site == "getbalans")
  {
      $koloms = array(0 => "ID", 1 => "General ledger no.", 2 => "Tegenrekening", 3 => "Relation", 4 => "Purchase book", 5 => "Selling Book", 6 => "Amount", 7 => "Boek jaar", 8 => "Boekdate", 9 => "Description", 10 => "Type dagboek", 11 => "BEDRIJFSID");
  }
  else if ($site == "balansrubriek") {
      $koloms = array(0 => "Name", 1 => "Description");
  }
  else if($site == "toevbalansrubriek" || $site == "bewbalansrubriek" || $site == "overzbalansrubriek" || $site == "getbalansrubriek")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Description", 3 => "BEDRIJFSID");
  }
  else if ($site == "openingsbalans") {
      $koloms = array(0 => "General ledger", 1 => "Boekjaar", 2 => "Debet", 3 => "Credit");
  }
  else if($site == "toevopeningsbalans" || $site == "bewopeningsbalans" || $site == "overzopeningsbalans" || $site == "getopeningsbalans")
  {
      $koloms = array(0 => "ID", 1 => "General ledger", 2 => "Boekjaar", 3 => "Debet", 4 => "Credit", 5 => "Additional info", 6 => "BEDRIJFSID");
  }
  else if ($site == "journaal") {
      $koloms = array(0 => "General ledger no.", 1 => "Amount", 2 => "Date");
  }
  else if($site == "toevjournaal" || $site == "bewjournaal" || $site == "overzjournaal" || $site == "getjournaal")
  {
      $koloms = array(0 => "ID",    1 => "Date", 2 => "General ledger", 3 => "Tegenrekeneing", 4 => "Selling Book", 5 => "Purchase book",     6 => "Amount", 7 => "Description", 8 => "BEDRIJFSID");
  }
  else if($site == "btwaangifte") {
      $koloms = array(0 => "Date", 1 => "Omzet hoog", 2 => "Omzet laag", 3 => "Omzet 0%", 4 => "Omzet vrij");
  }
  else if($site == "toevbtwaangifte" || $site == "bewbtwaangifte" || $site == "overzbtwaangifte" || $site == "getbtwaangifte")
  {
      $koloms = array(0 => "ID", 1 => "Date", 2 => "Binnenland hoog", 3 => "Binnenland laag", 4 => "To mij geleverde diensten binnenland hoog", 5 => "To mij geleverde diensten Binnenland laag", 6 => "Buitenland hoog", 7 => "Buitenland laag", 8 => "To mij geleverde diensten Buitenland hoog", 9 => "To mij geleverde diensten Buitenland laag", 10 => "Binnenland omzet hoog", 11 => "Binnenland omzet laag", 12 => "Binnenland omzet nul", 13 => "Binnenland omzet vrij", 14 => "Binnenland inkoop hoog", 15 => "Binnenland inkoop laag", 16 => "Binnenland inkoop nul", 17 => "Binnenland inkoop vrij", 18 => "Buitenland omzet hoog", 19 => "Buitenland omzet laag", 20 => "Buitenland omzet nul", 21 => "Buitenland omzet vrij", 22 => "Buitenland inkoop hoog", 23 => "BUIinkoop laag", 24 => "Buitenland inkoop nul", 25 => "Buitenland inkoop vrij", 26 => "BEDRIJFSID");
  }
  else if ($site == "rittenadmin") {
      $koloms = array(0 => "Date", 1 => "Beginstand", 2 => "Eindstand",  3 => "Customer",  4 => "From",  5 => "To");
  }
  else if($site == "toevrittenadmin" || $site == "bewrittenadmin" || $site == "overzrittenadmin" || $site == "getrittenadmin")
  {
      $koloms = array(0 => "ID",        1 => "Beginstand teller", 2 => "Eindstand teller", 3 => "Date",          4 => "From",    5 => "Naar",  6 => "Description", 7 => "Customer",        8 => "Project",   9 => "Employee",      10 => "Characteristic", 11 => "Price per km", 12 => "Behoort bij factuur", 13 => "Staat is dit uw prive auto?", 14 => "BEDRIJFSID");
  }
  else if ($site == "brainstorm") {
      $koloms = array(0 => "Type", 1 => "Name",2 => "Date", 3 => "Connected project");
  }
  else if($site == "toevbrainstorm" || $site == "bewbrainstorm" || $site == "overzbrainstorm" || $site == "getbrainstorm")
  {
      $koloms = array(0 => "ID",    1 => "Name",  2 => "Type", 3 => "Date",      4 => "Employee", 5 => "Connected project", 6 => "BEDRIJFSID");
  }
  else if ($site == "discussion") {
      $koloms = array(0 => "Type", 1 => "Stelling",2 => "Date");
  }
  else if($site == "toevdiscussion" || $site == "bewdiscussion" || $site == "overzdiscussion" || $site == "getdiscussion")
  {
      $koloms = array(0 => "ID",    1 => "Type",  2 => "Description", 3 => "Onderdeel van",   4 => "Employee", 5 => "Date", 6 => "BEDRIJFSID");
  }
  else if ($site == "reacties") {
      $koloms = array(0 => "Additional info", 1 => "Date",2 => "Time");
  }
  else if($site == "toevreacties" || $site == "bewreacties" || $site == "overzreacties" || $site == "getreacties")
  {
      $koloms = array(0 => "ID",    1 => "Additional info",  2 => "Eens / Voor", 3 => "Oneens / Tegen",  4 => "Geen mening / Onthouding", 5 => "Different", 6 => "Different", 7 => "BEDRIJFSID", 8 => "Date",    9 => "Time", 10 => "Stelling / Vraag / Poll", 11 => "Responce", 12 => "BEDRIJFSID");
  }
  else if($site == "toevpoll" || $site == "getpoll")
  {
      $koloms = array(0 => "ID",    1 => "Additional info",  2 => "Eens / Voor", 3 => "Oneens / Tegen",  4 => "Geen mening / Onthouding", 5 => "Different", 6 => "Different", 7 => "BEDRIJFSID", 8 => "Date",    9 => "Time", 10 => "Stelling / Vraag / Poll", 11 => "Responce", 12 => "BEDRIJFSID");
  }
  else if ($site == "agendanationaal") {
      $koloms = array(0 => "Name", 1 => "From", 2 => "Tot", 3 => "Iedereen vrij");
  }
  else if($site == "toevagendanationaal" || $site == "bewagendanationaal" || $site == "overzagendanationaal" || $site == "getagendanationaal")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Description", 3 => "Date (from)", 4 => "Date (till)", 5 => "Iederjaar op dezelfde dag", 6 => "Hebben alle werknemers een dag vrij?", 7 => "BEDRIJFSID");
  }
  else if ($site == "corve") {
      $koloms = array(0 => "Name", 1 => "From", 2 => "Tot", 3 => "Description");
  }
  else if($site == "toevcorve" || $site == "bewcorve" || $site == "overzcorve" || $site == "getcorve")
  {
      $koloms = array(0 => "ID", 1 => "Day van de week", 2 => "Time", 3 => "Employee", 4 => "Description", 5 => "BEDRIJFSID");
  }
  else if ($site == "leads") {
      $koloms = array(0 => "Name", 1 => "Date", 2 => "Reporter");
  }
  else if($site == "toevleads" || $site == "bewleads" || $site == "overzleads" || $site == "getleads")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Date", 3 => "Employee", 4 => "Customeren en relations", 5 => "Description", 6 => "BEDRIJFSID");
  }
  else if ($site == "abcdinet") {
      $koloms = array(0 => "Typerend woord", 1 => "Text");
  }
  else if($site == "toevabcdinet" || $site == "bewabcdinet" || $site == "overzabcdinet" || $site == "getabcdinet")
  {
      $koloms = array(0 => "ID",    1 => "Kenmerkend woord", 2 => "Text", 3 => "Employee", 4 => "BEDRIJFSID");
  }
  else if ($site == "documentbeheer") {
      $koloms = array(0 => "Name", 1 => "File");
  }
  else if($site == "toevdocumentbeheer" || $site == "bewdocumentbeheer" || $site == "overzdocumentbeheer" || $site == "getdocumentbeheer")
  {
      $koloms = array(0 => "ID",    1 => "Name", 2 => "Description", 3 => "Category",    4 => "Role", 5 => "Employee", 6 => "File", 7 => "Filename", 8 => "Type", 9 => "Size", 10 => "Content", 11 => "TAGS", 12 => "Date", 12 => "BEDRIJFSID");
  }
  else if ($site == "documentcatagorie") {
      $koloms = array(0 => "Description");
  }
  else if($site == "toevdocumentcatagorie" || $site == "bewdocumentcatagorie" || $site == "overzdocumentcatagorie" || $site == "getdocumentcatagorie")
  {
      $koloms = array(0 => "ID", 1 => "Is onderdeel van", 2 => "Name", 3 => "BEDRIJFSID");
  }
  else if ($site == "liquiditeitsbegroting") {
      $koloms = array(0 => "Description");
  }
  else if($site == "toevliquiditeitsbegroting" || $site == "bewliquiditeitsbegroting" || $site == "overzliquiditeitsbegroting" || $site == "getliquiditeitsbegroting")
  {
      $koloms = array(0 => "ID", 1 => "Kosten / Opbrengsten", 2 => "Name", 3 => "Amount", 4 => "Description", 5 => "Date", 6 => "BEDRIJFSID");
  }
  else if ($site == "projectbegroting") {
      $koloms = array(0 => "Description");
  }
  else if($site == "toevprojectbegroting" || $site == "bewprojectbegroting" || $site == "overzprojectbegroting" || $site == "getprojectbegroting")
  {
      $koloms = array(0 => "ID", 1 => "Kosten / Opbrengsten", 2 => "Name", 3 => "Amount", 4 => "Description", 5 => "Date", 6 => "BEDRIJFSID");
  }
  else if ($site == "text") {
    $koloms = array(0 => "Niveau", 1 => "Pagina titel", 2 => "Volgorde", 3 => "Online", 4 => "Url");
  }
  else if($site == "toevtext" || $site == "bewtext" || $site == "overztext" || $site == "gettext")
  {
    $koloms = array(0 => "ID", 1 => "URL", 2 => "Niveau", 3 => "Onderdeel van", 4 => "Type website", 5 => "Pagina titel", 6 => "Soort", 7 => "Positie in menu", 8 => "Online zichtbaar", 9 => "Tekst", 10 => "Hits", 11 => "Pagina beschrijving (metadata)", 12 => "Pagina tags (metadata)", 13 => "Pagina titel (metadata)", 14 => "BEDRIJFSID");
  }
  else if ($site == "intranet") {
    $koloms = array(0 => "Niveau", 1 => "Pagina titel", 2 => "Type tekst", 3 => "Volgorde", 4 => "Online");
  }
  else if($site == "toevintranet" || $site == "bewintranet" || $site == "overzintranet" || $site == "getintranet")
  {
    $koloms = array(0 => "ID", 1 => "Niveau", 2 => "Onderdeel van", 3 => "Type website", 4 => "Pagina titel", 5 => "Type", 6 => "Positie in menu", 7 => "Online zichtbaar", 8 => "Text", 9 => "Hits", 10 => "BEDRIJFSID");
  }
  else if ($site == "trash") {
    $koloms = array(0 => "Niveau", 1 => "Pagina titel", 2 => "Type tekst", 3 => "Online");
  }
  else if ($site == "sitestyle") {
    $koloms = array(0 => "Url", 1 => "Template", 2 => "field", 3 => "Basislocatie");
  }
  else if($site == "toevsitestyle" || $site == "bewsitestyle" || $site == "overzsitestyle" || $site == "getsitestyle")
  {
    $koloms = array(0 => "ID", 1 => "Url", 2 => "Template", 3 => "Title", 4 => "Basislocatie", 5 => "BEDRIJFSID");
  }
  else if ($site == "extramodules") {
      $koloms = array(0 => "Name", 1 => "Link", 2 => "Role");
  }
  else if($site == "toevextramodules" || $site == "bewextramodules" || $site == "overzextramodules" || $site == "getextramodules")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Link", 3 => "Doel", 4 => "Picture", 5 => "Volgorde", 6 => "Role", 7 => "BEDRIJFSID");
  }
  else if ($site == "logboek") {
      $koloms = array(0 => "Category", 1 => "Date", 2 => "Employee", 2 => "Description");
  } 
  else if($site == "toevlogboek" || $site == "bewlogboek" || $site == "overzlogboek" || $site == "getlogboek")
  {
      $koloms = array(0 => "ID", 1 => "Category", 2 => "Name", 3 => "Description", 4 => "Date", 5 => "Employee", 6 => "BEDRIJFSID");
  }
  else if ($site == "stam_titels") {
      $koloms = array(0 => "Titel", 1 => "Voor of achter de naam");
  }
  else if($site == "toevstam_titels" || $site == "bewstam_titels" || $site == "overzstam_titels" || $site == "getstam_titels")
  {
      $koloms = array(0 => "ID", 1 => "Titel", 2 => "Voor of achter de naam", 3 => "BEDRIJFSID");
  }  
  else if ($site == "stam_productcat") {
      $koloms = array(0 => "Naam", 1 => "Standaard omzetrekening", 2 => "Standaard kostenrekening", 3 => "Standaard voorraadrekening");
  }
  else if($site == "toevstam_productcat" || $site == "bewstam_productcat" || $site == "overzstam_productcat" || $site == "getstam_productcat")
  {
      $koloms = array(0 => "ID", 1 => "Is subcategorie van", 2 => "Naam", 3 => "Opmerking", 4 => "Standaard omzetrekening", 5 => "Standaard voorraadrekening", 6 => "Standaard kostenrekening", 7 => "BEDRIJFSID", 8 => "Volgordenummer");
  }
  else if ($site == "stam_ordners") {
      $koloms = array(0 => "Name", 1 => "Hangt onder");
  } 
  else if($site == "toevstam_ordners" || $site == "bewstam_ordners" || $site == "overzstam_ordners" || $site == "getstam_ordners")
  {
      $koloms = array(0 => "ID", 1 => "Is subcategorie van", 2 => "Name", 3 => "Description", 4  => "BEDRIJFSID");
  }
  else if(substr($site,0,5) == "stam_" || substr($site,0,9) == "verwstam_" || substr($site,0,9) == "toevstam_" || substr($site,0,8) == "bewstam_" || substr($site,0,10) == "overzstam_" || substr($site,0,8) == "getstam_")
  {
      if ($site == $tabel) { $koloms = array(0 => "Name", 1 => "Description"); }
      if ($site == "toev$tabel" || $site == "bew$tabel" || $site == "overz$tabel" || $site == "get$tabel") { $koloms = array(0 => "ID", 1 => "Name", 2 => "Description", 3 => "BEDRIJFSID"); }
  } 
  else if ($site == "typerelation") {
      $koloms = array(0 => "Name", 1 => "Opgenomen relations");
  } 
  else if($site == "toevtyperelation" || $site == "bewtyperelation" || $site == "overztyperelation" || $site == "gettyperelation")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Deze relations opnemen in alle uitvalmenutjes", 3 => "Additional info", 4 => "BEDRIJFSID");
  } 
  else if($site == "formulieren") {
      $koloms = array(0 => "Formuliernaam", 1 => "Formuliertype");
  } 
  else if($site == "toevformulieren" || $site == "bewformulieren" || $site == "overzformulieren" || $site == "getformulieren")
  {
      $koloms = array(0 => "ID", 1 => "Formuliernaam", 2 => "Type formulier", 3 => "Company name", 4 => "Gender",   5 => "T.n.v. (Contactpersoon)",  6 => "Initials (Contactpersoon)", 7 => "Contactpersoon",  8 => "Email address (Contactpersoon)", 9 => "Phone (Contactpersoon)", 10 => "Mobile (Contactpersoon)", 11 => "Address",       12 => "Land (Kantoor)", 13 => "Address (Kantoor)", 14 => "Telefooon (Kantoor)", 15 => "Fax (Kantoor)", 16 => "Homepage (Kantoor)", 17 => "Email address (Kantoor)", 18 => "PO Box",     19 => "Kamer van Koophandel Nr", 20 => "VAT Nr",      21 => "Betaalwijze", 22 => "Bank account nr.", 23 => "In particular", 24 => "Location of the bank (city)", 25 => "Creditcard Nr", 26 => "Type kaart", 27 => "Vervaldate kaart", 28 => "Marktsegment", 29 => "Birthday", 30 => "Who is responsible", 31 => "Start-date", 32 => "Eind-date", 33 => "Total (type 1)", 34 => "Total (type 2)", 35 => "Total (type 3)", 36 => "Total (type 4)", 37 => "Hoe heeft u ons gevonden", 38 => "Special information", 39 => "BEDRIJFSID");
  } 
  else if($site == "formuliereningevult") {
      $koloms = array(0 => "Invuldate", 1 => "Formulier", 2 => "Name", 3 => "Special information");
  } 
  else if($site == "toevformuliereningevult" || $site == "bewformuliereningevult" || $site == "overzformuliereningevult" || $site == "getformuliereningevult")
  {
      $koloms = array(0 => "ID", 1 => "Invuldate", 2 => "Formulier",    3 => "Company name", 4 => "Gender",       5 => "T.n.v.",     6 => "Initials", 7 => "First name", 8 => "Inner name", 9 => "Last name", 10 => "Email address", 11 => "Phone", 12 => "Mobile", 13 => "Address", 14 => "Number of the house", 15 => "Addition", 16 => "Zip", 17 => "City", 18 => "Land", 19 => "Address (Kantoor)", 20 => "Number of the house (Kantoor)", 21 => "Addition (Kantoor)", 22 => "Zip (Kantoor)", 23 => "Vestigingsplaats (Kantoor)", 24 => "Telefooon (Kantoor)", 25 => "Fax (Kantoor)", 26 => "Homepage (Kantoor)", 27 => "Email address (Kantoor)", 28 => "PO Box", 29 => "Zip of the PO Box", 30 => "Kamer van Koophandel Nr", 31 => "VAT Nr", 32 => "Factuur per mail?", 33 => "Bank account nr.", 34 => "In particular", 35 => "Location of the bank (city)", 36 => "Creditcard Nr", 37 => "Type kaart", 38 => "Vervaldate kaart", 39 => "Marktsegment", 40 => "Birthday", 41 => "Who is responsible", 42 => "Start-date", 43 => "Eind-date",    44 => "Total (type 1)", 45 => "Total (type 2)", 46 => "Total (type 3)", 47 => "Total (type 4)", 48 => "Lead?", 49 => "Wie heeft hem gevonden?", 50 => "Special information", 51 => "BEDRIJFSID");
  } 
  else if($site == "telemarkt") {
      $koloms = array(0 => "Formuliernaam", 1 => "Formuliertype");
  } 
  else if($site == "toevtelemarkt" || $site == "bewtelemarkt" || $site == "overztelemarkt" || $site == "gettelemarkt")
  {
      $koloms = array(0 => "ID", 1 => "Formuliernaam", 2 => "Hangt onder sessie",3 => "Type",         4 => "Text",   5 => "Volgorde",  6 => "Stats", 7 => "BEDRIJFSID");
  } 
  else if ($site == "telemarktform") {
      $koloms = array(0 => "Name", 1 => "Additional info");
  } 
  else if($site == "toevtelemarktform" || $site == "bewtelemarktform" || $site == "overztelemarktform" || $site == "gettelemarktform")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Welke klanten zijn gebelt?", 3 => "Additional info", 4 => "BEDRIJFSID");
  } 
  else if ($site == "watisergebeurt") {
      $koloms = array(0 => "Date", 1 => "Description");
  } 
  else if($site == "toevwatisergebeurt" || $site == "bewwatisergebeurt" || $site == "overzwatisergebeurt" || $site == "getwatisergebeurt")
  {
      $koloms = array(0 => "ID", 1 => "Date", 2 => "Omschijving", 3 => "Action", 4 => "Table", 5 => "Item-id", 6 => "BEDRIJFSID");
  } 
  else if ($site == "cursus") {
      $koloms = array(0 => "Name", 1 => "Instelling", 2 => "Employee");
  } 
  else if($site == "toevcursus" || $site == "bewcursus" || $site == "overzcursus" || $site == "getcursus")
  {
      $koloms = array(0 => "ID", 1 => "Name cursus", 2 => "Zoekterm van de cursus", 3 => "Instelling die de cursus verzorgt", 4 => "Kosten van de cursus", 5 => "Cursus inhoud &  programma", 6 => "Hoeveel tijd neemt het in beslag", 7 => "Cursus start op", 8 => "Cursus eindigd op", 9 => "True is meer informatie te vinden", 10 => "Valuering uitgedrukt in getal", 11 => "Ervaring en waardering opmerkingen", 12 => "Employee", 13 => "BEDRIJFSID");
  }
  else if ($site == "pers_loonstrook") {
      $koloms = array(0 => "Medewerker", 1 => "Datum", 2 => "Bestand");
  }
  else if($site == "toevpers_loonstrook" || $site == "bewpers_loonstrook" || $site == "overzpers_loonstrook" || $site == "getpers_loonstrook")
  {
      $koloms = array(0 => "ID", 1 => "Medewerker", 2 => "Datum", 3 => "Bestand", 4 => "Omschrijving", 5 => "BEDRIJFSID");
  }
  else if ($site == "selection") {
      $koloms = array(0 => "Name", 1 => "Velden", 2 => "Zoeken in");
  } 
  else if($site == "bewselection" || $site == "overzselection" || $site == "overzselectionExcel" || $site == "getselection")
  {
      $koloms = array(0 => "ID", 1 => "Name", 2 => "Toon velden", 3 => "Uit de tabel", 4 => "Onder voorwaarde", 5 => "Order op", 6 => "Limiteer uitkomst", 7 => "Employee", 8 => "Administratie");
  }
  else if ($site == "attenderingen") {
      $koloms = array(0 => "Subject", 1 => "Date", 2 => "Time", 3 => "Employee");
  } 
  else if($site == "toevattenderingen" || $site == "bewattenderingen" || $site == "overzattenderingen" || $site == "getattenderingen")
  {
      $koloms = array(0 => "ID", 1 => "Herinner mij aan", 2 => "Text", 3 => "Date", 4 => "Time", 5 => "PERSMS", 6 => "PERSONEELSLID", 7 => "BEDRIJFSID");
  }
  else if ($site == "verkochtderving") {
      $koloms = array(0 => "Datum", 1 => "Artikel", 2 => "Onverklaarbaar", 3 => "Derving", 4 => "Retour");
  }
  else if($site == "toevverkochtderving" || $site == "bewverkochtderving" || $site == "overzverkochtderving" || $site == "getverkochtderving")
  {
      $koloms = array(0 => "ID",    1 => "Datum",        2 => "Artikel",  3 => "Onverklaarbaar",  4 => "Derving", 5 => "Retour", 6 => "Varanten", 7 => "BEDRIJFSID");
  }
  else if ($site == "kosteninternspecs") {
      $koloms = array(0 => "Referentie", 1 => "Project", 2 => "Total", 3 => "Eenheid", 4 => "Description");
  }
  else if($site == "toevkosteninternspecs" || $site == "bewkosteninternspecs" || $site == "overzkosteninternspecs" || $site == "getkosteninternspecs")
  {
      $koloms = array(0 => "ID", 1 => "Referentie", 2 => "Datum", 3 => "Project", 4 => "Description", 5 => "Inkoopprijs", 6 => "Verkoopprijs", 7 => "Kostenplaats", 8 => "Kostensoort", 9 => "Total", 10 => "Eenheid", 11 => "BEDRIJFSID");
  } 
  else if ($site == "orderbevestiging") {
      $koloms = array(0 => "BONNR", 1 => "BESTELDATUM", 2 => "WANNEERUITGELEVERD", 3 => "OFFERTEAANGEMAAKT", 4 => "STATUS");
  } 
  else if($site == "toevorderbevestiging" || $site == "beworderbevestiging" || $site == "overzorderbevestiging" || $site == "getorderbevestiging")
  {
      $koloms = array(0 => "ID", 1 => "BONNR", 2 => "BESTELDATUM", 3 => "WANNEERUITGELEVERD", 4 => "OFFERTEAANGEMAAKT", 5 => "OFFERTEGOEDGEKEURD", 6 => "OFFERTEDOORGESTUURD", 7 => "ORDERBEVAANGEMAAKT", 8 => "ORDERBEVGOEDGEKEURD", 9 => "ORDERBEVDOORGESTUURD", 10 => "DRUKPROEFAANGEMAAKT", 11 => "DRUKPROEFGOEDGEKEURD", 12 => "DRUKPROEFDOORGESTUURD", 13 => "INGEKOCHT", 14 => "AFGEBROKEN", 15 => "WENSONTVANGDATUM", 16 => "TEXTID", 17 => "OFFERTE", 18 => "OPDRACHTBEVESTIGING", 19 => "DRUKPROEF", 20 => "VERSTUURD", 21 => "ISUITGELEVERD", 22 => "OPMERKING", 23 => "GEFACTUREERD", 24 => "STATUS", 25 => "BEDRIJFSID");
  } 
  else if ($site == "dossiers") {
      $koloms = array(0 => "Naam", 1 => "Datum tijd", 2 => "Medewerker", 3 => "Gekoppelt aan");
  } 
  else if($site == "toevdossiers" || $site == "bewdossiers" || $site == "overzdossiers" || $site == "getdossiers")
  {
      $koloms = array(0 => "ID", 1 => "Dossier nr.", 2 => "Naam", 3 => "Startdatum", 4 => "Medewerker", 5 => "Opmerking", 6 => "Koppeling", 7 => "BEDRIJFSID");
  } 
  else if ($site == "factuurtemplate") {
      $koloms = array(0 => "Logo op factuur", 1 => "Uren op factuur", 2 => "Footer op factuur");
  } 
  else if($site == "toevfactuurtemplate" || $site == "bewfactuurtemplate" || $site == "overzfactuurtemplate" || $site == "getfactuurtemplate")
  {
      $koloms = array(0 => "ID", 1 => "Logo op factuur", 2 => "Uren op factuur", 3 => "Factuuropmaak", 4 => "Factuurtekst", 5 => "Eventueel andere factuuropmaak voor incasso facturen", 6 => "Tekst boven of onder specificatie tonen", 7 => "Footer op factuur", 8 => "Standaard tekst voor de mail", 9 => "Tekst voor de mail na vervaldatum factuur", 10 => "BEDRIJFSID", 11 => "Standaard tekst voor de mail bij automatisch incasso facturen");
  }
    else if ($site == "polissen") {
      $koloms = array(0 => "Polisnummer", 1 => "Status", 2 => "Product", 3 => "Leverancier", 4 => "Klant", 5 => "Adviseur", 6 => "Ingangsdatum", 7 => "Einddatum");
  }
  else if($site == "toevpolissen" || $site == "bewpolissen" || $site == "overzpolissen" || $site == "getpolissen")
  {
      $koloms = array(0 => "ID", 1 => "Polisnummer", 2 => "Status", 3 => "Product", 4 => "Leverancier", 5 => "Klant", 6 => "Mutatiedatum", 7 => "Ingangsdatum", 8 => "Einddatum", 9 => "Adviseur", 10 => "Soort verzekering", 11 => "Koopsom", 12 => "Premie", 13 => "Premie frequentie", 14 => "Rente", 15 => "Looptijd rente", 16 => "Clausulering", 17 => "Toelichting", 18 => "IPBEDRAG", 19 => "IPAANTALKEER", 20 => "IPLOOPTIJD", 21 => "IPFREQUENTIE", 22 => "APBEDRAG", 23 => "APAANTALKEER", 24 => "APLOOPTIJD", 25 => "APFREQUENTIE", 26 => "Ingevoerd door", 27 => "BEDRIJFSID");
  }  else if ($site == "polis_soortverzekering") {
      $koloms = array(0 => "Naam");
  }
  else if($site == "toevpolis_soortverzekering" || $site == "bewpolis_soortverzekering" || $site == "overzpolis_soortverzekering" || $site == "getpolis_soortverzekering")
  {
      $koloms = array(0 => "ID", 1 => "Naam", 2 => "STARTDATUM", 3 => "EINDDATUM", 4 => "EINDDATUMPREMIEBET", 5 => "KOOPSOM", 6 => "PREMIE", 7 => "PREMIEFREQUENTIE", 8 => "RENTE", 9 => "LOOPTIJDRENTE", 10 => "CLAUSULERING", 11 => "BELEGGEN", 12 => "BEDRIJFSID");
  } 
  else if ($site == "contracten") {
      $koloms = array(0 => "Reference", 1 => "Type", 2 => "Item", 3 => "Name", 4 => "Start Date", 5 => "Expiration", 6 => "Contract Value");
  } 
  else if($site == "bewideal")
  {
      $koloms = array(0 => "ID", 1 => "Sleutel", 2 => "iDeal URL van uw bank", 3 => "Acceptant ID", 4 => "MERCHANTRETURNURL", 5 => "Certificaat", 6 => "Private Key", 7 => "BEDRIJFSID");
  } else if($site == "bewmsp")
  {
      $koloms = array(0 => "ID", 1 => "Account Id", 2 => "Site Id", 3 => "Site code", 4 => "MultiSafepay grootboekrekening", 5 => "BEDRIJFSID");
  }
  else if($site == "toevcontracten" || $site == "bewcontracten" || $site == "overzcontracten" || $site == "getcontracten")
  {
      $koloms = array(0 => "ID", 1 => "Reference", 2 => "Type", 3 => "Items", 4 => "Name", 5 => "Description", 6 => "Supplier", 7 => "External Reference", 8 => "Signed on", 9 => "Signed By", 10 => "Start Date", 11 => "Expiration", 12 => "Approved", 13 => "Valid until", 14 => "Renewal Frequancy", 15 => "Renewal Period", 16 => "Renewal manually", 17 => "Experation Frequency", 18 => "Experation Period", 19 => "Billing Frequency", 20 => "Cost per period", 21 => "Connections", 22 => "Tags", 23 => "Total value", 24 => "Entity", 25 => "Last modified");
  }
  else if ($site == "mrktingcampagne") {
      $koloms = array(0 => "Naam", 1 => "Status", 2 => "Start op", 3 => "Loopt tot", 4 => "Verwachte omzet", 5 => "Verwachte kosten");
  }
  else if($site == "toevmrktingcampagne" || $site == "bewmrktingcampagne" || $site == "overzmrktingcampagne" || $site == "getmrktingcampagne")
  {
      $koloms = array(0 => "ID",    1 => "Naam",  2 => "Omschrijving", 3 => "Type", 4 => "Status", 5 => "Budget", 6 => "Verwachte omzet", 7 => "Verwachte kosten", 8 => "Start op", 9 => "Loopt tot", 10 => "BEDRIJFSID");
  } else {
      $koloms = array(0 => "Name");
  }


  return $koloms;
}