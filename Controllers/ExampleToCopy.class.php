<?php

namespace Controllers;

// ToDo: Build in an autoloader
include_once './Controllers/Helpers/PageDefault.class.php';


/**
 * Class ExampleToCopy
 *
 * @package components
 */
class ExampleToCopy extends Helpers\PageDefault
{
    private $user,
        $company,
        $options;

    /**
     * ExampleToCopy constructor.
     */
    public function __construct($user = 0, $company = 0, $options = [])
    {
        $optionDefaults = array(
            'ajax' => false
        );

        foreach ($optionDefaults as $key => $defaultValue) {
            if (!isset($options[$key])) {
                $options[$key] = $defaultValue;
            }
        }

        $this->options = $options;
        $this->user = $user;
        $this->company = $company;
    }

    /**
     * GET overview table with all files and filters. Use AJAX request for 'clean' feed.
     */
    public function index()
    {
        $title = "Example To Copy";
        $baseLink = "content.php?SITE=example_to_copy";
        $table = "example_to_copy";
        $links = array(
            'create' => $baseLink . "_create",
            'edit' => oft_inputform_link_short($baseLink . "_edit" . "&ID=-ID-"),
            'delete' => ""
        );
        $submenuItems = oft_inputform_link($links['create'], "New row(s)");

        $fields = oft_get_structure($this->user, $this->company, "example_to_copy", "arrayVelden");
        $fieldNames = oft_get_structure($this->user, $this->company, "example_to_copy", "arrayVeldnamen");
        $fieldTypes = oft_get_structure($this->user, $this->company, "example_to_copy", "arrayVeldType");
        $fieldSearch = oft_get_structure($this->user, $this->company, "example_to_copy", "arrayZoekvelden");

        if ($this->options['ajax']) {
            echo oft_tabel_content($this->user, $this->company, $fields, $fieldNames, $fieldTypes, $fieldSearch,
                $table, 'BESTANDSNAAM', $links['create'], $links['edit'], $links['delete'], "", array(), true);
            return;
        }

        $contentFrame = oft_tabel($this->user, $this->company, $fields, $fieldNames, $fieldTypes, $fieldSearch,
            $table, 'ID', $links['create'], $links['edit'], $links['delete'], "", true);

        echo oft_framework_basic(0, 0, $contentFrame, $title, $submenuItems);
        return;
    }

    /**
     * GET create form for adding file(s).
     */
    public function create()
    {
        $title = "Add a row";
        $table = "example_to_copy";

        $templateFile = "./templates/example_to_copy_create.html";

        oft_add_page($this->user, $this->company, $templateFile, $table, $title);
    }

    /**
     *
     * GET edit form for editing a single file.
     */
    public function edit()
    {
        $title = "Edit a row";
        $table = "example_to_copy";

        $templateFile = "./templates/example_to_copy_edit.html";

        oft_edit_page($this->user, $this->company, $templateFile, $table, $title);
    }

    /**
     *
     * POST for adding new rows in the database
     */
    public function store()
    {
        if (!is_array($_POST) || !$this->options['ajax']) {
            return json_encode("Invalid data. Expected filled _POST");
        }

        global $pdo;
        return json_encode('success');
    }

    /**
     *
     * PATCH updating existing rows in the database.
     */
    public function update()
    {
        if (!is_array($_POST) || !$this->options['ajax']) {
            return json_encode("Invalid data. Expected filled _POST");
        }

        global $pdo;

        return json_encode('success');
    }

    private function OtherFunctionForInClassUse()
    {
        return true;
    }
}