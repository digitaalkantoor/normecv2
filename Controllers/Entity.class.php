<?php

namespace Controllers;

// ToDo: Build in an autoloader
include_once './Controllers/Helpers/PageDefault.class.php';
include_once './Controllers/Helpers/Page.interface.php';
include_once './Controllers/FileManagement.class.php';

use CloudVPS;
use Controllers\Helpers\Helper;
use Controllers\Helpers\Page;
use Controllers\Helpers\PageDefault; //Already included

/**
 * Class Entity
 *
 * @package components
 */
class Entity extends PageDefault implements Page
{
    public $links,
        $vars;

    /**
     * Entity constructor.
     */
    public function __construct($user = 0, $company = 0, $options = [])
    {

        $whitelist = [
            'ajax' => false,
            'popup' => false,
            'class' => 'entity',
            'keyName' => check("default_name_of_entity", "target", $company),
            'table' => 'entities',
            'structure' => 'entities',
            'title' => 'M&A Overview',
        ];

        parent::__construct($user, $company, $whitelist, $options);

        $linkBase = "content.php?SITE=entity";
        $this->links = [
            'base' => $linkBase,
            'create' => $linkBase . "_create",
            'store' => $linkBase . "_store_ajax",
            'update' => $linkBase . "_update_ajax&ID=-ID-",
            'edit' => $linkBase . "_view&ID=-ID-",
            'delete' => $linkBase . "_delete",
        ];
    }

    /**
     * GET overview table with all files and filters. Use AJAX request for 'clean' feed.
     */
    public function index()
    {

//        dpm($this->links['create']);

//        $submenuItems = oft_inputform_link($this->links['create'] . '', "New " . $this->keyName);
//        $submenuItems .= oft_inputform_link($this->links['base'] . '_edit_structure_popup', "Edit structure");
//        $submenuItems .= oft_inputform_link($this->links['base'] . '_edit_option_popup', "Edit option");

//        $submenuItems = "<a class=\"cursor\" href='" . $this->links['create'] . "'>New " . $this->keyName . "</a>";
        $submenuItems = "<a class=\"cursor\" href='" . $this->links['base'] . "_change_structure'>Table Format</a>";
        $submenuItems .= "<a class=\"cursor\" href='" . $this->links['base'] . "_change_option'>CRM Structure</a>";


        return $this->viewOverview($this->links, $submenuItems);
    }

    public function test()
    {
        $submenuItems = "";
        return $this->viewOverview($this->links, $submenuItems);
    }

    public function changeStructure()
    {
        $view = $this->editStructure();
        return $this->page($view);
    }

    public function changeOption()
    {
        $view = $this->editOption();
        return $this->page($view);
    }

    public function home()
    {
        global $pdo;
        $userid = getUserID();
        // TODO QUALITY_MANAGEMENT

        $colums = "BESTANDEN, CRM, CONTRACTS, COMPLIANCE, MA, INTEGRATIONCHECKLIST";
        $userInfo = getRechten($userid, $colums);

        ob_start();
        include_once './templates/choice.php';

        $view = ob_get_clean();
        $_SESSION['choice'] = 'unmade';

        return $this->page($view);
    }

    public function home2() {
        getRechten(getUserID(), "MA");
        if (!isset($_REQUEST['VISUAL'])) {
            $_REQUEST['VISUAL'] = "start";
            $_REQUEST['MENUID'] = "9";
        }
        $_SESSION['choice'] = 'm&a';
        return $this->visuals();
    }

    /**
     * GET show form for viewing object(s).
     */
    public function create()
    {
        global $pdo;

        $queries = [
            'stages' => 'SELECT ID, NAME FROM stages WHERE DELETED = 0 ORDER BY `ORDER`',
            'countries' => 'SELECT ID, NAME FROM countries WHERE DELETED = 0',
            'segments' => 'SELECT ID, NAME FROM segments WHERE DELETED = 0 ORDER BY `ORDER`',
            'activities' => 'SELECT ID, NAME FROM activities WHERE DELETED = 0 ORDER BY `ORDER`',
            'strategics' => 'SELECT ID, NAME FROM strategics WHERE DELETED = 0 ORDER BY `ORDER`',
            'managements' => 'SELECT ID, NAME FROM managements WHERE DELETED = 0 ORDER BY `ORDER`',
            'tags' => 'SELECT ID, NAME FROM tags WHERE DELETED = 0 ORDER BY `ORDER`',
            'accreditations' => 'SELECT ID, NAME FROM accreditations WHERE DELETED = 0 ORDER BY `ORDER`',
            'advisors' => 'SELECT entities.NAME AS NAME, entity_persons.ID AS ID FROM entity_persons
                              JOIN entities ON entities.ID = entity_persons.ENTITY_ID
                              WHERE entities.ADVISOR = 1
                              GROUP BY entities.NAME'
        ];
        foreach ($queries as $var => $query) {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll($pdo::FETCH_KEY_PAIR);
            $this->vars[$var] = $result;
            $$var = $result;
        }

        ob_start();
        include_once './templates/entity_create.php';
        $view = ob_get_clean();

        return $this->page($view);
//        return $view;
    }

    /**
     * GET show form for viewing object(s).
     */
    public function view()
    {
        global $pdo;

        $query = 'SELECT CONCAT(COALESCE(VOORNAAM, ""), " ", COALESCE(TUSSENVOEGSEL, ""), " ", COALESCE(ACHTERNAAM, "")) AS NAAM FROM personeel WHERE ID = :id LIMIT 1';
        $stmt = $pdo->prepare($query);
        $stmt->bindValue('id', getUserID());
        $stmt->execute();
        $userName = $stmt->fetchColumn();

        $stmt = $pdo->prepare("SELECT ID, NAME FROM pipeline_types WHERE `DELETED` = 0");
        $stmt->execute();
        $pipeline_types = $stmt->fetchAll($pdo::FETCH_KEY_PAIR);


        $id = Helper::reqVal('ID');
        $recover = Helper::reqVal('RECOVER');
        $links = $this->links;
        $links['edit'] = str_replace('-ID-', $id, $links['edit']);
        $links['update'] = str_replace('-ID-', $id, $links['update']);

        if (!$id) {
            header("Location: /content.php?SITE=entity");
        }
        $queries = [
            'entity' => "SELECT
                    `entities`.*,
                    CONCAT(COALESCE(`personeel`.`VOORNAAM`, \"\"), ' ', COALESCE(`personeel`.`TUSSENVOEGSEL`, \"\"), ' ', COALESCE(`personeel`.`ACHTERNAAM`, \"\")) AS `RESPONSIBLE`,
                    `stages`.`NAME` AS `STAGE`,
                    `countries`.`NAME` AS `COUNTRY`,
                    `segments`.`NAME` AS `SEGMENT`,
                    `activities`.`NAME` AS `ACTIVITY`,
                    `strategics`.`NAME` AS `STRATEGIC`,
                    `managements`.`NAME` AS `MANAGEMENT`
                FROM `entities`
                LEFT JOIN
                    `personeel` ON
                        `personeel`.`ID` = `entities`.`RESPONSIBLE_ID`
                LEFT JOIN
                    `stages` ON
                        `stages`.`ID` = `entities`.`STAGE_ID`
                LEFT JOIN
                    `countries` ON
                        `countries`.`ID` = `entities`.`COUNTRY_ID`
                LEFT JOIN
                    `segments` ON
                        `segments`.`ID` = `entities`.`SEGMENT_ID`
                LEFT JOIN
                    `activities` ON
                        `activities`.`ID` = `entities`.`ACTIVITY_ID`
                LEFT JOIN
                    `strategics` ON
                        `strategics`.`ID` = `entities`.`STRATEGIC_ID`
                LEFT JOIN
                    `managements` ON
                        `managements`.`ID` = `entities`.`MANAGEMENT_ID`
                WHERE `entities`.`ID` = :entity_id
                LIMIT 1;",
            'attachments' => 'SELECT *
                FROM `entity_attachments`
                WHERE `ENTITY_ID` = :entity_id;',
            'persons' => 'SELECT *
                FROM `entity_persons`
                WHERE `ENTITY_ID` = :entity_id;',
            'finances' => 'SELECT
                    `entity_finances`.*,
                    `financial_groups`.`NAME` AS `GROUP`
                FROM `entity_finances`
                INNER JOIN
                    `financial_groups` ON
                        `financial_groups`.`ID` = `entity_finances`.`FINANCIAL_GROUP_ID`
                WHERE `entity_finances`.`ENTITY_ID` = :entity_id
                ORDER BY `entity_finances`.`YEAR`;',
            'pipelines' => 'SELECT
                    `entity_pipeline`.*,
                    CONCAT(COALESCE(`personeel`.`VOORNAAM`, ""), " ", COALESCE(`personeel`.`TUSSENVOEGSEL`, ""), " ", COALESCE(`personeel`.`ACHTERNAAM`, "")) AS `USER`,
                    `pipeline_types`.`NAME` AS `TYPE`
                FROM `entity_pipeline`
                LEFT JOIN
                    `pipeline_types` ON
                        `pipeline_types`.`ID` = `entity_pipeline`.`PIPELINE_TYPE_ID`
                LEFT JOIN
                    `personeel` ON
                        `personeel`.`ID` = `entity_pipeline`.`USER_ID`
                WHERE `entity_pipeline`.`ENTITY_ID` = :entity_id
                AND `entity_pipeline`.`DELETED` = 0
                ORDER BY `entity_pipeline`.`CREATED_AT` DESC;',
            'tags' => 'SELECT
                    `tags`.*
                FROM `tags`
                INNER JOIN
                    `entity_tags` ON
                        `entity_tags`.`TAG_ID` = `tags`.`ID`
                INNER JOIN
                    `entities` ON
                        `entities`.`ID` = `entity_tags`.`ENTITY_ID`
                WHERE `entities`.`ID` = :entity_id
                AND `tags`.`DELETED` = 0
                ORDER BY `ORDER`',
            'accreditations' => 'SELECT
                    `accreditations`.*
                FROM `accreditations`
                INNER JOIN
                    `entity_accreditations` ON
                        `entity_accreditations`.`ACCREDITATION_ID` = `accreditations`.`ID`
                INNER JOIN
                    `entities` ON
                        `entities`.`ID` = `entity_accreditations`.`ENTITY_ID`
                WHERE `entities`.`ID` = :entity_id
                AND `accreditations`.`DELETED` = 0
                ORDER BY `ORDER`'
        ];
//        dpm($id);
//        dpm($queries, true);

        $available_vars = [];
        foreach ($queries as $var => $query) {
            $stmt = $pdo->prepare($query);
            $stmt->bindValue('entity_id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll($pdo::FETCH_ASSOC);
            foreach ($result as &$item) {
                $item = (object)array_change_key_case($item);
            }
            if ($var === 'entity' && is_array($result) && count($result) === 1 && array_key_exists(0, $result)) {
                $result = $result[0];
            }
            if ($var === 'entity') {
                $result = (array)$result;
                foreach ($result as $key => $value) {
                    $value = $this->addDecimal($key, $value);
                    $key = Helper::snake2camel($key);
                    $this->vars[$key] = $value;
                    $$key = $value;
                    $available_vars[] = $key;
                }
            } else {
                $var = Helper::snake2camel($var);
                $this->vars[$var] = $result;
                $$var = $result;
                $available_vars[] = $var;
            }
        }

        if (isset($finances) && $finances) {
            $financialRows = [];
            foreach ($finances as $finance) {
                if (!isset($financialRows['year']) || !in_array($finance->year, $financialRows['year'])) {
                    $financialRows['year'][] = $finance->year;
                }
                $financialRows[$finance->group][$finance->year] = $finance;
            }
        }

        $rating_potential_query = "SELECT RATE_POTENTIAL FROM entities WHERE ID = ".$_REQUEST['ID'];
        $stmt = $pdo->prepare($rating_potential_query);
        $stmt->execute();
        $rating = $stmt->fetchColumn();

        $stageClosed = ($stage && (strtolower($stage) == 'closed'));

        $available_vars = false;
        ob_start();
        include_once './templates/entity_view.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    /**
     *
     * GET edit form for editing a single file.
     */
    public function edit()
    {
        $id = Helper::reqVal('ID');
        if (Helper::reqVal('ajax')) {
            global $pdo;

            if ($key = Helper::reqVal('editkey')) {
                $type = 'text';
                $queryType = false;
                $label = str_replace('_', ' ', $key);
                $pieces = explode('-', $key);
                //return json_encode(['status' => 'success', 'result' => $pieces[0]]);
                switch ($pieces[0]) {
                    case 'entity_accreditations':
                    case 'entity_tags':
                        $queryType = 'many-to-many';
                        break;
                    case 'responsible_id':
                    case 'stage_id':
                    case 'segment_id':
                    case 'country_id':
                    case 'activity_id':
                    case 'strategic_id':
                    case 'management_id':
                        $queryType = 'one-to-many';
                        break;
                    case 'entity_finances':
                    case 'entity_persons':
                    case 'entity_attachments':
                    case 'entity_pipeline':
                        $queryType = 'many-to-one';
                        break;
                    case 'headcount':
                    case 'acquired':
                    case 'rate_potential':
//                    case 'rate_financial':
                    case 'rate_reception':
                    case 'growth':
                    case 'margin':
                    case 'synergies':
                    case 'year':
                        $type = 'number';
                        break;
//                    case 'synergies':
                    case 'revenue':
                    case 'ebitda':
                    case 'deal_size':
                    case 'valuation':
                    case 'ev_ebitda_multiple':
                    case 'upfront_price':
                    case 'earnout':
                        $type = 'float';
                        break;
                    case 'headline_date':
                        $type = 'date';
                        break;
                    case 'closed_date':
                    case 'created_at':
                        $type = 'datetime-local';
//                        $type = 'date';
                        break;
                    case 'description':
                    case 'headline_comment':
                        $type = 'textarea';
                        break;
                }
                if (count($pieces) >= 3 && $queryType === 'many-to-many') {
                    list($pivotTable, $foreignKey, $foreignTable) = $pieces;
                    $type = 'multi-select';
                    $label = str_replace('_', ' ', $foreignTable);
                    $query = 'SELECT `' . $foreignTable . '`.*, `ENTITY_ID` AS `selected`
                            FROM `' . $foreignTable . '`
                            LEFT JOIN `' . $pivotTable . '` ON `' . $pivotTable . '`.`' . $foreignKey . '` = `' . $foreignTable . '`.`ID` AND `' . $pivotTable . '`.`ENTITY_ID` = :entity_id
                            WHERE ' . $foreignTable . '.DELETED = 0
                            ORDER BY `ORDER`';

                    //return json_encode(['status' => 'success', 'result' => $query]);
                    $stmt = $pdo->prepare($query);
                    $stmt->bindValue('entity_id', $id);
                    $stmt->execute();
                    $result = $stmt->fetchAll($pdo::FETCH_OBJ);
                } elseif (count($pieces) >= 2 && $queryType === 'one-to-many') {
                    list($foreignColumn, $foreignTable) = $pieces;
                    $select = '`' . $foreignTable . '`.*, `entities`.`ID` AS selected';
                    if ($foreignTable == 'personeel') {
                        $select = '`' . $foreignTable . '`., `entities`.`ID` AS selected';
                        $select = "$foreignTable.ID, CONCAT(COALESCE(`$foreignTable`.`VOORNAAM`, \"\"), ' ', COALESCE(`$foreignTable`.`TUSSENVOEGSEL`, \"\"), ' ', COALESCE(`$foreignTable`.`ACHTERNAAM`, \"\")) AS `NAME`, `entities`.`ID` AS selected";
                    }
                    $type = 'select';
                    $label = str_replace('_', ' ', $foreignTable);
                    $query = 'SELECT ' . $select . ' FROM `' . $foreignTable . '`
                          LEFT JOIN `entities` ON `entities`.`' . strtoupper($foreignColumn) . '` = `' . $foreignTable . '`.`ID` AND `entities`.`ID` = :entity_id WHERE  `' . $foreignTable . '`.`DELETED` = 0';

                    $stmt = $pdo->prepare($query);
                    $stmt->bindValue('entity_id', $id);
                    $stmt->execute();
                    $result = $stmt->fetchAll($pdo::FETCH_OBJ);
                } elseif (count($pieces) >= 3 && $queryType === 'many-to-one') {
                    list($linkedTable, $id, $column) = $pieces;
                    $label = str_replace('_', ' ', $linkedTable);
                    $query = 'SELECT `' . strtoupper($column) . '` FROM `' . $linkedTable . '` WHERE `ID` = :id LIMIT 1;';


                    $stmt = $pdo->prepare($query);
                    $stmt->bindValue('id', $id);

                    $stmt->execute();
                    $result = $stmt->fetchColumn();
                    switch ($column) {
                        case 'content':
                            $type = 'textarea';
                    }

                    // shows all statusses available and the one previously selected.
                    if (isset($pieces) && $pieces[0] == "entity_pipeline" && $type == "text") {
                        $pipelineID = $pieces[3];
                        $query = "SELECT `pipeline_types`.*, `entity_pipeline`.`ID` as selected FROM pipeline_types
                                  LEFT JOIN `entity_pipeline` ON `entity_pipeline`.`PIPELINE_TYPE_ID` = `pipeline_types`.ID
                                  AND `entity_pipeline`.`ID` = " . $pipelineID . "
                                  WHERE `pipeline_types`.`DELETED` = 0";
                        $stmt = $pdo->prepare($query);
                        $stmt->execute();
                        $result = $stmt->fetchAll($pdo::FETCH_OBJ);
                        $type = "select";
                    }
                } elseif ($pieces[0] == "created_at") {
                    $values = explode('/', $pieces[1]);
                    if ($values[0] == "entity_pipeline") {
                        $key = strtoupper($pieces[0]);
                        $stmt = $pdo->prepare("SELECT `$key` FROM `entity_pipeline` WHERE `ID` = :entity_id LIMIT 1");
                        $stmt->bindValue('entity_id', $values[1]);
                        $stmt->execute();
                        $result = $stmt->fetchColumn();
                    }
                } elseif ($pieces[0] == "cf_advisor") {
                    $queryOne = "SELECT CF_ADVISOR FROM entities WHERE ID = " . $id;
                    $stmtOne = $pdo->prepare($queryOne);
                    $stmtOne->execute();
                    $advisorID = $stmtOne->fetchColumn(0);
                    try {
                        $advisorID = (int)$advisorID;
                    } catch (Exception $e) {
                        // do nothing
                    }
                    if ($advisorID == null || !is_int($advisorID)) {
                        $advisorID = 0;
                    }

                    $query = "SELECT entities.NAME, entity_persons.ID AS SPECIAL_ID, entity_persons.ID,
                                (SELECT entity_persons.ID as SPECIAL_ID FROM entity_persons WHERE entity_persons.ID = SPECIAL_ID AND entity_persons.ID = " . $advisorID . ") AS selected
                              FROM entity_persons
                              JOIN entities ON entities.ID = entity_persons.ENTITY_ID
                              WHERE entities.ADVISOR = 1
                              GROUP BY entities.NAME";

//                    return json_encode(['status' => 'success', 'result' => $query]);

                    $stmt = $pdo->prepare($query);
                    $stmt->execute();
                    $result = $stmt->fetchAll($pdo::FETCH_OBJ);
                    $type = "select";
//                    return json_encode(['status' => 'success', 'result' => $type]);
                    return json_encode([
                        'status' => 'success',
                        'result' => $result,
                        'type' => $type
                    ]);
                } else {
                    $stmt = $pdo->prepare("SELECT `$key` FROM `entities` WHERE `ID` = :entity_id LIMIT 1");
                    $stmt->bindValue('entity_id', $id);
                    $stmt->execute();
                    $result = $stmt->fetchColumn();
                }


                if ($type === 'datetime-local' && $result) {
                    $result = date("Y-m-d\TH:i", strtotime($result));
                } elseif ($type === 'date' && $result) {
                    $result = date("Y-m-d", strtotime($result));
                } elseif (($type == 'float' || $type == 'number' || $type == 'text') && $result != 'null' && $result != null) {
                  if($type == 'float') {
                      $result = number_format($result, 1, '.', ',');
                  }
                } elseif ((!$result || $result === 'null') && $result !== 0) {
                    $result = '';
                }

                return json_encode([
                    'status' => 'success',
                    'label' => $label,
                    'type' => $type,
                    'result' => $result
                ]);

//                return json_encode(['status' => 'failed']);
            }

            if ($action = Helper::reqVal('favourite')) {
                switch ($action) {
                    case 'false':
                        $query = "UPDATE entities SET FAVOURITE = 0 WHERE ID = :id";
                        break;
                    case 'true':
                        $query = "UPDATE entities SET FAVOURITE = 1 WHERE ID = :id";
                        break;
                    case 'toggle':
                    default:
                        $query = "UPDATE entities SET FAVOURITE = CASE WHEN FAVOURITE = 1 THEN 0 ELSE 1 END WHERE ID = :id";
                        break;
                }
                $stmt = $pdo->prepare($query);
                $stmt->bindValue('id', $id);
                $stmt->execute();
                $stmt = $pdo->prepare("SELECT FAVOURITE FROM entities WHERE ID = :id LIMIT 1");
                $stmt->bindValue('id', $id);
                $stmt->execute();
                $result = $stmt->fetchColumn();
                return $result ? 'true' : 'false';
            }

            if ($action = Helper::reqVal('advisor')) {
                switch ($action) {
                    case 'false':
                        $query = "UPDATE entities SET ADVISOR = 0 WHERE ID = :id";
                        break;
                    case 'true':
                        $query = "UPDATE entities SET ADVISOR = 1 WHERE ID = :id";
                        break;
                    case 'toggle':
                    default:
                        $query = "UPDATE entities SET ADVISOR = CASE WHEN ADVISOR = 1 THEN 0 ELSE 1 END WHERE ID = :id";
                        break;
                }
                $stmt = $pdo->prepare($query);
                $stmt->bindValue('id', $id);
                $stmt->execute();
                $stmt = $pdo->prepare("SELECT ADVISOR FROM entities WHERE ID = :id LIMIT 1");
                $stmt->bindValue('id', $id);
                $stmt->execute();
                $result = $stmt->fetchColumn();
                return $result ? 'true' : 'false';
            }

            if (Helper::reqVal('recover')) {
                $query = "UPDATE entities SET DELETED = 0 WHERE ID = :id";
                $stmt = $pdo->prepare($query);
                $stmt->bindValue('id', Helper::reqVal('ID'));
                $stmt->execute();
                return 'success';
            }
            return 'failed';
        }
    }

    /**
     * POST for adding new rows in the database
     */
    public function store()
    {
        global $pdo;

        if (!is_array($_POST) || !$this->ajax) {
            return json_encode(['error' => 'no data to save']);
        }

        $storeType = Helper::arrVal($_POST, 'store_type');

        $fillable = [
            'entities' => [
                'user_id' => $pdo::PARAM_INT,
                'company_id' => $pdo::PARAM_INT,
                'name' => false,
                'year' => false,
                'deferred' => false,
                'description' => false,
                'rate_potential' => $pdo::PARAM_INT,
                'rate_reception' => $pdo::PARAM_INT,
                'headline_action' => false,
                'headline_date' => false,
                'stage_id' => $pdo::PARAM_INT,
                'headline_comment' => false,
                'address' => false,
                'country_id' => $pdo::PARAM_INT,
                'website' => false,
                'shareholders' => false,
                'accreditation' => false,
                'activity_id' => $pdo::PARAM_INT,
                'segment_id' => $pdo::PARAM_INT,
//                'tags' => false, // This is not a direct value, but a many-to-many relation
                'strategic_id' => $pdo::PARAM_INT,
                'synergies' => $pdo::PARAM_INT,
                'management_id' => $pdo::PARAM_INT,
                'headcount' => $pdo::PARAM_INT,
                'revenue' => false,
                'growth' => $pdo::PARAM_INT,
                'margin' => $pdo::PARAM_INT,
                'ebitda' => false,
                'deal_size' => false,
                'valuation' => false,
                'acquired' => $pdo::PARAM_INT,
                'ev_ebitda_multiple' => false,
                'upfront_price' => false,
                'earnout' => false,
                'cf_advisor' => false,
                'favourite' => $pdo::PARAM_BOOL,
                'responsible_id' => $pdo::PARAM_INT
            ],
            'entity_pipeline' => [
                'user_id' => $pdo::PARAM_INT,
                'entity_id' => $pdo::PARAM_INT,
                'title' => false,
                'content' => false,
                'pipeline_type_id' => $pdo::PARAM_INT,
                'created_at' => false
            ],
            'entity_persons' => [
                'name' => $pdo::PARAM_INT,
                'entity_id' => $pdo::PARAM_INT,
                'function' => false,
                'phone' => false,
                'email' => false
            ],
            'entity_attachments' => [
                'name' => false,
                'entity_id' => $pdo::PARAM_INT,
                'date' => false,
                'documentbeheer_id' => $pdo::PARAM_INT,
            ]
        ];

        if (!isset($fillable[$storeType])) {
            return json_encode(['error' => 'no location found']);
        }

        $_POST['user_id'] = getUserID();
        $_POST['responsible_id'] = getUserID();

        // store file
        if ($storeType == 'entity_attachments') {
            if (!$file = Helper::arrVal($_FILES, 'attachment')) {
                return false;
            }
            $_POST['documentbeheer_id'] = $this->saveAttachment($file);
        }


        $query = "INSERT INTO `$storeType` (`";
        $keyValues = [];
        $c = 0;
        foreach (array_keys($fillable[$storeType]) as $key) {
            if (!$value = Helper::arrVal($_POST, $key, null, true)) {
                continue;
            }
            $keyValues[$key] = $value;
            if ($c) {
                $query .= '`, `';
            }
            $query .= strtoupper($key);
            $c++;
        }
        $query .= "`) VALUES (:";
        $c = 0;
        foreach (array_keys($keyValues) as $key) {
            if ($c) {
                $query .= ', :';
            }
            $query .= $key;
            $c++;
        }
        $query .= ");";

        $stmt = $pdo->prepare($query);

        foreach ($keyValues as $key => $value) {
//        foreach ($fillable[$storeType] as $key => $type) {
            $type = $fillable[$storeType][$key];
//            if (!$value = Helper::arrVal($_POST, $key, null, true)) {
//                continue;
//            }
            $postVar = htmlentities($value, ENT_QUOTES | ENT_IGNORE, "UTF-8");
            $stmt->bindValue($key, $postVar, $type);
        }

        $result = $stmt->execute();

        // save tags for entity
        $entity_id = $pdo->lastInsertId();
        $tags = Helper::postVal('tags');
        $accreditations = Helper::postVal('accreditations');
        if (!empty($accreditations)) {
            foreach ($accreditations as $id) {
                $query = "INSERT INTO entity_accreditations (ENTITY_ID, ACCREDITATION_ID) VALUES (" . $entity_id . ", " . $id . ");";
                $stmt = $pdo->prepare($query);
                $stmt->execute();
            }
        }

        if (!empty($tags)) {
            foreach ($tags as $id) {
                $query = "INSERT INTO entity_tags (ENTITY_ID, TAG_ID) VALUES (" . $entity_id . ", " . $id . ");";
                $stmt = $pdo->prepare($query);
                $stmt->execute();
            }
        }
        if(isset($_POST['documentbeheer_id'])) {
            return header('Location: content.php?SITE=entity_view&ID='.$keyValues['entity_id']) ;
        }

        return json_encode(['status' => 'success', 'id' => $entity_id]);
    }

    /**
     *
     * PATCH updating existing rows in the database.
     */
    public function update()
    {

        global $pdo;

        if (!is_array($_POST) || !$this->ajax) {
            return json_encode(['status' => 'failed', 'error' => 'no data to save']);
        }

        if (!$id = Helper::reqVal('ID')) {
            echo "No ID found";
            die();
        }


        // none and one-to-many relationships
        $fillable = [
            'responsible_id' => $pdo::PARAM_INT,
            'stage_id' => $pdo::PARAM_INT,
            'country_id' => $pdo::PARAM_INT,
            'segment_id' => $pdo::PARAM_INT,
            'activity_id' => $pdo::PARAM_INT, // New
            'strategic_id' => $pdo::PARAM_INT, // New
            'management_id' => $pdo::PARAM_INT, // New
            'name' => false,
            'year' => $pdo::PARAM_INT,
            'deferred' => false,
            'headcount' => $pdo::PARAM_INT,
            'headline_action' => false,
            'headline_date' => false,
            'headline_comment' => false,
            'shareholders' => false,
            'address' => false,
            'deal_size' => false,
            'valuation' => false,
            'accreditation' => false,
            'acquired' => $pdo::PARAM_INT,
            'ev_ebitda_multiple' => false,
            'upfront_price' => false,
            'earnout' => false,
            'cf_advisor' => false,
//            'owner' => false,
            'description' => false,
            'rate_potential' => $pdo::PARAM_INT,
//            'rate_financial' => $pdo::PARAM_INT,
            'rate_reception' => $pdo::PARAM_INT,
//            'rate_comment' => false,
            'revenue' => false,
            'ebitda' => false,
            'website' => false,
            'synergies' => $pdo::PARAM_INT,
            'currency' => false,
            'growth' => $pdo::PARAM_INT,
            'margin' => $pdo::PARAM_INT,
            'closed_date' => false,
            'created_at' => false
        ];

        // many-to-many relationships
        $pivotTables = [
            'entity_tags',
            'entity_accreditations'
        ];

        // many-to-one relationships
        $linkedTables = [
            'entity_finances',
            'entity_attachments',
            'entity_persons',
            'entity_pipeline'
        ];

        $filtered = $pivotFiltered = $linkedFiltered = [];

        // Add new option to table and select that new option
        if ((false !== $key = array_search('add-option', $_POST))
            && (false !== $value = Helper::postVal('new-option'))) {
            $newValue = $this->addOption($key, $value, $fillable, $pivotTables, $linkedTables);
            if ($newValue) {
                $_POST[$key] = $newValue;
            }
        } else {
            foreach ($_POST as $postKey => $postValue) {
                if (is_array($postValue)) {
                    if ((false !== $key = array_search('add-option', $postValue))
                        && (false !== $value = Helper::postVal('new-option'))) {
                        unset($_POST[$postKey][$key]);
                        $newValue = $this->addOption($postKey, $value, $fillable, $pivotTables, $linkedTables);
                        if ($newValue) {
                            if (is_array($newValue)) {
                                $newValue = array_unique(array_merge($_POST[$postKey], $newValue));
                            }
                            $_POST[$postKey] = $newValue;
                        }
                    }
                }
            }
        }
        foreach ($_POST as $key => $value) {
            $keyPieces = explode('-', $key);

            // specially made if users wanna edit the created_at date of an interaction (communication)
            if ($keyPieces[0] == "created_at") {
                $values = explode('/', $keyPieces[1]);
                if ($values[0] == "entity_pipeline") {
                    $id = $values[1];
                    $query = "UPDATE `entity_pipeline` SET `CREATED_AT` =  '" . $value . "' WHERE `ID` = " . $id . " AND `DELETED` = 0";
                    $stmt = $pdo->prepare($query);
                    if (!$stmt->execute()) {
                        return json_encode(['status' => 'failed! ' . print_r($stmt->errorInfo(), true)]);
                    }
                    return json_encode(['status' => 'success']);
                }
            } else {
                if ($keyPieces[0] == "entity_pipeline" && $keyPieces[1] == "pipeline_type_id") {
                    $id = $keyPieces[3];
                    $query = "UPDATE `entity_pipeline` SET `PIPELINE_TYPE_ID` =  " . $value . " WHERE `ID` = " . $id . " AND `DELETED` = 0";
                    $stmt = $pdo->prepare($query);
                    if (!$stmt->execute()) {
                        return json_encode(['status' => 'failed! ' . print_r($stmt->errorInfo(), true)]);
                    }
                    return json_encode(['status' => 'success']);
                }
            }

//            if (count($pieces) >= 1 && isset($fillable[$pieces[0]])) {
            if (isset($fillable[$keyPieces[0]])) {
                $filtered[$key] = [
                    'key' => $keyPieces[0],
                    'value' => $value
                ];
            } elseif (count($keyPieces) >= 2 && in_array($keyPieces[0], $pivotTables)) {
                list($pivotTable, $column) = $keyPieces;
                if (!is_array($value)) {
                    $value = [$value];
                }

                $pivotFiltered[$pivotTable]['set'] = [strtoupper($column), 'ENTITY_ID'];
                foreach ($value as $index => $val) {
                    if (!$val) {
                        continue; // catch the 0
                    }
                    $pivotFiltered[$pivotTable]['actions'][] = [
                        ':' . $column . '_' . $index => $val,
                        ':entity_id' => false
                    ];
                }

            } elseif (count($keyPieces) >= 3 && in_array($keyPieces[0], $linkedTables)) {
                list($linkedTable, $id, $column) = $keyPieces;
                $linkedFiltered[$linkedTable][$id][$column] = $value;
            }
        }

        if ($filtered) {
            $query = "UPDATE `entities` SET ";

            foreach ($filtered as $key => $type) {
                if (strtoupper($type['key']) == "STAGE_ID") {
                    $query .= "`entities`.`CLOSED_DATE` = '" . date('Y-m-d') . "', ";
                }
                $query .= "`entities`.`" . strtoupper($type['key']) . "` = :" . $type['key'] . " ";
            }
            $query .= "WHERE `entities`.`ID` = :entity_id";

            $stmt = $pdo->prepare($query);

            $stmt->bindValue('entity_id', $id, $pdo::PARAM_INT);

            foreach ($filtered as $item) {
                $type = Helper::arrVal($fillable, $item['key'], false);
                $postVar = htmlentities($item['value'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
//                if (str_ends_with($item['key'], '_id') && !$postVar) {
//                return json_encode(['status' => $item['key']]);
                if ($item['key'] == "ebitda"
                    || $item['key'] == "revenue"
                    || $item['key'] == "valuation"
                    || $item['key'] == "upfront_price"
                    || $item['key'] == "ev_ebitda_multiple"
                    || $item['key'] == "growth"
                    || $item['key'] == "margin"
                    || $item['key'] == "acquired" // == stake
                    || $item['key'] == "deferred"
                    || $item['key'] == "synergies"
                ) {
                    if (!$postVar) {
                        if ($postVar != "0") {
                            $postVar = null;
                        }
                    }
                    $postVar = number_format($postVar, 1);
                } elseif (!$postVar) { // geen lege string sopslaan (mag soms ook niet (bijv: dates mogen geen lege string hebben, maar wel null zijn)
                    $postVar = null;
                }
                $stmt->bindValue($item['key'], $postVar, $type);
            }
            if (!$stmt->execute()) {
                return json_encode(['status' => 'failed! ' . print_r($stmt->errorInfo(), true)]);
            }

        }

        if ($pivotFiltered) {
            foreach ($pivotFiltered as $pivotTable => $args) {
                // remove all current pivot rows
                $query = "DELETE FROM `" . $pivotTable . "` WHERE `ENTITY_ID` = :entity_id;";
                $stmt = $pdo->prepare($query);
                $stmt->bindValue('entity_id', $id, $pdo::PARAM_INT);
                $stmt->execute();

                if (!isset($args['actions'])) { // don't add new rows clear all results
                    continue;
                }

                // create statement with new
                $query = "INSERT INTO `" . $pivotTable . "` (" . implode(', ', $args['set']) . ") VALUES ";
                foreach ($args['actions'] as $index => $action) {
                    if ($index) {
                        $query .= ", ";
                    }
                    $query .= "(" . implode(', ', array_keys($action)) . ")";
                }
                $stmt = $pdo->prepare($query);
                $stmt->bindValue('entity_id', $id, $pdo::PARAM_INT);
                foreach ($args['actions'] as $action) {
                    foreach ($action as $column => $value) {
                        if (!$value) {
                            continue;
                        }
                        $stmt->bindValue($column, $value, $pdo::PARAM_INT);
                    }
                }
                if (!$stmt->execute()) {
                    return json_encode(['status' => 'failed']);
                }
            }
        }

        if ($linkedFiltered) {
            foreach ($linkedFiltered as $linkedTable => $row) {
                foreach ($row as $id => $columns) {
                    $query = "UPDATE `$linkedTable` SET ";

                    foreach ($columns as $column => $value) {
                        $query .= "`$linkedTable`.`" . strtoupper($column) . "` = :" . $column . " ";
                    }
                    $query .= "WHERE `$linkedTable`.`ID` = :id";
                    $stmt = $pdo->prepare($query);

                    $stmt->bindValue('id', $id, $pdo::PARAM_INT);
                    foreach ($columns as $column => $value) {
                        $value = htmlentities($value, ENT_QUOTES | ENT_IGNORE, "UTF-8");
                        $stmt->bindValue($column, $value);
                    }
                    if (!$stmt->execute()) {
                        return json_encode(['status' => 'failed']);
                    }
                }
            }
        }

        return json_encode(['status' => 'success']);
    }

    private function addOption($key, $value, $fillable, $pivotTables, $linkedTables)
    {
        global $pdo;
        $keyPieces = explode('-', $key);
        $newValue = false;

        if (isset($fillable[$keyPieces[0]]) && $table = Helper::arrVal($keyPieces, 1)) {
            $query = "SELECT ID FROM `$table` WHERE `NAME` = '$value' LIMIT 1";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if (!$newValue = $stmt->fetchColumn()) {
                $query = "INSERT INTO `$table` (`NAME`) VALUES ('$value')";
                $stmt = $pdo->prepare($query);
                $stmt->execute();
                $newValue = $pdo->lastInsertId();
            }
        } elseif (in_array($keyPieces[0], $pivotTables) && $table = Helper::arrVal($keyPieces, 2)) {
            $values = explode(',', $value);
            $newValue = [];
            foreach ($values as &$val) {
                $val = trim($val);
                $query = "SELECT ID FROM `$table` WHERE `NAME` = '$val' LIMIT 1";
                $stmt = $pdo->prepare($query);
                $stmt->execute();
                $result = $stmt->fetchColumn();
                if (!$result) {
                    $query = "INSERT INTO `$table` (`NAME`) VALUES ('$val')";
                    $stmt = $pdo->prepare($query);
                    $stmt->execute();
                    $newValue[] = $pdo->lastInsertId();
                } else {
                    $newValue[] = $result;
                }
            }

        } elseif (count($keyPieces) >= 3 && in_array($keyPieces[0], $linkedTables)) {
            // @todo ?
        }

        return $newValue;
    }

    public function delete()
    {
        global $pdo;

        if (!$this->ajax) {
            return '404.';
        }

        $whitelist = [
            'entity_actions',
            'actions',
            'action_types',
            'action_subjects',
            'entity_pipeline',
            'pipeline_types',
            'entity_tags',
            'tags',
            'entity_finances',
            'financial_groups',
            'entity_persons',
            'entity_attachments',
            'entities',
            'segments',
            'countries',
            'stages'
        ];
        $softDelete = [
            'entities',
            'countries',
            'entity_pipeline'
        ];

        if ($this->ajax) {
            $table = Helper::postVal('table');
            $id = Helper::postVal('id');
            $column = Helper::postVal('column');
        } else {
            $table = Helper::reqVal('table');
            $id = Helper::reqVal('id');
            $column = Helper::reqVal('column');
        }
        if ((!$id && !$table) || !in_array($table, $whitelist)) {
            return json_encode(['status' => 'failed']);
        }

        if ($column) {
            $query = "UPDATE `" . (string)$table . "` SET `" . (string)$column . "` = NULL WHERE ID = " . (int)$id;
        } elseif (in_array($table, $softDelete)) {
            $query = "UPDATE `" . (string)$table . "` SET `DELETED` = true WHERE ID = " . (int)$id;
        } else {
            $query = "DELETE FROM `" . (string)$table . "` WHERE ID = " . (int)$id;
        }
        $stmt = $pdo->prepare($query);

        if (!$stmt->execute()) {
            return json_encode(['status' => 'failed']);
        }
        return json_encode(['status' => 'success']);
    }

    public function visuals()
    {
        global $pdo;

        $available_vars = false;

        $query = "SELECT * FROM visuals";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll($pdo::FETCH_ASSOC);

        $baseURL = $this->links['base'];

        ob_start();
        include_once './templates/entity_visuals.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    public function editVisuals()
    {
        global $pdo;

        $available_vars = false;

        $where = "";

        if (isset($_REQUEST['VISUAL'])) {
            $visual = $_REQUEST['VISUAL'];
            if ($visual == "start") {
                $where = "WHERE `KEY` IN('visual_1', 'visual_2')";
            } else {
                if ($visual == "metrics") {
                    $where = "WHERE `KEY` IN('visual_3', 'visual_9', 'visual_10', 'visual_5', 'visual_6', 'visual_7', 'visual_8')";
                } else {
                    if ($visual == "pipeline") {
                        $where = "WHERE `KEY` IN('visual_4')";
                    }
                }
            }
        }

        $query = "SELECT * FROM visuals " . $where . " ORDER BY `KEY`, `COMPONENT`";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll($pdo::FETCH_ASSOC);

        $query = "SELECT * FROM countries WHERE DELETED = 0";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $countries = $stmt->fetchAll($pdo::FETCH_ASSOC);

        ob_start();
        include_once './templates/entity_visuals_edit.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    public function updateVisuals()
    {
        global $pdo;

        if (!$this->ajax || !isset($_POST['table_checker'])) {
            return json_encode(['status' => 'failed', 'error' => 'No data found']);
        }

        $currentTable = $_POST['table_checker'];

        $error = [];
        foreach ($_POST as $key => $element) {

            if ($element == $currentTable) {
                continue;
            }
            $explode = explode("_", $key);
            if ($explode[0] == "delete") {
                $query = "UPDATE $currentTable SET `DELETED` = 1 WHERE `ID` = $explode[1]";
            } else {
                $query = "UPDATE $currentTable SET `VALUE` = '$element', `DELETED` = 0 WHERE `ID` = $explode[1]";
            }

            $stmt = $pdo->prepare($query);
            if (!$stmt->execute()) {
                $error[] = $query;
                $error[] = $pdo->errorInfo();
            }
        }

        if ($error) {
            return json_encode(['status' => 'failed', 'error' => print_r($error, true)]);
        }

        $lastPage = $_SERVER['HTTP_REFERER'];
        $visual = preg_replace('/.*?VISUAL=(.*)?&.*/', '$1', $lastPage);

        if ($visual == "start") {
            header('Location: ' . $this->links['base'] . '_home&MENUID=1');
        } elseif ($visual == "pipeline") {
            header('Location: ' . $this->links['base'] . '_home&VISUAL=pipeline&MENUID=10');
        } elseif ($visual == "metrics") {
            header('Location: ' . $this->links['base'] . '_home&VISUAL=metrics&MENUID=11');
        } else {
            header('Location: ' . $this->links['base'] . '_home&MENUID=1');
        }
        exit;
    }

    public function advisors()
    {
        global $pdo;

        $query = "SELECT IF(
        LOCATE(' ', entity_persons.NAME) > 0,
        SUBSTRING(entity_persons.NAME, 1, LOCATE(' ', entity_persons.NAME) - 1),
        entity_persons.NAME
    ) AS PERSON_FIRST,
    IF(
        LOCATE(' ', entity_persons.NAME) > 0,
        SUBSTRING(entity_persons.NAME, LOCATE(' ', entity_persons.NAME) + 1),
        NULL
    ) AS PERSON_LAST, entity_persons.ID as PERSONS_ID, entities.NAME as COMPANY, entities.ID as COMPANY_ID, entity_persons.FUNCTION, entities.WEBSITE, countries.ISO as COUNTRY, entities.ADDRESS as LOCATION, entity_persons.EMAIL, entity_persons.PHONE , entities.ADVISOR
    FROM entity_persons
JOIN entities ON entity_persons.ENTITY_ID = entities.ID
JOIN countries ON entities.COUNTRY_ID = countries.ID
ORDER BY entities.NAME ASC";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll($pdo::FETCH_ASSOC);

        include_once './templates/advisors_overview.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    public function viewAdvisor()
    {
        global $pdo;

        $this->links['base'] = "/content.php?SITE=entity_advisors&MENUID=5";
        $deleted = false;
        $recover = false;

        $stmt = $pdo->prepare("SELECT ID, NAME FROM pipeline_types WHERE `DELETED` = 0");
        $stmt->execute();
        $pipeline_types = $stmt->fetchAll($pdo::FETCH_KEY_PAIR);

        $id = Helper::reqVal('ID');
        if (!$id) {
            header("Location: /content.php?SITE=entity");
        }

        $query = "SELECT IF(
        LOCATE(' ', entity_persons.NAME) > 0,
        SUBSTRING(entity_persons.NAME, 1, LOCATE(' ', entity_persons.NAME) - 1),
        entity_persons.NAME
    ) AS PERSON_FIRST,
    IF(
        LOCATE(' ', entity_persons.NAME) > 0,
        SUBSTRING(entity_persons.NAME, LOCATE(' ', entity_persons.NAME) + 1),
        NULL
    ) AS PERSON_LAST, entity_persons.ID as PERSONS_ID, entities.NAME as COMPANY, entities.ID as COMPANY_ID, entity_persons.FUNCTION, entities.WEBSITE, countries.ISO as COUNTRY, countries.NAME as COUNTRY_FULL, entities.ADDRESS as LOCATION, entity_persons.EMAIL, entity_persons.PHONE, entity_persons.COMMENT
      FROM entity_persons
JOIN entities ON entity_persons.ENTITY_ID = entities.ID
JOIN countries ON entities.COUNTRY_ID = countries.ID
WHERE entity_persons.ID = :entity_id
ORDER BY ISNULL(PERSON_LAST), PERSON_LAST ASC, ISNULL(PERSON_FIRST), PERSON_FIRST ASC";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue('entity_id', $id);
        $stmt->execute();
        $advisor = $stmt->fetchAll($pdo::FETCH_ASSOC);
        $advisor = $advisor[0];

        ob_start();
        include_once './templates/advisor_details.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    public function favorites()
    {
        $this->structure = "entity_favorites";
        $submenuItems = "";
//        $submenuItems = "<a class=\"cursor\" href='" . $this->links['base'] . "_change_structure'>Table Format</a>";
//        $submenuItems .= "<a class=\"cursor\" href='" . $this->links['base'] . "_change_option'>CRM Structure</a>";

        return $this->viewMyFavorites($this->links, $submenuItems);
    }


    public function myPipeline()
    {
        $this->structure = "entity_pipeline";
        $submenuItems = "<a class=\"cursor\" href='" . $this->links['base'] . "_change_structure'>Table Format</a>";
        $submenuItems .= "<a class=\"cursor\" href='" . $this->links['base'] . "_change_option'>CRM Structure</a>";

        return $this->viewMyPipeline($this->links, $submenuItems);
    }

    public function saveAttachment($file)
    {
        if (!$file) {
            return false;
        }

        $fName = Helper::arrVal($file, 'name');
        $fType = Helper::arrVal($file, 'type');
        $fSize = Helper::arrVal($file, 'size');
        $fContent = file_get_contents(Helper::arrVal($file, 'tmp_name'));

        $fileManagement = new FileManagement($this->user, $this->company);

        $result = $fileManagement->insertCloudvps($fName, $fType, $fSize, $fContent);
        if (!is_int($result)) {
            dpm($result);
            return false;
        }
        return $result;
    }

    public function getAttachment()
    {
        global $pdo;

        if (!$id = (int)Helper::getVal('ID')) {
            return false;
        }
        $query = "SELECT a.* FROM documentbeheer as a JOIN entity_attachments as b ON a.ID = b.DOCUMENTBEHEER_ID WHERE b.ID = $id LIMIT 1";

        $stmt = $pdo->prepare($query);
        $stmt->execute();

        $result = $stmt->fetchAll($pdo::FETCH_OBJ);
        $result = $result[0];
        $CloudVPS = new CloudVPS();
        $file = $CloudVPS->getFile($result->CLOUDVPS);
        $content = $file->stream;
        $size = $file->size;
        $type = $file->type;

        $name = $result->BESTANDSNAAM;
        $name = str_replace(" ", "_", $name);

        if ($type == "pdf") {
            $type = "application/pdf";
        }

        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        session_cache_limiter("must-revalidate");
        header("Content-length: $size");
        header("Content-Type: $type");
        header("Content-Disposition: attachment; filename=\"$name\"");

        echo $content;
        exit;

    }

    public function getFileCloudVPS()
    {
        global $pdo;

        if (!$id = (int)Helper::getVal('ID')) {
            return false;
        }
        $query = "SELECT BESTANDSNAAM, CLOUDVPS FROM documentbeheer WHERE ID = $id LIMIT 1";

        $stmt = $pdo->prepare($query);
        $stmt->execute();

        $result = $stmt->fetchAll($pdo::FETCH_OBJ);
        $result = $result[0];
        $CloudVPS = new CloudVPS();
        $file = $CloudVPS->getFile($result->CLOUDVPS);
        $content = $file->stream;
        $size = $file->size;
        $type = $file->type;

        $name = $result->BESTANDSNAAM;
        $name = str_replace(" ", "_", $name);

        if ($type == "pdf") {
            $type = "application/pdf";
        }

        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        session_cache_limiter("must-revalidate");
        header("Content-length: $size");
        header("Content-Type: $type");
        header("Content-Disposition: attachment; filename=\"$name\"");

        echo $content;
        exit;

    }

    public function integrationChecklist()
    {
        global $pdo;

        getRechten(getUserID(), "INTEGRATIONCHECKLIST");
        $baseURL = "content.php?SITE=entity_integration_checklist";
        if (!isset($_REQUEST['COMPANY_ID'])) {
            $extraWhere = "";
            if(isset($_REQUEST['STARTDATE'])) {
                $year_from = explode('-', $_REQUEST['STARTDATE'])[0];
                $month_from = explode('-', $_REQUEST['STARTDATE'])[1];
                $extraWhere .= " AND (year(entity_pipeline.CREATED_AT) >= ".$year_from." AND month(entity_pipeline.CREATED_AT) >= ".$month_from.")";
            }
            if(isset($_REQUEST['ENDDATE'])) {
                $year_to = explode('-', $_REQUEST['ENDDATE'])[0];
                $month_to = explode('-', $_REQUEST['ENDDATE'])[1];
                $extraWhere .= " AND (year(entity_pipeline.CREATED_AT) <= ".$year_to." AND month(entity_pipeline.CREATED_AT) <= ".$month_to.")";
            }
            $query = "SELECT entities.ID, entities.NAME, entity_pipeline.CREATED_AT FROM entities
                    JOIN entity_pipeline ON entity_pipeline.ENTITY_ID = entities.ID
                    JOIN pipeline_types ON pipeline_types.ID = entity_pipeline.PIPELINE_TYPE_ID
                    LEFT JOIN integration_checklist_filter ON integration_checklist_filter.ENTITY_ID = entities.ID
                      AND integration_checklist_filter.USER_ID = ".getLoginID()."
                    WHERE entity_pipeline.PIPELINE_TYPE_ID = 10
                    ".$extraWhere."
                    AND pipeline_types.DELETED = 0
                    AND entities.DELETED = 0
                    AND (integration_checklist_filter.HIDE_FROM_CHECKLIST = 0
                    	OR integration_checklist_filter.HIDE_FROM_CHECKLIST IS NULL)
                    GROUP BY entities.NAME
                    ORDER BY entity_pipeline.CREATED_AT DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $entities = $stmt->fetchAll($pdo::FETCH_ASSOC);

            $query = "SELECT entities.ID, entities.NAME, integration_checklist_filter.HIDE_FROM_CHECKLIST FROM entities
						  JOIN entity_pipeline ON entity_pipeline.ENTITY_ID = entities.ID
                    JOIN pipeline_types ON pipeline_types.ID = entity_pipeline.PIPELINE_TYPE_ID
                    LEFT JOIN integration_checklist_filter ON integration_checklist_filter.ENTITY_ID = entities.ID AND integration_checklist_filter.USER_ID = ".getLoginID()."
                    WHERE entity_pipeline.PIPELINE_TYPE_ID = 10
                    AND pipeline_types.DELETED = 0
                    AND entities.DELETED = 0
                    GROUP BY entities.NAME
                    ORDER BY entity_pipeline.CREATED_AT DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $entitiesFilterList = $stmt->fetchAll($pdo::FETCH_ASSOC);

            $query = "SELECT integration_checklist_sub_categories.ID, integration_checklist.CATEGORY_ID, integration_checklist.SUB_CATEGORY_ID, integration_checklist.COMPLETED_ID
                        FROM integration_checklist
                        LEFT JOIN integration_checklist_sub_categories ON integration_checklist_sub_categories.ID = integration_checklist.SUB_CATEGORY_ID
                        JOIN integration_checklist_categories ON integration_checklist_categories.ID = integration_checklist.CATEGORY_ID
                        WHERE integration_checklist_sub_categories.DELETED = 0
                        GROUP BY integration_checklist_sub_categories.ID
                        ORDER BY integration_checklist_categories.ORDER, integration_checklist.TEMPLATE, integration_checklist_sub_categories.ORDER";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $checklist = $stmt->fetchAll($pdo::FETCH_ASSOC);
        } else {
            $this->checklistCheckRows($_REQUEST['COMPANY_ID']);
            $query = "SELECT integration_checklist.*
                        FROM integration_checklist
                        JOIN integration_checklist_sub_categories ON integration_checklist_sub_categories.ID = integration_checklist.SUB_CATEGORY_ID
                        JOIN integration_checklist_categories ON integration_checklist_categories.ID = integration_checklist.CATEGORY_ID
                        WHERE integration_checklist.ENTITY_ID = ".$_REQUEST['COMPANY_ID']."
                        AND integration_checklist_sub_categories.DELETED = 0
                        ORDER BY integration_checklist_categories.ORDER, integration_checklist.TEMPLATE, integration_checklist_sub_categories.ORDER";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $checklistRows = $stmt->fetchAll($pdo::FETCH_ASSOC);
        }

        ob_start();
        include_once './templates/integration_checklist_home.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    public function integrationChecklist2()
    {
        global $pdo;
        getRechten(getUserID(), "INTEGRATIONCHECKLIST");
        $baseURL = "content.php?SITE=entity_integration_checklist2";
        if (!isset($_REQUEST['COMPANY_ID'])) {
            $extraWhere = "";
            if(isset($_REQUEST['STARTDATE'])) {
                $year_from = explode('-', $_REQUEST['STARTDATE'])[0];
                $month_from = explode('-', $_REQUEST['STARTDATE'])[1];
                $extraWhere .= " AND (year(entity_pipeline.CREATED_AT) >= ".$year_from." AND month(entity_pipeline.CREATED_AT) >= ".$month_from.")";
            }
            if(isset($_REQUEST['ENDDATE'])) {
                $year_to = explode('-', $_REQUEST['ENDDATE'])[0];
                $month_to = explode('-', $_REQUEST['ENDDATE'])[1];
                $extraWhere .= " AND (year(entity_pipeline.CREATED_AT) <= ".$year_to." AND month(entity_pipeline.CREATED_AT) <= ".$month_to.")";
            }
            $query = "SELECT entities.ID, entities.NAME, entity_pipeline.CREATED_AT FROM entities
                    JOIN entity_pipeline ON entity_pipeline.ENTITY_ID = entities.ID
                    JOIN pipeline_types ON pipeline_types.ID = entity_pipeline.PIPELINE_TYPE_ID
                    LEFT JOIN integration_checklist_filter ON integration_checklist_filter.ENTITY_ID = entities.ID
                      AND integration_checklist_filter.USER_ID = ".getLoginID()."
                    WHERE entity_pipeline.PIPELINE_TYPE_ID = 10
                    ".$extraWhere."
                    AND pipeline_types.DELETED = 0
                    AND entities.DELETED = 0
                    AND (integration_checklist_filter.HIDE_FROM_CHECKLIST = 0
                    	OR integration_checklist_filter.HIDE_FROM_CHECKLIST IS NULL)
                    GROUP BY entities.NAME
                    ORDER BY entity_pipeline.CREATED_AT DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $entities = $stmt->fetchAll($pdo::FETCH_ASSOC);

            $query = "SELECT entities.ID, entities.NAME, integration_checklist_filter.HIDE_FROM_CHECKLIST FROM entities
						  JOIN entity_pipeline ON entity_pipeline.ENTITY_ID = entities.ID
                    JOIN pipeline_types ON pipeline_types.ID = entity_pipeline.PIPELINE_TYPE_ID
                    LEFT JOIN integration_checklist_filter ON integration_checklist_filter.ENTITY_ID = entities.ID AND integration_checklist_filter.USER_ID = ".getLoginID()."
                    WHERE entity_pipeline.PIPELINE_TYPE_ID = 10
                    AND pipeline_types.DELETED = 0
                    AND entities.DELETED = 0
                    GROUP BY entities.NAME
                    ORDER BY entity_pipeline.CREATED_AT DESC";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $entitiesFilterList = $stmt->fetchAll($pdo::FETCH_ASSOC);

            $query = "SELECT integration_checklist_sub_categories.ID, integration_checklist.CATEGORY_ID, integration_checklist.SUB_CATEGORY_ID, integration_checklist.COMPLETED_ID, integration_checklist.DURATION_ID, integration_checklist.INITIATION_ID, integration_checklist.ENTITY_ID, integration_checklist.ID as CHECKLIST_ID
                        FROM integration_checklist
                        LEFT JOIN integration_checklist_sub_categories ON integration_checklist_sub_categories.ID = integration_checklist.SUB_CATEGORY_ID
                        JOIN integration_checklist_categories ON integration_checklist_categories.ID = integration_checklist.CATEGORY_ID
                        WHERE integration_checklist_sub_categories.DELETED = 0
                          AND integration_checklist.ENTITY_ID != 0
                        GROUP BY integration_checklist_sub_categories.ID
                        ORDER BY integration_checklist_categories.ORDER, integration_checklist.TEMPLATE, integration_checklist_sub_categories.ORDER";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $checklist = $stmt->fetchAll($pdo::FETCH_ASSOC);
        } else {
            $this->checklistCheckRows($_REQUEST['COMPANY_ID']);
            $query = "SELECT integration_checklist.*
                        FROM integration_checklist
                        JOIN integration_checklist_sub_categories ON integration_checklist_sub_categories.ID = integration_checklist.SUB_CATEGORY_ID
                        JOIN integration_checklist_categories ON integration_checklist_categories.ID = integration_checklist.CATEGORY_ID
                        WHERE integration_checklist.ENTITY_ID = ".$_REQUEST['COMPANY_ID']."
                        AND integration_checklist_sub_categories.DELETED = 0
                        ORDER BY integration_checklist_categories.ORDER, integration_checklist.TEMPLATE, integration_checklist_sub_categories.ORDER";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $checklistRows = $stmt->fetchAll($pdo::FETCH_ASSOC);
        }

        ob_start();
        include_once './templates/integration_checklist_home2.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    public function checklistCheckRows($entityID)
    {
        global $pdo;
        $query = "SELECT ID FROM integration_checklist WHERE ENTITY_ID = " . $entityID . " LIMIT 1";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $entities = $stmt->fetchColumn(0);

        $query = "SELECT ID FROM entities WHERE ID = " . $entityID . " LIMIT 1";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $entity = $stmt->fetchColumn(0);
        if (empty($entity) && $entityID != "0") {
            echo "Company not found.";
            exit;
        }
        $this->checklistAddNew($entityID);
    }

    // not in use anymore
    public function checklistCreateRows($entityID)
    {
        global $pdo;
        $query = "SELECT * FROM integration_checklist_sub_categories WHERE DELETED = 0 AND TEMPLATE = 'BASE'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $subCategories = $stmt->fetchAll($pdo::FETCH_ASSOC);
        foreach ($subCategories as $row) {
            $query = "INSERT INTO integration_checklist (ENTITY_ID, TEMPLATE, CATEGORY_ID, SUB_CATEGORY_ID, ROLE_ID, USER_ID, INITIATION_ID, DURATION_ID, REVENUE_ID, COMPLETED_ID) VALUES(" . $entityID . ", 'base', " . $row["CATEGORY_ID"] . ", " . $row["ID"] . ", 0, 0, 0, 0, 0, 0)";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        }
    }

    public function checklistAddNew($entityID) {
        global $pdo;
        $query = "SELECT * FROM integration_checklist WHERE ENTITY_ID = 0";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $subCategories = $stmt->fetchAll($pdo::FETCH_ASSOC);

        foreach($subCategories as $row) {
            $query = "SELECT ID FROM integration_checklist WHERE ENTITY_ID = ".$entityID." AND SUB_CATEGORY_ID = ".$row['SUB_CATEGORY_ID']." AND TEMPLATE = 'base'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            if(empty($result) && $entityID != "0") {
                $query = "INSERT INTO integration_checklist (ENTITY_ID, TEMPLATE, CATEGORY_ID, SUB_CATEGORY_ID, ROLE_ID, USER_ID, INITIATION_ID, DURATION_ID, REVENUE_ID, COMPLETED_ID)
                        VALUES(".$entityID.", 'base', ".$row["CATEGORY_ID"].", ".$row["SUB_CATEGORY_ID"].", ".$row["ROLE_ID"].", ".$row["USER_ID"].", ".$row["INITIATION_ID"].", ".$row["DURATION_ID"].", ".$row["REVENUE_ID"].", ".$row["COMPLETED_ID"].")";
                $stmt = $pdo->prepare($query);
                $stmt->execute();
            }
        }
    }

    public function checklistUpdateRows()
    {
        if (!is_array($_POST)) {
            return json_encode(['status' => 'failed', 'error' => 'no data to save']);
        }
        global $pdo;
        foreach ($_POST as $key => $value) {
            if (strpos($key, 'FLD_') !== false) {
                $array = explode('_', $key);
                $fieldName = strtoupper($array[2]) . "_ID";
                if (empty($value)) {
                    $value = 0;
                }
                $query = "UPDATE integration_checklist SET " . $fieldName . " = " . $value . " WHERE ID = " . $array[1];
                $stmt = $pdo->prepare($query);
                $stmt->execute();
            }
        }

        return header("Location: /content.php?SITE=entity_integration_checklist&COMPANY_ID=" . $_REQUEST['ID']);
    }

    public function checklistUpdateSubcategory()
    {
        if (!is_array($_POST)) {
            return json_encode(['status' => 'failed', 'error' => 'no data to save']);
        }

        global $pdo;
        foreach ($_POST as $key => $value) {
            if (strpos($key, 'FLD_') !== false) {
                $array = explode('_', $key);
                if(empty($value) && $array[1] == "new") {
                    continue;
                }
                elseif(isset($array[2]) && $array[2] == "DELETE") {
                    $query = "UPDATE integration_checklist_sub_categories SET DELETED = 1 WHERE ID = ".$array[1];
                }
                elseif(isset($array[2]) && $array[2] == "CATEGORY") {
                    $query = "UPDATE integration_checklist_categories SET NAME = '".$value."' WHERE ID = ".$array[1];
                }
                elseif($array[1] == "new") {
                    $template_type = "EXTRA";
                    if(isset($_REQUEST['ID']) && $_REQUEST['ID'] == "0") {
                        $template_type = "BASE";
                    }
                    $value = str_replace("'", "\'", $value);
                    $query = "INSERT INTO integration_checklist_sub_categories (CATEGORY_ID, TEMPLATE, NAME, DELETED, `ORDER`) VALUES(".$array[2].", '".$template_type."', '".$value."', 0, 0)";
                }
                elseif (empty($value)) {
                    return "No value found.";
                }
                else {
                    $query = "UPDATE integration_checklist_sub_categories SET NAME = '" . $value . "' WHERE ID = " . $array[1];
                }
                $stmt = $pdo->prepare($query);
                $_SESSION['status'] = 'failed';
                if($stmt->execute()) {
                    $_SESSION['status'] = 'success';
                }
                if(!empty($value) && $array[1] == "new") {
                    $subCatID = $pdo->lastInsertId();
                    $query = "INSERT INTO integration_checklist (ENTITY_ID, TEMPLATE, CATEGORY_ID, SUB_CATEGORY_ID, ROLE_ID, USER_ID, INITIATION_ID, DURATION_ID, REVENUE_ID, COMPLETED_ID) VALUES(" . $_REQUEST['ID'] . ", 'extra', " . $_REQUEST["CATEGORY_ID"] . ", " . $subCatID . ", 0, 0, 0, 0, 0, 0)";
                    $stmt = $pdo->prepare($query);
                    $stmt->execute();
                }
            }
        }

        return header("Location: /content.php?SITE=entity_integration_checklist&COMPANY_ID=" . $_REQUEST['ID']."&CATEGORY_ID=".$_REQUEST['CATEGORY_ID']);
    }

    public function checklistUpdateHeaders() {
        if(isset($_REQUEST['TABLE'])) {
            $table = $_REQUEST['TABLE'];
        }
        if(!isset($table)) {
            $_SESSION['status'] = 'failed';
            return header('Location: content.php?SITE=entity_integration_checklist&CHECKLIST_HEADER='.$_REQUEST['CHECKLIST_HEADER']);
        }
        global $pdo;
        foreach($_POST as $key => $value) {
            if (strpos($key, 'FLD_') !== false) {
                $array = explode('_', $key);
                if (strpos($key, '_new_') !== false) {
                    if($_REQUEST['CHECKLIST_HEADER'] == 'role' || $_REQUEST['CHECKLIST_HEADER'] == 'revenue') {
                        if($value == '') {
                            continue;
                        }
                        $query = "INSERT INTO ".$table." (NAME, DELETED, `ORDER`) VALUES('".$value."', 0, 0)";
                        $stmt = $pdo->prepare($query);
                        $stmt->execute();
                        continue;
                    }
                    else {
                        dpm('quit', true);
                    }
                }
                elseif(isset($array[2]) && $array[2] == 'DELETE') {
                    $query = "DELETE FROM ".$table." WHERE ID=".$array[1];
                    $stmt = $pdo->prepare($query);
                    $stmt->execute();
                    continue;
                }
                $id = $array[1];
                $field = $array[2];
                if($field == "ROLE") {
                    $field .= "_".$array[3];
                }
                $query = "UPDATE ".$table. " SET `".$field."` = '".$value."' WHERE ID=".$id;
                $stmt = $pdo->prepare($query);
                $stmt->execute();
            }
        }
        $_SESSION['status'] = 'success';
        return header('Location: content.php?SITE=entity_integration_checklist&CHECKLIST_HEADER='.$_REQUEST['CHECKLIST_HEADER'].'&COMPANY_ID='.$_REQUEST['COMPANY_ID']);
    }

    public function checklistAddNewUser() {
        global $pdo;
        if(isset($_POST)) {
            $query = "INSERT INTO integration_checklist_users_role (ROLE_ID, NAME, `ORDER`, DELETED) VALUES(".$_POST['FLD_0_ROLE_ID'].", '".$_POST['FLD_0_NAME']."', 0, 0)";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $_SESSION['status'] = 'success';
            return header('Location: content.php?SITE=entity_integration_checklist&CHECKLIST_HEADER=name&COMPANY_ID='.$_REQUEST['COMPANY_ID']);
        }
    }

    public function checklistAddNewInitiation() {
        global $pdo;
        if(isset($_POST)) {
            $query = "INSERT INTO integration_checklist_initiation (VALUE, QUANTITY, QUALITY, `ORDER`, DELETED) VALUES('".$_POST['FLD_0_VALUE']."', '".$_POST['FLD_0_QUANTITY']."', '".$_POST['FLD_0_QUALITY']."', 0, 0)";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $_SESSION['status'] = 'success';
            return header('Location: content.php?SITE=entity_integration_checklist&CHECKLIST_HEADER=initiation&COMPANY_ID='.$_REQUEST['COMPANY_ID']);
        }
    }

    public function checklistAddNewDuration() {
        global $pdo;
        if(isset($_POST)) {
            $query = "INSERT INTO integration_checklist_duration (VALUE, QUANTITY, QUALITY, `ORDER`, DELETED) VALUES('".$_POST['FLD_0_VALUE']."', '".$_POST['FLD_0_QUANTITY']."', '".$_POST['FLD_0_QUALITY']."', 0, 0)";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $_SESSION['status'] = 'success';
            return header('Location: content.php?SITE=entity_integration_checklist&CHECKLIST_HEADER=duration&COMPANY_ID='.$_REQUEST['COMPANY_ID']);
        }
    }

    public function checklistAddNewCompleted() {
        global $pdo;
        if(isset($_POST)) {
            $query = "INSERT INTO integration_checklist_completed (VALUE, STATUS, COLOR, `ORDER`, DELETED) VALUES('".$_POST['FLD_0_VALUE']."', '".$_POST['FLD_0_STATUS']."', '".$_POST['FLD_0_COLOR']."', 0, 0)";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $_SESSION['status'] = 'success';
            return header('Location: content.php?SITE=entity_integration_checklist&CHECKLIST_HEADER=completed&COMPANY_ID='.$_REQUEST['COMPANY_ID']);
        }
    }

    public function checklistUpdateFilter() {
        global $pdo;
        $dateFrom = '';
        $dateTo = '';
        if(isset($_POST)) {
            foreach($_POST as $key => $value) {
                if(contains($key, 'FLD_')) {
                    $array = explode('_', $key);
                    if($array[1] == 'ENTITY') {
                        $userID = getLoginID();
                        $query = "SELECT ID FROM integration_checklist_filter WHERE ENTITY_ID = ".$array[2]." AND USER_ID=".$userID. " LIMIT 1";
                        $stmt = $pdo->prepare($query);
                        $stmt->execute();
                        $result =  $stmt->fetchColumn();
                        if($value == 'on') {
                            $value = "0";
                        }
                        elseif($value == 'hidden') {
                            $value = "1";
                        }
                        if(!$result) {
                            $query = "INSERT INTO integration_checklist_filter (ENTITY_ID, USER_ID, HIDE_FROM_CHECKLIST) VALUES (".$array[2].", ".$userID.", ".$value.")";
                        }
                        else {
                            $query = "UPDATE integration_checklist_filter SET `HIDE_FROM_CHECKLIST` = ".$value." WHERE ENTITY_ID = ".$array[2] . " AND USER_ID = ".$userID;
                        }
                        $stmt = $pdo->prepare($query);
                        $stmt->execute();
                    }
                    elseif($array[1] == 'DATE') {
                        if($value != '') {
                            if($array[2] == 'FROM') {
                                $dateFrom = "&STARTDATE=".$value;
                            }
                            elseif($array[2] == 'TO') {
                                $dateTo = "&ENDDATE=".$value;
                            }
                        }
                    }
                }
            }
        }
        return header('Location: content.php?SITE=entity_integration_checklist'.$dateFrom.$dateTo);
    }

    public function editPassword() {
        global $pdo;

        include_once './templates/edit_password.php';
        $view = ob_get_clean();

        return $this->page($view);
    }

    public function updatePassword() {
        if(!$this->checkOldPassword($_POST['old_password'])) {
            $_SESSION['password_status'] = 'wrong_old';
            return header('Location: content.php?SITE=entity_editPassword');
        }
       if(!$this->checkNewPassword()) {
           $_SESSION['password_status'] = 'no_match';
           return header('Location: content.php?SITE=entity_editPassword');
       }

       global $pdo;

        $options = [
            'cost' => 12
        ];

       $query = "UPDATE login SET
                  PASSWORD='".$_POST['new_password_2']."',
                  HASH = '".password_hash($_POST['new_password_2'], PASSWORD_BCRYPT, $options)."'
                  WHERE PERSONEELSLID = ".getUserID();

        $stmt = $pdo->prepare($query);
        if(!$stmt->execute()) {
            $_SESSION['password_status'] = 'error';
            return header('Location: content.php?SITE=entity_editPassword');
        }

        $_SESSION['password_status'] = 'saved';
        return header('Location: content.php?SITE=entity_editPassword');
    }

    public function updateColorsCountries() {
        if(!isset($_POST['formchecker_countrycolor'])) {
            return '';
        }
        global $pdo;
        foreach($_POST as $key => $value) {
            if($key == "submit" || $key == "formchecker_countrycolor") {
                continue;
            }
            $array = explode("-", $key);
            $countryID = $array[1];
            $query = "UPDATE countries SET COLOR = '".$value."' WHERE ID = ".$countryID;
            $stmt = $pdo->prepare($query);
            if(!$stmt->execute()) {
                    $error = "set";
                    return $query;
            }
        }
        return header('Location: content.php?SITE=entity_home2&VISUAL=start&MENUID=9');
    }

    private function checkOldPassword($oldPass) {
        global $pdo;

        $persID = getUserID();
        $query = "SELECT PASSWORD FROM login WHERE PERSONEELSLID = ".$persID." AND DELETED = 'Nee' LIMIT 1";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $savedPass = $stmt->fetchColumn();

        if($oldPass != $savedPass) {
            return false;
        }
        return true;
    }

    private function checkNewPassword() {
        $newPasswordOne = $_POST['new_password_1'];
        $newPasswordTwo = $_POST['new_password_2'];

        if($newPasswordOne != $newPasswordTwo) {
            return false;
        }
        return true;
    }
}
