<?php

namespace Controllers;

use Controllers\Helpers\Helper;

include_once './Controllers/Helpers/PageDefault.class.php';


class Import extends Helpers\PageDefault
{
    public $ajax,
        $class,
        $keyName,
        $title;

    /**]
     * Entity constructor.
     */
    public function __construct($user = 0, $company = 0, $options = [])
    {

        $whitelist = [
            'ajax' => false,
            'class' => 'import',
            'title' => 'Import tool',
        ];


        foreach ($whitelist as $key => $value) {
            if (isset($options[$key])) {
                $value = $options[$key];
            }
            $this->$key = $value;
        }

        $this->user = $user;
        $this->company = $company;
        $this->linkBase = "content.php?SITE=import";

    }

    /**
     * GET overview table with all files and filters. Use AJAX request for 'clean' feed.
     */
    public function index()
    {
        // default page for import
    }

    /**
     * GET import form for csv files
     */
    public function csv()
    {
        $this->linkBase .= '_csv';
        ob_start();
        include_once './templates/import_csv.php';
        $view = ob_get_clean();
        return $this->page($view);
    }

    /**
     * GET import form for csv files
     */
    public function csvSave()
    {
        $result = '';

        $style_error = "
                    padding: 15px;
                    margin-bottom: 20px;
                    border: 1px solid transparent;
                    border-radius: 4px;
                    color: #a94442;
                    background-color: #f2dede;
                    border-color: #ebccd1;
                ";


        if (!$this->ajax || !isset($_FILES['importfile'])) {
            $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                There's no data to save.
                              </div>";
            return $div_error;
            return json_encode(['status' => 'failed', 'error' => 'no data to save']);
        }

        ini_set('memory_limit', '1024M');

        $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
        $importfile = $_FILES['importfile'];
        $root = getcwd();
        $importLocation = $root . "/import_csv/";
        $import = $importLocation . $importfile["name"];

        if (!in_array($importfile['type'], $mimes)) {
            $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                Sorry, the type '".$importfile['type']."' is not allowed.
                              </div>";
            return $div_error;
            return json_encode(['status' => 'failed', 'error' => 'Sorry, type: '.$importfile['type'].' is not allowed.']);
        } elseif ($importfile["error"] > 0) {
            $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                An unknown error has occurred. Error code: ". $importfile["error"]."
                              </div>";
            return $div_error;
            return json_encode(['status' => 'failed', 'error' => 'Return Code: ' . $importfile["error"]]);
        }
        $result .= "Upload: " . $importfile["name"] . "<br />";
        $result .= "Type: " . $importfile["type"] . "<br />";
        $result .= "Size: " . ($importfile["size"] / 1024) . " Kb<br />";
        $result .= "Temp file: " . $importfile["tmp_name"] . "<br />";

        // UITCOMMENTEN WANNEER KLAAR
//        if (file_exists($import)) {
//            $div_error = "<div style='".$style_error."'>
//                                    <b>The import failed!</b><br /><br />
//                                    The file you tried to upload has already been uploaded.
//                                  </div>";
//            return $div_error;
//            return json_encode(['status' => 'failed', 'error' => $importfile["name"] . ' already exists.']);
//        }

        if ($error = move_uploaded_file($importfile["tmp_name"], $import) !== true) {
            $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                An unknown error has occurred. Error: ".$error."
                              </div>";
            return $div_error;
            return json_encode(['status' => 'failed', 'error' => $error]);
        }
        $result .= "Stored in: " . $import . "<br />";

        if (!$file = fopen($import, 'r')) {
            $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                The file couldn't be opened.
                              </div>";
            return $div_error;
            return json_encode(['status' => 'failed', 'error' => 'File couldn\'t be opened.']);
        }


        $data = $this->csv_to_multidimension_array($import, ';');
        $headers = array_shift($data);

        $columns = [
            'accreditation' => 'ACCREDITATION',
            'activity' => 'ACTIVITY_ID',
            'advisor' => 'CF_ADVISOR',
            'businessdescription' => 'DESCRIPTION',
            'callbackon' => 'HEADLINE_DATE',
            'country' => 'COUNTRY_ID',
            'comment' => 'HEADLINE_COMMENT',
            'commentnormec' => 'HEADLINE_COMMENT',
            'company'   => 'NAME',
            'companyname' => 'NAME',
            'curency'   => 'CURRENCY',
            'currency'  => 'CURRENCY',
            'ebitda' => 'EBITDA',
            'ebitdamultiple' => 'EV_EBITDA_MULTIPLE',
            'email' => 'HEADLINE_COMMENT',
            'geo' => 'COUNTRY_ID',
            'growth' => 'GROWTH',
            'headcount' => 'HEADCOUNT',
            'location' => 'ADDRESS',
            'margin' => 'MARGIN',
            'mgmt' => 'MANAGEMENT_ID',
            'phone' => 'HEADLINE_COMMENT',
            'potential' => 'RATE_POTENTIAL',
            'probability' => 'RATE_RECEPTION',
            'rating'    => 'RATE_POTENTIAL',
            'recalldate'    => 'HEADLINE_DATE',
            'reception' => 'RATE_RECEPTION',
            'responsible'   => 'RESPONSIBLE_ID',
            'revenue' => 'REVENUE',
            'stage' => 'STAGE_ID',
            'status' => 'STAGE_ID',
            'sector' => 'SEGMENT_ID',
            'shareholders' => 'SHAREHOLDERS',
            'strategic' => 'STRATEGIC_ID',
            'synergies' => 'SYNERGIES',
            'tags' => 'TAGS',
            'targetname' => 'NAME',
            'valuation' => 'VALUATION',
            'website' => 'WEBSITE',
            'year'  => 'YEAR',

        ];

        $unmappedHeaders = [];
        $mappedHeaders = [];
        foreach ($headers as $i => $value) {
            $valueClean = preg_replace('/\s+/', '', preg_replace("/\xEF\xBB\xBF/", '', html_entity_decode(strtolower($value))));
//    $column = array_search($valueClean, $columns);
            $column = Helper::arrVal($columns, $valueClean);
            if ($column === false) {

                $iReadable = $i;
                $unmappedHeaders[$i] = ++$iReadable . '. ' . $value . '(' . trim(strtolower($value)) . ')';
                continue;
            }
            $mappedHeaders[$i] = $column;
        }

        if ($unmappedHeaders) {
            $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                Some of the headers were not recognized. Please contact an administrator.<br /><br />
                                Unrecognizable headers: <br />
                                <pre>".print_r($unmappedHeaders)."</pre>
                              </div>";
            return $div_error;
            return json_encode(['status' => 'failed', 'error' => 'These headers are not allowed '.print_r($unmappedHeaders, true)]);
        }

        $defaults = [
            'USER_ID' => 0,
            'COMPANY_ID' => 1,
            'HEADLINE_ACTION' => "'Call back on'"
        ];

        $importName = time() . " - " . $importfile["name"];

        require_once("dbConnecti.php");
        global $pdo;

        $mysqli = db_open();

        $result .= "<table>";
        $tagsFound = false;
        $accreditatationFound = false;
        $backupHeaders = $mappedHeaders;
        $mappedHeaders = array_unique($mappedHeaders);
        $headerWithoutTags = $mappedHeaders;
        if (($key = array_search("TAGS", $headerWithoutTags)) !== false) {
            unset($headerWithoutTags[$key]);
        }
        if (($key = array_search("ACCREDITATION", $headerWithoutTags)) !== false) {
            unset($headerWithoutTags[$key]);
        }
        foreach ($data as $rowNumber => $row) {
            //$result .= "<tr>";
            $rowNumber++;
            $rowNumber++;
            //$result .= "<td>Starting to import <b>$rowNumber</b></td><td>" . $row[array_search('NAME',
              //      $mappedHeaders)] . '</td>';
            $query = "INSERT INTO entities (`IMPORT`, `" . implode("`, `",
                    array_keys($defaults)) . "`, `" . implode("`, `",
                    $headerWithoutTags) . "`) VALUES (";
            $query .= "'$importName', " . implode(", ", $defaults);
            foreach ($mappedHeaders as $i => $column) {
                $tagsFoundSkip = false;
                $accreditatationFoundSkip = false;

                $aValue = "";
                $query .= ", ";
                $value = $row[$i];
                if ($value === '' && $column != 'HEADLINE_COMMENT') {
                    $value = "NULL";
                    if($column == "COUNTRY_ID") {
                        $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                The field 'country' is empty on row: ".$rowNumber.".<br />
                                Country is not allowed to be empty.
                              </div>";

                        return $div_error;
                    }
                    elseif($column == 'TAGS' || $column == "ACCREDITATION") {
                        $query = substr($query, 0, -2);
                        continue;
                    }
                } else {
                    $value = mysqli_real_escape_string($mysqli, $value);
                    switch ($column) {
                        case 'HEADLINE_DATE':
                            $value = "'" . date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $value))) . "'";
                            break;

                        case 'HEADLINE_COMMENT':
                            foreach ($backupHeaders as $p => $coll) {
                                $tempValue = str_replace("'", "", $row[$p]);
                                if($coll == 'HEADLINE_COMMENT') {
                                    if($aValue != "") {
                                        $aValue .= "&lt;br /&gt;" . $tempValue;
                                    } else {
                                        $aValue .= $tempValue;
                                    }
                                }
                            }
                            $value = "'$aValue'";
                            break;
                        case 'REVENUE':
                        case 'EBITDA':
                        case 'SYNERGIES':
                        case 'DEAL_SIZE':
                        case 'VALUATION':
                        case 'EV_EBITDA_MULTIPLE':
                        case 'UPFRONT_PRICE':
                        case 'EARNOUT':
                            $value = floatval(str_replace(',', '.', $value));
                            break;
                        case 'GROWTH':
                        case 'MARGIN':
                        case 'HEADCOUNT':
                        case 'ACQUIRED':
                            $value = intval($value);
                            break;
                        case 'STAGE_ID':
                            $stmt_stage = $pdo->prepare("INSERT INTO stages(`NAME`) SELECT '$value' FROM mutex "
                                . "LEFT OUTER JOIN stages ON stages.`NAME` = '$value' "
                                . "WHERE mutex.i = 1 AND stages.`NAME` IS NULL;");
                            $stmt_stage->execute();
                            $value = "(SELECT stages.ID FROM stages WHERE LOWER(`NAME`) = LOWER('$value') LIMIT 1)";
                            break;
                        case 'COUNTRY_ID':
                            $stmt_stage = $pdo->prepare("INSERT INTO countries(`NAME`) SELECT '$value' FROM mutex "
                                . "LEFT OUTER JOIN countries ON countries.`NAME` = '$value' "
                                . "WHERE mutex.i = 1 AND countries.`NAME` IS NULL;");
                            $stmt_stage->execute();
                            $value = "(SELECT countries.ID FROM countries WHERE LOWER(`NAME`) = LOWER('$value') LIMIT 1)";
                            break;
                        case 'SEGMENT_ID':
                            $stmt_stage = $pdo->prepare("INSERT INTO segments(`NAME`) SELECT '$value' FROM mutex "
                                . "LEFT OUTER JOIN segments ON segments.`NAME` = '$value' "
                                . "WHERE mutex.i = 1 AND segments.`NAME` IS NULL;");
                            $stmt_stage->execute();
                            $value = "(SELECT segments.ID FROM segments WHERE LOWER(`NAME`) = LOWER('$value') LIMIT 1)";
                            break;
                        case 'RESPONSIBLE_ID':
                            $value = "(SELECT personeel.ID FROM personeel WHERE LOWER(REPLACE(CONCAT(`VOORNAAM`, `TUSSENVOEGSEL`, `ACHTERNAAM`), ' ', '')) = LOWER(REPLACE('$value', ' ', '')) LIMIT 1)";
                            break;
                        case 'ACTIVITY_ID':
                            $stmt_stage = $pdo->prepare("INSERT INTO activities(`NAME`) SELECT '$value' FROM mutex "
                                . "LEFT OUTER JOIN activities ON activities.`NAME` = '$value' "
                                . "WHERE mutex.i = 1 AND activities.`NAME` IS NULL;");
                            $stmt_stage->execute();
                            $value = "(SELECT activities.ID FROM activities WHERE LOWER(`NAME`) = LOWER('$value') LIMIT 1)";
                            break;
                        case 'STRATEGIC_ID':
                            $stmt_stage = $pdo->prepare("INSERT INTO strategics(`NAME`) SELECT '$value' FROM mutex "
                                . "LEFT OUTER JOIN strategics ON strategics.`NAME` = '$value' "
                                . "WHERE mutex.i = 1 AND strategics.`NAME` IS NULL;");
                            $stmt_stage->execute();
                            $value = "(SELECT strategics.ID FROM strategics WHERE LOWER(`NAME`) = LOWER('$value') LIMIT 1)";
                            break;
                        case 'MANAGEMENT_ID':
                            $stmt_stage = $pdo->prepare("INSERT INTO managements(`NAME`) SELECT '$value' FROM mutex "
                                . "LEFT OUTER JOIN managements ON managements.`NAME` = '$value' "
                                . "WHERE mutex.i = 1 AND managements.`NAME` IS NULL;");
                            $stmt_stage->execute();
                            $value = "(SELECT managements.ID FROM managements WHERE LOWER(`NAME`) = LOWER('$value') LIMIT 1)";
                            break;
                        case 'TAGS':
                            $tagsFoundSkip = true;
                            $tagsFound = true;
                            $query = substr($query, 0, -2);
                            $saveTags = $value;
                            break;
                        case 'ACCREDITATION':
                            $accreditatationFoundSkip = true;
                            $accreditatationFound = true;
                            $saveAccreditations = $value;
                            $value = '';
                            $query = substr($query, 0, -2);
                            break;
                        case 'NAME':
                        case 'ADDRESS':
                            $value = "'" . htmlentities($value) . "'";
                            break;
                        default:
                            $value = "'" . $value . "'";
                            break;
                    }
                }

                if($tagsFoundSkip) {
                    continue;
                }

                $query .= $value;
            }
            $query .= ")";
//            dpm($query, true);
            $stmt = $pdo->prepare($query);
            if (!$stmt->execute()) {
                $div_error = "<div style='".$style_error."'>
                                <b>The import failed!</b><br /><br />
                                Import failed on row: ".$rowNumber." <br /><br />
                                Error info: <pre>".print_r($stmt->errorInfo()[2], true) . "</pre><br />
                                Query: <pre>".$query."</pre>
                              </div>";
                return $div_error;
                return json_encode(['status' => 'failed', 'error' => 'Failed ('.$rowNumber.') '.$query.'<pre>'.print_r($stmt->errorInfo(), true) . '</pre>']);
            }
            $entity_ID = $pdo->lastInsertId();
            if($tagsFound) {
                $pieces = explode(",", $saveTags);
                foreach($pieces as $piece) {
                    $query = "INSERT INTO entity_tags (`ENTITY_ID`, `TAG_ID`) VALUES (".$entity_ID.", (SELECT ID FROM tags WHERE LOWER(NAME) = '".trim(strtolower($piece))."' LIMIT 1))";
                    $stmt = $pdo->prepare($query);
                    if (!$stmt->execute()) {
                        $stmt = $pdo->prepare("SELECT ID FROM tags WHERE LOWER(NAME) = '".trim(strtolower($piece))."' LIMIT 1");
                        $stmt->execute();
                        $value = $stmt->fetchAll($pdo::FETCH_COLUMN);
                        if(empty($value)) {
                            $result .= "<tr><td>`".$piece."` is not saved in DB. Tried to add tag to entity: ".$entity_ID."</td></tr>";
                        } else {
                            $result .= "<tr><td>Something went wrong with tags import, Query: ".$query."</td></tr>";
                        }
                    }
                }
                $tagsFound = false;
            }

            if($accreditatationFound) {
                $pieces = explode(",", $saveAccreditations);
                foreach($pieces as $piece) {
                    $query = "INSERT INTO entity_accreditations (`ENTITY_ID`, `ACCREDITATION_ID`) VALUES (".$entity_ID.", (SELECT ID FROM accreditations WHERE LOWER(NAME) = '".trim(strtolower($piece))."' LIMIT 1))";
                    $stmt = $pdo->prepare($query);
                    if (!$stmt->execute()) {
                        $stmt = $pdo->prepare("SELECT ID FROM accreditations WHERE LOWER(NAME) = '".trim(strtolower($piece))."' LIMIT 1");
                        $stmt->execute();
                        $value = $stmt->fetchAll($pdo::FETCH_COLUMN);
                        if(empty($value)) {
                            $result .= "<tr><td>`".$piece."` is not saved in DB. Tried to add accreditation to entity: ".$entity_ID."</td></tr>";
                        } else {
                            $result .= "<tr><td>Something went wrong with accreditations import, Query: ".$query."</td></tr>";
                        }
                    }
                }
                $accreditatationFound = false;
            }
            //$result .= "</tr>";
        }
        $result .= "</table>";
        $result .= "<br/> Upload successfull.";
        /*
//var_dump($headers);
//var_dump($data);


//$rows = array_map('str_getcsv', file($import));
//
//$headers = str_getcsv();
//var_dump($headers);
//foreach($rows as $row) {
//    $columns = str_getcsv($row);
//    var_dump($columns);
//}

//function parse_row($row) {
//    return array_map('trim', explode(';', $row));
//}
//
////$rows   = str_getcsv(file($import), "\n");
//$keys   = parse_row(array_shift($rows));
//$result = array();
//
//foreach ($rows as $row) {
//    $row = parse_row($row);
//    $row = array_pad($row, 3, NULL);
//
//    $result[] = array_combine($keys, $row);
//}

        /*
        $firstline = fgets($file, 4096);
        //Gets the number of fields, in CSV-files the names of the fields are mostly given in the first line
        $num = strlen($firstline) - strlen(str_replace(";", "", $firstline));

        //save the different fields of the firstline in an array called fields
        $fields = array();
        $fields = explode(";", $firstline, ($num + 1));

        $line = array();
        $i = 0;

        //CSV: one line is one record and the cells/fields are seperated by ";"
        //so $dsatz is an two dimensional array saving the records like this: $dsatz[number of record][number of cell]
        while ($line[$i] = fgets($file, 4096)) {

            $dsatz[$i] = array();
            $dsatz[$i] = explode(";", $line[$i], ($num + 1));

            $i++;
        }

        echo "<table>";
        echo "<tr>";
        for ($k = 0; $k != ($num + 1); $k++) {
            echo "<td>" . $fields[$k] . "</td>";
        }
        echo "</tr>";

        foreach ($dsatz as $key => $number) {
            //new table row for every record
            echo "<tr>";
            foreach ($number as $k => $content) {
                //new table cell for every field of the record
                echo "<td>" . $content . "</td>";
            }
        }

        echo "</table>";*/

        return $result; // html


        return json_encode(['status' => 'success', 'message' => $result]);
    }

    private function csv_to_multidimension_array($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                $data[] = $row;
            }
            fclose($handle);
        }
        return $data;
    }
}