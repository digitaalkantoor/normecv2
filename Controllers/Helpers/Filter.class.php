<?php

namespace Controllers\Helpers;


class Filter
{

    static function select(
        $column,
        $children,
        $url,
        $htmlTable = "",
        $reset = true
    ) {
        $onChange = "";
        if ($url != '') {
            $onChange = "onchange=\"getPageContent2('$url', 'contentTable_$htmlTable', searchReq2);\"";
        }

        $content = "<select name=\"SRCH_$column\" id=\"SRCH_$column\" $onChange multiple>";

        if ($reset) {
            $content .= "<option value=\"\">All</option>";
        }

        foreach ($children as $child => $tmpName) {
            if (is_array($tmpName) || $tmpName === NULL) {
                $tmpName = $child;
            }
            $name = tl(stripslashes($tmpName));

            $selected = "";
            if (isset($_REQUEST["SRCH_$column"]) && ($_REQUEST["SRCH_$column"] == $child || $_REQUEST["SRCH_$column"] == "x" . $child)) {
                $selected = "selected=\"true\"";
            }

            $content .= "<option $selected value='" . str_replace("'", "&apos;", $child) . "'>" . lb($name) . "</option>";

        }

        $content .= "</select>";

        return $content;

    }
}