<?php

namespace Controllers\Helpers;

// ToDo: Build in an autoloader
include_once 'Filter.class.php';
include_once 'Helper.class.php';

use PDO;

/**
 * Class Table
 *
 * @package components
 */
class Table
{
    public $company,
        $structure,
        $table,
        $tableFields,
        $user,
        $whitelistOptionTables;

    public function __construct()
    {
        $this->whitelistOptionTables = [
            "managements",
            "strategics",
            "activities",
            "segments",
            "stages",
            "entity_attachments",
            "pipeline_types",
            "tags",
            "accreditations"
        ];
    }

    protected function getStructure($column = 'all')
    {
        include_once './components/table_structure.php';
        return getTableStructure($this->user, $this->company, $this->structure, $column);
    }

    public function editStructure()
    {
        global $pdo;

        $query = "SELECT * FROM table_structure "
            . "WHERE COMPANY_ID = $this->company AND `TABLE` = '$this->table' AND !DELETED "
            . "ORDER BY `ORDER`";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll($pdo::FETCH_OBJ);
        if (!$result) {
            return 'ERROR: Structure not found in database.';
        }
        ob_start();
        include_once './templates/structure_edit.php';
        $view = ob_get_clean();

        return $view;
    }

    public function updateStructure()
    {
        global $pdo;
        if (!$this->ajax || !isset($_POST['order']) || !isset($_POST['table'])) {
            return json_encode(['status' => 'failed', 'error' => 'No data found']);
        }
        $ordering = Helper::postVal('order');
        $showColumn = Helper::postVal('columns');
        $showFilter = Helper::postVal('filters');
        if (!is_array($ordering)) {
            return json_encode(['status' => 'failed', 'error' => "Something wen't wrong"]);
        }
        foreach ($ordering as $id => $order) {
            $query = "UPDATE table_structure SET"
                . "`order` = $order, "
                . "SHOW_COLUMN = " . (Helper::arrVal($showColumn, $id) ? 'true' : 'false') . ", "
                . "SHOW_FILTER = " . (Helper::arrVal($showFilter, $id) ? 'true' : 'false') . " "
                . "WHERE ID = $id";
            $stmt = $pdo->prepare($query);
            if (!$stmt->execute()) {
//                return json_encode(['status' => 'failed', 'error' => "Column $id: " .print_r($stmt->errorInfo(), true)]);
                return json_encode(['status' => 'failed', 'error' => "Column $id: Error while saving"]);
            }
        }
        return json_encode(['status' => 'success', 'error' => 'All data saved']);
    }

    public function editOption()
    {
        global $pdo;

        $tables = $this->whitelistOptionTables;

        $results = array();
        foreach ($tables as $table) {
            $query = "SELECT " . $table . ".* FROM  " . $table . " WHERE `DELETED` = 0 ORDER BY `ORDER`";

            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll($pdo::FETCH_OBJ);
            $results[$table] = $result;
        }
        if (!$results) {
            return 'ERROR: Structure not found in database.';
        }
        ob_start();
        include_once './templates/option_edit.php';
        $view = ob_get_clean();

        return $view;
    }

    public function updateOption()
    {
        global $pdo;
        $tables = $this->whitelistOptionTables;
        if (!$this->ajax || !isset($_POST['table_checker']) || !isset($_POST['table_checker']) || !in_array($_POST['table_checker'],
                $tables)) {
            return json_encode(['status' => 'failed', 'error' => 'No data found']);
        }

        $currentTable = $_POST['table_checker'];

        $error = [];
        foreach ($_POST[$currentTable] as $id => $value) {
            if ($id == "new") {
                $value = array_filter($value);
                if (!$value) {
                    continue;
                }
                $query = "INSERT INTO $currentTable (`NAME`) VALUES ";
                $query .= "('" . implode("'), ('", $value) . "')";
                $stmt = $pdo->prepare($query);
                if (!$stmt->execute()) {
                    $error[] = $query;
                    $error[] = $pdo->errorInfo();
                }
                continue;
            }
            $name = $value['name'];
            $deleted = (int)isset($value['deleted']);
            $order = (int)$value['order'];
            $query = "UPDATE $currentTable SET `NAME` = '$name', `DELETED` = $deleted, `ORDER` = $order WHERE `ID` = $id";
            $stmt = $pdo->prepare($query);
            if (!$stmt->execute()) {
                $error[] = $query;
                $error[] = $pdo->errorInfo();
            }
        }

        if ($error) {
            return json_encode(['status' => 'failed', 'error' => print_r($error, true)]);
        }

        return json_encode(['status' => 'success', 'error' => 'All data saved']);
    }

    public function getTableWithHeader($tableContent, $hideFilters = false)
    {
        if (!is_array($this->tableFields)) {
            $this->tableFields = $this->getStructure();
        }

        return $this->getTableFilters($hideFilters) . '<br /><br /><div class="oft2_page_centering">' . $tableContent . '</div>';
    }

    protected function getTableContent($links, $options = [])
    {
        global $pdo;

        $this->tableFields = $this->getStructure();
        if (!$this->tableFields) {
            return 'Error: Missing structure for page';
        }

//        dpm($this->tableFields, true);
        $paginate = false;

        $currentUserID = Helper::getVal('PERSONEELSID', (int)getUserID());

        $sql = Helper::arrVal($options, 'sql');
        $group = Helper::arrVal($options, 'group', []);
        $multiSelect = Helper::arrVal($options, 'multiSelect');

        $fallback = $this->structure == "entity_pipeline" ? "HEADLINE_DATE" : "ID";
        $order = Helper::reqVal('SORTEEROP', Helper::arrVal($options, 'order', $fallback));

        $currentPage = Helper::arrVal($_REQUEST, 'PAGNR', 1);
        $content = '';
        $itemsPerPage = 20;
        $limit = '';
        $_REQUEST['BID'] = $this->company;
        $urlQuery = $this->urlQueryBase();
        $urlQuery .= $this->searchFieldsConverter($this->tableFields['arrayZoekvelden']);

        $orderAlt = false;
        $distinct = 'distinct';
        $join = '';

        if (str_starts_with($order, 'pivot|')) {
            $fields = explode('|', $order);
            if (count($fields) == 4) {

                if($_REQUEST['SITE'] && ($_REQUEST['SITE'] == "entity_addressbook_ajax")) {
                    if($fields[2] != "entity_persons") {
                        $join .= "JOIN $fields[2] ON $fields[2].ID = $this->table.$fields[1] ";
                    }
                    else {
                        $join .= "JOIN $fields[2] ON $fields[2].ENTITY_ID = $this->table.$fields[1] ";
                    }
                }
                else {
                    $join .= "JOIN $fields[2] ON $fields[2].ID = $this->table.$fields[1] ";
                    $distinct = '';
                }

                $orderAlt = $order;
                $order = $fields[2] . '.' . $fields[3];
            }
        }

        if($_REQUEST['SITE'] && ($_REQUEST['SITE'] == "entity_addressbook" || $_REQUEST['SITE'] == "entity_addressbook_ajax")) {
            if (strpos($join, 'entity_persons.ENTITY_ID') !== false) {
                // do nothing
            }
            else {
                $join .= "JOIN entity_persons ON entity_persons.ENTITY_ID = entities.ID";
            }
        }

        if(isset($_REQUEST['TAG_ID'])) {
            $join .= "JOIN entity_tags ON entity_tags.ENTITY_ID = entities.ID";
        }

        if(isset($_REQUEST['ACCREDITATION_ID'])) {
            $join .= "JOIN entity_accreditations ON entity_accreditations.ENTITY_ID = entities.ID";
        }

        $fallbackOrder = $this->structure == "entity_pipeline" ? "ASC" : "DESC";
        $orderDir = Helper::arrVal($_REQUEST, 'SORTORDER', $fallbackOrder);
        if ($paginate) {
            $limit = $currentPage > 0 ? ' LIMIT ' . ($itemsPerPage * ($currentPage - 1)) . ', ' . $itemsPerPage : '';
        }
        //Opbouw query
        $whereString = '';
        foreach ($this->sqlQuerySearch() as $queryElem) {
//            dpm($queryElem, true);
            $whereString .= $queryElem[0];
            if (isset($queryElem[2])) {
                $join .= $queryElem[2] . " = $this->table.ID ";
            }
        }

        $where = "";
        if($this->table != "entity_persons") {
            $where = "WHERE (!$this->table.DELETED) $whereString $sql";
        }
//        $showClosedStatus = Helper::getVal('SHOW_CLOSED',
//            "NO") == "YES" ? "" : " AND (STAGE_ID != 1 OR STAGE_ID IS NULL)";

        $where .= $this->structure == "entity_pipeline" ? "AND RESPONSIBLE_ID = " . $currentUserID /*. $showClosedStatus*/ : "";
        $where .= $this->structure == "entity_favorites" ? "AND FAVOURITE = 1" : "";

        if ($this->structure == "entity_pipeline" && !isset($_GET['SORTEEROP'])) {
            $order = "HEADLINE_DATE";
        }

        if(isset($_REQUEST['STAGE_ID']) && isset($_REQUEST['PIPELINE_TYPE_ID'])) {
            $join .= "LEFT JOIN entity_pipeline ON entities.ID = entity_pipeline.ENTITY_ID ";
            $where .= " AND ((STAGE_ID IN(".$_REQUEST['STAGE_ID'].") AND (entities.CREATED_AT >= '".$_REQUEST['DATE_FROM']."' AND entities.CREATED_AT <= '".$_REQUEST['DATE_TO']. "')) OR (entity_pipeline.PIPELINE_TYPE_ID IN (".$_REQUEST['PIPELINE_TYPE_ID'].") AND entity_pipeline.CREATED_AT >= '".$_REQUEST['PIPELINE_DATE_FROM']."' AND entity_pipeline.CREATED_AT <= '".$_REQUEST['PIPELINE_DATE_TO']. "')) ";
        } else {
            if(isset($_REQUEST['STAGE_ID'])) {
                if (strpos($_REQUEST['STAGE_ID'], ',') !== false) {
                    $where .= "AND STAGE_ID IN (".$_REQUEST['STAGE_ID'].")";
                } else {
                    $stageID = intval($_REQUEST['STAGE_ID']);
                    if(is_int($stageID)) {
                        $where .= "AND STAGE_ID = ".$stageID." ";
                    }
                }
            }
            if(isset($_REQUEST['PIPELINE_TYPE_ID'])) {
                $join .= "JOIN entity_pipeline ON entities.ID = entity_pipeline.ENTITY_ID ";
                if (strpos($_REQUEST['PIPELINE_TYPE_ID'], ',') !== false) {
                    $IDs = explode(',', $_REQUEST['PIPELINE_TYPE_ID']);
                    if(is_int(intval($IDs[0])) && is_int(intval($IDs[1]))) {
                        $where .= "AND entity_pipeline.PIPELINE_TYPE_ID IN (".$_REQUEST['PIPELINE_TYPE_ID'].")";
                    }
                }
                else {
                    $pipelineTypeID = intval($_REQUEST['PIPELINE_TYPE_ID']);
                    if(is_int($pipelineTypeID)) {
                        $where .= "AND entity_pipeline.PIPELINE_TYPE_ID = ".$pipelineTypeID." ";
                    }
                }
                $where .= " AND entity_pipeline.DELETED = 0 ";
            }

            if(isset($_REQUEST['DATE_FROM']) && $_REQUEST['DATE_TO']) {
                $where .= "AND entities.CREATED_AT >= '".$_REQUEST['DATE_FROM']."' AND entities.CREATED_AT <= '".$_REQUEST['DATE_TO']. "' ";
            }
        }

        if(isset($_REQUEST['COUNTRY_ID'])) {
            $countryID = intval($_REQUEST['COUNTRY_ID']);
            if($countryID == 0) {
                $where .= "AND COUNTRY_ID IS NULL ";
            }
            else if(is_int($countryID)) {
                $where .= "AND COUNTRY_ID = ".$countryID." ";
            }
        }

        if(!isset($_REQUEST['PIPELINE_TYPE_ID']) && (isset($_REQUEST['PIPELINE_DATE_FROM']) || isset($_REQUEST['MONTH']) || isset($_REQUEST['YEAR']))) {
            $join .= "JOIN entity_pipeline ON entities.ID = entity_pipeline.ENTITY_ID ";
        }

        if(isset($_REQUEST['PIPELINE_DATE_FROM']) && $_REQUEST['PIPELINE_DATE_TO'] && !isset($_REQUEST['STAGE_ID'])) {
            $where .= "AND entity_pipeline.CREATED_AT >= '".$_REQUEST['PIPELINE_DATE_FROM']."' AND entity_pipeline.CREATED_AT <= '".$_REQUEST['PIPELINE_DATE_TO']. "' ";
        }
        elseif(isset($_REQUEST['PIPELINE_DATE_FROM']) && !isset($_REQUEST['STAGE_ID'])) {
            $where .= "AND entity_pipeline.CREATED_AT >= '".$_REQUEST['PIPELINE_DATE_FROM']."' ";
        }

        if(($_REQUEST['SITE'] == "entity_my_pipeline" || $_REQUEST['SITE'] == "entity_my_pipeline_ajax") && !isset($_REQUEST['NO_RECALL_DATE'])) {
            $where .= " AND HEADLINE_DATE IS NOT NULL";
        }

        if(isset($_REQUEST['NO_RECALL_DATE']) && $_REQUEST['NO_RECALL_DATE'] == "SHOW") {
            $where .= " AND ISNULL(HEADLINE_DATE)";
        }

        if(isset($_REQUEST['MONTH'])) {
            $where .= " AND MONTH(entity_pipeline.CREATED_AT) = ".$_REQUEST['MONTH'];
        }

        if(isset($_REQUEST['YEAR'])) {
            $where .= " AND YEAR(entity_pipeline.CREATED_AT) = ".$_REQUEST['YEAR'];
        }

        if(isset($_REQUEST['ONLY_SHOW']) && $_REQUEST['ONLY_SHOW'] == "ADVISORS") {
            $where .= " AND entities.ADVISOR = 1";
        }
        elseif(isset($_REQUEST['ONLY_SHOW']) && $_REQUEST['ONLY_SHOW'] == "TARGETS") {
            $where .= " AND entities.ADVISOR = 0";
        }


        if(isset($_REQUEST['TAG_ID'])) {
            $tag_id = $_REQUEST['TAG_ID'];
            if(!is_int($tag_id)) {
                try {
                    $tag_id = (int)$tag_id;
                    $where .= " AND entity_tags.TAG_ID = ".$tag_id;
                }
                catch(Exception $e) {
                    // do nothing
                }
            }
            else {
                $where .= " AND entity_tags.TAG_ID = ".$tag_id;
            }
        }

        if(isset($_REQUEST['ACCREDITATION_ID'])) {
            $accreditation_id = $_REQUEST['ACCREDITATION_ID'];
            if(!is_int($accreditation_id)) {
                try {
                    $accreditation_id = (int)$accreditation_id;
                    $where .= " AND entity_accreditations.ACCREDITATION_ID = ".$accreditation_id;
                }
                catch(Exception $e) {
                    // do nothing
                }
            }
            else {
                $where .= " AND entity_accreditations.ACCREDITATION_ID = ".$accreditation_id;
            }
        }

        $orderby = "";
        if($this->table == "entities") {
            $orderby = " ORDER BY if(entities.$order = '' or entities.$order is null,1,0), entities.$order $orderDir";
            if (strpos($order, '.') !== false) {
                $orderby = " ORDER BY if($order = '' or $order is null,1,0), $order $orderDir";
            }
        }

        if(isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "entity_addressbook") {
            $orderby = " ORDER BY entities.NAME";
        }

//        if ($this->structure == "entity_pipeline") {
//            $orderby = "ORDER BY HEADLINE_DATE ASC";
//        }

        if($_REQUEST['SITE'] == "entity_favorites") {
            $orderby = "ORDER BY FIELD(STAGE_ID, 1, 2, 3, 9, 4, 5, 13, 7, 6, 8, 10, 12) ASC, STAGE_ID";
        }
        if($_REQUEST['SITE'] == "entity") {
            $orderby = "ORDER BY if(STAGE_ID = '' or STAGE_ID is null, 1,0), FIELD(STAGE_ID, 1, 2, 3, 10, 9, 4, 5, 13, 7, 12, 6, 8, 11) ASC, STAGE_ID";
        }

        if($_REQUEST['SITE'] == "entity" && (!isset($_REQUEST['SRCH_SEARCH']) && !isset($_REQUEST['COUNTRY_ID']) && !isset($_REQUEST['PIPELINE_TYPE_ID']) && !isset($_REQUEST['STAGE_ID']))) {
            $limit = "LIMIT 20";
        }


        $statement = "SELECT $distinct $this->table.* FROM $this->table $join $where $orderby $limit";

        // PRINT QUERY HERE
        if(isset($_REQUEST["DEBUG"]) || isset($_REQUEST["DEVELOPER"])) {
            dpm($statement);
        }

        // PRINT QUERY HERE
        $query = $pdo->prepare($statement);

        $queryAantalRegels = $pdo->prepare("SELECT COUNT($this->table.ID) FROM $this->table $join $where ");
        foreach ($this->sqlQuerySearch() as $queryPart) {
            if (isset($queryPart[1])) {
                foreach ($queryPart[1] as $key => $value) {
                    if($key == "entity_pipelinePIPELINE_TYPE_ID" && strpos($value, ',') !== false) {
                        continue;
                    }
                    $queryAantalRegels->bindValue($key, $value);
                }
            }
        }
        $queryAantalRegels->execute();

        $totaalAantalRegels = $queryAantalRegels->fetchColumn();

        $content .= '<div id="contentTable_' . $this->table . '">';


        //Toon header
        $content .= '<table name="exportexcel" class="sortable" width="100%"><tr style="border-bottom: 1pt solid #7f7466;">';

        if ($multiSelect) {
            $content .= '<th class="sorttable_nosort"><input type="checkbox" name="select_all" value="multi-row-select" onclick="toggleAllCheckboxes(this);"></th>';
        }

        //Bepaal ordering
        foreach ($this->tableFields['arrayVeldnamen'] as $key => $fieldName) {

            if (str_starts_with($key, 'SKIP_')) {
                continue;
            }

            $aParameters = '';
            $orderDir = '';
            if ($order == $this->tableFields['arrayVelden'][$key] || $orderAlt == $this->tableFields['arrayVelden'][$key]) {
                if (isset($_REQUEST['SORTORDER'])) {
                    $orderDir = (($_REQUEST['SORTORDER'] == 'ASC') ? 'DESC' : 'ASC');
                } else {
                    $orderDir = 'ASC';
                    $aParameters = '&SORTORDER=' . $orderDir;
                }
            }

            if(isset($_REQUEST['ONLY_SHOW'])) {
                $aParameters .= "&ONLY_SHOW=".$_REQUEST['ONLY_SHOW'];
            }

            $sortIcon = '';
            if ($fieldName) {
                $sortIcon = '<span class="filter-icon">&#9650;</span>';
            }
            if ($order == $this->tableFields['arrayVelden'][$key] || $orderAlt == $this->tableFields['arrayVelden'][$key]) {
                $sortIcon = '<span class="filter-icon active">&#9650;</span>';
                if ($orderDir == 'DESC') {
                    $sortIcon = '<span class="filter-icon active">&#9660;</span>';
                }
            }

            $onClickUrl = "$urlQuery&tableid=$this->table&PAGNR=$currentPage&SORTEEROP={$this->tableFields['arrayVelden'][$key]}$aParameters";
            $title = trim("$fieldName $sortIcon");

            if (substr($this->tableFields['arrayVelden'][$key], 0, 4) != 'SKIP') {
                $content .= '<th><a onClick="getPageContent2(\'' . $onClickUrl . '\', \'contentTable_' . $this->table . '\', searchReq2);">' . $title . '</a></th>';
            }

            $SortOrder[$key] = $orderDir;
        }

        $content .= '</tr>';

//Toon resultaten
        $sectionChange = [];
        foreach ($group as $field) {
            $sectionChange[$field] = '';
        }

        $teller = 0;
        foreach ($this->sqlQuerySearch() as $queryPart) {
            $whereString .= $queryPart[0];
            if (isset($queryPart[1])) {
                foreach ($queryPart[1] as $key => $value) {
                    if($key == "entity_pipelinePIPELINE_TYPE_ID" && strpos($value, ',') !== false) {
                        continue;
                    }
                    $query->bindValue($key, $value);
                }
            }
        }

        $query->execute();
        $specialData = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($specialData as $data) {
            $row = $data;
            $twistregel = false;
            foreach ($group as $field) {
                if ($row[$field] != $sectionChange[$field]) {
                    $sectionChange[$field] = $row[$field];
                    $twistregel = true;
                }
            }
            $tekst = 'Group';
            if ($twistregel) {
                $group = [];
                foreach ($group as $field) {
                    if ($row[$field]) {
                        $group[] = $row[$field];
                    }
                }
                if (count($group)) {
                    $tekst = implode('/', $group);
                }
                $content .= '<tr><td>twistie</td><td>' . $tekst . '</td><td colspan="6"></td></tr>';
            }

            if(isset($_REQUEST['SITE']) && ($_REQUEST['SITE'] != "entity_addressbook" || $_REQUEST['SITE'] != "entity_addressbook_ajax")) {
                //Check of bedrijven verwijderd zijn.
                $qCheck = $pdo->prepare('SELECT ID FROM bedrijf WHERE ID = :bedrijfsid AND (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED IS NULL) LIMIT 1');
                if (isset($data['COMPANY_ID'])) {
                    $data['BEDRIJFSID'] = $data['COMPANY_ID'];
                }
                $qCheck->bindValue('bedrijfsid', $data['BEDRIJFSID']);
                // echo $data['BEDRIJFSID']; exit;
                $qCheck->execute();
                $noCheckNeeded = false;
            }
            else {
                $noCheckNeeded = true;
            }


            if ($noCheckNeeded || $qCheck->rowCount() > 0) {
                $bewerkScriptTr = '';
                $bewerkScript = '';
                $tablerow = '';
                $tablecells = [];


//                if (!isset($_REQUEST["PRINT"]) && !isset($_REQUEST["EXPORTEXCEL"])) {
//
//                    if (substr($links['edit'], 0, 10) == 'javascript') {
//                        $bewerkScriptTr = ' style="" class="table-border cursor" ';
//                        $bewerkScript = 'onClick="' . str_replace('-ID-', ($data['ID'] * 1), $links['edit']) . '"';
//                        $tablerow = '<tr data-dk-bedrijfsid="1-{' . $data['BEDRIJFSID'] . '}" ' . $bewerkScriptTr . '>';
//                    } elseif ($links['edit'] != '') {
//                        $bewerkScriptTr = ' style="" class="table-border cursor" ';
//                        $bewerkScript = 'onClick="window.location.href=\'' . str_replace('-ID-', ($data['ID'] * 1),
//                                $links['edit']) . '\';"';
//                        $tablerow = '<tr data-dk-bedrijfsid="2-{' . $data['BEDRIJFSID'] . '}" ' . $bewerkScriptTr . '>';
//                    } else {
//                        $tablerow = '<tr data-dk-bedrijfsid="3-' . $data['BEDRIJFSID'] . '">';
//                    }
//                }

                if ($multiSelect) {
                    $tablecells[0][] = '<td><input type="checkbox" class="multi-row-select" name="row_select[]" value="' . $data['ID'] . '" data-filename="' . $data['NAAM'] . '"></td>';
                }

                foreach ($this->tableFields['arrayVelden'] as $key => $field) {
                    $type = $this->tableFields['arrayVeldType'][$key];
                    $fieldAll = Helper::arrVal($this->tableFields['fields'], $key);
                    $prefix = $fieldAll ? Helper::arrVal($fieldAll, 'prefix') : null;
                    $suffix = $fieldAll ? Helper::arrVal($fieldAll, 'suffix') : null;

                    if ($type === 'secondrow') {
                        if (str_starts_with($key, 'SKIP_')) {
                            $key = substr(strtoupper($key), strlen('SKIP_'));
                        }
                    }

                    if (str_starts_with($key, 'SKIP_')) {
                        continue;
                    }

                    $style = "";
                    if ($this->structure == "entity_pipeline" && $this->tableFields['arrayVeldType'][$key] == "date" && strtotime($data[$field]) != "" && strtotime($data[$field]) < strtotime(date('Y-m-d H:i:s'))) {
                        $style = "style=\"background-color: #EAEAEA;\"";
                    }

                    $td = '<td ' . $style . '><a href="' . str_replace('-ID-', ($data['ID'] * 1),
                            $links['edit']) . '">';
//                    $td .= $prefix ? $prefix . '&nbsp;' : '';
//                    $td2 = $suffix ? '&nbsp;'.$suffix : '';
                    $td2 = '</a></td>';
                    switch ($this->tableFields['arrayVeldType'][$key]) {
                        case 'pivot':
                            $fields = explode('|', $field);
                            if (count($fields) == 4) {
                                $pivot_results = getPivotValue($data['ID'], $this->table, $fields[1], $fields[2],
                                    $fields[3]);
                                $single_result = '';
                                if (is_array($pivot_results)) {
                                    if (isset($pivot_results[$fields[3]])) {
                                        $single_result = $pivot_results[$fields[3]];
                                    }
                                } else {
                                    $single_result = $pivot_results;
                                }

                                if ($single_result || $single_result === 0 || $single_result === '0') {
                                    $single_result = $prefix . $single_result . $suffix;
                                }
                                if (!isset($single_result) || $single_result == "") {
                                    $single_result = "-";
                                }
                                $tablecells[0][] = $td . lb($single_result) . $td2;
                            }
                            elseif(count($fields == 7)) {
                                $pivot_results = getPivotValueFromLinkTable($data['ID'], $fields[5], $fields[4], $fields[3], $fields[4], $fields[2]);
                                $single_result = '';
                                if (is_array($pivot_results)) {
                                    foreach($pivot_results as $result) {
                                        $single_result .= implode("", $result).", ";
                                    }
                                    $single_result = substr($single_result, 0, -2);

                                } else {
                                    $single_result = '';
                                }

                                if (!isset($single_result) || $single_result == "") {
                                    $single_result = "-";
                                }
                                $tablecells[0][] = $td . $single_result . $td2;
                            }
                            break;
                        case 'field':
                            if (isset($data[$field])) {
                                $value = verkort(lb($data[$field]), 70);
                                if ($value || $value === 0 || $value === '0') {
                                    $value = $this->addDecimal($key, $value);
                                    $value = $prefix . $value . $suffix;
                                }
                                $value = strip_tags(html_entity_decode($value));
                                if($key == "ADVISOR") {
                                    if($value == "1") {
                                        $value = "Advisor";
                                    }
                                    elseif($value == "0") {
                                        $value = "Target";
                                    }
                                    else {
                                        $value = "-";
                                    }
                                }
                                $tablecells[0][] = $td . $value . $td2;
                            } else {
                                $tablecells[0][] = $td . "-" . $td2;
                            }
                            break;
                        case 'date':
                            if (isset($data[$field])) {
                                $tablecells[0][] = $td . date('d-m-Y', strtotime($data[$field])) . $td2;
                            } else {
                                $tablecells[0][] = $td . "-" . $td2;
                            }
                            break;
                        case 'bool':
                            if (isset($data[$field])) {
                                if ($data[$field] === '0') {
                                    $data[$field] = 'No';
                                } elseif ($data[$field] === '1') {
                                    $data[$field] = 'Yes';
                                }
                                $tablecells[0][] = $td . verkort($data[$field], 70) . $td2;
                            } else {
                                $tablecells[0][] = '<td ' . $bewerkScript . '></td>';
                            }
                            break;
                        case 'datum':
                        case 'datumleeg':
                            $tablecells[0][] = $td . datumconversiePresentatieKort($data[$field]) . $td2;
                            break;
                        case 'secondrow':
                            $tablecells[1][] = '<td ' . $bewerkScript . ' colspan="100%" style="padding-top: 0;">' . lb($data[$field]) . $td2;
                            break;
                        default:
                            $tablecells[0][] = $td . lb($data[$field]) . $td2;
                            break;
                    }
                }

                $content .= '<tr style="color: black;" class="cursor">' . implode('', $tablecells[0]) . '</tr>';

                if (isset($tablecells[1])) {
                    $content .= '<tr style="color: #777;">' . html_entity_decode(implode('', $tablecells[1])) . '</tr>';
                }

                // reset $tablerow for next iteration
                $tablerow = '';
                // reset $row for next iteration
                $row = '';
                $teller++;
            }
        }

        if ($teller == 0) {
            $content .= '<tr><td colspan="' . count($this->tableFields['arrayVelden']) . '"><em>No results</em></td><tr>';
        }

        if($_REQUEST['SITE'] == "entity_favorites") {
            $totalRevenue   = 0;
            $totalEBITDA    = 0;
            $totalHeadcount = 0;
            $totalValuation = 0;

            foreach ($specialData as $data) {
                $totalRevenue   += (float)$data["REVENUE"];
                $totalEBITDA    += (float)$data["EBITDA"];
                $totalHeadcount += (int)$data["HEADCOUNT"];
                $totalValuation += (float)$data["VALUATION"];
//                dpm($data);
            }
            $content .= "<tr style='background-color: EAEAEA; color: black; font-weight: bold;'>
                            <td>Total: </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&euro;".number_format($totalRevenue, 1, ",", ".")."M</td>
                            <td>&euro;".number_format($totalEBITDA, 1, ",", ".")."M</td>
                            <td>".number_format($totalHeadcount, 0, ",", ".")."</td>
                            <td>&euro;".number_format($totalValuation, 1, ",", ".")."M</td>
                         </tr>";
        }

        $content .= '</table>';

        $excelUrl = str_replace('_ajax', '', $urlQuery);

        if ($totaalAantalRegels > 0) {

            $url = $urlQuery;
            if (isset($_REQUEST['SORTEEROP']) && $_REQUEST['SORTEEROP']) {
                $url .= '&SORTEEROP=' . $_REQUEST['SORTEEROP'];
                if (isset($_REQUEST['SORTORDER']) && $_REQUEST['SORTORDER']) {
                    $url .= '&SORTORDER=' . $_REQUEST['SORTORDER'];
                }
            }
            if ($paginate) {
                $content .= '<br/><br/>' . paginate($url, $totaalAantalRegels, $itemsPerPage, $currentPage,
                        ['ajax' => true, 'table' => $this->table]);
            }
            if($_REQUEST['SITE'] != "entity_my_pipeline") {
                $content .= '<br /> <a class="button" onClick="window.open(\'' . $excelUrl . '&PAGNR=$currentPage&EXPORTEXCEL=true\');">Open in excel</a>';
            }

            $content .= '<br/><br/><br/>' . $totaalAantalRegels . ' results found. ';

        } else {
            $content .= '<br><br>No results found.';
        }

        $content .= '</div>';

        return $content;
    }

    public function urlQueryBase()
    {
        $BID = isset($_REQUEST['BID']) ? '&BID=' . ps($_REQUEST['BID'], 'nr') : '';
        $PID = isset($_REQUEST['PID']) ? '&PID=' . ps($_REQUEST['PID'], 'nr') : '';
        $MENUID = isset($_REQUEST['MENUID']) ? '&MENUID=' . ps($_REQUEST['MENUID'], 'nr') : '';
        $ID = isset($_REQUEST['ID']) ? '&ID=' . ps($_REQUEST['ID'], 'nr') : '';
        $TABEL = isset($_REQUEST['TABEL']) ? '&TABEL=' . ps($_REQUEST['TABEL'], 'tabel') : '';

        $urlQuery = 'content.php?SITE=' . str_replace('_ajax', '',
                $_REQUEST['SITE']) . '_ajax' . $BID . $PID . $MENUID . $ID . $TABEL;

        if (isset($_REQUEST['SHOWNEW'])) {
            $urlQuery .= '&SHOWNEW=' . $_REQUEST['SHOWNEW'];
        }
        if (isset($_REQUEST['SHOWOPEN'])) {
            $urlQuery .= '&SHOWOPEN=' . $_REQUEST['SHOWOPEN'];
        }
        if (isset($_REQUEST['SHOWCLOSED'])) {
            $urlQuery .= '&SHOWCLOSED=' . $_REQUEST['SHOWCLOSED'];
        }

        return $urlQuery;
    }

    public function searchFieldsConverter($searchFields)
    {
        $return = '';
        if (!is_array($searchFields) || !count($searchFields)) {
            return false;
        }

        foreach ($searchFields as $value) {
            if (!is_string($value)) {
                continue;
            }
            switch ($value) {
                case 'ENTITY':
                case 'ENTITY_TABLE':
                    $showField = 'SRCH_BEDRIJFSNAAM';
                    $searchVeld = 'SRCH_BEDRIJFSID';
                    break;
                case 'UPDATEDBY':
                    $showField = 'SRCH_ACHTERNAAM';
                    $searchVeld = 'SRCH_PERSONEELSLID';
                    break;
                case 'COUNTRY':
                case 'COUNTRY_TABLE':
                    $showField = 'SRCH_LAND';
                    $searchVeld = 'SRCH_LAND';
                    break;
                case 'YEAR':
                    $showField = 'SRCH_JAAR';
                    $searchVeld = 'SRCH_YEAR';
                    break;
                default:
                    $showField = 'SRCH_' . $value;
                    $searchVeld = 'SRCH_' . $value;
                    break;
            }
            $return .= "&$searchVeld=x'+fixMultiSelect($('#$showField').val())+'";
        }

        return $return;
    }

    protected function sqlQuerySearch()
    {
        global $pdo;

        $sql_toevoeging = [];
        $needle = 'SRCH_';
        $whitelist = [
            'entities' => [
                'name',
                'headline_action',
                'headline_comment',
                'shareholders',
                'address',
                'accreditation',
                'description',
                'rate_comment'
            ]
        ];
        $blacklist = [
            'SRCH_SEARCH',
        ];
        foreach ($_REQUEST as $key => $value) {
            if (!str_starts_with($key,
                    $needle) || $value === '' || $value === 'x' || $value === 'xnull' || in_array($key, $blacklist)) {
                continue;
            }

            $key = substr(strtoupper($key), strlen($needle));
            $value = htmlspecialchars_decode($value);
            $value = substr($value, strlen('x'));
            if (preg_match('/^[0-9]*([,][0-9]*)?$/', $value)) {
            }
            $toevoeging = [];
            if (str_starts_with($key, 'PIVOT_')) {
                foreach ($this->tableFields['fields'] as $originalId => $originalValue) {
                    if (Helper::arrVal($originalValue, 'searchable') === $key) {
//                        dpm($originalId);
//                        dpm($originalValue);
                        $parts = explode('|', $originalId);
                        //dpm($parts, true);
                        $partsPivot = explode('|', $originalValue["pivot"]["field"]);
                        if ($originalValue["pivot"]["field"]) {
                            if(strpos($value, '|||') !== false) {
                                $value = str_replace("|||", ",", $value);
                                $toevoeging = [
                                    " AND `$partsPivot[0]`.`$parts[1]` IN($value) ",
                                    [$partsPivot[0] . $parts[1] => $value],
                                    "JOIN `$partsPivot[0]` ON `$partsPivot[0]`.`$partsPivot[1]`"
                                ];
                            }
                            else {
                                $toevoeging = [
                                    " AND `$partsPivot[0]`.`$parts[1]` = :$partsPivot[0]$parts[1] ",
                                    [$partsPivot[0] . $parts[1] => $value],
                                    "JOIN `$partsPivot[0]` ON `$partsPivot[0]`.`$partsPivot[1]`"
                                ];
                            }
//                            dpm($toevoeging);
                        }

                        break;
                    }
                }
            }
            if (!$toevoeging) {
                if(strpos($value, '|||') !== false) {
                    $multiSelectArray = explode('|||', $value);
                    $query = " AND (";
                    $values = [];
                    foreach($multiSelectArray as $i => $select) {
                        if ($i) {
                            $query .= "OR ";
                        }
                        $query .= "$this->table.$key = :$key$i ";
                        $values[$key.$i] = $select;
                    }
                    $query .= ") ";
//                    die($query);
                    $toevoeging = [
                        $query,
                        $values
                    ];
//                    dpm($value);
                }
                else {
                    $toevoeging = [
                        " AND $this->table.$key = :$key ",
                        [$key => $value]
                    ];
                }
            }
            $sql_toevoeging[] = $toevoeging;
        }
        if (isset($_REQUEST['SRCH_SEARCH'])) {
            $ZOEKVELD = 'NAAM';
            $needle = substr($_REQUEST['SRCH_SEARCH'], strlen('x'));
            $columns = Helper::arrVal($whitelist, $this->table, false);
            if ($needle && $columns) {
                $sql_toevoeging[] = [
                    ' AND LOWER(CONCAT_WS(' . strtoupper(implode(',', $columns)) . ')) LIKE LOWER("%' . $needle . '%")'
                ];
            }
        }
        return $sql_toevoeging;
    }

    protected function getTableFilters($hideFilters = false)
    {
        global $pdo;
        $content = '';

        if (!Helper::arrVal($this->tableFields, 'arrayZoekvelden')) {
            return '';
        }

//        dpm($this->tableFields, true);
        $url = oft_tabel_querystring();

        // Exceptions
        $url .= $this->searchFieldsConverter($this->tableFields['arrayZoekvelden']);

        //Toon zoekfunctie
        if($hideFilters) {$content .= '<div class="oft2_page_header" style="display:none">'; }
        else { $content .= '<div class="oft2_page_header">';}
        $content .= '<div class="oft2_page_centering">';
        foreach ($this->tableFields['arrayZoekvelden'] as $key => $field) {
            $fieldOrigin = Helper::arrVal($this->tableFields['fields'], $key, []);
            if (str_starts_with($key, 'SKIP_')) {
                $key = substr(strtoupper($key), strlen('SKIP_'));
            }
            $element = '';
            $children = false;
            if (is_array($fieldOrigin) && $fieldOrigin) {
                $type = Helper::arrVal($fieldOrigin, 'type', 'field');
                switch ($type) {
                    case 'bool':
                        $childrenPrepare = $pdo->prepare("SELECT $field FROM $this->table WHERE (!DELETED AND DELETED = 'Nee') GROUP BY $field ORDER BY $field");
                        $childrenPrepare->execute();
                        $children = $childrenPrepare->fetchAll(PDO::FETCH_GROUP);
                        foreach ($children as $child => $name) {
                            $children[$child] = $child ? 'Yes' : 'No';
                        }
                        $element = Filter::select($field, $children, $url, $this->table);
                        break;
                    case 'pivot':
                        $newKey = Helper::arrVal($fieldOrigin, 'filtername', $key);
                        $parts = explode('|', $newKey); // 0 = table & 1 = column

                        $children = [];

                        if (count($parts) == 2) {

                            // this->table      field        table       column
                            // entities         COUNTRY_ID   countries   NAME
                            // entities         STAGE_ID     stages      NAME

                            $table = strtolower($parts[0]);
                            $column = strtoupper($parts[1]);
                            $childrenStatement = "SELECT $this->table.$field, `$table`.`$column`
                                                    FROM $this->table
                                                    INNER JOIN `$table` ON `$table`.ID = $this->table.$field
                                                    WHERE (!$this->table.DELETED AND $this->table.DELETED = 'Nee')
                                                    GROUP BY $this->table.$field
                                                    ORDER BY `$table`.`$column`";
                            $childrenPrepare = $pdo->prepare($childrenStatement);
                            $childrenPrepare->execute();
                            $children = $childrenPrepare->fetchAll(PDO::FETCH_KEY_PAIR);

                        } elseif (count($parts) == 4) {
                            $table = strtolower($parts[0]);
                            $column = strtoupper($parts[1]);
                            $tableJoin = strtolower($parts[2]);
                            $columnJoin = strtoupper($parts[3]);

                            if($columnJoin == "N") {
                                $columnJoin = "NAME";
                                $parts[3] = "NAME";
                            }
                            $partsPivot = explode('|', $fieldOrigin["pivot"]["field"]);
                            if (count($partsPivot) != 2) {
                                continue;
                            }
                            // entity_tags|TAG_ID|tags|NAME
                            // entity_tags|ENTITY_ID
                            $pivotTable = strtolower($partsPivot[0]);
                            $pivotColumn = strtoupper($partsPivot[1]);
                            $childrenStatement = "SELECT `$table`.`$column`, `$tableJoin`.`$columnJoin`
                                                  FROM `$table`
                                                  INNER JOIN $this->table ON $this->table.ID = `$pivotTable`.`$pivotColumn`
                                                  INNER JOIN `$tableJoin` ON `$tableJoin`.ID = `$table`.`$column`
                                                  WHERE (!$this->table.DELETED AND $this->table.DELETED = 'Nee')
                                                  GROUP BY `$table`.`$column`
                                                  ORDER BY `$tableJoin`.`$columnJoin`";
                            $childrenPrepare = $pdo->prepare($childrenStatement);
                            $childrenPrepare->execute();
                            $children = $childrenPrepare->fetchAll(PDO::FETCH_KEY_PAIR);
                        }
                        $element = Filter::select($field, $children, $url, $this->table);
                        break;
                    case 'date':
                        $element = '<input type="date" class="field" value="" id="SRCH_' . $field . '" name="SRCH_' . $field . '" onchange="getPageContent2(\'' . $url . '\', \'contentTable_' . $this->table . '\', searchReq2);">';
                        break;
                    case 'field':
                    default:
                        $childrenPrepare = $pdo->prepare("SELECT $field FROM $this->table WHERE (!DELETED AND DELETED = 'Nee') GROUP BY $field ORDER BY $field");
                        $childrenPrepare->execute();
                        $children = $childrenPrepare->fetchAll(PDO::FETCH_GROUP);
                        $element = Filter::select($field, $children, $url, $this->table);
                        break;
                }
            } elseif ($field == 'SEARCH') {
                $element = '<input type="text" class="field" value="" id="SRCH_SEARCH" name="SRCH_SEARCH" onkeyup="getPageContent2(\'' . $url . '\', \'contentTable_' . $this->table . '\', searchReq2);" />';
            }

            // Don't show (redudant) element if only one outcome is possible
            // $redudant = $children && count($children) === 1;
            $redudant = false;

            if($field == 'SEARCH') {
                // hide search input field from overview
                $redudant = true;
            }

            $title = Helper::arrVal($fieldOrigin, 'title', ucfirst(strtolower($field)), true);
            $content .= '<div class="oft2_combo_box' . ($redudant ? ' hidden' : '') . '"><label for="SRCH_' . $field . '">' . $title . '</label>' . $element . '</div>';
        }

        $content .= '</div></div>';


        return $content;
    }

    protected function addDecimal($key, $value)
    {
        $decimals = [
//            'synergies',
            'revenue',
            'ebitda',
            'deal_size',
            'valuation',
            'ev_ebitda_multiple',
            'upfront_price',
            'earnout'
        ];
        if (in_array(strtolower($key), $decimals)) {
            $value = number_format($value, 1, '.', '');
        }
        return $value;
    }
}
