<?php

namespace Controllers\Helpers;


/**
 * Class Helper
 *
 * @package components
 */
class Helper
{
    public $company,
        $user;

    public function __construct($user = 0, $company = 0)
    {
        $this->company = $company;
        $this->user = $user;
    }

    public static function arrVal($array, $key, $fallback = '', $strict = false)
    {
        // if array isn't an array OR key is empty
        if (!is_array($array) || (!$key && $key !== 0)) {
            return false;
        }

        // if array is an empty array OR key doesn't exist OR key value is empty and not allowed to be empty. If value is string and false after trim
        if (!$array || !isset($array[$key]) || ($strict && (!$array[$key] || (is_string($array[$key]) && !trim($array[$key]))))) {
            return $fallback;
        }

        // Return value of array key
        return $array[$key];
    }

    public static function reqVal($key, $fallback = '', $strict = false)
    {
        return self::arrVal($_REQUEST, $key, $fallback, $strict);
    }

    public static function postVal($key, $fallback = '', $strict = false)
    {
        return self::arrVal($_POST, $key, $fallback, $strict);
    }

    public static function getVal($key, $fallback = '', $strict = false)
    {
        return self::arrVal($_GET, $key, $fallback, $strict);
    }

    public static function snake2camel($str)
    {
        return str_replace('_', '', lcfirst(ucwords($str, '_')));
    }

    public static function camel2snake($str)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $str));
    }
}
