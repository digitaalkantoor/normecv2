<?php

namespace Controllers\Helpers;


/**
 * interface Page
 *
 * @package components
 */
interface Page
{

    public function __construct($user = 0, $company = 0, $options = []);

    /**
     * GET overview table with all files and filters. Use AJAX request for 'clean' feed.
     */
    public function index();

    /**
     * GET create form for adding object(s).
     */
    public function create();

    /**
     *
     * POST files from create form into the database.
     */
    public function store();

    /**
     *
     * PATCH existing file from edit form into the database.
     */
    public function update();

    /**
     *
     * GET edit form for editing a single object.
     */
    public function edit();
}