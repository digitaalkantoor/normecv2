<?php

namespace Controllers\Helpers;
//
//include_once 'Helper.class.php';
//
//use Controllers\Helpers\Helper as Helper;
//
//
/**
 * Class Form
 *
 * @package components
 */
class Form
{
//    public $company,
//        $user;
//
//
    public function __construct($user = 0, $company = 0)
    {
//        $this->company = $company;
//        $this->user = $user;
    }
//
//
//    public function createBlock($elements, $title = '', $key = '', $options = [])
//    {
//        global $pdo;
//        $htmlBlock = '';
//        if ($title) {
//            $htmlBlock .= $this->createFormHeader($title, Helper::arrVal($options, 'header_class'));
//        }
//
//        // typeArray is a custom solution for having a repeatblock in the forms
//        $typeArray = (Helper::arrVal($options, 'type') === 'array');
//
//        $values = Helper::arrVal($options, 'values');
//
//        if ($elements) {
//            foreach ($elements as $name => $elementOptions) {
//
//                if (!is_string($name) || !is_array($elementOptions)) {
//                    return false;
//                }
//
//                $tmpName = $key . $name;
//
//                if ($typeArray) {
//                    $parts = explode('[', $key);
//                    $lastPart = end($parts);
//                    $dbName = $lastPart . $name;
//                    $name = $tmpName . ']';
//                } else {
//                    $name = $tmpName;
//                    $dbName = $tmpName;
//                }
//                $dbName = strtoupper($dbName);
//
//
//                // Get child info from join table
//                $children = arrVal($elementOptions, 'children');
//                if (is_string($children)) {
//                    // Get variables from other table
//                    $childs = explode('.', $children);
//                    if (count($childs) === 3 && $childs[0] === 'join') {
//                        $elementOptions['children'] = [];
//                        $table = trim((string)$childs[1]);
//                        $column = trim((string)$childs[2]);
//                        $idOnly = ($column === 'ID');
//                        $fields = ($idOnly) ? 'ID' : 'ID`, `' . $column;
//                        $statement = 'SELECT `' . $fields . '` FROM `' . $table . '` limit 100;';
//                        $pdo->query($statement);
//                        while ($result = $pdo) {
//                            $child['value'] = $result['ID'];
//                            if (!$idOnly) {
//                                $child['text'] = $result[$column];
//                            }
//                            $elementOptions['children'][] = $child;
//                        }
//                        if ($elementOptions['children']) {
//                            $elementOptions['children'][] = ['value' => 0, 'text' => 'Anders...'];
//                        }
//                    }
//                }
//
//
//                // set values for editing existing form data
//                if ($values) {
//                    $type = arrVal($elementOptions, 'type');
//                    switch ($type) {
//                        case 'checkbox':
//                            if (arrVal($values, $dbName)) {
//                                $elementOptions['attr']['checked'] = 'checked';
//                            }
//                            break;
//                        case 'radio':
//                        case 'select':
//                            if (!arrVal($elementOptions, 'children')) {
//                                $elementOptions['children'] = [
//                                    ['value' => 0, 'text' => 'Ja'],
//                                    ['value' => 1, 'text' => 'Nee']
//                                ];
//                            }
//                            if ($savedName = arrVal($values, $dbName)) {
//                                foreach ($elementOptions['children'] as &$child) {
//                                    $child['selected'] = false; // Only one can be set to selected
//                                    if ($child['value'] == $savedName) {
//                                        $child['selected'] = true;
//                                    }
//                                }
//                            }
//                            break;
//                        case 'signature':
//                            if ($value = arrVal($values, 'SIGNATURE_IMAGE')) {
//                                $elementOptions['value'] = $value;
//                            }
//                            break;
//                        default:
//                            if ($value = arrVal($values, $dbName)) {
//                                $elementOptions['value'] = $value;
//                            }
//                            break;
//                    }
//                }
//
//                $formBlock .= createFormElement($name, $elementOptions);
//            }
//        }
//        return $formBlock;
//    }
//
//    public function createFormHeader($title, $class)
//    {
//        return '<tr class="forms forms-table-header ' . (string)$class . '"><th colspan="2"><strong>' . $title . '</strong></th></tr>';
//    }
//}
//
//// Create a form element, the form element is easily created with a lot of optional settings, that have a default value
//function createFormElement($name, $options = [])
//{
//    if (!is_array($options) || !is_string($name)) {
//        return false;
//    }
//    $defaults = [
//        'type' => 'text',
//        'name' => $name,
//        'id' => $name . '-' . time(),
//        'class' => [], // array to avoid doubles
//        'value' => false,
//        'label' => false,
//        'placeholder' => false,
//        'required' => false,
//        'label_for' => true,
//        'label_place' => 'left', // [ top | right | bottom | left ]
//        'wrapper_class' => [],
//        'children' => [], // Optional, used for type=select
//        'multiple' => false, // Optional, used for type=select or type=file
//        'disabled' => false, // Optional
//        'data' => [],
//        'attr' => [],
//    ];
//
//    $default_children = [
//        'value' => null, // Mandatory
//        'text' => '',
//        'selected' => false,
//        'disabled' => false,
//    ];
//
//    // Fix common mistakes (like giving strings instead of arrays)
//    if (isset($options['class']) && is_string($options['class'])) {
//        // Convert string to array
//        $options['class'] = explode(' ', $options['class']);
//    }
//    if (isset($options['wrapper_class']) && is_string($options['wrapper_class'])) {
//        // Convert string to array
//        $options['wrapper_class'] = explode(' ', $options['wrapper_class']);
//    }
//
//    // Add pre-logics
//    if (arrVal($options, 'type')) {
//        if ($options['type'] === 'date') {
//            $defaults['placeholder'] = 'jjjj-mm-dd';
//            $defaults['class'][] = 'forms-date-picker';
//        }
//        if ($options['type'] === 'checkbox') {
//            $defaults['label_place'] = 'right';
//            $defaults['value'] = true;
//        }
//        if (in_array($options['type'], ['button', 'checkbox', 'radio'])) {
//            $defaults['label'] = true;
//        }
//        if ($options['type'] === 'euro') {
//            $defaults['placeholder'] = '0,00';
//            $defaults['attr']['step'] = '0.0001';
//        }
//    }
//
//
//    // Overwrite the defaults options
//    $option = array_merge($defaults, $options);
//    if (is_array(arrVal($options, 'children'))) {
//        $c = 0;
//        foreach ($option['children'] as &$select_option) {
//            if (!is_array($select_option)) {
//                continue;
//            }
//            $select_option = array_merge($default_children, $select_option);
//            if (!$select_option['value'] && $select_option['value'] !== 0) {
//                $select_option['value'] = $c;
//            }
//            if (!$select_option['text']) {
//                $select_option['text'] = $select_option['value'];
//            }
//            $c++;
//        }
//    }
//    $option['id'] = preg_replace("/[^a-zA-Z0-9]+/", "-", $option['id']); //  remove brackets and stuff
//
//    // Add default class for forms type
//    $option['class'][] = 'forms-type-' . $option['type'];
//
//    // Add required class if field is required
//    if ($option['required'] === true) {
//        array_unshift($option['class'], 'forms-required');
//    }
//
//    // Add custom kvk classes
//    if ($option['type'] === 'kvk_search') {
//        array_push($option['class'], 'forms-kvk-search', 'searchselect', 'ui-autocomplete-input');
//    }
//
//    // Add default field class
//    if (!in_array($option['type'], ['plain_text', 'checkbox', 'radio', 'button', 'submit'])) {
//        array_unshift($option['class'], 'field');
//    }
//    // Add default button class
//    if ($option['type'] === 'button') {
//        $option['class'][] = 'btn-action';
//    }
//    // Add default forms class
//    if ($option['type'] !== 'plain_text') {
//        array_unshift($option['class'], 'forms');
//    }
//    // Add default file class
//    if ($option['type'] === 'file') {
////        $option['class'][] = 'file';
//        $option['name'] .= '[]';
//    }
//
//    $option['wrapper_class'][] = 'forms-element-' . $option['type'];
//
//    // Create label
//    $label = '';
//    if ($option['label'] || $option['required']) {
//        $label_name = is_string($option['label']) ? '<span>' . $option['label'] . '</span>' : '';
//        $label_required = $option['required'] ? '<abbr title="required">*</abbr>' : '';
//        $label = '<label for="' . $option['id'] . '">' . $label_name . '' . '</label>' . $label_required;
//    }
//
//    if ($option['data']) {
//        foreach ($option['data'] as $key => $value) {
//            $option['attr']['data-' . $key] = $value;
//        }
//    }
//
//    // Create attributes
//    $attr = ' name="' . $option['name'] . '"';
//    $attr .= ' id="' . $option['id'] . '"';
//    $attr .= ' class="' . implode(' ', $option['class']) . '"';
//    $attr .= $option['value'] ? ' value="' . $option['value'] . '"' : '';
//    $attr .= $option['placeholder'] ? ' placeholder="' . $option['placeholder'] . '"' : '';
//    $attr .= $option['disabled'] ? ' disabled' : '';
//    $attr .= $option['multiple'] ? ' multiple' : '';
//    $attr .= $option['required'] ? ' required' : '';
//    if (is_array($option['attr'])) {
//        foreach ($option['attr'] as $key => $value) {
//            if (is_numeric($key)) {
//                if (is_string($value)) { //only allow string to be added as attribute item
//                    $attr .= ' ' . $value;
//                }
//            } else {
//                $attr .= ' ' . $key . '="' . $value . '"';
//            }
//        }
//    }
//
//    // Create element
//    $element = '';
//    switch ($option['type']) {
//        case 'date':
//            // Todo: hier dat date picker veld gebruiken
//            // en font hetzelfde als de andere velden
//            // testen in safari
//            $element = '<div class="input-append">';
//            $element .= '<input type="text"' . $attr . '/>';
//            $element .= '<span class="add-on forms" data-for="' . $option['id'] . '"><img src="./images/calendar_13.png"></span>';
//            $element .= '</div>';
//            break;
//        case 'euro';
//            $element = '<div class="input-append">';
//            $element .= '<input type="number"' . $attr . '/>';
//            $element .= '<span class="add-on forms" data-for="' . $option['id'] . '">&euro;</span>';
//            $element .= '</div>';
//            break;
//        case 'file':
////            $element = '<div class="uploadVeld">';
////            $element .= '<input type="' . $option['type'] . '"' . $attr . '>';
////            $element .= '<input class="nepVeld field">';
////            $element .= '</div>';
//            $element = '<div class="input-append">';
//            $element .= '<input type="' . $option['type'] . '"' . $attr . '>';
//            $element .= '<label class="add-on forms buttonBewerken" for="' . $option['id'] . '"></label>
//';
//            $element .= '</div>';
//            break;
//        case 'kvk_search':
//            $element = '<div class="input-append">';
//            $element .= '<input type="text" ' . $attr . '/>';
//            $element .= '<span class="add-on forms" data-for="' . $option['id'] . '"><img src="./images/icon_kvk.png"></span>';
//            $element .= '</div>';
//            break;
//        case 'plain_text';
//            $element = '<p id="' . $option['id'] . '" class="' . implode(' ',
//                    $option['class']) . '">' . $option['value'] . '</p>';
//            break;
//        case 'select':
//            $element = '<select' . $attr . '>';
//            $element_children = '';
//            $element_selected = false;
//            foreach ($option['children'] as $child) {
//                $child_attr = ' value="' . $child['value'] . '"';
//                $child_attr .= $child['disabled'] ? ' disabled' : '';
//                $child_attr .= $child['selected'] ? ' selected' : '';
//                $element_selected = ($child['selected'] || $element_selected);
//                $element_children .= '<option' . $child_attr . '>' . $child['text'] . '</option>';
//            }
//            if (!$element_selected || !$option['required']) {
//                $element .= '<option selected value="" ' . ($option['required'] ? 'style="display:none; disabled" ' : '') . '></option>';
//            }
//            $element .= $element_children;
//            $element .= '</select>';
//            break;
//        case 'radio':
//            $element = '';
//            $c = 0;
//            foreach ($option['children'] as $child) {
//                $id = $option['id'] . '-' . $c;
//                $child_attr = ' type="' . $option['type'] . '"';
//                if ($option['attr']) {
//                    foreach ($option['attr'] as $key => $value) {
//                        if (is_numeric($key)) {
//                            $child_attr .= ' ' . $value;
//                        } else {
//                            $child_attr .= ' ' . $key . '="' . $value . '"';
//                        }
//                    }
//                }
//                if (arrVal($child, 'attr')) {
//                    foreach ($child['attr'] as $key => $value) {
//                        if (is_numeric($key)) {
//                            $child_attr .= ' ' . $value;
//                        } else {
//                            $child_attr .= ' ' . $key . '="' . $value . '"';
//                        }
//                    }
//                }
//                $child_attr .= ' name="' . $option['name'] . '"';
//                $child_attr .= ' value="' . $child['value'] . '"';
//                $child_attr .= ' id="' . $id . '"';
//                $child_attr .= ' class="' . implode(' ', $option['class']) . '"';
//                $child_attr .= $child['disabled'] ? ' disabled' : '';
//                $child_attr .= $child['selected'] ? ' checked' : '';
////                if ($child['disabled']) {
////                    $element .= '<input type="hidden" name="'.$option['name'].'" id="'.$id.'-hidden" value="' . $option['value'] . '" />';
////                }
//                $element .= ' <input' . $child_attr . '><label for="' . $id . '"> ' . $child['text'] . '</label>';
//                $c++;
//            }
//            break;
//        case 'signature':
//            if ($option['disabled']) { // don't show signing field
//                if ($option['value']) { // show image when already signed
//                    $element = '<img class="forms forms-signature-image" src="data:' . $option['value'] . '" alt="signature" />';
//                    $element .= '<input type="hidden" name="signature_image" value="' . $option['value'] . '" />';
//                    $element .= '<input type="hidden" name="AKKOORD" value="Ja" />';
//                }
//                break;
//            }
//            $element = '<div class="forms forms-signature signatureField">';
//            $element .= '<input type="hidden" name="signature_image" id="signature-image" />';
//            $element .= '<input type="hidden" name="AKKOORD" id="signature-state"/>';
//            $element .= '<div id="signature-input"></div>';
//            $element .= '<div class="clearCanvas"><a href="javascript:void()" onclick="clearCanvas()">&#10006;</a></div>';
//            $element .= '</div>';
//            $element .= '<br /><input type="button" name="signature_agree" id="signature_agree" value="Akkoord" class="btn-action forms-agree"/>';
//            break;
//        case 'textarea':
//            $element = '<textarea' . $attr . '>' . $option['value'] . '</textarea>';
//            break;
//        case 'iban':
//            $attr .= ' pattern="\s*[a-zA-Z]{2}\d{2}\s?[a-zA-Z]{4}\s?\d{8}\s*"';
//            $element = '<input type="text" ' . $attr . '/>';
//            break;
//        default:
//            $element = '<input type="' . $option['type'] . '" ' . $attr . '/>';
//            break;
//    }
//    if ($option['disabled'] && $option['value']) {
//        $element .= '<input type="hidden" name="' . $option['name'] . '" id="' . $option['id'] . '-hidden" value="' . $option['value'] . '" />';
//    }
//
//    // arrange form element
//    $form_element = '<td colspan="2">' . $element . '</td>';
//    if ($label) {
//        switch ($option['label_place']) {
//            case 'top':
//                $form_element = $label . $element;
//                break;
//            case 'left':
//                $form_element = '<td class="forms forms-label">' . $label . '</td><td class="forms forms-element">' . $element . '</td>';
//                break;
//            case 'right':
//                $form_element = '<td class="forms forms-element">' . $element . '</td><td class="forms forms-label">' . $label . '</td>';
//                break;
//            case 'bottom':
//                $form_element = $element . $label;
//                break;
//            default:
//                // Label placement direction not known.
//                return false;
//        }
//    }
//
//    // Add wrapper to form element
//    $wrapper_attr = '';
//    if ($option['wrapper_class']) {
//        $wrapper_attr = ' class="' . implode(' ', $option['wrapper_class']) . '" ';
//    }
//    if ($label && ($option['label_place'] == 'left' || $option['label_place'] == 'right')) {
//        $return = '<tr' . $wrapper_attr . '>' . $form_element . '</tr>';
//    } else {
//        $return = '<div' . $wrapper_attr . '>' . $form_element . '</div>';
//    }
//    return $return;
//}
//
//// Fastlane to create a form header with form elements
//function createFormBlock($title, $key, $formElements, $formBlockOptions = [])
//{
//    global $db;
//    $formBlock = '';
//    if ($title) {
//
//        $formBlock .= createFormHeader($title, arrVal($formBlockOptions, 'header_class'));
//    }
//
//    // typeArray is a custom solution for having a repeatblock in the forms
//    $typeArray = (arrVal($formBlockOptions, 'type') === 'array');
//
//    $values = arrVal($formBlockOptions, 'values');
//
//    if ($formElements) {
//        foreach ($formElements as $name => $elementOptions) {
//
//            if (!is_string($name) || !is_array($elementOptions)) {
//                return false;
//            }
//
//            $tmpName = $key . $name;
//
//            if ($typeArray) {
//                $parts = explode('[', $key);
//                $lastPart = end($parts);
//                $dbName = $lastPart . $name;
//                $name = $tmpName . ']';
//            } else {
//                $name = $tmpName;
//                $dbName = $tmpName;
//            }
//            $dbName = strtoupper($dbName);
//
//
//            // Get child info from join table
//            $children = arrVal($elementOptions, 'children');
//            if (is_string($children)) {
//                // Get variables from other table
//                $childs = explode('.', $children);
//                if (count($childs) === 3 && $childs[0] === 'join') {
//                    $elementOptions['children'] = [];
//                    $table = trim((string)$childs[1]);
//                    $column = trim((string)$childs[2]);
//                    $idOnly = ($column === 'ID');
//                    $fields = ($idOnly) ? 'ID' : 'ID`, `' . $column;
//                    $statement = 'SELECT `' . $fields . '` FROM `' . $table . '` limit 100;';
//                    $query = db_query($statement, $db);
//                    while ($result = db_fetch_assoc($query)) {
//                        $child['value'] = $result['ID'];
//                        if (!$idOnly) {
//                            $child['text'] = $result[$column];
//                        }
//                        $elementOptions['children'][] = $child;
//                    }
//                    if ($elementOptions['children']) {
//                        $elementOptions['children'][] = ['value' => 0, 'text' => 'Anders...'];
//                    }
//                }
//            }
//
//
//            // set values for editing existing form data
//            if ($values) {
//                $type = arrVal($elementOptions, 'type');
//                switch ($type) {
//                    case 'checkbox':
//                        if (arrVal($values, $dbName)) {
//                            $elementOptions['attr']['checked'] = 'checked';
//                        }
//                        break;
//                    case 'radio':
//                    case 'select':
//                        if (!arrVal($elementOptions, 'children')) {
//                            $elementOptions['children'] = [
//                                ['value' => 0, 'text' => 'Ja'],
//                                ['value' => 1, 'text' => 'Nee']
//                            ];
//                        }
//                        if ($savedName = arrVal($values, $dbName)) {
//                            foreach ($elementOptions['children'] as &$child) {
//                                $child['selected'] = false; // Only one can be set to selected
//                                if ($child['value'] == $savedName) {
//                                    $child['selected'] = true;
//                                }
//                            }
//                        }
//                        break;
//                    case 'signature':
//                        if ($value = arrVal($values, 'SIGNATURE_IMAGE')) {
//                            $elementOptions['value'] = $value;
//                        }
//                        break;
//                    default:
//                        if ($value = arrVal($values, $dbName)) {
//                            $elementOptions['value'] = $value;
//                        }
//                        break;
//                }
//            }
//
//            $formBlock .= createFormElement($name, $elementOptions);
//        }
//    }
//    return $formBlock;
}