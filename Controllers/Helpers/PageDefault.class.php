<?php

namespace Controllers\Helpers;


include_once 'Table.class.php';


/**
 * Class PageDefault
 *
 * @package components
 */
class PageDefault extends Table
{
    public $ajax,
        $class,
        $keyName,
        $title,
        $vars;

    public function __construct($user = 0, $company = 0, $whitelist = [], $options = [])
    {
        parent::__construct();

        foreach ($whitelist as $key => $value) {
            if (isset($options[$key])) {
                $value = $options[$key];
            }
            $this->$key = $value;
        }

        $this->user = $user;
        $this->company = $company;
    }

    public function viewOverview(
        $links,
        $submenuItems,
        $options = [
        ]
    ) {
        $tableContent = $this->getTableContent($links, $options);
        if ($this->ajax) {
            return $tableContent;
        }
        $tableWithHeader = $this->getTableWithHeader($tableContent);

        if(isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "entity_addressbook") {
            $title = "Contacts";
            $submenuItems = '';
        }
        else {
            $title = $this->title;
        }

        return oft_framework_basic($this->user, $this->company, $tableWithHeader, $title, $submenuItems);
    }

    public function viewMyPipeline($links, $submenuItems) {
        $tableContent = $this->getTableContent($links);

        if ($this->ajax) {
            return $tableContent;
        }
        $submenuItems = "";

        $tableWithHeader = $this->getTableWithHeader($tableContent, true);

        $user = getUser(Helper::getVal('PERSONEELSID', (int)getUserID()));

        $name = $user['VOORNAAM'].(isset($user['TUSSENVOEGSEL'])? $user['TUSSENVOEGSEL']:"")." ".$user['ACHTERNAAM'];

        $title = $this->structure == "entity_pipeline" ? "My Pipeline" : $this->title;

        return oft_framework_basic($this->user, $this->company, $tableWithHeader, $title, $submenuItems);
    }

    public function viewMyFavorites($links, $submenuItems) {
        $tableContent = $this->getTableContent($links);

        if ($this->ajax) {
            return $tableContent;
        }
        $tableWithHeader = $this->getTableWithHeader($tableContent, true);

        $user = getUser(Helper::getVal('PERSONEELSID', (int)getUserID()));

        $name = $user['VOORNAAM'].(isset($user['TUSSENVOEGSEL'])? $user['TUSSENVOEGSEL']:"")." ".$user['ACHTERNAAM'];

        $title = $this->structure == "entity_favorites" ? "Top Targets" : $this->title;

        return oft_framework_basic($this->user, $this->company, $tableWithHeader, $title, $submenuItems);
    }

    /**
     * GET show form for viewing object(s).
     */
    public function show()
    {
        $view = oft_add_page(
            $this->user,
            $this->company,
            "./templates/" . $this->class . "_view.html",
            $this->table,
            "View $this->keyName",
            false
        );
        return oft_framework_menu($this->user, $this->company, $view, '', '', '');
    }

    /**
     * GET create form for adding object(s).
     */
    public function create()
    {
        return oft_add_page(
            $this->user,
            $this->company,
            "./templates/" . $this->class . "_create.html",
            $this->table,
            "Add $this->keyName(s)",
            false
        );
    }

    /**
     *
     * GET edit form for editing a single object.
     */
    public function edit()
    {
        return oft_edit_page(
            $this->user,
            $this->company,
            "./templates/" . $this->class . "_edit.html",
            $this->table,
            "Edit file",
            [],
            false
        );
    }

    public function page($content, $title = '')
    {
        $return = '<div class="oft2_main_menu">'
            . '<div class="oft2_page_centering">'
            . oft_menu($this->user, $this->company)
            . '</div>'
            . '</div>';
        if ($title) {
            $return .= '<div class="oft2_second_menu">'
                . '<div class="oft2_page_centering">'
                . '<h1 class="title">' . $title . '</h1>'
                . '</div>'
                . '</div>';
        }
        $return .= '<div class="oft2_main_content">'
            . '<div class="oft2_contentFrame">'
            . $content
            . '</div>'
            . '</div>';
        return $return;
    }

    protected function editable($var, $options = [])
    {
        global $pdo;

        $availableOptions = [
            'key' => null,
            'prefix' => '',
            'suffix' => '',
            'default' => '&nbsp;-&nbsp;',
            'label' => null
        ];

        $options = array_merge($availableOptions, $options);

        // in most cases key equals var
        if (!$options['key']) {
            $options['key'] = $var;
        }

        $key = $options['key'];

        // key must be in snake_case
        if (preg_match('/[A-Z]/', $key)) {
            $key = Helper::camel2snake($key);
        }

        // var must be in lowerCamelCase
        if (strpos($var, '_') !== false) {
            $var = Helper::snake2camel($var);
        }

        $keyValue = $var;

        $var = $this->vars[$var];

//        echo $keyValue;

        if (( $var === null || $var === '') || $var == '') {
            $var = '<i>' . $options['default'] . '</i>';
            if ($options['default'] !== $availableOptions['default']) {
                $var = $options['prefix'] . "-" . $options['suffix'];
            }
        }
        elseif($keyValue == "ebitda" || $keyValue == "revenue" || $keyValue == "valuation" ||  $keyValue == "upfrontPrice" || $keyValue == "evEbitdaMultiple") {
            switch($keyValue) {
                case "ebitda":
                    $element = "EBITDA";
                    break;
                case "revenue":
                    $element = "REVENUE";
                    break;
                case "valuation":
                    $element = "VALUATION";
                    break;
                case "upfrontPrice":
                    $element = "UPFRONT_PRICE";
                    break;
                case "evEbitdaMultiple":
                    $element = "EV_EBITDA_MULTIPLE";
                    break;
                case "growth":
                    $element = "GROWTH";
                    break;
                default:
                    $element = "ID";
            }
            $query = "SELECT ".$element." FROM entities WHERE ID = ".$_REQUEST['ID'];
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            if($result == null || strtolower($result) == "null") {
                $var = "-";
            }
            elseif($result == 0) {
                $var = "0.0M";
                if($keyValue == "evEbitdaMultiple") {
                    $var = $result."x";
                }
                if ($options['default'] !== $availableOptions['default']) {
                    $var = $options['prefix'] . $var . $options['suffix'];
                }
            }
            else {
                if(!contains($result, ',') && !contains($result, '.')) {
                    $result .= ".0";
                }
                $var = $result."M";
                if($keyValue == "evEbitdaMultiple") {
                    $var = $result."x";
                }
                if ($options['default'] !== $availableOptions['default']) {
                    $var = $options['prefix'] . $var . $options['suffix'];
                }
            }
        }
        elseif($var === '0.0' && $keyValue == "ebitda") {
            $var = $var . 'M';
            if ($options['default'] !== $availableOptions['default']) {
                $var = $options['prefix'] . $var . $options['suffix'];
            }
        }
        elseif($keyValue == "cfAdvisor") {
            if(is_numeric($var)) {
//                return $var;
                $queryOne = "
                    SELECT entities.NAME FROM entities
                    JOIN entity_persons ON entity_persons.ENTITY_ID = entities.ID
                    WHERE entity_persons.ID = ".$var;
                $stmtOne = $pdo->prepare($queryOne);
                $stmtOne->execute();
                $var = $stmtOne->fetchColumn(0);
                if($var == "") {
                    $var = "-";
                }
            }

        }
        else {
            if($var == null) {
                $var = "0";
            }
            $var = $options['prefix'] . $var . $options['suffix'];
        }

        $dataAttributes = '';
        foreach ($options as $key => $value) {
            $dataAttributes .= ' data-' . $key . '="' . $value . '"';
        }
        return '<span class="editable' /*. ($var ? '' : ' editable-icon')*/ . '" data-toggle="modal" data-target="#editable-modal" ' . $dataAttributes . '>' . $var . '</span>';
    }
}
