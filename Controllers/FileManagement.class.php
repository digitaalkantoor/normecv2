<?php

namespace Controllers;

// ToDo: Build in an autoloader
include_once './Controllers/Helpers/PageDefault.class.php';
include_once './koppelingen/cloudvps/cloudvps.php';

use Controllers\Helpers\Helper; //Already included
use Controllers\Helpers\PageDefault;
use CloudVPS;


/**
 * Class FileManagement
 *
 * @package components
 */
class FileManagement extends Helpers\PageDefault
{

    /**
     * FileManagement constructor.
     */
    public function __construct($user = 0, $company = 0, $options = [])
    {
        $whitelist = [
            'ajax' => false,
            'popup' => false,
            'class' => 'file_management',
            'keyName' => 'file',
            'table' => 'documentbeheer',
            'structure' => 'file_management',
            'title' => 'Manage files',
        ];

        parent::__construct($user, $company, $whitelist, $options);
    }

    /**
     * GET overview table with all files and filters. Use AJAX request for 'clean' feed.
     */
    public function index()
    {
        $baseLink = "content.php?SITE=file_management";

        $links = array(
            'create' => $baseLink . "_create",
            'edit' => oft_inputform_link_short($baseLink . "_edit" . "&ID=-ID-"),
            'bulk_edit' => $baseLink . "_bulk_edit",
            'delete' => ""
        );

        $submenuItems = oft_inputform_link($links['create'], "New file(s)");
        $submenuItems .= oft_inputform_link($links['bulk_edit'], "Edit selected file(s)");

        return $this->viewOverview($links, $submenuItems, ['multiSelect' => true, 'order' => 'BESTANDSNAAM']);
    }

    /**
     *
     * GET edit form for editing bulk/multiple files at once.
     */
    public function bulk_edit()
    {
        return oft_edit_page($this->user, $this->company, "./templates/file_management_bulk_edit.html", $this->table,
            "Edit file(s)", [], false);
    }

    public function edit_mini()
    {
        return oft_edit_page($this->user, $this->company, "./templates/file_management_edit_mini.html", $this->table,
            false, [], false);
    }

    /**
     *
     * POST files from create form into the database.
     */
    public function store()
    {
        // only store if has user rights for that company
        // also store in oft_tax or oft_annual_accounts

        $file = [];
        if (!isset($_FILES['file']) || !$file = $_FILES['file']) {
            echo 'Error: Expected a file, none given.';
        }
        if ((isset($file['error']) && $file['error'])
            || !isset($file['name']) || !$file['name']
            || !isset($file['tmp_name']) || !$file['tmp_name']
            || !isset($file['type']) || !$file['type']
            || !isset($file['size']) || !$file['size']) {
            echo 'Error: Expecting one file with a name, tmp_name, type, size and without errors';
            var_dump($file);
            return;
        }

        $file_open = fopen($file['tmp_name'], 'r');
        $file['content'] = fread($file_open, $file['size']);
        fclose($file_open);
        $file['name'] = str_replace(',', ' ', $file['name']);

        if (!checkAllowedFileType($file['name'], $file['tmp_name'])) {
            echo "Error: This file format is not allowed";
            return;
        }

        global $pdo;
        $updateArray = [];
        if (is_array($_POST)) {

            $fillable = ['DELETED', 'NAAM', 'JAAR', 'MONTH', 'DOCTYPE', 'TAGS', 'DATUM', 'STOPDATUM', 'BEDRIJFSID'];


            $form = $_POST;
            if (!isset($form['BEDRIJFSID'])) {
                $form['BEDRIJFSID'] = isset($_REQUEST["BID"]) ? $_REQUEST["BID"] : $this->company;
            }

            $updateArray = $this->updateFields($_POST, $fillable);
        }

        $updateArray['UPDATEDATUM'] = isset($updateArray['UPDATEDATUM']) ? $updateArray['UPDATEDATUM'] : date('Y-m-d');
        $updateArray['NAAM'] = $file['name'];
        $updateArray['BESTANDSNAAM'] = $file['name'];
        $updateArray['PERSONEELSLID'] = $this->user;

        $updateArray['BESTANDTYPE'] = $file['type'];
        $updateArray['BESTANDSIZE'] = $file['size'];

        $updateArray['VERSIENR'] = 1;
        $updateArray['BEKEKEN'] = 0;
        $updateArray['BEDRIJFSID'] = isset($updateArray['BEDRIJFSID']) ? $updateArray['BEDRIJFSID'] : $this->company;

        $cloud = new CloudVPS();
        try {
            $updateArray['CLOUDVPS'] =  $cloud->setFile($file['content'], $file['type'], $file['name']);
        } catch(Exception $e) {
            $updateArray['CLOUDVPS'] = "WRONG";
        }

        $updateArray['BESTANDCONTENT'] = NULL;
        if($updateArray['CLOUDVPS'] == "WRONG" || $updateArray['CLOUDVPS'] == "0") {
            $updateArray['BESTANDCONTENT'] = $file['content'];
        }


        $c = 0;
        $statement = 'INSERT INTO documentbeheer SET';
        foreach ($updateArray as $key => $value) {
            $statement .= $c ? ', ' : ' ';
            $statement .= "`$key` = :$key";
            $c++;
        }
        switch ($updateArray['DOCTYPE']) {
            case 'Tax return':
                $selectStatement = 'SELECT ID FROM `oft_tax` WHERE BEDRIJFSID = ' . (int)$updateArray['BEDRIJFSID'] . ' AND FINANCIALYEAR = ' . (int)$updateArray['JAAR'] . ' LIMIT 1';
                $statement .= ", `TABEL` = 'oft_tax'";
                $statement .= ", `TABELID` = ($selectStatement)";
                break;
            case 'Annual accounts':
                $selectStatement = 'SELECT ID FROM `oft_annual_accounts` WHERE BEDRIJFSID = ' . (int)$updateArray['BEDRIJFSID'] . ' AND FINANCIALYEAR = ' . (int)$updateArray['JAAR'] . ' LIMIT 1';
                $statement .= ", `TABEL` = 'oft_annual_accounts'";
                $statement .= ", `TABELID` = ($selectStatement)";
                break;
        }
        $statement .= ';';
        $storeQuery = $pdo->prepare($statement);

        foreach ($updateArray as $key => $value) {
            switch ($key) {
                case 'BEDRIJFSID':
                case 'EMPLOYEE':
                    $storeQuery->bindValue($key, $value, $pdo::PARAM_INT);
                    break;
                default:
                    $storeQuery->bindValue($key, $value, $pdo::PARAM_STR);
                    break;
            }
        }

        if ($storeQuery->execute()) {
            $last_id = $pdo->lastInsertId();
            @watisergebeurtEdit("Uploaded file: " . ps($file['name']) . " / " . $file['type'] . "", $this->company,
                "create", $this->table, $last_id);
            echo $last_id;
        } else {
            echo "Error: ";
            echo $storeQuery->queryString;
            print_r($storeQuery->errorInfo());
        }
        return;
    }

    /**
     *
     * PATCH existing file from edit form into the database.
     */
    public function update()
    {
        if (!is_array($_POST)) {
            echo "Invalid data. Expected filled _POST";
            return;
        }

        global $pdo;

        // Whitelist
        $fillable = ['DELETED', 'NAAM', 'JAAR', 'MONTH', 'DOCTYPE', 'TAGS', 'DATUM', 'STOPDATUM', 'BEDRIJFSID'];
        $id = $_POST['opslaan_ID'];

        $updateArray = $this->updateFields($_POST, $fillable);

        if (!is_numeric($id)) {
            echo 'Failed SQL injection, wrong primary key';
            return;
        }

        $c = 0;
        $statement = "UPDATE documentbeheer SET";
        foreach ($updateArray as $key => $value) {
            $statement .= $c ? ', ' : ' ';
            $statement .= "`$key` = :$key";
            $c++;
        }
        $statement .= " WHERE ID = :id";

        $updateQuery = $pdo->prepare($statement);
        $updateQuery->bindValue('id', $id);
        foreach ($updateArray as $key => $value) {
            switch ($key) {
                case 'BEDRIJFSID':
                case 'EMPLOYEE':
                    $updateQuery->bindValue($key, $value, $pdo::PARAM_INT);
                    break;
                default:
                    $updateQuery->bindValue($key, $value, $pdo::PARAM_STR);
                    break;
            }
        }
        $updateQuery->execute();

        if ($errorInfo = $updateQuery->errorInfo()) {
            var_dump($updateQuery->errorInfo());
        }
        return;
    }

    /**
     *
     * PATCH existing files from bulk edit form into the database.
     */
    public function bulk_update()
    {
        if (!isset($_POST['form'])
            || !$_POST['form'] != ''
            || !isset($_POST['bulk_ids'])
            || !is_array($_POST['bulk_ids'])) {
            echo "Invalid data. Expected form and ID's";
            return;
        }

        global $pdo;

        parse_str($_POST['form'], $settings);

        // Whitelist
        $fillable = ['NAAM', 'JAAR', 'MONTH', 'DOCTYPE', 'TAGS', 'DATUM', 'STOPDATUM', 'BEDRIJFSID'];

        $updateArray = $this->updateFields($settings, $fillable);

        foreach ($_POST['bulk_ids'] as $id) {
            if (!is_numeric($id)) {
                echo 'Failed SQL injection, wrong primary key';
                return;
            }

            $c = 0;
            $statement = "UPDATE documentbeheer SET";
            foreach ($updateArray as $key => $value) {
                $statement .= $c ? ', ' : ' ';
                $statement .= "`$key` = :$key";
                $c++;
            }

            $statement .= " WHERE ID = :id";

            $updateQuery = $pdo->prepare($statement);
            $updateQuery->bindValue('id', $id);
            foreach ($updateArray as $key => $value) {
                switch ($key) {
                    case 'BEDRIJFSID':
                    case 'EMPLOYEE':
                        $updateQuery->bindValue($key, $value, $pdo::PARAM_INT);
                        break;
                    default:
                        $updateQuery->bindValue($key, $value, $pdo::PARAM_STR);
                        break;
                }
            }

            $updateQuery->execute();

            if ($errorInfo = $updateQuery->errorInfo()) {
//                if ($errorInfo == ["00000", null, null]) {
//                    echo "success";
//                } else {
                var_dump($errorInfo);
//                }
            }
        }
        return;
    }

    private function updateFields($form = [], $fillable)
    {
        if (!is_array($form) || !$fillable) {
            return false;
        }

        $updateArray = [];

        foreach ($form as $key => $value) {
            if (str_starts_with($key, "documentbeheer_") && $value) {
                $updateKey = str_replace("documentbeheer_", "", $key);
                if (!in_array($updateKey, $fillable) || !$value) {
                    echo "Field '$updateKey' is not fillable!";
                    continue;
                }
                $updateArray[$updateKey] = $value;
            }
        }
        if ($updateArray !== []) {
            $updateArray['UPDATEDATUM'] = date('Y-m-d');
        }
        return $updateArray;
    }

    public function insertCloudvps($fName, $fType, $fSize, $fContent, $catId = 0, $clientId = 0) {
        global $pdo;

        $KOPPELING = Helper::reqVal('KOPPELING', '');
        $ORDNER = Helper::reqVal('ORDNR', '');
        $TAGS = Helper::reqVal('TAGS', '');
        $DOCTYPE = Helper::reqVal('DOCTYPE', '');
        $DOCDATUM = Helper::reqVal('DOCDATUM', date("Y-m-d"));
        $BESTANDSNAAM = Helper::reqVal('BESTANDSNAAM', $fName);
        $newNr = getNieuwNr("documentbeheer", "DOCUMENTNR", $this->company, $DOCTYPE);
        $VERSIENR = Helper::reqVal('VERSIENR', "A");
        $STATUS = Helper::reqVal('STATUS', '');
        $RECHTEN = Helper::reqVal('RECHTEN', '');

        if (!$fContent || !$fType) {
            return false;
        }

        $cloud = new CloudVPS();
        $query = "INSERT INTO documentbeheer SET DELETED        = 'Nee',
                                                    UPDATEDATUM    = '" . date("Y-m-d") . "',
                                                    CATAGORIEID    = '" . ps($catId, "nr") . "',
                                                    NAAM           = '" . ps($BESTANDSNAAM) . "',
                                                    PERSONEELSLID  = '" . ps($this->user, "nr") . "',
                                                    BESTAND        = '" . ps($fName) . "',
                                                    BESTANDSNAAM   = '" . ps($fName) . "',
                                                    BESTANDTYPE    = '" . ps($fType) . "',
                                                    BESTANDSIZE    = '" . ps($fSize) . "',
                                                    CLOUDVPS       = '" . $cloud->setFile($fContent, $fType) . "',
                                                    VERSIENR       = '" . ps($VERSIENR) . "',
                                                    BEKEKEN        = '0',
                                                    RECHTEN        = '" . ps($RECHTEN, "nr") . "',
                                                    TOKEN          = '" . uniekeId() . "',
                                                    DATUM          = '" . ps($DOCDATUM, "date") . "',
                                                    ORDNER         = '" . ps($ORDNER, "nr") . "',
                                                    TAGS           = '" . ps($TAGS) . "',
                                                    DOCTYPE        = '" . ps($DOCTYPE) . "',
                                                    JAAR           = '" . date("Y") . "',
                                                    BEDRIJFSID     = '" . ps($this->company, "nr") . "';";
        $stmt = $pdo->prepare($query);
        return $stmt->execute() ? (int) $pdo->lastInsertId() : $pdo->errorInfo();
    }
}