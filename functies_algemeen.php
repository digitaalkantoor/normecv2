<?php
/*
 * Function to redirect a user to given url.
 * 
 * @param (string) $url The url to redirect to
 */
function rd($url)
{
   $url = trim($url);
   $url = str_replace("Location: ", "", $url);
   $url = str_replace(" ", "", $url);
   header("Location: $url");
   exit;
}

function factuurbedrag($totaalbedrag, $korting, $boete, $adminkosten, $verzendkosten, $btw, $totaalbedragpr, $btwpr, $totaalNullBTW)
{
     if(($btwpr != "") && ($btwpr != $btw))
     {
         $bedrag1 = (($totaalbedrag-$korting)+$boete+$adminkosten + $verzendkosten);
         if ($btw != "Verlegd" || $btw != "0") {
           $bedrag1 = prijsconversie(($bedrag1*(($btw/100)+1)));
         }

         if ($btwpr != "Verlegd" || $btwpr != "0") {
            $btwpr = prijsconversie(($totaalbedragpr*(($btwpr/100)+1)));
         }
         $antwoord = $bedrag1 + $btwpr;
     } else {
         $antwoord = (($totaalbedrag-$korting)+$boete+$adminkosten + $verzendkosten + $totaalbedragpr);
         if ($btw != "Verlegd" || $btw != "0") {
           $antwoord = prijsconversie(($antwoord*(($btw/100)+1)));
         }
     }
     
     if(($totaalNullBTW*1) != 0)
     {
        $antwoord += $totaalNullBTW;
     }

     return $antwoord;
}

function factuurbedragBTW($totaalbedrag, $korting, $boete, $adminkosten, $verzendkosten, $btw, $totaalbedragpr, $btwpr)
{
     if(($btwpr != "") && ($btwpr != $btw))
     {
         $bedrag1 = (($totaalbedrag-$korting)+$boete+$adminkosten + $verzendkosten);
         if ($btw != "Verlegd" || $btw != "0") {
           $btw = prijsconversie(($bedrag1*($btw/100)));
         }

         if ($btwpr != "Verlegd" || $btwpr != "0") {
            $btwpr = prijsconversie(($totaalbedragpr*($btwpr/100)));
         }
         $antwoord = $btw + $btwpr;
     } else {
         $antwoord = (($totaalbedrag-$korting)+$boete+$adminkosten + $verzendkosten + $totaalbedragpr);
         if ($btw != "Verlegd" || $btw != "0") {
           $antwoord = prijsconversie(($antwoord*(($btw/100))));
         }
     }

     return $antwoord;
}

function maandenvanhetjaar($type = "", $taal = "")
{
  if($taal == "") {
    $taal = taal();
  }

  if($type == "2")
  {
      $return = array("Geen", "januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december");
    
      if($taal == "EN")
      {
        $return = array("None", "January","February","March","April","May","June","July","August","September","October","November","December");
      }
  } elseif($type == "3")
  {
      $return = array("Geen", "jan","febr","mrt","apr","mei","jun","jul","aug","sep","okt","nov","dec");
    
      if($taal == "EN")
      {
        $return = array("None", "Jan","Febr","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec");
      }
  } else {
      $return = array("januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december");
    
      if($taal == "EN")
      {
        $return = array("January","February","March","April","May","June","July","August","September","October","November","December");
      }
  }
  
  return $return;
}

function getGrootboekRekNRBijBTWTarrief($btwTarrif, $factnr, $bedrijfsid) //19 als invoer en niet 0.19
{
     $grBnRBTW  = "24";
     if ($btwTarrif > 0) {
         $btwTarrif = $btwTarrif / 100;
     }

     if(isFactuurBuitenland($factnr, $bedrijfsid))
     {
       if($btwTarrif == btwHoog())
       {
         $grBnRBTW = "103";
       } else {
         $grBnRBTW = "104";
       }
     } else {
       if($btwTarrif == btwHoog())
       {
         $grBnRBTW = "24";
       } else {
         $grBnRBTW = "25";
       }
     }

     return $grBnRBTW;
}

function schrijfUurtje($totaalMin, $toonNul = true)
{
     if($toonNul == false && $totaalMin == 0) {
        return '';
     } else {
       if($totaalMin < 0)
       {
           $totaalMin                    *= -1;
           $uur                          = floor($totaalMin / 60);
           $min                          = $totaalMin - ($uur*60);
           $min                          = ceil($min);
           if(strlen($min)==1)           { $min = "0". $min; }
           if(strlen($uur)==1)           { $uur = "0". $uur; }
           return "-$uur:$min";
       } else {
           $uur                          = floor($totaalMin / 60);
           $min                          = $totaalMin - ($uur*60);
           $min                          = ceil($min);
           if(strlen($min)==1)           { $min = "0". $min; }
           if(strlen($uur)==1)           { $uur = "0". $uur; }
           return "$uur:$min";
       }
     }
}

function schrijfBedrag($bedrag1, $bedrag2)
{
    $output = "";
    if($bedrag1 != 0 && $bedrag2 != 0)
    {
        $output = "<font color=\"red\">".prijsconversiePuntenKommas($bedrag1)."</font> + ".prijsconversiePuntenKommas($bedrag2);
    } elseif($bedrag1 != 0 && $bedrag2 == 0)
    {
        $output = "<font color=\"red\">".prijsconversiePuntenKommas($bedrag1)."</font>";
    }  elseif($bedrag1 == 0 && $bedrag2 != 0)
    {
        $output = prijsconversiePuntenKommas($bedrag2);
    } else { $output = "-"; }
    return $output;
}

function dateDiffZonderWeekend($dformat, $endDate, $beginDate)
{
    $dagteller = 0;

    $breek = 1;
    while ($breek == 1)
    {
       //echo "<br/>" . $beginDate . " ";
       $beginDate = explode("/",$beginDate);
       $dag   = $beginDate[0];
       $maand = $beginDate[1];
       $jaar  = $beginDate[2];

       $dagVDweek = date("w", mktime(0, 0, 0, $maand, $dag, $jaar));

       //echo $dagVDweek . " " . $endDate;
       if ($dagVDweek != 0 && $dagVDweek != 6) { $dagteller++; }

       $tmpLaatsteData  = explode("-",volgendeDag($dag, $maand, $jaar, 1));
       $volgendedag  = $tmpLaatsteData[0];
       $volgendeMaand= $tmpLaatsteData[1];
       $volgendeJaar = $tmpLaatsteData[2];

       if (strlen($volgendedag) == 1) { $volgendedag = "0".$volgendedag; }
       if (strlen($volgendeMaand) == 1) { $volgendeMaand = "0".$volgendeMaand; }

       $beginDate    = "$volgendedag/$volgendeMaand/$volgendeJaar";
       if ($beginDate == $endDate) { $breek = 0; }

       if($volgendeJaar > (date("Y")+4)) { $breek = 0; }

       if ($dagteller == 100) { $breek = 0; }
    }
    //echo "<br/> " . $dagteller;
    return lb($dagteller);
}

function datumconversie($datum)
{
  $datum = substr($datum,5,6);
  $deel1 = substr($datum,0,2);
  $deel2 = substr($datum,3,4);
  return lb($deel2."-".$deel1);
}

function datumconversieTotal($datum)
{
      $deel3 = substr($datum,0,4);
      $datum = substr($datum,5,6);
      $deel1 = substr($datum,0,2);
      $deel2 = substr($datum,3,4);
      return lb($deel2."-".$deel1."-".$deel3);
}

function datumconversiePresentatie($datum, $taal = "")
{
  if($taal == "Nederlands") {
    $taal = "NL";
  } elseif($taal == "Engels") {
    $taal = "EN";
  }

  if($taal == "") {
    $taal = taal();
  }

  if ($datum == "" || $datum == "0000-00-00") {
    return "";
  } else {
    $maanden = maandenvanhetjaar("2", $taal);
    if($taal == "NL") {
      $deel3 = substr($datum,0,4);
      $datum = substr($datum,5,6);
      $deel1 = substr($datum,0,2);
      $deel2 = substr($datum,3,4);

      $maandNaam = "";
      if(isset($maanden[($deel1*1)])) {
        $maandNaam = $maanden[($deel1*1)];
      }

      return lb($deel2." ".$maandNaam." ".$deel3);
    } else {
      $deel3 = substr($datum,0,4);
      $datum = substr($datum,5,6);
      $deel1 = substr($datum,0,2);
      $deel2 = substr($datum,3,4);

      $maandNaam = "";
      if(isset($maanden[($deel1*1)])) {
        $maandNaam = $maanden[($deel1*1)];
      }

      return lb($maandNaam . " " . $deel2 ." ".$deel3);
    }
  }
}

function datumconversiePresentatieKort($datum, $taal = "")
{
  if($taal == "Nederlands") {
    $taal = "NL";
  } elseif($taal == "Engels") {
    $taal = "EN";
  }

  if($taal == "") {
    $taal = taal();
  }

  if ($datum == "" || $datum == "0000-00-00") {
    return "";
  } else {
    $maanden = maandenvanhetjaar("3", $taal);
    if($taal == "NL") {
      $deel3 = substr($datum,0,4);
      $datum = substr($datum,5,6);
      $deel1 = substr($datum,0,2);
      $deel2 = substr($datum,3,4);

      $maandNaam = "";
      if(isset($maanden[($deel1*1)])) {
        $maandNaam = $maanden[($deel1*1)];
      }

      return lb($deel2." ".$maandNaam." ".$deel3);
    } else {
      $deel3 = substr($datum,0,4);
      $datum = substr($datum,5,6);
      $deel1 = substr($datum,0,2);
      $deel2 = substr($datum,3,4);

      $maandNaam = "";
      if(isset($maanden[($deel1*1)])) {
        $maandNaam = $maanden[($deel1*1)];
      }

      return lb($maandNaam . " " . $deel2 ." ".$deel3);
    }
  }
}

function datumconversienaarVS($datum)
{
  $datum = str_replace("/", "-", $datum);

  if(strlen($datum) == 10)
  {
      if(substr($datum, 2,1) == "-")
      {
         //NL notatie
         $dag    = substr($datum,0,2);
         $maand  = substr($datum,3,2);
         $jaar   = substr($datum,6,10);
         $return = $jaar."-".$maand."-".$dag;
      } else {
         $return = $datum;
      }
  } else {
      $return = $datum;
  }

  if($return == "--") { $return = ""; }

  return lb($return);
}

function datumconversieVerval($datum, $dagen = 30)
{
  return datumconversiePresentatie(date("Y-m-d",strtotime("+".$dagen." day", strtotime($datum))));
}

function datumconversieTotal2($datum)
{
  $arraymaand = maandenvanhetjaar();

  $deel3 = substr($datum,0,4);
  $datum = substr($datum,5,6);
  $deel1 = substr($datum,0,2);
  $deel2 = substr($datum,3,4);
  return lb($deel2." ".@$arraymaand[(($deel1*1)-1)]." ".$deel3);
}

function datumMaand($datum)
{
  $datum = substr($datum,5,6);
  $maand = substr($datum,0,2);
  $arraymaand = maandenvanhetjaar();
  if(!isset($arraymaand[$maand-1])) { 
    $maand_nom = $arraymaand[$maand];
  } else {
    $maand_nom = $arraymaand[$maand-1];
  }
  return lb($maand_nom);
}

function datumMaandTaal($datum, $taal)
{
  if($taal == "Nederlands")
  {
    return datumMaand($datum);
  } elseif($taal == "Engels") {
      $datum = substr($datum,5,6);
      $maand = substr($datum,0,2);

      $arraymaand = array("January","February","March","April","May","June","July","August","September","October","November","December");
      $maand_nom = $arraymaand[$maand-1];
      return $maand_nom;
  } else {
      return datumMaand($datum);
  }
}

function diff_days($start, $stop)
{
    $diff_secs = abs(strtotime($stop) - strtotime($start));
    $base_year = min(date("Y", substr($stop,0,4)), date("Y", substr($start,0,4)));

    $datumverschil = mktime(0, 0, $diff_secs, 1, 1, $base_year);
    $datumverschil = floor($datumverschil / (3600 * 24));
    
    return $datumverschil;
}

function get_time_difference_string( $start, $end )
{
    $dagtijddiff = get_time_difference( $start, $end );

    if(isset($dagtijddiff["years"]))
    {
      $dagtijddiff["years"] += 1970;
    } else {
      $dagtijddiff["years"] = 1970;
    }

    $diff        = voorloopTekens($dagtijddiff["years"], 4, "0") . "-". voorloopTekens($dagtijddiff["months"], 2, "0") . "-" . voorloopTekens($dagtijddiff["days"], 2, "0")." ".voorloopTekens($dagtijddiff["hours"], 2, "0").":".voorloopTekens($dagtijddiff["minutes"], 2, "0").":".voorloopTekens($dagtijddiff["seconds"], 2, "0");
    return lb($diff);
}

function get_time_difference( $start, $end )
{
    $uts['start'] = strtotime( $start );
    $uts['end']   = strtotime( $end );

    if( $uts['start']!==-1 && $uts['end']!==-1 )
    {
        if( $uts['end'] >= $uts['start'] )
        {
            $diff    =    $uts['end'] - $uts['start'];
        } else {
            $diff    =    $uts['start'] - $uts['end'];
        }
        if( $years=intval((floor($diff/31536000))) )
            $diff = $diff % 2592000;
        if( $months=intval((floor($diff/2592000))) )
            $diff = $diff % 2592000;
        if( $days=intval((floor($diff/86400))) )
            $diff = $diff % 86400;
        if( $hours=intval((floor($diff/3600))) )
            $diff = $diff % 3600;
        if( $minutes=intval((floor($diff/60))) )
            $diff = $diff % 60;
        $diff    =    intval( $diff );

        return( array('years'=>$years, 'months'=>$months, 'days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
    }
    else
    {
        trigger_error( "Invalid date/time data detected", E_USER_WARNING );
    }
    return( false );
}


function prijsconversie($prijs)
{
     $prijs  = str_replace(" ", "", $prijs);
     $prijs  = str_replace(",", ".", $prijs);
     $prijs  = $prijs*1;

     //$prijs = round($prijs, 2);


     $minderNull = "nee";
     if($prijs < 0) { $minderNull = "ja"; $prijs = $prijs *-1; }

     //$rondNaarBenedenAf = rondNaarBenedenAf($prijs*1000);
     //$laatstecijfer = substr($rondNaarBenedenAf, (strlen($rondNaarBenedenAf)-1),1);
     //echo("<BR> laatse cijfer: " .$laatstecijfer);

     //rond af
     $prijs = $prijs*100;
     $prijs = round($prijs);
     $prijs = $prijs/100;

     //Wanneer 3e decimaal groter is dan 5, dan 0.01 erbij.
     //if($laatstecijfer >= 5) { $prijs += 0.01; }

     $y = explode(".", $prijs);

     //echo("<BR> p5: " .$y[0] . " - " . $y[1] . "<br/> ");
     if (sizeof($y) == 0) { $prijs = "0.00"; } //echo("<BR> p6: " .$prijs); }
     elseif (sizeof($y) == 1) { $prijs = "$prijs.00"; } //echo("<BR> p7: " .$prijs); }
     elseif (sizeof($y) == 2)
     {
        if (strlen($y[1]) == 1) { $prijs = $y[0] . "." . $y[1] . "0"; } // echo("<BR> p8: " .$prijs); }
        if (strlen($y[1]) == 0) { $prijs = $prijs . ".00"; } //echo("<BR> p9: " .$prijs);}
        if (strlen($y[1]) == 2) { $prijs = $y[0] . "." . $y[1];  } //echo("<BR> p10: " .$prijs);}
        //echo("<BR> p11: " .$prijs);
     }

     if($minderNull == "ja") { $prijs = "-".$prijs; }
     
     return $prijs;
}


function prijsconversiePuntenKommas($prijs, $valuta = '', $toonNul = true)
{
     $prijs = prijsconversie($prijs);

     if(isset($_REQUEST["EXPORTEXCEL"])) {
         if($toonNul == false && $prijs == 0) {
           return '';
         } else {
           return $prijs = str_replace(".", ",", $prijs);
         }
     } else {

         $minteken = "";
         if(substr($prijs, 0,1) == "-") { $minteken = "-"; $prijs = substr($prijs,1,strlen($prijs)); }
    
         $x = explode(".", $prijs);
         $prijsA = $x[0];
         $prijsB = $x[1];
    
         $lengtestr = strlen($prijsA);
         if ($lengtestr > 3) {
            //12000
            $rest = "";
            while($lengtestr > 3)
            {
               $rest      = substr($prijsA, (strlen($prijsA)-3),3) . "." . $rest; //200
               $prijsA    = substr($prijsA, 0,(strlen($prijsA)-3)); //12
               $lengtestr = strlen($prijsA); //2
            }
            $rest  = ($prijsA . "." . $rest);
            $prijsA = substr($rest,0,(strlen($rest)-1));
         }
         
         if($toonNul == false && $prijsA == 0 && $prijsB == 0) {
           return '';
         } else {
           return $valuta . $minteken . $prijsA . "," .$prijsB;
         }
     }
}

function rondNaarBovenAf($prijs)
{
  $prijsArray = explode(".", $prijs);

  if ($prijsArray[0] < $prijs) { $prijs = $prijsArray[0] + 1; }
  return $prijs;
}

function rondNaarBenedenAf($prijs)
{
  $prijsArray = explode(".", $prijs);

  $prijs = $prijsArray[0];
  return $prijs;
}

function rondAf($prijs)
{
  $prijsArray = explode(".", $prijs);

  if (($prijsArray[0]+0.5) < $prijs) { $prijs = $prijsArray[0] + 1; }
  else { $prijs = $prijsArray[0]; }

  return $prijs;
}

function rondAbsoluutAf($prijs)
{
  if ($prijs < 0) { $prijs = $prijs * -1; }

  return $prijs;
}

function alleenBovenNul($getal)
{
  if ($getal < 0) { $getal = 0; }

  return $getal;
}

function uniekeId()
{
   return str_replace(".", "", urlencode(uniqid(date("his"), true)));
}

function basisUrlDK()
{
        $filename = $_SERVER["REQUEST_URI"];
        $filename = explode("/", $filename);
        $filename = $filename[(count($filename)-1)];

        $host = "http://".$_SERVER["HTTP_HOST"];
    	$REQUEST_URI = $_SERVER["REQUEST_URI"];
    	$REQUEST_URI = explode($filename, $REQUEST_URI);
    	if(count($REQUEST_URI) > 1)
    	{
    	   $REQUEST_URI = $REQUEST_URI[0];
    	} else {
           $REQUEST_URI = "";
        }
        $host .= $REQUEST_URI;
        return $host;
}

function bestandsnaamcheck($bestandsnaam)
{
  $bestandsnaam = stripslashes($bestandsnaam);
  $bestandsnaam = strtolower($bestandsnaam);
  $bestandsnaam = str_replace("  ", " ", $bestandsnaam);
  $bestandsnaam = str_replace("'", "", $bestandsnaam);
  $bestandsnaam = str_replace("\"", "", $bestandsnaam);
  $bestandsnaam = str_replace(" ", "_", $bestandsnaam);
  $bestandsnaam = str_replace(array('"', "'", ' ', ','), '_', $bestandsnaam);
  $bestandsnaam = trim($bestandsnaam);
  if ($bestandsnaam == '') {
    $bestandsnaam = "document";
  }

  return $bestandsnaam;
}

// Our custom error handler
function dk_error_handler($number, $message, $file, $line, $vars)
{
  $email = "<p>An error ($number) occurred on line <strong>$line</strong> and in the <strong>file: $file.</strong><p> $message </p>";
  $email .= "<pre>" . print_r($vars, 1) . "</pre>";

  $from = "support@digitaalkantoor.nl";
  $to = "support@digitaalkantoor.nl";
  $headers = "From: $from \nReply-to: $from <$from>\nX-mailer: DK mailverstuurprogramma";
  //mail($from, "Error", $to, $headers);

  //$filename = explode("/", $file);
  //$filename = end($filename);
  //mail_versturen("support@digitaalkantoor.nl", "support@digitaalkantoor.nl", '', '', getCompanyDatabase(). " / $filename / $line", $email, '', '', 1, 1, '');
}

/**
 *
 * Function to check if a string begins with another string
 *
 * @param $haystack
 * @param $needle
 *
 * @return bool
 */
function str_starts_with($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

/**
 *
 * Function to check if a string ends with another string
 *
 * @param $haystack
 * @param $needle
 *
 * @return bool
 */
function str_ends_with($haystack, $needle)
{
    $length = strlen($needle);
    return $length === 0 ||
        (substr($haystack, -$length) === $needle);
}

/**
 *
 * Function to convert a string to a CamelCase string.
 *
 * @param        $input
 * @param string $separator
 *
 * @return string
 */
function CamelCase($input, $separator = '_')
{
    return str_replace($separator, '', ucwords($input, $separator));
}

/**
 *
 * Function to convert a string to a lowerCamelCase string.
 *
 * @param        $input
 * @param string $separator
 *
 * @return string
 */
function lowerCamelCase($input, $separator = '_')
{
    return lcfirst(CamelCase($input, $separator));
}