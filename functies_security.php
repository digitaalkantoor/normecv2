<?php
function melding($tekst)
{
    return $tekst;
}

function checkInvoer($invoer)
{
    global $db;

    $invoer = str_replace("insert", "", $invoer);
    $invoer = str_replace("information_schema", "", $invoer);
    $invoer = str_replace("update ", "", $invoer);
    $invoer = str_replace("delete ", "", $invoer);
    $invoer = str_replace("table ", "", $invoer);
    $invoer = str_replace("drop ", "", $invoer);
    $invoer = str_replace("union", "", $invoer);
    $invoer = str_replace("concat", "", $invoer);
    $invoer = str_replace("truncate", "", $invoer);
    $invoer = str_replace("drop database", "", $invoer);
    $invoer = str_replace("drop table", "", $invoer);

    //TODO: select-optie uitzetten
    $invoer = str_replace("select", "", $invoer);
    $invoer = str_replace("sleep", "", $invoer);
    $invoer = str_replace("alter", "", $invoer);
    $invoer = str_replace("\"", "", $invoer);
    $invoer = str_replace("'", "", $invoer);

    $invoer = str_replace("`", "", $invoer);
    //$invoer = str_replace("!", "", $invoer);
    $invoer = str_replace("#", "", $invoer);
    //$invoer = str_replace("$", "", $invoer);
    //$invoer = str_replace("%", "", $invoer);
    $invoer = str_replace("^", "", $invoer);
    //$invoer = str_replace("&", "", $invoer);
    $invoer = str_replace("{", "", $invoer);
    $invoer = str_replace("}", "", $invoer);
    $invoer = str_replace("[", "", $invoer);
    $invoer = str_replace("]", "", $invoer);

    $invoer = str_replace("\xbf\x27", "", $invoer);

    if ($db !== null) {
        $invoer = mysqli_real_escape_string($db, $invoer);
    }

    //$invoer = iconv("UTF-8", "ISO-8859-1//IGNORE", $invoer);
    //$invoer = mb_convert_encoding($invoer, "UTF-8");

    return $invoer;
}

/*
 * Function to check if table is a valid table
 *
 * @param (int) $userid
 * @param (int) $bedrijfsid
 * @param (array) $structure
 *
 * @return (bool)
 */
function checkValidTable($userid, $bedrijfsid, $structure)
{
    if (count(oft_get_structure($userid, $bedrijfsid, $structure, "arrayVelden")) == 0) {
        return true;
    }
    return true;
}

function ps($statement, $type = "")
{
    global $db;

    if (is_array($statement)) {
        var_dump([$type, $statement]);
        die();
    }

    //prepared statement
    $statement = addslashes($statement);

    if ($type == "nr") {
        //TODO: is_numeric -> toevoegen ??
        if (strlen($statement) > 25) {
            $statement = "";
        } else {
            $statement = $statement * 1;
        }
    } elseif ($type == "date") {
        if (strlen($statement) > 11) {
            $statement = "";
        } else {
            if (strlen($statement) == 0) {
                $statement = null;
            } elseif ($statement == "--") {
                $statement = null;
            } elseif ($statement == "") {
                $statement = null;
            } elseif ($statement == "0000-00-00") {
                $statement = null;
            } else {
                $statement = checkInvoer($statement);
            }
        }
        $statement = datumconversienaarVS($statement);
    } elseif ($type == "amount") {
        if (strlen($statement) == 0) {
            $statement = "0";
        } else {
            $statement = checkInvoer($statement);
            $statement = str_replace(",", ".", $statement);
        }
    } elseif ($type == "text") {
        $statement = preg_replace("/[^\x9\xA\xD\x20-\x7F]/", "", $statement);
        $statement = checkInvoer($statement);
        $statement = iconv("UTF-8", "ISO-8859-1//IGNORE", $statement);
        $statement = iconv("UTF-8", "UTF-8//IGNORE", $statement);
        $statement = mb_convert_encoding($statement, "UTF-8");
    } elseif ($type == "tabel") {
        if (!checkValidTable(getMySelf(), getMyBedrijfsID(), $statement . "_overzicht")) {
            $statement = "";
        }
        $statement = checkInvoer($statement);
    } else {
        $statement = checkInvoer($statement);
    }

    return $statement;
}

function gaNaarLogin($error, $voorvoegsel = "")
{

    $url = $voorvoegsel . "index.php?ERROR=$error";

    if (!isset($_REQUEST["APP_ACTIVE"])) {
        header("Location: $url");
    }

    // echo("<script type=\"text/javascript\">\n");
    // echo("window.top.location.replace('".$voorvoegsel."index.php?ERROR=$error');\n");
    // echo("</script>");
    // exit;
}

function errorMeldingen($error)
{
    $return = "Error";

    if (taal() == "EN") {
        if ($error == "1") {
            $return = melding("Please log in again.");
        } elseif ($error == "2") {
            $return = melding("Your user-id is not valid.");
        } elseif ($error == "3") {
            $return = melding("Cross site scripting error!");
        } elseif ($error == "4") {
            $return = melding("SQL injection!");
        } elseif ($error == "5") {
            $return = melding("Please check your username and/or password.");
        } elseif ($error == "6") {
            $return = melding("Your account is blocked.");
        }
    } else {
        if ($error == "1") {
            $return = melding("U dient opnieuw in te loggen.");
        } elseif ($error == "2") {
            $return = melding("Uw gebruikers-id is niet valide.");
        } elseif ($error == "3") {
            $return = melding("Cross site scripting error.");
        } elseif ($error == "4") {
            $return = melding("SQL injection.");
        } elseif ($error == "5") {
            $return = melding("De combinatie van gebruikersnaam en wachtwoord is verkeerd.");
        } elseif ($error == "6") {
            $return = melding("Uw account is geblokkeerd.");
        }
    }

    return $return;
}

function sercurityCheck()
{

    if (!isset($_REQUEST["APP_ACTIVE"])) {
        $url = explode("/", $_SERVER["PHP_SELF"]);
        $url = $url[(count($url) - 1)];
        if ($url != "admin.php") {
            $loginid = getValidatedLoginID();

            if ($loginid == 0 || $loginid == "" || $loginid == "0") {

                $_SESSION['DB'] = null;
                $_SESSION['LID'] = null;
                gaNaarLogin("1");
            }

            //check id's of de code niet langer is dan 10 tekens
            if (isset($_REQUEST["ID"]) && !@$_REQUEST["ID"] == "(Action Blog)") {
                if (strlen($_REQUEST["ID"]) > 12) {
                    gaNaarLogin("2");
                }
            }
        }
    }
}

function sercurityDoubleCheck()
{

    checkSSL();
    sercurityCheck();

    // Set Session lifetime to 30 minutes.
    ini_set('session.gc_maxlifetime', 1800);

    // start session when not started yet.
    if (!isset($_SESSION)) {
        session_start();
    }

    // check if session is expired.
    // CTO = Custom TimeOut
    // http://stackoverflow.com/questions/520237/
    if (isset($_SESSION['CTO_LAST_ACTIVITY']) && (time() - $_SESSION['CTO_LAST_ACTIVITY'] > 1800)) {
        // last request was more than 30 minutes ago
        session_unset();    // unset $_SESSION variable for the run-time
        session_destroy();  // destroy session data in storage
        gaNaarLogin("1");   // go to login page
    }
    $_SESSION['CTO_LAST_ACTIVITY'] = time(); // update last activity time stamp

    // regenerate session when session is older than 30 minutes and still active.
    if (!isset($_SESSION['CTO_CREATED'])) {
        $_SESSION['CTO_CREATED'] = time();
    } elseif (time() - $_SESSION['CTO_CREATED'] > 1800) {
        // session started more than 30 minutes ago
        session_regenerate_id(true);    // change session ID for the current session and invalidate old session ID
        $_SESSION['CTO_CREATED'] = time();  // update creation time
    }

    //Check of ipadres overeenkomt met Host

    //Check waar request vandaan komt. Als het niet van de huidige site komt, dan error.
    $REFERRER = "";
    if (isset($_SERVER["HTTP_REFERER"]) && $_SERVER["HTTP_REFERER"] != '') {
        $host = parse_url($_SERVER["HTTP_REFERER"], PHP_URL_HOST);
        $domains = [
            "normec-cm.com",
            "www.normec-cm.com",
            "normec-cm.localhost",
            "normec.localhost",
        ];
        if (!in_array($host, $domains)) {
            gaNaarLogin("3");
        }
    }

    //XXS check - GET
    $queryString = $_SERVER["QUERY_STRING"];
    $queryString = str_replace("&&", "&", $queryString);
    $queryString = str_replace("%20", "", $queryString);
    $queryString = str_replace("&_=", "&X=", $queryString);
    $queryString = str_replace("&amp;", "&", $queryString);

    $queryString = str_replace("-", "", $queryString);
    $queryString = str_replace("_", "", $queryString);
    $queryString = str_replace("VERANTWPLID=&", "", $queryString);
    $queryString = str_replace("&WANNEERUITGELEVERD=", "", $queryString);
    if (isset($_REQUEST["ID"])) {
        if (empty($_REQUEST["ID"])) {
            $queryString = str_replace("&ID=", "", $queryString);
        }
    }
    //TODO: bestanden/files -> de forward slash

//  if ( !validateQueryString ( $queryString )) {
//    gaNaarLogin("3");
//  }

    if (isset($_REQUEST["GROUP"])) {
        if (!preg_match('/^GROUPID=\d+$/', $_REQUEST["GROUP"])) {
            gaNaarLogin("4");
        }
    }

    //Alles wat html heet wordt eruit gesloopt.
    //XXS check - POST

    $_REQUEST = replaceXXS($_REQUEST);
    $_POST = replaceXXS($_POST);
    $_GET = replaceXXS($_GET);
    $_FILES = replaceXXS($_FILES);
}

//XXS check
function replaceXXS($array)
{
    foreach ($array as $postName => $postValue) {
        if (is_array($postValue)) {
            $array[$postName] = replaceXXS($postValue);
        } else {
            $array[$postName] = xss_clean($postValue);
        }
    }
    return $array;
}

/*
 * Input validation
 * Based on functies_oft_structure.php validate input
 *
 * @param (int) $userid The Id of logged in user
 * @param (int) $bedrijfsid The Id of the selected Company
 * @param (string) $name name of input
 * @param (string) $value value of input
 *
 * @return (mixed) Cleanced input value
 */
function input_validation($userid, $bedrijfsid, $name, $value)
{

    $return = $value;

    //Check if value is empty
    //If so, check if field is required
    if (empty($value) && $_POST["submit"] != 'delete') {
        if (preg_match("/^([a-z_]+)_([A-Z_]+)$/", $name, $matches)) {
            $tabel = $matches[1];
            $veld = $matches[2];
            // Get data structure
            $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel, "");
            $arrayVerplicht = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVerplicht");
            $key = array_search($veld, $arrayVelden);

            if ($key !== false) {
                if (isset($arrayVerplicht[$key])) {
                    if ($arrayVerplicht[$key] == "Ja") {
                        throw new Exception("1. Required field cannot be empty");
                    }
                }
            }
        }
    }

    // Only do something when value is not empty
    // and get table and kolom from input[name]
    if (!empty($value) && preg_match("/^([a-z_]+)_([A-Z_]+)$/", $name, $matches)) {

        $tabel = $matches[1];
        $veld = $matches[2];
        // Get data structure
        $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
        //$arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen"); Todo?
        $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");

        $key = array_search($veld, $arrayVelden);

        /*if($name == 'oft_entitie_numbers_TYPE') {
          dpm(array($name, $value, $tabel, $veld, $key, $arrayVeldType[$key])); exit;
        }*/

        if ($key !== false) {
            $type = $arrayVeldType[$key];
            switch ($type) {
                case 'SKIP_ADD_FILES_TYPE':
                case 'SKIP_ADD_FILES':
                case 'SKIP_LIST_FILES':
                case 'SKIP_AGENDA':
                case 'SKIP_INVITEES':
                case 'SKIP_INCIDENT_ACTIONS':
                case 'SKIP_EXISTING':
                case 'SKIP_UPLOADED_BY':
                case 'SKIP_VERSIONS':
                case 'SKIP_FILE_HISTORY':
                case 'AMOUNTPERSENTAGE': // what does this do?
                case 'TIME': // not in oft_tpl_tonen.php
                case 'bestand_in_year_from_type':
                case 'boardpacklink': // not in oft_tpl_tonen.php
                case 'newuser': // what does this do?
                case 'nr_of_docs': // not in oft_tpl_tonen.php
                case 'bedrag': // not in oft_tpl_tonen.php
                case 'bestand': // not in oft_tpl_tonen.php
                case 'country': // in structue oft_compliance_chart_overzicht but not in oft_tpl_tonen.php
                case 'countryofresidence': // not in oft_tpl_tonen.php
                case 'field': // not in oft_tpl_tonen.php
                case 'fieldLang': // not in oft_tpl_tonen.php
                case 'get_personeel_email': // not in oft_tpl_tonen.php
                case 'getal': // not in oft_tpl_tonen.php
                case 'selectJa': // what does this do?
                case 'persentage': // not in oft_tpl_tonen.php
                case 'person_name': // not in oft_tpl_tonen.php
                case 'fullname': // not in oft_tpl_tonen.php
                case 'stam_labels': // not in oft_tpl_tonen.php
                case 'stam_compliantstatus': // not in oft_tpl_tonen.php
                case 'stam_contracttype': // not in oft_tpl_tonen.php
                case 'stam_ordners': // not in oft_tpl_tonen.php
                case 'stam_rechtsvormen': // not in oft_tpl_tonen.php
                case 'stam_risklevel': // not in oft_tpl_tonen.php
                case 'stam_trainingsstatus':
                case 'type_classes': // not in oft_tpl_tonen.php
                case 'translate': // not in oft_tpl_tonen.php
                case 'rechtentot': // not a field stored in a db
                    break;

                // selectBox values which are in functies_oft_listoptions.php
                case 'accounting_systems':
                case 'adres_type':
                case 'AUTHORIZATION':
                case 'incident_soort':
                case 'mena_category':
                case 'mena_services':
                case 'mena_status':
                case 'numbers_types':
                case 'PRIORITY':
                case 'PROFIEL':
                case 'reg_incidenttype':
                case 'reg_recoverable':
                case 'reg_status':
                case 'reg_typeofaction':
                case 'secties':
                case 'service_provider':
                case 'service_type':
                case 'status':
                case 'termijn':
                case 'type_managementboard':
                    $options = getListOptions($type);
                    if (!array_key_exists($value, $options)) {
                        throw new Exception("2. Submitted form entry contains an invalid value.");
                    }
                    break;

                // 4 digit integer
                case 'alleenjarenlijst':
                case 'jaar':
                    if (!preg_match("/[0-9]{4}/", $value)) {
                        throw new Exception("3. Submitted form entry contains an invalid value.");
                    }
                    break;

                // Numeric values
                case 'boardmeetings':
                case 'maanden':
                case 'newentity':
                case 'personeel':
                    if (!is_numeric($value)) {
                        throw new Exception("4. Submitted form entry contains an invalid value.");
                    }
                    break;

                // Dates
                case 'datum':
                case 'endfinyear':
                case 'datumleeg':
                case 'jarenlijst':
                    $pattern = "/^[0-9-]{10}$/";
                    if (!preg_match($pattern, $value)) {
                        throw new Exception("5. Submitted form entry contains an invalid value.");
                    }
                    break;

                // Yes or No
                case 'selectjanee':
                    if (!in_array($value, array('Ja', 'Nee'))) {
                        throw new Exception("6. Submitted form entry contains an invalid value.");
                    }
                    break;

                // Tekst
                case 'volgnummering':
                case 'bedrijfsnaam':
                case 'CLASSTYPE':
                case 'DOCTYPE':
                case 'extraveld':
                case 'land':
                case 'managementType':
                case 'oft_department':
                case 'SIGNATORY':
                case 'TRANSACTIONTYPE':
                case 'valuta':
                default:
                    $return = preg_replace("/[!#\$%\^&*\(\)<>\{\}]/i", "", $value);
            }
        }
    }
    return $return;
}

function xss_clean($data)
{

    //Single qoute eruit
    //Eruit: !@#$%^&*()+<> '"

    // Fix &entity\n;
    $data = str_replace(array('&amp;', '&lt;', '&gt;', '"', '<', '>'),
        array('&amp;amp;', '&amp;lt;', '&amp;gt;', '&amp;', '&lt;', '&gt;'), $data);
    $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
    $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
    $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

    // Remove any attribute starting with "on" or xmlns
    $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

    // Remove javascript: and vbscript: protocols
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu',
        '$1=$2nojavascript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu',
        '$1=$2novbscript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u',
        '$1=$2nomozbinding...', $data);

    // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>',
        $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>',
        $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu',
        '$1>', $data);

    $data = preg_replace('#(<[^>]+?)on[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?[\x00-\x20]*\([^>]*+>#i', '$1>', $data);

    // Remove namespaced elements (we do not need them)
    $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

    do {
        // Remove really unwanted tags
        $old_data = $data;
        $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i',
            '', $data);
    } while ($old_data !== $data);

    // we are done...
    return $data;
}

function upgradeAccount()
{
    global $pdo;

    head1();

    if (isset($_REQUEST["OPSLAAN"])) {
        $query = $pdo->prepare('update standaardwaarde SET WAARDE = "Live" WHERE NAAM = "Status";');
        if ($query->execute()) {
            $queryStandaardwaarde = $pdo->prepare('update standaardwaarde SET WAARDE = :waarde WHERE NAAM = "PAKKET";');
            $queryStandaardwaarde->bindValue('waarde', $_REQUEST["Pakket_keuze"]);
            $queryStandaardwaarde->execute();

            //Mail versturen
            //TODO: alle gegevens klant opvragen
            $headers = "From: mijn@digitaalkantoor.nl \nReply-to: mijn@digitaalkantoor.nl <mijn@digitaalkantoor.nl>\nX-mailer: DK_webmailer";
            $inhoud = "<h1>" . tl("Abbonnement Digitaal Kantoor") . "</h1>" . tl("Datum/Tijd") . ": " . date("Y-m-d H:i") . "
                          <br />" . tl("Database") . ": " . getCompanyDatabase() . "
                          <br />" . tl("Pakket keuze") . ": {$_REQUEST["Pakket_keuze"]}";

            $queryBedrijf = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = "1" LIMIT 1;');
            $queryBedrijf->execute();
            $data = $queryBedrijf->fetch(PDO::FETCH_ASSOC);

            if ($data !== false) {
                $inhoud .= "<br/>" . tl("Bedrijfsnaam") . ": " . $data["BEDRIJFSNAAM"];
                $inhoud .= "<br/>" . tl("Contactpersoon") . ": " . $data["VOORNAAM"] . " " . $data["TUSSENVOEGSEL"] . " " . $data["ACHTERNAAM"];
                $inhoud .= "<br/>" . tl("E-mail") . ": " . $data["EMAIL"];
                $inhoud .= "<br/>" . tl("Telefoon") . ": " . $data["TELVAST"];
            }

            mail_versturen("mijn@digitaalkantoor.nl", "mijn@digitaalkantoor.nl", "", "", "Digitaal Kantoor abonnement",
                $inhoud, "", "", "1", "1", "");
            rd("content.php?SITE=digi");
        }
    }

    echo("<form action=\"" . $_SERVER['REQUEST_URI'] . "\" method=\"Post\" onSubmit=\"\" >");
    //alert(this.document.algemene_voorwaarden.value); if(this.algemene_voorwaarden.value == 'Ja'){ return true; } else { alert('U bent nog niet akkoord gegaan met de algemene voorwaarden!'); return false; }
    echo("<center>
   <div align=\"left\" style=\"width: 950px;\">
   <br/>
   <img src=\"https://www.digitaalkantoor.nl/images/dklogo.png\" border=\"0\" />
   <br/><br/>
   <h2>" . tl("Proeftijd verlopen") . "</h2>
   " . tl("De proeftijd van uw Digitaal Kantoor is verlopen. U kunt verder gaan, door een van de onderstaande pakketten te kiezen.") . "
<br/><br/>
   <table class=\"mainfield\">
   <tr>
       <th>" . tl("Module") . "</th>
       <th>" . tl("Proeftijd verlengen<br/>met 2 weken?") . "</th>
       <th>" . tl("Basis") . "</th>
       <th>" . tl("Projecten") . "</th>
       <th>" . tl("Verkoop") . "</th>
       <th>" . tl("CRM") . "</th>
       <th>" . tl("Compleet") . "</th>
       <th>" . tl("Volledig") . "</th>
   </tr>
   <tr>
       <td>Uw pakket keuze</td>
       <td align=\"center\"><input type=\"radio\" name=\"Pakket_keuze\" value=\"Verleng\" /></td>
       <td align=\"center\"><input type=\"radio\" name=\"Pakket_keuze\" value=\"Basis\" /></td>
       <td align=\"center\"><input type=\"radio\" name=\"Pakket_keuze\" value=\"Projecten\" /></td>
       <td align=\"center\"><input type=\"radio\" name=\"Pakket_keuze\" value=\"Verkoop\" /></td>
       <td align=\"center\"><input type=\"radio\" name=\"Pakket_keuze\" value=\"CRM\" /></td>
       <td align=\"center\"><input type=\"radio\" name=\"Pakket_keuze\" value=\"Compleet\" checked=\"true\" /></td>
       <td align=\"center\"><input type=\"radio\" name=\"Pakket_keuze\" value=\"Volledig\" /></td>
   </tr>
   <tr>
       <td>" . tl("Betaling") . "</td><td align=\"left\" colspan=\"5\"><input type=\"radio\" name=\"betaling\" value=\"Factuur\" /> " . tl("Factuur") . " <input type=\"radio\" name=\"betaling\" value=\"Automatische incasso\" checked=\"true\" /> " . tl("Automatische incasso") . "</td>
   </tr>
   <tr>
       <td>" . tl("Prijzen") . "</td><td align=\"center\" colspan=\"5\"><i><a target=\"_blank\" href=\"http://www.digitaalkantoor.nl/\">zie www.digitaalkantoor.nl</a></td>
   </tr>
   <tr>
       <td>" . tl("Akkoord met de algemene voorwaarden?") . "</td><td align=\"left\" colspan=\"5\"><input type=\"radio\" name=\"algemene_voorwaarden\" id=\"algemene_voorwaarden\" value=\"Ja\" /> Ja <input type=\"radio\" name=\"algemene_voorwaarden\" value=\"Nee\" checked=\"true\" /> Nee</td>
   </tr>
   <tr>
       <td></td><td align=\"left\" colspan=\"5\"><input type=\"submit\" name=\"OPSLAAN\" value=\"" . tl("Aanvraag verzenden") . "\" class=\"button2\" /></td>
   </tr>
   </table>
   </div>
   </center>");
    echo("</form>");
    footer1();
}

function betaald()
{
    global $db;

    $valid = "0";
    if (getValueFromStandaardWaarde("Status") == "Proef") {
        $startdatum = getValueFromStandaardWaarde("STARTDATUM");
        $geldigTot = date("Y-m-d", strtotime("+30 day", strtotime($startdatum)));
        if (date("Y-m-d") <= $geldigTot) {
            $valid = "1";
        }
    } else {
        $valid = "1";
    }

    return $valid;
}

function statusDKAccount()
{

    global $pdo;

    $return = "";
    $query = $pdo->prepare('SELECT * FROM toegang limit 1;');
    $query->execute();
    $data_toegang = $query->fetch(PDO::FETCH_ASSOC);

    if ($data_toegang !== false) {
        //Ja er wordt geen gebruik gemaakt van een abonnement
        $query = $pdo->prepare('SELECT * FROM toegang where VERVALDATUM <= "' . date("Y-m-d") . '" AND NOGOVER > 0 ORDER BY ID limit 1;');
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            $return = "<br/><b>Status account: " . $data["NOGOVER"] . "</b>";
        }
    }
    return $return;
}

function validateQueryString($queryString, $min = 1, $max = 255)
{
    $queryString = str_replace("Cura%C3%A7ao", "Curacao", $queryString);
    //	Cura&amp;ccedil;ao

    return (preg_match("/^([a-zA-Z0-9]{" . $min . "," . $max . "}=[a-zA-Z0-9À-ÿ]{" . $min . "," . $max . "}&?)+$/",
            $queryString) > 0);
}


function securityCheckAdmin()
{
    checkSSL();

    //Check of user komt van bepaald ipadres
}

function checkSSL()
{
    //Check of user wel inlogt via https
    require_once("./config/config.php");
    $url = selfURL();

    if (config("config_ssl") == "true" && strpos($url, '.localhost') === false) {

        $https = substr($url, 0, 5);

        if ($https != "https") {
            $url = str_replace("http", "https", $url);
            header("Location: $url");
            exit;
        }
    }
}

function selfURL()
{
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = gc_strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/") . $s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":" . $_SERVER["SERVER_PORT"]);
    return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
}

function gc_strleft($s1, $s2)
{
    return substr($s1, 0, strpos($s1, $s2));
}

function getValidatedLoginID()
{

    $loginid = 0;
    if (!isset($_SESSION)) {
        session_start();
    }
    if (isset($_SESSION['LID'])) {
        $loginid = getLoginID();
    }

    return $loginid * 1;
}

function setLoginID($loginid)
{

    if (!isset($_SESSION)) {
        session_start();
    }

    $db_hash = substr(md5(date("Y-m-d--H:i:s")), 0, 50);
    $db_hash = str_replace("\'", "", $db_hash);
    $db_hash = str_replace("\\", "", $db_hash);
    $db_hash = ps($db_hash);
    $_SESSION['LID'] = $db_hash;
    $_SESSION['IP'] = $_SERVER["REMOTE_ADDR"];

    return $db_hash;
}

function getLoginID()
{

    if (!isset($_SESSION)) {
        session_start();
    }
    if (!isset($_SESSION['tmp_LID']) || $_SESSION['tmp_LID'] == "0") {
        require_once("dbConnecti.php");
        global $pdo;

        $loginid = 0;

        if (isset($_SESSION['LID'])) {

            $query = $pdo->prepare('select LOGINID from ingelogt where SESSIONID = :sessionid limit 1;');
            $query->bindValue('sessionid', $_SESSION['LID']);
            $query->execute();
            $login = $query->fetch(PDO::FETCH_ASSOC);

            if ($login !== false) {
                $loginid = $login["LOGINID"]; //zorgt ervoor dat je een legaal cookie niet kan gebruiken op andere DB.
                $_SESSION['tmp_LID'] = $loginid;
            }
        }
        return $loginid * 1;
    } else {
        return $_SESSION['tmp_LID'] * 1;
    }
}

function getDBHash()
{
    $getDBHash = "";

    if (isset($_SESSION['DB'])) {
        $getDBHash = getCompanyDatabase();
        $getDBHash = md5($getDBHash);
    }

    return $getDBHash;
}

function getCompanyDatabase()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    return $_SESSION['DB'];
}

function setCompanyDatabase($hash)
{
    if (!isset($_SESSION)) {
        session_start();
    }
    $_SESSION['DB'] = $hash;
}

function setAID($hash)
{
    setcookie("AID", "$hash", (time() + 3600 * 24 * 365), "/");
}

function getAID()
{
    return $_COOKIE['AID'];
}

function cleanData($data)
{
    $data = trim($data);
    return $data;
}

function cleanDataForDB($data)
{
    $data = trim(htmlentities(strip_tags($data)));

    if (get_magic_quotes_gpc()) {
        $data = stripslashes($data);
    }

    $data = mysql_real_escape_string($data);

    return $data;
}

function SMSUsername()
{
    global $pdo;

    if (config_smsUser() != "Geen") {
        return config_smsUser();
    } else {
        $query = $pdo->prepare('SELECT * FROM standaardwaarde WHERE ID = ("9"*1) LIMIT 1;');
        $query->execute();
        $data_stand = $query->fetch(PDO::FETCH_ASSOC);
        return $data_stand["WAARDE"];
    }
}

function SMSPassword()
{
    global $pdo;

    if (config_smsPass() != "Geen") {
        return config_smsPass();
    } else {
        $query = $pdo->prepare('SELECT * FROM standaardwaarde WHERE ID = ("10"*1) LIMIT 1;');
        $query->execute();
        $data_stand = $query->fetch(PDO::FETCH_ASSOC);
        return $data_stand["WAARDE"];
    }
}

function nieuweCodeGen()
{
    $code = chr(mt_rand()) . date("mihs");
    $code = substr(md5($code), 0, 8);

    return $code;
}

function sleutel()
{

    $sleutel = "1234";
    if (isOrangeField()) {
        $sleutel = date("dH") . $_SERVER["REMOTE_ADDR"];
        $sleutel = md5($sleutel);
        $sleutel = substr($sleutel, 0, 20);
    }
    return $sleutel;
}

function checkRechten($rechten, $leesOfSchrijf)
{
    if ($leesOfSchrijf == "add") {
        $leesOfSchrijf = "Ja";
    } elseif ($leesOfSchrijf == "read") {
        $leesOfSchrijf = "Lees";
    } else {
        $leesOfSchrijf = "Mee";
    }

    if (!isRechten($rechten, $leesOfSchrijf)) {
        echo melding("U heeft niet voldoende rechten om deze pagina te bekijken");
        exit;
    }
}

function getLoginHash()
{
    global $pdo;

    if (!isset($_SESSION)) {
        session_start();
    }

    if (!isset($_REQUEST["H"])) {
        require_once("dbConnecti.php");
    }

    $hash = "0";
    if (isset($_REQUEST['X'])) {
        $hash = $_REQUEST['X'];
    } elseif (isset($_SESSION['LID']) && $_SESSION['LID'] != '' && $_SESSION['LID'] != 0) {
        $query = $pdo->prepare('select SESSIONID from ingelogt where SESSIONID = :sessionid limit 1;');
        $query->bindValue('sessionid', $_SESSION['LID']);
        $query->execute();
        $login = $query->fetch(PDO::FETCH_ASSOC);

        if ($login !== false) {
            $hash = $login["SESSIONID"]; //zorgt ervoor dat je een legaal cookie niet kan gebruiken op andere DB.
        }
    }

    if ($hash == '0') {
        $query = $pdo->prepare('select SESSIONID from ingelogt where LOGINID = :loginid ORDER BY ID DESC limit 1;');

        $loginId = getLoginId();
        $query->bindValue('loginid', $loginId);
        $query->execute();
        $login = $query->fetch(PDO::FETCH_ASSOC);

        if ($login !== false) {
            $hash = $login["SESSIONID"]; //zorgt ervoor dat je een legaal cookie niet kan gebruiken op andere DB.
        }
    }

    $hash .= "&DB=" . getCompanyDatabase();

    if (isset($_REQUEST["WEBSITE"])) {
        $hash .= "&WEBSITE=" . $_REQUEST["WEBSITE"];
    }

    return $hash;
}

function mayAccessSystem($maxAccess = 5)
{
    //if(isSMSSpammer($maxAccess) || isSpammer($maxAccess)) {
    //  http_response_code(403);
    //  echo '<h2>Too many login attempts</h2><p>You have been locked out for 5 minutes due to too many login attempts.</p>';
    //  die;
    //}
}

function isSpammer($maxAccess)
{
    global $pdo;

    if ($pdo == null) {
        return false;
    }

    $query = $pdo->prepare('SELECT * FROM login_failed WHERE
          LOGINIP = :loginip
          AND LOGINATTEMPT >= :loginattempt
          AND LASTLOGIN >= "' . date('Y-m-d H:i:s', strtotime('-5 mins')) . '";');
    $query->bindValue('loginip', $_SERVER['REMOTE_ADDR']);
    $query->bindValue('loginattempt', $maxAccess);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    return $data !== false;
}

function isSMSSpammer($maxAccess)
{
    global $pdo;

    if ($pdo == null) {
        return false;
    }

    $query = $pdo->prepare('SELECT * FROM login_token_request WHERE
          LOGINIP = :loginip
          AND TOKENREQUESTS >= :tokenrequests
          AND LASTLOGIN >= "' . date('Y-m-d H:i:s', strtotime('-5 mins')) . '";');
    $query->bindValue('loginip', $_SERVER['REMOTE_ADDR']);
    $query->bindValue('tokenrequests', $maxAccess);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    return $data !== false;
}

function resetFailedLogins($ip, $username)
{
    global $pdo;

    $query = $pdo->prepare('DELETE FROM login_failed
    WHERE LOGINIP = :loginip
    AND LOGINNAME = :loginname;');
    $query->bindValue('loginip', $ip);
    $query->bindValue('loginname', $username);
    $query->execute();
}

function addFailedLogins($ip, $username)
{
    global $pdo;
    $currentDate = date('Y-m-d H:i:s');
    $query = $pdo->prepare('INSERT INTO login_failed
  (LOGINIP, LOGINNAME, LOGINATTEMPT, LASTLOGIN)
  VALUES
    (:ip,:username,  1, :currentdate)
  ON DUPLICATE KEY UPDATE
    LOGINNAME = VALUES(LOGINNAME),
    LOGINATTEMPT = LOGINATTEMPT + 1,
    LASTLOGIN = VALUES(LASTLOGIN);');
    $query->bindValue('ip', $ip);
    $query->bindValue('username', $username);
    $query->bindValue('currentdate', $currentDate);

    $query->execute();
}

function resetTokenRequest($ip, $username)
{
    global $pdo;
    $query = $pdo->prepare('DELETE FROM login_token_request
    WHERE LOGINIP = :loginip
    AND LOGINNAME = :loginname;');
    $query->bindValue('loginip', $ip);
    $query->bindValue('loginname', $username);

    $query->execute();
}

function addTokenRequest($ip, $username)
{
    global $pdo;
    $currentDate = date('Y-m-d H:i:s');

    $query = $pdo->prepare('INSERT INTO login_token_request
  (LOGINIP, LOGINNAME, TOKENREQUESTS, LASTLOGIN)
  VALUES
    (:ip,:username,  1, :currentdate)
  ON DUPLICATE KEY UPDATE
    LOGINNAME = VALUES(LOGINNAME),
    TOKENREQUESTS = TOKENREQUESTS + 1,
    LASTLOGIN = VALUES(LASTLOGIN);');
    $query->bindValue('ip', $ip);
    $query->bindValue('username', $username);
    $query->bindValue('currentdate', $currentDate);

    $query->execute();

}


function getTableData($table, $skip = array())
{
    global $pdo, $db;

    //if($rechten == 'MASTER' || isMaster()) {
    $notAllowed = array();
    //}

    $data = array();
    if (in_array($table, $notAllowed)) {
        return $data;
    }

    $query = $pdo->prepare('DESCRIBE ' . mysqli_real_escape_string($db, $table));
    $query->execute();

    foreach ($query->fetchAll() as $row) {
        if (!in_array($row['Field'], $skip)) {
            $data[$row['Field']] = $row;
        }
    }
    return $data;
}

function getRechten($userid, $column) {
    global $pdo;
    /*
     UITGEVOERDE QUERIES:
        ALTER TABLE LOGIN ADD `MA` VARCHAR(10) DEFAULT 'Ja' AFTER `CONTRACTS`;
        ALTER TABLE LOGIN ADD `INTEGRATIONCHECKLIST` VARCHAR(10) DEFAULT 'Ja' AFTER `MA`;

        UPDATE login SET BESTANDEN = 'Ja';
        UPDATE login SET CRM = 'Ja';
        UPDATE login SET CONTRACTS = 'Ja';
        UPDATE login SET COMPLIANCE = 'Ja';
     */
    $stmt = $pdo->prepare("SELECT $column FROM login WHERE PERSONEELSLID = $userid LIMIT 1;");
//    $stmt = $pdo->bindValue("id", $userid);
    $stmt->execute();
    $result = $stmt->fetch($pdo::FETCH_ASSOC);
    if (strpos($column, ',') !== false) {
        return $result;
    }
    if($result[$column] == "Nee") {
        $_SESSION['Error_Rechten'] = "You don't have the rights to visit this page. \n U heeft niet voldoende rechten deze pagina te bezoeken.";
        header("Location: content.php?SITE=entity_home");
        exit;
    }
}
