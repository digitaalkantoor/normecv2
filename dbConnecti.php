<?php
require_once("toegang.php");

if(isset($db_inc)) return;
else $db_inc = TRUE;

$db = db_open();

function db_hash_get() {
  $hash = "";

  if(isset($_SESSION['DB']) && trim($_SESSION['DB']) != '') {
     $hash = $_SESSION['DB'];
  } elseif (isset($_REQUEST["D"]) && trim($_REQUEST['D']) != '') {
     $hash = trim($_REQUEST["D"]);
     if(!isset($_SESSION)) {
       session_start();
     }
     $_SESSION['DB'] = $hash;
  } elseif (isset($_REQUEST["Database"]) && trim($_REQUEST['Database']) != '') {
     $hash = trim($_REQUEST["Database"]);
     if(!isset($_SESSION)) {
       session_start();
     }
     $_SESSION['DB'] = $hash;
  }

  if($hash == null || $hash == "") {
    $hash = "1234";
  }

  $hash = strtolower(trim($hash));

  //echo "HASH: ". $hash;
  //exit;
  
  return trim($hash);
}

function db_open()
{
    global $db;

    if(!isset($_SESSION)) {
      session_start();
    }

    $toev = date("dhym");
    $hash = db_hash_get();

    $administraties = getAdministraties();
    if(!isset($administraties[$hash])) {
      reset($administraties);
      $hash = key($administraties);
    }

    $dbConn = $administraties[$hash];
    $dbHost = "172.22.0.117";
    if(isset($dbConn["dbhost"])) { $dbHost = $dbConn["dbhost"]; }
    $dbUser = $dbConn["dbuser"];
    $dbPass = $dbConn["dbpass"];
    $dbNaam = $dbConn["dbnaam"];
    
    //echo "$hash: $dbHost, $dbUser, $dbPass, $dbNaam";
    $db = new mysqli($dbHost, $dbUser, $dbPass, $dbNaam);

    db_query("SET CHARACTER SET UTF8", $db);
    db_query("SET NAMES UTF8", $db);
  
    if (mysqli_connect_error())
    {
      die('DB Error 002');
      //die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
    }
  
    return $db;
}

function db_close($link = null) {
  if($link) { $db = $link; } else { global $db; }
  return $db -> close();
}

function db_error($link = null) {
  if($link) { $db = $link; } else { global $db; }
  return $db -> error;
}

function db_query($query, $link = null)
{
  if($link) { $db = $link; } else { global $db; }

  $start = microtime(true);
  if($query != "")
  {
    $result = $db -> query($query);
    if (!$result)
    {
      if(config("config_debug") == "true") {
        echo("Fout in: $query <br/>DB error: " . db_error($db)); exit;
      } else {
        die('DB Error 003');
      }
    }
  }
  else
  {
    $result = null;
  }

  //Hier wordt de tijd die de querie gebruikt opgeslagen
  if(isset($_REQUEST["QD"])) {
    $end = microtime(true) - $start;
    if($end > $_REQUEST["QD"] && $end > 0.0001)
    {
      $file = "./bestanden/querylog.txt";
      if (!$file_handle = @fopen($file,"a+")) { echo "Cannot open file"; }
      if (!@fwrite($file_handle, "$end - $query\n")) { echo "Cannot write to file"; }
      @fclose($file_handle);
    }
  }

  return $result;
}


function db_num_rows($res) 
{
  return $res->num_rows;
}

function db_insert_id($link = null) {
  if($link) { $db = $link; } else { global $db; }
	return mysqli_insert_id($db);
}

function db_fetch_row($res) {
	return $res -> fetch_row();
}

function db_fetch_assoc($res) {
	return $res -> fetch_assoc();
}
function db_fetch_array($res) {
	return $res -> fetch_array();
}

function db_real_escape_string($res)
{
        global $db;
        return $db -> real_escape_string($res);
}

//------------------------------------------------------------nieuwe functies zoals echte prepared statements - ----------------------------------------------------- - - - - - - - 

/*
Uitleg van de db_ps_...() functies:

ze werken net zo als de bovenstaande functies, behalve de db_ps_query:
dat is een samentrekking van prepair() en de bind_param() functie.
voor uitleg van die functies zie:

http://php.net/manual/en/mysqli-stmt.bind-param.php
http://www.php.net/manual/en/mysqli-stmt.prepare.php

zoals bind_param geen vast aantal parameters heeft, zo heeft deze functie ook geen vast aantal parameters.

de opbouw is als volgt:

db_ps_query($query,$types,$var1,$var2, etc...)

als je met blobs gaat werken zijn de volgende punten nog belangrijk:

1. de blob wordt automatisch herkent en wordt in stukken naar de server gestuurt.
2. helaas kan de functie max 1 blob aan.(hier werk ik nog aan)
3. waar integers, doubles en strings van allerlei typen mogen zijn is het voor een blob verplicht dfat het een file stream is(zoals een fopen).

*/

function db_ps_query($query,$type)
{
  global $db;
  $start = microtime(true);

  $stmt = $db -> stmt_init();
  if($stmt -> prepare($query))
  {
    if($stmt -> param_count == func_num_args() - 2)
    {
      $args = func_get_args();
      unset($args[0]);
      $args = array_values($args);
      $refs = array();
      
      foreach($args as $key => $value)
      {
        $refs[$key] =& $args[$key];
      }

      $blobs = array();
      $offset = 0;
      $blob = strpos($type,'b',$offset);
      while($blob !== false)
      {
        $data = $args[$blob + 1];
        $blobs[] = array($blob,$data);
        $args[$blob + 1] = null;
        
        $offset = $blob + 1;
        $blob = strpos($type,'b',$offset);
      }
      
      call_user_func_array(array($stmt,'bind_param'),$refs);

      foreach($blobs as $blob)
      {
         $data = $blob[1];
         $blob = $blob[0];
         fseek($data, 0);
         while(!feof($data))
         {
           $stmt -> send_long_data($blob,fread($data,1024));
         }
      }

      if($stmt -> execute())
      {
        $stmt -> store_result();
      }
      else
      {
        //echo("Fout in: $query <br/>DB error: " . $stmt -> error); exit;
        //throw new Exception("Fout in: $query <br/>\nDB error: " . $stmt -> error);
        throw new Exception("DB error 004");
      }
    }
    else
    {
      //die("Fout in: $query <br/>DB error: Ongeldig aantal argumenten in functie db_ps_query()");
      //throw new Exception("Fout in: $query <br/>\nDB error: Ongeldig aantal argumenten in functie db_ps_query()");
      throw new Exception("DB error 005");
    }
  }
  else
  {
      //throw new Exception("Fout in: $query <br/>\nDB error: " . $stmt -> error);
      throw new Exception("DB error 006");
      //echo("Fout in: $query <br/>DB error: " . $stmt -> error); exit;
  }

  /*
  $end = microtime(true) - $start;
  $file = "./querylog.txt";
  if (!$file_handle = @fopen($file,"a+")) { echo "Cannot open file"; }
  if (!fwrite($file_handle, "\n\n".$query . "\n" . $end)) { echo "Cannot write to file"; }
  @fclose($file_handle);
  */

  return $stmt;
}

function db_ps_fetch($stmt,$type = 3)
{
  $fields = $stmt -> result_metadata() -> fetch_fields();
  $keys = array();
  $values = array();
  $refs = array();

  foreach($fields as $field)
  {
    $keys[]   = $field -> name;
    $values[] = "";
    $refs[]   =& $values[count($values) -1 ];
  }

  call_user_func_array(array($stmt,'bind_result'),$refs);

  if($stmt -> fetch())
  {
    $result = array();
    foreach($keys as $i => $key)
    {
      if($type % 2 == 1)
      {
        $result[$i]   = $values[$i];
      }
      
      if($type > 1)
      {
        $result[$key] = $values[$i];
      }
    }

    return $result;
  }
  else
  {
    return false;
  }
}

function db_ps_fetch_row($stmt)
{
  return db_ps_fetch($stmt,1);
}

function db_ps_fetch_assoc($stmt)
{
  return db_ps_fetch($stmt,2);
}

function db_ps_fetch_array($stmt)
{
  return db_ps_fetch($stmt,3);
}

function db_ps_num_rows($stmt)
{
  return $stmt -> num_rows;
}

function db_ps_insert_id($stmt)
{
  return $stmt -> insert_id;
}

function db_ps_foutmelding($exception) {

  //$end = microtime(true) - $start;
  //$file = "./errorlog.txt";
  //if (!$file_handle = @fopen($file,"a+")) { echo "Cannot open file"; }
  //if (!fwrite($file_handle, $exception->getMessage() . "\n")) { echo "Cannot write to file"; }
  //@fclose($file_handle);

  echo $exception->getMessage(), "\n";
}

set_exception_handler('db_ps_foutmelding');

function initPDO() {
  global $pdo;
  
  if(!isset($_SESSION)) {
    session_start();
  }

  $hash = db_hash_get();

  $administraties = getAdministraties();
  if(!isset($administraties[$hash])) {
    reset($administraties);
    $hash = key($administraties);
  }

  $dbConn = $administraties[$hash];
  $dbHost = "172.22.0.117";
  if(isset($dbConn["dbhost"])) { $dbHost = $dbConn["dbhost"]; }
  $dbUser = $dbConn["dbuser"];
  $dbPass = $dbConn["dbpass"];
  $dbNaam = $dbConn["dbnaam"];

  try {
      $pdo = new PDO('mysql:host=' . $dbHost . ';dbname=' . $dbNaam, $dbUser, $dbPass);
      $pdo->query('SELECT * from FOO');
  } catch (PDOException $e) {
      print "DB 001";
      die();
  }

  $pdo->query('SET CHARACTER SET UTF8');
  $pdo->query('SET NAMES UTF8');

  return $pdo;
  
}

$pdo = initPDO();

