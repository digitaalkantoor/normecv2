<?php

include_once __DIR__.'/../../libraries/opencloud/autoload.php';
include_once __DIR__.'/../../includes.php';

use OpenCloud\ObjectStore\Resource\DataObject;

class CloudVPS
{
    private $client;
    private $service;
    private $container;
    public function __construct()
    {
        $this->client = new OpenCloud\OpenStack('https://identity.stack.cloudvps.com/v2.0', array(
                'username' => 'bob.salm@minded.nl',
                'password' => 'TmuMs2ZRsuSLzn8J',
                'tenantId' => 'fd0b40dba1174144b1d01473a926c670',
        ));

        $this->service = $this->client->objectStoreService('swift', 'NL', 'publicURL');

        $selectedBedrijfsid = getBedrijfsID(getValidatedLoginID());
        if(isset($_REQUEST["selectedBedrijfsid"]) && $_REQUEST["selectedBedrijfsid"] > 0) {
          $selectedBedrijfsid = $_REQUEST["selectedBedrijfsid"] * 1;
        }

        $this->container = $this->getContainer($selectedBedrijfsid);
    }

    /**
     * Geeft file object terug.
     *
     * @param int $id
     *
     * @return object $file
     */
    public function getFile($id)
    {
        try {
            $object = $this->container->getObject($id);
            $content = $object->getContent();
            $content->rewind();

            $file = new stdClass();
            $file->stream = $content->read($content->getContentLength());
            $file->size = $object->getContentLength();
            $file->type = $object->getContentType();

            return $file;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getSizeContainer($containerName) {
      try {
        $container = $this->service->getContainer($containerName);

        $usage = new stdClass();
        $usage->inBytes = $container->getBytesUsed();

        $base = log($usage->inBytes, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');

        $usage->inHuman = round(pow(1024, $base - floor($base)), 2) .' '. $suffixes[(int) floor($base)];

        return $usage;
      } catch (Exception $e) {
        return $e;
      }
    }

    /**
     * Zet bestand op cloudVPS en verwijderd deze lokaal.
     *
     * @param string $pathtofile
     * @param string $nameFile
     * @param string $stream
     */
    public function setFile($file, $mimetype, $filename = null)
    {
        $lastIdRes = $this->container->getMetadata()->toArray();
        if (!isset($lastIdRes ['currentid'])) {
          $name = 1;
          $this->container->saveMetadata(array(
            'currentid' => $name,
          ));
        } else {
            $lastIdRes['currentid'] = $this->checkHighestCloudVPSID( $lastIdRes['currentid'] );
            $name = (int) $lastIdRes['currentid'];
        }

        $metadata = array(
          'id' => $name,
          'name' => $filename,
        );
        $httpHeaders = array(
          'Content-Type' => $mimetype,
        );

        $allHeaders = array_merge(DataObject::stockHeaders($metadata), $httpHeaders);
        $this->container->uploadObject(strval($name), $file, $allHeaders);

        $newObjectName = $name + 1; //TODO: Wat gebeurd er al je teglijk upload?
        //$newObjectName = uniqid();

        $this->container->saveMetadata(array(
          'currentid' => $newObjectName,
        ));

        $exists = $this->container->objectExists($name);
        if($exists) {
          //echo "<br/>bestaat " . $name;
          return $name;
        } else {
          echo "<br/>bestaat niet " . $name;
          //var_dump($this->container);
          return 0;
        }
    }

    /**
     * Update een bestand, deze word eerst geupload en dan pas verwijderd.
     *
     * @param object         $file     FileStream van nieuwe best@minded01and
     * @param string         $mimetype Mimetype van het bestand
     * @param string         $filename Optioneel filename van nieuwe bestand
     * @param int            $oldId    Oude CloudVPS identifier, als deze gezet is
     *                                 word het bestand van cloudvps verwijderd.
     *                                 Let op het archief gebruikt identifier nog steeds.
     *
     * @return int Geeft file identifier van CloudVPS standaard terug
     */
    public function updateFile($file, $mimetype, $filename = null, $oldId = null)
    {
        $fileId = $this->setFile($file, $mimetype, $filename);
        if (isset($oldId)) {
            $this->deleteFile($oldId);
        }

        return $fileId;
    }

    /**
     * Geeft de bedrijfscontainer van een bedrijf op, als deze niet bestaat
     * word deze gelijk aangemaakt.
     *
     * @param int $bedrijfsId bedrijfsid
     * @param int $database   deze kan optioneel overschreven worden om een andere
     *                        database aan te geven. Word gebruikt in de bulkoverwrite functie
     *
     * @return object container
     */
    private function getContainer($bedrijfsId, $database = null)
    {
      if(isset($database)) {
        $db = $database;
      } else  {
        if($db_fetch = db_fetch_assoc(db_query('SELECT DATABASE() FROM DUAL'))) {
          $db = $db_fetch['DATABASE()'];
        }
      }
      $useAlwaysDB = "";
      if($db_fetchdb = db_fetch_assoc(db_query("SELECT WAARDE FROM standaardwaarde WHERE NAAM = 'cloudvps_database';"))) {
        $useAlwaysDB = $db_fetchdb['WAARDE'];
        if($useAlwaysDB != '') {
          $db = $useAlwaysDB;
        }
      }

        $container = null;
        try {
            $container = $this->service->getContainer($db.'-'.$bedrijfsId);
        } catch (Exception $e) {
            $container = $this->service->createContainer($db.'-'.$bedrijfsId);
        }

        return $container;
    }

    /**
     * Verwijder een bestand van CloudVPS.
     *
     * @param int $fileId CloudVPS id van bestand (Dus anders dan documentid)
     *
     * @return bool Geeft resultaat van actie terug
     */
    public function deleteFile($fileId)
    {
        try {
            $this->container->deleteObject(strval($fileId));

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Verwijderd container van CloudVPS schijf.
     *
     * @param int $bedrijfsId bedrijfid
     *
     * @return bool Geeft resultaat van actie terug
     */
    public function deleteContainer($bedrijfsId)
    {
        try {
            $container = $this->getContainer($bedrijfsId);
            $container->deleteAllObjects();
            $container->delete();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Met het bulk invoeren van bestanden moet je een klant omgeving emuleren
     * hiermee kunnen deze waardes gezet worden. Eerst deze gebruiken voordat
     * je bestanden upload.
     *
     * @param int    $bedrijfsId id van het bedrijf dat geemuleerd moet worden
     * @param string $database   naam van de gebruike database
     */
    public function setCustomConfig($bedrijfsId, $database)
    {
        $this->container = $this->getContainer($bedrijfsId, $database);
    }

    /**
     * Geeft het gebruik van een bedrijf is bytes of in een menselijk leesbare string
     *
     * @param $bedrijfid
     * @return stdClass
     */
    public function getUsageBedrijf($bedrijfid)
    {
        $container = $this->getContainer($bedrijfid);
        $usage = new stdClass();
        $usage->inBytes = $container->getBytesUsed();

        $base = log($usage->inBytes, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');

        $usage->inHuman = round(pow(1024, $base - floor($base)), 2) .' '. $suffixes[(int) floor($base)];
        return $usage;
    }

    public function getHighestLocalID() {
        $return = 1;
        $sql = "SELECT CLOUDVPS FROM documentbeheer ORDER BY CLOUDVPS DESC LIMIT 1";
        $result = db_fetch_array( db_query( $sql ) );
        if ( $result ) {
          $return = $result['CLOUDVPS'];
        }

        // $sql = "SELECT CLOUDVPS FROM mail_attachments ORDER BY CLOUDVPS DESC LIMIT 1";
        // $result = db_fetch_array( db_query( $sql ) );
        // if ( $result ) {
        //   if($result['CLOUDVPS'] > $return) {
        //     $return = $result['CLOUDVPS'];
        //   }
        // }

        return $return;
    }

    public function checkHighestCloudVPSID( $cloudvps_id ) {
        $local_id = $this->getHighestLocalID();
        if ( $cloudvps_id >= $local_id ) {
            return $cloudvps_id;
        }
        $this->container->saveMetadata( array( 'currentid' => $local_id ) );
        return $local_id;
    }
}
