<?php 
$data = array(
		'document_type'		=> 'P01',
		'connection'		=> array(
				'AuthenticationKey'		=> 'b51f7ade602d27747eea55efa849168f567d2a15',
				'LayoutType'			=> 'P01',
				'LayoutVersion'			=> '2.2',
				'LayoutPlatform'		=> 'L01' ),
		'order'				=> array(
				/* Shipment info */
				'OrderNumber'			=> '001-01',
				'OrderReference'		=> 'MD-001-01',
				'OrderContent'			=> 'cookie',
				'ShippingMethod'		=> 'PPLUSST',
				'CODValue'				=> 20.2583764,
				'CODCurrency'			=> 'EURO',
				'Currency'				=> 'EURO',
				'TYPNumber'				=> '',
				/* Adress info */
				'AddressType'			=> 'DL1',
				'ConsigneeName'			=> 'Kaspar Smoolenaars',
				'CompanyName'			=> 'Mijn Huis',
				'Street'				=> 'Hekbootstraat',
				'AdditionalAddressInfo'	=> '',
				'HouseNumber'			=> 6,
				'HouseNumberExtension'	=> 'D',
				'CityOrTown'			=> 'Rotterdam',
				'StateOrProvince'		=> 'Zuid Holland',
				'ZIPCode'				=> '3028XH',
				'CountryCode'			=> 'NLD',
				/* Contact info */
				'PhoneNumber'			=> '010 2238 256',
				'SMSNumber'				=> '06 120 70 454',
				'EmailAddress'			=> 'kaspar.smoolenaars@gmail.com',
				'PersonalNumber'		=> '06 120 70 454',
				/* Validation */
				'TotalShipments'		=> 1,
				'MailAddressConfirmation' => 'kaspar.smoolenaars@gmail.com' ),
		/* Packages */
		'packages'			=> array(	array(
				'PackageBarcode'		=> 'BarCode',
				'PackageNumber'			=> 1,
				'PackageWeight'			=> 350,
				'DimensionHeight'		=> 15,
				'DimensionWidth'		=> 40,
				'DimensionLength'		=> 30 ) ) );

