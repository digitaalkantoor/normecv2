<?php
class B2CException extends Exception {
}
class B2C_Message {
	const LABEL_MASK_REGEX = '/%%([a-z][a-z0-9_]*)%%/i';
	const LABEL_LAYOUT = <<<B2C_LABEL
<div class="label">';
	<div id="A" class="logo">
		<img src="%%LOGO%%"/>
	</div>
	<div id="B" class="order_number">
		<span class="label">Order Number</span>
		<span class="value">%%OrderNumber%%</span>
	</div>
	<div id="C" class="typ_number">
		<span class="label">TYP Number</span>
		<span class="value">%%TYPNumber%%</span>
	</div>
	<div id="D" class="shipper">
		<span class="label">Shipper</span><br/>
		<span class="value">%%Address_line_1%%</span>
		<span class="value">%%Address_line_2%%</span>
		<span class="value">%%Address_line_3%%</span>
		<span class="value">%%Address_line_4%%</span>
		<span class="value">%%Address_line_5%%</span>
	</div>
	<div id="E" class="consignee">
		<span class="label">Consignee</span>
		<span class="value">%%CompanyName%%</span>
		<span class="value">%%ConsigneeName%%</span>
		<span class="value">%%Street%% %%HouseNumber%% %%HouseNumberExtension%%</span>
		<span class="value">%%CityOrTown%% %%ZIPCode%% %%StateOrProvince%%</span>
		<span class="value">%%CountryCode%%</span>
	</div>
	<div id="F" class="barcode">
		<img src="%%Barcode%%"/>
	</div>
	<div id="G" class="package_barcode">
		<img src="%%PackageBarcode%%"/>
	</div>
	<div id="H" class="free_space">
		<span class="label">%%FreeSpace%%</span>
	</div>
	<div id="I" class="shipping_method">
		<span class="label">Shipping Method</span>
		<span class="value">%%ShippingMethod%%</span>
	</div>
</div>
B2C_LABEL;
	const LABEL_STYLE = <<<B2C_LABEL_STYLE
.label {
	position:absolute;
	width:104mm;
	height:150mm;
	padding:0;
	margin:0;
	border-width:0;
	font-family:Arial;
	font-size:10pt;
	font-color:#000;
}
.logo {
	top:10mm;
	left:5mm;
}
.order_number {
	top:28mm;
	left:5mm;
	right:65mm;
	width:60mm;
}
.typ_number {
	top:39mm;
	left:5mm;
	right:65mm;
	width:60mm;
}
.shipper {
	top:50mm;
	left:5mm;
	right:65mm;
	width:60mm;
}
.consignee {
	top:76.5mm;
	left:5mm;
	right:65mm;
	width:60mm;
}
.barcode {
	margin-top:105mm;
	margin-left:5mm;
	margin-right:5mm;
	width:90mm;
}
.package_barcode {
	margin-top:127mm;
	margin-left:5mm;
	margin-right:5mm;
	width:90mm;
}
.free_space {
	top:10mm;
	left:67mm;
	right:4mm;
	width:33mm;
			
}
.shipping_method {
	top:28mm;
	left:67mm;
	right:4mm;
	width:33mm;
}
B2C_LABEL_STYLE;
	protected static $shipping_codes = array( 
			'ParcelPLUS standard' => 'PPLUSST', 
			'ParcelPLUS' => 'PPLUS', 
			'Standaard Parcel' => 'STAN', 
			'Standaard Express' => 'STEX' 
	);
	protected static $xml_struct = array( 
			'P01' => array( 
					'objects' => array( 
							'Prealert' => '1' 
					), 
					'Prealert' => array( 
							'objects' => array( 
									'Shipment' => '+', 
									'PrealertValidation' => '1' 
							), 
							'req' => true, 
							'data_provider' => 'connection', 
							'fields' => array( 
									'AuthenticationKey' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true, 
											'ref' => '01' 
									), 
									'LayoutType' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true, 
											'ref' => '02' 
									), 
									'LayoutVersion' => array( 
											'type' => 'text', 
											'max' => 5, 
											'req' => true 
									), 
									'LayoutPlatform' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									), 
									'PrealertReference' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => false 
									) 
							) 
					), 
					'Shipment' => array( 
							'objects' => array( 
									'ShipmentContent' => '*', 
									'ShipmentAddress' => '1', 
									'ShipmentContact' => '1', 
									'ShipmentPackage' => '+', 
									'CollectYourParcel' => '?' 
							), 
							'req' => true, 
							'data_provider' => 'order', 
							'fields' => array( 
									'OrderNumber' => array( 
											'type' => 'text', 
											'max' => 25, 
											'req' => true 
									), 
									'OrderReference' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => false 
									), 
									'OrderContent' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'ShippingMethod' => array( 
											'type' => 'text', 
											'max' => 10, 
											'req' => true 
									), 
									'CODValue' => array( 
											'type' => 'dec', 
											'max' => 6.2, 
											'req' => false 
									), 
									'CODCurrency' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => false 
									), 
									'Currency' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => false 
									), 
									'TYPNumber' => array( 
											'type' => 'text', 
											'max' => 15, 
											'req' => false 
									) 
							) 
					), 
					'ShipmentContent' => array( 
							'req' => false, 
							'data_provider' => '', 
							'fields' => array( 
									'PackageNumber' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'SKUCode' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => true 
									), 
									'SKUDescription' => array( 
											'type' => 'text', 
											'max' => 2000, 
											'req' => true 
									), 
									'Quantity' => array( 
											'type' => 'num', 
											'req' => true 
									), 
									'Price' => array( 
											'type' => 'dec', 
											'max' => 6.2, 
											'req' => true 
									), 
									'Category' => array( 
											'type' => 'text', 
											'max' => 100, 
											'req' => false 
									), 
									'ImageUrl' => array( 
											'type' => 'text', 
											'max' => 1000, 
											'req' => false 
									) 
							) 
					), 
					'ShipmentAddress' => array( 
							'req' => true, 
							'data_provider' => 'order', 
							'fields' => array( 
									'AddressType' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									), 
									'ConsigneeName' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true 
									), 
									'CompanyName' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'Street' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true 
									), 
									'AdditionalAddressInfo' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'HouseNumber' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'HouseNumberExtension' => array( 
											'type' => 'text', 
											'max' => 10, 
											'req' => false 
									), 
									'CityOrTown' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true 
									), 
									'StateOrProvince' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'ZIPCode' => array( 
											'type' => 'text', 
											'max' => 12, 
											'req' => true 
									), 
									'CountryCode' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									) 
							) 
					), 
					'ShipmentContact' => array( 
							'req' => true, 
							'data_provider' => 'order', 
							'fields' => array( 
									'PhoneNumber' => array( 
											'type' => 'text', 
											'max' => 20, 
											'req' => false 
									), 
									'SMSNumber' => array( 
											'type' => 'text', 
											'max' => 20, 
											'req' => false 
									), 
									'EmailAddress' => array( 
											'type' => 'text', 
											'max' => 50, 
											'req' => true 
									), 
									'PersonalNumber' => array( 
											'type' => 'text', 
											'max' => 20, 
											'req' => false 
									) 
							) 
					), 
					'ShipmentPackage' => array( 
							'req' => true, 
							'data_provider' => 'packages', 
							'fields' => array( 
									'PackageBarcode' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => true 
									), 
									'PackageNumber' => array( 
											'type' => 'num', 
											'req' => true 
									), 
									'PackageWeight' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'DimensionHeight' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'DimensionWidth' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'DimensionLength' => array( 
											'type' => 'num', 
											'req' => false 
									) 
							) 
					), 
					'CollectYourParcel' => array( 
							'req' => false, 
							'data_provider' => '', 
							'fields' => array( 
									'CPYID' => array( 
											'type' => 'text', 
											'max' => 13, 
											'req' => true 
									) 
							) 
					), 
					'PrealertValidation' => array( 
							'req' => true, 
							'data_provider' => 'order', 
							'fields' => array( 
									'TotalShipments' => array( 
											'type' => 'num', 
											'req' => true 
									), 
									'MailAddressConfirmation' => array( 
											'type' => 'text', 
											'max' => 50, 
											'req' => false 
									), 
									'MailAddressError' => array( 
											'type' => 'text', 
											'max' => 50, 
											'req' => false 
									), 
									'Timezone' => array( 
											'type' => 'dec', 
											'max' => 50, 
											'req' => false 
									) 
							) 
					) 
			), 
			'P02' => array( 
					'objects' => array( 
							'Prealert' => '1' 
					), 
					'Prealert' => array( 
							'objects' => array( 
									'Shipment' => '+', 
									'PrealertValidation' => '1' 
							), 
							'req' => true, 
							'data_provider' => '', 
							'fields' => array( 
									'AuthenticationKey' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true, 
											'ref' => '01' 
									), 
									'LayoutType' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true, 
											'ref' => '02' 
									), 
									'LayoutVersion' => array( 
											'type' => 'text', 
											'max' => 5, 
											'req' => true 
									), 
									'LayoutPlatform' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									), 
									'PrealertReference' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => false 
									) 
							) 
					), 
					'Shipment' => array( 
							'objects' => array( 
									'ShipmentContent' => '*', 
									'ShipmentAddress' => '1', 
									'ShipmentContact' => '1', 
									'ShipmentPackage' => '+', 
									'CollectYourParcel' => '?' 
							), 
							'req' => true, 
							'data_provider' => '', 
							'fields' => array( 
									'OrderNumber' => array( 
											'type' => 'text', 
											'max' => 25, 
											'req' => true 
									), 
									'OrderReference' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => false 
									), 
									'OrderContent' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'ShippingMethod' => array( 
											'type' => 'text', 
											'max' => 10, 
											'req' => true 
									), 
									'CountryCodeOrigin' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									), 
									'PurchaseDate' => array( 
											'type' => 'text', 
											'max' => 10, 
											'req' => true 
									), 
									'Currency' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									), 
									'ShippingCosts' => array( 
											'type' => 'dec', 
											'max' => 6.2, 
											'req' => false 
									), 
									'HandlingCosts' => array( 
											'type' => 'dec', 
											'max' => 6.2, 
											'req' => false 
									), 
									'CustomsService' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									), 
									'TYPNumber' => array( 
											'type' => 'text', 
											'max' => 15, 
											'req' => false 
									) 
							) 
					), 
					'ShipmentContentCustoms' => array( 
							'req' => true, 
							'data_provider' => '', 
							'fields' => array( 
									'PackageNumber' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'SKUCode' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => true 
									), 
									'SKUDescription' => array( 
											'type' => 'text', 
											'max' => 2000, 
											'req' => true 
									), 
									'HSCode' => array( 
											'type' => 'text', 
											'max' => 6, 
											'req' => true 
									), 
									'TaricCode' => array( 
											'type' => 'text', 
											'max' => 22, 
											'req' => false 
									), 
									'Quantity' => array( 
											'type' => 'num', 
											'req' => true 
									), 
									'Price' => array( 
											'type' => 'dec', 
											'max' => 6.2, 
											'req' => true 
									), 
									'Category' => array( 
											'type' => 'text', 
											'max' => 100, 
											'req' => false 
									), 
									'ImageUrl' => array( 
											'type' => 'text', 
											'max' => 1000, 
											'req' => false 
									) 
							) 
					), 
					'ShipmentAddress' => array( 
							'req' => true, 
							'data_provider' => 'order', 
							'fields' => array( 
									'AddressType' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									), 
									'ConsigneeName' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true 
									), 
									'CompanyName' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'Street' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true 
									), 
									'AdditionalAddressInfo' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'HouseNumber' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'HouseNumberExtension' => array( 
											'type' => 'text', 
											'max' => 10, 
											'req' => false 
									), 
									'CityOrTown' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => true 
									), 
									'StateOrProvince' => array( 
											'type' => 'text', 
											'max' => 40, 
											'req' => false 
									), 
									'ZIPCode' => array( 
											'type' => 'text', 
											'max' => 12, 
											'req' => true 
									), 
									'CountryCode' => array( 
											'type' => 'text', 
											'max' => 3, 
											'req' => true 
									) 
							) 
					), 
					'ShipmentContact' => array( 
							'req' => true, 
							'data_provider' => '', 
							'fields' => array( 
									'PhoneNumber' => array( 
											'type' => 'text', 
											'max' => 20, 
											'req' => false 
									), 
									'SMSNumber' => array( 
											'type' => 'text', 
											'max' => 20, 
											'req' => false 
									), 
									'EmailAddress' => array( 
											'type' => 'text', 
											'max' => 50, 
											'req' => true 
									), 
									'PersonalNumber' => array( 
											'type' => 'text', 
											'max' => 20, 
											'req' => false 
									) 
							) 
					), 
					'ShipmentPackage' => array( 
							'req' => true, 
							'data_provider' => '', 
							'fields' => array( 
									'PackageBarcode' => array( 
											'type' => 'text', 
											'max' => 30, 
											'req' => true 
									), 
									'PackageNumber' => array( 
											'type' => 'num', 
											'req' => true 
									), 
									'PackageWeight' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'DimensionHeight' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'DimensionWidth' => array( 
											'type' => 'num', 
											'req' => false 
									), 
									'DimensionLength' => array( 
											'type' => 'num', 
											'req' => false 
									) 
							) 
					), 
					'CollectYourParcel' => array( 
							'req' => false, 
							'data_provider' => '', 
							'fields' => array( 
									'CPYID' => array( 
											'type' => 'text', 
											'max' => 13, 
											'req' => true 
									) 
							) 
					), 
					'PrealertValidation' => array( 
							'req' => true, 
							'data_provider' => '', 
							'fields' => array( 
									'Totalshipments' => array( 
											'type' => 'num', 
											'req' => true 
									), 
									'MailAddressConfirmation' => array( 
											'type' => 'text', 
											'max' => 50, 
											'req' => false 
									), 
									'MailAddressError' => array( 
											'type' => 'text', 
											'max' => 50, 
											'req' => false 
									), 
									'Timezone' => array( 
											'type' => 'dec', 
											'max' => 50, 
											'req' => false 
									) 
							) 
					) 
			) 
	);
	public static function getXML_struct() {
		return self::$xml_struct;
	}
	public static function hasLayout_type( $layout_type ) {
		return array_key_exists( $layout_type, self::$xml_struct );
	}
	public static function getElement_names( $layout_type ) {
		if( self::hasLayoutType( $layout_type ) ) {
			return array_keys( self::$xml_struct[$layout_type] );
		}
		return null;
	}
	public static function hasElement( $layout_type, $element_name ) {
		if( self::hasLayoutType( $layout_type ) ) {
			return array_key_exists( $element_name, self::$xml_struct[$layout_type] );
		}
		return null;
	}
	public static function isRequired_element( $layout_type, $element_name ) {
		if( self::hasElement( $layout_type, $element_name ) ) {
			return self::$xml_struct[$layout_type][$element_name]['req'];
		}
		return null;
	}
	public static function getField_names( $layout_type, $element_name ) {
		if( self::hasElement( $layout_type, $element_name ) ) {
			return array_keys( self::$xml_struct[$layout_type][$element_name] );
		}
		return null;
	}
	public function hasField( $layout_type, $element_name, $field_name ) {
		if( self::hasElement( $layout_type, $element_name ) ) {
			return array_key_exists( $field_name, self::$xml_struct[$layout_type][$element_name] );
		}
		return null;
	}
	public static function isRequired_field( $layout_type, $element_name, $field_name ) {
		if( self::hasField( $layout_type, $element_name, $field_name ) ) {
			if( array_key_exists( 'rec', self::$xml_struct[$layout_type][$element_name][$field_name] ) ) {
				return self::$xml_struct[$layout_type][$element_name][$field_name]['rec'];
			}
		}
		return null;
	}
	public static function getField_type( $layout_type, $element_name, $field_name ) {
		if( self::hasField( $layout_type, $element_name, $field_name ) ) {
			if( array_key_exists( 'type', self::$xml_struct[$layout_type][$element_name][$field_name] ) ) {
				return self::$xml_struct[$layout_type][$element_name][$field_name]['type'];
			}
		}
		return null;
	}
	public static function getField_max_size( $layout_type, $element_name, $field_name ) {
		if( self::hasField( $layout_type, $element_name, $field_name ) ) {
			if( array_key_exists( 'max', self::$xml_struct[$layout_type][$element_name][$field_name] ) ) {
				return self::$xml_struct[$layout_type][$element_name][$field_name]['max'];
			}
		}
		return null;
	}
	public static function getField_reference( $layout_type, $element_name, $field_name ) {
		if( self::hasField( $layout_type, $element_name, $field_name ) ) {
			if( array_key_exists( 'ref', self::$xml_struct[$layout_type][$element_name][$field_name] ) ) {
				return self::$xml_struct[$layout_type][$element_name][$field_name];
			}
		}
		return null;
	}
	protected $database_handle = null;
	protected $document_struct = null;
	protected $document = null;
	protected $customer_TYP_prefix = TYP_PREFIX;
	protected $customer_TYP_number = 0;
	public function __construct( $arguments = array() ) {
		if( array_key_exists( 'prefix', $arguments ) ) {
			$this->setTYP_prefix( $prefix );
		}
		if( array_key_exists( 'number', $arguments ) ) {
			$this->setTYP_number( $number );
		} else {
		}
	}
	public function setDatabase_handle( $dbh ) {
		if( $dbh ) {
			$this->database_handle = $dbh;
			return true;
		}
		return false;
	}
	public function getDatabase_handle() {
		if( $this->hasDatabase_handle() ) {
			return $this->database_handle;
		}
		return null;
	}
	public function hasDatabase_handle() {
		return is_null( $this->database_handle );
	}
	public function setTYP_prefix( $prefix ) {
		if( preg_match( '/[A-Z0-9]{4}/' ) ) {
			$this->customer_TYP_prefix = $prefix;
			return true;
		}
		return false;
	}
	public function getTYP_prefix() {
		return $this->customer_TYP_prefix;
	}
	public function setTYP_number( $number ) {
		if( is_int( $number ) && $number < 10000000 ) {
			$this->customer_TYP_number = $number;
			return true;
		}
		return false;
	}
	public function getTYPE_number() {
		return $this->customer_TYP_number;
	}
	public function getTYP_code() {
		if( !is_null( $this->customer_TYP_number ) && !is_null( $this->customer_TYP_prefix ) ) {
			return sprintf( "TYP%4s%'.08d", $this->customer_TYP_prefix, $this->customer_TYP_number );
		}
		return null;
	}
	public function makeLabel() {
		$text = self::LABEL_LAYOUT;
		preg_match( self::LABEL_MASK_REGEX, $text, $matches );
		if( count( $matches ) ) {
			foreach( $matches as $match ) {
				if( property_exists( __CLASS__, $match ) ) {
					str_replace( "%%${match}%%", $this->$match, $text );
				}
			}
		}
		$text .= self::LABEL_STYLE;
		return $text;
	}
	public function generateBarcode( $content, $arguments = array() ) {
		$code = new BCGcode128();
		$scale = 2;
		$line_width = 30;
		$foreground_colour = '#000';
		$background_colour = '#FFF';
		$maxWidth = 90;
		$filename = 'barcode_tmp_' . preg_replace_all( '/\s/', '', $content ) . '.png';
		$font = new BCGFont( './class/font/Arial.ttf', 18 );
		if( array_key_exists( 'scale', $arguments ) ) {
			$scale = $arguments['scale'];
		}
		if( array_key_exists( 'line_width', $arguments ) ) {
			$line_width = $arguments['line_width'];
		}
		if( array_key_exists( 'bg_colour', $arguments ) ) {
			$background_colour = $arguments['bg_colour'];
		}
		if( array_key_exists( 'fg_colour', $arguments ) ) {
			$foreground_colour = $arguments['fg_colour'];
		}
		if( array_key_exists( 'font', $arguments ) ) {
			$font = $arguments['font'];
		}
		if( array_key_exists( 'max_width', $arguments ) ) {
			$maxWidth = $arguments['max_width'];
		}
		$code->setScale( $scale );
		$code->setThickness( $line_width );
		$code->setForegroundColor( $foreground_colour );
		$code->setBackgroundColor( $background_colour );
		$code->setFont( $font );
		$code->parse( $content );
		
		// Rescale to fit maximum width;
		list( $width, $height ) = $code->getMaxSize();
		if( $width != $maxWidth ) {
			$scale = $maxWidth / ( $width * $scale );
		}
		$code->setScale( $scale );
		$code->parse( $content );
		
		$drawing = new BCGDrawing( $filename, $background_colour );
		$drawing->setBarcode( $code );
		$drawing->draw( $filename );
		return $filename;
	}
	public function generateXML_message( $data = array() ) {
		$auth_key = 'b51f7ade602d27747eea55efa849168f567d2a15';
		$this->document = new DOMDocument( '1.0', 'UTF-8' );
		if( array_key_exists( 'document_type', $data ) ) {
			if( array_key_exists( $data['document_type'], self::$xml_struct ) ) {
				$this->document_struct = self::$xml_struct[$data['document_type']];
				foreach( $this->document_struct['objects'] as $tagname => $repeat ) {
					$this->document->appendChild( $this->generateXML_element( $tagname, $repeat, $data ) );
				}
			} else {
				throw new B2CException( "The document type ${data['document_type']} is unknown." );
			}
		} else {
			throw new B2CException( "The document type is required." );
		}
		return $this->document->saveXML();
	}
	public function generateXML_element( $tagname, $repeat, $data ) {
		if( array_key_exists( $tagname, $this->document_struct ) ) {
			$substruct = $this->document_struct[$tagname];
			if( array_key_exists( 'data_provider', $substruct ) ) {
				if( array_key_exists( $substruct['data_provider'], $data ) ) {
					$subdata = ( array_keys( $data[$substruct['data_provider']] ) == range( 0, count( $data[$substruct['data_provider']] ) - 1 ) ? $data[$substruct['data_provider']] : array( 
							$data[$substruct['data_provider']] 
					) );
					$object = $this->document->createElement( $tagname );
					switch( $repeat ) {
						case '?': // 0 or 1
						case '1': // 1
							if( count( $subdata ) > 1 ) {
								// Extra records are ignored!!!
								$subdata = array( 
										array_shift( $subdata ) 
								);
							}
							break;
						case '*': // 0 or n
						case '+': // 1 of n
						          // Fine as it is..
							break;
						default :
							throw new B2CException( "Unclear repetition directive [${repeat}]." );
					}
					$element = $this->document->createElement( $tagname );
					if( array_key_exists( 'fields', $substruct ) ) {
						foreach( $subdata as $subsubdata ) {
							foreach( $substruct['fields'] as $fieldname => $subsubstruct ) {
								if( array_key_exists( $fieldname, $subsubdata ) ) {
									$fstring = '';
									if( array_key_exists( 'max', $subsubstruct ) ) {
										$fstring .= $subsubstruct['max'];
									}
									if( array_key_exists( 'type', $subsubstruct ) ) {
										switch( $subsubstruct['type'] ) {
											case 'text':
												if( !is_string( $subsubdata[$fieldname] ) ) {
													throw new B2CException( "A value of type texts is expected in ${tagnname}->${fieldname}." );
												}
												if( strlen( $subsubdata[$fieldname] ) > $fstring ) {
													$subsubdata[$fieldname] = substr( $subsubdata[$fieldname], 0, $fstring );
												}
												$fstring .= 's';
												break;
											case 'num':
												if( !is_int( $subsubdata[$fieldname] ) ) {
													throw new B2CException( "A value of type number is expected in ${tagname}->${fieldname}." );
												}
												$fstring .= 'u';
												break;
											case 'dec':
												if( !is_double( $subsubdata[$fieldname] ) ) {
													throw new B2CException( "A value of type decimal is expected in ${tagnname}->${fieldname}." );
												}
												$fstring .= 'f';
										}
										$fstring = '%' . $fstring;
										$formatted = trim( sprintf( $fstring, $subsubdata[$fieldname] ) );
										echo "RAW : ${subsubdata[$fieldname]} SPF : ${fstring} FRM : ${formatted}<br/>\n";
										$field = $this->document->createElement( $fieldname, $formatted );
										$element->appendChild( $field );
									}
								} else {
									if( array_key_exists( 'req', $subsubstruct ) && $subsubstruct['req'] ) {
										throw new B2CException( "The field ${fieldname} of element ${tagname} is required." );
									}
								}
							}
						}
					}
					if( array_key_exists( 'objects', $substruct ) ) {
						foreach( $substruct['objects'] as $tagname => $repeat ) {
							if( $subelement = $this->generateXML_element( $tagname, $repeat, $data ) ) {
								$element->appendChild( $subelement );
							}
						}
					}
					return $element;
				} else {
					// Check for required things and break if these aren't met.
					switch( $repeat ) {
						case '?': // 0 or 1
							return;
							break;
						case '*': // 0 or n
							return;
							break;
						default :
							throw new B2CException( "The element ${tagname} is expected at least once." );
					}
				}
			}
		} else {
			throw new B2CException( "The element type ${tagname} is unknown." );
		}
	}
}