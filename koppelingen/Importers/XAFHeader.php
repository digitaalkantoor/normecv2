<?php

namespace Importer;

require_once('Reader.php');

class XAFHeader implements Reader
{
    protected $record_counter = 0;
    protected $record_pointer = 0;
    protected $source_parameters = array();
    protected $structure = array();

    public function connect($source_parameters)
    {
        if (array_key_exists('path', $source_parameters)) {
            $this->reader_parameters['path'] = $source_parameters['path'];
        }
        if (array_key_exists('filename', $source_parameters)) {
            $this->reader_parameters['filename'] = $source_parameters['filename'];
        }
        if (file_exists($this->reader_parameters['path'] . $this->reader_parameters['filename'])) {
            $xml = file_get_contents($this->reader_parameters['path'] . $this->reader_parameters['filename']);
            $full = simplexml_load_string($xml);
            $line = array();
            $header = $full->header;
            $line['auditfileVersion'] = $header->auditfileVersion->__toString();
            $line['companyId'] = $header->companyId->__toString();
            $line['companyBtwNr'] = $header->taxRegistrationNr->__toString();
            $line['companyName'] = $header->companyName->__toString();
            $line['companyAddress'] = $header->companyAddress->__toString();
            $line['companyCity'] = $header->companyCity->__toString();
            $line['companyPostalCode'] = $header->companyPostalCode->__toString();
            $line['fiscalYear'] = $header->fiscalYear->__toString();
            $line['startDate'] = $header->startDate->__toString();
            $line['endDate'] = $header->endDate->__toString();
            $line['currencyCode'] = $header->currencyCode->__toString();
            $line['dateCreated'] = $header->dateCreated->__toString();
            $line['productId'] = $header->productId->__toString();
            $line['productVersion'] = $header->productVersion->__toString();
            $this->structure[] = $line;
            $this->record_counter++;
            $line = array();

            unset($xml);
            unset($full);
            return true;
        }
        return false;
    }

    public function read()
    {
        if ($this->has_next()) {
            return $this->structure[$this->record_pointer++];
        }
    }

    public function has_next()
    {
        if ($this->record_pointer < $this->record_counter) {
            return true;
        }
        return false;
    }

    public function reset()
    {
        $this->record_pointer = 0;
    }

    public function has_header()
    {
        return true;
    }

    public function get_column_count()
    {
        return 14;
    }

    public function disconnect($disconnect_parameters)
    {
    }
}