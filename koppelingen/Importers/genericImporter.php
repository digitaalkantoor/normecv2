<?php

function getColumns($table)
{
    global $db;
    $sql = "SELECT * FROM $table WHERE 1 LIMIT 1;";
    $result = db_query($sql, $db);
    $fields = array();
    foreach ($result->fetch_fields() as $field) {
        $fields[] = $field->name;
    }
    return $fields;
}

function getColumnScreenlabel($table, $column)
{
    global $db;
    static $CACHED_VELDNAMEN = array();
    if (array_key_exists("$table$column", $CACHED_VELDNAMEN)) {
        return $CACHED_VELDNAMEN["$table$column"];
    }
    $sql = "SELECT NL FROM dbstructuur WHERE TABEL = '$table' AND KOLOM = '$column' LIMIT 1;";
    $result = db_query($sql, $db);
    if ($result) {
        $row = db_fetch_array($result);
        $CACHED_VELDNAMEN["$table$column"] = $row['NL'];
        return $row['NL'];
    }
    $CACHED_VELDNAMEN["$table$column"] = $row['NL'];
    return $column;
}

function makeColumnSelect($name, $table, $columns, $selected = '')
{
    $return = "<select name=\"$name\" class=\"field\">\n";
    $has_selected = false;
    foreach ($columns as $column) {
        $this_one = '';
        $screen_name = getColumnScreenlabel($table, $column);
        if ($screen_name == '') {
            $screen_name = $column;
        }
        if (strtolower($column) == strtolower($selected) ||
            strtolower($screen_name) == strtolower($selected) ||
            strtolower(str_replace(" ", "_", $screen_name)) == strtolower($selected)) {
            $this_one = ' selected';
            $has_selected = true;
        }
        $return .= "\t<option value=\"$column\"$this_one><b>$screen_name</b> [$column]</option>\n";
    }
    if ($has_selected == false) {
        $this_one = ' selected';
    }
    $return .= "\t<option disabled value=\"\"$this_one>" . tl('Kies een kolom') . "</option>\n";
    $return .= "</select>\n";
    return $return;
}

function makeFieldSelect($name, $columns, $selected = '')
{
    $return = "<select name=\"$name\" class=\"field\">\n";
    $has_selected = false;
    foreach ($columns as $column => $example) {
        $this_one = '';
        if (strtolower($column) == strtolower($selected)) {
            $this_one = ' selected';
            $has_selected = true;
        }
        $return .= "\t<option value=\"$column\"$this_one><b>$column</b> [$example]</option>\n";
    }
    if ($has_selected == false) {
        $this_one = ' selected';
    }
    $return .= "\t<option disabled value=\"\"$this_one>" . tl('Kies een kolom') . "</option>\n";
    $return .= "</select>\n";
    return $return;
}

function makeTableSelect($name, $selected)
{
    $tables = array(
        'klanten' => 'Relaties',
        'facturen' => 'Facturen',
        'factuurregels' => 'Factuur regels',
        'producten' => 'Artikelen',
        'agenda' => 'Agenda',
        'grootboekitems' => 'Grootboek',
        'balans' => 'Balans',
        'offerte' => 'Projecten',
        'uren' => 'Uren',
        'importtemporary' => 'Tijdelijk 1',
        'importtemporary2' => 'Tijdelijk 2',
        'personeel' => 'Medewerkers'
    );
    $return = "<select name=\"$name\" class=\"field\" >\n";
    $has_selected = false;
    foreach ($tables as $table => $table_label) {
        $this_one = '';
        if ($table == $selected) {
            $this_one = ' selected';
            $has_selected = true;
        }
        $return .= "\t<option value=\"$table\"$this_one>$table_label</option>\n";
    }
    if ($has_selected == false) {
        $this_one = ' selected';
    }
    $return .= "\t<option value=\"\"$this_one>" . tl('Kies een tabel') . "</option>\n";
    $return .= "</select>\n";
    return $return;
}

function makeMappingSelect($name, $bedrijfsid)
{
    global $db;
    $sql = "SELECT * FROM importmappings WHERE BEDRIJFSID = '$bedrijfsid' AND ACTIEF = 1;";
    $result = db_query($sql, $db);
    $return = "<select name=\"$name\" class=\"field\">\n";
    $return .= '<option value="">Kies een mapping</option>' . "\n";
    if ($result) {
        while ($data = db_fetch_array($result)) {
            $return .= '<option value="' . $data['ID'] . '">' . $data['NAAM'] . "</option>\n";
        }
    } else {
        echo tl("Er is een probleem opgetreden met de database:" . $result->error);
    }
    $return .= "</select>\n";
    return $return;
}

function uploadImportFile()
{
    $bestand = $_FILES ['X_File'] ['name'];
    $bestand_tmp = $_FILES ['X_File'] ['tmp_name'];

    $bestandlocatie = bestandLocatie();
    $savefile = "./$bestandlocatie/temporary/" . $bestand;
    uploadBestand("./$bestandlocatie/temporary/", $bestand_tmp, $bestand, $_FILES ['X_File']);

    return $savefile;
}

function importeerFile($userid, $bedrijfsid)
{
    global $db;
    $extra_velden = 5;

    config_menu("Import/Export");

    if (isset ($_REQUEST ["IMPORTEERRELATIE"])) {
        $uitleg[] = "Voor het importeren van relaties doorloop de volgende stappen:";
        $uitleg[] = "1. Maak alle velden die moeten worden geimporteerd actief in het invoerscherm. <a href=\"content.php?SITE=dbStructuur&TABEL=klanten\">Bewerk hier het invoerscherm.</a> ";
        $uitleg[] = "2. <a href=\"content.php?SITE=voorbeeldTemplateRelaties&EXPORTEXCEL=true\">Download voorbeeldbestand</a> ";
        $uitleg[] = "3. Plaats de gegevens in het voorbeeldbestand en upload deze hieronder. Behoud de kolomnamen in de eerste regel.";

        echo paginaTitel("Relaties importeren", "", null, null, $uitleg);
    } else {
        echo paginaTitel("Importeer bestand", "");
    }

    echo "<div class=\"dashboardVakje\">";

    $has_header = 0;
    if (isset($_REQUEST['has_header']) && $_REQUEST['has_header'] != 0) {
        $has_header = $_REQUEST['has_header'];
    }
    $allow_update = false;
    if (isset($_REQUEST['allow_update']) && $_REQUEST['allow_update'] == 'Ja') {
        $allow_update = true;
    }
    $delete_old = false;
    if (isset($_REQUEST['delete_old']) && $_REQUEST['delete_old'] == 'Ja') {
        $delete_old = true;
    }
    $table = '';
    if (isset($_REQUEST['table'])) {
        $table = $_REQUEST['table'];
    }
    $path = '';
    if (isset($_REQUEST['tmp_path'])) {
        $path = urldecode($_REQUEST['tmp_path']);
    } else {
        if (isset($_FILES ['X_File'])) {
            $path = uploadImportFile();
        }
    }
    $unique_key = '';
    if (isset($_REQUEST['key_field'])) {
        $unique_key = $_REQUEST['key_field'];
    }

    if (isset ($_REQUEST ["IMPORTEERRELATIE"])) {

        echo titelBalkje("Stap 1: Selecteer bestand");
        echo '<form action="content.php?SITE=importeerBestand" method="Post" enctype="multipart/form-data">';
        echo '<input name="site" value="importeerBestand" type="hidden"/>';
        echo '<input name="table" value="klanten" type="hidden"/>';
        echo '<input name="has_header" value="1" type="hidden"/>';
        echo '<input name="key_field" value="DEBITEURNR" type="hidden"/>';

        echo '<table>';
        echo '<tr><td>' . tl('Kies het te importeren excel bestand.') . '</td><td>' . fileVeld("X_File",
                "field") . '</td></tr>';
        echo '<tr><td>' . tl('Vul bestaande informatie aan.') . '</td><td>' . selectjanee('allow_update',
                'Nee') . '</td></tr>';
        echo '<tr><td colspan="2">' . opslaanButton('OPSLAAN1', 'Upload bestand', 'btn-action') . '</td></tr>';
        echo '</table>';
        echo "</form>";
    } elseif ((!isset ($_REQUEST ["OPSLAAN1"]) &&
            !isset($_REQUEST['OPSLAAN2']) &&
            !isset($_REQUEST['OPSLAAN3'])) ||
        ((!isset($_FILES['X_File']) ||
                !isset($_FILES['X_File']['tmp_name']) ||
                !$_FILES['X_File']['tmp_name']) && !$path)) {
        echo titelBalkje("Importeer bestand [Stap 1]");
        echo tl("Importeer excel bestanden in Digitaal Kantoor.");
        echo '<form action="" method="Post" enctype="multipart/form-data">';
        echo '<input name="site" value="importeerBestand" type="hidden"/>';
        echo '<table>';
        echo '<tr><td>' . tl('Kies het te importeren excel bestand.') . '</td><td>' . fileVeld("X_File",
                "field") . '</td></tr>';
        echo '<tr><td colspan="2">' . tl('Voor een bestand met een headerregel geldt dat alle headervelden uniek moeten zijn en liever geen punten, spaties of underscores bevatten.') . '</td></tr>';
        echo '<tr><td>' . tl('Kies de tabel waarin geimporteerd moet worden.') . '</td><td>' . makeTableSelect('table',
                '') . '</td></tr>';
        echo '<tr><td>' . tl('Op welke regel staan de headers in het bestand.') . '</td><td><input class="field" type="text" name="has_header" value="' . $has_header . '"/> ' . tl('(0 als er geen headerregel is.') . '<br/>' . tl('Kies ook nul als een veld meer dan eens in de header voorkomt. )') . '</td></tr>';
        echo '<tr><td>' . tl('Vul bestaande informatie aan.') . '</td><td>' . selectjanee('allow_update',
                'Nee') . '</td></tr>';
        echo '<tr><td>' . tl('Gooi eerst alle bestaande informatie in deze tabel weg.') . '</td><td>' . selectjanee('delete_old',
                'Nee') . '</td></tr>';
        echo '<tr><td>' . tl('Kies een bestaande mapping.') . '</td><td>' . makeMappingSelect('mappings',
                $bedrijfsid) . '</td></tr>';
        echo '<tr><td colspan="2">' . opslaanButton('OPSLAAN1', 'Upload bestand', 'btn-action') . '</td></tr>';
        echo '</table>';
        echo "</form>";
    } else {
        if (isset($_REQUEST["OPSLAAN1"]) && !(isset($_REQUEST['mappings']) && $_REQUEST['mappings'] != '')) {
            $columns = getColumns($table);
            $excel = new Importer\Excel();
            if (!$excel->connect(array('path' => $path, 'filename' => '', 'header_line' => $has_header))) {
                die(tl("Het bestand [$path] kan niet geopend worden"));
            }
            echo titelBalkje("Importeer bestand [Stap 2]");
            echo '<form action="" method="Post" enctype="multipart/form-data">';
            echo '<table>';
            echo '<tr><td colspan="2">' . tl('Het te importeren excel bestand.') . '</td><td>' . $path . '<input type="hidden" name="tmp_path" value="' . urlencode($path) . '"/></td><td></td></tr>';
            echo '<tr><td colspan="2">' . tl('Tabel waarin geimporteerd moet worden.') . '</td><td>' . $table . '<input type="hidden" name="table" value="' . $table . '"></td><td></td></tr>';
            echo '<tr><td colspan="2">' . tl('Bestand heeft header-regel') . '</td><td>' . ($has_header ? 'Ja' : 'Nee') . '<input type="hidden" name="has_header" value="' . $has_header . '"/></td><td></td></tr>';
            echo '<tr><td colspan="4">' . tl('Bestaande informatie wordt ') . '<b>' . ($allow_update ? 'wel' : 'niet') . '</b>' . tl(' aangevuld.') . '<input type="hidden" name="allow_update" value="' . ($allow_update ? 'Ja' : 'Nee') . '"/></td></tr>';
            echo '<tr><td colspan="4">' . tl('De tabel ' . $table . ' wordt ') . '<b>' . ($delete_old ? 'wel' : 'niet') . '</b>' . tl(' eerst leeggegooid.') . '<input type="hidden" name="delete_old" value="' . ($delete_old ? 'Ja' : 'Nee') . '"/></td></tr>';
            echo '<tr><td colspan="2">' . tl('Gebruik dit veld om te controleren of deze informatie al in de database zit.') . '</td><td>' . makeColumnSelect('key_field',
                    $table, $columns, '') . '</td><td>' . tl('(Dit moet een geldige kolom zijn).') . '</td></tr>';
            echo '<tr><td colspan="4">' . tl('Hieronder moet ingesteld worden welk veld uit het excelbestand in welk veld in de ' . $table . ' tabel moet komen. ') . '</td></tr>';
            echo '<tr><td colspan="4"><br/><Br/></td></tr>';

            $line = $excel->read();
            echo '<tr><th>' . tl('Excel bestand veld') . '</th>
                          <th>' . tl('Voorbeeld') . '</th>
                          <th>' . tl('Database bestand veld') . '</th>
                          <th>' . tl('Standaard waarde') . '</th></tr>';
            foreach ($excel->get_header_fields() as $name) {
                $default = '';
                if (isset($_REQUEST["Default$name"])) {
                    $default = $_REQUEST["Default$name"];
                }
                echo '<tr><td>' . lb($name) . '</td><td>' . lb($line[$name]) . '</td><td>' . makeColumnSelect('Map' . $name,
                        $table, $columns,
                        $name) . '</td><td><input class="fieldHalf" name="Default' . $name . '" type="text" value="' . $default . ' "/></td></tr>';
            }
            for ($i = 0; $i < $extra_velden; $i++) {
                $value = '';
                if (isset($_REQUEST["ExtraVeld$i"])) {
                    $value = $_REQUEST["ExtraVeld$i"];
                }
                echo '<tr><td colspan="2">' . makeFieldSelect('ExtraVeld' . $i, $line,
                        $value) . '</td><td>' . makeColumnSelect('ExtraMap' . $i, $table, $columns,
                        '') . '</td><td><input class="fieldHalf" name="ExtraDefault' . $i . '" type="text" value=""/></td></tr>';
            }
            echo '<tr><td colspan="4"><hr/></td></tr>';
            echo '<tr><td colspan="2">' . opslaanButton('OPSLAAN2', 'Start import',
                    'btn-action') . '</td><td><b>Mapping naam :</b> <input type="text" class="field" name="mappingname" value=""/></td><td>' . opslaanButton('OPSLAAN3',
                    'Bewaar mapping', 'btn-action') . '</td></tr>';
            echo '</table>';
            echo "</form>";
        } else {
            if (isset($_REQUEST['OPSLAAN2']) ||
                isset($_REQUEST['OPSLAAN3']) ||
                (isset($_REQUEST['OPSLAAN1']) && isset($_REQUEST['mappings']) && $_REQUEST['mappings'] != '')) {
                echo titelBalkje("Importeer bestand [Stap 3]");
                $fields = array();
                $extraFields = array();
                foreach ($_REQUEST as $key => $value) {
                    if (preg_match('/^Map(?P<label>.*)$/', $key, $matches)) {
                        $fields[$matches['label']] = $value;
                    }
                }
                $map = new Importer\Mapper();
                $storer = new Importer\Storer();
                $storer->setDB_handle($db);
                $storer->setAllow_update($allow_update);
                $map->setStorer($storer);
                if (!isset($_REQUEST['mappings']) || !$_REQUEST['mappings']) {
                    $map->addOutput_object('output');
                    $map->getOutput_object('output')->setTable($table);
                    foreach ($fields as $field => $value) {
                        if ($value) {
                            $default = null;
                            if (isset($_REQUEST["Default$field"]) && $_REQUEST["Default$field"] != '') {
                                $default = $_REQUEST["Default$field"];
                            }
                            $map->addMapping($field, 'output', $value, $default);
                        }
                    }
                    for ($x = 0; $x < $extra_velden; $x++) {
                        if (array_key_exists("ExtraVeld$x", $_REQUEST) &&
                            (array_key_exists("ExtraMap$x", $_REQUEST) || array_key_exists("ExtraDefault$x",
                                    $_REQUEST))) {
                            $field = $_REQUEST["ExtraVeld$x"];
                            $value = (array_key_exists("ExtraMap$x", $_REQUEST) ? $_REQUEST["ExtraMap$x"] : null);
                            $default = (array_key_exists("ExtraDefault$x", $_REQUEST) ? $_REQUEST["ExtraMap$x"] : null);
                            $map->addMapping($field, 'output', $value, $default);
                        }
                        $map->getOutput_object('output')->setKey($unique_key);
                    }
                    //var_export( $map ); exit;
                } else {
                    $map->fetch($_REQUEST['mappings']);
                }
                if (isset($_REQUEST['OPSLAAN2']) || isset($_REQUEST['OPSLAAN1'])) {
                    $excel = new Importer\Excel();
                    if (!$excel->connect(array('path' => $path, 'filename' => '', 'header_line' => $has_header))) {
                        die(tl("Het bestand [$path] kan niet geopend worden."));
                    }
                    $map->setReader($excel);
                    $count = $map->process();
                    if ($count) {
                        echo tl("De import was succesvol.") . "<br/>";
                        echo $count . tl(" records geimporteerd.") . "<br/>";
                    } else {
                        echo tl("Er zijn problemen opgetreden bij het importeren van het bestand.") . "<br/>";
                    }
                } else {
                    if (isset($_REQUEST['OPSLAAN3']) && isset($_REQUEST['mappingname'])) {
                        $mapping_name = $_REQUEST['mappingname'];
                        if ($map->store($mapping_name, $bedrijfsid)) {
                            echo tl("De mapping $mappingname is succesvol opgeslagen.");
                        } else {
                            echo tl("Er zijn problemen opgetreden bij het opslaan van mapping $mapping_name.");
                        }
                    }
                }
            } else {
                echo "Invoer niet begrijpelijk. Actie gestopt";
            }
        }
    }

    echo "</div>";

    if (!isset ($_REQUEST ["IMPORTEERRELATIE"]) && $table != "klanten") {
        echo showImporterHelp();
    }
}

function showImporterHelp()
{
    echo "<br/><br/><div class=\"dashboardVakje\">";
    echo titelBalkje("Importeer een Excel bestand in drie stappen");
    echo '<h2>Stap 1</h2>
<p>In deze stap wordt een bestand opgeladen en gekozen in welke gegevenskaarten deze informatie gezet moet worden.<br/>
</p>
<p>Een overzicht van de velden in het formulier:</p>
<ul>
  <li><b>Kies het te importeren excel bestand.</b> <br/>Met dit veld kiest u welk bestand opgeladen moet worden.</li>
  <li><b>Kies de tabel waarin geimporteerd moet worden.</b> <br/>Selecteer de gegevenskaart waarin de informatie hoort.</li>
  <li><b>Op welke regel staan de headers in het bestand.</b> <br/>Als het opgeladen bestand een header regel bevat dan kan hier het regelnummer ingevoerd worden. Als in de headerregel underscores, spaties of punten voorkomen kunnen er problemen ontstaan met de import. Als in de headerregel een headerveld meerdan eens voorkomt kunnen problemen met de import ontstaan. In deze gevallen is het het best hier <b>0</b> in te vullen.</li>
  <li><b>Vul bestaande informatie aan.</b> <br/>Gebruik de opgeladen informatie om bestaande gegevenskaarten aan te vullen/ te wijzigen.</li>
  <li><b>Gooi eerst alle bestaande informatie in deze tabel weg.</b> <br/>Deze functie staat niet aan!</li>
  <li><b>Kies een bestaande mapping.</b> <br/>Als er al een mapping bestaat dan kan u deze hier kiezen. <b>Stap 2</b> wordt dan overgeslagen.
</ul>
<h2>Stap 2</h2>
<p>In deze stap wordt het systeem verteld hoe het bestand in de gegevenskaarten gestopt moet worden.<br/>
Bovenaan staan de gegevens zoals die in <b>Stap 1</b> ingevuld zijn.</p>
<p>Een overzicht vande velden in het formulier:</p>
<ul>
  <li><b>Gebruik dit veld om te controleren of deze informatie al in de database zit.</b> <br/>Dit veld moet altijd ingevuld worden met een veld wat in de onderstaande tabel verbonden is. Als ingesteld is dat bestaande gegevenskaarten aangevuld moeten worden dan wordt van dit veld gekeken of een gegevenskaart al bestaat.</li>
  <li><b>Hieronder moet ingesteld worden welk veld uit het excelbestand in welk veld in de importtemporary tabel moet komen.</b> <br/>In deze tabel wordt een veld uit het bestand ( eerste kolom ) gebonden aan een veld op de gegevenskaart ( tweede kolom ). In het veld "Standaardwaarde" ( derde kolom ) kan een waarde ingesteld worden die gebruikt wordt als het veld in het bestand leeg is.<br/>
    Het onderste gedeelte van de tabel bevat een aantal velden die de gebruiker dubbel kan verbinden. Zodat gegevens op meer dan &eacute;&eacute;n plaats kunnen staan.  </li>
</ul>
<h2>Stap 3</h2>
<p>Dit is het rapportagescherm. U krijgt te zien of de import geslaagd is en zo ja hoeveel regels uit het bestand zijn gelezen.<br/>Als er problemen opgetreden zijn bij de import dan ziet u dat hier.
</p>';
    echo "</div>";
}

function importeerUren($userid, $bedrijfsid)
{
    global $db;
    config_menu("Import/Export");
    echo "<div class=\"dashboardVakje\">";

    $path = '';
    if (isset($_REQUEST['tmp_path'])) {
        $path = urldecode($_REQUEST['tmp_path']);
    } else {
        if (isset($_FILES ['X_File'])) {
            $path = uploadImportFile();
        }
    }
    $unique_key = ''; // Stap 1 : NAAM | stap 2 : DATUM ( insert )
    if (!isset ($_REQUEST ["OPSLAAN1"])) {

        echo titelBalkje("Stap 1: Selecteer bestand");
        echo '<form action="content.php?SITE=importeerBestand" method="Post" enctype="multipart/form-data">';
        echo '<input name="site" value="importeerBestand" type="hidden"/>';
        echo '<table>';
        echo '<tr><td>' . tl('Kies het te importeren excel bestand.') . '</td><td>' . fileVeld("X_File",
                "field") . '</td></tr>';
        echo '<tr><td colspan="2">' . opslaanButton('OPSLAAN1', 'Upload bestand', 'btn-action') . '</td></tr>';
        echo '</table>';
        echo "</form>";
    } else {
        echo titelBalkje("Stap 2: Importeer bestand");
        echo "<p><b>Als er problemen zijn bij de import dan tonen deze hieronder</b><br/>In sommige gevallen wordt de import afgebroken, in andere gevallen zal het systeem proberen toch de data te importeren.</p>";
        echo "<hr/>";
        $has_header = 1;
        $excel = new Importer\Excel();
        if (!$excel->connect(array('path' => $path, 'filename' => '', 'header_line' => $has_header))) {
            die(tl("Het bestand [$path] kan niet geopend worden"));
        }
        $linecount = 0;
        while ($excel->has_next()) {
            $linecount++;
            $line = $excel->read();

            // Naam
            $narr = explode(' ', $line['User']);
            $firstname = array_shift($narr);
            $lastname = array_pop($narr);
            $restname = implode(' ', $narr);

            // Datum
            $date = PHPExcel_Style_NumberFormat::toFormattedString($line['Date'], "YYYY/MM/DD");

            // Tijd
            $time = $line['Hours'] * 60;

            $offertenr = 0;
            // Check of het project al bestaat
            $SQL = "SELECT * FROM `offerte` WHERE `OFFERTENR` = " . $line['Project_number'] . " OR `NAAM` = '" . $db->real_escape_string($line['Project_name']) . "' LIMIT 1;";
            $RES = db_query($SQL, $db);
            if (!$RES || !$RES->num_rows > 0) {
                $SQL = "INSERT INTO `offerte` ( `OFFERTENR`, `NAAM` ) VALUES ( " . $line['Project_number'] . ", '" . $db->real_escape_string($line['Project_name']) . "' );";
                $RES = db_query($SQL, $db);
                if (!$RES && $db->errno) {
                    die(tl("Insert van een offerte faalde: [$SQL], " . $db->error));
                }
            }
            $offertenr = $line['Project_number'];

            $personeelslid = 0;
            // Check of het gekoppelde personeelslid al bestaat
            $SQL = "SELECT * FROM `personeel` WHERE `ACHTERNAAM` like '" . $db->real_escape_string($lastname) . "' LIMIT 1;";
            $RES = db_query($SQL, $db);
            if (!$RES || !$RES->num_rows > 0) {
                $SQL = "INSERT INTO `personeel` ( `VOORNAAM`, `TUSSENVOEGSEL`, `ACHTERNAAM` )  VALUES ( '" . $db->real_escape_string($firstname) . "', '" . $db->real_escape_string($restname) . "', '" . $db->real_escape_string($lastname) . "' );";
                $RES = db_query($SQL, $db);
                if (!$RES && $db->errno) {
                    die(tl("Insert van een personeelslid faalde: [$SQL], " . $db->error));
                }
                $personeelslid = $db->insert_id;
            } else {
                $DAT = db_fetch_array($RES);
                $personeelslid = $DAT['ID'];
            }

            // Insert uren
            $SQL = " INSERT INTO `uren` ( `OFFERTENR`, `BEDRIJFSID`, `PERSONEELSLID`, `OMSCHRIJVING`, `TIJDEIND`, `KOPPELING` ) VALUES ( " . $offertenr . ", " . $bedrijfsid . ", " . $personeelslid . ", '" . $db->real_escape_string($line['Issue']) . "', " . $time . ", '" . $db->real_escape_string($line['Activity']) . "' );";
            $RES = db_query($SQL, $db);
            if (!$RES && $db->errno) {
                die(tl("Insert van uren faalde: [$SQL], " . $db->error));
            }
        }
        echo "<hr/>Er zijn $linecount regels verwerkt.<br/>";
    }
}
