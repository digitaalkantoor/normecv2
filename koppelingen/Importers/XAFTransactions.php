<?php

namespace Importer;

require_once('Reader.php');

class XAFTransactions implements Reader
{
    protected $record_counter = 0;
    protected $record_pointer = 0;
    protected $source_parameters = array();
    protected $structure = array();

    // TODO: Versieonafhankelijk maken door de structuur in een ASSOC-array te stoppen.

    public function connect($source_parameters)
    {
        if (array_key_exists('path', $source_parameters)) {
            $this->reader_parameters['path'] = $source_parameters['path'];
        }
        if (array_key_exists('filename', $source_parameters)) {
            $this->reader_parameters['filename'] = $source_parameters['filename'];
        }
        if (file_exists($this->reader_parameters['path'] . $this->reader_parameters['filename'])) {
            $xml = file_get_contents($this->reader_parameters['path'] . $this->reader_parameters['filename']);
            $full = simplexml_load_string($xml);
            $line = array();
            if (isset($full->transactions->journal)) {
                foreach ($full->transactions->journal as $journal) {
                    foreach ($journal->transaction as $transaction) {
                        foreach ($transaction->line as $record) {
                            $line['journalId'] = $journal->journalID->__toString();
                            $line['journalDescription'] = $journal->description->__toString();
                            $line['journalType'] = $journal->type->__toString();

                            $line['transactionId'] = $transaction->transactionID->__toString();
                            $line['transactionDescription'] = $transaction->description->__toString();
                            $line['transactionPeriod'] = $transaction->period->__toString();
                            $line['transactionDate'] = $transaction->transactionDate->__toString();

                            $line['recordId'] = $record->recordID->__toString();
                            $grootboek_nummer = $record->accountID->__toString();
                            $grootboekId = $this->fetchAccountId($grootboek_nummer);

                            $line['custSupID'] = getValue('klanten', 'DEBITEURNR', $record->custSupID->__toString(),
                                'ID');

                            $line['recordDocumentId'] = $record->documentID->__toString();
                            $line['recordEffectiveDate'] = $record->effectiveDate->__toString();
                            $line['recordDescription'] = $record->description->__toString();
                            if (isset($record->creditAmount)) {
                                $line['recordCreditAmount'] = $record->creditAmount->__toString();
                                $line['recordAmount'] = $record->creditAmount->__toString();
                                $line['recordType'] = 'Credit';

                                if (isset($record->currency)) {
                                    $line['recordCurrencyAmount'] = $record->currency->currencyCreditAmount->__toString();
                                    $line['recordCurrencyCode'] = $record->currency->currencyCode->__toString();
                                }
                            } else {
                                $line['recordDebitAmount'] = $record->debitAmount->__toString();
                                $line['recordAmount'] = $record->debitAmount->__toString();
                                $line['recordType'] = 'Debet';

                                if (isset($record->currency)) {
                                    $line['recordCurrencyAmount'] = $record->currency->currencyDebitAmount->__toString();
                                    $line['recordCurrencyCode'] = $record->currency->currencyCode->__toString();
                                }
                            }

                            $boekingszijde = getValue("grootboekitems", "ID", $grootboekId, "CREDITDEBET");
                            if ($boekingszijde == "Debet" && $line['recordType'] == "Debet") {
                                $line['recordAccountIdDebet'] = $grootboekId;
                                $line['recordAccountIdCredit'] = '11';
                            } elseif ($boekingszijde == "Debet" && $line['recordType'] == "Credit") {
                                $line['recordAccountIdDebet'] = '11';
                                $line['recordAccountIdCredit'] = $grootboekId;
                            } elseif ($boekingszijde == "Credit" && $line['recordType'] == "Credit") {
                                $line['recordAccountIdDebet'] = '11';
                                $line['recordAccountIdCredit'] = $grootboekId;
                            } elseif ($boekingszijde == "Credit" && $line['recordType'] == "Debet") {
                                $line['recordAccountIdDebet'] = $grootboekId;
                                $line['recordAccountIdCredit'] = '11';
                            } else {
                                echo "<br/>Error: $boekingszijde - " . $line['recordType'];
                            }
                            //echo "<br/>line: $boekingszijde - " . $line['recordType'] . ": $grootboek_nummer: ".$line['recordAmount'] . ": ". $line['recordDescription'];
                            //echo "BZ: $grootboekId: ".$boekingszijde;
                            //dpm($line); exit;

                            if (isset($recortd->vat)) {
                                $line['vatCode'] = $record->vat->vatCode->__toString();
                                $line['vatAmount'] = $record->vat->vatAmount->__toString();
                            }
                            $this->structure[] = $line;
                            $this->record_counter++;
                            $line = array();
                        }
                    }
                }
                unset($xml);
                unset($full);
                return true;
            }
        }
        return false;
    }

    protected function fetchAccountId($grootboek_nummer)
    {
        return getValue('grootboekitems', 'GROOTBOEKNR', $grootboek_nummer, 'ID');
    }

    public function read()
    {
        if ($this->has_next()) {
            return $this->structure[$this->record_pointer++];
        }
    }

    public function has_next()
    {
        if ($this->record_pointer < $this->record_counter) {
            return true;
        }
        return false;
    }

    public function reset()
    {
        $this->record_pointer = 0;
    }

    public function has_header()
    {
        return true;
    }

    public function get_column_count()
    {
        return 14;
    }

    // Change this function if the fetching of an ID changes.
    // LET WEL! Omdat er nu een functie uit de rest van het systeem gebruikt is is dit GEEN schone OO code meer!!

    public function disconnect($disconnect_parameters)
    {
    }
}