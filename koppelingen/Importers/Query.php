<?php

namespace Importer;

require_once('Reader.php');

class Query implements Reader
{
    protected $connection_handle = null;
    protected $database_handle = null;
    protected $reader_parameters = array();
    protected $field_names = array();

    public function __construct()
    {
        $this->reader_parameters ['line_counter'] = 0;
    }

    public function explain()
    {
        return <<<EXPLAIN
A basic database query reader.	( only meant to at least HAVE a database reader )
		
\$this->connect( array() ) 
Connects the input;

The array must contain the following keys and values:
 - host..................A database server.
 - username..............Login username.
 - password..............Login password.
 - database..............The database name.
OR
 - db_handle.............A valid database handle.
  
The array may contain the following keys, otherwise default values are used:
 - port..................The port to talk to on the host. This is optional.
 - header_line...........The index of the header line. If omitted no header will be assumed.
                         ( and a numbered array will be returned ).
 - table.................The targeted table int he database, a query will be constructed.
 - query.................A query to pass to the database, if this is given 'table' will be 
                         overloaded ).
 
                        
\$this->read()
Reads one record from the input. If the last record is already read it wil return null;
If a header line is read returns an associative array with header-field => value pairs
otherwise an array.

\$this->reset()
Resets the record pointer to the first record.

\$this->has_next()
Returns true if another record can be read, false otherwise.

\$this->has_header();
Returns true if a headerline is set.

\$this->get_column_count();
Returns the number of columns ( a count, so 1 columns if only column [0] is set )
If not set when read tries the first 15 lines of the file and then resets it.

\$this->disconnect()
Disconnects the input
EXPLAIN;
    }

    public function connect($source_parameters)
    {
        if (array_key_exists('db_handle', $source_parameters)) {
            $this->database_handle = $source_parameters ['db_handle'];
        } else {
            if (array_key_exists('host', $source_parameters) && array_key_exists('database',
                    $source_parameters) && array_key_exists('username',
                    $source_parameters) && array_key_exists('password', $source_parameters)) {
                $host = $source_parameters ['host'];
                if (array_key_exists('port', $source_parameters)) {
                    $host .= ':' . $source_parameters ['port'];
                }
                $dbh = mysqli_connect($host, $source_parameters ['username'], $source_parameters ['password'],
                    $source_parameters ['database']) or die ('Error connecting :' . mysqli_error($this->database_handle));
                $this->database_handle = $dbh;
            } else {
                die ('No valid connection given');
            }
        }
        if (array_key_exists('table', $source_parameters)) {
            $this->reader_parameters ['table'] = $source_parameters ['table'];
            $this->reader_parameters ['query'] = 'SELECT * FROM `' . $source_parameters ['table'] . '`;';
        }
        if (array_key_exists('query', $source_parameters)) {
            $this->reader_parameters ['query'] = $source_parameters ['query'];
        }
        if (array_key_exists('header_line', $source_parameters)) {
            $this->reader_parameters ['header_line'] = $source_parameters ['header_line'];
        } else {
            $this->reader_parameters ['header_line'] = false;
        }
        if (array_key_exists('query', $this->reader_parameters)) {
            if (!$this->connection_handle = $this->database_handle->query($this->reader_parameters ['query'])) {
                return false;
            }
            $this->reader_parameters ['line_counter'] = 0;
        }
    }

    public function read()
    {
        if ($this->has_next()) {
            $this->reader_parameters ['line_counter']++;

            if ($this->has_header()) {
                return $this->connection_handle->fetch_array(MYSQLI_ASSOC);
            } else {
                return $this->connection_handle->fetch_array(MYSQLI_NUM);
            }
        }
    }

    public function has_next()
    {
        $nr = $this->connection_handle->num_rows;
        if ($this->reader_parameters ['line_counter'] < $nr) {
            return true;
        }
        return false;
    }

    public function has_header()
    {
        if ($this->reader_parameters ['header_line'] === false) {
            return false;
        }
        return true;
    }

    public function reset()
    {
        $this->data_seek(0);
    }

    public function get_column_count()
    {
        return $this->connection_handle->field_count;
    }

    public function disconnect($disconnect_parameters = array())
    {
        return $this->database_handle->close();
    }
}