<?php

namespace Importer;

require_once('MapObject.php');
require_once('Reader.php');
require_once('Storer.php');

class Mapper
{
    protected $input_fields = array();
    protected $output_objects = array();
    protected $reader = null;
    protected $storer = null;

    public function explain()
    {
        return <<<EXPLAIN
A basic Mapper.

The order of doing things:
1...Create a mapper object
2...Create a reader object
3...Create a storer object
4...Put the storer and reader in the mapper
5...Create mappings
6...Process

TODO: Find a way to insert id's of other objects into objects.

\$this->setReader( READER_OBJECT )
Sets the reader object in the mapper.

\$this->getReader()
Returns the reader object. NULL if it's not set.
		
\$this->hasReader()
A check whether a reader is set.
		
\$this->setStorer( STORER_OBJECT )
Sets the storer object in the mapper.
		
\$this->getStorer()
Returns the storer object. NULL if it's not set.
		
\$this->hasStorer()
A check whether a storer is set.

\$this->isReady()
A check to see if the mapper is ready for processing.
		
\$this->addInput_field( key )
Adds an input field to the mapper. The label of this field must correspond with a field 
of a reader line. 
		
\$this->deleteInput_field( key )
A way to get rid of a wrongly added field. Maybe handy when you auto-map something.
	
\$this->hasInput_field( key )
A check whether a field exists.
		
\$this->addOutput_object( label )
Creates a new output object.
		
\$this->deleteOutput_object( label )
Deletes an output object. The best way to get rid of them.
		
\$this->getOutput_object( label )
Returns the output object.

\$this->hasOutput_object( label )
A check whether a n output object with this label exists.

\$this->addMapping( input_field, output_object, object_field, default_value = null )
The way to add a mapping. It automates the creation of input fields, output objects and 
fields in those output objects, if these don't exist in the mapper. If they exist they
are used.
If the default value is set it's used when reading an empty field from the read line.

\$this->deleteMapping( input_field, output_object, output_field = null )
If the output field is set only the field mapping is deleted. If it is not set the entire 
object mapping is deleted.

\$this->getMapping( input_field )
Get the mapping for this input field. It returns an array;

\$this->hasMapping( input_field )
A check whether there is a mapping for this input field.

\$this->processLine() 
First is checked whether the mapper is ready, then whether another line can be read. Then
for each field in the reader line all mapped fields in objects are written. 
When done the object is offered to the storer module and cleared afterwards. 
It returns true if a line is read.

\$this->process()
A function which just loops processLine() until it returns false and counts the number of read lines. 
EXPLAIN;
    }

    public function deleteInput_field($key)
    {
        if ($this->hasInput_field($key)) {
            unset($this->input_fields[$key]);
            return true;
        }
        echo '<span class="error">[Mapper:deleteInput_field]Error: No input field with key [' . $key . "] exists.</span><br/>\n";
        return false;
    }

    public function hasInput_field($key)
    {
        if (array_key_exists($key, $this->input_fields)) {
            return true;
        }
        return false;
    }

    public function deleteOutput_object($label)
    {
        if ($this->hasOutput_object($label)) {
            unset($this->output_objects[$label]);
            return true;
        }
        echo '<span class="error">[Mapper:deleteOutput_object]Error: No output object with label [' . $label . "] exists.</span><br/>\n";
        return false;
    }

    public function hasOutput_object($label)
    {
        if (array_key_exists($label, $this->output_objects)) {
            return true;
        }
        return false;
    }

    public function deleteMapping($input_field, $output_object, $output_field = null)
    {
        if (isset($output_field)) {
            unset($this->input_fields[$input_field][$output_object]);
            return true;
        }
        unset($this->input_fields[$input_field][$output_object][$output_field]);
        return true;
    }

    public function process()
    {
        $count = 0;
        while ($this->processLine()) {
            $count++;
        }
        return $count;
    }

    public function processLine()
    {
        if ($this->isReady()) {
            if ($this->getReader()->has_next()) {
                $readline = $this->getReader()->read();
                foreach (array_keys($this->input_fields) as $label) {
                    $value = null;
                    //echo "Label : $label -- Keys : ". implode( ',', array_keys( $readline ) );
                    if (array_key_exists($label, $readline)) {
                        //echo " <b>MAtch!</b>";
                        $value = $readline[$label];
                    }
                    //echo "<br/>";
                    foreach ($this->getMapping($label) as $object => $fields) {
                        foreach ($fields as $field_label => $default) {
                            if (!is_null($value)) {
                                $this->getOutput_object($object)->setField($field_label, $value);
                            } else {
                                $this->getOutput_object($object)->setField($field_label, $default);
                            }
                        }
                    }
                }
                $id_line = array();
                foreach ($this->output_objects as $label => $object) {
                    $this->getStorer()->store($object);
                    $id_line[$label] = $object->getId();
                }

                foreach (array_keys($this->input_fields) as $label) {
                    foreach ($this->getMapping($label) as $object => $fields) {
                        $update = false;
                        $updateobject = null;
                        foreach ($fields as $field_label => $default) {
                            if (array_key_exists($label, $id_line)) {
                                $updateobject = $this->getOutput_object($object);
                                $updateobject->setField($field_label, $id_line[$label]);
                                $update = true;
                            }
                        }
                        if ($update) {
                            $this->getStorer()->update($updateobject);
                        }
                    }
                }

                foreach ($this->output_objects as $label => $object) {
                    $object->clear();
                }
                return true;
            }
            // Trying to read beyond last line
            return false;
        }
        echo '<span class="error">[Mapper:processLine]Error: No connected reader- or storer-object found.' . "</span><br/>\n";
        return false;
    }

    public function isReady()
    {
        if ($this->hasReader() && $this->hasStorer()) {
            return true;
        }
        return false;
    }

    public function hasReader()
    {
        if (isset($this->reader)) {
            return true;
        }
        // No reader has been set
        return false;
    }

    public function hasStorer()
    {
        if (isset($this->storer)) {
            return true;
        }
        return false;
    }

    public function getReader()
    {
        if ($this->hasReader()) {
            return $this->reader;
        }
        echo '<span class="error">[Mapper:getReaderer]Error: No reader object ' . " is set.</span><br/>\n";
        return null;
    }

    public function setReader($reader)
    {
        $this->reader = $reader;
    }

    public function getMapping($input_field)
    {
        if ($this->hasMapping($input_field)) {
            return $this->input_fields[$input_field];
        }
        echo '<span class="error">[Mapper:getMapping]Error: No such mapping exists. [' . $input_field . "]</span><br/>\n";
        return null;
    }

    public function hasMapping($input_field)
    {
        if (array_key_exists($input_field, $this->input_fields)) {
            return true;
        }
        return false;
    }

    public function getOutput_object($label)
    {
        if ($this->hasOutput_object($label)) {
            return $this->output_objects[$label];
        }
        echo '<span class="error">[Mapper:getOutput_object]Error: No output object with label [' . $label . "] exists.</span><br/>\n";
        return null;
    }

    public function getStorer()
    {
        if ($this->hasStorer()) {
            return $this->storer;
        }
        echo '<span class="error">[Mapper:getStorer]Error: No storer object ' . " is set.</span><br/>\n";
        return null;
    }

    public function setStorer($storer)
    {
        $this->storer = $storer;
    }

    public function store($mapping_name, $bedrijfsid)
    {
        $this->reader = null;
        $database = $this->getStorer()->getDB_handle();
        $mapping_name = $database->real_escape_string($mapping_name);
        $sql = "SELECT * FROM importmappings WHERE NAAM='$mapping_name' LIMIT 1;";
        //echo "$sql<br/>";
        $result = $database->query($sql);
        $sql = "INSERT INTO importmappings SET NAAM='$mapping_name', ACTIEF=1, BEDRIJFSID='$bedrijfsid';";
        $id = 0;
        $return = true;
        if ($result && $result->num_rows) {
            $sql = "UPDATE importmappings SET NAAM='$mapping_name', BEDRIJFSID='$bedrijfsid';";
            //echo "$sql<br/>";
            $data = $result->fetch_array();
            $id = $data['ID'];
            $result = $database->query($sql);
            if (!$result) {
                echo '<span class="error">[store]SQL error: [' . $database->error . "][$sql]</span><br/>\n";
                return false;
            }
            $sql = "DELETE FROM importmappingregels WHERE MAPPINGID = $id;";
            //echo "$sql<br/>";
            $result = $database->query($sql);
            if (!$result) {
                echo '<span class="error">[store]SQL error: [' . $database->error . "][$sql]</span><br/>\n";
                return false;
            }
        } else {
            //echo "$sql<br/>";
            $result = $database->query($sql);
            if ($result) {
                $id = $database->insert_id;
            } else {
                echo '<span class="error">[store]SQL error: [' . $database->error . "][$sql]</span><br/>\n";
                return false;
            }
        }
        //if ( $this->isReady() ) {
        foreach ($this->input_fields as $invoer_kolom => $objects) {
            foreach ($objects as $object => $fields) {
                $table = $this->getOutput_object($object)->getTable();
                $key = $this->getOutput_object($object)->getKey();
                foreach ($fields as $field => $default) {
                    $iskey = 0;
                    if ($field == $key) {
                        $iskey = 1;
                    }
                    $default = $database->real_escape_string($default);
                    $invoer_kolom = $database->real_escape_string($invoer_kolom);
                    $table = $database->real_escape_string($table);
                    $field = $database->real_escape_string($field);
                    echo "INVOER: $invoer_kolom OBJECT: $object TABLE: $table ISKEY: " . ($iskey ? 'true' : 'false') . " FIELD: $field DEFAULT: $default<br/>";
                    $sql = "INSERT INTO importmappingregels SET MAPPINGID = '$id', TABEL = '$table', VELD = '$field', INVOERKOLOM = '$invoer_kolom', STANDAARDWAARDE = '$default', ISUPDATEKEY = $iskey;";
                    //echo "$sql<br/>";
                    $result = $database->query($sql);
                    if (!$result) {
                        echo '<span class="error">[store]SQL error: [' . $database->error . "][$sql]</span><br/>\n";
                        return false;
                    }
                }
            }
        }
        return $return;
        //}
        echo '<span class="error">[Mapper:store]Error: The mapping object is not ready yet.' . "</span><br/>\n";
        return false;
    }

    public function fetch($id)
    {
        $sql = "SELECT * FROM importmappingregels WHERE MAPPINGID = $id;";
        $result = $this->getStorer()->getDB_handle()->query($sql);
        if (!$result) {
            echo '<span class="error">[fetch]SQL error: [' . $this->getStorer()->getDB_handle()->error . "][$sql]</span><br/>\n";
            return false;
        }
        while ($data = $result->fetch_array()) {
            $this->addMapping($data['INVOERKOLOM'], $data['TABEL'], $data['VELD'], $data['STANDAARDWAARDE']);
            $this->getOutput_object($data['TABEL'])->setTable($data['TABEL']);
            if ($data['ISUPDATEKEY']) {
                $this->getOutput_object($data['TABEL'])->setKey($data['VELD']);
            }
        }
    }

    public function addMapping($input_field, $output_object, $object_field, $default_value = null, $type = '')
    {
        if (!$this->hasInput_field($input_field)) {
            $this->addInput_field($input_field);
        }
        if (!$this->hasOutput_object($output_object)) {
            $this->addOutput_object($output_object);
        }
        if (!$this->getOutput_object($output_object)->hasField($object_field)) {
            $this->getOutput_object($output_object)->addField($object_field, $type);
        }
        if (!isset($this->input_fields[$input_field][$output_object])) {
            $this->input_fields[$input_field][$output_object] = array();
        }
        $this->input_fields[$input_field][$output_object][$object_field] = $default_value;
    }

    public function addInput_field($key)
    {
        if (!$this->hasInput_field($key)) {
            $this->input_fields[$key] = array();
            return true;
        }
        echo '<span class="error">[Mapper:addInput_field]Error: An input field with key [' . $key . "] already exists.</span><br/>\n";
        return false;
    }

    public function addOutput_object($label)
    {
        if (!$this->hasOutput_object($label)) {
            $this->output_objects[$label] = new MapObject();
            return true;
        }
        return false;
    }

    protected function getOutputObjects()
    {
        return $this->output_objects;
    }
}

//Lev.bon datum,Factuurnr,Fact.datum,Eenheid,Artikelnr,Omschrijving 1,Geleverd,Korting,Netto,Btw bedrag,Totaal bedrag,Subnummer,Naam subnr,extra1,extra2,OMSCHRIJVING,BTWPerc
//Factuurnr = | Fact_datum = Korting = | Subnummer = | Naam_subnr = extra1 = | extra2 = |