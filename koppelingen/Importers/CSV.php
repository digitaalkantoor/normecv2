<?php
namespace Importer;

require_once ('Reader.php');

class CSV implements Reader {
	protected $connection_handle = null;
	protected $reader_parameters = array ();
	protected $field_names = array ();
	
	// TODO: See if you can determine a headerline in a document
	public function explain() {
		return <<<EXPLAIN
A basic CSV reader for very long files.		
		
\$this->connect( array() ) 
Connects the input

The array must contain the following keys and values:
 - path..................A valid path.
 - filename..............Must be a valid path concatenated behind path.
 
The array may contain the following keys, otherwise default values are used:
 - line_separator........The character(s) used to go to a new line. ( default "\\n" )
 - field_separator.......The character used to separate fields. ( default ',' )
 - string_delimiter......The character strings are enclosed in, the field_delimiter 
                         and line_separator character inbetween do not work. ( default '"' )
 - header_line...........The index of the header line. If omitted no header will be assumed.
                         ( and a numbered array will be returned )
                        
\$this->read()
Reads one record from the input. If the last record is already read it wil return null;
If a header line is read returns an associative array with header-field => value pairs
otherwise an array.
Delimited strings may contain field separators, it doesn't break them.
The escape character escapes the next character. An escaped field separator doesn't count, 
an escaped string-delimiter doesn't close a string. 

\$this->reset()
Resets the record pointer to the first record.

\$this->has_next()
Returns true if another record can be read, false otherwise.

\$this->has_header();
Returns true if a headerline is set.

\$this->get_column_count();
Returns the number of columns ( a count, so 1 columns if only column [0] is set )
If not set when read tries the first 15 lines of the file and then resets it.

\$this->disconnect()
Disconnects the input
		
NOTE: If a headerline contains the same string in different fields only one key of that
name will exist. The last one to be precise. If this happens, don't use a header line and
just read an array.
EXPLAIN;
	}
	public function __construct() {
		$this->reader_parameters ['field_count'] = null;
	}
	public function connect($source_parameters) {
		if (array_key_exists ( 'path', $source_parameters )) {
			$this->reader_parameters ['path'] = $source_parameters ['path'];
		}
		if (array_key_exists ( 'filename', $source_parameters )) {
			$this->reader_parameters ['filename'] = $source_parameters ['filename'];
		}
		if (array_key_exists ( 'line_separator', $source_parameters )) {
			$this->reader_parameters ['line_separator'] = $source_parameters ['line_separator'];
		} else {
			$this->reader_parameters ['line_separator'] = "\n";
		}
		if (array_key_exists ( 'field_separator', $source_parameters )) {
			$this->reader_parameters ['field_separator'] = $source_parameters ['field_separator'];
		} else {
			$this->reader_parameters ['field_separator'] = ',';
		}
		if (array_key_exists ( 'string_delimiter', $source_parameters )) {
			$this->reader_parameters ['string_delimiter'] = $source_parameters ['string_delimiter'];
		} else {
			$this->reader_parameters ['string_delimiter'] = '"';
		}
		if (array_key_exists ( 'escape_character', $source_parameters )) {
			$this->reader_parameters ['escape_character'] = $source_parameters ['escape_character'];
		} else {
			$this->reader_parameters ['escape_character'] = '\\';
		}
		if (array_key_exists ( 'header_line', $source_parameters )) {
			$this->reader_parameters ['header_line'] = $source_parameters ['header_line'];
		} else {
			$this->reader_parameters ['header_line'] = false;
		}
		if (file_exists ( $this->reader_parameters ['path'] . $this->reader_parameters ['filename'] )) {
			$this->connection_handle = fopen ( $this->reader_parameters ['path'] . $this->reader_parameters ['filename'], 'r' );
			if ($this->has_header ()) {
				$hl = $this->reader_parameters ['header_line'];
				$this->reader_parameters ['header_line'] = false;
				for($i = 1; $i <= $hl; $i ++) {
					$this->field_names = $this->read ();
				}
				$this->reader_parameters ['header_line'] = $hl;
			}
			return true;
		}
		return false;
	}
	public function read() {
		$sd = $this->reader_parameters ['string_delimiter'];
		$ls = $this->reader_parameters ['line_separator'];
		$fs = $this->reader_parameters ['field_separator'];
		$ec = $this->reader_parameters ['escape_character'];
		$is = false; // inside string_escape characters?
		$co = '';
		$record = array ();
		$field = '';
		$field_count = 0;
		while ( $this->has_next () && ($c = fgetc ( $this->connection_handle )) != '' ) {
			if ($co == $ec) {
				$field .= $c;
				if ($c == $ec) {
					$c = '';
				}
			} else if ($is && $c != $sd && $c != $ec) {
				$field .= $c;
			} else {
				switch ($c) {
					case $sd :
						$is = ! $is;
						if ($co == $c) {
							$field = '';
						}
						break;
					case $ls :
						$this->set_field ( $record, $field_count, $field );
						$field_count ++;
						$this->fill_up ( $record, $field_count );
						return $record;
					case $fs :
						$this->set_field ( $record, $field_count, $field );
						$field_count ++;
						$field = null;
						break;
					case $ec :
						break;
					default :
						$field .= $c;
				}
			}
			$co = $c;
		}
		$this->set_field ( $record, $field_count, $field );
		$field_count ++;
		$this->fill_up ( $record, $field_count );
		return $record;
	}
	protected function set_field(&$array, $fieldindex, $value) {
		if ($this->has_header ()) {
			$array [$this->field_names [$fieldindex]] = $value;
		} else {
			$array [$fieldindex] = $value;
		}
	}
	protected function fill_up(&$array, $fieldindex) {
		if ($fieldindex > $this->reader_parameters ['field_count']) {
			$this->reader_parameters ['field_count'] = $fieldindex;
		}
		if ($fieldindex < $this->reader_parameters ['field_count']) {
			for($i = $fieldindex; $i < $this->reader_parameters ['field_count']; $i ++) {
				if ($this->has_header () == false) {
					$array [$i] = null;
				} else {
					$array [$this->field_names [$i]] = null;
				}
			}
		}
	}
	public function reset() {
		rewind ( $this->connection_handle );
		if ($this->has_header ()) {
			$hl = $this->reader_parameters ['header_line'];
			$this->reader_parameters ['header_line'] = false;
			for($i = 1; $i <= $hl; $i ++) {
				$this->field_names = $this->read ();
			}
			$this->reader_parameters ['header_line'] = $hl;
		}
	}
	public function has_next() {
		return ! feof ( $this->connection_handle );
	}
	public function has_header() {
		if ($this->reader_parameters ['header_line'] === false) {
			return false;
		}
		return true;
	}
	public function get_column_count() {
		if (! isset ( $this->reader_parameters ['field_count'] )) {
			for($i = 0; $i < 15; $i ++) {
				$this->read ();
			}
			$this->reset ();
		}
		return $this->reader_parameters ['field_count'];
	}
	public function disconnect($disconnect_parameters = null) {
		return fclose ( $this->connection_handle );
	}
}