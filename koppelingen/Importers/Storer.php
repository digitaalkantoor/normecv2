<?php

namespace Importer;

class Storer
{
    protected $db_handle = null;
    protected $allow_update = false;
    protected $data_object = null;

    public function store($object)
    {
        $query = $this->makeInsert_query($object);
        $newid = true;
        if ($this->allow_update() && $this->recordExists($object)) {
            return $this->update($object);
        } else {
            return $this->insert($object);
        }
    }

    protected function makeInsert_query($object)
    {
        $table = $object->getTable();
        $key = $object->getKey();
        $value = $object->getField($key);
        $query = "INSERT INTO `$table` SET ";
        $fields = array();
        foreach ($object->getFields() as $fkey => $fvalue) {
            // Check op fkey ingebouwd ging fout als hij leeg was : J. 
            if (trim($fkey)) {
                $fields[] = "`$fkey` = '" . $this->getDB_handle()->real_escape_string($fvalue) . "'";
            }
        }
        $query .= implode(', ', $fields);
        $query .= ";";
        return $query;
    }

    public function getDB_handle()
    {
        if ($this->hasDB_handle()) {
            return $this->db_handle;
        }
        echo '<span class="error">[Storer:getDB_handle]Error: No database handle was set.' . "</span><br/>\n";
        return null;
    }

    public function setDB_handle($db_handle)
    {
        $this->db_handle = $db_handle;
    }

    public function hasDB_handle()
    {
        if (!is_null($this->db_handle)) {
            return true;
        }
        return false;
    }

    public function allow_update()
    {
        return $this->getAllow_update();
    }

    public function getAllow_update()
    {
        return $this->allow_update;
    }

    public function setAllow_update($boolean = true)
    {
        if (is_bool($boolean)) {
            $this->allow_update = $boolean;
            return true;
        }
        echo '<span class="error">[Storer:ssetAllow_update]Error: A boolean is expected. [' . $boolean . "]</span><br/>\n";
        return false;
    }

    public function recordExists($object)
    {
        $table = $object->getTable();
        $key = $object->getKey();
        if (!$key) {
            $key = 'ID';
        }
        $value = $object->getField($key);
        $query = "SELECT * FROM `$table` WHERE `$key` = '" . $this->getDB_handle()->real_escape_string($value) . "' LIMIT 1;";
        $result = $this->getDB_handle()->query($query);
        if ($result && $result->num_rows) {
            $line = $result->fetch_array();
            $object->setId($line['ID']);
            return true;
        }
        if ($this->getDB_handle()->error) {
            echo '<span class="error">[recordExists]SQL error: [' . $this->getDB_handle()->error . "][$query]</span><br/>\n";
        }
        return false;
    }

    public function update($object)
    {
        $query = $this->makeUpdate_query($object);
        //echo $query. '<br/>';
        $result = $this->getDB_handle()->query($query);
        if ($result && !$this->getDB_handle()->error) {
            return true;
        }
        if ($this->getDB_handle()->error) {
            echo '<span class="error">[update]SQL error: [' . $this->getDB_handle()->error . "][$query]</span><br/>\n";
        }
        return false;
    }

    protected function makeUpdate_query($object)
    {
        $table = $object->getTable();
        $key = $object->getKey();
        $value = $object->getField($key);
        $query = "UPDATE `$table` SET ";
        $fields = array();
        foreach ($object->getFields() as $fkey => $fvalue) {
            // Hier ook check op fkey ingebouwd : J.
            if (trim($fkey)) {
                $fields[] = "`$fkey` = '" . $this->getDB_handle()->real_escape_string($fvalue) . "'";
            }
        }
        $query .= implode(', ', $fields);
        $query .= " WHERE `$key` = '" . $this->getDB_handle()->real_escape_string($value) . "';";
        return $query;
    }

    public function insert($object)
    {
        $query = $this->makeInsert_query($object);
        //echo $query. '<br/>';
        $result = $this->getDB_handle()->query($query);
        if ($result && !$this->getDB_handle()->error) {
            $object->setId($this->getDB_handle()->insert_id);
            return true;
        }
        if ($this->getDB_handle()->error) {
            echo '<span class="error">[insert]SQL error: [' . $this->getDB_handle()->error . "][$query]</span><br/>\n";
        }
        return false;
    }
}