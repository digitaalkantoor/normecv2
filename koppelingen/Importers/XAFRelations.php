<?php

namespace Importer;

require_once('Reader.php');

class XAFRelations implements Reader
{
    protected $record_counter = 0;
    protected $record_pointer = 0;
    protected $source_parameters = array();
    protected $structure = array();

    /*
<customerSupplier>
  <custSupID>1000001</custSupID>
  <type>Customer</type>
  <taxRegistrationNr />
  <companyName>Onbekende klant</companyName>
  <streetAddress>
    <country>NL</country>
  </streetAddress>
</customerSupplier>
<customerSupplier>
  <custSupID>1000002</custSupID>
    <type>Customer</type>
    <taxRegistrationNr />
    <companyName />
      <streetAddress>
        <country>NL</country>
      </streetAddress>
    </customerSupplier>
    */

    public function connect($source_parameters)
    {
        if (array_key_exists('path', $source_parameters)) {
            $this->reader_parameters['path'] = $source_parameters['path'];
        }
        if (array_key_exists('filename', $source_parameters)) {
            $this->reader_parameters['filename'] = $source_parameters['filename'];
        }
        if (file_exists($this->reader_parameters['path'] . $this->reader_parameters['filename'])) {
            $xml = file_get_contents($this->reader_parameters['path'] . $this->reader_parameters['filename']);
            $full = simplexml_load_string($xml);
            $line = array();
            if (isset($full->customersSuppliers)) {
                foreach ($full->customersSuppliers->customerSupplier as $relation) {
                    $line['relationId'] = $relation->custSupID->__toString();
                    $line['relationType'] = $relation->type->__toString();
                    $line['relationBtwNr'] = $relation->taxRegistrationNr->__toString();
                    $line['relationBtwDatum'] = $relation->taxVerificationDate->__toString();
                    $line['relationName'] = $relation->companyName->__toString();
                    $line['relationContact'] = $relation->contact->__toString();
                    $line['relationTelephone'] = $relation->telephone->__toString();
                    $line['relationFax'] = $relation->fax->__toString();
                    $line['relationEmail'] = $relation->email->__toString();
                    $line['relationWebsite'] = $relation->website->__toString();

                    if (isset($relation->streetAddress->address)) {
                        $line['streetaddressAddress'] = $relation->streetAddress->address->__toString();
                    }
                    if (isset($relation->streetAddress->streetname)) {
                        $line['streetaddressStreetname'] = $relation->streetAddress->streetname->__toString();
                    }
                    if (isset($relation->streetAddress->number)) {
                        $line['streetaddressNumber'] = $relation->streetAddress->number->__toString();
                    }
                    if (isset($relation->streetAddress->numberExtension)) {
                        $line['streetaddressExtension'] = $relation->streetAddress->numberExtension->__toString();
                    }
                    if (isset($relation->streetAddress->property)) {
                        $line['streetaddressProperty'] = $relation->streetAddress->property->__toString();
                    }
                    if (isset($relation->streetAddress->city)) {
                        $line['streetaddressCity'] = $relation->streetAddress->city->__toString();
                    }
                    if (isset($relation->streetAddress->postalCode)) {
                        $line['streetaddressPostalCode'] = $relation->streetAddress->postalCode->__toString();
                    }
                    if (isset($relation->streetAddress->region)) {
                        $line['streetaddressRegion'] = $relation->streetAddress->region->__toString();
                    }
                    if (isset($relation->streetAddress->country)) {
                        $line['streetaddressCountry'] = $relation->streetAddress->country->__toString();
                    }
                    if (isset($relation->postalAddress->address)) {
                        $line['postaladdressAddress'] = $relation->postalAddress->address->__toString();
                    }
                    if (isset($relation->postalAddress->streetname)) {
                        $line['postaladdressStreetname'] = $relation->postalAddress->streetname->__toString();
                    }
                    if (isset($relation->postalAddress->number)) {
                        $line['postaladdressNumber'] = $relation->postalAddress->number->__toString();
                    }
                    if (isset($relation->postalAddress->numberExtension)) {
                        $line['postaladdressExtension'] = $relation->postalAddress->numberExtension->__toString();
                    }
                    if (isset($relation->postalAddress->property)) {
                        $line['postaladdressProperty'] = $relation->postalAddress->property->__toString();
                    }
                    if (isset($relation->postalAddress->city)) {
                        $line['postaladdressCity'] = $relation->postalAddress->city->__toString();
                    }
                    if (isset($relation->postalAddress->postalCode)) {
                        $line['postaladdressPostalCode'] = $relation->postalAddress->postalCode->__toString();
                    }
                    if (isset($relation->postalAddress->region)) {
                        $line['postaladdressRegion'] = $relation->postalAddress->region->__toString();
                    }
                    if (isset($relation->postalAddress->country)) {
                        $line['postaladdressCountry'] = $relation->postalAddress->country->__toString();
                    }
                    $this->structure[] = $line;
                    $this->record_counter++;
                    $line = array();
                }
                unset($xml);
                unset($full);
                return true;
            }
        }
        return false;
    }

    public function read()
    {
        if ($this->has_next()) {
            return $this->structure[$this->record_pointer++];
        }
    }

    public function has_next()
    {
        if ($this->record_pointer < $this->record_counter) {
            return true;
        }
        return false;
    }

    public function reset()
    {
        $this->record_pointer = 0;
    }

    public function has_header()
    {
        return true;
    }

    public function get_column_count()
    {
        return 28;
    }

    public function disconnect($disconnect_parameters)
    {
    }
}