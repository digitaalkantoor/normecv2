<?php

namespace Importer;

require_once('Reader.php');
if (file_exists('./koppelingen/PHPExcel.1.8.0/Classes/PHPExcel.php')) {
    require_once('./koppelingen/PHPExcel.1.8.0/Classes/PHPExcel.php');
}

class Excel implements Reader
{
    protected $connection_handle = null; // row iterator of a worksheet
    protected $worksheet = null; // the loaded worksheet
    protected $reader_parameters = array(); // given parameters
    protected $field_names = array(); // header line
    protected $line_pointer = 0;
    protected $reader = null; // \PHPExcel_IOFactory::load reader object

    // TODO: See if you can determine a headerline in a document
    public function explain()
    {
        return <<<EXPLAIN
A basic Excel reader based on PHPExcel.    

\$this->connect( array() )
Connects the input;

The array must contain the following keys and values:
- path..................A valid path.
- filename..............Must be a valid path concatenated behind path.

The array may contain the following keys, otherwise default values are used:
- sheet_index...........The index number of the worksheet, if omitted the active worksheet is used.
- header_line...........The number of the header line. If omitted no header will be assumed.
        ( and a numbered array will be returned )

\$this->read()
Reads one record from the input. If the last record is already read it wil return null;
If a header line is read returns an associative array with header-field => value pairs
otherwise an array.

\$this->reset()
Resets the record pointer to the first record.

\$this->has_next()
Returns true if another record can be read, false otherwise.

\$this->has_header();
Returns true if a headerline is set.

\$this->get_column_count();
Returns the number of columns ( a count, so 1 columns if only column [0] is set )

\$this->disconnect()
Disconnects the input

NOTE: If a headerline contains the same string in different fields only one key of that
name will exist. The last one to be precise. If this happens, don't use a header line and
just read an array.
EXPLAIN;
    }

    public function connect($source_parameters)
    {
        if (array_key_exists('path', $source_parameters)) {
            $this->reader_parameters ['path'] = $source_parameters ['path'];
        }
        if (array_key_exists('filename', $source_parameters)) {
            $this->reader_parameters ['filename'] = $source_parameters ['filename'];
        }
        if (array_key_exists('header_line', $source_parameters)) {
            $this->reader_parameters ['header_line'] = $source_parameters ['header_line'];
        } else {
            $this->reader_parameters ['header_line'] = false;
        }
        $filename = $this->reader_parameters ['path'] . $this->reader_parameters ['filename'];
        if (file_exists($filename)) {

            // Make reader for Excel file
            $this->reader = \PHPExcel_IOFactory::load($filename);

            if (array_key_exists('sheet_index', $source_parameters)) {
                $this->reader_parameters ['sheet_index'] = $source_parameters ['sheet_index'];
                $this->openWorksheet($source_parameters['sheet_index']);
            } else {
                $this->openWorksheet();
            }
            return true;
        }
        die ("Connection failed: " . $source_parameters ['path'] . $source_parameters ['filename'] . " is not a valid path.");
        return false;
    }

    public function openWorksheet($index = false)
    {
        if ($index !== false) {
            $this->worksheet = $this->reader->getSheet($index);
        } else {
            $this->worksheet = $this->reader->getActiveSheet();
        }

        $hc = $this->worksheet->getHighestColumn();
        $this->reader_parameters ['field_count'] = \PHPExcel_Cell::columnIndexFromString($hc);
        $this->connection_handle = $this->worksheet->getRowIterator();
        if ($this->has_header()) {
            $hl = $this->reader_parameters ['header_line'] * 1;
            $this->reader_parameters ['header_line'] = false;
            for ($i = 1; $i <= $hl; $i++) {
                $this->field_names = $this->read();
            }
            $this->reader_parameters ['header_line'] = $hl;
            $this->postProcessHeaders();
        }
    }

    public function has_header()
    {
        if (!$this->reader_parameters ['header_line']) {
            return false;
        }
        return true;
    }

    public function read()
    {
        $return = null;
        if ($this->has_next()) {
            $return = array();
            $index = 0;
            $cell_iterator = $this->connection_handle->current()->getCellIterator();
            $cell_iterator->setIterateOnlyExistingCells(false);
            foreach ($cell_iterator as $cell) {
                if ($this->has_header() == false) {
                    $return[$index] = $cell->getValue();
                } else {
                    // ToDo: Does not work with multiple column headers with equal values
                    $return[$this->field_names[$index]] = $cell->getValue();
                }
                $index++;
            }
            $this->connection_handle->next();
        } else {
            die("Trying to read beyond end of file.");
        }
        //echo implode( ',', $return ). "<br/>\n";
        return $return;
    }

    public function has_next()
    {
        return $this->connection_handle->valid();
    }

    protected function postProcessHeaders()
    {
        $header = $this->get_header_fields();
        $processed = array();
        foreach ($header as $columnhead) {
            $columnhead = preg_replace('/%/', '_perc', $columnhead);
            $columnhead = preg_replace('/#/', '_nr', $columnhead);
            $columnhead = preg_replace('/[._:;, @"\']/', '_', $columnhead);
            $processed[] = preg_replace('/_+/', '_', $columnhead);
        }
        $this->field_names = $processed;
    }

    public function get_header_fields()
    {
        if ($this->has_header()) {
            return $this->field_names;
        }
        $return = array();
        for ($i = 1; $i <= $this->reader_parameters['field_count']; $i++) {
            $return[$i] = $i;
        }
        return $return;
    }

    public function reset()
    {
        $this->connection_handle->rewind();
        if ($this->has_header()) {
            $hl = $this->reader_parameters ['header_line'] * 1;
            $this->reader_parameters ['header_line'] = false;
            for ($i = 1; $i <= $hl; $i++) {
                $this->read();
            }
            $this->reader_parameters ['header_line'] = $hl;
        }
    }

    public function get_column_count()
    {
        return $this->reader_parameters ['field_count'];
    }

    public function disconnect($disconnect_parameters = null)
    {
    }

    /*
     * Public function to open a worksheet by its index number
     * @param (int) $index The index of the worksheet to open
     *  when index is false, active worksheet is opened
     * @return (bool)
     * @author: RJP Granneman
     */

    public function sheetNames()
    {
        //return $this->reader->listWorksheetNames($this->reader_parameters ['filename']);
        return $this->reader->getSheetNames();
    }
}