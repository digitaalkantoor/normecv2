<?php

namespace Importer;

interface Reader
{
    public function connect($source_parameters);

    public function read();

    public function reset();

    public function has_next();

    public function has_header();

    public function get_column_count();

    public function disconnect($disconnect_parameters);
}