<?php

namespace Importer;

require_once('Reader.php');

class XAFLedger implements Reader
{
    protected $record_counter = 0;
    protected $record_pointer = 0;
    protected $source_parameters = array();
    protected $structure = array();

    public function connect($source_parameters)
    {
        if (array_key_exists('path', $source_parameters)) {
            $this->reader_parameters['path'] = $source_parameters['path'];
        }
        if (array_key_exists('filename', $source_parameters)) {
            $this->reader_parameters['filename'] = $source_parameters['filename'];
        }
        if (file_exists($this->reader_parameters['path'] . $this->reader_parameters['filename'])) {
            $xml = file_get_contents($this->reader_parameters['path'] . $this->reader_parameters['filename']);
            $full = simplexml_load_string($xml);
            $line = array();
            if (isset($full->generalLedger)) {
                foreach ($full->generalLedger as $ledgers) {
                    foreach ($ledgers->ledgerAccount as $ledger) {
                        $line['ledgerTaxonomy'] = $ledgers->taxonomy->__toString();
                        $line['ledgerAccountId'] = $ledger->accountID->__toString();
                        $line['ledgerAccountDescription'] = $ledger->accountDesc->__toString();
                        $line['ledgerAccountType'] = $ledger->accountType->__toString();
                        if (strtolower($ledger->accountType->__toString()) == 'activa') {
                            $line['ledgerDebetCredit'] = 'Debet';
                            $line['BALANSKOSTEN'] = "Balans";
                        } elseif (strtolower($ledger->accountType->__toString()) == 'passiva') {
                            $line['ledgerDebetCredit'] = 'Credit';
                            $line['BALANSKOSTEN'] = "Balans";
                        } elseif (strtolower($ledger->accountType->__toString()) == 'kosten') {
                            $line['ledgerDebetCredit'] = 'Debet';
                            $line['BALANSKOSTEN'] = "Kosten of opbrengst";
                        } elseif (strtolower($ledger->accountType->__toString()) == 'opbrengsten') {
                            $line['ledgerDebetCredit'] = 'Credit';
                            $line['BALANSKOSTEN'] = "Kosten of opbrengst";
                        }
                        $line['ledgerAccountLeadCode'] = $ledger->leadCode->__toString();
                        $line['ledgerAccountLeadDescription'] = $ledger->leadDescription->__toString();
                        $this->structure[] = $line;
                        $this->record_counter++;
                        $line = array();
                    }
                }
                unset($xml);
                unset($full);
                return true;
            }
        }
        return false;
    }

    public function read()
    {
        if ($this->has_next()) {
            return $this->structure[$this->record_pointer++];
        }
    }

    public function has_next()
    {
        if ($this->record_pointer < $this->record_counter) {
            return true;
        }
        return false;
    }

    public function reset()
    {
        $this->record_pointer = 0;
    }

    public function has_header()
    {
        return true;
    }

    public function get_column_count()
    {
        return 7;
    }

    public function disconnect($disconnect_parameters)
    {
    }
}