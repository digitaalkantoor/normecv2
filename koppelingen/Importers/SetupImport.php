<?php

namespace Importer;

require_once('Mapper.php');
require_once('Storer.php');

$mapfields_array = array();
$keyfields_array = array();

if (isset ($_POST ['importer'])) {
}
function setup_import($path, $filename, $mapfields_array, $header = null)
{
    $extension = '';
    $readerdata = array(
        'path' => $path,
        'filename' => $filename
    );

    if (isset ($header) && $header > 0) {
        $readerdata ['header'] = $header;
    }

    if (preg_match('/\.(?<ext>\w+)$/', $filename, $matches)) {
        $extension = $matches ['ext'];
    }

    $extension = strtolower($extension);
    echo "Filetype found : [$extension]<br/>\n";

    // If the number of readers exceeds 5 it may be a good idea to change this into a
    // database lookup. Definition values of the $readerdata are stored in a table.
    switch ($extension) {
        case 'xls' :
            $readerdata ['class'] = 'Excel';
            break;
        case 'xlsx' :
            $readerdata ['class'] = 'Excel';
            break;
        case 'csv' :
            $readerdata ['class'] = 'CSV';
            break;
        default :
            die ("No reader module is found to read files of type [$extension]");
    }

    return setup_import_form($readerdata);
}

function setup_import_form($readerdata, $mapfields_array)
{
    load_required_class($readerdata ['class']);

    $reader = new $readerData ['class'] ();
    $reader->connect($readerdata);
    $sample = $reader->read();
    $headers = array_keys($sample);

    $form = '<form name="setup_reader" method="POST" action="s">';
    $form = '<input type="hidden" name="importer" value="true"/>';
    $form .= '<span class="information_header">Determined file type:</span>';
    $form .= '<span class="information_data">' . $readerdata ['class'] . '</span><br/>';
    $form .= '<span class="remark">' . tl('Kies het veld waarbij de kolom hoort.') . '</span>';
    foreach ($headers as $header) {
        $form .= '<span class="information_header" >' . $header . '</span>';
        $form .= '<span class="information_data" >';
        $form .= make_selector("${header}_map", $mapfields_array);
        $form .= '</span><br/>';
    }
    $form .= '<span class="information_header"></span>';
    $form .= '<span class="information_data">' . /* submit */
        '</span>';
    $form .= '</form>';

    return $form;
}

function setup_import_process($readerdata, $keyfields_array, $update = null)
{
    global $db;
    load_required_class($readerdata ['class']);

    $reader = new $readerData ['class'] ();
    $reader->connect($readerdata);
    $sample = $reader->read();
    $headers = array_keys($sample);

    $map = new Mapper ();
    $store = new Storer ();
    $store->setDB_handle($db);
    if (isset ($update)) {
        $store->setAllow_update($update);
    }

    $map->setReader($reader);
    $map->setStorer($store);

    foreach ($headers as $header) {
        if (array_key_exists("${header}_map", $_POST)) {
            if (strpos($_POST ["${header}_map"], ':')) {
                list ($table, $column) = explode(':', $_POST ["${header}_map"]);
                $map->addMapping($header, $table, $column);
                $map->getOutput_object($table)->setTable($table);
                if (array_key_exists($table, $keyfields_array)) {
                    $map->getOutput_object($table)->setKey($keyfields_array [$table]);
                }
            }
        }
    }

    $map->process();
}

function load_required_class($classname)
{
    if (!class_exists($classname)) {
        $classpath = $classname . '.php';
        if (file_exists($classpath)) {
            require_once($classpath);
        } else {
            die ("The reader module required to read files of this type cannot be found at [$classpath]");
        }
    }
    return true;
}

function make_selector($name, $datastructure)
{
    $return = "<select name=\"$name\">";
    foreach ($datastructure as $label => $value) {
        $return .= "<option value=\"$value\">$label</option>";
    }
    $return .= "</select>";
    return $return;
}