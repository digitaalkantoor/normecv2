<?php

namespace Importer;

class MapObject
{
    protected $fields = array();
    protected $fieldtypes = array();
    protected $table = null;
    protected $key = null;
    protected $id = 0;

    public function getTable()
    {
        if (isset($this->table)) {
            return $this->table;
        }
        echo '<span class="error">[MapObject:getTable]Error: No table was set.' . "</span><br/>\n";
        return null;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function getKey()
    {
        if (isset($this->key)) {
            return $this->key;
        }
        return null;
    }

    public function setKey($key)
    {
        if ($this->hasField($key)) {
            $this->key = $key;
            return true;
        }
        echo '<span class="error">[MapObject:setKey]Error: The key [' . $key . "] must be a valid field.</span><br/>\n";
        return false;
    }

    public function hasField($label)
    {
        if (array_key_exists($label, $this->fields)) {
            return true;
        }
        return false;
    }

    public function getId()
    {
        if ($this->hasId()) {
            return $this->id;
        }
        echo '<span class="error">[MapObject:getId]Error: No identifier was set.' . "</span><br/>\n";
        return null;
    }

    public function setId($id)
    {
        $id *= 1;
        if (is_integer($id)) {
            $this->id = $id;
            return true;
        }
        echo '<span class="error">[MapObject:setId]Error: An identifier is an integer. [' . $id . "]</span><br/>\n";
        return false;
    }

    public function hasId()
    {
        if (!is_null($this->id)) {
            return true;
        }
        return false;
    }

    public function addField($label, $type = '')
    {
        if (!$this->hasField($label)) {
            $this->fields[$label] = null;
            $this->fieldtypes[$label] = $type;
            return true;
        }
        echo '<span class="error">[MapObject:addField]Error: A field with label [' . $label . "] already exists.</span><br/>\n";
        return false;
    }

    public function deleteField($label)
    {
        if ($this->hasField($label)) {
            unset($this->fields[$label]);
            return true;
        }
        echo '<span class="error">[MapObject:deleteField]Error: No field with label [' . $label . "] exists.</span><br/>\n";
        return false;
    }

    public function setField($label, $value)
    {
        if ($this->hasField($label)) {
            $this->fields[$label] = $value;
            return true;
        }
        echo '<span class="error">[MapObject:setField]Error: No field with label [' . $label . "] exists.</span><br/>\n";
        return false;
    }

    public function getField($label)
    {
        if ($this->hasField($label)) {
            return $this->fields[$label];
        }
        echo '<span class="error">[MapObject:getField]Error: No field with label [' . $label . "] exists.</span><br/>\n";
        return null;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function clear()
    {
        foreach (array_keys($this->fields) as $label) {
            $this->fields[$label] = null;
        }
    }
}