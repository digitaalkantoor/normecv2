<?php

namespace Importer;

require_once('XAF.php');
require_once('Mapper.php');
require_once('Storer.php');

function read_auditfile($path, $database, $allow_update = false, $delete_old = false, $bedrijfsid = null)
{
    if ($delete_old) {
        delete_old_data($database, $bedrijfsid);
    }
    $xaf = new XAF();

    $xaf->connect(array('path' => $path, 'filename' => ''));
    return process_xaf($xaf, $database, $allow_update, $bedrijfsid);
}


function delete_old_data($database, $bedrijfsid = null)
{

    /*$deleteContent = array( 'klanten', 'balansrubriek', 'stam_dagboeken', 'balans' );
      foreach ( $deleteContent as $table ) {
        $sql = "DELETE FROM `$table` WHERE BEDRIJFSID = $bedrijfsid;";
        $result = $database->query( $sql );
        if ( !$result && $database->error ) {
          die ( "SQL eror : [". $db->error. "][$sql]<br/>" );
        }
      }
      $sql = 'DELETE FROM grootboekitems WHERE AANPASBAAR != "Nee";';
      $result = $database->query( $sql );
      if ( !$result && $database->error ) {
        die ( "SQL eror : [". $db->error. "][$sql]<br/>" );
      }
      return true;*/
    return false;
}

function process_xaf($xaf, $database, $allow_update, $bedrijfsid = null)
{
    $result = true;
    $BID = null;
    $result &= processHeader($xaf, $database, $bedrijfsid, $allow_update);
    $result &= processLedger($xaf, $database, $bedrijfsid, $allow_update);
    $result &= processRelations($xaf, $database, $bedrijfsid, $allow_update);
    $result &= processTransactions($xaf, $database, $bedrijfsid, $allow_update);
    return $result;
}

function processHeader($xaf_object, $db, &$bedrijfsid, $allow_update)
{
    $storer = new Storer();
    $storer->setDB_handle($db);
    $storer->setAllow_update($allow_update);

    $reader = $xaf_object->getHeader();

    $mapper = new Mapper();
    $mapper->setReader($reader);
    $mapper->setStorer($storer);
//---------------------------------------	
//	$mapper->addOutput_object( 'import' );
//	$mapper->getOutput_object( 'import' )->setTable( 'imports' );
//	
//	$mapper->addMapping( 'auditfileVersion',	'import', '');
//	$mapper->addMapping( 'dateCreated',			'import', '');
//	$mapper->addMapping( 'productId',			'import', '');
//	$mapper->AddMapping( 'productVersion',		'import', ''); 
//	
//	$mapper->getOutput_object( 'import' )->setKey( 'IMPORT_DATE' );
//---------------------------------------	
    $mapper->addOutput_object('bedrijf');
    $mapper->getOutput_object('bedrijf')->setTable('bedrijf');

    $mapper->addMapping('companyId', 'bedrijf', 'BEDRIJFNR');
    $mapper->addMapping('companyBtwNr', 'bedrijf', 'BTWNR');
    $mapper->addMapping('companyName', 'bedrijf', 'BEDRIJFSNAAM');
    $mapper->addMapping('companyAddress', 'bedrijf', 'ADRES');
    $mapper->addMapping('companyCity', 'bedrijf', 'PLAATS');
    $mapper->addMapping('companyPostalCode', 'bedrijf', 'POSTCODE');
    $mapper->addMapping('', 'bedrijf', 'UPDATEDATUM', date("Y-m-d"));
    $mapper->addMapping('', 'bedrijf', 'DELETED', 'Nee');

    $mapper->getOutput_object('bedrijf')->setKey('BEDRIJFNR');
//---------------------------------------
//  $mapper->addOutput_object( 'settings' );
//  $mapper->getOutput_object( 'settings' )->setTable( 'settings' );
//  
//  $mapper->addMapping( 'fiscalYear',      'settings', '');
//  $mapper->addMapping( 'startDate',      'settings', '');
//  $mapper->addMapping( 'endDate',        'settings', '');
//  $mapper->addMapping( 'currencyCode',    'settings', '');
//  
//  $mapper->getOutput_object( 'settings' )->setKey( 'JAAR' );
//---------------------------------------  
    $return = $mapper->process();
    $bedrijfsid = $mapper->getOutput_object('bedrijf')->getId(); // Na de store, anders kan de ID niet bekend zijn.
    return $return;
}

function processLedger($xaf_object, $db, $bedrijfsid, $allow_update)
{
    $storer = new Storer();
    $storer->setDb_handle($db);
    $storer->setAllow_update($allow_update);

    $reader = $xaf_object->getLedger();

    $mapper = new Mapper();
    $mapper->setReader($reader);
    $mapper->setStorer($storer);
//---------------------------------------
    $mapper->addOutput_object('grootboek');
    $mapper->getOutput_object('grootboek')->setTable('grootboekitems');

    $mapper->addMapping('ledgerAccountDescription', 'grootboek', 'OMSCHRIJVING');
    $mapper->addMapping('ledgerAccountDescription', 'grootboek', 'NAAM');
    $mapper->addMapping('ledgerAccountType', 'grootboek', 'TYPE');
    $mapper->addMapping('balansrubriek', 'grootboek', 'RUBRIEK');
    $mapper->addMapping('ledgerDebetCredit', 'grootboek', 'CREDITDEBET', 'Debet');
    $mapper->addMapping('ledgerAccountId', 'grootboek', 'GROOTBOEKNR');
    $mapper->addMapping('BALANSKOSTEN', 'grootboek', 'BALANSKOSTEN', 'Balans');
    $mapper->addMapping('', 'grootboek', 'GEBLOKKEERD', 'Nee');
    $mapper->addMapping('', 'grootboek', 'TOONINGRAPH', 'Nee');
    $mapper->addMapping('', 'grootboek', 'ISSALARIS', 'Nee');
    $mapper->addMapping('', 'grootboek', 'OFFERTENR', '');
    $mapper->addMapping('', 'grootboek', 'AANPASBAAR', 'Ja');
    $mapper->addMapping('', 'grootboek', 'NIETBESCHIKBAARIN', '');
    $mapper->addMapping('', 'grootboek', 'DAGBOEK', '1');  //TODO: stam_dagboeken?
    $mapper->addMapping('', 'grootboek', 'BEDRIJFSID', $bedrijfsid);

    $mapper->getOutput_object('grootboek')->setKey('GROOTBOEKNR');
//---------------------------------------
    $mapper->addOutput_object('balansrubriek');
    $mapper->getOutput_object('balansrubriek')->setTable('balansrubriek');

    $mapper->addMapping('ledgerAccountLeadDescription', 'balansrubriek', 'NAAM');
    $mapper->addMapping('ledgerAccountLeadDescription', 'balansrubriek', 'OMSCHRIJVING');
    $mapper->addMapping('ledgerAccountLeadCode', 'balansrubriek', 'NR');
    $mapper->addMapping('', 'balansrubriek', 'BEDRIJFSID', $bedrijfsid);

    $mapper->getOutput_object('balansrubriek')->setKey('NAAM');
//---------------------------------------
    return $mapper->process();
}

function processRelations($xaf_object, $db, $bedrijfsid, $allow_update)
{
    $storer = new Storer();
    $storer->setDb_handle($db);
    $storer->setAllow_update($allow_update);

    $reader = $xaf_object->getRelations();

    $mapper = new Mapper();
    $mapper->setReader($reader);
    $mapper->setStorer($storer);
//---------------------------------------
    $mapper->addOutput_object('klant');
    $mapper->getOutput_object('klant')->setTable('klanten');

    $mapper->addMapping('relationId', 'klant', 'DEBITEURNR');
    $mapper->addMapping('relationType', 'klant', 'TYPERELATIE');
    $mapper->addMapping('relationBtwNr', 'klant', 'BTWNR'); // Lekker consistent "Relation" en "BTW", da's "VAT"!
    $mapper->addMapping('relationBtwDatum', 'klant', 'BTWPLICHTIG');
    $mapper->addMapping('relationName', 'klant', 'BEDRIJFSNAAM');
    $mapper->addMapping('relationContact', 'klant', 'ACHTERNAAM');
    $mapper->addMapping('relationTelephone', 'klant', 'TELVAST');
    $mapper->addMapping('relationFax', 'klant', 'FAX');
    $mapper->addMapping('relationEmail', 'klant', 'EMAIL');
    $mapper->addMapping('relationWebsite', 'klant', 'WWW');
    $mapper->addMapping('streetaddressAddress', 'klant', 'ADRES');
    $mapper->addMapping('streetaddressStreetname', 'klant', 'ADRES');
    $mapper->addMapping('streetaddressNumber', 'klant', 'HUISNR');
    $mapper->addMapping('streetaddressExtension', 'klant', 'TOEVOEGING');
    //$mapper->addMapping( 'streetaddressProperty',  'klant', '' );
    $mapper->addMapping('streetaddressCity', 'klant', 'PLAATS');
    $mapper->addMapping('streetaddressPostalCode', 'klant', 'POSTCODE');
    //$mapper->addMapping( 'streetaddressRegion',    'klant', '' );
    $mapper->addMapping('streetaddressCountry', 'klant', 'LAND');
    $mapper->addMapping('postaladdressAddress', 'klant', 'KANTOORADRES');
    $mapper->addMapping('postaladdressStreetname', 'klant', 'KANTOORADRES');
    $mapper->addMapping('postaladdressNumber', 'klant', 'KANTOORHUISNR');
    $mapper->addMapping('postaladdressExtension', 'klant', 'KANTOORTOEVOEGING');
    //$mapper->addMapping( 'postaladdressProperty',  'klant', '' );
    $mapper->addMapping('postaladdressCity', 'klant', 'KANTOORPLAATS');
    $mapper->addMapping('postaladdressPostalCode', 'klant', 'KANTOORPOSTCODE');
    //$mapper->addMapping( 'postaladdressRegion',    'klant', '' );
    $mapper->addMapping('', 'klant', 'BEDRIJFSID', $bedrijfsid);

    $mapper->getOutput_object('klant')->setKey('DEBITEURNR');
//---------------------------------------  
    return $mapper->process();
}

function processTransactions($xaf_object, $db, $bedrijfsid, $allow_update)
{
    $storer = new Storer();
    $storer->setDb_handle($db);
    $storer->setAllow_update($allow_update);

    $reader = $xaf_object->getTransactions();

    $mapper = new Mapper();
    $mapper->setReader($reader);
    $mapper->setStorer($storer);
//---------------------------------------
    $mapper->addOutput_object('dagboek');
    $mapper->getOutput_object('dagboek')->setTable('stam_dagboeken');

    $mapper->addMapping('journalDescription', 'dagboek', 'NAAM');
    $mapper->addMapping('journalDescription', 'dagboek', 'OPMERKING');
    $mapper->addMapping('', 'dagboek', 'BEDRIJFSID', $bedrijfsid);

    $mapper->getOutput_object('dagboek')->setKey('OPMERKING');
//---------------------------------------
    $mapper->addOutput_object('balans');
    $mapper->getOutput_object('balans')->setTable('balans');

//$mapper->addMapping( 'recordId',        'balans', '' );
//$mapper->addMapping( 'recordDocumentId',    'balans', '' );
    $mapper->addMapping('recordId', 'balans', 'BATCHCODE');
    $mapper->addMapping('transactionDescription', 'balans', 'OMSCHRIJVING');
    $mapper->addMapping('transactionPeriod', 'balans', 'BOEKDATUM', 'date');
    $mapper->addMapping('transactionDate', 'balans', 'DATUM', 'date');
    $mapper->addMapping('recordAccountIdDebet', 'balans', 'GROOTBOEKNR');
    $mapper->addMapping('recordAccountIdCredit', 'balans', 'TEGENREKENING');
    $mapper->addMapping('recordEffectiveDate', 'balans', 'DATUM', 'date');
    $mapper->addMapping('recordEffectiveDate', 'balans', 'BOEKDATUM', 'date');
    $mapper->addMapping('recordDescription', 'balans', 'OMSCHRIJVING');
    $mapper->addMapping('recordAmount', 'balans', 'BEDRAG');
    $mapper->addMapping('dagboek', 'balans', 'TYPE'); //Dagboek id
    $mapper->addMapping('custSupID', 'balans', 'KLANTID');
    $mapper->addMapping('', 'balans', 'BEDRIJFSID', $bedrijfsid);

    $mapper->getOutput_object('balans')->setKey('BATCHCODE');
//---------------------------------------
    return $mapper->process();
}
