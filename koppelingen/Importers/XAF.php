<?php

namespace Importer;

require_once('XAFTransactions.php');
require_once('XAFLedger.php');
require_once('XAFRelations.php');
require_once('XAFHeader.php');

class XAF
{
    protected $reader_parameters = array();

    public function explain()
    {
        return <<<EXPLAIN
A basic XAF reader in an attempt to make it work like any other reader module.
HOWEVER!!!!
This object wil return you objects to read underlying sections.				
				
\$this->connect( array() )
Connects the input
	
The array must contain the following keys and values:
 - path..................A valid path.
 - filename..............Must be a valid path concatenated behind path.

NOTE!: Een XAF-file always has headers!
				
\$this->getHeader();
Returns an XAF header reader object.
									
\$this->getLedger();
Returns an XAF ledger reader object.
	
\$this->getTransactions()
Returns an XAF transactions reader object.
	
\$this->getRelations();
Returns an XAF relations reader object.

NOTE: The class cleans up after itself. 
EXPLAIN;
    }

    public function connect($source_parameters)
    {
        if (array_key_exists('path', $source_parameters)) {
            $this->reader_parameters['path'] = $source_parameters['path'];
        }
        if (array_key_exists('filename', $source_parameters)) {
            $this->reader_parameters['filename'] = $source_parameters['filename'];
        }
        if (file_exists($this->reader_parameters['path'] . $this->reader_parameters['filename'])) {
            return true;
        }
    }

    public function getHeader()
    {
        $header = new XAFHeader();
        $header->connect(array(
            'path' => $this->reader_parameters['path'],
            'filename' => $this->reader_parameters['filename']
        ));
        return $header;
    }

    public function getLedger()
    {
        $ledger = new XAFLedger();
        $ledger->connect(array(
            'path' => $this->reader_parameters['path'],
            'filename' => $this->reader_parameters['filename']
        ));
        return $ledger;
    }

    public function getRelations()
    {
        $relations = new XAFRelations();
        $relations->connect(array(
            'path' => $this->reader_parameters['path'],
            'filename' => $this->reader_parameters['filename']
        ));
        return $relations;
    }

    public function getTransactions()
    {
        $transactions = new XAFTransactions();
        $transactions->connect(array(
            'path' => $this->reader_parameters['path'],
            'filename' => $this->reader_parameters['filename']
        ));
        return $transactions;
    }
}