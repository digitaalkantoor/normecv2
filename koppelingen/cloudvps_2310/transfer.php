<?php
/**
 * Transfer alle bestanden in de database naar cloudVPS.
 */
class Transfer
{
    private $cloud;

    public function __construct()
    {
        require_once 'cloudvps.php';
        $this->cloud = new CloudVPS();
    }

    /**
     * Haalt alle bestanden van een database over naar cloudvps.
     *
     * @param string $host     ip/domein van database
     * @param string $user     database gebruiker
     * @param string $password wachtwoord database
     * @param string $database database naam
     *
     * @throws Exception als er iets fout gaat gooit exception
     */
    public function transferBedrijf($conn)
    {
        if ($resultBedrijf = $conn->query('SELECT * FROM `bedrijf`')) {
            while ($bedrijfsid = $resultBedrijf->fetch_assoc()) {
                if ($result = $conn->query('SELECT * FROM `documentbeheer` WHERE NOT BESTANDCONTENT is null AND `BEDRIJFSID` = '.(int) $bedrijfsid['ID'].' AND CLOUDVPS IS NULL;')) {
                  // zet de container naar het ingegeven bedrijf, database is leeg dus word uit de sessie gehaald
                    while ($row = $result->fetch_assoc()) {
                        $this->cloud->setCustomConfig((int) $bedrijfsid['ID'], $database);
                        // upload het bestand
                        $cloudid = $this->cloud->setFile($row['BESTANDCONTENT'], $row['BESTANDTYPE'], $row['BESTAND']);
                        if($cloudid > 0 && $row['ID'] > 0) {
                          // update de CloudVPS referentie voor het bestand
                          $sql_update = "UPDATE `documentbeheer` SET `CLOUDVPS` = '".ps($cloudid,"nr")."' WHERE `ID` = '".ps($row['ID'], "nr")."';";
                          //echo "<br/>".$sql_update;
                          $conn->query($sql_update);
                        }
                    }
                } else {
                    echo 'Geen archief bestanden gevonden';
                }
                
                if ($result = $conn->query('SELECT * FROM `document_facturen_hitorie` WHERE NOT BESTANDCONTENT is null AND `BEDRIJFSID` = '.(int) $bedrijfsid['ID'].' AND CLOUDVPS IS NULL;')) {
                  // zet de container naar het ingegeven bedrijf, database is leeg dus word uit de sessie gehaald
                    while ($row = $result->fetch_assoc()) {
                        $this->cloud->setCustomConfig((int) $bedrijfsid['ID'], $database);
                        // upload het bestand
                        $cloudid = $this->cloud->setFile($row['BESTANDCONTENT'], $row['BESTANDTYPE'], $row['BESTANDSNAAM']);
                        if($cloudid > 0 && $row['ID'] > 0) {
                          // update de CloudVPS referentie voor het bestand
                          $sql_update = "UPDATE `document_facturen_hitorie` SET `CLOUDVPS` = '".ps($cloudid,"nr")."' WHERE `ID` = '".ps($row['ID'], "nr")."';";
                          //echo "<br/>".$sql_update;
                          $conn->query($sql_update);
                        }
                    }
                } else {
                    echo 'Geen archief bestanden gevonden';
                }
            }
        } else {
            echo 'geen bedrijven gevonden';
        }
    }

    /**
     * Verwijder alle blobs uit de database.
     *
     * @param string $host     ip/domein van database
     * @param string $user     database gebruiker
     * @param string $password wachtwoord database
     * @param string $database database naam
     *
     * @throws Exception als er iets fout gaat gooit exception
     */
    public function removeBlobs($conn)
    {
        if ($resultBedrijf = $conn->query('SELECT * FROM `bedrijf`')) {
            while ($bedrijfsid = $resultBedrijf->fetch_assoc()) {
                if ($result = $conn->query('SELECT * FROM `documentbeheer` WHERE `BEDRIJFSID` = '.(int) $bedrijfsid['ID']
                  .' AND CLOUDVPS IS NOT NULL')) {
                    while ($row = $result->fetch_assoc()) {
                        $conn->query('UPDATE `documentbeheer` SET `BESTANDCONTENT` = NULL WHERE `ID` = '.$row['ID']);
                    }
                } else {
                    echo 'geen bestanden gevonden';
                }
            }
        } else {
            echo 'geen bedrijven gevonden';
        }
    }
}
