
function oft2OpenPopup()
{
    window.scrollTo(0, 0);
    $(".oft2_main_content").addClass("oft2_popup_open");
    $(".oft2_main_content_no_top").addClass("oft2_popup_open");
}

function oft2ClosePopup()
{
    $(".oft2_main_content").removeClass("oft2_popup_open");
    $(".oft2_main_content_no_top").removeClass("oft2_popup_open");
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return null;
}

function readChoices()
{
   $(".oft2_page_header .oft2_combo_box select, .oft2_page_header .oft2_combo_box input").each(function() {
      var cookieSaved = getCookie($(this).prop('name'));

      if(getParameterByName($(this).prop('name')) == '') {
        if(cookieSaved != null) {
    		$(this).val(cookieSaved);
            if ($(this) != null) {
                try {
                    $(this).change();
                }
                catch(err) {
                    if(console) {
                        console.log(err);
                    }
                }
            }
    	}
      }
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function toggleField() {
    if ($('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM"]').length > 0) {
        $('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM"]').change(function () {
            if ($('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM"]').val() == "Other") {
                $('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM_CUSTOM"]').show();
            } else {
                $('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM_CUSTOM"]').hide();
            }
        });

        if ($('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM"]').val() == "Other") {
            $('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM_CUSTOM"]').show();
        } else {
            $('[name="oft_entitie_accounting_CLIENTACCOUNTINGSYSTEM_CUSTOM"]').hide();
        }
    }
}

function rememberChoices()
{
    $(".oft2_page_header .oft2_combo_box select, .oft2_page_header .oft2_combo_box input").change(function () {
        var name = $(this).prop('name');
        var value = $(this).val();
        document.cookie = name + '=' + value;
    });
}

function setupSelectBox()
{
//     $('.oft2_page_header .oft2_combo_box select, .oft2_page_header .oft_tabel select').fancySelect({includeBlank: true}).on('change.fs', function () {
//         try {
//             $(this).trigger('change.$');
//         } catch (err) {
//             if (console) {
//                 console.log(err);
//             }
//         }
//     });
    $('.oft2_page_header .oft2_combo_box select, .oft2_page_header .oft_tabel select').select2().on('select2:open',function(){

        $('.select2-dropdown--above').attr('id','fix');
        $('#fix').removeClass('select2-dropdown--above');
        $('#fix').addClass('select2-dropdown--below');

    });;
}

// var inputValidator = function () {
//     $('input, textarea').change(function (event) {
//         $(this).val(cleanString($(this).val()));
//     });
//
//     $('input, textarea').keyup(function (event) {
//         $(this).val(cleanString($(this).val()));
//     });
//
//     function cleanString(stringToClean) {
//         return stringToClean.replace(/[!#\$%\^&*\(\)<>\{\}]/, '');
//     }
// }

$(document).ready(function () {

    $(".oft2_popup_exit").on('click', function () {
        oft2ClosePopup();
    });

    rememberChoices();
    readChoices();

    setupSelectBox();

    // inputValidator();

    toggleField();
});

function select_all(element, classname) {
    if ($(element).is(':checked')) {
        $('.' + classname).prop('checked', true);
    } else {
        $('.' + classname).prop('checked', false);
    }
}