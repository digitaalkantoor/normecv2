function toggle(theDiv) {
  if(document.getElementById(theDiv))
  {
    document.getElementById(theDiv).style.display = (document.getElementById(theDiv).style.display == "none")?"":"none";
  }
}

function validateAdres(veldnaam)
{
    if (document.getElementById(veldnaam).value == "")
    {
      document.getElementById('st_' + veldnaam).src = './images/validation_kruis.png';
    } else {
      document.getElementById('st_' + veldnaam).src = './images/validation_oke.png';      
    }
}
