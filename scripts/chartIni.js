statusChart = null;
jurisChart = null;


$(document).ready(function(){
    $.getScript('/scripts/Chart.min.js', function() {
        setupCharts();
    });
});

var setupCharts = function() {
    statusChart = new Chart($('#chartTarget').get(0).getContext("2d")).Pie(dataStates, getPieChartOptions());
    jurisChart = new Chart($('#chartJurisdiction').get(0).getContext("2d")).Pie(dataJuris, getPieChartOptions());


    $('#chartTarget').click(
        function(evt){
            var activePoints = statusChart.getSegmentsAtEvent(evt);
            var newLocation = '/content.php?SITE=oft_mena&SRCH_STATUS=x' + encodeURI(activePoints[0].label);
            window.location = newLocation;
        }
    );

    $('#chartJurisdiction').click(
        function(evt){
            var activePoints = jurisChart.getSegmentsAtEvent(evt);
            var newLocation = '/content.php?SITE=oft_mena&SRCH_JURISDICTION=x' + encodeURI(activePoints[0].label);
            window.location = newLocation;
        }
    );
};


var getPieChartOptions = function () {
  return {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke : true,

      //String - The colour of each segment stroke
      segmentStrokeColor : "#fff",

      //Number - The width of each segment stroke
      segmentStrokeWidth : 2,

      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout : 50, // This is 0 for Pie charts

      //Number - Amount of animation steps
      animationSteps : 100,

      //String - Animation easing effect
      animationEasing : "easeOutBounce",

      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate : true,

      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale : false,

      //String - A legend template
      legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

  };
};

var getBarChartOptions = function() {
    return {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero : true,

        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines : true,

        //String - Colour of the grid lines
        scaleGridLineColor : "rgba(0,0,0,.05)",

        //Number - Width of the grid lines
        scaleGridLineWidth : 1,

        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,

        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,

        //Boolean - If there is a stroke on each bar
        barShowStroke : true,

        //Number - Pixel width of the bar stroke
        barStrokeWidth : 2,

        //Number - Spacing between each of the X value sets
        barValueSpacing : 5,

        //Number - Spacing between data sets within X values
        barDatasetSpacing : 1,

        //String - A legend template
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

    };
};