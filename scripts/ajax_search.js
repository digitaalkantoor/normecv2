//Gets the browser specific XmlHttpRequest Object
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

//Our XmlHttpRequest object to get the auto suggest
var searchReq = getXmlHttpRequestObject();
var searchReq2 = getXmlHttpRequestObject();
var searchReq3 = getXmlHttpRequestObject();
var searchReq4 = getXmlHttpRequestObject();
var searchReq5 = getXmlHttpRequestObject();

//Called from keyup on the search textbox.
//Starts the AJAX request.
function searchSuggest(functie, veld) {
    setTimeout(function(){searchSuggest_timeout(functie, veld)}, 1000);
}

function searchSuggest_timeout(functie, veld) {
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById((veld + '_name')).value);
        searchReq.open("GET", 'content.php?SITE='+functie+'&VELD='+veld+'&LETTERS=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);

        //Show field
        document.getElementById(veld+'_search_suggest').style.display   = "";
    } else {
        setTimeout(function(){
          if(document.getElementById(veld+'_search_suggest').innerHTML == '' || document.getElementById(veld+'_search_suggest').innerHTML == '<img src="./images/wait16trans.gif">') {
            document.getElementById(veld+'_search_suggest').innerHTML = "Geen resultaat gevonden.";
          }
        }, 3000);
    }
}

//Called when the AJAX response is returned.
function handleSearchSuggest() {

    if (searchReq.readyState == 4) {
        var str              = searchReq.responseText.split("\n");
        var veld             = str[0].split("::");

        if(veld.length > 2)
        {
            veld                 = veld[2];
            var ss               = document.getElementById(veld + '_search_suggest');
            ss.innerHTML         = "<div class=\"ajaxBalkSluiten\"><span class=\"cursor\" onClick=\"javascript:document.getElementById('" + veld + "_search_suggest').style.display='none';\">Close&nbsp;&nbsp;</span></div>";
            var delen            = "";
            var naam             = "";
            var id               = "";

            for(i = 0; i < str.length - 1; i++) {
                //Build our element string.  This is cleaner using the DOM, but
                //IE doesn't support dynamically added attributes.
                delen            = str[i].split("::");
                naam             = delen[0];
                id               = delen[1];
                veld             = delen[2];
                var suggest      = '<div id="cell'+veld+ i +'" onmouseover="javascript:suggestOver(this);" ';
                suggest         += 'onmouseout="javascript:suggestOut(this);" ';
                suggest         += 'onclick="javascript:setSearch(\''+veld+'\', this.innerHTML, \''+id+'\');" waarde="'+ id +'"  veldnaam="'+ veld +'" ';
                suggest         += 'class="suggest_link">' + naam + '</div>';
                ss.innerHTML    += suggest;
            }
        }
    }
}

//Starts the AJAX request.
function searchSuggestHTML(functie, veld) {
    setTimeout(function(){searchSuggestHTML_timeout(functie, veld)}, 1000);
}

function searchSuggestHTML_timeout(functie, veld) {
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById((veld + '_name')).value);
        searchReq.open("GET", 'content.php?SITE='+functie+'&VELD='+veld+'&LETTERS=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggestHTML;
        searchReq.send(null);

        //Show field
        document.getElementById(veld+'_search_suggest').style.display   = "";
    }
}

//Called when the AJAX response is returned.
function handleSearchSuggestHTML() {

    if (searchReq.readyState == 4) {
        var str              = searchReq.responseText.split("\n");
        var veld             = str[0].split("::");

        if(veld.length > 2)
        {
            veld                 = veld[2];

            var ss               = document.getElementById(veld + '_search_suggest');
            //ss.innerHTML = "<table><tr><td>hoi</td></tr><tr><td>hoi</td></tr></table>";

            var string          = "<table class=\"tabel\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">";
            string             += "<tr><td colspan=\"10\"><div class=\"ajaxBalkSluiten\"><span class=\"cursor\" onClick=\"javascript:document.getElementById('" + veld + "_search_suggest').style.display='none';\">Close&nbsp;&nbsp;</span></div></td></tr>\n";
            string             += searchReq.responseText.replace("::0::"+veld, "");
            string             += "</table>";
            
            ss.innerHTML       = string;

        }
    }
}

function searchSuggestMulti(functie, veld, param) {
    setTimeout(function(){searchSuggestMulti_timeout(functie, veld, param)}, 1000);
}
function searchSuggestMulti_timeout(functie, veld, param) {
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById((veld + '_name')).value);
        searchReq.open("GET", 'content.php?SITE='+functie+'&VELD='+veld+'&LETTERS=' + str + '&param='+param, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);

        //Show field
        document.getElementById(veld+'_search_suggest').style.display   = "";
    }
}

function searchresulttonen(functie, veld, id, ToonAdministratie, BID) {
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById((veld + '_name')).value);
        searchReq.open("GET", 'content.php?SITE='+functie+'&ID='+id+'&VELD='+veld+'&TOONADMIN='+ToonAdministratie+BID+'&LETTERS=' + escape(str), true);
        searchReq.onreadystatechange = handleSearchresulttonen;
        searchReq.send(null);
        
        //Show field
        document.getElementById(veld+'_search_suggest').style.display   = "";
    }
}
//Called when the AJAX response is returned.
function handleSearchresulttonen() {

    if (searchReq.readyState == 4) {
        var str              = searchReq.responseText.split("::::");
        var boxnaam          = str[1]+'_search_suggest';
        document.getElementById(boxnaam).innerHTML = "";
        for(i=2; i < str.length; i++) {
          if(document.getElementById(boxnaam) && str[i] != null) {
               document.getElementById(boxnaam).innerHTML = str[i];
          }
        }
    }
}

//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';

    if(document.getElementById('hidden_'+div_value.veldnaam))
    {
      document.getElementById('hidden_'+div_value.veldnaam).innerHTML = div_value.id.replace('cell', '').replace(div_value.veldnaam, '');
    }
}

//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}

//Click function
function setSearch(veld, value, id) {
    if(document.getElementById(veld))
    {
        document.getElementById(veld+'').value                          = id;
        if(veld == "Aan")
        {
          value = value.replace(/ /gi, "");
          value = value.replace("\t", "");
        }
        if(document.getElementById(veld+'_name')) {
           document.getElementById(veld+'_name').value                     = stripHTML(value);
        }   
        if(document.getElementById(veld+'_search_suggest')) { 
          document.getElementById(veld+'_search_suggest').style.display   = "none"; //hide field
        }

        //Check op het bestaand van het veld voor taken.
        if(document.getElementById(veld+'_taak'))
        {
           //alert("update");
           getPageContent('content.php?SITE=generateTakenLijst&OFFERTENR='+id, veld+'_taak');
        } else if(document.getElementById('POSTENVASTID_taak'))
        {
           //alert("update1: " + id);
           getPageContent('content.php?SITE=generateTakenLijst&OFFERTENR='+id, 'POSTENVASTID_taak');
        } else if(document.getElementById(veld+'_mtaak'))
        {
           //alert("update1: " + id);
           getPageContent('content.php?SITE=generateTakenLijstMulti&OFFERTENR='+id + '&veld=' + veld, veld+'_mtaak');
        }
    }
} 

function stripHTML(oldString) {

   var newString = "";
   var inTag = false;
   for(var i = 0; i < oldString.length; i++) {
   
        if(oldString.charAt(i) == '<') inTag = true;
        if(oldString.charAt(i) == '>') {
              if(oldString.charAt(i+1)=="<")
              {
              		//dont do anything
              } else {
          		inTag = false;
          		i++;
              }
        }
   
        if(!inTag) newString += oldString.charAt(i);
   }

   return newString;
}

/********************************** Standaard functies **************************************/
//Initiate the asyncronous request.
function getPageContent(site, div) {
//If our XmlHttpRequest object is not in the middle of a request, start the new asyncronous call.
        if (searchReq.readyState == 4 || searchReq.readyState == 0) {
	//Setup the connection as a GET call to SayHello.html.
		//True explicity sets the request to asyncronous (default).
		searchReq.open("GET", site, true);
		//Set the function that will be called when the XmlHttpRequest objects state changes.
		//searchReq.onreadystatechange = schrijfContentUit;
		searchReq.onreadystatechange=function() {
                  if(searchReq.readyState == 4) {
                    //alert("div: " + div);
                    //alert(searchReq.responseText);
                    document.getElementById(div).innerHTML = searchReq.responseText;
                  }
                }

        	//Make the actual request.
   		searchReq.send(null);
	}
}

//Initiate the asyncronous request.
function setPageContent(site, div) {
//If our XmlHttpRequest object is not in the middle of a request, start the new asyncronous call.
        if (searchReq.readyState == 4 || searchReq.readyState == 0) {
	//Setup the connection as a GET call to SayHello.html.
		//True explicity sets the request to asyncronous (default).
		searchReq.open("GET", site, true);
		//Set the function that will be called when the XmlHttpRequest objects state changes.
		//searchReq.onreadystatechange = schrijfContentUit;
		searchReq.onreadystatechange=function() {
                  if(searchReq.readyState == 4) {
                    //alert("div: " + div);
                    //alert(searchReq.responseText);
                    document.getElementById(div).value = searchReq.responseText;
                  }
                }

        	//Make the actual request.
   		searchReq.send(null);
	}
}

function getPageContentWindowTop(site, div) {
//If our XmlHttpRequest object is not in the middle of a request, start the new asyncronous call.
        if (searchReq.readyState == 4 || searchReq.readyState == 0) {
	//Setup the connection as a GET call to SayHello.html.
		//True explicity sets the request to asyncronous (default).
		searchReq.open("GET", site, true);
		//Set the function that will be called when the XmlHttpRequest objects state changes.
		//searchReq.onreadystatechange = schrijfContentUit;
		searchReq.onreadystatechange=function() {
                  if(searchReq.readyState == 4) {
                    //alert("div: " + div);
                    window.top.document.getElementById(div).innerHTML = searchReq.responseText;
                  }
                }

        	//Make the actual request.
   		searchReq.send(null);
	}
}

function getAddPageContent(site, div) {
//If our XmlHttpRequest object is not in the middle of a request, start the new asyncronous call.
        if (searchReq.readyState == 4 || searchReq.readyState == 0) {
	//Setup the connection as a GET call to SayHello.html.
		//True explicity sets the request to asyncronous (default).
		searchReq.open("GET", site, true);
		//Set the function that will be called when the XmlHttpRequest objects state changes.
		//searchReq.onreadystatechange = schrijfContentUit;
		searchReq.onreadystatechange=function() {
                  if(searchReq.readyState == 4) {
                    document.getElementById(div).innerHTML = document.getElementById(div).innerHTML + searchReq.responseText;
                  }
                }

        	//Make the actual request.
   		searchReq.send(null);
	}
}

function getPageContent2(site, div, object) {
        //If our XmlHttpRequest object is not in the middle of a request, start the new asyncronous call.
        if (object.readyState == 4 || object.readyState == 0) {
	//Setup the connection as a GET call to SayHello.html.
		//True explicity sets the request to asyncronous (default).
		object.open("GET", site, true);
		//Set the function that will be called when the XmlHttpRequest objects state changes.
		//searchReq.onreadystatechange = schrijfContentUit;
		object.onreadystatechange=function() {
                  if(object.readyState == 4) {
                    document.getElementById(div).innerHTML = object.responseText;
                    scripts = document.getElementById(div).getElementsByTagName('script');
                    for (index = 0; index < scripts.length; index++) {
                      var codestring = scripts[index].textContent;
                      codestring = codestring.replace("<script>", "").replace("</script>", "");
                      jQuery.globalEval(codestring);
                    }
                  }
                }

        	//Make the actual request.
   		object.send(null);
	}
}

function fixMultiSelect(value) {
    console.log(value);
    if(!value || (typeof value == "object" && value.length < 2) || typeof value == "string") {
        return encodeURIComponent(value);
    }
    return encodeURIComponent(value.join('|||'));
}

function openStatistieken(offerteid, rechten)
{
  //getPageContent2('content.php?SITE=generatePlanningtijd&OFFERTEID=' + offerteid + "&RECHTEN=" + rechten, 'generatePlanningtijd', searchReq);
  //getPageContent2('content.php?SITE=generateDeadlines&OFFERTEID=' + offerteid + "&RECHTEN=" + rechten, 'generateDeadlines', searchReq2);
  //getPageContent2('content.php?SITE=generatePlanningtijdPerPersoon&OFFERTEID=' + offerteid + "&RECHTEN=" + rechten, 'generatePlanningtijdPerPersoon', searchReq3);
  getPageContent2('content.php?SITE=generateProjectOverzichtStat', 'generateProjectOverzichtStat', searchReq4);
}

function openSubDiv()
{
  //if(document.getElementById('rssLezerKort'))  { getPageContent2('content.php?SITE=generaterssLezerKort', 'rssLezerKort', searchReq3); }
  //if(document.getElementById('checkForRenewProducten')) { getPageContent2('content.php?SITE=generatercheckForRenewProducten', 'checkForRenewProducten', searchReq2); }
}

function projectTabBlad(url, id, projectId)
{
  document.getElementById('generatePlanningtijd'+projectId).style.display='none';
  document.getElementById('generatePlanningtijdPerPersoon'+projectId).style.display='none';
  document.getElementById('generateDeadlines'+projectId).style.display='none';
  document.getElementById('generateTickets'+projectId).style.display='none';
  document.getElementById('generateVoortgang'+projectId).style.display='none';
  document.getElementById(id).style.display='';
  getPageContent(url, id);
}

function selectWithKeyDown(veld, waarde, functie, basistekst, evt)
{
   if(waarde.value.length > 0) {
       if(document.getElementById('hidden_'+veld))
       {
          document.getElementById(veld+'_search_suggest').style.display='';

          var e = evt ? evt  : window.event;
          if(!e) return;
          var key = 0;
          if (e.keyCode) { key = e.keyCode; } // for moz/fb, if keyCode==0 use 'which'
          else if (typeof(e.which)!= 'undefined') { key = e.which; }
          //alert(""+key);

          switch (key) {
                  case 9: // tab
                     document.getElementById(veld+'_search_suggest').style.display='none';
                  case 27: // Escape
                     document.getElementById(veld+'_search_suggest').style.display='none';
                  case 13: // enter
                     var divnaam = 'cell' + veld + document.getElementById('hidden_'+veld).innerHTML;
                     if(document.getElementById(divnaam).getAttribute('waarde'))
                     {
                         setSearch(veld, document.getElementById(divnaam).innerHTML, document.getElementById(divnaam).getAttribute('waarde'));
                         document.getElementById(veld+'_search_suggest').style.display='none';
                     }
                     e.preventDefault ? e.preventDefault() : e.returnValue = false;  //Dont submit with enter
                     break;
                  case 38: // naar boven
                     document.getElementById(veld + '_search_suggest').scrollTop -= 10;
                     var divnaamvorige = 'cell' + veld + document.getElementById('hidden_'+veld).innerHTML;
                     if(document.getElementById('hidden_'+veld).innerHTML > 0) { document.getElementById('hidden_'+veld).innerHTML = (document.getElementById('hidden_'+veld).innerHTML*1)-1; }
                     var divnaam = 'cell' + veld + document.getElementById('hidden_'+veld).innerHTML;
                     break;
                  case 40: // naar beneden
                     document.getElementById(veld + '_search_suggest').scrollTop += 10;
                     var divnaamvorige = 'cell' + veld + document.getElementById('hidden_'+veld).innerHTML;
                     document.getElementById('hidden_'+veld).innerHTML = (document.getElementById('hidden_'+veld).innerHTML*1)+1;
                     var divnaam = 'cell' + veld + document.getElementById('hidden_'+veld).innerHTML;
                     break;
                  default:
                     document.getElementById(veld+'_search_suggest').innerHTML='<img src=./images/wait16trans.gif />';
                     if(functie == 'genCrediteurenLijst') {
                       searchSuggestHTML(functie, veld);
                     } else {
                       searchSuggest(functie, veld);
                     }
                     break;
          }
          if(document.getElementById(divnaamvorige)) { suggestOut(document.getElementById(divnaamvorige)); }
          if(document.getElementById(divnaam))       { suggestOver(document.getElementById(divnaam));      }

          //Page down, dan 10 naar beneden
       }
   } else {
      document.getElementById(veld+'_search_suggest').style.display='none';
   }
}