window.onbeforeunload = function (e) {
    var e = e || window.event;

    if (document.getElementById('leave_page')) {
        if (document.getElementById('leave_page').innerHTML == '1') {
            //IE & Firefox
            if (e) {
                e.returnValue = 'U gaat deze pagina verlaten. Weet u het zeker?';
            }

            // For Safari
            return 'U gaat deze pagina verlaten. Weet u het zeker?';
        }
    }
};

// Zorgt ervoor dat de juiste menuknop lichtblauw wordt.
// Is op het moment gekoppeld aan het kruimelpad in het iFrame
function setHighLight() {
    loc = window.MAIN.location.search;
    if (window.MAIN.document.getElementById('breadcrumbs')) {
        if (document.getElementById('Documenten')) {
            document.getElementById('Documenten').style.backgroundColor = '';
        }
        if (document.getElementById('Boekhouding')) {
            document.getElementById('Boekhouding').style.backgroundColor = '';
        }
        if (document.getElementById('Handel')) {
            document.getElementById('Handel').style.backgroundColor = '';
        }
        if (document.getElementById('Kennisbank')) {
            document.getElementById('Kennisbank').style.backgroundColor = '';
        }
        if (document.getElementById('Personeel')) {
            document.getElementById('Personeel').style.backgroundColor = '';
        }
        if (document.getElementById('Project en uren')) {
            document.getElementById('Project en uren').style.backgroundColor = '';
        }
        if (document.getElementById('Relatiebeheer')) {
            document.getElementById('Relatiebeheer').style.backgroundColor = '';
        }

        if (window.MAIN.window.document.getElementById('breadcrumbs').innerHTML == 'Documenten') {
            document.getElementById('Documenten').style.backgroundColor = '#316ac5';
        } else if (window.MAIN.window.document.getElementById('breadcrumbs').innerHTML == 'Boekhouding') {
            document.getElementById('Boekhouding').style.backgroundColor = '#316ac5';
        } else if (window.MAIN.window.document.getElementById('breadcrumbs').innerHTML == 'Handel') {
            document.getElementById('Handel').style.backgroundColor = '#316ac5';
        } else if (window.MAIN.window.document.getElementById('breadcrumbs').innerHTML == 'Kennisbank') {
            document.getElementById('Kennisbank').style.backgroundColor = '#316ac5';
        } else if (window.MAIN.window.document.getElementById('breadcrumbs').innerHTML == 'Personeel') {
            document.getElementById('Personeel').style.backgroundColor = '#316ac5';
        } else if (window.MAIN.window.document.getElementById('breadcrumbs').innerHTML == 'Projecten') {
            document.getElementById('Project en uren').style.backgroundColor = '#316ac5';
        } else if (window.MAIN.window.document.getElementById('breadcrumbs').innerHTML == 'Relaties') {
            document.getElementById('Relatiebeheer').style.backgroundColor = '#316ac5';
        }
    }
}

function gaNaarLink(link) {
    if (document.getElementById("scherm1") && document.getElementById("scherm1").style.display == "") {
        window.MAIN.location.replace(link);
    }
    if (document.getElementById("scherm2") && document.getElementById("scherm2").style.display == "") {
        window.MAIN2.location.replace(link);
    }
    if (document.getElementById("scherm3") && document.getElementById("scherm3").style.display == "") {
        window.MAIN3.location.replace(link);
    }
    if (document.getElementById("scherm4") && document.getElementById("scherm4").style.display == "") {
        window.MAIN4.location.replace(link);
    }
}

function gone(theForm) {
    var ganaar = document.getElementById('ganaarform').value;
    if (document.getElementById("scherm1") && document.getElementById("scherm1").style.display == "") {
        window.MAIN.location.replace(ganaar);
    }
    if (document.getElementById("scherm2") && document.getElementById("scherm2").style.display == "") {
        window.MAIN2.location.replace(ganaar);
    }
    if (document.getElementById("scherm3") && document.getElementById("scherm3").style.display == "") {
        window.MAIN3.location.replace(ganaar);
    }
    if (document.getElementById("scherm4") && document.getElementById("scherm4").style.display == "") {
        window.MAIN4.location.replace(ganaar);
    }
}

function gone2(theForm) {
    var ganaar = document.getElementById('ganaarform2').value;
    window.location.replace(ganaar);
}

function gonePlanning(theForm) {
    var ganaarPlanning = document.getElementById('ganaarPlanning');
    var ganaar = ganaarPlanning.GANAAR.value + '&MAAND=' + ganaarPlanning.MAAND.value + '&JAAR=' + ganaarPlanning.JAAR.value;
    window.location.replace(ganaar);
}

function setValueInSearchField(fieldNaam, waarde1, waarde2, nrOfField) {
    for (var i = 0; i <= nrOfField; i++) {
        if (document.getElementById(fieldNaam + i)) {
            document.getElementById(fieldNaam + i).value = waarde1;
            document.getElementById(fieldNaam + i + '_name').value = waarde2;
        }
    }
}

function checkUncheckSome(controller, theElements) {

    var formElements = theElements.split(',');
    var theController = document.getElementById(controller);
    for (var z = 0; z < formElements.length; z++) {
        theItem = document.getElementById(formElements[z]);
        //alert('1');
        if (theItem) {
            //alert(theItem.type + " - " + theItem.name);
            if (theItem.type) {
                //alert('3');
                if (theItem.type == 'checkbox') {
                    //alert('4');
                    if (theItem.id != theController.id) {
                        //alert('5');
                        theItem.checked = theController.checked;
                    }
                }
            } else {
                var nextArray = '';
                for (var x = 0; x < theItem.childNodes.length; x++) {
                    if (theItem.childNodes[x]) {
                        if (theItem.childNodes[x].id) {
                            nextArray += theItem.childNodes[x].id + ',';
                        }
                    }
                }
                checkUncheckSome(controller, nextArray);
            }
        }
    }
}

function checkCheckboxen(controller, theElements) {

    var formElements = theElements.split(',');
    var theController = document.getElementById(controller);
    for (var z = 0; z < formElements.length; z++) {
        theItem = document.getElementById(formElements[z]);
        if (theItem) {
            if (theItem.type) {
                theItem.checked = true;
            } else {

                var nextArray = '';
                for (var x = 0; x < theItem.childNodes.length; x++) {
                    if (theItem.childNodes[x]) {
                        if (theItem.childNodes[x].id) {
                            nextArray += theItem.childNodes[x].id + ',';
                        }
                    }
                }
                checkUncheckSome(controller, nextArray);

            }
        }
    }
}

//Function print window
function printWindow() {
    bV = parseInt(navigator.appVersion)
    if (bV >= 4) window.print()
}

function printen() {
    //generateSchermURL
    searchReq.open("GET", 'content.php?SITE=generateSchermURL', true);
    searchReq.onreadystatechange = handleOpenPrintWindow;
    searchReq.send(null);
}

function handleOpenPrintWindow() {

    if (searchReq.readyState == 4) {
        var str = searchReq.responseText;
        window.open(str + '&amp;PRINTEN=true&amp;POESPAS=false');
    }
}

function changePage(newLoc, url) {
    nextPage = newLoc.options[newLoc.selectedIndex].value
    if (nextPage != "") {
        document.location.href = url + nextPage
    }
}

function toggle(id) {
    if (elem = document.getElementById(id)) {
        elem.style.display = (elem.style.display == "none") ? "" : "none";
    }
}

function tonen(id, type) {
    if (type && (type == 'class') && (elem = document.getElementsByClassName(id)[0])
        || (elem = document.getElementById(id))) {
        elem.style.display = 'inherit';
    }
}

function verbergen(id, type) {
    if (type && (type == 'class') && (elem = document.getElementsByClassName(id)[0])
        || (elem = document.getElementById(id))) {
        elem.style.display = 'none';
    }
}

function legen(theDiv) {
    if (document.getElementById(theDiv)) {
        document.getElementById(theDiv).value = '';
    }
    if (document.getElementById(theDiv + '_name')) {
        document.getElementById(theDiv + '_name').value = '';
    }
}

function sluiten(theDiv) {
    if (document.getElementById(theDiv)) {
        var elem = document.getElementById(theDiv);
        elem.style.display = 'none';
        if (theDiv == 'invoerscherm' && document.getElementById('invoerschermContent')) {
            document.getElementById('invoerschermContent').innerHTML = '';
        }
    }
}

function toggleArrow(theDiv, thePlus) {
    if (document.getElementById(theDiv)) {
        if (document.getElementById(theDiv).style.display == '') {
            document.getElementById(theDiv).style.display = 'none';
            document.getElementById(thePlus).src = './images/oft/pijlright.jpg';
        } else {
            document.getElementById(thePlus).src = './images/oft/pijldown.jpg';
            document.getElementById(theDiv).style.display = '';
        }
    }
}

function togglePlus(theDiv, thePlus) {
    if (document.getElementById(theDiv)) {
        if (document.getElementById(theDiv).style.display == '') {
            document.getElementById(theDiv).style.display = 'none';
            document.getElementById(thePlus).src = './images/plus.gif';
        } else {
            document.getElementById(thePlus).src = './images/min.gif';
            document.getElementById(theDiv).style.display = '';
        }
    }
}

function expandDiscussion(sec) {
    thisSec = eval(sec);
    //for (var i=0;i<thisSec.length;i++) {thisSec[i].style.display = "inline"}
    thisSec.style.display = (thisSec.style.display == "none") ? "" : "none";
}

function expandCollaps(sec) {
    thisSec = eval(sec);
    //for (var i=0;i<thisSec.length;i++) {thisSec[i].style.display = "inline"}
    thisSec.style.display = (thisSec.style.display == "none") ? "" : "none";
    //document.layers[thisSec].visibility=\"show\";
}

function sitemap() {
    var theDiv = "sitemap";
    var elem = document.getElementById(theDiv);
    elem.style.display = (elem.style.display == "none") ? "" : "none";
}

function checkUncheckAll(theElement) {
    var theForm = theElement.form, z = 0;
    for (z = 0; z < theForm.length; z++) {
        if (theForm[z].type == 'checkbox' && theForm[z].name != 'checkall') {
            theForm[z].checked = theElement.checked;
        }
    }
}

function validateDashBoardUren(theForm, evt) {
    if (theForm.OFFERTENR.value == "") {
        alert("Geen project opgegeven.");
        //theForm.OFFERTENR.focus();
        return (false);
    } else if (theForm.POSTENVASTID.value == "") {
        alert("Geen taak opgegeven.");
        //theForm.POSTENVASTID.focus();
        return (false);
    }

    return (true);
}

function x_days_after(s, x) {
    // parse s for month, day, year
    var dateArray = s.split('-');

    sdate = new Date(dateArray[0], (dateArray[1] * 1), (dateArray[2] * 1));

    // figure out day, month, year 7 days after
    var odate = new Date(sdate.getTime() + (x * 86400000));

    var maand = odate.getMonth() + "";
    if (maand.length == 1) {
        maand = "0" + maand;
    }

    var dag = odate.getDate() + "";
    if (dag.length == 1) {
        dag = "0" + dag;
    }

    var jaar = odate.getYear();

    if (navigator.userAgent.indexOf('MSIE') >= 0 && navigator.userAgent.indexOf('Opera') < 0) {
    } else {
        jaar = (jaar + 1900);
    }
    //if(navigator.userAgent.indexOf('Opera')>=0) jaar = (jaar+1900);

    // return value
    return jaar + '-' + maand + '-' + dag;
}

function schermTabbladVerwissel(scherm) {
    document.getElementById("scherm1woord").style.backgroundImage = "url('./images/tabblad2.jpg')";
    document.getElementById("scherm2woord").style.backgroundImage = "url('./images/tabblad2.jpg')";
    document.getElementById("scherm3woord").style.backgroundImage = "url('./images/tabblad2.jpg')";
    document.getElementById("scherm4woord").style.backgroundImage = "url('./images/tabblad2.jpg')";

    document.getElementById("scherm" + scherm + "woord").style.backgroundImage = "url('./images/tabblad2active.jpg')";
}

function scherm1(linkurl) {
    document.getElementById("scherm1").style.display = "";
    document.getElementById("scherm2").style.display = "none";
    document.getElementById("scherm3").style.display = "none";
    document.getElementById("scherm4").style.display = "none";

    document.getElementById("scherm1").style.height = "100%";
    document.getElementById("scherm2").style.height = "0px";
    document.getElementById("scherm3").style.height = "0px";
    document.getElementById("scherm4").style.height = "0px";

    document.getElementById("scherm1woord").style.fontWeight = "Bolder";
    document.getElementById("scherm1woord").style.color = "#ff5800";
    document.getElementById("scherm2woord").style.fontWeight = "normal";
    document.getElementById("scherm2woord").style.color = "#333333";
    document.getElementById("scherm3woord").style.fontWeight = "normal";
    document.getElementById("scherm3woord").style.color = "#333333";
    document.getElementById("scherm4woord").style.fontWeight = "normal";
    document.getElementById("scherm4woord").style.color = "#333333";

    getPageContent2('content.php?SITE=setScherm&SCHERM=1', 'setScherm', searchReq3);
}

function scherm2(linkurl) {
    document.getElementById("scherm1").style.display = "none";
    document.getElementById("scherm2").style.display = "";
    document.getElementById("scherm3").style.display = "none";
    document.getElementById("scherm4").style.display = "none";

    document.getElementById("scherm1").style.height = "0px";
    document.getElementById("scherm2").style.height = "100%";
    document.getElementById("scherm3").style.height = "0px";
    document.getElementById("scherm4").style.height = "0px";

    document.getElementById("scherm1woord").style.fontWeight = "normal";
    document.getElementById("scherm1woord").style.color = "#333333";
    document.getElementById("scherm2woord").style.fontWeight = "Bolder";
    document.getElementById("scherm2woord").style.color = "#ff5800";
    document.getElementById("scherm3woord").style.fontWeight = "normal";
    document.getElementById("scherm3woord").style.color = "#333333";
    document.getElementById("scherm4woord").style.fontWeight = "normal";
    document.getElementById("scherm4woord").style.color = "#333333";

    getPageContent2('content.php?SITE=setScherm&SCHERM=2', 'setScherm', searchReq3);

    var x = document.getElementById("MAIN2").src.substring(document.getElementById("MAIN2").src.length - 10, document.getElementById("MAIN2").src.length);
    if (document.getElementById("MAIN2").src == "" || x == "leegScherm") {
        document.getElementById("MAIN2").src = linkurl + '&SCHERM=2';
    }
}

function scherm3(linkurl) {
    document.getElementById("scherm1").style.display = "none";
    document.getElementById("scherm2").style.display = "none";
    document.getElementById("scherm3").style.display = "";
    document.getElementById("scherm4").style.display = "none";

    document.getElementById("scherm1").style.height = "0px";
    document.getElementById("scherm2").style.height = "0px";
    document.getElementById("scherm3").style.height = "100%";
    document.getElementById("scherm4").style.height = "0px";

    document.getElementById("scherm1woord").style.fontWeight = "normal";
    document.getElementById("scherm1woord").style.color = "#333333";
    document.getElementById("scherm2woord").style.fontWeight = "normal";
    document.getElementById("scherm2woord").style.color = "#333333";
    document.getElementById("scherm3woord").style.fontWeight = "Bolder";
    document.getElementById("scherm3woord").style.color = "#ff5800";
    document.getElementById("scherm4woord").style.fontWeight = "normal";
    document.getElementById("scherm4woord").style.color = "#333333";

    getPageContent2('content.php?SITE=setScherm&SCHERM=3', 'setScherm', searchReq3);

    var x = document.getElementById("MAIN3").src.substring(document.getElementById("MAIN3").src.length - 10, document.getElementById("MAIN3").src.length);
    if (document.getElementById("MAIN3").src == "" || x == "leegScherm") {
        document.getElementById("MAIN3").src = linkurl + '&SCHERM=3';
    }
}

function scherm4(linkurl) {
    document.getElementById("scherm1").style.display = "none";
    document.getElementById("scherm2").style.display = "none";
    document.getElementById("scherm3").style.display = "none";
    document.getElementById("scherm4").style.display = "";

    document.getElementById("scherm1").style.height = "0px";
    document.getElementById("scherm2").style.height = "0px";
    document.getElementById("scherm3").style.height = "0px";
    document.getElementById("scherm4").style.height = "100%";

    document.getElementById("scherm1woord").style.fontWeight = "normal";
    document.getElementById("scherm1woord").style.color = "#333333";
    document.getElementById("scherm2woord").style.fontWeight = "normal";
    document.getElementById("scherm2woord").style.color = "#333333";
    document.getElementById("scherm3woord").style.fontWeight = "normal";
    document.getElementById("scherm3woord").style.color = "#333333";
    document.getElementById("scherm4woord").style.fontWeight = "Bolder";
    document.getElementById("scherm4woord").style.color = "#ff5800";

    getPageContent2('content.php?SITE=setScherm&SCHERM=4', 'setScherm', searchReq3);

    var x = document.getElementById("MAIN4").src.substring(document.getElementById("MAIN4").src.length - 10, document.getElementById("MAIN4").src.length);
    if (document.getElementById("MAIN4").src == "" || x == "leegScherm") {
        document.getElementById("MAIN4").src = linkurl + '&SCHERM=4';
    }
}

function checkBTW(veldteller) {
    btw = document.getElementById('BTW' + veldteller).value.replace(",", ".");
    btw = btw / 100;
    bedrag = document.getElementById('EURO' + veldteller).value.replace(",", ".");
    btwbedrag = Math.floor((bedrag * btw) * 100);
    btwbedrag = btwbedrag / 100;
    document.getElementById('check' + veldteller).innerHTML = "&#128; " + btwbedrag;
}

function checkOfInboekenKloptMetTotaal(totaalBedragFactuurVeld, totaalBedragFactuurBerekening) {
    var totBedragBerekening = document.getElementById(totaalBedragFactuurBerekening).innerHTML;
    var totBedragVeld = document.getElementById(totaalBedragFactuurVeld).value;
    totBedragVeld = totBedragVeld.replace(",", ".");
    totBedragVeld = totBedragVeld.replace("&#128;", "");
    totBedragVeld = totBedragVeld.replace("&euro;", "");
    totBedragVeld = totBedragVeld.replace("�", "");
    totBedragVeld = totBedragVeld.replace(" ", "");
    totBedragVeld = parseFloat(totBedragVeld);

    totBedragBerekening = totBedragBerekening.replace(",", ".");
    totBedragBerekening = totBedragBerekening.replace("&#128;", "");
    totBedragBerekening = totBedragBerekening.replace("&euro;", "");
    totBedragBerekening = totBedragBerekening.replace("�", "");
    totBedragBerekening = totBedragBerekening.replace(" ", "");
    totBedragBerekening = parseFloat(totBedragBerekening);

    var verschil = (totBedragVeld * 1) - (totBedragBerekening * 1);

    if (verschil > 0.10 || verschil < -0.10) {
        alert("Totaalbedrag is nog niet compleet! Te boeken: " + verschil);
        return false;
    }
    return true;
}

function checkBTWInvoerType1(regels) {
    var totaalbedrag = 0;
    var totaalBTW = 0;

    for (i = 1; i <= regels; i++) {
        //Bereken BTW
        if (document.getElementById('BTW' + i).value == "Geen BTW" || document.getElementById('BTW' + i).value == "Verlegd" || document.getElementById('BTW' + i).value == "0") {
            btw = 0;
        } else {
            btw = document.getElementById('BTW' + i).value.replace(",", ".");
            btw = btw / 100;
        }
        bedrag = document.getElementById('BEDRAG' + i).value.replace(",", ".");
        btwbedrag = Math.round((bedrag * btw) * 100);
        btwbedrag = btwbedrag / 100;

        if ((bedrag * 1) != 0) {
            totaalbedrag = parseFloat(totaalbedrag) + parseFloat(bedrag);
            totaalBTW = parseFloat(totaalBTW) + parseFloat(btwbedrag);
        }
        document.getElementById('check' + i).innerHTML = btwbedrag;
    }
    totaalBTW = Math.round((totaalBTW * 100));
    totaalBTW = totaalBTW / 100;

    document.getElementById('totaalBedragFactuurBerekening').innerHTML = totaalbedrag;
    document.getElementById('totaalBTW').innerHTML = totaalBTW;
    document.getElementById('totaalBedragFactuur').innerHTML = (totaalbedrag + totaalBTW);
}

function checkImportData() {

    var nrTabel = document.getElementById('nrTabel').value;
    var result = true;

    // Check if any tabel dropdown lists have the same selection
    for (i = 0; i < nrTabel; i++) {
        for (j = 0; j < nrTabel; j++) {
            if (document.getElementById('tabel' + i).value == document.getElementById('tabel' + j).value && document.getElementById('tabel' + i).value != '') {
                if (i != j)
                    result = false;
            }


        }


    }

    if (result)
        return true;
    else {
        alert('Een of meerdere kolomnamen conflicteren met elkaar.');
        return false;
    }

}

function submitenter(myfield, e) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        myfield.form.submit();
        return false;
    }
    else {
        return true;
    }
}

function sumBeschikbareEuroProject(id) {
    var ACTIDS = document.getElementById('ACTIDS').value.split(",");
    var totaalTijd = 0;
    var totaalEuro = 0;
    for (i = 0; i < ACTIDS.length; i++) {
        id = ACTIDS[i];
        if (document.getElementById('UUR' + id)) {
            minutes = (document.getElementById('UUR' + id).value * 60) + (document.getElementById('MIN' + id).value * 1);
            totaalTijd += minutes;
            bedrag = (document.getElementById('POST' + id).value / 60) * minutes;
            bedrag = Math.round((bedrag * 100));
            bedrag = bedrag / 100;
            totaalEuro += bedrag;
            document.getElementById('beschikbareeuro' + id).innerHTML = '&#128;&nbsp;' + bedrag;
        }
    }
    document.getElementById('totaalbeschikbaretijd').innerHTML = Math.floor(totaalTijd / 60) + ':' + (totaalTijd % 60);
    document.getElementById('totaalbeschikbareeuro').innerHTML = '&#128;&nbsp;' + totaalEuro;

}

function formOpslaan(formToevoeging, TEMPLATEID, toId) {
//  deel1 = velden.split("@@@");
    var teller = 0;
    var content = '';

    while (teller <= 100 && document.getElementById('VELD' + teller)) {
        if (document.getElementById('VELD' + teller)) {
            content = content + '&VELD' + teller + '=' + document.getElementById('VELD' + teller).value + formToevoeging
        }
        teller++;
    }

    getPageContent2('content.php?SITE=saveForm&TEMPLATEID=' + TEMPLATEID + content, toId, searchReq3);
}

function setWindowHeight() {
    document.getElementById('wrapper').height = (getWindowHeight() - 150) + 'px';
    document.getElementById('iframes').height = (getWindowHeight() - 113) + 'px';
    document.body.style.position = "fixed";
    document.body.style.overflow = "hidden";

}

function getWindowHeight() {
    var windowHeight = 0;
    if (typeof(window.innerHeight) == 'number') {
        windowHeight = window.innerHeight;
    }
    else {
        if (document.documentElement && document.documentElement.clientHeight) {
            windowHeight = document.documentElement.clientHeight;
        }
        else {
            if (document.body && document.body.clientHeight) {
                windowHeight = document.body.clientHeight;
            }
        }
    }
    return windowHeight;
}

function calculate_amount_of_shares() {
    if (document.getElementById('calculate_amount_of_shares').innerText != undefined) {
        var total = parseFloat(document.getElementById('total_amount_of_shares').innerText);
        if (!total) {
            return false;
        }
        if (total > 0) {
            var percentage = parseFloat(document.getElementById('oft_shareholders_PERSENTAGE').value);
            var shares = Math.round((total / 100) * percentage);
            document.getElementById('oft_shareholders_QUANTITY').value = shares;
        }
    }
}

function calculate_percentage_of_shares() {
    if (document.getElementById('calculate_amount_of_shares').innerText != undefined) {
        var total = parseFloat(document.getElementById('total_amount_of_shares').innerText);
        if (!total) {
            return false;
        }
        if (total > 0) {
            var shares = parseFloat(document.getElementById('oft_shareholders_QUANTITY').value);
            var percentage = Math.round((100 / total) * shares);
            document.getElementById('oft_shareholders_PERSENTAGE').value = percentage;
        }
    }
}

function show_next_file(this_id, max) {
    if (document.getElementById(this_id)) {
        // show the element
        var target_id = this_id.replace("SHOW", "");
        document.getElementById(target_id).style.display = '';

        // set "add next" to next element
        var splitted = this_id.split("_");
        var nr = parseFloat(splitted[0].replace("SHOWID", ""));
        if (nr < max) {
            var new_id = "SHOWID" + (nr + 1) + "_" + splitted[1];
            document.getElementById(this_id).id = new_id;
        } else {
            document.getElementById(this_id).style.display = 'none';
        }
        return false;
    }
}

/*
* * Toggle all checkboxes at once.
*
* Add this as an onclick function to a checkbox (for example a checkbox in a thead).
* The value of the 'source' checkbox will be used to determine which checkboxes will be effected.
* The affected checkboxes will inherit the 'checked'-status from the 'source' checkbox.
*
*/
function toggleAllCheckboxes(source) {
    checkboxes = document.getElementsByClassName(source.value);
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

function ajaxSubmit(source, closePopup) {

    source.preventDefault();

    var $form = $(source.srcElement);
    var formURL = $form.attr("action");
    var formAction = $form.attr("method");
    var formData = new FormData(source.srcElement);
    var $btn = $(document.activeElement);

    if ($btn.length && $form.has($btn) && $btn.is('button[type="submit"], input[type="submit"], input[type="image"]') && $btn.is('[name]')) {
        formData.append($btn.attr('name'), $btn.val());
        formData.append('submit', $btn.attr('name'));
    } else {
        formData.append('submit', 'save');
    }

    $.ajax({
        // url: formURL,
        url: 'content.php?SITE=clean-ajax',
        type: formAction,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR) {
            //    It works!
            if (closePopup) {
                oft2ClosePopup();
            }
            console.log(textStatus);
            location.reload(true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            console.log(errorThrown);
            alert("Something went wrong. Form not accepted. Create a screenshot of the next alert and send it to your administrator.");
            alert(errorThrown);
        }
    });
}