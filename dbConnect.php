<?php
require_once("toegang.php");

if(isset($db_inc)) return;
else $db_inc = TRUE;

$db = db_open();

function db_open() {
    global $db;

    if(!isset($_SESSION))
    {
      session_start();
    }
    $hash = $_SESSION['DB'];
    if($hash == null || $hash == "")
    {
      $toev        = date("dhym");
      if(isset($_GET["D"]))
      {
         $hash = $_GET["D"];
         if(!isset($_SESSION))
         {
           session_start();
         }
         $_SESSION['DB'] = $hash;
      } else if(isset($_POST["Database"]))
      {
         $hash = $_POST["Database"];
         if(!isset($_SESSION))
         {
           session_start();
         }
         $_SESSION['DB'] = $hash;
      }
    }
  
    if($hash == null || $hash == "") { $hash = "1234"; }
  
    //echo "HASH: ". $hash;
    //exit;
  
    $administraties = getAdministraties();
    if(!isset($administraties[$hash])) {
        $url = "../content.php?SITE=loginError";
        header("Location: $url"); exit;
    }
    $dbConn = $administraties[$hash];
    $dbHost = "172.22.0.117";
    if(isset($dbConn["dbhost"])) { $dbHost = $dbConn["dbhost"]; }
    $dbUser = $dbConn["dbuser"];
    $dbPass = $dbConn["dbpass"];
    $dbNaam = $dbConn["dbnaam"];

    $db = mysql_connect($dbHost, $dbUser, $dbPass);// or die(mysql_error());

    mysql_query("SET CHARACTER SET UTF8", $db);
    mysql_query("SET NAMES UTF8", $db);
  
    if(!$db) return $db;
    if(!mysql_select_db($dbNaam)) {
      mysql_close($db);
      return FALSE;
    }
    return $db;
}

function db_close($db) {
	return mysql_close($db);
}

function db_error($db) {
	return mysql_error($db);
}

function db_query($query, $db = "") {
   $start = microtime(true);
   
   if($db == "")
   {
     global $db;
   }
   if($query != "")
   {
       $result = mysql_query($query, $db);
       //echo("<br/><br/>Query: $query ");

       if (!$result) {
             //TODO: dit aan en uit kunnen zetten.
             echo("Fout in: ".str_replace("<", "&lt;", str_replace(">", "&gt;", $query))." <br/>DB error: " . db_error($db)); exit;

       } else {
          //echo "<br />". $query;
          /*if(
             strtolower(substr($query, 0, 6)) != "select"
             && strtolower(substr($query, 0, 19)) != "update geschiedenis"
             && strtolower(substr($query, 0, 24)) != "delete from geschiedenis"
             && strtolower(substr($query, 0, 24)) != "insert into geschiedenis"
             && strtolower(substr($query, 0, 35)) != "update personeel set laatstingelogt"
             && strtolower(substr($query, 0, 25)) != "delete from zitinchatroom"
          )
          {
              //echo "<br />". $query;
              //Registreer dat men inlogt, en haal daar 1 vanaf.
              $sql_toegang     = "SELECT * FROM toegang where VERVALDATUM <= '".date("Y-m-d")."' AND NOGOVER > 0 ORDER BY ID limit 1;";
              $result_toegang  = mysql_query($sql_toegang, $db) or die(db_error());
              if ($data_toegang = mysql_fetch_array($result_toegang))
              {
                $sql_toegang_update = "update toegang set LAATSTELOGIN = '".date("Y-m-d")."', NOGOVER = (NOGOVER-1) where ID = '".ps($data_toegang["ID"], "nr")."' AND NOGOVER > 0 limit 1;";
                mysql_query($sql_toegang_update, $db);
              }
          }*/
       }
   } else {
     $result = null;
   }

   //$end = microtime(true) - $start;
   //if($end > 0.1)
   //{
       //$file = "./bestanden/querylog.txt";
       //if (!$file_handle = @fopen($file,"a+")) { echo "Cannot open file"; }
       //if (!fwrite($file_handle, "$end - $query\n")) { echo "Cannot write to file"; }
       //@fclose($file_handle);
   //}

   return $result;
}

function db_num_rows($res) {
	return mysql_num_rows($res);
}

function db_insert_id($db) {
	return mysql_insert_id($db);
}

function db_fetch_row($res) {
	return mysql_fetch_row($res);
}

function db_fetch_assoc($res) {
	return mysql_fetch_assoc($res);
}
function db_fetch_array($res) {
	return mysql_fetch_array($res);
}

function db_real_escape_string($res)
{
        return mysql_real_escape_string($res);
}
