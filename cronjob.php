<?php
require_once "toegang.php";
//require_once("functies_security.php");

$admins = getAdministraties();

if (!isset($_SESSION)) {
    session_start();
}

foreach ($admins As $key => $dbConn) {
    $dbHost = $dbConn["dbhost"];
    $dbUser = $dbConn["dbuser"];
    $dbPass = $dbConn["dbpass"];
    $dbNaam = $dbConn["dbnaam"];

    $_SESSION['DB'] = $key;
    $_SESSION["GEENPOESPAS"] = "Nee";

    if ($db = mysqli_connect($dbHost, $dbUser, $dbPass)) {
        $GLOBALS['db'] = $db;

        mysqli_query($db, "SET CHARACTER SET UTF8");
        mysqli_query($db, "SET NAMES UTF8");

        if (mysqli_select_db($db, $dbNaam)) {
            //echo "<br/>".$dbNaam;
            cronjob();
        }

        mysqli_close($db);
        $db = null;
        $GLOBALS['db'] = null;
        $_SESSION['LID'] = null;
    }
}

function cronjob()
{
    global $db;

    optimaliseer_DK();

    $start = microtime(true);

    if ($result = mysqli_query($db, "SELECT * FROM login WHERE MASTER = 'Ja' ORDER BY ID limit 1;")) {
        while ($data = mysqli_fetch_array($result)) {
            //if(isset($_REQUEST["DEBUG"])) { echo "<br/>Laatst uigevoerd: " . $data["LAATSTUITGEVOERD"]; }
            //if($data["LAATSTUITGEVOERD"] == '' || date("Y-m-d H:i:s", strtotime($data["TERMIJN"], strtotime($data["LAATSTUITGEVOERD"]))) < date("Y-m-d H:i:s", time()) || isset($_REQUEST["DEBUG"])) {
            //Login onder de goede naam
            $SessionHash = cron_setLoginId($data["ID"]);
            $insert = "INSERT INTO ingelogt SET IP              = 'cronjob',
                                                DATUM              = '" . date("Y-m-d") . "',
                                                TIJD               = '" . date("h:i") . "',
                                                PERSONEELSLID      = '" . ($data["PERSONEELSLID"] * 1) . "',
                                                LOGINID            = '" . ($data["ID"] * 1) . "',
                                                SESSIONID          = '" . $SessionHash . "',
                                                BEDRIJFSID         = '" . ($data["BEDRIJFSID"] * 1) . "';";
            //echo "<br/>". $insert;
            mysqli_query($db, $insert);

            //echo $insert;
            //exit;
            notificatiesBijwerken();
            notificatiesMailen($data["BEDRIJFSID"]);

            //mysqli_query($db, "update cronjob set LAATSTUITGEVOERD = '".date("Y-m-d H:i:s", strtotime($data["TERMIJN"], strtotime($data["LAATSTUITGEVOERD"])))."' WHERE ID = '".$data["ID"]."' limit 1;", $db);
            mysqli_query($db,
                "update cronjob set LAATSTUITGEVOERD = '" . date("Y-m-d H:i:s") . "' WHERE ID = '" . $data["ID"] . "' limit 1;");
            //     }
        }
        if (isset($_REQUEST["DEBUG"])) {
            echo "<br/>Einde acties loop";
        }
    }

    if (isset($_REQUEST["DEBUG"])) {
        $end = microtime(true) - $start;
        echo "<br/>Einde cron: $end";
    }
}

function cron_setLoginId($loginid)
{
    if (!isset($_SESSION)) {
        session_start();
    }

    $db_hash = substr(md5(date("Y-m-d--H:i:s")), 0, 50);
    $db_hash = str_replace("\'", "", $db_hash);
    $db_hash = str_replace("\\", "", $db_hash);
    $db_hash = addslashes($db_hash);
    $_SESSION['LID'] = $db_hash;
    $_SESSION['IP'] = $_SERVER["REMOTE_ADDR"];

    return $db_hash;
}

function optimaliseer_DK()
{
    global $db;

    mysqli_query($db, "update bedrijf set LAND = 'Cura&ccedil;ao' where LAND = 'Cura&Atilde;&sect;ao';");
    mysqli_query($db, "update bedrijf set LAND = 'Cura&ccedil;ao' where LAND = 'Cura';");
    mysqli_query($db, "update bedrijf set LAND = 'Cura&ccedil;ao' where LAND = 'Curacao';");

    mysqli_query($db, "update bedrijf set TYPE = 'Nee' where TYPE is null;");
//    mysqli_query($db, "update bedrijf set ENDREDEN = 'Active' where ENDREDEN is null AND DELETED = 'Nee';");

    mysqli_query($db, "update personeel set TAAL = 'EN';");

    mysqli_query($db, "update country set NAAM = 'Cura&ccedil;ao' where NAAM = 'Cura&Atilde;&sect;ao';");
    mysqli_query($db, "update country set NAAM = 'Cura&ccedil;ao' where NAAM = 'Cura';");
    mysqli_query($db, "update country set NAAM = 'Cura&ccedil;ao' where NAAM = 'Curacao';");

    mysqli_query($db, "update bedrijf set LAND = 'Netherlands' WHERE LAND = 'The Netherlands';");
    mysqli_query($db, "update bedrijf set LAND = 'British Virgin Islands' WHERE LAND = 'Virgin Islands, British';");
}

/**
 * NOTIFICATIONS MAILEN
 *
 * @param $userid
 * @param $bedrijfsid
 */
function notificatiesBijwerken()
{
    global $db;
    require_once("includes.php");

    if (isset($_REQUEST["DEBUG"])) {
        echo "<Br/><br/>Notificaties update";
    }

    $sql = "SELECT ID, BEDRIJFSID
             FROM personeel
           WHERE (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null) 
             AND NOT LAATSTINGELOGT is null
             AND LAATSTINGELOGT >= '" . date("Y-m-d", strtotime("-35 day")) . "';";
    //echo $sql; exit;
    $result = mysqli_query($db, $sql);
    if ($result !== false) {
        while ($data = mysqli_fetch_array($result)) {
            check_annual_accounts($data["ID"], $data["BEDRIJFSID"]);
            oft_default_checks($data["ID"], $data["BEDRIJFSID"]);
            notificaties_bijwerken($data["ID"], $data["BEDRIJFSID"]);
        }
    }
}

/**
 * NOTIFICATIONS MAILEN
 *
 * @param $userid
 * @param $bedrijfsid
 */
function notificatiesMailen($bedrijfsid)
{
    global $db;
    require_once("includes.php");
    headcron();

    $duedate = date("Y-m-d", strtotime("+2 weeks")); // alles wat binnen twee weken afloopt

    $sql = "SELECT 
              personeel.EMAIL, 
              personeel.VOORNAAM, 
              personeel.TUSSENVOEGSEL, 
              personeel.ACHTERNAAM, 
              personeel.ID AS PERSONEELSLID, 
              bedrijf.BEDRIJFSNAAM, 
              oft_notificatie_gemaild.USERID AS GEMAILED,
              notificaties.*
           FROM notificaties
           JOIN oft_rechten_tot ON oft_rechten_tot.BEDRIJFSID = notificaties.BEDRIJFSID
           LEFT JOIN oft_notificatie_gemaild ON notificaties.ID = oft_notificatie_gemaild.NOTIFICATIEID
           JOIN personeel ON personeel.ID = oft_rechten_tot.USERID
           JOIN bedrijf ON bedrijf.ID = notificaties.BEDRIJFSID
           WHERE notificaties.STATUS = 'Active'
             AND notificaties.VERVALDATUM < '" . $duedate . "'
             AND oft_rechten_tot.MAILNOTIFICATIONS = 'Ja'
             AND (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null)";

    if (isset($_REQUEST["DEBUG"])) {
        echo "<Br/><br/>notificatiesMailen";
    }

    // Meerdere email-adressen: scheiden met ;
    //echo "<br/>$sql<br/>";
    $result = mysqli_query($db, $sql);
    if ($result !== false) {
        // Verzamel alle notificaties per e-mailadres
        $send_notifications = null;
        while ($data = mysqli_fetch_array($result)) {
            $send_notifications[$data['PERSONEELSLID'] * 1][$data['ID']] = $data;
            $already_mailed[$data['PERSONEELSLID'] * 1][$data['ID']] = $data['GEMAILED'];
        }
    }
    // Notificaties gevonden
    if ($send_notifications) {
        foreach ($send_notifications as $personeelslid => $row) {
            reset($row);
            $key = key($row);
            $to = trim($row[$key]['EMAIL']);
            $name = trim($row[$key]['VOORNAAM'] . ' ' . $row[$key]['TUSSENVOEGSEL'] . ' ' . $row[$key]['ACHTERNAAM']);

            $mailbody = "Dear " . $name . ", <br/><br/>Please take notice of the following notifications:<br/><hr/>";
            $message = null;
            $notifications = null;
            $mailed = null;

            foreach ($row as $note) {
                // check of deze notificatie al naar deze persoon is gemaild
                if ($already_mailed[$personeelslid][$note['ID']] != $note['PERSONEELSLID']) {
                    $now = time(); // or your date as well
                    $expiredate = strtotime($note['VERVALDATUM']);
                    $datediff = $expiredate - $now;
                    $days = floor($datediff / (60 * 60 * 24));

                    $message = "<br/><strong>Please be aware that the " . $note['EVENT'] . " of " . $note['BEDRIJFSNAAM'] . " is due in " . $days . " days.</strong><br/>";
                    $message .= "<br/>Entity: " . $note['BEDRIJFSNAAM'];
                    $message .= "<br/>Type: " . $note['EVENT'];
                    $message .= "<br/>Financial year: " . substr($note['STARTDATUM'], 0, 4);
                    $message .= "<br/>Standard due date: " . datumconversiePresentatieKort($note['VERVALDATUM']);
                    $message .= "<br/>Extention: " . datumconversiePresentatieKort($note['UPDATEDATUM']);
                    $message .= "<br/>Status: " . $note['STATUS'];
                    $message .= "<br/>";

                    $notifications[] = $message;
                    $mailed[] = "(" . ($note['PERSONEELSLID'] * 1) . ", " . $note['ID'] . ")";
                }
            }

            if (isset($_REQUEST["DEBUG"])) {
                echo "<br/>Notificatie: $personeelslid ";
            }

            // Verstuur mail met notificaties
            if ($notifications) {
                // send notifications
                $mailbody .= implode('<hr/>', $notifications);
                $mailbody .= "<hr/><br/><i>This is an automatic sent mail.</i>";
                $mailbody = '<div style="font-family:Arial, Helvetica">' . $mailbody . '</div>';

                mail_versturen("no-reply@orangefieldonline.com", $to, "", "", "Notification(s)", $mailbody, "", "",
                    $personeelslid, $bedrijfsid, "");

                if (isset($_REQUEST["DEBUG"])) {
                    echo("<br/>To: " . $to . '<hr/>' . $mailbody . "</br/>");
                }
                // register send mail
                $sql = "INSERT INTO oft_notificatie_gemaild (USERID, NOTIFICATIEID) VALUES " . implode(',', $mailed);
                mysqli_query($db, $sql);
            }
        }
    }

    footer0();
}

