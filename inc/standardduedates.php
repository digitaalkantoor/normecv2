<?php
function getStandardDueDates($type, $rechtsvorm)
{

    //TODO geen getallen gebruiken in array..
    $StandardDueDates = array(
        "47" => array( //09. Annual Accounts
            "ARUBA V.B.A." => array("maand" => "8"),
            "LUX S.A." => array("maand" => "7"),
            "LUX S.A.R.L." => array("maand" => "7"),
            "LUX S.C.A." => array("maand" => "7"),
            "NL B.V." => array("maand" => "7", "dag" => "8"),
            "NL N.V." => array("maand" => "7", "dag" => "8"),
            "NL Stichting" => array("maand" => "8", "dag" => "8"),
            "NL C.V." => array("maand" => "7", "dag" => "8"),
            "N.A. N.V." => array("maand" => "8"),
            "N.A. B.V." => array("maand" => "8"),
            "N.A. Foundation" => array("maand" => "8"),
            "Foundation" => array("maand" => "8"),
            "N.A. SPF" => array("maand" => "8"),
            "N.A. L.P." => array("maand" => "8"),
            "N.A. C.V." => array("maand" => "8"),
            "MAI GBC1/GBC2" => array("maand" => "6"),
            "Cyprus LLC" => array("maand" => "15"),
            "UK Ltd." => array("maand" => "9"),
            "FR S.A." => array("maand" => "7"),
            "FR S.A.S." => array("maand" => "7"),
            "FR S.A.R.L." => array("maand" => "7"),
            "FR EURL." => array("maand" => "7"),
            "FR SNC" => array("maand" => "7"),
            "FR SCS" => array("maand" => "7"),
            "Singapore private limited company" => array("maand" => "6"),
            "Spain SL" => array("maand" => "6", "dag" => "30"),
            "Coop." => array("maand" => "7", "dag" => "8"),
            "HK. Limited Company" => array("maand" => "9")
        ),
        "46" => array( //08. Annual General Meeting
            "ARUBA A.V.V." => array("maand" => "9"),
            "ARUBA N.V." => array("maand" => "9"),
            "ARUBA V.B.A." => array("maand" => "8", "dag" => "8"),
            "LUX S.A." => array("maand" => "6"),
            "LUX S.A.R.L." => array("maand" => "6"),
            "LUX S.C.A." => array("maand" => "6"),
            "NL B.V." => array("maand" => "6"),
            "NL N.V." => array("maand" => "6"),
            "MAI GBC1/GBC2" => array("maand" => "6"),
            "Cyprus LLC" => array("maand" => "15"),
            "UK Ltd." => array("maand" => "9"),
            "FR S.A." => array("maand" => "6"),
            "FR S.A.S." => array("maand" => "6"),
            "FR S.A.R.L." => array("maand" => "6"),
            "FR EURL." => array("maand" => "6"),
            "FR SNC" => array("maand" => "6"),
            "FR SCS" => array("maand" => "6"),
            "Spain SL" => array("maand" => "6"),
            "Coop." => array("maand" => "7"),
            "HK. Limited Company" => array("maand" => "9")
        ),
        "48" => array( //10. Tax Filings
            "ARUBA A.V.V." => array("maand" => "12"),
            "ARUBA N.V." => array("maand" => "12"),
            "ARUBA V.B.A." => array("maand" => "12"),
            "LUX S.A." => array("maand" => "5"),
            "LUX S.A.R.L." => array("maand" => "5"),
            "LUX S.C.A." => array("maand" => "5"),
            "NL B.V." => array("maand" => "5"),
            "NL N.V." => array("maand" => "5"),
            "NL Stichting" => array("maand" => "5"),
            "NL C.V." => array("maand" => "5"),
            "N.A. N.V." => array("maand" => "6"),
            "N.A. B.V." => array("maand" => "6"),
            "N.A. Foundation" => array("maand" => "6"),
            "Foundation" => array("maand" => "6"),
            "N.A. SPF" => array("maand" => "6"),
            "N.A. L.P." => array("maand" => "6"),
            "N.A. C.V." => array("maand" => "6"),
            "MAI GBC1/GBC2" => array("maand" => "6"),
            "Cyprus LLC" => array("maand" => "12"),
            "UK Ltd." => array("maand" => "11"),
            "FR S.A." => array("maand" => "4"),
            "FR S.A.S." => array("maand" => "4"),
            "FR S.A.R.L." => array("maand" => "4"),
            "FR EURL." => array("maand" => "4"),
            "FR SNC" => array("maand" => "4"),
            "FR SCS" => array("maand" => "4"),
            "Spain SL" => array("maand" => "6", "dag" => "25"),
            "Coop." => array("maand" => "5"),
            "Singapore private limited company" => array("maand" => "11"),
            "HK. Limited Company" => array("maand" => "8", "dag" => "15")
        ),
        "0" => array( //Extention
            "ARUBA V.B.A." => array("maand" => "13"),
            "NL B.V." => array("maand" => "13"),
            "NL N.V." => array("maand" => "13"),
            "NL Stichting" => array("maand" => "13"),
            "NL C.V." => array("maand" => "14"),
            "N.A. N.V." => array("maand" => "12"),
            "N.A. B.V." => array("maand" => "12"),
            "N.A. Foundation" => array("maand" => "12"),
            "Foundation" => array("maand" => "12"),
            "N.A. SPF" => array("maand" => "12"),
            "Cyprus LLC" => array("maand" => "0"), //Niet mogelijk
            "HK limited" => array("maand" => "8", "dag" => "15"),
            "Coop." => array("maand" => "13")
        )
    );

    $StandardDueDate = array("maand" => "N/A", "dag" => "");
    if (isset($StandardDueDates[$type][$rechtsvorm])) {
        $StandardDueDate = $StandardDueDates[$type][$rechtsvorm];
    }

    /*$dbHost = "localhost";
    $dbUser = "root";
    $dbPass = "";
    $dbNaam = "dkbeheer";
  
    $dbA = mysql_connect($dbHost, $dbUser, $dbPass) or die(mysql_error());
    if(!$dbA) return $dbA;
    if(mysql_select_db($dbNaam)) {
        $rAdmin  = mysql_query("SELECT * FROM administraties;", $dbA) or die(mysql_error());
        while($dAdmin = mysql_fetch_array($rAdmin))
        {
          $hash                  = $dAdmin["landcode"].$dAdmin["clientcode"];
          $administraties[$hash] = array("dbhost" => $dAdmin["dbhost"], "dbnaam" => $dAdmin["dbname"], "dbuser" => $dAdmin["dbuser"], "dbpass" => $dAdmin["dbpass"], "bedrijf" => $dAdmin["company"]);
        }
    }
    mysql_close($dbA); */

    return $StandardDueDate;
}

?>