<?php
function getStandardDueDates($type, $rechtsvorm)
{

    //TODO geen getallen gebruiken in array..
    $StandardDueDates = array(
        "47" => array( //09. Annual Accounts
            "ARUBA V.B.A." => array("maand" => "8"),
            "LUX S.A." => array("maand" => "7"),
            "LUX S.A.R.L." => array("maand" => "7"),
            "LUX S.C.A." => array("maand" => "7"),
            "NL B.V." => array("maand" => "7", "dag" => "8"),
            "NL N.V." => array("maand" => "7", "dag" => "8"),
            "NL Stichting" => array("maand" => "8", "dag" => "8"),
            "NL C.V." => array("maand" => "7", "dag" => "8"),
            "N.A. N.V." => array("maand" => "8"),
            "N.A. B.V." => array("maand" => "8"),
            "N.A. Foundation" => array("maand" => "8"),
            "N.A. SPF" => array("maand" => "8"),
            "N.A. L.P." => array("maand" => "8"),
            "N.A. C.V." => array("maand" => "8"),
            "MAI GBC1/GBC2" => array("maand" => "6"),
            "Coop." => array("maand" => "7", "dag" => "8")
        ),
        "46" => array( //08. Annual General Meeting
            "ARUBA A.V.V." => array("maand" => "9"),
            "ARUBA N.V." => array("maand" => "9"),
            "ARUBA V.B.A." => array("maand" => "8", "dag" => "8"),
            "LUX S.A." => array("maand" => "6"),
            "LUX S.A.R.L." => array("maand" => "6"),
            "LUX S.C.A." => array("maand" => "6"),
            "NL B.V." => array("maand" => "6"),
            "NL N.V." => array("maand" => "6"),
            "MAI GBC1/GBC2" => array("maand" => "6"),
            "Coop." => array("maand" => "7")
        ),
        "48" => array( //10. Tax Filings
            "ARUBA A.V.V." => array("maand" => "12"),
            "ARUBA N.V." => array("maand" => "12"),
            "ARUBA V.B.A." => array("maand" => "12"),
            "LUX S.A." => array("maand" => "5"),
            "LUX S.A.R.L." => array("maand" => "5"),
            "LUX S.C.A." => array("maand" => "5"),
            "NL B.V." => array("maand" => "5"),
            "NL N.V." => array("maand" => "5"),
            "NL Stichting" => array("maand" => "5"),
            "NL C.V." => array("maand" => "5"),
            "N.A. N.V." => array("maand" => "6"),
            "N.A. B.V." => array("maand" => "6"),
            "N.A. Foundation" => array("maand" => "6"),
            "N.A. SPF" => array("maand" => "6"),
            "N.A. L.P." => array("maand" => "6"),
            "N.A. C.V." => array("maand" => "6"),
            "MAI GBC1/GBC2" => array("maand" => "6"),
            "Coop." => array("maand" => "5")
        ),
        "0" => array( //Extention
            "ARUBA V.B.A." => array("maand" => "13"),
            "NL B.V." => array("maand" => "13"),
            "NL N.V." => array("maand" => "13"),
            "NL Stichting" => array("maand" => "13"),
            "NL C.V." => array("maand" => "14"),
            "N.A. N.V." => array("maand" => "12"),
            "N.A. B.V." => array("maand" => "12"),
            "N.A. Foundation" => array("maand" => "12"),
            "N.A. SPF" => array("maand" => "12"),
            "HK limited" => array("maand" => "8", "dag" => "15"),
            "Coop." => array("maand" => "13")
        )
    );

    $StandardDueDate = array("maand" => "N/A");
    if (isset($StandardDueDates[$type][$rechtsvorm])) {
        $StandardDueDate = $StandardDueDates[$type][$rechtsvorm];
    }

    /*$dbHost = "localhost";
    $dbUser = "root";
    $dbPass = "";
    $dbNaam = "dkbeheer";
  
    $dbA = mysql_connect($dbHost, $dbUser, $dbPass) or die(mysql_error());
    if(!$dbA) return $dbA;
    if(mysql_select_db($dbNaam)) {
        $rAdmin  = mysql_query("SELECT * FROM administraties;", $dbA) or die(mysql_error());
        while($dAdmin = mysql_fetch_array($rAdmin))
        {
          $hash                  = $dAdmin["landcode"].$dAdmin["clientcode"];
          $administraties[$hash] = array("dbhost" => $dAdmin["dbhost"], "dbnaam" => $dAdmin["dbname"], "dbuser" => $dAdmin["dbuser"], "dbpass" => $dAdmin["dbpass"], "bedrijf" => $dAdmin["company"]);
        }
    }
    mysql_close($dbA); */

    return $StandardDueDate;
}

?>