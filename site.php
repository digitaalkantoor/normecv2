<?php
sercurityCheck();

function site($site, $loginid)
{
    if ($loginid == "") {
        $loginid = 0;
    }

    $userid = ps(getUserID($loginid), "nr");
    $bedrijfsid = ps(getBedrijfsID($loginid), "nr");


    //##########################################Algemeen####################################
    if ($site == "setScherm") {
        setScherm($_GET["SCHERM"]);
    } elseif ($site == "leeg") {
        head2();
        footer1();
    } elseif ($site == "leegScherm") {
        head2();
        leegScherm();
        footer1();
    } elseif ($site == "digi") {
        head_start();
        enkelesite($loginid, "content.php?SITE=afterlogin");
        footer1();
    } elseif ($site == "draganddropupload") {
        draganddropupload($userid, $loginid, $bedrijfsid);
    } elseif ($site == "inloggen") {
        head2();
        enkeleSite($loginid, "admin/index.php");
        footer1();
    } elseif ($site == "uitloggen") {
        stopDigitaalKantoor($userid, $bedrijfsid);
    } elseif ($site == "gen_app_appointments_overzicht") {
        gen_app_appointments_overzicht($userid, $bedrijfsid);
    } elseif ($site == "gen_app_appointment") {
        gen_app_appointment($userid, $bedrijfsid);
    } elseif ($site == "gen_app_index") {
        gen_app_index($userid, $bedrijfsid);
    } elseif ($site == "gen_app_notifications_overzicht") {
        gen_app_notifications_overzicht($userid, $bedrijfsid);
    } elseif ($site == "gen_app_notifications") {
        gen_app_notifications($userid, $bedrijfsid);
    } elseif ($site == "gen_app_documents_overzicht") {
        gen_app_documents_overzicht($userid, $bedrijfsid);
    } elseif ($site == "gen_app_contracts_overzicht") {
        gen_app_contracts_overzicht($userid, $bedrijfsid);
    } elseif ($site == "gen_app_contract") {
        gen_app_contract($userid, $bedrijfsid);
    } elseif ($site == "afterlogin") {
        afterlogin($loginid);
    } elseif ($site == "importeerAuditfile") {
        head2();
        importeerAuditfile($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "toevwatisergebeurt" || $site == "bewwatisergebeurt" || $site == "overzwatisergebeurt" || $site == "getwatisergebeurt") {
        $koloms1 = array(
            0 => "ID",
            1 => "DATUM",
            2 => "OMSCHRIJVING",
            3 => "ACTIE",
            4 => "TABEL",
            5 => "ITEMID",
            6 => "BEDRIJFSID"
        );
        $koloms3 = getSiteKolom($site);
        $koloms2 = array(
            0 => "ID",
            1 => "field",
            2 => "textbox",
            3 => "field",
            4 => "field",
            5 => "field",
            6 => "hidden"
        );

        if ($site == "getwatisergebeurt") {
            return getKolomFromSites($koloms1, $koloms2, $koloms3, $loginid);
        }
    } // tijdelijk, na de update weer uitzetten
    elseif ($site == "oft_reset_alle_rechten") {
        echo oft_reset_alle_rechten();
    } else {
//        // execute anyways
//        update_database();
        require_once("./components/site_oft.php");
        site_site_oft($site, $loginid, $userid);
    }
}

function stamtabel($site, $tabel, $userid, $loginid, $bedrijfsid)
{
    //Stamtabel overzicht
    if ($site == "toev$tabel" || $site == "bew$tabel" || $site == "overz$tabel" || $site == "get$tabel") {
        $koloms1 = array(0 => "ID", 1 => "NAAM", 2 => "OPMERKING", 3 => "BEDRIJFSID");
        $koloms3 = getSiteKolom($site, $tabel);
        $koloms2 = array(0 => "ID", 1 => "require", 2 => "textbox", 3 => "hidden");

        if ($site == "get$tabel") {
            return getKolomFromSites($koloms1, $koloms2, $koloms3, $loginid);
        }
    }
}
