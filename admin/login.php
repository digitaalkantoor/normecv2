<?php
require_once("../dbConnecti.php");
require_once("../functies_security.php");
require_once("../xmlparser.php");
require_once("../functies_xml.php");

//checkSSL();

if (isset($_GET["U"])) {
    $USERNAME = $_GET["U"];
    $PASSWOORD = $_GET["P"];
    $hash = $_GET["D"];

} else {
    $toev = date("dhym");
    $USERNAME = $_POST["UserName"]; //$_POST["UserName$toev"];
    $PASSWOORD = $_POST["Password"]; //$_POST["Password$toev"];
    $hash = $_POST["Database"]; //$_POST["Database$toev"];
}
if ($hash == null || $hash == "") {
    $hash = "1234";
}

$USERNAME = checkInvoer($USERNAME);
$PASSWOORD = checkInvoer($PASSWOORD);

setCompanyDatabase($hash);
sleep(1);
setCompanyDatabase($hash);

mayAccessSystem();

if ($pdo == null) {
    header('Location: ../index.php?ERROR=5');
    exit;
}

//Statestieken per site
$query = $pdo->prepare('SELECT * FROM login WHERE LOWER(USERNAME) = :username AND NOT (SUSPEND = "Ja");');
$query->bindValue('username', $USERNAME);
$query->execute();

$validPassword = false;
$poging = 0;
foreach ($query->fetchAll() as $GEGEVENS) {
    if (valideerPass($GEGEVENS["PASSWORD"], $USERNAME, $PASSWOORD, $hash)) {

        $SessionHash = setLoginID(($GEGEVENS["ID"] * 1));
        $validPassword = true;

        //Schrijf log weg
        $query = $pdo->prepare('INSERT INTO ingelogt SET  IP    = :ip,
                                          DATUM              = :datum,
                                          TIJD               = :tijd,
                                          PERSONEELSLID      = :personeelslid,
                                          LOGINID            = :loginid,
                                          SESSIONID          = :sessionid,
                                          BEDRIJFSID         = :bedrijfsid');

        $datum = date("Y-m-d");
        $tijd = date("h:i");

        $query->bindValue('ip', $_SERVER["REMOTE_ADDR"]);
        $query->bindValue('datum', $datum);
        $query->bindValue('tijd', $tijd);
        $query->bindValue('personeelslid', $GEGEVENS["PERSONEELSLID"]);
        $query->bindValue('loginid', $GEGEVENS["ID"]);
        $query->bindValue('sessionid', $SessionHash);
        $query->bindValue('bedrijfsid', $GEGEVENS["BEDRIJFSID"]);
        $query->execute();


        $query = $pdo->prepare('delete FROM geschiedenis WHERE USERID = :userid;');
        $query->bindValue('userid', $GEGEVENS["PERSONEELSLID"]);
        $query->execute();

        //Succesfull login
        resetFailedLogins($_SERVER["REMOTE_ADDR"], $USERNAME);
        resetTokenRequest($_SERVER["REMOTE_ADDR"], $USERNAME);

        header("location: ../content.php?SITE=oft_home");
        $_SESSION['choice'] = 'unmade';
        exit;
    } elseif ($poging < 1) {
        //resetted password way too often
//        $nieuweCode = nieuweCodeGen();
//
//        $query = $pdo->prepare('update login set PASSWORD = :password WHERE ID = :id;');
//        $query->bindValue('password', $nieuweCode);
//        $query->bindValue('id', $GEGEVENS['ID']);
//        $query->execute();
    }
    $poging++;
}

if (!$validPassword) {

    addFailedLogins($_SERVER["REMOTE_ADDR"], $USERNAME, $PASSWOORD);

    if (isset($_REQUEST["redirect"]) && trim($_REQUEST["redirect"]) != "") {
        header("location: " . $_REQUEST["redirect"]);
        exit;
    } else {
        gaNaarLogin("5", "../");
    }
}

function valideerPass($password, $USERNAME, $invoerPASS, $hash)
{
    if (TypeLogin("..") == "CONTROLID") {
        $IDControlSharedSecret = IDControlSharedSecret("..");
        $ipIDControlServer = IDControlIP("..");
        $ipIDControlPort = IPIDControlPort("..");

        //echo "ID Control: $IDControlSharedSecret - $ipIDControlServer - $ipIDControlPort - User: $USERNAAM - P: $invoerPASS <br/>";

        require_once('./radius/radius.class.php');
        $radius = new Radius($ipIDControlServer, $IDControlSharedSecret);

        $radius->SetNasIpAddress($ipIDControlServer);
        $radius->SetNasPort($ipIDControlPort);
        //$radius->SetDebugMode(TRUE);

        if ($radius->AccessRequest($USERNAME, $invoerPASS)) {
            return true;
        }

        //echo $radius->GetReadableReceivedAttributes();

    } else {
        if (strtolower($password) == strtolower($invoerPASS)) {
            if (isset($_REQUEST["rememberMe"])) {
                rememberMe($USERNAME, $invoerPASS, $hash);
            } else {
                if (!isset($_REQUEST["nietRememberMe"])) {
                    deleteRememberMe();
                }
            }
            return true;
        }
    }
    return false;
}

function zetNaamEnWachtWoordOpServer($naam, $wachtwoord, $bestandslocatie)
{
    $content = "";
    $bingo = false;
    $ww = crypt($wachtwoord, CRYPT_STD_DES);
    $htpasswd_text = "$naam:$ww";

    $file = "../$bestandslocatie/.htpasswd";
    $lines = @file($file);
    $teller = 0;
    foreach ($lines as $line_num => $line) {
        $tmp = @explode(":", $line);
        if (strtolower($tmp[0]) == strtolower($naam) && strtolower($tmp[1]) == strtolower($ww)) {
            $bingo = true;
        }
        //echo $line;
        $teller++;
    }
    @fclose($file_handle);

    $voorvoegsel = "\n";
    if ($teller == 0) {
        $voorvoegsel = "";
    }

    if ($bingo == false) {
        if (!$file_handle = @fopen($file, "a+")) {
            echo "Cannot open file";
        }
        if (!fwrite($file_handle, $voorvoegsel . $htpasswd_text)) {
            echo "Cannot write to file";
        }
        //echo "You have successfully written data to $file";
        @fclose($file_handle);
    }
}

function rememberMe($username, $pass, $db)
{
    setcookie("REMEMBER", "$username;;;$pass;;;$db", (time() + 3600 * 24 * 365), "/");
}

function deleteRememberMe()
{
    setcookie("REMEMBER", "", (time() - 3600), "/");
}
