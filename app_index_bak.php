<?php
//error_reporting(E_ALL);

//Set header
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400'); // cache for 1 day
}

header("Content-Type: text/html; charset=UTF-8");
date_default_timezone_set('Europe/Amsterdam');
ini_set("default_charset","UTF-8");
ini_set("display_errors","Off");

$_REQUEST["APP_ACTIVE"] = 'true';

if(!isset($_SESSION)) {
  session_start();
}

$DATABASE = '';
if(isset($_REQUEST['DB'])) {
  $_SESSION['DB'] = $_REQUEST['DB'];
  $DATABASE = $_REQUEST['DB'];
}

$USERNAME = "";
if(isset($_REQUEST['USERNAME']) && $_REQUEST['USERNAME'] != '') {
  $USERNAME = $_REQUEST['USERNAME'];
}

$PASSWOORD = "";
if(isset($_REQUEST['PASSWORD']) && $_REQUEST['PASSWORD'] != '') {
  $PASSWOORD = $_REQUEST['PASSWORD'];
}

if(isset($_REQUEST["SENDPASS"]) && $USERNAME != '' && $DATABASE != '') {
  getSmsPassword($USERNAME, $DATABASE);
  exit;
}

require_once "includes.php";

$site = getSite();
if($site == "leeg") {
  $site = "gen_app_index";
  $_REQUEST["SITE"] = $site;
}

//Login as user
$loginid = 0;
if(isset($_REQUEST['X']) && $_REQUEST['X'] != '0' && $_REQUEST['X'] != '') {
  
  $query = $pdo->prepare('select LOGINID from ingelogt where SESSIONID = :sessionid limit 1;');
  $query->bindValue('sessionid', $_REQUEST['X']);
  $query->execute();

  foreach ($query->fetchAll() as $dX)
  {
    $loginid = $dX["LOGINID"] * 1;
  }
}

$query = null;
if(isset($_REQUEST['USERNAME']) && $_REQUEST['USERNAME'] != '') {
  $query = $pdo->prepare('SELECT * FROM login WHERE LOWER(USERNAME) = :username AND LOWER(PASSWORD) = :password AND not PASSWORD = ""  AND not USERNAME = "" AND NOT SUSPEND = "Ja" AND NOT KLANT = "Ja" limit 1;');
  $query->bindValue('username', $USERNAME);
  $query->bindValue('password', $PASSWOORD);
} else if($loginid > 0) {
  $query = $pdo->prepare('SELECT * FROM login WHERE ID = :loginid AND NOT SUSPEND = "Ja" AND NOT KLANT = "Ja" limit 1;');
  $query->bindValue('loginid', $loginid);
}

if($query != null) {
  $query->execute();

  foreach ($query->fetchAll() as $dLogin) {

    $SessionHash = setLoginID($dLogin["ID"]);

    if(!isset($_REQUEST["X"])) {
      $query = $pdo->prepare('INSERT INTO ingelogt SET  IP = "website",
                                           DATUM           = :datum,
                                           TIJD            = :tijd,
                                           PERSONEELSLID   = :personeelslid,
                                           LOGINID         = :loginid,
                                           SESSIONID       = :sessionid,
                                           BEDRIJFSID      = :bedrijfsid;');
      $datum = date("Y-m-d");
      $tijd = date("h:i");
      $query->bindValue('datum', $datum);
      $query->bindValue('tijd', $tijd);
      $query->bindValue('personeelslid', $dLogin["PERSONEELSLID"]);
      $query->bindValue('loginid', $dLogin["ID"]);
      $query->bindValue('sessionid', $SessionHash);
      $query->bindValue('bedrijfsid', $dLogin["BEDRIJFSID"]);
      $query->execute();
    }

    site($site, $dLogin["ID"]);
  } 
  //else {
  //  echo "<h3>1. Login error</h3>The username or password is not correct!";
  //}
} else {
  echo "<h3>2. Login error</h3>No valid authentication!";
}


/**
 * Genereerd een wachtwoord en stuurt deze per sms naar de gebruiker
 *
 * @param string $username Gebruikersnaam
 * @param string $database Database van de administratie
 */
function getSmsPassword($username, $database)
{
    global $pdo;
    
    echo "<h1>Send password by SMS</h1>";

    require_once("dbConnecti.php");

    $query = $pdo->prepare('SELECT personeel.MOB, login.ID as LoginId
              FROM login, personeel
             WHERE login.USERNAME = :username
               AND NOT (login.SUSPEND = "Ja")
               AND login.PERSONEELSLID = personeel.ID;');
    $query->bindValue('username', $username);
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        //Maak nieuwe code
        $nieuweCode = chr(mt_rand()) . date("mihs");
        $nieuweCode = substr(md5($nieuweCode), 0, 6);

        //Copy login
        $sLogin = "";
        
        $queryLogin = $pdo->prepare('select * from login WHERE login.USERNAME = :username;');
        $queryLogin->bindValue('username', $username);
        if ($queryLogin->execute() == 1) {
            foreach ($queryLogin->fetchAll() as $dLogin) {
                $sInsertLogin = "INSERT INTO login SET ";
                $values = array();
                foreach ($dLogin as $key => $value) {
                    if ($key == '' || is_numeric($key) || $key == "ID") {
                    } elseif ($key == "UPDATEDATUM") {
                        $sInsertLogin .= "  `" . $key . "` = :" . $key . ",";
                        $values[$key] = date("Y-m-d");
                    } elseif ($key == "PERSONEELSLID" || $key == "BEDRIJFSID") {
                        $sInsertLogin .= "  `" . $key . "` = :" . $key . ",";
                        $values[$key] = $dLogin[$key] * 1;
                    } else {
                        $sInsertLogin .= "  `" . $key . "` = :" . $key . ",";
                        $values[$key] = $dLogin[$key];
                    }
                }
                $sInsertLogin = substr($sInsertLogin, 0, strlen($sInsertLogin) - 1);

                $queryInsertLogin = $pdo->prepare($sInsertLogin);
                foreach($values as $key => $value) {
                  $queryInsertLogin->bindValue($key, $value); 
                }
                
                $queryInsertLogin->execute();
            }
        }

        $query = $pdo->prepare('update login set PASSWORD = :password WHERE login.USERNAME = :username;');
        $query->bindValue('password', $nieuweCode);
        $query->bindValue('username', $username);
        $query->execute();

        //SMS deze code
        $SMSUsername = "orange";
        $SMSPassword = "orange";

        $mob = $data["MOB"];
        if ($mob != "") {
            $mob = str_replace(" ", "", $mob);
            $mob = str_replace("-", "", $mob);
            $mob = str_replace("(", "", $mob);
            $mob = str_replace(")", "", $mob);
            $mob = str_replace("+", "00", $mob);

            $url = "http://sms.minded.nl/api_send_sms_get.php?user=$SMSUsername&pass=$SMSPassword&recnr=$mob&msg=$nieuweCode&sendernr=" . $mob;
            //echo $url;
            if(fopen($url, 'rb')) {
              echo "Password is send!";
            }
        } else {
          echo "No valid mobile number found. Please contact you administrator.";
        }
    } 
    //else {
    //  echo "Not a valid username.";
    //}
}
