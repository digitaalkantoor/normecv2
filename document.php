<?php
date_default_timezone_set('Europe/Amsterdam');
require_once 'functies_security.php';
require_once 'functies_algemeen.php';
require_once 'functies_functies.php';
require_once 'components/functies_oft_basic.php';

   if(!(isset($_REQUEST["TOKEN"]) && isset($_REQUEST["D"])) && !(isset($_REQUEST["CODE"])))
   {
     sercurityCheck();
   }
   require_once('dbConnecti.php');

   if(isset($_REQUEST["TOKEN"]) && strlen($_REQUEST["TOKEN"]) > 10)
   {
      $query = $pdo->prepare('SELECT ID, BESTANDSNAAM, BESTANDTYPE, BESTANDSIZE, BESTANDCONTENT, BEDRIJFSID, JAAR, DATUM, DOCTYPE FROM documentbeheer WHERE TOKEN = :token limit 1;');
      $query->bindValue('token', $_REQUEST["TOKEN"]);
      $query->execute();
      $result = $query->fetch(PDO::FETCH_ASSOC);

      if($result !== false) {
        list($DId, $name, $type, $size, $content, $bedrijfsid, $JAAR, $DATUM, $DOCTYPE) = $result;
        if(isRechtenOFT($bedrijfsid, "CRM", "Ja")) {

            $name = uniformFileName('documentbeheer', $DId);

            $query = $pdo->prepare('update documentbeheer set BEKEKEN = BEKEKEN + 1 WHERE TOKEN = :token LIMIT 1;');
            $query->bindValue('token', $_REQUEST["TOKEN"]);
            $query->execute();

            header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
            header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            session_cache_limiter("must-revalidate");

            header("Content-length: $size");
            header("Content-type: $type");
            header("Content-Disposition: attachment; filename=$name");

            echo ($content);

            exit;
        } else {
          echo "You do not have access rights to this document!"; exit;
        }
      }
   } else if(isset($_REQUEST["DID"]) || isset($_REQUEST["AID"]))
   {
       $table = "documentbeheer";
       if(isset($_REQUEST["DID"]))
       {
          $DId = ps($_GET["DID"], "nr");
       } else if(isset($_REQUEST["AID"])) {
          $DId = ps($_REQUEST["AID"], "nr");
          $table = "documentbeheer_archief";
       }
       $DId = substr($DId,0,5);
       if(!empty($table)) {
         $tableData = getTableData($table);
         if(empty($tableData)) {
          return;
         }
       }

       $query = $pdo->prepare('SELECT BESTANDSNAAM, BESTANDTYPE, BESTANDSIZE, BESTANDCONTENT, BEDRIJFSID, JAAR, DATUM, DOCTYPE FROM ' . $table . ' WHERE ID = :id limit 1;');
       $query->bindValue('id', $DId);
       $query->execute();
       $result = $query->fetch(PDO::FETCH_NUM);

       //echo $query;
       if($result !== false)
       {
          list($name, $type, $size, $content, $bedrijfsid, $JAAR, $DATUM, $DOCTYPE) = $result;
          
          $name = uniformFileName($table, $DId);

          if(isset($_REQUEST["PREVIEW"]) && class_exists('Imagick')) {
            if(isRechtenOFT($bedrijfsid, "CRM", "Ja")) {
              //extension=php_imagick.dll
              //$source = realpath($source);
              //$target = dirname($source).DIRECTORY_SEPARATOR.$target;
              $source = "document.php?DID=$DId";
              $source = "http://localhost/digitaalkantoor/Documentatie/Algemene_Voorwaarden_Digitaal_Kantoor.pdf";
              $im = new Imagick($source."[0]"); // 0-first page, 1-second page
              $im->setImageColorspace(255); // prevent image colors from inverting
              $im->setimageformat("jpeg");
              $im->thumbnailimage(160, 120); // width and height
              //$im->writeimage($target);
              
              // Output the image
              imagejpeg($im);
              
              // Free up memory
              imagedestroy($im);
              //$im->clear();
              //$im->destroy();
            } else {
              //Show default
            }
          } else {
            if(isRechtenOFT($bedrijfsid, "CRM", "Ja")) {
              $query = $pdo->prepare('update ' . $table . ' set BEKEKEN = BEKEKEN + 1 WHERE ID = (:id*1) LIMIT 1;');
              $query->bindValue('id', $DId);
              $query->execute();
              
              header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
              header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
              header("Pragma: public");
              header("Expires: 0");
              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              header("Cache-Control: public");
              header("Content-Description: File Transfer");
              session_cache_limiter("must-revalidate");
  
              header("Content-length: $size");
              header("Content-type: $type");
              header("Content-Disposition: attachment; filename=$name");
  
              echo ($content);
  
              exit;
            } else {
              echo "You do not have access rights to this document!"; exit;
            }
          }
        }
   } else if(isset($_GET["DID_BLOB"]))
   {
       $DId = addslashes($_GET["DID_BLOB"]);
       $DId = substr($DId,0,5);
       $query = $pdo->prepare('SELECT NAAM, PDFBLOB FROM documenten WHERE ID = :id limit 1;');
       $query->bindValue('id', $DId);
       $query->execute();
       
       $result = $query->fetch(PDO::FETCH_ASSOC);

       if($result !== false) {
            list($name, $content) = $result;

            header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
            header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            session_cache_limiter("must-revalidate");

            //header("Content-length: $size");
            header("Content-type: PDF");
            header("Content-Disposition: attachment; filename=$name.pdf");

            echo ($content);
    
            exit;
        }
   } else if(isset($_GET["DAID"]))
   {
       $DId                = addslashes($_GET["DAID"]);
       $DId                = substr($DId,0,5);
       
      $query = $pdo->prepare('SELECT * FROM documenten_archief WHERE ID = :id limit 1;');
      $query->bindValue('id', $DId);
      $query->execute();
      $result = $query->fetch(PDO::FETCH_ASSOC);

      if($result !== false) {
        echo ("<html>
                 <head>
                 <title>{$data["NAAM"]} - {$data["DATUMTIJD"]}</title>
                 </head>
                 <body>");

                 echo urlEnDeCodeTekst(stripslashes($data["TEKST"]), "decode");
                 echo("</body>
               </html>");
      }
   }
   // template uit de database halen en omzetten naar word document
   else if(isset($_GET["WIZZARD"]) && isset($_GET["KOLOM"]))
   {
     
      $tableData = getTableData('oft_wizzard');
      if(in_array($_GET["KOLOM"], $tableData)) {
        return;
      }
      
      $query = $pdo->prepare('SELECT `TYPEFORM`, `' . $tableData . '` As DOC FROM oft_wizzard WHERE ID = :id limit 1;');
      $query->bindValue('id', $_GET["WIZZARD"]);
      $query->execute();
      $data = $query->fetch(PDO::FETCH_ASSOC);

      if($data !== false) {
        /*
        echo "<BR />document.php, r165:<BR /> ".$query;
        die($data["DOC"]);
         header('Cache-control: private');
         header('Pragma: public');
         header('Content-Type: application/msword');
         header('Content-Disposition: attachment; filename=document.doc');

         echo urlEnDeCodeTekst(stripslashes($data["DOC"]), "decode");
        */
        $bestandsnaam =  isset($_GET["NAME"]) && $_GET["NAME"] ? ps($_GET["NAME"]) : $data["TYPEFORM"] . "_" . strtolower(ps($_GET["KOLOM"]));
        $bestandsnaam .= ".docx";

        $vars["CONTENT"] = $data["DOC"];
        $vars["VARS"] = "CONTENT;";

        header('Cache-control: private');
        header('Pragma: public');
        header('Content-Type: application/msword');
        header('Content-Disposition: attachment; filename='.$bestandsnaam);

        $resource = curl_init();
        curl_setopt($resource, CURLOPT_URL, "http://docx.digitaalkantoor.net/service.php");
        //curl_setopt($resource, CURLOPT_HEADER, 1);
        curl_setopt($resource, CURLOPT_POST,count($vars));
        curl_setopt($resource, CURLOPT_POSTFIELDS,http_build_query($vars));
        curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($resource, CURLOPT_BINARYTRANSFER, 1);
        $file = curl_exec($resource);
        $info = curl_getinfo($resource, CURLINFO_CONTENT_TYPE);
        curl_close($resource);

        //var_dump($info);
        //var_dump($file);

        echo $file;
      }
   } else if(isset($_GET["WIZZARD"]))
   {
      //echo $query;
      $query = $pdo->prepare('SELECT * FROM oft_wizzard WHERE ID = :id limit 1;');
      $query->bindValue('id', $_GET["WIZZARD"]);
      $query->execute();
      $data = $query->fetch(PDO::FETCH_ASSOC);

      if($data !== false) {
         //header('Cache-control: private');
         // header('Pragma: public');
         //header('Content-Type: application/msword');
         //header('Content-Disposition: attachment; filename=document.doc');

         $template = "";
         if(isset($_REQUEST["FILE"])) {
           if(file_exists('./sjablonen/'.$_REQUEST["FILE"])) {
             $template  = file_get_contents('./sjablonen/'.$_REQUEST["FILE"]);
           }
         }

         //Replace wizzard data
         foreach($data As $key => $value) {
           $template = str_replace("[". $key . "]", $value, $template);
         }

         //Edit company data
         if(isset($_REQUEST["BEDRIJFSID"])) {
          $query = $pdo->prepare('SELECT *
                                   FROM bedrijf
                                  WHERE ID = :id;');
          $query->bindValue('id', $_REQUEST["BEDRIJFSID"]);
          $query->execute();
          $dBedrijf = $query->fetch(PDO::FETCH_ASSOC);

          if($dBedrijf !== false) {
            $template = preg_replace("/n(.*)a(.*)m(.*)e(.*)e(.*)n(.*)t(.*)i(.*)t(.*)y/", $dBedrijf["BEDRIJFSNAAM"], $template);

            //$template = str_replace("[name entity]", $dBedrijf["BEDRIJFSNAAM"], $template);
            //$template = str_replace("[entity address]", $dBedrijf["ADRES"], $template);
            //$template = str_replace("[entity city]", $dBedrijf["PLAATS"], $template);
            echo $template; exit;
          }
         }

         echo $template;
      }
   } else if(isset($_GET["WORD"]))
   {
        $sql = "";
        $ACHTERGROND = "";
        $docs = 0;
        $bestandsnaam = "document.doc";
        if(isset($_REQUEST["ARRAYID"]))
        {
           $idArray           = substr($_REQUEST["ARRAYID"], 0, strlen($_REQUEST["ARRAYID"])-1);
           $idArray           = ps($idArray, "");
           if($idArray != "")
           {
             $idArray         = str_replace(";", " OR ID = ", $idArray);
             $sql             = "SELECT * FROM documenten WHERE ID = $idArray;";
           }
        } else {
           $id                = ps($_GET["ID"], "nr");
           $sql               = "SELECT * FROM documenten WHERE ID = ('".$id."'*1) LIMIT 1;";
        }
        $html                 = "";
        
        //TODO : Moet veiliger worden
        $query = $pdo->prepare($sql);
        $query->execute();

        foreach ($query->fetchAll() as $data) {
              if($data["ACHTERGROND"] != "") { 
                 $ACHTERGROND    = $data["ACHTERGROND"];
              }
              if(!isset($_REQUEST["ARRAYID"]))
              {
                 $bestandsnaam   = $data["NAAM"].".doc";
              }

              $tekst = str_replace("<center style=\"PAGE-BREAK-AFTER: always\"></center>", "--PAGE_BREAK--", $data["TEKST"]);
              $tekst = stripslashes($tekst);
              $tekst = explode("--PAGE_BREAK--", $tekst);

              for($y = 0; $y < count($tekst); $y++)
              {
                  //TODO add page
                  $html .= $tekst[$y];
              }
              $docs++;
        }

        header('Cache-control: private');
header('Pragma: public');
header('Content-Type: application/msword');
header('Content-Disposition: attachment; filename='.$bestandsnaam);

  /*
  <!--[if gte mso 9]><xml>
       <w:WordDocument>
        <w:View>Print</w:View>
        <w:HyphenationZone>21</w:HyphenationZone>
        <w:ValidateAgainstSchemas/>
        <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
        <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
        <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
        <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
       </w:WordDocument>
      </xml><![endif]-->
      <!--[if gte mso 9]><xml>
       <w:LatentStyles DefLockedState=\"false\" LatentStyleCount=\"156\">
       </w:LatentStyles>
      </xml><![endif]-->
      */
        echo("<html xmlns:v=\"urn:schemas-microsoft-com:vml\"
    xmlns:o=\"urn:schemas-microsoft-com:office:office\"
    xmlns:w=\"urn:schemas-microsoft-com:office:word\"
    xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\"
    xmlns:st1=\"urn:schemas-microsoft-com:office:smarttags\"
    xmlns=\"http://www.w3.org/TR/REC-html40\">
    <head>
      <meta http-equiv=Content-Type content=\"text/html; charset=us-ascii\">
      <meta name=ProgId content=Word.Document>
      <meta name=Generator content=\"Microsoft Word 11\">
      <meta name=Originator content=\"Microsoft Word 11\">

      <title>$bestandsnaam</title>
      <style>
      <!--");
      if($ACHTERGROND != "") {
          echo("body {
            background-image: url(./document.php?DID=".stripslashes($ACHTERGROND).");
            background-repeat: no-repeat;
            background-position: 0px 0px;
            }");
      }
        echo("@page Section3 {
           size:595.3pt 841.9pt;
           margin:53.85pt 70.9pt 70.9pt 70.9pt;
           mso-header-margin:35.45pt;
           mso-footer-margin:35.45pt;
           mso-paper-source:0;
           font-family:Arial;
        }
        div.Section3 { page:Section3; }
        @page Section2 {
                size:841.9pt 595.3pt;
        	writing-mode: tb-rl;
        	margin:70.9pt 53.85pt 70.9pt 70.9pt;
        	mso-header-margin:35.45pt;
        	mso-footer-margin:35.45pt;
        	mso-paper-source:0;
                font-family:Arial;
        }
        div.Section2 { page:Section2; }
      -->
      </style>
    </head>

    <body>");//mso-page-orientation:landscape;

    echo $html;

    echo ("\n</body>\n</html>");

   } else {

        $sql = "";
        $ACHTERGROND = "";
        $ACHTERGROND_docroot = "";
        $docs = 0;
        if(isset($_REQUEST["ARRAYID"]))
        {
           $idArray           = substr($_REQUEST["ARRAYID"], 0, strlen($_REQUEST["ARRAYID"])-1);
           $idArray           = ps($idArray, "");
           if($idArray != "")
           {
             $idArray         = str_replace(";", " OR ID = ", $idArray);
             $sql             = "SELECT * FROM documenten WHERE ID = $idArray;";
           }
        } else {
           $id                = ps($_GET["ID"], "nr");
           $sql               = "SELECT * FROM documenten WHERE ID = ('".$id."'*1) LIMIT 1;";
        }
        
        $font                 = "Helvetica";
        $fontsize             = "12";        

        $html                 = "";

        //TODO : moet veiliger
        $query = $pdo->prepare($sql);
        $query->execute();

        foreach ($query->fetchAll() as $data) {
              if(trim(getNaam("doctemplate", $data["TEMPLATEID"], "FONT")) != '') {
                $font = trim(getNaam("doctemplate", $data["TEMPLATEID"], "FONT"));
              }
              
              if(trim(getNaam("doctemplate", $data["TEMPLATEID"], "FONTSIZE")) != '') {
                $fontsize = trim(getNaam("doctemplate", $data["TEMPLATEID"], "FONTSIZE"));
              }

              if($data["ACHTERGROND"] != "") {
                $query = $pdo->prepare('select DEFAULTDIR from bedrijf where ID = :id limit 1;');
                $query->bindValue('id', $data["BEDRIJFSID"]);
                $query->execute();
                $dBedrijf = $query->fetch(PDO::FETCH_ASSOC);

                if($dBedrijf !== false) {
                    $BASE_URL = ($_SERVER['SERVER_PORT'] == 443 ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['SCRIPT_NAME']) . "/";
                    $ACHTERGROND    = $BASE_URL . $dBedrijf["DEFAULTDIR"] ."/achtergrond/". $data["ACHTERGROND"];
                    $ACHTERGROND_docroot = $_SERVER["DOCUMENT_ROOT"] . dirname($_SERVER['SCRIPT_NAME']) . $dBedrijf["DEFAULTDIR"] ."\\achtergrond\\". $data["ACHTERGROND"];
                    $ACHTERGROND_docroot = str_replace("\\","/", $ACHTERGROND_docroot);
                 }
              }
              $tekst = str_replace("<center style=\"PAGE-BREAK-AFTER: always\"></center>", "--PAGE_BREAK--", $data["TEKST"]);
              $tekst = str_replace("--PAGE_BREAK--", "<div style=\"page-break-before:always\"></div>", $tekst);
              $tekst = str_replace("&euro;", "&#128;", $tekst);
              $tekst = stripslashes($tekst);
              $html .= $tekst;
              $docs++;
        }

        $footer = "";
        $footer_tmp = explode("--FOOTER_START--", $html);
        if(count($footer_tmp) > 1) {
           $footer_tmp2 = explode("--FOOTER_STOP--", $footer_tmp[1]);
           if(isset($footer_tmp2[0])) {
             $footer = $footer_tmp2[0];
             $html = $footer_tmp[0];
           }
        }

$headerFooter = "";
if(trim($footer) != "") {

$headerFooter = "<script type=\"text/php\">

if ( isset("."$"."pdf) ) {

  "."$"."font = Font_Metrics::get_font(\"$font\");;
  "."$"."size = $fontsize;
  "."$"."color = array(0,0,0);
  "."$"."text_height = Font_Metrics::get_font_height("."$"."font, "."$"."size);

  "."$"."foot = "."$"."pdf->open_object();

  "."$"."w = "."$"."pdf->get_width();
  "."$"."h = "."$"."pdf->get_height();

  // Draw a line along the bottom
  "."$"."y = "."$"."h - 2 * "."$"."text_height - 24;
  "."$"."y += "."$"."text_height;

  "."$"."pdf->close_object();
  "."$"."pdf->add_object("."$"."foot, \"all\");

  global "."$"."initials;
  "."$"."initials = "."$"."pdf->open_object();

  // Mark the document as a duplicate
  "."$"."text = \"$footer\";

  // Center the text
  "."$"."width = Font_Metrics::get_text_width(\"$footer\", "."$"."font, "."$"."size);
  "."$"."pdf->page_text("."$"."w / 2 - "."$"."width / 2, "."$"."y, "."$"."text, "."$"."font, "."$"."size, "."$"."color);

}
</script>";
}
        //echo $ACHTERGROND; exit;
        $style = "";
        if(file_exists($ACHTERGROND_docroot) && $ACHTERGROND != "") {
            $style = "body {
                      background-image: url(".stripslashes($ACHTERGROND).");
                      background-repeat: no-repeat;
                      background-position: 0px 0px;
                      }";
        }
        $html = "<html><head><style>$style</style></head><body style=\"margin: 0; padding: 0;\"><div style=\"padding: 50px 50px 50px 50px; height: 100%;\">$html</div>$headerFooter</body></html>";

        //echo $ACHTERGROND_docroot. $html; exit;

        if($docs > 0)
        {
            require_once("./dompdf-0.5.1/dompdf_config.inc.php");
            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->render();
            $dompdf->stream("doc.pdf");

            exit(0);
        }
   }

function urlEnDeCodeTekst($input, $encodeDecode)
{
   $input = stripslashes($input);

   $host = basisUrlDK();
   $zoek = "../../../../../";
   $input = str_replace($zoek, $host, $input);

   if($encodeDecode == "decode") {
     $output = preg_replace_callback('#src="([^"]*)"#is', 'my_urldecode', $input);
   } else {
     $output = preg_replace_callback('#src="([^"]*)"#is', 'my_urlencode', $input);
   }

   return $output;
}

function my_urlencode($a){
	return "src=\"". urlencode($a[1]) . "\"";
}

function my_urldecode($a){
	return "src=\"". urldecode($a[1]) . "\"";
}
