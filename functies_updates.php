<?php

/**
 * ANDERE DATABASE UPDATEN
 *
 * @param $dbNaam
 *
 * @return mysqli
 *
 * Check of alles geimporteerd is:
 * SELECT (personeel.ID *1) AS ID, CONCAT( VOORNAAM,  '', ACHTERNAAM ) AS NAAM, personeel.BEDRIJFSNAAM
 * FROM personeel
 * LEFT JOIN oft_rechten_tot ON personeel.ID*1 = oft_rechten_tot.USERID
 * WHERE (personeel.DELETED !=  'Ja' OR personeel.DELETED IS NULL) AND oft_rechten_tot.USERID IS NULL
 * GROUP BY personeel.ID
 */
function set_update_db($nr)
{

    if ($nr) {
        $administraties = oft_administraties_for_update($nr);
    } else {
        $hash = db_hash_get();
        $administraties = getAdministraties();

    }
    $dbConn = $administraties[$hash];
    $dbHost = "localhost";
    if (isset($dbConn["dbhost"])) {
        $dbHost = $dbConn["dbhost"];
    }
    $dbUser = $dbConn["dbuser"];
    $dbPass = $dbConn["dbpass"];
    $dbNaam = $dbConn["dbnaam"];

    $db = new mysqli($dbHost, $dbUser, $dbPass, $dbNaam);
    return $db;
}

/**
 * @param bool $lastid
 * @param int  $c
 *
 * @return bool
 *
 * function oft_eenmalige_reset_alle_rechten($lastid = false, $c = 0) {
 * global $db;
 *
 * // check uitvoer
 * if (!$lastid) {
 * $check = "SELECT * FROM updates WHERE FILE = 'oft_reset_alle_rechten' LIMIT 1";
 * $result = db_query($check, $db);
 * while($row = db_fetch_array($result)) {
 * return false;
 * }
 * }
 *
 * $secretkey = "W9OsE9d7WJrQW-E".date("i");
 * $lastid = oft_reset_alle_rechten($secretkey, $lastid);
 * if ($lastid && $c < 10) {
 * $c++;
 * oft_eenmalige_reset_alle_rechten($lastid, $c) ;
 * }
 * }
 *
 * /**
 * Alle medewerkers alle rechten tot alle entiteitten geven.
 * De secretkey is om te voorkomen dat de functie meerdere keren wordt uitgevoerd
 * De key wordt aangevult door de huiidge minuten (met leading zero)
 * Indien de key niet klopt zie je de huidige minuten (en seconden) in beeld, zodat je weet wat je moet aanpassen.
 * Functie aanroepen: SITE=oft_reset_alle_rechten&secretkey=W9OsE9d7WJrQW-E34&database=orangefield_oft003
 * De laatste twee cijfers van de secretkey (34) zijn dus de huidige minuten
 */
function oft_reset_alle_rechten($requestkey = false, $lastid = false, $toid = false)
{
    global $pdo;

    $udb = isset($_REQUEST["database"]) ? set_update_db($_REQUEST["database"]) : $db;

    $log = "";
    $secretkey = "W9OsE9d7WJrQW-E" . date("i");
    $requestkey = $requestkey ? $requestkey : (isset($_REQUEST["secretkey"]) ? $_REQUEST["secretkey"] : false);
    $aantalperkeer = 25;

    if ($requestkey != $secretkey) {
        return "Not allowed! (" . date("i-s") . ")";
    }

    $log .= "<h1>Eenmalige update</h1>";
    $log .= "<p>Mysql kan vastlopen als alle medewerkers voor alle entiteiten tegelijk geimporteerd worden.</p>";

    $fromid = $lastid ? $lastid : (isset($_REQUEST["fromid"]) ? $_REQUEST["fromid"] : 0);
    $toid = $toid ? $toid : (isset($_REQUEST["toid"]) ? $_REQUEST["toid"] : 0);

    // Alle medewerkers ophalen
    $query = $pdo->prepare('SELECT (ID*1) as ID, CONCAT(VOORNAAM,"", ACHTERNAAM) AS NAAM FROM personeel WHERE (DELETED != "Ja" OR DELETED IS NULL) AND ID > :formid');
    if ($toid) {
        $query = $pdo->prepare('SELECT (ID*1) as ID, CONCAT(VOORNAAM,"", ACHTERNAAM) AS NAAM FROM personeel WHERE (DELETED != "Ja" OR DELETED IS NULL) AND ID > :formid AND ID < :todid');
        $query->bindValue('id', $toid);
    }
    $c = 0;

    $query->bindValue('formid', $fromid);
    $query->execute();

    foreach ($query->fetchAll() as $row_p) {
        if ($c < $aantalperkeer) {
            $personeel[$row_p["ID"]] = $row_p["NAAM"];
            $lastid = $row_p["ID"];
            $c++;
        }
    }

    // Alle entities ophalen
    $query = $pdo->prepare('SELECT (ID*1) as ID, BEDRIJFSNAAM as NAAM FROM bedrijf WHERE DELETED != "Ja" OR DELETED IS NULL');
    $query->execute();

    foreach ($query->fetchAll() as $row_b) {
        $bedrijf[$row_b["ID"] * 1] = $row_b["NAAM"];
    }

    // Oft rechten opnieuw inrichten
    if ($fromid == 0) {
        $query = $pdo->prepare('TRUNCATE oft_rechten_tot');
        $query->execute();
    }

    $oft_rechten = array();
    foreach ($personeel as $pid => $pnaam) {
        foreach ($bedrijf as $bid => $bnaam) {
            $log .= "<p>" . $pnaam . ":  " . $bnaam . "</p>";
            $queryInsert = $pdo->prepare('INSERT INTO oft_rechten_tot (USERID, BEDRIJFSID, LEGAL , COMPLIANCE, BOARDPACK, FINANCE, HR, CONTRACTS, MAILNOTIFICATIONS) VALUES (:pid, :bid, "Ja" , "Ja", "Ja", "Ja", "Ja", "Ja", "Nee")');
            $queryInsert->bindValue('pid', $pid);
            $queryInsert->bindValue('bid', $bid);
            $queryInsert->execute();
        }
    }


    $nogeenkeer = "";
    if ($c == $aantalperkeer) {
        $nogeenkeer = '<h3><a href="/content.php?SITE=oft_reset_alle_rechten&secretkey=W9OsE9d7WJrQW-E' . date("i") . '&fromid=' . $lastid . ($toid ? "&toid=" . $toid : false) . '">Volgende ' . $aantalperkeer . ' medewerkers</a></h3>';
    }
    $log = "<h3>$c medewerkers alle rechten gegeven</h3>" . $log;

    echo $nogeenkeer . $log;

    if ($c == $aantalperkeer) {
        return $lastid;
    } else {
        $query = $pdo->prepare('INSERT INTO updates SET FILE="oft_reset_alle_rechten", DATUM = NOW()');
        $query->execute();
        return false;
    }
}


function oft_administraties_for_update($nr)
{
    $adm = array(
        1 => array(
            "dbnaam" => "arch_db1",
            "dbuser" => "orangefield1",
            "dbpass" => 'k6N$jW2V',
            'dbhost' => 'oft-p-db1.p'
        ),
        2 => array(
            "dbnaam" => "arch_db2",
            "dbuser" => "orangefield2",
            "dbpass" => '3N#636xx',
            'dbhost' => 'oft-p-db1.p'
        ),
        3 => array(
            "dbnaam" => "arch_db3",
            "dbuser" => "orangefield3",
            "dbpass" => 'M<3bZ56n',
            'dbhost' => 'oft-p-db1.p'
        ),
        4 => array(
            "dbnaam" => "arch_db4",
            "dbuser" => "orangefield4",
            "dbpass" => 'fP6xQwCF',
            'dbhost' => 'oft-p-db1.p'
        ),
        5 => array(
            "dbnaam" => "arch_db5",
            "dbuser" => "orangefield5",
            "dbpass" => 'G>9c4z0v',
            'dbhost' => 'oft-p-db1.p'
        ),
        6 => array(
            "dbnaam" => "arch_db6",
            "dbuser" => "orangefield6",
            "dbpass" => '223pe6Le',
            'dbhost' => 'oft-p-db1.p'
        ),
        7 => array(
            "dbnaam" => "arch_db7",
            "dbuser" => "orangefield7",
            "dbpass" => 'Ve5pw4$Q',
            'dbhost' => 'oft-p-db1.p'
        ),
        8 => array(
            "dbnaam" => "arch_db8",
            "dbuser" => "orangefield8",
            "dbpass" => 'I22yT9gU',
            'dbhost' => 'oft-p-db1.p'
        ),
        9 => array(
            "dbnaam" => "arch_db9",
            "dbuser" => "orangefield9",
            "dbpass" => 'd2kzI8Fu',
            'dbhost' => 'oft-p-db1.p'
        ),
        10 => array(
            "dbnaam" => "arch_db10",
            "dbuser" => "orangefield10",
            "dbpass" => 'SdV2Hks7',
            'dbhost' => 'oft-p-db1.p'
        ),
    );
    return $adm[$nr];
}

function update_database()
{
    global $pdo;

    $results = [];

    // update notification table with a financialyear column
    $results[] = $pdo->exec("ALTER TABLE notificaties ADD FINANCIALYEAR YEAR");

    // Adjust endreden
    $results[] = $pdo->exec("UPDATE bedrijf SET ENDREDEN = NULL WHERE ENDREDEN = 'Active'");

    // update dates that are filled with 0000-00-00 instead of NULL
    $updateDates = [
        'bedrijf' => ['updatedatum', 'datum', 'firstbookyear', 'oprichtingsdatum', 'enddatum'],
        'oft_annual_accounts' => ['updatedatum', 'standardduedate', 'extention', 'filingdate'],
        'oft_tax' => ['updatedatum', 'standardduedate', 'extention', 'filingdate'],
        'documentbeheer' => ['updatedatum', 'datum',' stopdatum']
    ];
    $c = 0;
    $pdo->query("SET GLOBAL sql_mode='', SESSION sql_mode=''");
    foreach ($updateDates as $table => $columns) {
        foreach ($columns as $column) {
            $column = strtoupper($column);
            $query = "UPDATE $table SET $column = NULL WHERE $column = '0000-00-00'";
            $c += $pdo->exec($query);
        }
    }
    $results[] = $c;

    // Attach tax documents to tax reports
    $results[] = $pdo->exec("UPDATE documentbeheer as doc JOIN oft_tax as source ON doc.BEDRIJFSID = source.BEDRIJFSID SET doc.TABEL = 'oft_tax', doc.TABELID = source.ID WHERE doc.DOCTYPE = 'Tax return' AND doc.JAAR = source.FINANCIALYEAR;");
    // Add filingdate to tax returns
    $results[] = $pdo->exec("UPDATE oft_tax as source JOIN documentbeheer as doc on doc.TABELID = source.ID SET source.FILINGDATE = doc.DATUM WHERE source.FILINGDATE IS NULL");
    // Attach reports documents to annual accounts
    $results[] = $pdo->exec("UPDATE documentbeheer as doc JOIN oft_annual_accounts as source  ON doc.BEDRIJFSID = source.BEDRIJFSID SET doc.TABEL = 'oft_annual_accounts', doc.TABELID = source.ID WHERE doc.DOCTYPE = 'Annual accounts' AND doc.JAAR = source.FINANCIALYEAR");
    // Add filingdate to annual accounts
    $results[] = $pdo->exec("UPDATE oft_annual_accounts as source JOIN documentbeheer as doc on doc.TABELID = source.ID SET source.FILINGDATE = doc.DATUM WHERE source.FILINGDATE IS NULL");

    return $results;
}