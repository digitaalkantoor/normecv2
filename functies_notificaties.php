<?php
sercurityCheck();

// indien een entity opgegeven: alleen bedrijfsid van opgegeven enitty updaten
function notificaties_bijwerken($userid, $bedrijfsid, $entity = false)
{
    global $pdo;

    //Doorloop notificaties, en kijk of ze nog toe doen
    $query = $pdo->prepare('SELECT * FROM notificaties WHERE TABEL NOT LIKE "oft_entitie_accounting%" AND STATUS = "Active"');
    if ($entity) {
        $query = $pdo->prepare('SELECT * FROM notificaties WHERE TABEL NOT LIKE "oft_entitie_accounting%" AND STATUS = "Active" AND BEDRIJFSID = :bedrijfsid');
        $query->bindValue('bedrijfsid', $entity);
    }
    $query->execute();

    foreach ($query->fetchAll() as $data_not) {
        if (notificatie_checks($data_not['BEDRIJFSID'], $data_not['USERID'], $data_not['TABEL'],
            $data_not['TABELID'])) {
            //Als valide, dan notificatie uitzetten
            $query = $pdo->prepare('update notificaties set STATUS = "Passive", UPDATEDATUM   = "' . date("Y-m-d") . '" WHERE ID = :id;');
            $query->bindValue('id', $data_not["ID"]);
            $query->execute();
        }
    }

    $queryLike = $pdo->prepare('SELECT * FROM notificaties WHERE TABEL LIKE "oft_entitie_accounting%" AND STATUS = "Active"');
    if ($entity) {
        $queryLike = $pdo->prepare('SELECT * FROM notificaties WHERE TABEL LIKE "oft_entitie_accounting%" AND STATUS = "Active"  AND BEDRIJFSID = :bedrijfsid');
        $queryLike->bindValue('bedrijfsid', $entity);
    }

    $queryLike->execute();

    foreach ($queryLike->fetchAll() as $data_not) {
        $id = $data_not["ID"];
        if (!notificatie_checks_accounting($id, afterSubstring($data_not['TABEL'], "oft_entitie_accounting"))) {
            $queryUpdate = $pdo->prepare('update notificaties set STATUS = "Passive", UPDATEDATUM = "' . date("Y-m-d") . '" WHERE ID = :id;');
            $queryUpdate->bindValue('id', $data_not["ID"]);
            $queryUpdate->execute();
        }
    }

    //Kijk of er nieuwe notificaties gemaakt moeten worden.
    notificatie_renew($bedrijfsid, $userid, "oft_tax", $entity);
    notificatie_renew($bedrijfsid, $userid, "oft_annual_accounts", $entity);
    notificatie_renew($bedrijfsid, $userid, "oft_entitie_accounting", $entity);
    //notificatie_renew($bedrijfsid, $userid, "oft_bedrijf_adres", $entity); // End date address

    //if(isRechten("PERSONEELSGEG")) {
    //notificatie_renew($bedrijfsid, $userid, "personeel", $entity); //Re-election date // zit niet in personeel
    //}
    //if(isRechten("CONTRACTEN")) { rechten verwijdered... weergave heeft ook geen rechten, lijkt nu net of het niet werkt
    notificatie_renew($bedrijfsid, $userid, "contracten", $entity); // Due date contract
    //}
    notificatie_renew($bedrijfsid, $userid, "oft_management", $entity); // re-election date

    //FINANCIALYEAR
    db_query("update notificaties, oft_tax set notificaties.FINANCIALYEAR = oft_tax.FINANCIALYEAR where notificaties.TABELID = oft_tax.ID AND notificaties.TABEL = 'oft_tax';",
        $pdo);
    db_query("update notificaties, oft_annual_accounts set notificaties.FINANCIALYEAR = oft_annual_accounts.FINANCIALYEAR where notificaties.TABELID = oft_annual_accounts.ID AND notificaties.TABEL = 'oft_annual_accounts';",
        $pdo);
}


function notificatie_checks($bedrijfsid, $userid, $tabel, $tabelId, $data = '')
{
    global $pdo;

    //TODO: niet checken de notificaties die je al gehad hebt.

    if ($data == '' && $tabelId > 0) {
        $tableData = getTableData($tabel);
        if (empty($tableData)) {
            return;
        }

        $query = $pdo->prepare('SELECT * FROM ' . $tabel . ' WHERE ID = :id limit 1;');
        $query->bindValue('id', $tabelId);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
    }

    $return = true;

    if ($data != '') {
        $MAAND = "2";
        $DAG = "0";
        $query = $pdo->prepare('SELECT * FROM notificatie_schema WHERE TABEL = :tabel limit 1;');
        $query->bindValue('tabel', $tabel);
        $query->execute();
        $dSchema = $query->fetch(PDO::FETCH_ASSOC);
        $status = isset($data['STATUS']) ? strtolower($data['STATUS']) : "";


        if ($dSchema !== false) {
            $MAAND = $dSchema["MAAND"];
            $DAG = $dSchema["DAG"];
        }


        if ($tabel == 'oft_tax' || $tabel == 'oft_annual_accounts') {
            /*
            @@@46;;;Financial year;;;=;;;2010;;;hard;;;stoptruefalse;;;
            @@@46;;;Alternative due date;;;>;;;[TODAY];;;stap_1_4;;;stoptruefalse;;;
            @@@46;;;Standard due date;;;>;;;[TODAY];;;stap_2_4;;;stoptrue;;;
            @@@46;;;Date;;;!=;;;[EMPTY];;;stap_3_4;;;stopfalse;;;
            @@@46;;;Standard due date;;;>;;;[FIELD]Date;;;stap_4_4;;;stoptruefalse;;;

            @@@48;;;Financial year;;;=;;;2010;;;hard;;;stoptruefalse;;;
            @@@48;;;Status;;;=;;;Final;;;stap_1_5;;;stoptruefalse;;;
            @@@48;;;Alternative due date;;;>;;;[TODAY];;;stap_2_5;;;stoptruefalse;;;
            @@@48;;;Standard due date;;;>;;;[TODAY];;;stap_3_5;;;stoptrue;;;
            @@@48;;;Date;;;!=;;;[EMPTY];;;stap_4_5;;;stopfalse;;;
            @@@48;;;Standard due date;;;>;;;[FIELD]Date;;;stap_5_5;;;stoptruefalse;;;

            @@@47;;;Financial year;;;=;;;2010;;;hard;;;stoptruefalse;;;
            */
            if ($status != 'final' && $status != 'not required') { // && $data["FINANCIALYEAR"] == date("Y")
                $vervaldatum = $data["EXTENTION"];
                if ($vervaldatum == "" || $vervaldatum == "0000-00-00") {
                    $vervaldatum = $data["STANDARDDUEDATE"];
                }
                if ($vervaldatum != "" && $vervaldatum != "0000-00-00") {
                    $query = $pdo->prepare('update notificaties set VERVALDATUM = :vervaldatum WHERE TABEL = :tabel and TABELID = :tabelid;');
                    $query->bindValue('vervaldatum', $vervaldatum);
                    $query->bindValue('tabel', $tabel);
                    $query->bindValue('tabelid', $tabelId);
                    $query->execute();
                }

                if (date("Y-m-d", strtotime("+" . $MAAND . " month +" . $DAG . " day", time())) > date("Y-m-d",
                        strtotime($vervaldatum))) {
                    $return = false;
                }
            } else {
                $query = $pdo->prepare('update notificaties set DELETED = "Ja" WHERE TABEL = :tabel and TABELID = :tabelid;');
                $query->bindValue('tabel', $tabel);
                $query->bindValue('tabelid', $tabelId);
                $query->execute();
            }
        } elseif ($tabel == 'contracten') {
            //echo date("Y-m-d", strtotime("+".$MAAND." month +".$DAG." day", time()));
            if (date("Y-m-d", strtotime("+" . $MAAND . " month +" . $DAG . " day", time())) > date("Y-m-d",
                    strtotime($data["DATUMSTOP"]))) {
                $query = $pdo->prepare('update notificaties set VERVALDATUM = :vervaldatum WHERE TABEL = :tabel and TABELID = :tabelid;');
                $query->bindValue('vervaldatum', $data["DATUMSTOP"]);
                $query->bindValue('tabel', $tabel);
                $query->bindValue('tabelid', $tabelId);
                $query->execute();

                $return = false;
            }
        } elseif ($tabel == 'oft_bedrijf_adres') {
            if (date("Y-m-d", strtotime("+" . $MAAND . " month +" . $DAG . " day", time())) > date("Y-m-d",
                    strtotime($data["STOPDATE"]))) {
                $query = $pdo->prepare('update notificaties set VERVALDATUM = :vervaldatum WHERE TABEL = :tabel and TABELID = :tabelid;');
                $query->bindValue('vervaldatum', $data["STOPDATE"]);
                $query->bindValue('tabel', $tabel);
                $query->bindValue('tabelid', $tabelId);
                $query->execute();

                $return = false;
            }
        } elseif ($tabel == 'personeel') {
            if (date("Y-m-d", strtotime("+" . $MAAND . " month +" . $DAG . " day", time())) > date("Y-m-d",
                    strtotime($data["TOTWANNEERCONTRACT"]))) {
                $query = $pdo->prepare('update notificaties set VERVALDATUM = :vervaldatum WHERE TABEL = :tabel and TABELID = :tabelid;');
                $query->bindValue('vervaldatum', $data["TOTWANNEERCONTRACT"]);
                $query->bindValue('tabel', $tabel);
                $query->bindValue('tabelid', $tabelId);
                $query->execute();

                $return = false;
            }
        } elseif ($tabel == 'oft_management') {
            if (date("Y-m-d", strtotime("+" . $MAAND . " month +" . $DAG . " day", time())) > date("Y-m-d",
                    strtotime($data["DATEOFREELECTION"]))) {
                $query = $pdo->prepare('update notificaties set VERVALDATUM = :vervaldatum WHERE TABEL = :tabel and TABELID = :tabelid;');
                $query->bindValue('vervaldatum', $data["DATEOFREELECTION"]);
                $query->bindValue('tabel', $tabel);
                $query->bindValue('tabelid', $tabelId);
                $query->execute();

                $return = false;
            }
        }
    }

    return $return;
}

function notificatie_checks_accounting($id, $fieldData)
{
    global $pdo;

    $MAAND = "2";
    $DAG = "0";
    $query = $pdo->prepare('SELECT * FROM notificatie_schema WHERE TABEL = "oft_entitie_accounting" limit 1;');
    $query->execute();
    $dSchema = $query->fetch(PDO::FETCH_ASSOC);

    if ($dSchema !== false) {
        $MAAND = $dSchema["MAAND"];
        $DAG = $dSchema["DAG"];
    }

    if (date("Y-m-d", strtotime("+" . $MAAND . " month +" . $DAG . " day", time())) > date("Y-m-d",
            strtotime($fieldData))) {
        $query = $pdo->prepare('update notificaties set VERVALDATUM = :vervaldatum WHERE TABEL = "oft_entitie_accounting" and TABELID = :tabelid;');
        $query->bindValue('vervaldatum', $fieldData);
        $query->bindValue('tabelid', $id);
        $query->execute();
        return false;
    }

    return true;
}

// Toegevoegd: alleen één enitteit vernieuwen
function notificatie_renew($bedrijfsid, $userid, $tabel, $entity = false)
{
    global $pdo;

    $tableData = getTableData($tabel);
    if (empty($tableData)) {
        return;
    }

    $sql = "SELECT * FROM $tabel";
    $skipTables = array('oft_entitie_accounting');
    if (!in_array($tabel, $skipTables)) {
        $sql .= ' where (NOT DELETED = \'Ja\' OR DELETED is null)';
        if ($entity) {
            $sql .= " AND BEDRIJFSID = :entity";
        }
    } elseif ($entity) {
        $sql .= " WHERE BEDRIJFSID = :entity";
    }


    $query = $pdo->prepare($sql);

    if ($entity) {
        $query->bindValue('entity', $entity);
    }

    $query->execute();

    foreach ($query->fetchAll() as $data_not) {
        if (!notificatie_checks($data_not["BEDRIJFSID"], $userid, $tabel, $data_not["ID"], $data_not)) {
            //Action false, make insert

            $event = "";
            $insert = true;
            if ($tabel == 'contracten') {
                if ($data_not["HOE"] == "Ja") {
                    $vervaldatum = $data_not["DATUMSTOP"];
                    $event = "Contract";
                }
                /*} elseif($tabel == 'personeel') {
                  $vervaldatum = $data_not["TOTWANNEERCONTRACT"];
                  $event = "Due date contract"; */ // Dit stond hier als re-election date, maar re-election date zit in oft_management
            } elseif ($tabel == 'oft_management') {
                $vervaldatum = $data_not["DATEOFREELECTION"];
                $event = "Reelection date";
            } elseif ($tabel == 'oft_bedrijf_adres') {
                $vervaldatum = $data_not["STOPDATE"];
                $event = "End date address";
            } elseif ($tabel == 'oft_tax' || $tabel == 'oft_annual_accounts') {
                $vervaldatum = $data_not["EXTENTION"];
                if (!$vervaldatum || $vervaldatum == "" || $vervaldatum == "0000-00-00") {
                    $vervaldatum = $data_not["STANDARDDUEDATE"];
                }
                if ($tabel == 'oft_tax') {
                    $event = "Tax";
                } else {
                    $event = "Annual accounts";
                }
                $insert = $data_not["STATUS"] ? false : true;
            }
            if ($insert && $event != "" && $vervaldatum != "") {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel, $data_not["ID"], $event, $vervaldatum);
            }
        }

        if ($tabel == 'oft_entitie_accounting') {
            $id = $data_not["ID"];
            if (!notificatie_checks_accounting($id, $data_not["BAS"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "BAS", $id,
                    'Business Activity Statements - Filing Due Date', $data_not["BAS"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["BASPAYMENT"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "BASPAYMENT", $id,
                    'Business Activity Statements - Payment Due Date', $data_not["BASPAYMENT"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["ESSFILING"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "ESSFILING", $id,
                    'ESS (Employment Share Scheme) - Filing Due Date', $data_not["ESSFILING"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["FBTFILING"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "FBTFILING", $id,
                    'FBT (Fringe Benefit Tax) - Filing Due Date', $data_not["FBTFILING"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["FBTPAYMENT"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "FBTPAYMENT", $id,
                    'FBT (Fringe Benefit Tax) - Payment Due Date', $data_not["FBTPAYMENT"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["PAYROLLTAXEXPIRES"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "PAYROLLTAXEXPIRES", $id,
                    'Payroll Tax - Expires Date', $data_not["PAYROLLTAXEXPIRES"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["WORKERSCOMPEXPIRES_VIC"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "WORKERSCOMPEXPIRES_VIC", $id,
                    'Workers Comp (State : VIC) - Expires Date', $data_not["WORKERSCOMPEXPIRES_VIC"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["WORKERSCOMPEXPIRES_NSW"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "WORKERSCOMPEXPIRES_NSW", $id,
                    'Workers Comp (State : NSW) - Expires Date', $data_not["WORKERSCOMPEXPIRES_NSW"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["WORKERSCOMPEXPIRES_QLD"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "WORKERSCOMPEXPIRES_QLD", $id,
                    'Workers Comp (State : QLD) - Expires Date', $data_not["WORKERSCOMPEXPIRES_QLD"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["WORKERSCOMPEXPIRES_TAS"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "WORKERSCOMPEXPIRES_TAS", $id,
                    'Workers Comp (State : TAS) - Expires Date', $data_not["WORKERSCOMPEXPIRES_TAS"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["WORKERSCOMPEXPIRES_WA"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "WORKERSCOMPEXPIRES_WA", $id,
                    'Workers Comp (State : WA) - Expires Date', $data_not["WORKERSCOMPEXPIRES_WA"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["WORKERSCOMPEXPIRES_SA"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "WORKERSCOMPEXPIRES_SA", $id,
                    'Workers Comp (State : SA) - Expires Date', $data_not["WORKERSCOMPEXPIRES_SA"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["INCOMETAXFILING"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "INCOMETAXFILING", $id,
                    'Income Tax Return - Filing Due Date', $data_not["INCOMETAXFILING"]);
            }
            if (!notificatie_checks_accounting($id, $data_not["INCOMETAXPAYMENT"])) {
                notificatie_insert($data_not["BEDRIJFSID"], $userid, $tabel . "INCOMETAXPAYMENT", $id,
                    'Income Tax Return - Payment Due Date', $data_not["INCOMETAXPAYMENT"]);
            }
        }
    }
}

function notificatie_insert($bedrijfsid, $userid, $tabel, $tabelid, $event, $vervaldatum)
{
    global $pdo;

    if ($vervaldatum != "0000-00-00" && $vervaldatum != '' && $vervaldatum != null) {
        //Monitoring staat aan.
        $query = $pdo->prepare('SELECT ID FROM bedrijf WHERE ID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null) AND (ENDDATUM = "0000-00-00" OR ENDDATUM is null OR ENDDATUM = "") AND MONITOR = "Ja" limit 1;');
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $query->execute();


        if ($query->rowCount() == 1) {
            //Hij mag niet al aamgemaakt zijn.
            $querySelect = $pdo->prepare('SELECT ID, VERVALDATUM FROM notificaties WHERE TABEL = :tabel AND TABELID = :tabelid AND STATUS = "Active";');
            $querySelect->bindValue('tabel', $tabel);
            $querySelect->bindValue('tabelid', $tabelid);
            $querySelect->execute();

            if ($querySelect->rowCount() == 0) {
                //Invoeren notificatie
                $queryInsert = $pdo->prepare('insert into notificaties set BEDRIJFSID = :bedrijfsid,
                                                       NOTIFICATIENR = :notificatienr,
                                                       USERID        = :userid,
                                                       TABEL         = :tabel,
                                                       TABELID       = :tabelid,
                                                       EVENT         = :event,
                                                       STATUS        = "Active",
                                                       STARTDATUM    = "' . date("Y-m-d") . '",
                                                       UPDATEDATUM   = "' . date("Y-m-d") . '",
                                                       DELETED       = "No",
                                                       VERVALDATUM   = :vervaldatum;');

                $notificatienr = getNieuwNr("notificaties", "NOTIFICATIENR", $bedrijfsid);

                $queryInsert->bindValue('bedrijfsid', $bedrijfsid);
                $queryInsert->bindValue('notificatienr', $notificatienr);
                $queryInsert->bindValue('userid', $userid);
                $queryInsert->bindValue('tabel', $tabel);
                $queryInsert->bindValue('tabelid', $tabelid);
                $queryInsert->bindValue('event', $event);
                $queryInsert->bindValue('vervaldatum', $vervaldatum);
                return $queryInsert->execute();
            } elseif ($event == 'Tax' || $event == 'Annual accounts') {
                foreach ($querySelect->fetchAll(PDO::FETCH_ASSOC) as $row) {
                    if ($row['VERVALDATUM'] != $vervaldatum) {
                        $queryUpdate = $pdo->prepare('UPDATE notificaties SET USERID = :userid, UPDATEDATUM = "' . date('Y-m-d') . '", VERVALDATUM = :vervaldatum WHERE ID = :id');
                        $queryUpdate->bindValue('userid', $userid);
                        $queryUpdate->bindValue('vervaldatum', $vervaldatum);
                        $queryUpdate->bindValue('id', $row['ID']);
                        $queryUpdate->execute();
                    }
                }
            }
        }
    }
}

function afterSubstring($string, $after)
{
    return substr($string, strpos($string, $after) + strlen($after));
}
