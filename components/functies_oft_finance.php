<?php
sercurityCheck();

function getDoctypesBySection($section, $excludeContracts = true) {
  global $pdo;

  $sql = "";

  if($section != '') {
    $query = $pdo->prepare('select NAAM from stam_documenttype where SECTIE LIKE :section AND DELETED = "Nee"');
    $likesection = '%' . $section . '%';
    $query->bindValue('section', $likesection);
    $query->execute();

    foreach ($query->fetchAll() as $document_from_type) {
      //TODO oplossing maken naar PDO!!
      $sql .= "DOCTYPE = '".ps($document_from_type["NAAM"])."' OR ";
    }
  
    if($sql != "") {
      $sql = substr($sql, 0, (strlen($sql)-3));
      $sql = "AND ($sql)";
      if($excludeContracts) {
        $sql .= " AND NOT DOCTYPE = 'Contract'"; //TODO, moeten deze documenten er bij? AND NOT DOCTYPE = 'Board resolution'
      }
    }
  }
  
  return $sql;
}

function oft_finance($userid, $bedrijfsid) {
  $_REQUEST["SRCH_YEAR"] = "x".date("Y");
  $_REQUEST["SRCH_JAAR"] = date("Y");

  $titel           = tl('Finance');
  $tabel           = "documentbeheer";
  $toevoegen       = "content.php?SITE=oft_document_add&SECTIE=Finance";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
  $verwijderen     = "";
  $submenuitems    = "<a href=\"content.php?SITE=dragAndDrop&SECTION=oft_finance\">Upload multiple documents</a>  | ". oft_inputform_link($toevoegen, "New document");

  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayZoekvelden");

  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, getDoctypesBySection("Finance"));

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_finance_ajax($userid, $bedrijfsid) {
  $tabel           = "documentbeheer";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_finance_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
  $verwijderen     = "";

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, getDoctypesBySection("Finance")); 
}

function oft_entitie_finance($userid, $bedrijfsid) {

  echo oft_standaard_menu_page($userid, $bedrijfsid, "Finance", "oft_entitie_finance", "documentbeheer", "BESTANDSNAAM", oft_back_button_entity(), "Add document", "");
}

function oft_entitie_finance_ajax($userid, $bedrijfsid) {

  $tabel           = "documentbeheer";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_entitie_finance_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_entitie_finance_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_entitie_finance_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_entitie_finance_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_document_add&SECTIE=Finance";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
  $verwijderen     = "";

  $sql_toevoeging  = "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."' ". getDoctypesBySection("Finance");

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, $sql_toevoeging);
}