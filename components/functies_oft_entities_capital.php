<?php
sercurityCheck();

function oft_entitie_capital($userid, $bedrijfsid) {
  global $pdo;

  $MENUID          = getMenuItem();

  $titel           = oft_titel_entities($bedrijfsid);
  $submenuitems    = "";
  if(isRechtenOFT($bedrijfsid, "CRM")) {
    $submenuitems    = "<a href=\"content.php?SITE=oft_entitie_capital_edit&MENUID=$MENUID&BID=$bedrijfsid\">Add changes</a>";
  }
  $back_button     = oft_back_button_entity();

  $contentFrame    = "<h2>Capital</h2><br/><br/>
                      <table class=\"oft_tabel\" width=\"50%\">";

  $AUTHORIZED      = 0; $VALUTA1 = "Euro";
  $ISSUED          = 0; $VALUTA2 = "Euro";
  $PAIDUP          = 0; $VALUTA3 = "Euro";
  $queryCapital = $pdo->prepare('SELECT * FROM oft_capital where BEDRIJFSID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null);');
  $queryCapital->bindValue('bedrijfsid', $bedrijfsid);
  $queryCapital->execute();
  $dCapital = $queryCapital->fetch(PDO::FETCH_ASSOC);

  if($dCapital !== false)
  {
    $AUTHORIZED    = $dCapital["AUTHORIZED"]; $VALUTA1 = $dCapital["VALUTA1"];
    $ISSUED        = $dCapital["ISSUED"];     $VALUTA2 = $dCapital["VALUTA2"];
    $PAIDUP        = $dCapital["PAIDUP"];     $VALUTA3 = $dCapital["VALUTA3"];
  }

  $contentFrame   .= "<tr><td class=\"oft_label\">Authorized</td><td align=\"right\" style=\"padding: 0px 20px 0 20px;\">".prijsconversiePuntenKommas($AUTHORIZED)."</td><td>".getNaam("valuta", $VALUTA1, "NAAM")."</td></tr>";
  $contentFrame   .= "<tr><td><br/></td></tr>";
  $contentFrame   .= "<tr><td class=\"oft_label\">Issued</td><td align=\"right\" style=\"padding: 0px 20px 0 20px;\">".prijsconversiePuntenKommas($ISSUED)."</td><td>".getNaam("valuta", $VALUTA2, "NAAM")."</td></tr>";
  $contentFrame   .= "<tr><td><br/></td></tr>";
  $contentFrame   .= "<tr><td class=\"oft_label\">Paid up</td><td align=\"right\" style=\"padding: 0px 20px 0 20px;\">".prijsconversiePuntenKommas($PAIDUP)."</td><td>".getNaam("valuta", $VALUTA3, "NAAM")."</td></tr>";
  $contentFrame   .= "</table><br/>";

  $contentFrame   .= "<h2>Classes</h2>";

  $tabel           = "oft_classes";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  $contentFrame   .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'TYPE', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");

  $contentFrame   .= oft_capital_warning($bedrijfsid, $userid);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
} 

function oft_entitie_capital_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_classes";
  $toevoegen       = "";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_".$tabel."_edit&ID=-ID-");
  $verwijderen     = "";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'TYPE', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");
}

function oft_entitie_capital_edit($userid, $bedrijfsid, $wizzard = "Nee") {
  global $pdo;

  $MENUID          = getMenuItem();

  $titel           = oft_titel_entities($bedrijfsid);
  $submenuitems    = '';
  $back_button     = "";
  if($wizzard == "Nee") {
    $back_button   = oft_back_button_entity();
  }
  
  $tabel           = "oft_classes";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  if(isset($_REQUEST["CHANGE"]) || isset($_REQUEST["NEXTST"])) {
    $queryCapital = $pdo->prepare('select * from oft_capital where BEDRIJFSID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null) limit 1;');
    $queryCapital->bindValue('bedrijfsid', $bedrijfsid);
    $queryCapital->execute();
    $rCapital = $queryCapital->fetch(PDO::FETCH_ASSOC);

    if($rCapital == false)
    {
      $query = $pdo->prepare("INSERT INTO oft_capital
                SET UPDATEDATUM  = :updatedatum,
                    DELETED      = 'Nee',
                    BEDRIJFSID   = :bedrijfsid,
                    AUTHORIZED   = :authorized,
                    ISSUED       = :issued,
                    PAIDUP       = :paidup,
                    VALUTA1      = :valuta1,
                    VALUTA2      = :valuta2,
                    VALUTA3      = :valuta3");
      $query->bindValue('updatedatum', date("Y-m-d"));
      $query->bindValue('bedrijfsid', $bedrijfsid);
      $query->bindValue('authorized', $_REQUEST["oft_capital_AUTHORIZED"]);
      $query->bindValue('issued', $_REQUEST["oft_capital_ISSUED"]);
      $query->bindValue('paidup', $_REQUEST["oft_capital_PAIDUP"]);
      $query->bindValue('valuta1', ($_REQUEST["oft_capital_VALUE1"]*1));
      $query->bindValue('valuta2', ($_REQUEST["oft_capital_VALUE1"]*1));
      $query->bindValue('valuta3', ($_REQUEST["oft_capital_VALUE1"]*1));
      $query->execute();
    } else {
      $query = $pdo->prepare('update oft_capital
                  set UPDATEDATUM  = :updatedatum,
                      AUTHORIZED   = :authorized,
                      ISSUED       = :issued,
                      PAIDUP       = :paidup,
                      VALUTA1      = :valuta1,
                      VALUTA2      = :valuta2,
                      VALUTA3      = :valuta3
                  WHERE BEDRIJFSID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null);');
      $query->bindValue('updatedatum', date("Y-m-d"));
      $query->bindValue('authorized', $_REQUEST["oft_capital_AUTHORIZED"]);
      $query->bindValue('issued', $_REQUEST["oft_capital_ISSUED"]);
      $query->bindValue('paidup', $_REQUEST["oft_capital_PAIDUP"]);
      $query->bindValue('valuta1', ($_REQUEST["oft_capital_VALUE1"]*1));
      $query->bindValue('valuta2', ($_REQUEST["oft_capital_VALUE1"]*1));
      $query->bindValue('valuta3', ($_REQUEST["oft_capital_VALUE1"]*1));
      $query->bindValue('bedrijfsid', $bedrijfsid);
      $query->execute(); 
    }
    
    oft_tabel_content_save($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_classes", "ISSUEDQUANTITY");

    if(isset($_REQUEST["NEXT_STEP"])) {
      $BID = $bedrijfsid;
      if(isset($_REQUEST["BID"])) {
        $BID = "&BID=".$_REQUEST["BID"];
      }
      rd($_REQUEST["NEXT_STEP"].$BID);
    } else {
      rd("content.php?SITE=oft_entitie_capital&MENUID=$MENUID&BID=$bedrijfsid");
    }
  }

  $contentFrame    = "<form action=\"\" method=\"POST\">";
  $contentFrame   .= "<h2>Capital</h2>";
  $contentFrame   .= "<br/><br/>";
  $contentFrame   .= "<table class=\"oft_tabel\" width=\"50%\">";

  $AUTHORIZED      = 0; $VALUTA1 = "Euro";
  $ISSUED          = 0; $VALUTA2 = "Euro";
  $PAIDUP          = 0; $VALUTA3 = "Euro";
  $query = $pdo->prepare('select * from oft_capital where BEDRIJFSID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null) limit 1;');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->execute();
  $dCapital = $query->fetch(PDO::FETCH_ASSOC);

  if($dCapital !== false)
  {
    $AUTHORIZED    = $dCapital["AUTHORIZED"]; $VALUTA1 = $dCapital["VALUTA1"];
    $ISSUED        = $dCapital["ISSUED"];     $VALUTA2 = $dCapital["VALUTA2"];
    $PAIDUP        = $dCapital["PAIDUP"];     $VALUTA3 = $dCapital["VALUTA3"];
  }

  $contentFrame   .= "<tr><td class=\"oft_label\">Authorized</td><td align=\"right\" style=\"padding: 0px 20px 0 20px;\"><input class=\"fieldHalf\" type=\"text\" name=\"oft_capital_AUTHORIZED\" value=\"".$AUTHORIZED."\" /></td><td><select class=\"fieldHalf\" name=\"oft_capital_VALUE1\">". selectbox_tabel("valuta", "ID", "NAAM", $VALUTA1, false, '')."</select></td></tr>";
  $contentFrame   .= "<tr><td><br/></td></tr>";
  $contentFrame   .= "<tr><td class=\"oft_label\">Issued</td><td align=\"right\" style=\"padding: 0px 20px 0 20px;\"><input class=\"fieldHalf\" type=\"text\" name=\"oft_capital_ISSUED\" value=\"".$ISSUED."\" /></td><td>".getNaam("valuta", $VALUTA2, "NAAM")."</td></tr>";
  $contentFrame   .= "<tr><td><br/></td></tr>";
  $contentFrame   .= "<tr><td class=\"oft_label\">Paid up</td><td align=\"right\" style=\"padding: 0px 20px 0 20px;\"><input class=\"fieldHalf\" type=\"text\" name=\"oft_capital_PAIDUP\" value=\"".$PAIDUP."\" /></td><td>".getNaam("valuta", $VALUTA3, "NAAM")."</td></tr>";
  $contentFrame   .= "</table><br/><Br/>";

  $contentFrame   .= "<h2>Classes</h2>";

  $contentFrame   .= oft_tabel_content_insert($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'TYPE', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");

  $contentFrame   .= oft_capital_warning($bedrijfsid, $userid);

  if($wizzard == "Ja") { 
    $contentFrame   .= "<br/><Br/><div align=\"right\">
                          <input type=\"hidden\" name=\"PREV_STEP\" value=\"content.php?SITE=oft_entities_add_3\" />
                          <input type=\"hidden\" name=\"NEXT_STEP\" value=\"content.php?SITE=oft_entities_add_5\" />
                          <input type=\"submit\" name=\"PREVIOUS\" value=\"Back\" class=\"button\" />
                          <input type=\"submit\" name=\"NEXTST\" value=\"Next\" class=\"button\" />
                          <input type=\"button\" class=\"cancelbutton\" onClick=\"window.location = 'content.php?SITE=oft_entitie_capital&MENUID=$MENUID&BID=$bedrijfsid';\" value=\"Cancel\" name=\"ANNULEREN\" />
                        </div>";
  } else {
    $contentFrame   .= "<br/><Br/><div align=\"right\">
                          <input type=\"submit\" name=\"CHANGE\" value=\"Save\" class=\"button\" />
                          <input type=\"button\" class=\"cancelbutton\" onClick=\"window.location = 'content.php?SITE=oft_entitie_capital&MENUID=$MENUID&BID=$bedrijfsid';\" value=\"Cancel\" name=\"ANNULEREN\" />
                        </div>";
  }
  $contentFrame   .= "</form>";

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}