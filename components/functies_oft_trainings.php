<?php
sercurityCheck();

/* Non Audit */
function oft_trainingsschema($userid, $bedrijfsid) {
  global $db;

  $titel         = tl('Training');
  $submenuitems  = oft_inputform_link('content.php?SITE=oft_trainingsschema_add', "Add training", 500)
                   . ' | '. "<a href=\"content.php?SITE=oft_trainingsschema_print&EXPORTEXCEL=true&PAGNR=-1\" target=\"_blank\">Excel</a>"
                   . ' | '. "<a class=\"cursor\" onClick=\"window.location.href='content.php?SITE=oft_trainingsschema_print&PRINT=true&PAGNR=-1&SRCH_SEARCH=x'+encodeURIComponent(document.getElementById('SRCH_SEARCH').value)+'&SRCH_LAND=x'+document.getElementById('SRCH_LAND').value;\" target=\"_blank\">Printable version</a>";
  $back_button   = "";

  $tabel           = "oft_trainingsschema";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_trainingsschema";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_trainingsschema_edit&ID=-ID-");
  $verwijderen     = "";
  
  $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_trainingsschema", 'ID DESC', $toevoegen, $bewerken, $verwijderen);

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_trainingsschema_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_trainingsschema";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_trainingsschema_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_trainingsschema_edit&ID=-ID-");
  $verwijderen     = "";
  
  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen);
}

function oft_trainingsschema_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_trainingsschema";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_trainingsschema_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_trainingsschema_edit&ID=-ID-");
  $verwijderen     = "";

  echo "<h1>Training</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_trainingsschema", 'ID DESC', $toevoegen, $bewerken, $verwijderen);
}

function oft_trainingsschema_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_trainingsschema.htm";
  $tabel = "oft_trainingsschema";

  echo "<h2>Add training</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_trainingsschema_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_trainingsschema.htm";
  $tabel = "oft_trainingsschema";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit training</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_trainingsschema_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_trainingsschema_print.htm";
  $tabel = "oft_trainingsschema";

  $id = "";
  if(isset($_REQUEST["ID"])) { 
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print training</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}
