<?php
sercurityCheck();

/* Non Audit */
function oft_compliance_chart($userid, $bedrijfsid) {
  global $db;

  $titel         = tl('Risk Assessment');
  $urltoevEx = "";
  if (isset($_REQUEST['SRCH_SEARCH'])) $urltoevEx .= "&SRCH_SEARCH=".$_REQUEST['SRCH_SEARCH'];
  if (isset($_REQUEST['SRCH_LAND'])) $urltoevEx .= "&SRCH_LAND=".$_REQUEST['SRCH_LAND'];
  if (isset($_REQUEST['SRCH_stam_risklevel'])) $urltoevEx .= "&SRCH_stam_risklevel=".$_REQUEST['SRCH_stam_risklevel'];
  if (isset($_REQUEST['SRCH_stam_compliantstatus'])) $urltoevEx .= "&SRCH_stam_compliantstatus=".$_REQUEST['SRCH_stam_compliantstatus'];

  $urltoev = "&SRCH_SEARCH=x'+document.getElementById('SRCH_SEARCH').value+'&SRCH_LAND=x'+document.getElementById('SRCH_LAND').value+'&SRCH_stam_risklevel=x'+document.getElementById('SRCH_stam_risklevel').value+'&SRCH_stam_compliantstatus=x'+document.getElementById('SRCH_stam_compliantstatus').value;\"";
  $submenuitems  = oft_inputform_link('content.php?SITE=oft_compliance_chart_add', "Add risk area", 500)
                   . ' | '. "<a class=\"cursor\" onClick=\"window.location.href='content.php?SITE=oft_compliance_chart_print&PRINT=true&PAGNR=-1$urltoev\" target=\"_blank\">Printable version</a>";
  $back_button   = "";

  if(isset($_REQUEST["EXPORTEXCEL"])) {
    $tabel = "oft_compliance_chart";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_compliance_chart_excel", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_compliance_chart_excel", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_compliance_chart_excel", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_compliance_chart_excel", "arrayZoekvelden");
  } else {
    $tabel           = "oft_compliance_chart";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");
  }

  $toevoegen       = "content.php?SITE=oft_compliance_chart";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_compliance_chart_edit&ID=-ID-");
  $verwijderen     = "";
  
  $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_compliance_chart", 'ID DESC', $toevoegen, $bewerken, $verwijderen);

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_compliance_chart_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_compliance_chart";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_compliance_chart_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_compliance_chart_edit&ID=-ID-");
  $verwijderen     = "";
  
  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen);
}

function oft_compliance_chart_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_compliance_chart";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_compliance_chart_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_compliance_chart_edit&ID=-ID-");
  $verwijderen     = "";

  echo "<h1>Risk Assessment</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_compliance_chart", 'ID DESC', $toevoegen, $bewerken, $verwijderen);
}

function oft_compliance_chart_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_compliance_chart.htm";
  $tabel = "oft_compliance_chart";

  echo "<h2>Add risk area</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_compliance_chart_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_compliance_chart.htm";
  $tabel = "oft_compliance_chart";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Risk Assessment</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_compliance_chart_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_compliance_chart_print.htm";
  $tabel = "oft_compliance_chart";

  $id = "";
  if(isset($_REQUEST["ID"])) { 
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Risk Assessment</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}
