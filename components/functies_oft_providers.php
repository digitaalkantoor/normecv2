<?php
sercurityCheck();

function oft_providers($user, $company)
{
    $title = "Corporate providers";
    $submenuItems = oft_inputform_link('content.php?SITE=oft_providers_add', "Add item");
    $backButton = "";
    $contentFrame = oft_providers_ajax($user, $company, true);
    echo oft_framework_menu($user, $company, $contentFrame, $title, $submenuItems, $backButton);
}

function oft_providers_ajax($user, $company, $return = false)
{
    $table = "oft_providers";
    $order = "TYPE";
    $fields = oft_get_structure($user, $company, "oft_providers_overzicht", "all");
    $sql = "AND BEDRIJFSID = '" . ps($company, "nr") . "'";
    $add = "content.php?SITE=oft_providers_add";
    $edit = oft_inputform_link_short("content.php?SITE=oft_providers_edit&ID=-ID-");
    $delete = "";
    if ($return) {
        return oft_tabel($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete, $sql);
    }
    echo oft_tabel_content($user, $company,
        $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
        $table, $order, $add, $edit, $delete, $sql);
    return true;
}

function oft_providers_print($user, $company)
{
    echo "<h1>Providers</h1><br/><br/>";
    $table = "oft_providers";
    $order = "ID DESC";
    $fields = oft_get_structure($user, $company, "oft_providers", "all");
    $sql = "AND BEDRIJFSID = '" . ps($company, "nr") . "'";
    echo oft_tabel_print($user, $company,
        $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
        $table, $order, "", "", "", $sql);
}

function oft_providers_add($user, $company)
{
    $templateFile = "./templates/oft_providers.htm";
    $table = "oft_providers";
    echo "<h2>Add provider</h2>";
    echo replace_template_content($user, $company, $table, $templateFile);
}

function oft_providers_edit($user, $company)
{
    $templateFile = "./templates/oft_providers.htm";
    $table = "oft_providers";
    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }
    echo "<h2>Edit provider</h2>";
    echo replace_template_content_edit($user, $company, $table, $templateFile, $id);
}

function oft_providers_1_print($user, $company)
{
    $templateFile = "./templates/oft_provider_print.htm";
    $table = "oft_providers";
    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }
    echo "<h2>Print provider</h2>";
    echo replace_template_content_edit($user, $company, $table, $templateFile, $id);
}