<?php
sercurityCheck();

function oft_entitie_compliance($userid, $bedrijfsid)
{

    $titel = oft_titel_entities($bedrijfsid);
    $submenuitems = "";
    if (isMaster()) {
        $submenuitems = oft_inputform_link('content.php?SITE=oft_compliance_change_settings&ID=' . $bedrijfsid,
            "Change settings");
    }
    $back_button = oft_back_button_entity();

    $contentFrame = oft_entitie_compliance_ajax($userid, $bedrijfsid, false);

    $contentFrame .= oft_document_legenda($userid, $bedrijfsid);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_compliance_ajax($user, $company, $echo = true)
{

    check_annual_accounts($user, $company, $company);

    $tableid = isset($_REQUEST['tableid']) ? $_REQUEST['tableid'] : false;
    $contentFrame = $tableid ? '' : "<h2>Compliance</h2>";

    $order = 'FINANCIALYEAR';
    $add = "";
    $delete = "";

    $table = "oft_tax";
    if (!$tableid || $tableid == $table) {

        $sql = "AND $table.BEDRIJFSID = '" . ps($company, "nr") . "'";
        $fields = oft_get_structure($user, $company, "oft_tax_overzicht", "all");
        $edit = oft_inputform_link_short("content.php?SITE=oft_tax_edit&ID=-ID-");

        $contentFrame .= $tableid ? '' : "<h3>Tax filings</h3><div id=\"contentTable_$table\">";
        $contentFrame .= oft_tabel($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete, $sql);
        $contentFrame .= $tableid ? '' : "</div>";
    }

    $table = "oft_annual_accounts";
    if (!$tableid || $tableid == $table) {

        $sql = "AND $table.BEDRIJFSID = '" . ps($company, "nr") . "'";
        $fields = oft_get_structure($user, $company, "oft_annual_accounts_overzicht", "all");
        $edit = oft_inputform_link_short("content.php?SITE=oft_annual_accounts_edit&ID=-ID-");

        $contentFrame .= $tableid ? '' : "<br/><h3>Annual accounts</h3><div id=\"contentTable_$table\">";
        $contentFrame .= oft_tabel_content($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete, $sql);
        $contentFrame .= $tableid ? '' : "</div>";
    }
    if (check('DIF', 'Nee', $company) == 'Nee') {

        $table = "oft_boardmeetings";
        if (!$tableid || $tableid == $table) {

            $sql = "AND $table.BEDRIJFSID = '" . ps($company, "nr") . "'";
            $fields = oft_get_structure($user, $company, "oft_boardmeetings_overzicht", "all");
            $edit = oft_inputform_link_short("content.php?SITE=oft_boardmeetings_edit&ID=-ID-");

            $contentFrame .= $tableid ? '' : "<br/><h3>Board meetings</h3><div id=\"contentTable_$table\">";
            $contentFrame .= oft_tabel_content($user, $company,
                $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
                $table, $order, $add, $edit, $delete, $sql);
            $contentFrame .= $tableid ? '' : "</div>";
        }
    }

    if (check('DIF', 'Nee', $company) != 'Ja') {
        $contentFrame .= "<br/><h3>Composition of Executive Board</h3>";
        $contentFrame .= oft_board_composition_warning($company, $user);
    }

    $contentFrame .= "<br/><br/>";

    if ($echo) {
        echo $contentFrame;
    }
    return $contentFrame;
}

function oft_tax($userid, $bedrijfsid)
{

    $titel = tl('Tax');
    $tabel = "oft_tax";
    $toevoegen = "";
    if (isRechtenOFT($bedrijfsid, "MASTER")) {
        $toevoegen = "content.php?SITE=" . $tabel . "_add";
        $submenuitems = oft_inputform_link($toevoegen, "Add tax");
    }
    $bewerken = oft_inputform_link_short("content.php?SITE=" . $tabel . "_edit&ID=-ID-");
    $verwijderen = "";

    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayZoekvelden");

    $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'FINANCIALYEAR', $toevoegen, $bewerken, $verwijderen);

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_tax_ajax($userid, $bedrijfsid)
{

    $tabel = "oft_tax";
    $toevoegen = "content.php?SITE=" . $tabel . "_add";
    $bewerken = oft_inputform_link_short("content.php?SITE=" . $tabel . "_edit&ID=-ID-");
    $verwijderen = "";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayZoekvelden");

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'FINANCIALYEAR', $toevoegen, $bewerken, $verwijderen);
}

function oft_tax_add($userid, $bedrijfsid)
{
    if (!isRechtenOFT($bedrijfsid, "MASTER")) {
        return;
    }

    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $templateFile = "./templates/oft_tax_dif.htm";
    } else {
        $templateFile = "./templates/oft_tax.htm";
    }
    $tabel = "oft_tax";

    echo "<h2>Add tax</h2>";

    echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_boardmeetings_edit($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_boardmeetings.htm";
    $tabel = "oft_boardmeetings";
    $titel = "Edit board meeting";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_tax_edit($userid, $bedrijfsid, $loginid)
{
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        if (getNaam("login", $loginid, "PROFIEL") == "Administrator") {
            $templateFile = "./templates/oft_tax_dif.htm";
        } else {
            $templateFile = "./templates/oft_tax_dif_rights.htm";
        }
    } else {
        $templateFile = "./templates/oft_tax.htm";
    }

    $tabel = "oft_tax";
    $titel = "Edit Tax filings";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_compliance_change_settings($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_compliance_change_settings.htm";
    $tabel = "bedrijf";
    $titel = "Change settings";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_annual_accounts($userid, $bedrijfsid)
{

    $titel = tl('Annual accounts');
    $tabel = "oft_annual_accounts";
    $toevoegen = "";
    if (isRechtenOFT($bedrijfsid, "MASTER")) {
        $toevoegen = "content.php?SITE=" . $tabel . "_add";
        $submenuitems = oft_inputform_link($toevoegen, "New annual account");
    }
    $bewerken = oft_inputform_link_short("content.php?SITE=" . $tabel . "_edit&ID=-ID-");
    $verwijderen = "";

    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayZoekvelden");

    $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'FINANCIALYEAR', $toevoegen, $bewerken, $verwijderen);

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_annual_accounts_ajax($userid, $bedrijfsid)
{

    $tabel = "oft_annual_accounts";
    $toevoegen = "content.php?SITE=" . $tabel . "_add";
    $bewerken = oft_inputform_link_short("content.php?SITE=" . $tabel . "_edit&ID=-ID-");
    $verwijderen = "";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht_all", "arrayZoekvelden");

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'FINANCIALYEAR', $toevoegen, $bewerken, $verwijderen);
}

function oft_annual_accounts_add($userid, $bedrijfsid)
{
    if (!isRechtenOFT($bedrijfsid, "MASTER")) {
        return;
    }

    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $templateFile = "./templates/oft_annual_accounts_dif.htm";
    } else {
        $templateFile = "./templates/oft_annual_accounts.htm";
    }
    $tabel = "oft_annual_accounts";

    echo "<h2>Add Annual Accounts</h2>";

    echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_annual_accounts_edit($userid, $bedrijfsid, $loginid)
{
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        if (getNaam("login", $loginid, "PROFIEL") == "Administrator") {
            $templateFile = "./templates/oft_annual_accounts_dif.htm";
        } else {
            $templateFile = "./templates/oft_annual_accounts_dif_rights.htm";
        }
    } else {
        $templateFile = "./templates/oft_annual_accounts.htm";
    }
    $tabel = "oft_annual_accounts";
    $titel = "Edit Annual Accounts";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}
