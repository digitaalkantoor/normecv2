<?php

/*
 * Function to return translation (vertaalDatabaseKolomNaarLeesbaar).
 * 
 * @param (string) $name The name of the Options List.
 * 
 * @return (string) Translation
 */
function vertaalDatabaseKolomNaarLeesbaar($userid, $bedrijfsid, $tabel, $kolom)
{
  $r = "";
  
  $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $combiArray = array();
  foreach($arrayVelden As $nr => $veldNaam) {
    if(isset($arrayVeldnamen[$nr])) {
      $combiArray[$veldNaam] = $arrayVeldnamen[$nr];
    }
  }
  
  if(isset($combiArray[$kolom])) {
    $r = $combiArray[$kolom];
  }
  
  $r = str_replace("Entities", check("default_name_of_entities", "Entities", 1), $r);
  $r = str_replace("Entity", check("default_name_of_entity", "Entity", 1), $r);

  return $r;
}