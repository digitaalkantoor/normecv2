<?php
sercurityCheck();

function rapport_magaging_director($userid, $bedrijfsid) {
  global $pdo;

  $titel         = "Management board";
  $submenuitems  = "";
  $back_button   = "";

  $contentFrame  = "";
  
  $_REQUEST["PAGNR"] = -1;

  $telEntiteiten = 0;

  $query = $pdo->prepare('select * from bedrijf where ID > 1 AND (NOT DELETED = "Ja" OR DELETED is null);');
  $query->execute();

  foreach ($query->fetchAll() as $dBedrijven) {
    $contentFrame   .= "<h2 class=\"border-bottom\">".$dBedrijven["BEDRIJFSNAAM"]."</h2>";
    //$contentFrame   .= "<p>".$dBedrijven["OMSCHRIJVING"]."</p>";
    //$contentFrame   .= "<h3 style=\"float:left; width: 200px; clear: none; display: block;\">Executive Board</h3>";
    //$contentFrame   .= "<div style=\"clear:both;\"></div>";
  
    $tabel           = "oft_management";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");
  
    $toevoegen       = "";
    $bewerken        = "";
    $verwijderen     = "";

    $contentFrame   .= oft_tabel($userid, $dBedrijven["ID"], $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'USERID', $toevoegen, $bewerken, $verwijderen, "AND TYPE = 'Executive Board' AND BEDRIJFSID = '".ps($dBedrijven["ID"], "nr")."'");
    
    $contentFrame   .= "<Br/><br/>";
    $telEntiteiten++;
  }
  
  $contentFrame .= "<br/><br/>Number of entities: $telEntiteiten";
  
  $contentFrame = "<div class='oft2_page_centering' style=\"padding-top: 50px;\">$contentFrame</div>";

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_mena($userid, $bedrijfsid) {
  $titel         = tl('Pipeline');
  $submenuitems  = oft_inputform_link('content.php?SITE=oft_mena_add', "Add deal", 500) .
                   '<a href="content.php?SITE=oft_mena_reports_page">Reports</a>' .
                   '<a href="content.php?SITE=oft_mena_evaluation_page">Evaluations</a>' .
                   oft_inputform_link('content.php?SITE=oft_mena_lessonslearned_page', "Lessons learned", 500) .
                   ' | '. "<a class=\"cursor\" onClick=\"window.location.href='content.php?SITE=oft_mena_print&PRINT=true&PAGNR=-1".registers_showdate_url()."&SRCH_BEDRIJFSNAAM='+document.getElementById('SRCH_BEDRIJFSNAAM').value+'&SRCH_LAND='+document.getElementById('SRCH_LAND').value+'&SRCH_SEARCH='+document.getElementById('SRCH_SEARCH').value;\" target=\"_blank\">Printable version</a>";
  $back_button   = "";

  $tabel           = "oft_mena";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_mena_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_mena_edit&ID=-ID-");
  $verwijderen     = "";
  $sql             = registers_showdate();
  
  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_mena", 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_mena_map() {
  $contentFrame = "<div style=\"text-align: center\" class=\"ms-rte-embedcode ms-rte-embedwp\"><img width=\"956\" border=\"0\" height=\"234\" alt=\"\" usemap=\"#image-maps-2014-02-20-153852\" src=\"./images/Navi.png\" id=\"Image-Maps-Com-image-maps-2014-02-20-153852\">
                     <map id=\"ImageMapsCom-image-maps-2014-02-20-153852\" name=\"image-maps-2014-02-20-153852\">
                         <area href=\"" . buildUrl('SRCH_STATUS','') . "\" target=\"_self\" title=\"AllTargets\" alt=\"AllTargets\" coords=\"11,8,943,50\" shape=\"rect\" id=\"AllTargets\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Pending') . "\" target=\"_self\" title=\"Pending\" alt=\"Pending\" coords=\"12,70,157,146\" shape=\"rect\" id=\"Pending\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Active Discussions') . "\" target=\"_self\" title=\"ActiveDiscussions\" alt=\"ActiveDiscussions\" coords=\"402,70,547,146\" shape=\"rect\" id=\"ActiveDiscussions\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Dead') . "\" target=\"_self\" title=\"Dead\" alt=\"Dead\" coords=\"798,70,943,146\" shape=\"rect\" id=\"Dead\">
                         <area href=\"#\" title=\"Image Map\" alt=\"Image Map\" coords=\"954,232,956,234\" shape=\"rect\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Active Discussions : NBO') . "\" target=\"_self\" title=\"NBO\" alt=\"NBO\" coords=\"183,166,292,166,318,193,287,224,183,224\" shape=\"poly\" id=\"NBO\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Active Discussions : DD') . "\" target=\"_self\" title=\"DD\" alt=\"DD\" coords=\"295,167,395,168,423,195,396,224,295,224,318,196\" shape=\"poly\" id=\"DD\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Active Discussions : SPA') . "\" target=\"_self\" title=\"SPA\" alt=\"SPA\" coords=\"400,166,504,166,531,192,506,222,403,223,423,197\" shape=\"poly\" id=\"SPA\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Active Discussions : CP') . "\" target=\"_self\" title=\"CP\" alt=\"CP\" coords=\"507,166,618,166,645,197,618,224,508,224,530,198\" shape=\"poly\" id=\"CP\">
                         <area href=\"" . buildUrl('SRCH_STATUS', 'Active Discussions : Closed') . "\" target=\"_self\" title=\"Closed\" alt=\"Closed\" coords=\"621,166,741,166,768,197,742,224,622,223,644,198\" shape=\"poly\" id=\"Closed\">
                     </map>
                 </div>
                 <br/><br/>";
  return $contentFrame;
}

function oft_mena_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_mena";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_mena_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_mena_edit&ID=-ID-");
  $verwijderen     = "";
  
  $sql             = registers_showdate();
  
  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_mena_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_mena";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_mena_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_mena_edit&ID=-ID-");
  $verwijderen     = "";

  echo "<h1>Deals</h1><br/><br/>";
  
  $sql             = registers_showdate();

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_mena", 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_mena_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_mena.htm";
  $tabel = "oft_mena";

  echo "<h2>Add deal</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_mena_evaluation_page($userid, $bedrijfsid) {
    global $db;

    $titel         = tl('Evaluations');
    $submenuitems  = '<a href="content.php?SITE=oft_mena">Targets</a>' .
                     '<a href="content.php?SITE=oft_mena_lessonslearned_page">Lessons learned</a>';
    $back_button   = "";

    $tabel           = "oft_mena_evaluation";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht_page", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht_page", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht_page", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht_page", "arrayZoekvelden");

    if(isset($_REQUEST["SRCH_MENAID"])) {
        $menaID = $_REQUEST["SRCH_MENAID"];
    }

    $toevoegen       = "content.php?SITE=oft_mena_add_evaluation";
    $bewerken        = oft_inputform_link_short("content.php?SITE=oft_mena_edit_evaluation" . (!empty($menaID) ? '&MENAID=' . $menaID : '') . "&ID=-ID-");
    $verwijderen     = "";

    $sql             = registers_showdate();

    $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_mena_reports_page($userid, $bedrijfsid) {
    global $pdo;

    $titel         = tl('Reports by targets');
    $submenuitems  = '<a href="content.php?SITE=oft_mena">Targets</a>' .
                     '<a href="content.php?SITE=oft_mena_evaluation_page">Evaluations</a>';
    $back_button   = "";

    $queryStates = $pdo->prepare('SELECT COUNT(*) AS `COUNTER`, `STATUS` FROM oft_mena GROUP BY `STATUS`;');
    $queryStates->execute();
    
    $queryJurisdict = $pdo->prepare('SELECT COUNT(*) AS `COUNTER`, `JURISDICTION` FROM oft_mena GROUP BY `JURISDICTION`;');
    $queryJurisdict->execute();

    $contentFrame = '<div class="oft2_page_header"><div class="oft2_page_centering"></div></div>';
    $contentFrame .= '<div class="oft2_page_centering"><script type=""></script>
                        <div class="left char-container">
                            <h2>Targets by Status</h2>
                            <canvas id="chartTarget" width="400" height="400"></canvas>
                        </div>
                        <div class="left char-container">
                            <h2>Targets by Jurisdiction</h2>
                            <canvas id="chartJurisdiction" width="400" height="400"></canvas>
                        </div>'
                        . printChartData($queryStates, $queryJurisdict);
    $contentFrame .= '</div>';

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_mena_lessonslearned_page($userid, $bedrijfsid) {
    global $pdo;

    $titel         = tl('Lessons Learned');
    $submenuitems  = '<a href="content.php?SITE=oft_mena">Targets</a>' .
                     '<a href="content.php?SITE=oft_mena_evaluation_page">Evaluations</a>';
    $back_button   = "";

    $sqlAllLessons = "";

    $headerRows = array('LESSON' => 'Lesson');
    $headerData = array();

    $query = $pdo->prepare('SELECT ID, LESSONSLEARNED_ONE as LESSON FROM oft_mena_evaluation WHERE LESSONSLEARNED_ONE IS NOT NULL
                        UNION
                        SELECT ID, LESSONSLEARNED_TWO as LESSON FROM oft_mena_evaluation WHERE LESSONSLEARNED_TWO IS NOT NULL
                        UNION
                        SELECT ID, LESSONSLEARNED_THREE as LESSON FROM oft_mena_evaluation WHERE LESSONSLEARNED_THREE IS NOT NULL
                        UNION
                        SELECT ID, LESSONSLEARNED_FOUR as LESSON FROM oft_mena_evaluation WHERE LESSONSLEARNED_FOUR IS NOT NULL;');

    foreach ($query->fetchAll() as $row) {
        $headerData[] = array('ID' => $row['ID'], 'LESSON' => $row['LESSON']);
    }

    $contentFrame = '<div class="oft2_page_header"><div class="oft2_page_centering"></div></div>';
    $contentFrame .= '<div class="oft2_page_centering">';
    $contentFrame .=  buildCustomTable($headerRows, $headerData);
    $contentFrame .= '</div>';

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}


function oft_mena_evaluation($userid, $bedrijfsid) {
    $tabel           = "oft_mena_evaluation";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

    if(isset($_REQUEST["SRCH_MENAID"])) {
        $menaID = $_REQUEST["SRCH_MENAID"];
    }

    $toevoegen       = "content.php?SITE=oft_mena_add_evaluation";
    $bewerken        = oft_inputform_link_short("content.php?SITE=oft_mena_edit_evaluation" . (!empty($menaID) ? '&MENAID=' . $menaID : '') . "&ID=-ID-");
    $verwijderen     = "";

    $sql             = registers_showdate();

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_mena_add_evaluation($userid, $bedrijfsid) {
    $templateFile = "./templates/oft_mena_evaluation.htm";
    $tabel = "oft_mena_evaluation";

    echo "<h2>Add evaluation</h2>";

    $customValues = array("MENAID" => $_REQUEST['MENAID']);

    echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile, $customValues);
}

function oft_mena_edit_evaluation($userid, $bedrijfsid) {
    global $pdo;

    $templateFile = "./templates/oft_mena_evaluation.htm";
    $tabel = "oft_mena_evaluation";

    $id = "";
    $menaID = "";
    if(isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }

    if(!isset($_REQUEST['MENAID']) && !empty($id)) {
        $query = $pdo->prepare('SELECT MENAID FROM oft_mena_evaluation WHERE ID = :id');
        $query->bindValue('id', $id);
        $query->execute();
        $d_evaluation = $query->fetch(PDO::FETCH_ASSOC);

        if($d_evaluation !== false) {
            $menaID = $d_evaluation['MENAID'];
        }
    } elseif(isset($_REQUEST['MENAID'])) {
        $menaID = $_REQUEST['MENAID'];
    }

    $customValues = array("MENAID" => $menaID);

    echo "<h2>Edit Evaluation</h2>";
    echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id, $customValues);
}

function oft_mena_edit($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_mena.htm";
  $tabel = "oft_mena";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit deal" .
      "<span style=\"float:right\">" . oft_inputform_link('content.php?SITE=oft_mena_evaluation&SRCH_MENAID=' . $id, "<button class=\"button\">Evaluations</button>", 500) .
      "&nbsp;" . oft_inputform_link('content.php?SITE=oft_mena_add_evaluation&MENAID=' . $id, "<button class=\"button\">Add Evaluation</button>", 500) . "</span>"
      . '</h2>';

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_mena_1_print($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_mena_print.htm";
  $tabel = "oft_mena";

  $id = "";
  if(isset($_REQUEST["ID"])) { 
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Deals</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}


function buildUrl($key, $value, $url = null) {
    if(is_null($url)) {
        $url = $_SERVER['REQUEST_URI'];
    }
    $newUrl = '';
    $alreadyExist = false;
    $queryUrls = explode('&', parse_url($url, PHP_URL_QUERY));

    foreach($queryUrls as $querypart) {
       $part = explode('=', $querypart);

       if($part[0] == $key) {
           $alreadyExist = true;
           $part[1] = $value;
       }

       if(empty($newUrl)) {
           $newUrl .= '?';
       } else {
           $newUrl .= '&';
       }

        $newUrl .= $part[0] . '=' . ($part[0] != 'SITE' && substr($part[1], 0, 1) != 'x' ? 'x' : '') . $part[1];
    }

    if(!$alreadyExist) {
        if(empty($newUrl)) {
            $newUrl .= '?';
        } else {
            $newUrl .= '&';
        }

        $newUrl .= $key . '=x' . $value;
    }

    return $newUrl;
}

function buildCustomTable($headerRows, $rows) {
    $html = "<div id=\"contentTable\">
                <table name=\"exportexcel\" class=\"sortable\" width=\"100%\">
                    <thead>";
    if(!empty($headerRows)) {
        $html .= "<tr>";
        foreach($headerRows as $headerRow) {
            $html .=  "<th style=\"\">
                            <a class=\"cursor\" onclick=\"\">$headerRow</a>
                        </th>";
        }
        $html .= "</tr>";
    }

    $html .=    "</thead>
                 <tbody>";


    foreach($rows as $row) {
        $html .= "<tr style=\"\" class=\"table-border cursor\">";

        foreach($row as $dataKey => $data){
            if(isset($headerRows[$dataKey])) {
                $html .= "<td onclick=\"\" align=\"left\" valign=\"top\">
                            $data
                          </td>";
            }
        }

        $html .= "</tr>";
    }


    $resultsCount = count($rows);

    $html .=     "</tbody>
                </table>
              </div>
              <br>
              <br>
              $resultsCount results found.
              <a class=\"button\" onclick=\"window.open('content.php?SITE=oft_mena_lessonslearned_page&EXPORTEXCEL=true');\">Open in excel</a>";



    return $html;
}

function printChartData($dataSetStates, $dataSetJuris) {

    $counter = 0;
    $colors = array(array('firstColor' => '#ff6500', 'secondColor' => '#ff8433'),
        array('firstColor' => '#c84d26', 'secondColor' => '#c86b4e'),
        array('firstColor' => '#f4f4f4', 'secondColor' => '#c2c2c2'),
        array('firstColor' => '#d0021b', 'secondColor' => '#d02c40'),
        array('firstColor' => '#fa893f', 'secondColor' => '#faa770'),
        array('firstColor' => '#8a8a8a', 'secondColor' => '#bdbdbd'));
    $html = '<script type="text/javascript">';
    $html .= 'window.dataStates = [';

    foreach ($dataSetStates->fetchAll() as $row) {
        if($counter != 0) {
            $html .= ',';
        }

        if(!isset($colors[$counter])) {
            $counter = 0;
        }

        $html .= '{
            value: ' . $row['COUNTER'] . ',
            label: "' . $row['STATUS'] . '",
            color: "' . $colors[$counter]['firstColor'] . '",
            highlight: "' . $colors[$counter]['secondColor'] . '"
        }';

        $counter++;
    }
    $html .= '];';

    $counter = 0;
    $html .= 'window.dataJuris = [';

    foreach ($dataSetJuris->fetchAll() as $row) {
        if($counter != 0) {
            $html .= ',';
        }

        if(!isset($colors[$counter])) {
            $counter = 0;
        }

        $html .= '{
            value: ' . $row['COUNTER'] . ',
            label: "' . $row['JURISDICTION'] . '",
            color: "' . $colors[$counter]['firstColor'] . '",
            highlight: "' . $colors[$counter]['secondColor'] . '"
        }';

        $counter++;
    }
    $html .= '];
    ';

    $html .= '</script>';

    $html .= '<script type="text/javascript" src="/scripts/chartIni.js"></script>';

    return $html;
}