<?php

/*
 * Function to return an array of Key => Values to use for selectBox().
 * 
 * @param (string) $name The name of the Options List.
 * 
 * @return (array) Array of Key => Values
 */
function getListOptions($name)
{
    $user = getValidatedLoginID();
    $company = getMyBedrijfsID();
    $return = array();
    switch ($name) {
        case 'owner':
            if (check('TCAM', 'Nee', $company) == 'Ja') {
                $return = array("TCAM" => "TCAM");
            } elseif (check('DIF', 'Nee', $company) == 'Ja') {
                $return = array("DIF" => "DIF");
            } else {
                $return = array("Orangefield" => "Orangefield", "Vistra" => "Vistra");
            }
            break;
        case 'accounting_systems':
            $return = array(
                "NetSuite" => "NetSuite",
                "Intact" => "Intact",
                "Oracle" => "Oracle",
                "Quickbooks" => "Quickbooks",
                "Other" => "Other"
            );
            break;
        case 'director_functions':
            $return = array(
                "Director" => "Director",
                "Director third party" => "Director third party",
                "Director independent" => "Director independent",
                "Company secretary" => "Company secretary",
                "Shareholder" => "Shareholder",
                "Supervisory Board Member" => "Supervisory Board Member",
            );
            break;
        case 'adres_type':
            $return = array(
                "Registered" => "Registered",
                //"Business" => "Business",
                "Postal address" => "Postal address",
                "Visiting address" => "Visiting address",
            );
            break;
        case 'AUTHORIZATION':
            $return = array(
                "Solely" => check('DIF', 'Nee', $company) == 'Ja' ? "Single director" : "Solely",
                "Jointly" => "Jointly A",
                "Jointly B" => "Jointly B",
                "Jointly A+B" => "Jointly A+B",
            );
            break;
        case 'incident_soort':
            $return = array(
                "Claim" => "Claim",
                //"Crisis Incidents" => "Crisis Incidents",
                //"Publicity Incidents" => "Publicity Incidents",
                //"Contamination Incidents" => "Contamination Incidents",
                //"Freeze list Incident" => "Freeze list Incident",
                //"Investigation Incident" => "Investigation Incident",
                //"IT Incidents" => "IT Incidents",
                //"Non compliance Incident" => "Non compliance Incident",
                "Incident" => "Incident",
            );
            break;
        case 'maanden':
            $return = array(
                "01" => "January",
                "02" => "February",
                "03" => "March",
                "04" => "April",
                "05" => "May",
                "06" => "June",
                "07" => "July",
                "08" => "August",
                "09" => "September",
                "10" => "October",
                "11" => "November",
                "12" => "December"
            );
            break;
        case 'mena_category':
            $return = array(
                "Pending" => "Pending",
                "Active Discussions" => "Active Discussions",
                "Dead" => "Dead"
            );
            break;
        case 'mena_services':
            $return = array(
                "Administration" => "Administration",
                "Fund Accounting" => "Fund Accounting",
                "International Expansion" => "International Expansion",
                "Other" => "Other",
                "Trust" => "Trust"
            );
            break;
        case 'mena_status':
            $return = array(
                "Pending" => "Pending",
                "Active Discussions" => "Active Discussions",
                "Active Discussions : NBO" => "Active Discussions : NBO",
                "Active Discussions : DD" => "Active Discussions : DD",
                "Active Discussions : SPA" => "Active Discussions : SPA",
                "Active Discussions : CP" => "Active Discussions : CP",
                "Active Discussions : Closed" => "Active Discussions : Closed",
                "Dead" => "Dead"
            );
            break;
        case 'numbers_types':
            $return = array(
                "KVK" => "Chamber of Commerce",
                "VAT" => "VAT (Value-added Tax)",
                "ABN" => "ABN",
                "PAYGW" => "PAYGW",
                "GIIN" => "GIIN (Global Intermediary Identification Number)",
                "LEI" => "LEI (Legal Entity Identifier)",
                "TFN" => "TFN",
                "Fiscal" => "Fiscal number"
            );
            break;
        case 'PRIORITY':
            $return = array(
                "High" => "High",
                "Medium" => "Medium",
                "Low" => "Low"
            );
            break;
        case 'PROFIEL':
            $return = array(
                //"Client" => "Client",
                "Relationship manager" => "Relationship manager",
                "Administrator" => "Administrator"
            );
            break;
        case 'reg_incidenttype';
            $return = array(
                "Internal fraud (ie: insider trading, market timing)" => " Internal fraud (ie: insider trading, market timing)",
                "External fraud (ie: Market abuse, falsification of documents, ML, TF)" => "External fraud (ie: Market abuse, falsification of documents, ML, TF)",
                "Employment practices and workplace safety (ie: injury claims, sexual haracement claims..)" => "Employment practices and workplace safety (ie: injury claims, sexual haracement claims..)",
                "Clients, products and business practices (ie: unintentional or negligent failure to meet a professional obligation)" => "Clients, products and business practices (ie: unintentional or negligent failure to meet a professional obligation)",
                "Damage to physical assets (ie: from natural disaster or other events)" => "Damage to physical assets (ie: from natural disaster or other events)",
                "Business disruption and system failures" => "Business disruption and system failures",
                "Execution, delivery and process management (ie: failed transaction processing or process management)" => "Execution, delivery and process management (ie: failed transaction processing or process management)"
            );
            break;
        case 'reg_recoverable':
            $return = array(
                "Ja" => "Yes",
                "Nee" => "No",
            );
            break;
        case 'reg_status':
            $return = array(
                "Open" => "Open",
                "Closed" => "Closed"
            );
            break;
        case 'reg_typeofaction':
            $return = array(
                "Risk Assesment" => "Risk Assesment",
                "Procedures - Risk mitigation" => "Procedures - Risk mitigation",
                "Monitoring" => "Monitoring",
                "Incidents" => "Incidents",
                "Training" => "Training",
                "Advisory" => "Advisory",
                "Governance" => "Governance",
                "Other projects" => "Other projects"
            );
            break;
        case 'secties':
            $return = array(
                "Legal" => "Legal",
                "Finance" => "Finance",
                "HR" => "HR",
                "Contracts" => "Contracts",
                "Legal, Finance" => "Legal, Finance",
                "Legal, HR" => "Legal, HR"
            );
            break;
        case 'service_provider':
            $return = array(
                "Legal" => "Legal",
                "Accounting" => "Accounting",
                "Payroll" => "Payroll",
                "PEO" => "PEO",
                "Corp Sec" => "Corp Sec",
                "Auditor" => "Auditor"
            );
            break;
        case 'service_type':
            $return = array(
                "Subsidiary" => "Subsidiary",
                "Branch" => "Branch",
                "Payroll" => "Payroll"
            );
            break;
        case 'status':
            $return = array("Final" => "Final");
            if (check('DIF', 'Nee', $company) == 'Nee') {
                $return["Draft"] = "Draft";
            } elseif (hasRights('administrator', $user)) {
                $return["Not required"] = "Not required";
            }
            break;
        case 'termijn':
            $return = array(
                "month" => "month",
                "quarter" => "quarter",
                "half year" => "half year",
                "year" => "year",
                "two year" => "two year"
            );
            break;
        case 'type_managementboard':
            $return = array(
                "Executive" => "Executive",
                "Non-executive" => "Non-executive"
            );
            break;
    }

    return $return;
}