<?php
sercurityCheck();

function organogramPrinten($userid, $bedrijfsid)
{
    global $pdo;
    $BID = ps($_REQUEST["BID"], "nr");

    echo "<a href=\"content.php?SITE=organogramPrinten&amp;PDF=true\" target=\"_blank\">PDF</a>";

    
    $query = $pdo->prepare('SELECT * FROM bedrijf WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null);');
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        $query = $pdo->prepare('SELECT (select Count(*) from oft_shareholders where oft_shareholders.ADMINID = bedrijf.ID) As KIDS, bedrijf.*, oft_shareholders.PERSENTAGE
                               FROM bedrijf, oft_shareholders 
                              WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                                AND (NOT oft_shareholders.DELETED = "Ja" OR oft_shareholders.DELETED is null)
                                AND (bedrijf.ENDDATUM >= "' . date("Y-m-d"). '" OR bedrijf.ENDDATUM is null OR bedrijf.ENDDATUM = "0000-00-00")
                                AND oft_shareholders.ADMINID = :adminid
                                AND bedrijf.ID = oft_shareholders.ADMINIDFROM
                              ORDER BY KIDS, (bedrijf.GROUPID*1), bedrijf.BEDRIJFSNAAM;');
        $query->bindValue('adminid', $data["ID"]);
        $query->execute();
        $row = $query->fetch(PDO::FETCH_ASSOC);

        if($row !== false) {
          organogramPerEntiteit($userid, $bedrijfsid, $data["ID"]);
          echo "<p style=\"page-break-before: always\" />";
        }
    }
}

function organogramPerEntiteit($userid, $bedrijfsid)
{
    global $pdo;
    
    $BID = $bedrijfsid;

    $vader         = "";
    //Print vader
    $query = $pdo->prepare('SELECT * FROM bedrijf 
                       WHERE ID in (select ADMINID from oft_shareholders WHERE ADMINIDFROM = :bid AND (NOT DELETED = "Ja" OR DELETED is null));');
    $query->bindValue('bid', $BID);
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        $vader     = "<tr><td colspan=\"100\" valign=\"top\" align=\"center\" style=\"background:#FFF\"><div style=\"background: #aaa; border: 1px solid #000; width: 80px; height: 80px; padding: 5px; text-align:center;margin:1px; margin-bottom:2px;\"><a style=\"font-size: 9px; font-weight: bold;\">[100%]<br/>".stripslashes($data["BEDRIJFSNAAM"])."</a></div> <img alt=\"1\" src=\"./images/pxgray.jpg\" height=\"37\" style=\"height: 37px; width: 1px;\" border=\"0\" />";
    }

   //Print Bedrijf met 1 laag organogram
   $queryBedrijf = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id;');
   $queryBedrijf->bindValue('id', $BID);
   $queryBedrijf->execute();

   foreach ($queryBedrijf->fetchAll() as $data) {
       $nr++;
       $bedrijfsvakje = $vader."\n<div style=\"border: 1px solid #000; width: 80px; height: 80px; padding: 5px; text-align:center;margin:1px; margin-bottom:2px;\"><a style=\"font-size: 9px; font-weight: bold;\">[100%]<br/>".stripslashes($data["BEDRIJFSNAAM"])."</a></div>";

       echo subOrganogram(($data["ID"]*1), 1, $bedrijfsvakje, "left", $bedrijfsvakje, true, ($data["ID"]*1), 9);
   }
}

function oft_entiteit_persentage($vader, $kind) {
  global $pdo;

  $return = 0;
  $query = $pdo->prepare('SELECT oft_shareholders.PERSENTAGE FROM oft_shareholders WHERE oft_shareholders.ADMINID = :adminid and oft_shareholders.ADMINIDFROM = :adminidform AND (NOT DELETED = "Ja" OR DELETED is null);');
  $query->bindValue('adminid', $vader);
  $query->bindValue('adminidform', $kind);
  $query->execute();

  foreach ($query->fetchAll() as $data) {
    $return += $data["PERSENTAGE"]*1;
  }

  return $return;
}

function oft_aantalVaders($BID) {
  global $pdo;

  $teller = 0;
  $query = $pdo->prepare('SELECT *
            FROM bedrijf 
           WHERE (NOT DELETED = "Ja" OR DELETED is null) 
             AND not ID = :id
             AND ID in (select ADMINID from oft_shareholders WHERE ADMINIDFROM = :adminformid AND (NOT DELETED = "Ja" OR DELETED is null) );');
  $query->bindValue('id', $BID);
  $query->bindValue('adminformid', $BID);
  $query->execute();

  foreach ($query->fetchAll() as $data) {
    $teller++;
  }

  return $teller;
}

function oft_aantalKinderen($BID) {
  global $pdo;

  $teller = 0;
  $query = $pdo->prepare('SELECT *
             FROM bedrijf
            WHERE (NOT DELETED = "Ja" OR DELETED is null)
              AND not ID = :id
              AND ID in (select ADMINIDFROM from oft_shareholders WHERE ADMINID = :adminid AND (NOT DELETED = "Ja" OR DELETED is null) );');
  $query->bindValue('id', $BID);
  $query->bindValue('adminid', $BID);
  $query->execute();

  foreach ($query->fetchAll() as $data) {
    $teller++;
  }

  return $teller;
}


function oft_organogram_vakje($vaderId, $BID, $fontsize, $breeteVakje, $isroot = false) {
  global $pdo;

  $vakje = "";

  $tweedeVader = "";
  $aantalTweedeVaders = 0;
  $toonvaders = true; // alle ouders altijd tonen, ook voor niet-root
  if ($isroot || $toonvaders) {
    $sql = "";

    $query = $pdo->prepare('SELECT *
            FROM bedrijf
           WHERE (NOT DELETED = "Ja" OR DELETED is null)
             AND not ID = :id
             AND not ID = :idbid
             AND ID in (select ADMINID from oft_shareholders WHERE ADMINIDFROM = :adminidform
             AND (NOT DELETED = "Ja" OR DELETED is null));');
    $query->bindValue('id', $vaderId);
    $query->bindValue('idbid', $BID);
    $query->bindValue('adminidform', $BID);
    $query->execute();

    foreach ($query->fetchAll() as $data) {
      // Voor een root binnen een organogram niet de sub tonen, anders wordt het dubbel getoond
      $inline_root = !$isroot && $data['TYPE'] == "Ja" ? true : false;

      if($tweedeVader == "") {
        $tweedeVader = "<table class=\"oft_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding: 0; margin: 0;\">";
      }

      $persentage = '';
      if($vaderId != 0) {
        $persentage = '['.oft_entiteit_persentage($data["ID"], $BID).'%]';
      }
      $tweedeVader .= "<tr><td align=\"right\" valign=\"bottom\" style=\"background:#FFF\">
                       <div style=\"width: ".($breeteVakje/2)."px; height: 48px;background:#FFF\">
                         <table class=\"oft_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"100%\" style=\"background:#FFF\">";
      if($aantalTweedeVaders > 0) {
        $tweedeVader .= "<tr><td style=\"background:#FFF\"><img name=\"v1\" border=\"0\" style=\"float: left; height: 100%; width: 2px;\" src=\"./images/pxgray.jpg\" alt=\"2\" /></td></tr>";
      } else {
        $tweedeVader .= "<tr><td height=\"50%\">&nbsp;</td></tr>";
      }
      $tweedeVader .= "<tr><td height=\"2\" style=\"background:#FFF\"><img name=\"v2\" border=\"0\" style=\"float: left; width: ".($breeteVakje/4)."px; height: 2px;\" src=\"./images/pxgray.jpg\" alt=\"3\" /></td></tr>
                         <tr><td height=\"50%\" style=\"background:#FFF\"><img name=\"v3\" border=\"0\" style=\"float: left; height: 100%; width: 2px;\" src=\"./images/pxgray.jpg\" alt=\"4\" /></td></tr>
                         </table>
                       </div>
                       </td><td align=\"left\" style=\"background:#FFF\">
                       <div
                         style=\"float: right; border: 1px solid #cccccc; width: 90px; padding: 5px; text-align:center;margin:1px; margin: 0px; font-size: ".$fontsize."px; font-weight: bold;\"
                         class=\"cursor tweedevader\"
                         onMouseOver=\"this.style.background='#ECEDED';\"
                         onMouseOut=\"this.style.background='#FFFFFF';\"
                         onClick=\"window.location.replace('content.php?SITE=oft_organogram&BID=".($data["ID"]*1)."');\">$persentage &nbsp;<br/>".verkort(stripslashes($data["BEDRIJFSNAAM"]), 20)."</div>
                       </td>
                      </tr>";
      $aantalTweedeVaders++;
    }
    if($tweedeVader != "") { $tweedeVader .= "</table>"; }
   }

  $query = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id limit 1;');
  $query->bindValue('id', $BID);
  $query->execute();
  $data = $query->fetch(PDO::FETCH_ASSOC);

  if($data !== false) {
    // Voor een root binnen een organogram niet de sub tonen, anders wordt het dubbel getoond
    $inline_root = !$isroot && $data['TYPE'] == "Ja" ? true : false;

    $persentage = '';
    if($vaderId != 0) {
      $persentage = '['.oft_entiteit_persentage($vaderId, $BID).'%]';
    }

    //Als er geen andere ouders zijn dan gewoon veldje tonen
    if($tweedeVader == "") {
      $vakje = "<div
                     style=\"border: 1px solid #ba381d; background:#fafafa; width: ".$breeteVakje."px; height: ".$breeteVakje."px; padding: 5px; text-align:center;margin:1px; margin-bottom:2px; font-size: ".$fontsize."px; font-weight: bold;\"
                     class=\"cursor geentweedevader\"
                     onMouseOver=\"this.style.background='#ECEDED';\"
                     onMouseOut=\"this.style.background='#FFFFFF';\"
                     onClick=\"window.location.replace('content.php?SITE=oft_entitie_overview&BID=".($data["ID"]*1)."');\">$persentage<br/>".stripslashes($data["BEDRIJFSNAAM"])."</div>";
    } else {
      $kindHeeftOokTweedeVader = oft_aantalVaders($vaderId);

      $vakje = "<table class=\"oft_table vakje\" style=\"border: 0px solid #000000;background:#FFF;\" cellspacing=\"0\" cellpadding=\"0\">";

      $vakje .= "<tr style=\"height: 2px;\">
                  <td valign=\"top\" align=\"right\" style=\"background:#FFF\">";

      if($vaderId > 0) {
        if(checkVaderHeeftMeerdereNonGroupVaders($vaderId, $vaderId) > 0) {
          //$vakje .= "1";
          if(checkHeeftBroers($vaderId) > 1) {
            $vakje .= "<img name=\"i\" border=\"0\" style=\"height: 2px; width: 50%;\" src=\"./images/pxgray.jpg\" alt=\"5\">";
          }
        } else {
          //$vakje .= "3";
          $vakje .= "<img name=\"i\" border=\"0\" style=\"height: 2px; width: 50%;\" src=\"./images/pxgray.jpg\" alt=\"5\">";
        }
      }

      $vakje .= "</td><td style=\"background:#FFF\">";
      if($vaderId > 0) {
        if(checkVaderHeeftMeerdereNonGroupVaders($BID, $vaderId) > 0) {
          if(checkHeeftBroers($vaderId) > 1) {
            $vakje .= "<img name=\"b\" border=\"0\" style=\"height: 2px; width: 25px;\" src=\"./images/pxgray.jpg\" alt=\"6\">";
          }
        } else {
          $vakje .= "<img name=\"b\" border=\"0\" style=\"height: 2px; width: 25px;\" src=\"./images/pxgray.jpg\" alt=\"6\">";
        }
      }
      $vakje .= "</td>
                 </tr>";

      $vakje .= "<tr>
                  <td valign=\"top\" align=\"center\" style=\"background:#FFF\">";

        if($vaderId > 0) {
          $vakje .= "<img name=\"a\" border=\"0\" style=\"height: ".((48*$aantalTweedeVaders)+20)."px; width: 2px;\" src=\"./images/pxgray.jpg\" alt=\"7\">";
        }

      $vakje .= "</td>
                  <td align=\"left\" valign=\"bottom\" style=\"background:#FFF\">";

      //if($kindHeeftOokTweedeVader == 1) {
        if($vaderId > 0) {
          $vakje .= "<img name=\"c\" border=\"0\" style=\"height: 20px; width: 120px;\" src=\"./images/px.gif\" alt=\"\">";
        }
      //}
      $vakje .= $tweedeVader."</td>
                </tr>";

      $vakje .= "<tr>
                  <td style=\"background:#FFF\">
                    <div 
                       style=\"border: 1px solid #ba381d; width: ".$breeteVakje."px; height: ".$breeteVakje."px; padding: 5px; text-align:center;margin:1px; margin-bottom:2px; font-size: ".$fontsize."px; font-weight: bold;\"
                       class=\"cursor regel203\"
                       onMouseOver=\"this.style.background='#ECEDED';\"
                       onMouseOut=\"this.style.background='#FFFFFF';\"
                       onClick=\"window.location.replace('content.php?SITE=oft_entitie_overview&BID=".($data["ID"]*1)."');\">$persentage<br/>".stripslashes($data["BEDRIJFSNAAM"])."</div>
                  </td>";

       $vakje .= "<td valign=\"top\" align=\"left\" style=\"background:#FFF\">
                  <table class=\"oft_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"100%\">
                    <tr>
                        <td valign=\"bottom\" style=\"background:#FFF\"><img name=\"c\" border=\"0\" style=\"height: 2px; width: 20px;\" src=\"./images/pxgray.jpg\" alt=\"8\"></td>
                        <td><img name=\"d\" border=\"0\" style=\"height: ".($breeteVakje/2)."px; width: 2px;\" src=\"./images/pxgray.jpg\" alt=\"9\"></td>
                    </tr>
                  </table>
                  </td style=\"background:#FFF\">";

      $vakje .= "</tr>";

      $kindHeeftOokTweedeVader = oft_aantalVaders($data["ID"]);
      $aantalkinderen = oft_aantalKinderen($data["ID"]);

      if($kindHeeftOokTweedeVader == 1) {
        $vakje .= "<tr>
                    <td valign=\"top\" style=\"background:#FFF\">
                      <center><img name=\"e\" border=\"0\" style=\"height: ".($breeteVakje/2)."px; width: 2px;\" src=\"./images/pxgray.jpg\" alt=\"10\"></center>
                    </td>
                    <td align=\"left\" valign=\"bottom\" style=\"background:#FFF\"></td>
                   </tr>";
      } elseif ($aantalkinderen > 0 && !$inline_root) {
          $vakje .= "<tr>
                      <td valign=\"top\" style=\"background:#FFF\">
                        <center><img name=\"f\" border=\"0\" style=\"height: ".($breeteVakje/2)."px; width: 2px;\" src=\"./images/pxgray.jpg\" alt=\"11\"></center>";

          if(checkVaderHeeftMeerdereNonGroupVaders($BID, $BID) > 0) {
          } else {
            $vakje .= "<div style=\"2px;\" align=\"right\"><img name=\"g\" border=\"0\" style=\"height: 2px; width: 50%;\" src=\"./images/pxgray.jpg\" alt=\"12\"></div>";
          }  

          $vakje .= "</td>
                      <td align=\"left\" valign=\"bottom\" style=\"background:#FFF\">";

          if(checkVaderHeeftMeerdereNonGroupVaders($BID, $BID) > 0) {
          } else {
            $vakje .= "<img name=\"h\" border=\"0\" style=\"height: 2px; width: 25px;\" src=\"./images/pxgray.jpg\" alt=\"13\">";
          }
          $vakje .= "</td>
                     </tr>";
      } else {
        // geen kinderen... niet tonen?
      }
      $vakje .= "</table>";
    }
   }
   return $vakje;
}

function oft_organogram($userid, $bedrijfsid)
{
    global $pdo;

    echo "<center><h1>Organizational chart</h1></center>";

    $level         = 1;
    $output        = "";
    $printen       = isset($_REQUEST["PRINTEN"]);
    $fontsize      = 9;
    $breeteVakje   = 80;
    $heightVLine   = 37;

    $paddingTop     = floor($breeteVakje / 2);
    $eenVierdeVakje = floor($breeteVakje / 4);
    $nr             = 0;

    $lijnHorizontaal   = "<img alt=\"14\" src=\"./images/pxgray.jpg\" height=\"1\" style=\"clear:both; height: 2px; width: 20px;\" border=\"0\" />";

    if(isset($_REQUEST["BID"])) {
        $sql = 'SELECT * FROM bedrijf WHERE ID = :id;';
      $query = $pdo->prepare($sql);
      $query->bindValue('id', $_REQUEST["BID"]);
    } else {
      $sql = 'SELECT *
                FROM bedrijf
               WHERE NOT INTERN = "Nee"
                 AND (NOT DELETED = "Ja" OR DELETED is null)
                 AND (ID not in (select ADMINIDFROM
                                  from oft_shareholders
                                 where (NOT oft_shareholders.DELETED = "Ja" OR oft_shareholders.DELETED is null)
                               )
                      OR TYPE = "Ja") limit 4;';
      $query = $pdo->prepare($sql);
    }
    
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        //Get kinderen van bedrijfsid
        $kinderen       = [];
        $sql = 'SELECT bedrijf.*
                             FROM bedrijf, oft_shareholders
                           WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                                  AND (ENDDATUM >= "' . date("Y-m-d") . '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00")
                                  AND oft_shareholders.ADMINID = :adminid
                                  AND bedrijf.ID = oft_shareholders.ADMINID;';

        $query = $pdo->prepare($sql);
        $query->bindValue('adminid', $data["ID"]);
        $query->execute();

        foreach ($query->fetchAll() as $dKids) {
          $kinderen[] = $dKids["ID"];
        }

        //ADMINFROM zijn kinderen
        //ADMINID zijn ouders

        //Selecteer alle externe entiteiten
        $externen       = "xxx";
        $telExtern      = 0;
        
        //" OR ADMINIDFROM = '".ps($dKids["ID"], "nr")."'";
        //ToDo: Is deze query goed?
        $sql = 'SELECT ID
                           FROM bedrijf
                           WHERE INTERN = "Nee"
                             AND (NOT DELETED = "Ja" OR DELETED is null)
                             AND ID not in (select ADMINID from oft_shareholders WHERE (NOT DELETED = "Ja" OR DELETED is null))
                             AND ID in (select ADMINID
                                          from oft_shareholders
                                         WHERE (NOT DELETED = "Ja" OR DELETED is null)
                                           AND (ADMINIDFROM = :adminidform ' . implode(' OR ADMINIDFROM = ', $kinderen) .'));';
        $query = $pdo->prepare($sql);
        $query->bindValue('adminidform', $data["ID"]);
        $query->execute();

        foreach ($query->fetchAll() as $dExtern) {
          $vakje        = "<td>opta".oft_organogram_vakje(0, $dExtern["ID"], $fontsize, $breeteVakje)."</td>";
          if(($telExtern % 2) == 1) {
            $externen     = $externen . "<td>$lijnHorizontaal</td>". $vakje;
          } else {
            $externen     = $vakje . "<td>$lijnHorizontaal</td>". $externen;
          }
          $telExtern++;
        }

        //Print moeder entiteit: ook vaders erbij
        $nr++;
        $bedrijfsvakje = "<td align=\"center\">".oft_organogram_vakje(0, $data["ID"], $fontsize, $breeteVakje, true)."</td>";

        $bedrijfsvakje = str_replace("xxx", $bedrijfsvakje, $externen);

        $bedrijfsvakje = "<br/><br/><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"10%\" align=\"center\" style=\"background:#FFF\">$bedrijfsvakje</table>";

        $output .= subOrganogram(($data["ID"]*1), $level, $bedrijfsvakje, "left", $bedrijfsvakje, false, ($data["ID"]*1));
    }

    echo $output;

    if($output == '') {
      echo "<center>No root entity found!</center>";
    }
}

function checkVaderHeeftMeerdereNonGroupVaders($BID, $vaderId) {
  global $pdo;

  $query = $pdo->prepare('SELECT *
           FROM bedrijf
          WHERE (NOT DELETED = "Ja" OR DELETED is null)
            AND not ID = :id
            AND ID in (select ADMINID from oft_shareholders WHERE ADMINIDFROM = :adminidform
            AND (NOT DELETED = "Ja" OR DELETED is null));');
  $query->bindValue('id', $vaderId);
  $query->bindValue('adminidform', $vaderId);
  $query->execute();
  
  return $query->rowCount() - 1;
}

function checkHeeftBroers($bedrijfsid) {
  global $pdo;
  $query = $pdo->prepare('SELECT (select Count(*) from oft_shareholders where oft_shareholders.ADMINID = bedrijf.ID) As KIDS, bedrijf.*, oft_shareholders.PERSENTAGE
                                  FROM bedrijf, oft_shareholders
                                 WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                                   AND NOT bedrijf.INTERN = "Nee"
                                   AND (ENDDATUM >= "' . date("Y-m-d"). '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00")
                                   AND (oft_shareholders.ADMINID = :adminid)
                                   and bedrijf.ID = oft_shareholders.ADMINIDFROM
                                   and not bedrijf.ID = oft_shareholders.ADMINID
                                 GROUP BY oft_shareholders.ADMINIDFROM
                                 ORDER BY KIDS, (bedrijf.GROUPID*1), bedrijf.BEDRIJFSNAAM;');
  $query->bindValue('adminid', $bedrijfsid);
  $query->execute();
  return $query->rowCount();
  
}

function subOrganogram($bedrijfsid, $level, $bedrijfsvakje, $linksrechts, $eersteVakje, $printen, $vaders)
{
    global $pdo;

    $fontSize                    = 9;
    $breeteWitVakje              = "34";
    $breeteVakje                 = 80;
    $heightVLine                 = 37;

    $paddingTop                  = floor($breeteVakje / 2);
    $eenVierdeVakje              = floor($breeteVakje / 4);

    $regelBoven                  = "";
    $regelOnder                  = "";
    $subOrganogrammen            = "";
    $teller                      = 1;
    $output                      = "";
    $lijnHorizontaal             = "<img alt=\"15\" src=\"./images/pxgray.jpg\" height=\"1\" style=\"clear:both; height: 2px; width: 100%;\" border=\"0\" />";
    $lijnVertikaal               = "<img alt=\"16\" src=\"./images/pxgray.jpg\" height=\"".$heightVLine."\" style=\"height: ".$heightVLine."px; width: 2px;\" border=\"0\" />";

    $query = $pdo->prepare('SELECT (select Count(*) from oft_shareholders where oft_shareholders.ADMINID = bedrijf.ID) As KIDS, bedrijf.*, oft_shareholders.PERSENTAGE
                                      FROM bedrijf, oft_shareholders
                                     WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                                       AND NOT bedrijf.INTERN = "Nee"
                                       AND (ENDDATUM >= "' . date("Y-m-d") . '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00")
                                       AND (oft_shareholders.ADMINID = :adminid)
                                       and bedrijf.ID = oft_shareholders.ADMINIDFROM
                                       and not bedrijf.ID = oft_shareholders.ADMINID
                                     GROUP BY oft_shareholders.ADMINIDFROM
                                     ORDER BY KIDS, (bedrijf.GROUPID*1), bedrijf.BEDRIJFSNAAM;');
    $query->bindValue('adminid', $bedrijfsid);
    $query->execute();
    
    $aantalBroers = $query->rowCount();

    if($aantalBroers > 0)
    {
         $output .= "\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"10%\" align=\"center\" style=\"background:#fff;\">";

         if($level == 1 && $eersteVakje != "")
         {
              //Dit is type 1, en heeft kinderen
              //Toon eerste vakje bij type 1

              //Eigenlijk net zolang niet tonen, todat je bij eerste kind zit. Wanneer kind is van type 2
              $output     .= "\n<tr><td valign=\"top\" align=\"center\" colspan=\"$aantalBroers\">".$eersteVakje . $lijnVertikaal ."</td></tr>";
              $eersteVakje = "";
         }
    }

    //Teken horizontaal lijntje bij type 1, mits hij wel kinderen heeft
    if($aantalBroers > 1 && $eersteVakje == "")
    {
       for($k = 1; $k <= $aantalBroers; $k++)
       {
          if($k == 1) {
            $output .= "<tr><td align=\"right\" height=\"2\" width=\"50%\"><img alt=\"17\" src=\"./images/pxgray.jpg\" height=\"2\" style=\"clear:both; height: 2px; width: 50%;\" border=\"0\" /></td>";
          } elseif($k == $aantalBroers) {
            $output .= "<td align=\"left\" height=\"2\" width=\"50%\"><img alt=\"18\" src=\"./images/pxgray.jpg\" height=\"2\" style=\"clear:both; height: 2px; width: 50%;\" border=\"0\" /></td></tr>";
          } else {
            $output .= "<td align=\"center\" height=\"2\">$lijnHorizontaal</td>";
          }
       }
    }

    $regelnr = 1;
    
    $query->execute();
    
    foreach ($query->fetchAll() as $data) {
      $inline_root = $data['TYPE'] == "Ja" ? true : false;
        //Gegevens van de kinderen
         $aantalkinderen      = $data["KIDS"];

         //Eerste vakje tekenen
         if($eersteVakje != "" && !$printen)
         {
             $output     .= "\n<tr><td valign=\"top\" align=\"center\" colspan=\"$aantalBroers\">$eersteVakje $lijnVertikaal</td></tr>";
             $eersteVakje = "";
         }

         $tr = ""; $slTR = "";
         if($teller == 1)      { $tr = "<tr>"; $linksrechts = "left"; }
         elseif($teller == 2) {
            //Zorg dat het aantal </tr>-ren gelijk is aan de max(regels links, regels rechts) en voeg die dan per regel toe.
            $slTR = "</tr>";

            $linksrechts = "right";
            $teller = 0;
            $regelnr++;
         }

         //$bedrijfsvakje = "<div style=\"border: 1px solid #ba381d; width: ".$breeteVakje."px; height: ".$breeteVakje."px; padding: 5px; text-align:center;margin:1px; margin-bottom:2px;\"><a href=\"content.php?SITE=oft_entitie_overview&BID=".($data["ID"]*1)."\" style=\"font-size: ".$fontSize."px; color: #000000;\">[{$data["PERSENTAGE"]}%]<br/>".stripslashes($data["BEDRIJFSNAAM"])."</a></div>";
         $bedrijfsvakje = oft_organogram_vakje($bedrijfsid, $data["ID"], $fontSize, $breeteVakje);
         if(($printen && $level < 1) || !$printen)
         {
           $subOrganogram = "";
           //Check deadlock
            $idMySelf            = ";".($data["ID"]*1);
            $lijstVaders         = explode($idMySelf, $vaders);
            //echo "<br/>$idMySelf, $vaders: " . count($lijstVaders);
            if(!$inline_root && (count($lijstVaders) == 1 || count($lijstVaders) == 2 || count($lijstVaders) > 1)) {
              $subOrganogram   = subOrganogram(($data["ID"]*1), ($level+1), $bedrijfsvakje, $linksrechts, $eersteVakje, $printen, $vaders.";".($data["ID"]*1));
            }
         }

         $background_css = $subOrganogram ? "background-image:url('./images/pxgray2.jpg');background-repeat:repeat-y;background-position:center;" : "background:#FFF";
         $regelBoven .= "<td align=\"center\" valign=\"top\" style=\"".$background_css."\">";

         //lijn naar boven
         if(!($aantalBroers < 2)) { $regelBoven .= "<img alt=\"19\" src=\"./images/pxgray.jpg\" style=\"clear:both; height: ".$heightVLine."px; width: 2px;\" border=\"0\" /><br/>"; }

         //Bedrijfvakje
         $regelBoven         .= $bedrijfsvakje;

         //lijn naar beneden
         if($subOrganogram != "") {
           if(checkVaderHeeftMeerdereNonGroupVaders($bedrijfsid, $data["ID"]) > 0) {
           } else {
             $regelBoven         .= "<div><img alt=\"20\" src=\"./images/pxgray.jpg\" style=\"clear:both; height: ".$heightVLine."px; width: 2px;\" border=\"0\" /></div>";
           }
         }
         else                     { $regelBoven         .= ""; }

         $regelBoven         .= "</td>";
         $regelOnder         .= "<td align=\"center\" valign=\"top\" style=\"background:#FFF\">". $subOrganogram . "</td>";


         $teller++;
    }

    if($aantalBroers > 0)
    {
        $output .= "\n<tr>$regelBoven</tr>";
        $output .= "\n<tr>$regelOnder</tr>";
        $output .= "\n</table>";
    }

    return $output;
}

function oft_overzichtadministraties($userid, $bedrijfsid)
{
    global $pdo;

    $submenuitems    = "<a href=\"content.php?SITE=oft_entities_add\">".check("default_name_of_new_entity", "New entity", 1)."</a> | <a href=\"content.php?SITE=oft_entities&EXPORTEXCEL=true\">Export to Excel</a>";

    $contentFrame = oft_entity_buttons('tree') . "<br/><br/>";

    $contentFrame .= "<table name=\"exportexcel\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\" width=\"100%\" class=\"sortable\">
                      <thead>
                      <tr><th>(Ownership) ".check("default_name_of_entity", "Entity", 1)."</th><th>Number</th><th>Country</th></tr>
                      </thead><tbody>";
                                                                                                      
    $query = $pdo->prepare('SELECT *
                    FROM bedrijf 
                   WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null) 
                     AND (ENDDATUM >= "' . date("Y-m-d") . '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00")
                     AND ID not in (select ADMINIDFROM from oft_shareholders WHERE NOT DELETED = "Ja" OR DELETED is null);');
    $query->execute();

    foreach ($query->fetchAll() as $data) {
       $bedrijfsnaam = "N/A";
       if($data["BEDRIJFSNAAM"] != "") { $bedrijfsnaam = stripslashes($data["BEDRIJFSNAAM"]); }

       $bedrijfsonderdelen = bedrijfsonderdelen(($data["ID"]*1), "&nbsp;&nbsp;&nbsp;&nbsp;", true, ";".($data["ID"]*1));

       $contentSub = $bedrijfsonderdelen[0];
       $kindIds    = $bedrijfsonderdelen[1];
       if($contentSub == "") {
         $bedrijfsnaam = "<img src=\"./images/oft/pijlright.jpg\" border=\"0\" onClick=\"$kindIds\" /> <a href=\"content.php?SITE=oft_entitie_overview&ID=".($data["ID"]*1)."\">$bedrijfsnaam</a>";
       } elseif($contentSub != "") {
         $bedrijfsnaam = "<img src=\"./images/oft/pijldown.jpg\" border=\"0\" onClick=\"$kindIds\" /> <a href=\"content.php?SITE=oft_entitie_overview&ID=".($data["ID"]*1)."\">$bedrijfsnaam</a>";
       }

       $contentFrame .= "<tr>
                           <td>".$bedrijfsnaam."</td>
                           <td>".$data["BEDRIJFNR"]."</td>
                           <td>".$data["LAND"]."</td>
                         </tr>$contentSub";
    }

    $contentFrame .= "</tbody></table>";
    
    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, check("default_name_of_entity", "Entity", 1) . " tree", $submenuitems);
}

function bedrijfsonderdelen($bedrijfsid, $pos, $rescursief = true, $vaders = "")
{
    global $pdo;
    
    $iDs       = "";
    $contentSub = "";
    $content   = "";
    $sql = 'SELECT bedrijf.ID, bedrijf.BEDRIJFSNAAM, bedrijf.BEDRIJFNR, bedrijf.LAND, oft_shareholders.AMOUNT, oft_shareholders.PERSENTAGE 
                    FROM bedrijf, oft_shareholders 
                   WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                     AND (NOT oft_shareholders.DELETED = "Ja" OR oft_shareholders.DELETED is null)
                     AND (ENDDATUM >= "' . date("Y-m-d") . '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00") 
                     AND oft_shareholders.ADMINID = :adminid
                     AND bedrijf.ID = oft_shareholders.ADMINIDFROM
                ORDER BY bedrijf.GROUPID, bedrijf.BEDRIJFSNAAM;';
    $query = $pdo->prepare($sql);
    $query->bindValue('adminid', $bedrijfsid);
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        $kidsiDs = "";
        if($rescursief)
        {
           $idMySelf         = ";".($data["ID"]*1);
           $tmp              = explode($idMySelf, $vaders);
           //Dead lock test
           if(count($tmp) == 1) {
              $bedrijfsonderdelen = bedrijfsonderdelen(($data["ID"]*1), "$pos&nbsp;&nbsp;&nbsp;&nbsp;", true, $vaders.";".($data["ID"]*1));
              $contentSub         = $bedrijfsonderdelen[0];
              $kidsiDs            = $bedrijfsonderdelen[1];
           }
        }

        $persentage = $data["PERSENTAGE"];

        if($contentSub == "") {
          $bedrijfsnaam = "<img src=\"./images/oft/pijlright.jpg\" border=\"0\" onClick=\"$kidsiDs\" />&nbsp;<a href=\"content.php?SITE=oft_entitie_overview&ID=".($data["ID"]*1)."\">[".$persentage."%] " . stripslashes($data["BEDRIJFSNAAM"]) . "</a>";
        } elseif($contentSub != "") {
          $bedrijfsnaam = "<img src=\"./images/oft/pijldown.jpg\" border=\"0\" onClick=\"$kidsiDs\" />&nbsp;<a href=\"content.php?SITE=oft_entitie_overview&ID=".($data["ID"]*1)."\">[".$persentage."%] " . stripslashes($data["BEDRIJFSNAAM"]) . "</a>";
        }
        $content .= "<tr id=\"ID".($data["ID"]*1)."\">
                      <td>".$pos.$bedrijfsnaam."</td>
                      <td>".$data["BEDRIJFNR"]."</td>
                      <td>".$data["LAND"]."</td>
                     </tr>$contentSub";
        $iDs .= "toggle('ID".($data["ID"]*1)."'); $kidsiDs";
    }

    return array($content, $iDs);
}

function rechtenTot($userid, $bedrijfsid, $pos, $rescursief = true, $vaders = "")
{
    global $pdo;
    
    $iDs       = "";
    $content   = "";
    $teller    = 0;
    $idlijstAll = "";

    $query = $pdo->prepare('SELECT bedrijf.LAND
                    FROM bedrijf
                   WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                     AND (ENDDATUM >= "' . date("Y-m-d") . '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00")
                     AND NOT BEDRIJFSNAAM = ""
                   GROUP BY bedrijf.LAND
                   ORDER BY bedrijf.LAND;');
    $query->execute();

    foreach ($query->fetchAll() as $dLand) {
      $idlijst = "";
      $contentLijst = '';

      $allchecked = "checked=\"true\"";
      $query = $pdo->prepare('SELECT bedrijf.ID, bedrijf.BEDRIJFSNAAM, bedrijf.BEDRIJFNR, bedrijf.LAND
                      FROM bedrijf
                     WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                       AND (ENDDATUM >= "' . date("Y-m-d") . '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00")
                       AND bedrijf.LAND = :land
                       AND NOT BEDRIJFSNAAM = ""
                     ORDER BY (bedrijf.GROUPID*1), bedrijf.BEDRIJFSNAAM;');
      $query->bindValue('land', $dLand["LAND"]);
      $query->execute();

      foreach ($query->fetchAll() as $data) {
        $available_types = '';
        $checked = "";

        if($userid == 'board_change') {
          if($bedrijfsid == ($data["ID"]*1)) {
            $checked = "checked=\"true\"";
          } else {
            if(count(explode(($data["ID"]*1), $vaders)) > 1) {
              $checked = "checked=\"true\"";
            }
          }
        } elseif($userid == 'new') {
          $checked = "checked=\"true\"";
        } else {
          $checked = "";
          
          $query = $pdo->prepare('SELECT ID FROM oft_rechten_tot WHERE BEDRIJFSID = :bedrijfsid AND USERID = :userid limit 1;');
          $query->bindValue('bedrijfsid', $data["ID"]);
          $query->bindValue('userid', $userid);
          $query->execute();
          
          if($query->rowCount() > 0)
          {
            $checked="checked=\"true\"";
          } else {
            $allchecked = '';
          }
        }

        $queryRechtenTot = $pdo->prepare('SELECT * FROM oft_rechten_tot WHERE BEDRIJFSID = :bedrijfsid AND USERID = :userid limit 1;');
        $queryRechtenTot->bindValue('bedrijfsid', $data["ID"]);
        $queryRechtenTot->bindValue('userid', $userid);
        $queryRechtenTot->execute();

        $rechtenTot = "";
        foreach ($queryRechtenTot->fetchAll() as $dRechtenTot) {
          if($dRechtenTot["LEGAL"] == "Ja") {
            $rechtenTot .= "Legal";
          }
          if($dRechtenTot["COMPLIANCE"] == "Ja") {
            $rechtenTot .= ", Compliance";
          }
          if($dRechtenTot["BOARDPACK"] == "Ja") {
            $rechtenTot .= ", Boardpack";
          }
          if($dRechtenTot["FINANCE"] == "Ja") {
            $rechtenTot .= ", Finance";
          }
          if($dRechtenTot["HR"] == "Ja") {
            $rechtenTot .= ", HR";
          }
          if($dRechtenTot["CONTRACTS"] == "Ja") {
            $rechtenTot .= ", Contracts";
          }
          if($dRechtenTot["MAILNOTIFICATIONS"] == "Ja") {
            $rechtenTot .= ", Notifications e-mail";
          }
        }

        $idlijst .= "ENT".($data["ID"]*1).",";
        $idlijstAll .= "ENT".($data["ID"]*1).",";

        $contentLijst .= "<tr id=\"ID".($data["ID"]*1)."\">
                            <td>".lb($data["BEDRIJFSNAAM"])."</td>
                            <td>".$data["BEDRIJFNR"]."</td>
                            <td><input type=\"checkbox\" $checked id=\"ENT".($data["ID"]*1)."\" name=\"ENT".($data["ID"]*1)."\" value=\"Ja\" /></td>
                            <td><a onclick=\"".
                              "tonen('user_settings_popup_".($data['ID']*1)."'); getPageContent2('content.php?SITE=gen_oft_users_edit_AJAX&USERID=". ( $userid* 1 ). "&BEDRIJFSID=". ( $data['ID']* 1 ). "', 'user_settings_popup_".($data['ID']*1)."', searchReq);"
                            . "\"><img src=\"./images/oft/oft_slotje.png\"></a>
                            <div id=\"user_settings_popup_".($data['ID']*1)."\" style=\"position:relative; display: none; background: none repeat scroll 0 0 #fff; border: 1px solid #ccc; margin-left: -20px; margin-top: -169px; padding: 10px; position: absolute;\"></div></td>
                            <td>$rechtenTot</td>
                          </tr>";
      }

      if($contentLijst != '') {
        $idlijst = substr($idlijst, 0, (strlen($idlijst)-1));
  
       $content  .= "<div id=\"pijla$teller\" style=\"font-weight: bold; cursor: pointer;\" onclick=\"tonen('table$teller'); tonen('pijlb$teller'); sluiten('pijla$teller');\"><img border=\"0\" onclick=\"toggle('table$teller');\" src=\"./images/oft/pijldown.jpg\"> ".$dLand["LAND"]."</div>
                    <div id=\"pijlb$teller\" style=\"font-weight: bold; cursor: pointer; display: none;\" onclick=\"sluiten('table$teller'); sluiten('pijlb$teller'); tonen('pijla$teller');\"><img border=\"0\" onclick=\"toggle('table$teller');\" src=\"./images/oft/pijlright.jpg\"> ".$dLand["LAND"]."</div>
                    <table class=\"oft_tabel\" id=\"table$teller\" style=\"display: none;\" width=\"100%\">
                      <tr>
                        <th style=\"width: 200px;\">Name</th>
                        <th style=\"width: 50px;\">Nr.</th>
                        <th style=\"width: 30px;\"><input type=\"checkbox\" $allchecked onClick=\"checkUncheckSome('Deel$teller', '$idlijst');\" name=\"Deel$teller\" id=\"Deel$teller\" value=\"Ja\" /></th>
                        <th style=\"width: 30px;\">Rights</th>
                        <th>Types</th>
                      </tr>
                    $contentLijst
                    <tr style=\"border-top: 1px solid #000000;\">
                      <td colspan=\"5\"></td>
                    </tr></table>";
      }
      $teller++;
    }
    
    $idlijstAll = substr($idlijstAll, 0, (strlen($idlijstAll)-1));

    $content  .= "<br/>
                  <br/><input type=\"checkbox\" checked=\"true\" onClick=\"checkUncheckSome('idlijstAll', '$idlijstAll');\" name=\"idlijstAll\" id=\"idlijstAll\" value=\"Ja\" /> check/uncheck all";
    return array($content, $iDs);
}

function generateOverzichtAdministraties($bedrijfsid, $userid)
{   global $pdo;

    $groups = "";
    if($_REQUEST["LETTERS"] != "")
    {
        $groups = " OR (BEDRIJFSNAAM like '%".ps($_REQUEST["LETTERS"])."%')";
        $query = $pdo->prepare('SELECT * FROM stam_bedrijfsgroep WHERE (NAAM like :naam);');
        $likeNaam = '%' . $_REQUEST["LETTERS"] . '%';
        $query->bindValue('naam', $likeNaam);
        $query->execute();

        foreach ($query->fetchAll() as $data6) {
           $groups .= " or (GROUPID*1) = '".($data6["ID"]*1)."'";
        }
        if($groups != "") { $groups = substr($groups, 3, strlen($groups)); }
    }
    overzichtadministraties($userid, $bedrijfsid, $groups, false);
}

function oft_users_edit_AJAX( $bedrijfsid, $userid ) {
  global $pdo, $db;

  // Haal userid en entityid op
  $userid = 0;
  $entityid = 0;
  if( isset( $_REQUEST['USERID'] ) && isset( $_REQUEST['BEDRIJFSID'] ) ) {
    $userid = $_REQUEST['USERID'];
    $entityid = $_REQUEST['BEDRIJFSID'];
  }

  // Kijk of we aan het tonen of verwerken zijn.
  if ( isset( $_REQUEST['VELDNAAM'] ) && isset( $_REQUEST['VALUE'] ) ) {
    // Verwerken
    $veldnaam = $_REQUEST['VELDNAAM'];
    $value = $_REQUEST['VALUE'];
    list( $table, $type, $id ) = explode( '_', $veldnaam );

    // Kijk of er al een record bestaat
    $query = $pdo->prepare('SELECT *
              FROM oft_rechten_tot
             WHERE USERID = :userid
               AND BEDRIJFSID = :bedrijfsid;');
    $query->bindValue('userid', $userid);
    $query->bindValue('bedrijfsid', $entityid);
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);

    if ($row !== false) {
      // Ja
      if ( array_key_exists( $type, $row ) ) {
        if ( $row[$type] != $value ) {
            db_query("UPDATE oft_rechten_tot SET `".ps($type)."` = '".ps($value)."' WHERE ID = '".ps($row['ID'], "nr")."';", $db);
        }
      }
    } else {
        db_query("insert into oft_rechten_tot ( USERID, BEDRIJFSID, ".ps($type).") VALUES ( '".ps($userid)."', '".ps($entityid)."', '".ps($value)."' );", $db);
    }
  }

  // Tonen
  $return = '';
  if ( isset( $_REQUEST['USERID'] ) && isset( $_REQUEST['BEDRIJFSID'] ) ) {
    $userid = $_REQUEST['USERID'];
    $entityid = $_REQUEST['BEDRIJFSID'];
    $return .= "<div style=\"position:absolute;right:0;top:0;\"><a onclick=\"sluiten( 'user_settings_popup_". ( $entityid* 1 ) . "' );\"><img src=\"./images/oft/deletebutton_normal.png\"</a></div>";
    $loginId = create_new_useraccount($userid, $entityid);

    $entities = array( 'Legal' => 'LEGAL', 'Compliance' => 'COMPLIANCE', 'Boardpack' => 'BOARDPACK', 'Finance' => 'FINANCE', 'Human Resources' => 'HR', 'Contracts' => 'CONTRACTS', 'Notifications e-mail' => 'MAILNOTIFICATIONS' );
    $rights = array( 'None' => "Nee", 'Read' => "Lees", 'Write & Read' => "Ja");

    $return .= "<table class=\"oft_tabel\">";

    $query = $pdo->prepare('SELECT *
                 FROM login
                WHERE ID = :loginid
                LIMIT 1;');
    $query->bindValue('loginid', $loginId);
    $query->execute();
    $dLogin = $query->fetch(PDO::FETCH_ASSOC);

    if($dLogin !== false) {
      foreach ( $entities as $e_label => $name ) {
        $teller = 0;
        $return .= '<tr><td class="oft_label">'.$e_label.'</td><td></td>';
        foreach ( $rights as $r_label => $rechtenLevel ) {
          //Specifieke rechten gaan voor op algemene rechten
          $checked = '';
          
          $query = $pdo->prepare('SELECT *
             FROM oft_rechten_tot
            WHERE USERID = :userid
              AND BEDRIJFSID = :bedrijfsid
            LIMIT 1;');
          $query->bindValue('userid', $userid);
          $query->bindValue('bedrijfsid', $entityid);
          $query->execute();
          $dRechtenTot = $query->fetch(PDO::FETCH_ASSOC);

          if($dRechtenTot !== false) {
            if (isset($dRechtenTot[$name]) &&  $dRechtenTot[$name] == $rechtenLevel ) {
              $checked = ' checked="checked"';
            }
          } else {
            if (isset($dLogin[$name]) &&  $dLogin[$name] == $rechtenLevel ) {
              $checked = ' checked="checked"';
            }
          }
          // uitzonderingen voor mailnotifications
          $tonen = true;
          if ($name == "MAILNOTIFICATIONS") {
            if ($rechtenLevel == "Lees") {
              $tonen = false;
            } elseif ($rechtenLevel == "Ja") {
                $r_label = "Yes";
            } elseif ($rechtenLevel == "Nee")  {
              $r_label = "No";
            }
          }
          // Radiobutton
          if ($tonen) {
            $return .=  "<td><input type=\"radio\" ".
              "onchange=\"getPageContent2('content.php?SITE=gen_oft_users_edit_AJAX&VELDNAAM='+ this.name+ '&VALUE='+ this.value+ '&USERID=".( $userid* 1 )."&BEDRIJFSID=".( $entityid* 1 ). "' , 'user_settings_popup_".($entityid*1)."', searchReq);\"".
              $checked.
              " name=\"login_". $name. "_". ( $entityid* 1 ).
              "\" value=\"$rechtenLevel\"/>".
              $r_label. "</td>";
          }

        }
        $return .= '</tr>';
        $teller++;
      }
      $return .= '<tr><td></td><td></td><td></td><td colspan="2">'.  '</td></tr>';
    }

    $return .= '</table>';
  }
  echo $return;
}