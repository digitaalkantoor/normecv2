<?php
sercurityCheck();

function oft_agenda($userid, $bedrijfsid) {
  global $db;

  $back_button     = oft_back_button_entity();

  $titel           = tl('Entities');
  $url_querystring = oft_tabel_querystring();
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_entitie_agenda_new', "New item");

  if(isset($_REQUEST["BID"])) {
    $bedrijfsid    = $_REQUEST["BID"];
  } else {
    $bedrijfsid    = 0;
  }

  $contentFrame    = agenda_script($bedrijfsid, 1000);
  $contentFrame    = "<div class='oft2_page_centering' style=\"padding-top: 50px;\">$contentFrame</div>";

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_entitie_agenda($userid, $bedrijfsid) {
  global $db;

  $back_button     = oft_back_button_entity();

  $titel           = tl('Entities');
  $url_querystring = oft_tabel_querystring();
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_entitie_agenda_new', "New item");

  if(isset($_REQUEST["BID"])) {
    $bedrijfsid    = $_REQUEST["BID"];
  }

  $contentFrame    = agenda_script($bedrijfsid);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_agenda_new($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_agenda.htm";
  $tabel = "alerts";

  echo "<h2>Add agenda</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_entitie_agenda_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_agenda.htm";
  $tabel = "alerts";

  $id = "";
  if(isset($_REQUEST["ID"])) {
      $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit agenda</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_entitie_agenda_delete($userid, $bedrijfsid) {
  global $db;

}

function agenda_script($bedrijfsid, $width = 730) {

  $return = "<script>
    $(document).ready(function() {

    $('#calendar').fullCalendar({
      events: 'content.php?SITE=generate_agenda_json&BID=".$bedrijfsid."',
      header: {
        left:   'title',
        center: '',
        right:  'month agendaWeek agendaFiveDay agendaDay prev next'
      },
      monthNames: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'July', 'Augustus', 'September', 'Oktober', 'November', 'December'],
      dayNames: ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'],
      dayNamesShort: ['Zon', 'Ma', 'Di', 'Woe', 'Don', 'Vr', 'Zat'],
      weekNumbers: true,
      columnFormat: {
        month: 'ddd',
        week: 'ddd D/M',
        day: ''
      },
      buttonText: {
        today: '" . tl("Today") . "',
        month: '" . tl("Month") . "',
        week: '" . tl("Week") . "',
        day: '" . tl("Day") . "'
      },
      axisFormat: 'H:mm',
      timeFormat: 'H:mm',
      defaultView: 'month',
      scrollTime: '08:00:00',
      slotDuration: '00:30:00',
      height: 800,
      firstDay: 7
    });

    $('#calendar').fullCalendar('gotoDate', $.fullCalendar.moment('" . date("Y-m-d") . "'));

    });

    </script>
    <div id='calendar' style='width: ".$width."px;'></div>";
    
    /*
,
      dayClick: function(date, allDay, jsEvent, view) {
        window.location.href = '".$dayClick."';
      }
    */
  
  return $return;
}

function generate_agenda_json($userid, $bedrijfsid) {
  global $db;

  $output = "";

  $toonUser = "Iedereen";
  //$toonUser = $_REQUEST["PERSLID"];

  $datumVan = date("Y-m-01");
  $datumTot = date("Y-m-30");

  if(isset($_REQUEST["start"]) && strlen($_REQUEST["start"]) == 10) {
    $start = ps($_REQUEST["start"], "date");
    $datumVan = $start;
  } elseif(isset($_REQUEST["start"])) {
    $start = ps($_REQUEST["start"]);
    $datumVan = date("Y-m-d", strtotime("+".$start." seconds", strtotime("1970-01-01")));
  }
  if(isset($_REQUEST["end"]) && strlen($_REQUEST["end"]) == 10) {
    $end = ps($_REQUEST["end"], "date");
    $datumTot = $end;
  } elseif(isset($_REQUEST["end"])) {
    $end = ps($_REQUEST["end"]);
    $datumTot = date("Y-m-d", strtotime("+".$end." seconds", strtotime("1970-01-01")));
  }
  
  if(isset($_REQUEST["BID"])) {
    $bedrijfsid = $_REQUEST["BID"];
  }

  $json_array = array();

  $toonUserLoginId = 0;
  $agendainhoud = dagOverzichtData($userid, $bedrijfsid, $datumVan, $datumTot);
  $toonDubbele = false;
  //krsort($agendainhoud);

  $even = true;

  foreach($agendainhoud As $agendaDagen) {
    foreach($agendaDagen As $agendaRegels) {
      foreach($agendaRegels As $uur => $agendaRegel) {
        if(strlen($uur) == 1) {
         $uur = "0".$uur;
        }

        $bgcolor = "#b1ddf2";
        if($agendaRegel["KLEUR"] != '') {
          $bgcolor = $agendaRegel["KLEUR"];
        }

        /*
        {events: [{title: 'Event1',start: '2011-04-04'},{title: 'Event2',start: '2011-05-05'}],color: 'yellow',textColor: 'black'}
        */
        $van = date("Y-m-d H:i:s", strtotime($agendaRegel["DATUM"] . ' '. $agendaRegel["VAN"]));
        if(strlen($agendaRegel["VAN"]) > 8) {
          $van = date("Y-m-d H:i:s", strtotime($agendaRegel["VAN"]));
        }
        
        $tot = date("Y-m-d H:i:s", strtotime($agendaRegel["DATUM"] . ' '. $agendaRegel["TOT"]));
        
        if(strlen($agendaRegel["VAN"]) > 8) {
          $tot = date("Y-m-d H:i:s", strtotime($agendaRegel["TOT"]));
        }

        $txt = $agendaRegel["TITEL"] . " ". $agendaRegel["OMSCHRIJVING"];
        $txt = strip_tags($txt);
        $txt = str_replace("&nbsp;", " ", $txt);
        $txt = verkort($txt, check('agenda_titellengte', 100, $bedrijfsid));

        $allDay = false;
        if(isset($agendaRegel["allDay"]) && $agendaRegel["allDay"] == "true") {
          $allDay = true;
        }

        $json_array[] = array("id" => lb($agendaRegel["ID"]),
                              "title" => lb($txt),
                              "resourceId" => ($toonUser*1),
                              "start" => $van,
                              "end" => $tot,
                              "textColor" => "#000",
                              "allDay" => $allDay,
                              "url" => html_entity_decode(lb($agendaRegel["BEWERKURL"])),
                             );
      }
    }
  }

  echo json_encode($json_array);
}

function verkorteBedrijfsnaam($bedrijfsid) {
  global $db;

  $naam = "";
  $sQl = "SELECT *
            FROM bedrijf
           WHERE ID = '".ps($bedrijfsid, "nr")."'
           LIMIT 1";
  $rEsUlT = db_query($sQl, $db);
  if ($dAtA = db_fetch_array($rEsUlT))
  {
    $naam = $dAtA["BEDRIJFNR"];
  }
  
  return $naam;
}

function dagOverzichtData($userid, $bedrijfsid, $datumVan, $datumTot)
{
  global $db;

  $dag   = getDag($datumVan);
  $maand = getMaand($datumVan);
  $jaar  = getJaar($datumVan);

  if(strlen($dag) == 1)   { $dag = "0". $dag; }
  if(strlen($maand) == 1) { $maand = "0". $maand; }

  $return      = array();

  $sqlBedrijfsId = "";
  if($bedrijfsid > 0) {
    $sqlBedrijfsId = "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'";
  }

  if(isRechten("BOARDPACK")) {
    //Boardmeetings


    $sAgenda = "SELECT *
                  FROM oft_boardpack
                 WHERE NOT DELETED = 'Ja'
                   $sqlBedrijfsId
                   AND DATUM <= '".ps($datumTot, "date")."'
                   AND DATUM >= '".ps($datumVan, "date")."'
                 ORDER BY DATUM";
    $rAgenda = db_query($sAgenda, $db);
    while ($dAgenda = db_fetch_array($rAgenda))
    {
      $van = $dAgenda["TIME"];
      $tot = date("H:i:s", strtotime("+1 hour", strtotime($van)));
      $type = verkorteBedrijfsnaam($dAgenda["BEDRIJFSID"]) . " | " . "Boardmeeting: ".$dAgenda["TYPE"];
  
      $return[str_replace("-", "", $dAgenda["DATUM"])][$van][] = array("VAN" => $van,
                                                                     "TOT" => $tot,
                                                                     "DATUM" => $dAgenda["DATUM"],
                                                                     "ID" => ($dAgenda["ID"]*1)."_oft_boardpack",
                                                                     "KLEUR" => '',
                                                                     "BEWERKURL" => "content.php?SITE=oft_boardpack_edit&ID=".($dAgenda["TABELID"]*1),
                                                                     "TITEL" => $type,
                                                                     "PERSLID" => getPersoneelsLidNaam($userid),
                                                                     "USERID" => $userid,
                                                                     "OMSCHRIJVING" => '');
    }
  }

  //Notifications
  $sAgenda = "SELECT *
                FROM notificaties
               WHERE NOT DELETED = 'Ja'
                 $sqlBedrijfsId
                 AND VERVALDATUM <= '".ps($datumTot, "date")."'
                 AND VERVALDATUM >= '".ps($datumVan, "date")."'
               ORDER BY VERVALDATUM";
  $rAgenda = db_query($sAgenda, $db);
  while ($dAgenda = db_fetch_array($rAgenda))
  {
    $van = "09:00:00";
    $tot = "10:00:00";
    $return[str_replace("-", "", $dAgenda["VERVALDATUM"])][$van][] = array("VAN" => $van,
                                                                   "TOT" => $tot,
                                                                   "DATUM" => $dAgenda["VERVALDATUM"],
                                                                   "ID" => ($dAgenda["ID"]*1)."_notificaties",
                                                                   "KLEUR" => '',
                                                                   "BEWERKURL" => "javascript:oft2OpenPopup();getPageContent2('content.php?SITE=".$dAgenda["TABEL"]."_edit&ID=".($dAgenda["TABELID"]*1)."', 'invoerschermContent', searchReq2);",
                                                                   "TITEL" => verkorteBedrijfsnaam($dAgenda["BEDRIJFSID"]) . " | " . lb($dAgenda['EVENT']),
                                                                   "PERSLID" => getPersoneelsLidNaam($dAgenda["USERID"]),
                                                                   "USERID" => $dAgenda["USERID"],
                                                                   "OMSCHRIJVING" => '');
  }

  $sAgenda = "SELECT *
                FROM alerts
               WHERE NOT DELETED = 'Ja'
                 $sqlBedrijfsId
                 AND DATUM <= '".ps($datumTot, "date")."'
                 AND DATUM >= '".ps($datumVan, "date")."'
               ORDER BY DATUM";
  $rAgenda = db_query($sAgenda, $db);
  while ($dAgenda = db_fetch_array($rAgenda))
  {
      $DATUM = ps($dAgenda["DATUM"], "date");

      $return[str_replace("-", "", $DATUM)][$dAgenda["TIJD"]][] = array("VAN" => $dAgenda["TIJD"],
                                                                     "TOT" => $dAgenda["TIJD"],
                                                                     "DATUM" => $DATUM,
                                                                     "ID" => ($dAgenda["ID"]*1)."_alerts",
                                                                     "KLEUR" => '',
                                                                     "BEWERKURL" => "javascript:oft2OpenPopup();getPageContent2('content.php?SITE=oft_entitie_agenda_edit&BID=$bedrijfsid&ID=".($dAgenda["ID"]*1)."', 'invoerschermContent', searchReq2);",
                                                                     "TITEL" => verkorteBedrijfsnaam($dAgenda["BEDRIJFSID"]) . " | " . lb($dAgenda['ITEM']),
                                                                     "PERSLID" => getPersoneelsLidNaam($dAgenda["ITEMOFUSERID"]),
                                                                     "USERID" => $dAgenda["ITEMOFUSERID"],
                                                                     "OMSCHRIJVING" => '');
  }

  return $return;
}