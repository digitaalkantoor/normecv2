<?php
sercurityCheck();

function oft_registers_view($userid, $bedrijfsid) {
  global $db;

  getRechten($userid, "CRM");

  $titel         = "Overview registers";
  $submenuitems  = "<a href=\"content.php?SITE=oft_registers_view&JAAR=".(date("Y")-2)."\">".(date("Y")-2)."</a>"
                   . ' | ' . "<a href=\"content.php?SITE=oft_registers_view&JAAR=".(date("Y")-1)."\">".(date("Y")-1)."</a>"
                   . ' | ' . "<a href=\"content.php?SITE=oft_registers_view&JAAR=".date("Y")."\">".date("Y")."</a>"
                   . ' | ' . oft_inputform_link('content.php?SITE=oft_registers_actiontracking_add', "Add actiontracking - non audit", 500)
                   . ' | ' . oft_inputform_link('content.php?SITE=oft_registers_actiontracking_audit_add', "Add actiontracking - audit", 500)
                   . ' | ' . oft_inputform_link('content.php?SITE=oft_registers_claim_add', "New Claim", 500)
                   . ' | ' . oft_inputform_link('content.php?SITE=oft_registers_incident_add', "New Incident", 500)
                   . ' | ' . oft_inputform_link('content.php?SITE=oft_registers_compliant_add', "Add complaint");
  $back_button   = "";

  $contentFrame  = oft_registers($userid, $bedrijfsid, "oft_action_tracking_audit", "ISSUEDATE", "DATECLOSE", "Action tracking - audit") . "<a href=\"content.php?SITE=oft_registers_actiontracking_audit\">Show all</a>";

  $contentFrame  .= oft_registers($userid, $bedrijfsid, "oft_action_tracking", "ISSUEDATE", "DATECLOSE", "Action tracking - non-audit") . "<a href=\"content.php?SITE=oft_registers_actiontracking\">Show all</a>";

  $contentFrame  .= oft_registers($userid, $bedrijfsid, "oft_incident_register", "DATEINCIDENT", "DATECLOSE", "Claim register", "AND SOORT = 'Claim'") . "<a href=\"content.php?SITE=oft_registers_claim\">Show all</a>";

  $contentFrame  .= oft_registers($userid, $bedrijfsid, "oft_incident_register", "DATEINCIDENT", "DATECLOSE", "Incident register", "AND NOT SOORT = 'Claim'") . "<a href=\"content.php?SITE=oft_registers_incident\">Show all</a>";

  $contentFrame  .= oft_registers($userid, $bedrijfsid, "oft_complaint", "ISSUEDATE", "DATECLOSE", "Complaint register") . "<a href=\"content.php?SITE=oft_registers_compliant\">Show all</a>";

  //$contentFrame  .= oft_registers($userid, $bedrijfsid, "oft_exceptions", "ISSUEDATE", "DATECLOSE", "Exceptions & Regulatory filings register") . "<a href=\"content.php?SITE=oft_registers_compliant\">Show all</a>";

  $contentFrame = "<div class='oft2_page_centering' style=\"padding-top: 50px;\">$contentFrame</div>";

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_registers($userid, $bedrijfsid, $tabel, $kolomInputDate, $kolomEndDate, $naam, $voorwaarde = ''){
  global $pdo, $db;

  $array_new    = array();
  $array_closed = array();
  $array_open   = array();
  $array_total_new    = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0);
  $array_total_closed = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0);
  $array_total_open   = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0);
  $array_landen = array();

  $jaar = date("Y");
  if(isset($_REQUEST["JAAR"])) {
    $jaar = $_REQUEST["JAAR"]*1;
  }

  $van = $jaar . "01";
  $tot = $jaar . "-12-01";
  $totMax = min(date("Ym", strtotime($jaar . "-12-01")), date("Ym"));

  $return       = "<h2>$naam $jaar</h2>";

  for($m = $van; $m <= $totMax; $m++) {
    $tableData = getTableData($tabel);
    if(!isset($tableData[$kolomInputDate]) || !isset($tableData[$kolomEndDate])) {
      throw new Exception("Incompatible table data.");
    }

    $sql          = "SELECT $kolomInputDate, $kolomEndDate, BEDRIJFSID from $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) $voorwaarde AND ($kolomInputDate <= '$jaar-".str_replace($jaar, "", $m)."-31' OR $kolomInputDate = '0000-00-00') AND ($kolomEndDate >= '$jaar-".str_replace($jaar, "", $m)."-01' OR $kolomEndDate = '0000-00-00');";
    //echo "<br/>$sql";
    $result       = db_query($sql, $db);
    while($data   = db_fetch_array($result))
    {
      $dataJaarInput = getJaar($data[$kolomInputDate]);
      $dataJaarEnd = getJaar($data[$kolomEndDate]);

      /*
      $land      = "";
      $queryLand = $pdo->prepare('SELECT LAND from bedrijf WHERE BEDRIJFSID = :bedrijfsid limit 1;');
      $queryLand->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
      $queryLand->execute();
      $dLand = $queryLand->fetch(PDO::FETCH_ASSOC);

      if(!$dLand) {
        $land    = $dLand["LAND"];
        $array_landen[$land] = $land;
      }
      */

      $land      = "";
      $rLand     = db_query("SELECT LAND from bedrijf WHERE BEDRIJFSID = '".ps($data["BEDRIJFSID"], "nr")."' limit 1;", $db);
      if($dLand  = db_fetch_array($rLand))
      {
        $land    = $dLand["LAND"];
        $array_landen[$land] = $land;
      }

       //echo "<br/>".(getMaand($data[$kolomEndDate])*1) . " == $m - ". $dataJaarEnd;

       if((getMaand($data[$kolomInputDate])*1) == str_replace($jaar, "", $m) && $dataJaarInput == $jaar
          && (getMaand($data[$kolomEndDate])*1) == str_replace($jaar, "", $m) && $dataJaarEnd == $jaar) {
         //Is open and closed
         if(isset($array_closed[$land][$m])) { $array_closed[$land][$m]++; }
         else                                { $array_closed[$land][$m] = 1; }

         if(isset($array_total_closed[$m])) {
           $array_total_closed[$m]++;
         } else {
           $array_total_closed[$m] = 1;
         }

         if(isset($array_new[$land][$m]))    { $array_new[$land][$m]++; }
         else                                { $array_new[$land][$m] = 1; }

         if(isset($array_total_new[$m])) {
           $array_total_new[$m]++;
         } else {
           $array_total_new[$m] = 1;
         }

         if(isset($array_open[$land][$m]))   { $array_open[$land][$m]--; }
         else                                { $array_open[$land][$m] = 0; }

         if(isset($array_total_open[$m])) {
           $array_total_open[$m]--;
         } else {
           $array_total_open[$m] = 0;
         }

       } elseif((getMaand($data[$kolomEndDate])*1) == str_replace($jaar, "", $m) && $dataJaarEnd == $jaar) {
         if(isset($array_closed[$land][$m])) { $array_closed[$land][$m]++; }
         else                                { $array_closed[$land][$m] = 1; }

         if(isset($array_total_closed[$m])) {
           $array_total_closed[$m]++;
         } else {
           $array_total_closed[$m] = 1;
         }
       } elseif((getMaand($data[$kolomInputDate])*1) == str_replace($jaar, "", $m) && $dataJaarInput == $jaar) {
         if(isset($array_new[$land][$m]))    { $array_new[$land][$m]++; }
         else                                { $array_new[$land][$m] = 1; }

         if(isset($array_total_new[$m])) {
           $array_total_new[$m]++;
         } else {
           $array_total_new[$m] = 1;
         }
       } else {
         if(isset($array_open[$land][$m]))   { $array_open[$land][$m]++; }
         else                                { $array_open[$land][$m] = 1; }

         if(isset($array_total_open[$m])) {
           $array_total_open[$m]++;
         } else {
           $array_total_open[$m] = 1;
         }
       }
    }
  }

  $return .= "<table class=\"orangefield_tabel\" cellpadding=\"3\" border=\"1\">";
  $return .= "<tr><td width=\"150\">&nbsp;</td>";
  for($m = $van; $m <= $totMax; $m++) {
    if(strlen($m) == 1) { $m = "0".$m; }

    $return .= "<th colspan=\"3\" align=\"center\" style=\"border-right: 2px solid #808080;\">".datumMaand("0000-".str_replace($jaar, "", $m)."-00")."</th>";
  }
  $return .= "</tr>";

  $return .= "<tr><th></th>";
  for($m = $van; $m <= $totMax; $m++) {
    $return .= "<th>N</th>";
    $return .= "<th>O</th>";
    $return .= "<th style=\"border-right: 2px solid #808080;\">C</th>";
  }
  $return .= "</tr>";


  /*
   * Get link to overview of register type
   **/
  $link = $tabel;
  if($link == "oft_action_tracking_audit") {
    $link = "oft_registers_actiontracking_audit";
  } elseif($link == "oft_action_tracking") {
    $link = "oft_registers_actiontracking";
  } elseif($link == "oft_incident_register" && $naam == "Claim register") {
    $link = "oft_registers_claim";
  } elseif($link == "oft_incident_register" && $naam == "Incident register") {
    $link = "oft_registers_incident";
  } elseif($link == "oft_complaint") {
    $link = "oft_registers_compliant";
  }

  foreach($array_landen As $land => $value) {

    $return .= "<tr><td><a href=\"content.php?SITE=$link&SRCH_LAND=$land\">$land</a></td>";
    for($m = $van; $m <= $totMax; $m++) {

      $new    = 0; if(isset($array_new[$land][$m]))    $new = $array_new[$land][$m];
      $open   = 0; if(isset($array_open[$land][$m]))   $open = $array_open[$land][$m];
      $closed = 0; if(isset($array_closed[$land][$m])) $closed = $array_closed[$land][$m];

      $colorNew = "";
      if($new == 0)    { $new = ""; } else { $colorNew = "style=\"background-color:".getColor("rood")."; color: #fff;\""; }
      if($open == 0)   { $open = ""; }
      if($closed == 0) { $closed = ""; }

      $open = $new + $open;// - $closed;


      $return .= "<td $colorNew><a href=\"content.php?SITE=$link&SHOWNEW=$m&SRCH_LAND=$land\">".$new."</a></td>";
      $return .= "<td><a href=\"content.php?SITE=$link&SHOWOPEN=$m&SRCH_LAND=$land\">".$open."</a></td>";
      $return .= "<td style=\"border-right: 2px solid #808080;\"><a href=\"content.php?SITE=$link&SHOWCLOSED=$m&SRCH_LAND=$land\">".$closed."</a></td>";
    }
    $return .= "</tr>";
  }


  $return .= "<tr><td align=\"right\">Total</td>";
    for($m = $van; $m <= $totMax; $m++) {

      $new    = 0; if(isset($array_total_new[$m]))    $new = $array_total_new[$m];
      $open   = 0; if(isset($array_total_open[$m]))   $open = $array_total_open[$m];
      $closed = 0; if(isset($array_total_closed[$m])) $closed = $array_total_closed[$m];

      $open = $new + $open;// - $closed;

      $colorNew = "";
      if($new == 0)    { $new = ""; } else { $colorNew = "style=\"background-color:".getColor("rood")."; color: #fff;\""; }
      if($open == 0)   { $open = ""; }
      if($closed == 0) { $closed = ""; }

      $return .= "<td $colorNew>".$new."</td>";
      $return .= "<td>".$open."</td>";
      $return .= "<td style=\"border-right: 2px solid #808080;\">".$closed."</td>";
    }
    $return .= "</tr>";

  $return .= "</table>";

  return $return;
}

function registers_showdate_url()
{
  $url_querystring = "";

  if(isset($_REQUEST["SHOWNEW"])) {
    $url_querystring .= "&SHOWNEW=".$_REQUEST["SHOWNEW"];
  }
  if(isset($_REQUEST["SHOWOPEN"])) {
    $url_querystring .= "&SHOWOPEN=".$_REQUEST["SHOWOPEN"];
  }
  if(isset($_REQUEST["SHOWCLOSED"])) {
    $url_querystring .= "&SHOWCLOSED=".$_REQUEST["SHOWCLOSED"];
  }

  return $url_querystring;
}

function registers_showdate($columnIssueDate = "ISSUEDATE") {
  $sql = "";

  if(isset($_REQUEST["SHOWNEW"])) {
    $date = $_REQUEST["SHOWNEW"];
    if(strlen($date) == 6) {
      $date = substr($date,0, 4) . "-". substr($date, -2)."-%";
      $sql = " AND $columnIssueDate LIKE '$date' ";
    }
  } elseif(isset($_REQUEST["SHOWCLOSED"])) {
    $date = $_REQUEST["SHOWCLOSED"];
    if(strlen($date) == 6) {
      $date = substr($date,0, 4) . "-". substr($date, -2)."-%";
      $sql = " AND DATECLOSE like '".$date."' ";
    }
  } elseif(isset($_REQUEST["SHOWOPEN"])) {
    $date = $_REQUEST["SHOWOPEN"];
    if(strlen($date) == 6) {
      $dateVan = substr($date,0, 4) . "-". substr($date, -2)."-01";
      $dateTot = substr($date,0, 4) . "-". substr($date, -2)."-31";
      $dateTot2 = substr($date,0, 4) . "-". substr($date, -2)."-%";
      $sql = " AND ($columnIssueDate <= '".$dateTot."' OR $columnIssueDate = '0000-00-00') AND (DATECLOSE >= '".$dateVan."' OR DATECLOSE = '0000-00-00') AND NOT DATECLOSE like '".$dateTot2."' ";
    }
  }

  //echo $sql;

  return $sql;
}

/* Non Audit */
function oft_registers_actiontracking($userid, $bedrijfsid) {
  global $db;

  $titel         = tl('Action tracking non-audit');
  $submenuitems  = oft_inputform_link('content.php?SITE=oft_registers_actiontracking_add', "Add actiontracking");
  $back_button   = "";

  $tabel           = "oft_action_tracking";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_action_tracking_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_actiontracking_edit&ID=-ID-");
  $verwijderen     = "";
  $sql             = registers_showdate();

  $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_action_tracking", 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button, $tabel, $tabel . "_overzicht", $toevoegen, $bewerken);
}

function oft_registers_actiontracking_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_action_tracking";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_action_tracking_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_actiontracking_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_actiontracking_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_action_tracking";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_action_tracking_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_actiontracking_edit&ID=-ID-");
  $verwijderen     = "";

  echo "<h1>Action tracking non-audit</h1><br/><br/>";

  $sql             = registers_showdate();

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_action_tracking", 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_actiontracking_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_action_tracking.htm";
  $tabel = "oft_action_tracking";

  echo "<h2>Add action tracking non-audit</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_registers_actiontracking_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_action_tracking.htm";
  $tabel = "oft_action_tracking";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Action Tracking non-audit</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_action_tracking_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_action_tracking_print.htm";
  $tabel = "oft_action_tracking";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Action Tracking non-audit</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

/*audit */
function oft_registers_actiontracking_audit($userid, $bedrijfsid) {
  global $db;

  $titel         = tl('Action tracking audit');
  $submenuitems  = oft_inputform_link('content.php?SITE=oft_registers_actiontracking_audit_add', "Add actiontracking");
  $back_button   = "";

  $tabel           = "oft_action_tracking_audit";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_actiontracking_audit_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_action_tracking_audit", 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button, $tabel, $tabel . "_overzicht", $toevoegen, $bewerken);
}

function oft_registers_actiontracking_audit_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_action_tracking_audit";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_actiontracking_audit_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_action_tracking_audit", 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_actiontracking_audit_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_action_tracking_audit";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo "<h1>Action tracking audit</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_action_tracking_audit", 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_actiontracking_audit_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_action_tracking_audit.htm";
  $tabel = "oft_action_tracking_audit";

  echo "<h2>Add action tracking audit</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_registers_actiontracking_audit_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_action_tracking_audit.htm";
  $tabel = "oft_action_tracking_audit";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Action Tracking Audit</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_action_tracking_audit_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_action_tracking_audit_print.htm";
  $tabel = "oft_action_tracking_audit";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Action Tracking Audit</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_registers_incident($userid, $bedrijfsid) {
  global $db;

  $titel           = tl('Incident register');
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_registers_incident_add', "New Incident");
  $back_button     = "";

  $tabel           = "oft_incident_register";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_actiontracking_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_incident_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate("DATEINCIDENT") . " AND SOORT = 'Incident' ";

  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button, $tabel, $tabel."_overzicht", $toevoegen, $bewerken);
}

function oft_registers_incident_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_incident_register";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_actiontracking_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_incident_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate("DATEINCIDENT") . " AND SOORT = 'Incident' ";

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_incident_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_incident_register";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  $sql             = registers_showdate("DATEINCIDENT") . " AND SOORT = 'Incident' ";

  echo "<h1>Incident register</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_incident_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_registers_incident.htm";
  $tabel = "oft_incident_register";

  echo "<h2>Add Incident</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_registers_incident_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_registers_incident.htm";
  $tabel = "oft_incident_register";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Incident</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_incident_register_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_incident_register_1_print.htm";
  $tabel = "oft_incident_register";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Incident</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_registers_compliant($userid, $bedrijfsid) {
  global $db;

  $titel           = tl('Compliant register');
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_registers_compliant_add', "Add complaint");
  $back_button     = "";

  $tabel           = "oft_complaint";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_complaint_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_compliant_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button, $tabel, $tabel . "_overzicht", $toevoegen, $bewerken);
}

function oft_registers_compliant_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_complaint";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_complaint_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_compliant_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_compliant_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_complaint";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo "<h1>Complaint register</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_compliant_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_complaint.htm";
  $tabel = "oft_complaint";

  echo "<h2>Add Compliant</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_registers_compliant_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_complaint.htm";
  $tabel = "oft_complaint";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Complaint</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_complaint_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_complaint_print.htm";
  $tabel = "oft_complaint";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Compliant</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

// Exceptions

function oft_registers_exceptions($userid, $bedrijfsid) {
  global $db;

  $titel           = tl('Exceptions & Regulatory filings register');
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_registers_exceptions_add', "Add exception");
  $back_button     = "";

  $tabel           = "oft_exceptions";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_exceptions_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_exceptions_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_registers_exceptions_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_exceptions";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_exceptions_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_exceptions_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_exceptions_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_exceptions";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo "<h1>Exceptions & Regulatory filings register</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_exceptions_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_exceptions.htm";
  $tabel = "oft_exceptions";

  echo "<h2>Add Exceptions & Regulatory filings register</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_registers_exceptions_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_exceptions.htm";
  $tabel = "oft_exceptions";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Exceptions & Regulatory filings register</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_exceptions_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_exceptions_print.htm";
  $tabel = "oft_exceptions";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Exceptions & Regulatory filings register</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_registers_claim($userid, $bedrijfsid) {
  global $db;

  $titel           = tl('Claim register');
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_registers_claim_add', "New Claim");
  $back_button     = "";

  $tabel           = "oft_incident_register";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_registers_claim_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_claim_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate("DATEINCIDENT") . " AND SOORT = 'Claim'";

  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button, $tabel, $tabel."_overzicht", $toevoegen, $bewerken);
}

function oft_registers_claim_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_incident_register";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_registers_claim_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_claim_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate("DATEINCIDENT") . " AND SOORT = 'Claim'";

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_claim_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_incident_register";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  $sql             = registers_showdate("DATEINCIDENT") . " AND SOORT = 'Claim'";

  echo "<h1>Claim register</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_claim_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_registers_incident.htm";
  $tabel = "oft_incident_register";

  echo "<h2>Add Claim</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_registers_claim_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_registers_incident.htm";
  $tabel = "oft_incident_register";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Claim</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_claim_register_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_claim_register_1_print.htm";
  $tabel = "oft_incident_register";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Claim</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}
