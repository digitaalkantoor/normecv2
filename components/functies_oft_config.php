<?php
sercurityCheck();

function oft_mastertables($userid, $bedrijfsid) {
  global $db;

  if(!checkProfile("Administrator")) {
    rd("content.php?SITE=oft_home");
  }

  if(isset($_REQUEST["ID"])) {
    $bedrijfsid = $_REQUEST["ID"];
  }

  $titel           = 'Configuration';
  $submenuitems    = '';
  $back_button     = '';
  $contentFrame    = '<h2>Master tables</h2><br/><br/>';
  
  $contentFrame   .= "<label class=\"oft_label\">Master table</lable>&nbsp;&nbsp;<select name=\"table\" id=\"stam_tabel\" class=\"fieldHalf\" onChange=\"getPageContent2('content.php?SITE=oft_stam_ajax&TABEL='+document.getElementById('stam_tabel').value, 'ajax_content', searchReq2);\">
                        <option value=\"\"></option>
                        <option value=\"oft_capacity\">Capacity</option>
                        <option value=\"stam_ordners\">Category</option>
                        <option value=\"stam_contracttype\">Contract types</option>
                        <option value=\"oft_classes_types\">Classes types</option>
                        <option value=\"country\">Country</option>
                        <option value=\"valuta\">Currencies</option>
                        <option value=\"stam_documenttype\">Document types</option>
                        <option value=\"stam_rechtsvormen\">Legal form</option>
                        <option value=\"oft_proxy\">Proxy</option>
                        <option value=\"oft_transaction_types\">Classes of share</option>
                        <option value=\"stam_trainingsstatus\">Training status</option>
                        <option value=\"stam_risklevel\">Risk level</option>
                      </select>
                      <br/><br/><div id=\"ajax_content\"></div>";
//<option value=\"stam_compliantstatus\">Risk Assessment status</option>
//<option value=\"oft_department\">Department</option>

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_stam_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "";

  if(isset($_REQUEST["TABEL"]) && trim($_REQUEST["TABEL"]) != "") {
    $tabel = ps($_REQUEST["TABEL"]);
  }

  if($tabel != "") {
      
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

    $toevoegen       = "content.php?SITE=oft_stam_add&TABEL=$tabel";
    $bewerken        = oft_inputform_link_short("content.php?SITE=oft_stam_edit&TABEL=$tabel&ID=-ID-");
    $verwijderen     = "";

    if(!isset($_REQUEST["PAGNR"])) {
      echo oft_inputform_link($toevoegen, "New item", 400);
      echo "<br/><br/>";
    }

    $ToonVeld = "NAAM";
    if($tabel == "oft_classes") {
      $ToonVeld = "PARVALUE";
    }

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, $ToonVeld, $toevoegen, $bewerken, $verwijderen);
  }
}

function oft_stam_add($userid, $bedrijfsid) {
  global $db;

  $templateFile    = "./templates/oft_stam.htm";
  $tabel           = "";

  if(isset($_REQUEST["TABEL"]) && trim($_REQUEST["TABEL"]) != "") {
    $tabel = ps($_REQUEST["TABEL"]);
  }

  if($tabel == "stam_documenttype") {
    $templateFile = "./templates/oft_stam_documenttype.htm";
  }

  echo "<h2>New item</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_stam_edit($userid, $bedrijfsid) {
  global $db;

  $tabel           = "";
  if(isset($_REQUEST["TABEL"]) && trim($_REQUEST["TABEL"]) != "") {
    $tabel = ps($_REQUEST["TABEL"]);
  }
  
  // template switch
  switch($tabel) {
    case 'stam_documenttype':
      $templateFile = "./templates/oft_stam_documenttype.htm";
    break;
    case 'country':
        $templateFile = "./templates/oft_stam_country.htm";
    break;
    default:
    $templateFile = "./templates/oft_stam.htm";
  }

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit item</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function extra_velden($userid, $bedrijfsid) {
  global $pdo;

  $titel           = 'Configuration';
  $submenuitems    = '';
  $back_button     = '';
  
  if(isset($_REQUEST["ADDFIELDOPSLAAN"])) {
    $query = $pdo->prepare('SELECT * from extra_velden WHERE TABEL = "bedrijf" limit 5;');
    $query->execute();

    foreach ($query->fetchAll() as $data) {
      $queryExtraVelden = $pdo->prepare('UPDATE extra_velden SET VELDOMSCHRIJVING = :veldomschrijving, ACTIVE = :active WHERE ID = :id LIMIT 1');
      $queryExtraVelden->bindValue('veldomschrijving', $_REQUEST["VELDOMSCHRIJVING".($data["ID"]*1)]);
      $queryExtraVelden->bindValue('active', $_REQUEST["ACTIVE".($data["ID"]*1)]);
      $queryExtraVelden->bindValue('id', $data["ID"]);
      $queryExtraVelden->execute();
    }
    rd("content.php?SITE=extra_velden&MENUID=".$_REQUEST["MENUID"]."&BID=$bedrijfsid");
  }

  $contentFrame    = "<h2>Additional fields</h2><br/><br/><form method=\"Post\" action=\"\"><table class=\"oft_table\"><tr><th>Field</th></tr>";

  $query = $pdo->prepare('SELECT * from extra_velden WHERE TABEL = "bedrijf" LIMIT 5;');
  $query->execute();

  foreach ($query->fetchAll() as $data) {
     $checkbox     = "";
     if($data["ACTIVE"] == "Ja") {
       $checkbox   = "checked=\"true\""; 
     }
     $contentFrame .= "<tr><td><input type=\"text\" class=\"field\" name=\"VELDOMSCHRIJVING".($data["ID"]*1)."\" value=\"".$data["VELDOMSCHRIJVING"]."\" /></td>
                           <td><input type=\"checkbox\" $checkbox name=\"ACTIVE".($data["ID"]*1)."\" value=\"Ja\" /> Active</td></tr>";
  }
  
  $contentFrame   .= "<tr><td></td><td><br/><br/><input type=\"submit\" name=\"ADDFIELDOPSLAAN\" value=\"Save\" class=\"button\" /></table></form>";

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_notifications($userid, $bedrijfsid) {
  global $pdo;
  
  if(!checkProfile("Administrator")) {
    rd("content.php?SITE=oft_home");
  }

  if(isset($_REQUEST["ID"])) {
    $bedrijfsid = $_REQUEST["ID"];
  }

  if(isset($_REQUEST["OPSLAAN"])) {
    $pdo->query("DELETE FROM notificatie_schema;");
    oft_notifications_insert_schema("oft_tax", $bedrijfsid);
    oft_notifications_insert_schema("oft_annual_accounts", $bedrijfsid);
    oft_notifications_insert_schema("contracten", $bedrijfsid);
    oft_notifications_insert_schema("personeel", $bedrijfsid);
    oft_notifications_insert_schema("oft_bedrijf_adres", $bedrijfsid);

    if(check('USA2EUROPE', 'Nee', $bedrijfsid) == 'Ja') {
      oft_notifications_insert_schema("oft_entitie_accounting", $bedrijfsid);
    }
    rd("content.php?SITE=oft_notifications");
  }

  $titel           = 'Configuration';
  $submenuitems    = '';
  $back_button     = '';
  $contentFrame    = '<h2>Notifications</h2><br/><br/>';
  
  $contentFrame   .= "<form action=\"\" method=\"Post\"><table class=\"oft_tabel\">";
  $tabel           = "oft_tax";
  $contentFrame   .= oft_notifications_schema($tabel, $bedrijfsid, "Tax filings");
  $tabel           = "oft_annual_accounts";
  $contentFrame   .= oft_notifications_schema($tabel, $bedrijfsid, "Annual account");
  $tabel           = "contracten";
  $contentFrame   .= oft_notifications_schema($tabel, $bedrijfsid, "Due date contract");
  $tabel           = "personeel";
  $contentFrame   .= oft_notifications_schema($tabel, $bedrijfsid, "Reelection date");
  $tabel           = "oft_bedrijf_adres";
  $contentFrame   .= oft_notifications_schema($tabel, $bedrijfsid, "End date address");

  if(check('USA2EUROPE', 'Nee', $bedrijfsid) == 'Ja') {
    $tabel           = "oft_entitie_accounting";
    $contentFrame   .= oft_notifications_schema($tabel, $bedrijfsid, "USA2EUROPE");
  }

  $contentFrame   .= "<tr><td align=\"right\" colspan=\"3\"><br/><input type=\"submit\" name=\"OPSLAAN\" value=\"Save\" class=\"button\" /></td></tr>";
  $contentFrame   .= "</table></form>";

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_notifications_insert_schema($tabel, $bedrijfsid) {
  global $pdo;
  
  $query = $pdo->prepare('INSERT INTO notificatie_schema SET BEDRIJFSID = :bedrijfsid,
                                               UPDATEDATUM = :updatedatum,
                                               TABEL       = :tabel,
                                               DAG         = :dag');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->bindValue('updatedatum', date("Y-m-d"));
  $query->bindValue('tabel', $tabel);
//  $query->bindValue('maand', $_REQUEST[$tabel."_MAAND"]);
  $query->bindValue('dag', $_REQUEST[$tabel."_DAG"]);
  $query->execute();
}

function oft_notifications_schema($tabel, $bedrijfsid, $titel) {
  global $pdo;
  
  $maand        = '';
  $dag          = '';

  $query = $pdo->prepare('SELECT MAAND, DAG from notificatie_schema WHERE TABEL = :tabel limit 1;');
  $query->bindValue('tabel', $tabel);
  
  $query->execute();
  
  $data = $query->fetch(PDO::FETCH_ASSOC);
  if ($data)
  {
    $maand      = $data["MAAND"];
    $dag        = $data["DAG"];
  }

  return "<tr>
            <td class=\"oft_label\">$titel</td>
            <td><input type=\"text\" name=\"".$tabel."_MAAND\" class=\"field4\" value=\"$maand\" /> Months</td>
            <td><input type=\"text\" name=\"".$tabel."_DAG\" class=\"field4\" value=\"$dag\"  /> Days</td>
         </tr>";
}
