<?php
/**
 *
 * Function that accepts RESTful methods and routing with Classes.
 * How it's used:
 * 1. Add a Controller to /Controllers with a CamelCase name ending with .class.php (copy ExampleToCopy.class.php)
 * 2. Add a __construct and index method to the new Controller Class
 * 3. Add the new Controller name (snake_case) to the $controllers (array) in this function
 * 4. Navigate to ?SITE=your_controller_name_in_snake_case
 *
 * Optional stuff
 * * if the url ends with _ajax, the controller will get is as a variable
 * * if you add a method .e.g. update, then you can call it in your url by added the method name after the controller
 * name
 *
 * @param $site
 * @param $user
 * @param $company
 */
function route($site, $user, $company)
{
    // Default options for methods
    $optionDefaults = array(
        'ajax' => false,
        'popup' => false
    );

    // All available controllers
    $controllers = array('file_management', 'entity', 'import');

    foreach ($controllers as $controller) {

        // Get clean options
        $options = $optionDefaults;

        // Does the url not begin with the controller name?
        if (!str_starts_with($site, $controller)) {
            continue;
        }

        // Is the request a popup
        if ($options['popup'] = str_ends_with($site, '_popup')) {
            $site = str_replace('_popup', '', $site);
        }

        // Is the request a ajax request
        if ($options['ajax'] = str_ends_with($site, '_ajax')) {
            $site = str_replace('_ajax', '', $site);
        }

        // What kind of action does the url give
        $action = lowerCamelCase(preg_replace('/^' . $controller . '_/', '', $site));

        // Only show head if not ajax request
        if (!$options['ajax'] && !$options['popup']) {
            head_oft($user, $company, $site);
        }

        // Convert url controller_name to ClassName
        $className = CamelCase($controller);
        $classFile = 'Controllers/' . $className . '.class.php';

        // Does the Class file exists?
        if (!file_exists($classFile)) {
            echo '<h1>Controller not found</h1>';
            echo '<br /><b>Input:</b> ' . $controller;
            echo '<br /><b>Expected file location:</b> ' . $classFile;
            echo '<br /><b>Possible causes:</b> wrong controller slug in new_route() or controller class file deleted';
            return;
        }

        include_once $classFile;
        $class = 'Controllers\\' . $className;

        if (isset($_REQUEST['BID']) && $_REQUEST['BID']) {
            $company = $_REQUEST['BID'];
        }

        // Create new instance of Class
        $class = new $class($user, $company, $options);

        // Check if action exists as a method in the Class
        if (method_exists($class, $action)) {
            echo $class->$action();
        } else {

            // Check if index method exists for default behaviour
            if (method_exists($class, 'index')) {
                echo $class->index();
            } else {
                echo '<h1>Controller method not found</h1>';
                echo '<br /><b>Input:</b> ' . $action;
                echo '<br /><b>Expected static method:</b> ' . $class . '::' . $action . '();';
                echo '<br /><b>No default method:</b> No index (default) method found in ' . $class;
                echo '<br /><b>Possible solutions:</b> add default method named index to controller class.';
                return;
            }
        }

        // Only show footer if not ajax request
        if (!$options['ajax']) {
            footer1();
        }
    }
}