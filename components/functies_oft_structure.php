<?php
sercurityCheck();

function oft_get_structure($userid, $bedrijfsid, $tabel, $kolom) {

  $arrayVelden     = array();
  $arrayVeldnamen  = array();
  $arrayVeldType   = array();
  $arrayZoekvelden = array();
  $arrayVerplicht  = array();

  if($tabel == "contracten_overzicht") {
    $arrayVelden     = array(0 => "REFERENTIE",    1 => "NAAM",          2 => "DATUMSTOP",    3 => "BEDRIJFSID",   4 => "",        5 => "" );
    $arrayVeldnamen  = array(0 => "Reference",     1 => "Name",          2 => "Due date",     3 => check("default_name_of_entity", "Entity", 1),       4 => "Country", 5 => "File");
    $arrayVeldType   = array(0 => "field",         1 => "field",         2 => "datum",        3 => "bedrijfsnaam", 4 => "country", 5 => "bestand_from_type");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "COUNTRY_TABLE", 2 => "TYPE",         3 => "SEARCH");
  } elseif($tabel == "contracten") {
    $arrayVelden     = array(0 => "REFERENTIE",    1 => "NAAM",          2 => "DATUMSTART",   3 => "DATUMSTOP",    4 => "SKIP_BEDRIJFSNAAM",   5 => "KOSTEN",  6 => "ONDERTEKENDDOOR",  7 => "HOE",        8 => "BESTAND", 9 => "SKIP_ADD_FILES_TYPE", 10 => "SKIP_ADD_FILES", 11 => "SKIP_LIST_FILES", 12 => "DEPARTMENT",      13 => "SIGNATORY", 14 => "VERLENING", 15 => "VERLENGINGTERMIJN", 16 => "VERNIEUW", 17 => "VERNIEUWTERMIJN", 18 => "VALUTA", 19 => "BEDRIJFSID", 20 => "TYPE");
    $arrayVeldType   = array(0 => "volgnummering", 1 => "field",         2 => "datum",        3 => "datumleeg",    4 => "bedrijfsnaam",        5 => "bedrag",  6 => "field",            7 => "selectjanee",8 => "bestand", 9 => "SKIP_ADD_FILES_TYPE", 10 => "SKIP_ADD_FILES", 11 => "SKIP_LIST_FILES", 12 => "oft_department",  13 => "SIGNATORY", 14 => "field",     15 => "termijn",           16 => "field",    17 => "termijn",         18 => "valuta", 19 => "bedrijf",    20 => "stam_contracttype");
    $arrayVerplicht  = array(0 => "Ja",            1 => "Nee",           2 => "Nee",          3 => "Nee",          4 => "Nee",                 5 => "Nee",     6 => "Nee",              7 => "Nee",        8 => "Nee",     9 => "Nee",                 10 => "Nee",            11 => "Nee",             12 => "Nee",             13 => "Nee",       14 => "Nee",       15 => "Nee",               16 => "Nee",      17 => "Nee",             18 => "Nee",    19 => "Nee",        20 => "Nee");
  } elseif($tabel == "oft_clients_overzicht") {
    $arrayVelden     = array(0 => "OFFICE",        1 => "NAMECLIENT",    2 => "NAMEUBO",      3 => "DATEREJECTION", 4 => "REASON" );
    $arrayVeldnamen  = array(0 => "Office",        1 => "Name client",   2 => "Name ubo",     3 => "Date rejection",4 => "Reason");
    $arrayVeldType   = array(0 => "field",         1 => "field",         2 => "field",        3 => "datum",         4 => "field");
    $arrayZoekvelden = array(0 => "SEARCH");
  } elseif($tabel == "oft_clients") {
    $arrayVelden     = array(0 => "OFFICE",        1 => "NAMECLIENT",    2 => "NAMEUBO",      3 => "DATEREJECTION", 4 => "REASON" );
    $arrayVeldType   = array(0 => "field",         1 => "field",         2 => "field",        3 => "datum",         4 => "field");
    $arrayVerplicht  = array(0 => "Ja",            1 => "Ja",            2 => "Ja",           3 => "Ja",           4 => "Ja");
  } elseif($tabel == "oft_action_tracking_overzicht") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "REFKEY",        2 => "FINDING",      3 => "STATUS",       4 => "ISSUEDATE",  5 => "UPDATEDATUM");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference",     2 => "Brief summary",3 => "Status",       4 => "Issue date", 5 => "Updated");
    $arrayVeldType   = array(0 => "country",       1 => "field",         2 => "field",        3 => "field",        4 => "datum",      5 => "datum");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "COUNTRY_TABLE", 2 => "STATUS",       3 => "SEARCH");
  } elseif($tabel == "oft_action_tracking") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "REFKEY",        2 => "FINDING",      3 => "STATUS",       4 => "ISSUEDATE",  5 => "AANBEVELING",    6 => "PRIORITY", 7 => "ACTIONTAKEN",  8 => "DATECLOSE",  9 => "ACTIONTAKENTYPE",  10 => "PROVISION");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference key", 2 => "Brief summary",3 => "Status",       4 => "Issue date", 5 => "Recommendation", 6 => "Priority", 7 => "Actions taken", 8 => "Date close", 9 => "Type of action",   10 => "Provision amount made");
    $arrayVeldType   = array(0 => "country",       1 => "volgnummering", 2 => "field",        3 => "reg_status",   4 => "datum",      5 => "field",          6 => "PRIORITY", 7 => "field",        8 => "datumleeg",  9 => "reg_typeofaction", 10 => "field");
    $arrayVerplicht  = array(0 => "Nee",           1 => "Nee",           2 => "Nee",          3 => "Nee",          4 => "Nee",        5 => "Nee",            6 => "Nee",      7 => "Nee",          8 => "Nee",        9 => "Nee",              10 => "Nee");
  } elseif($tabel == "oft_action_tracking_audit_overzicht") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "REFKEY",        2 => "FINDING",      3 => "STATUS",       4 => "ISSUEDATE",  5 => "UPDATEDATUM");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference",     2 => "Brief summary",3 => "Status",       4 => "Issue date", 5 => "Updated");
    $arrayVeldType   = array(0 => "country",       1 => "field",         2 => "field",        3 => "field",        4 => "datum",      5 => "datum");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "COUNTRY_TABLE", 2 => "STATUS",       3 => "SEARCH");
  } elseif($tabel == "oft_action_tracking_audit") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "REFKEY",        2 => "FINDING",      3 => "STATUS",       4 => "ISSUEDATE",  5 => "AANBEVELING",    6 => "PRIORITY", 7 => "ACTIONTAKEN",  8 => "DATECLOSE",  9 => "ACTIONTAKENTYPE",  10 => "PROVISION");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference key", 2 => "Brief summary",3 => "Status",       4 => "Issue date", 5 => "Recommendation", 6 => "Priority", 7 => "Actions taken", 8 => "Date close", 9 => "Type of action",   10 => "Provision amount made");
    $arrayVeldType   = array(0 => "country",       1 => "volgnummering", 2 => "field",        3 => "reg_status",   4 => "datum",      5 => "field",          6 => "PRIORITY", 7 => "field",        8 => "datumleeg",  9 => "reg_typeofaction", 10 => "field");
    $arrayVerplicht  = array(0 => "Nee",           1 => "Nee",           2 => "Nee",          3 => "Nee",          4 => "Nee",        5 => "Nee",            6 => "Nee",      7 => "Nee",          8 => "Nee",        9 => "Nee",              10 => "Nee");
  } elseif($tabel == "oft_incident_register_overzicht") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "INCIDENTNR",    2 => "SOORT",            3 => "STATUS",   4 => "DATEINCIDENT",  5 => "UPDATEDATUM");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference key",  2 => "Incidents or claim", 3 => "Status",   4 => "Date incident", 5 => "Updated");
    $arrayVeldType   = array(0 => "country",       1 => "field",         2 => "field",            3 => "field",    4 => "datum",         5 => "datum");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "COUNTRY_TABLE", 2 => "STATUS",           3 => "SOORT",    4 => "SEARCH");
  } elseif($tabel == "oft_incident_register") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "INCIDENTNR",    2 => "SKIP_X",       3 => "STATUS",       4 => "DATEINCIDENT",  5 => "INPUTDATE",  6 => "BRIEFSUMMARY",  7 => "DESCRIPTION", 8 => "PARTIESINVOLVED",  9 => "SOORT",              10 => "POTENTIALLOSSAMOUNT",   11 => "CHANCECLAIMSUCCESFULL",   12 => "RECOVERABLEFROMINSURANCE",   13 => "STRATEGY",  14 => "SKIP_INCIDENT_ACTIONS", 15 => "DATECLOSE",  16 => "OWNER",  17 => "PROVISION",             18 => "INCIDENTREFNR",             19 => "SKIP_ADD_FILES", 20 => "SKIP_LIST_FILES", 21 => "LEGAL_COUNSEL_APPOINTED", 22 => "LEGAL_COUNSEL_NAME",        23 => "FEE_QOUTE", 24 => "LEGAL_EXPENSES",);
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference key",  2 => "SKIP_X",       3 => "Status",      4 => "Date incident", 5 => "Input date", 6 => "Brief summary", 7 => "Description", 8 => "Parties involved", 9 => "Incidents or claim",   10 => "Potential loss amount", 11 => "Chance claim succesfull", 12 => "Recoverable from insurance", 13 => "Strategy",  14 => "Actions taken",       15 => "Date close", 16 => "Owner",  17 => "Provision amount made", 18 => "Incident Reference Number", 19 => "SKIP_ADD_FILES", 20 => "SKIP_LIST_FILES", 21 => "Legal counsel appointed", 22 => "Name of the legal counsel", 23 => "Fee quote", 24 => "Legal expenses occurred to date",);
    $arrayVeldType   = array(0 => "country",       1 => "volgnummering", 2 => "fieldLang",    3 => "reg_status",   4 => "datum",         5 => "datum",      6 => "fieldLang",     7 => "fieldLang",   8 => "fieldLang",        9 => "incident_soort",     10 => "fieldLang",             11 => "fieldLang",               12 => "reg_recoverable",            13 => "fieldLang", 14 => "SKIP_INCIDENT_ACTIONS", 15 => "datumleeg",  16 => "owner",  17 => "field",                 18 => "field",                     19 => "SKIP_ADD_FILES", 20 => "SKIP_LIST_FILES", 21 => "selectjanee",             22 => "field",                     23 => "field",     24 => "field",);
    $arrayVerplicht  = array(0 => "Nee",           1 => "Nee",           2 => "Nee",          3 => "Nee",          4 => "Nee",           5 => "Nee",        6 => "Ja",            7 => "Nee",         8 => "Nee",              9 => "Ja",                10 => "Nee",                   11 => "Nee",                     12 => "Nee",                        13 => "Nee",       14 => "Nee",                   15 => "Nee",        16 => "Nee",    17 => "Nee",                   18 => "Nee",                       19 => "Nee",            20 => "Nee",             21 => "Nee",                     22 => "Nee",                       23 => "Nee",       24 => "Nee");
  } elseif($tabel == "oft_complaint_overzicht") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "REFKEY",        2 => "DESCRIPTION",  3 => "STATUS",       4 => "ISSUEDATE",  5 => "UPDATEDATUM");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference key", 2 => "Description",  3 => "Status",       4 => "Issue date", 5 => "Updated");
    $arrayVeldType   = array(0 => "country",       1 => "field",         2 => "field",        3 => "field",        4 => "datum",      5 => "datum");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "COUNTRY_TABLE", 2 => "STATUS",       3 => "SEARCH");
  } elseif($tabel == "oft_complaint") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "REFKEY",        2 => "DESCRIPTION", 3 => "STATUS",        4 => "ISSUEDATE",  5 => "ACTION", 6 => "OWNER",  7 => "DATECLOSE",  8 => "PROVISION");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Reference",     2 => "Description", 3 => "Status",        4 => "Issue date", 5 => "Action", 6 => "Owner",  7 => "Date close", 8 => "Provision amount made");
    $arrayVeldType   = array(0 => "land",          1 => "volgnummering", 2 => "field",       3 => "reg_status",    4 => "datum",      5 => "field",  6 => "owner",  7 => "datumleeg",  8 => "field");
    $arrayVerplicht  = array(0 => "Nee",           1 => "Nee",           2 => "Nee",         3 => "Nee",           4 => "Nee",        5 => "Nee",    6 => "Nee",    7 => "Nee",        8 => "Nee");
  } elseif($tabel == "oft_exceptions_overzicht") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "EXCEPTION",     2 => "DESCRIPTION",  3 => "STATUS",       4 => "ISSUEDATE",  5 => "UPDATEDATUM");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Type",          2 => "Description",  3 => "Status",       4 => "Issue date", 5 => "Updated");
    $arrayVeldType   = array(0 => "country",       1 => "field",         2 => "field",        3 => "field",        4 => "datum",      5 => "datum");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "COUNTRY_TABLE", 2 => "STATUS",       3 => "SEARCH");
  } elseif($tabel == "oft_exceptions") {
    $arrayVelden     = array(0 => "COUNTRY",       1 => "EXCEPTION",     2 => "DESCRIPTION", 3 => "STATUS",        4 => "ISSUEDATE",  5 => "ACTIONREQUIRED", 6 => "DATECLOSE");
    $arrayVeldnamen  = array(0 => "Country",       1 => "Type",          2 => "Description", 3 => "Status",        4 => "Issue date", 5 => "Action",         6 => "Date close");
    $arrayVeldType   = array(0 => "land",          1 => "field",         2 => "field",       3 => "reg_status",    4 => "datum",      5 => "field",          6 => "datumleeg");
    $arrayVerplicht  = array(0 => "Nee",           1 => "Nee",           2 => "Nee",         3 => "Nee",           4 => "Nee",        5 => "Nee",            6 => "Nee");
  } elseif($tabel == "oft_entities_usa2eu_overzicht") {
    $arrayVelden     = array(0 => "BEDRIJFSNAAM",
                             1 => "LAND",
                             2 => "HQLAND",
                             3 => "RECHTSVORM",
                             4 => "ENDOFBOOKYEAR",
                             5 => "OPRICHTINGSDATUM",
                             6 => "STATE",
                             7 => "ENDDATUM", //Is dit goed?
                             //8 => "LAND", //Lodgement Date
                             //9 => "LAND", //Payment Date",
                             10 => "TFN", //TFN",
                             11 => "ABN", //ABN",
                             12 => "PAYGW", //PAYGW",
                             13 => "VAT", //VAT",
                             //14 => "LAND", //Last BAS filing date",
                             //15 => "ESSFILING", //ESS filing date"
                             );

    $arrayVeldnamen     = array(0 => "Company",
                             1 => "Country",
                             2 => "HQ Country",
                             3 => "Legal form",
                             4 => "Fiscal Year End",
                             5 => "Incorporation Date",
                             6 => "State",
                             7 => "Expiry date",
                             //8 => "Lodgement Date",
                             //9 => "Payment Date",
                             10 => "TFN",
                             11 => "ABN",
                             12 => "PAYGW",
                             13 => "VAT",
                             //14 => "Last BAS filing date",
                             //15 => "ESS filing date"
                             );
                             //oft_entitie_accounting

    $arrayVeldType     = array(0 => "field",
                             1 => "field",
                             2 => "field",
                             3 => "stam_rechtsvormen",
                             4 => "Fiscal Year End",
                             5 => "field",
                             6 => "field",
                             7 => "field",
                             //8 => "field",
                             //9 => "field",
                             10 => "oft_entitie_numbers_TFN",
                             11 => "oft_entitie_numbers_ABN",
                             12 => "oft_entitie_numbers_PAYGW",
                             13 => "oft_entitie_numbers_VAT",
                             //14 => "field",
                             //15 => "field"
                             );

    $arrayZoekvelden = array(0 => "ENTITY",       1 => "COUNTRY", 2 => "ENDREDEN");
  } elseif($tabel == "oft_entities_overzicht") {
    $arrayVelden     = array(0 => "BEDRIJFNR",     1 => "BEDRIJFSNAAM",                                                                 2 => "LAND",        3 => "ENDREDEN");
    $arrayVeldnamen  = array(0 => "Number",        1 => check("default_name_of_entity", "Entity", 1),       2 => "Country",      3 => "Status");
    $arrayVeldType   = array(0 => "field",         1 => "field",                                                                        2 => "field",       3 => "field");
    $arrayZoekvelden = array(0 => "ENTITY",        1 => "COUNTRY",                                                                      2 => "ENDREDEN");
  } elseif($tabel == "oft_entities") {
    $arrayVelden     = array(0 => "ID",            1 => "BEDRIJFSNAAM",  2 => "LAND");
    $arrayVeldType   = array(0 => "field",         1 => "field",         2 => "field");
    $arrayVerplicht  = array(0 => "Nee",           1 => "Ja",            2 => "Nee");
  } elseif($tabel == "alerts") {
    $arrayVelden     = array(0 => "ID",            1 => "ITEM",  2 => "CHECKED", 3 => "ITEMOFUSERID", 4 => "DATUM", 5 => "TIJD");
    $arrayVeldType   = array(0 => "field",         1 => "field", 2 => "field",   3 => "field",        4 => "field", 5 => "field");
    $arrayVerplicht  = array(0 => "Nee",           1 => "Nee",   2 => "Nee",     3 => "Nee",          4 => "Nee",   5 => "Nee");
  } elseif($tabel == "oft_tax_overzicht_all") {
    $arrayVelden     = array(0 => "STATUS",        1 => "BEDRIJFSID",    2 => "FINANCIALYEAR",     3 => "STANDARDDUEDATE",   4 => "EXTENTION", 5 => "STATUS", 6 => "FILE",                      7 => "FILE");
    $arrayVeldnamen  = array(0 => "",              1 => check("default_name_of_entity", "Entity", 1),        2 => "Fiscal year",    3 => "Standard due date", 4 => "Extension", 5 => "Status", 6 => "File",                      7 => "Quantity");
    $arrayVeldType   = array(0 => "status",        1 => "bedrijfsnaam",  2 => "field",             3 => "datum",             4 => "datum",     5 => "field",  6 => "bestand_in_year_from_type", 7 => "nr_of_docs");
    $arrayZoekvelden = array(0 => "ENTITY",        1 => "YEAR",          2 => "STATUS");
  }  elseif($tabel == "oft_tax") {
    $arrayVelden     = array(0 => "STATUS",        1 => "FINANCIALYEAR", 2 => "STANDARDDUEDATE",   3 => "TAGS", 4 => "MONTH", 5 => "EXTENTION",6 => "FILINGDATE", 7 => "STATUS", 8 => "SKIP_ADD_FILES_TYPE",       9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES");
    $arrayVeldType   = array(0 => "status",        1 => "field",         2 => "datum",             3 => "field", 4 => "maanden", 5 => "datum", 6 => "datum",      7 => "field",  8 => "SKIP_ADD_FILES_TYPE",       9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES");
    $arrayVerplicht  = array(0 => "Nee",            1 => "Nee",         2 => "Nee",             3 => (check('DIF', 'Nee', getMyBedrijfsID()) == 'Ja' ? "Ja" : "Nee"), 4 => "Nee", 5 => "Nee", 6 => "Nee", 7 => "Nee", 8 => "Nee", 9 => "Nee", 10 => "Nee");
  } elseif($tabel == "oft_boardpack_overzicht") {
    $arrayVelden     = array(0 => "FINANCIALYEAR", 1 => "DATUM",         2 => "DATUMSEND", 4 => "NAME",             5 => "TYPE");
    $arrayVeldnamen  = array(0 => "Fiscal year",1 => "Date meeting",  2 => "Date sent", 4 => "Name",             5 => "" );
    $arrayVeldType   = array(0 => "field",         1 => "datum",         2 => "datum",     4 => "field",            5 => "boardpacklink");
  } elseif($tabel == "oft_boardpack") {
    $arrayVelden     = array(0 => "FINANCIALYEAR", 1 => "DATUM",         2 => "DATUMSEND", 4 => "TYPE",                 5 => "SKIP_AGENDA", 6 => "SKIP_INVITEES", 7 => "TIME", 8 => "NAME");
    $arrayVeldnamen  = array(0 => "Fiscal year",1 => "Date meeting",  2 => "Date sent", 4 => "Management Board",     5 => "SKIP_AGENDA", 6 => "SKIP_INVITEES", 7 => "TIME", 8 => "Name");
    $arrayVeldType   = array(0 => "alleenjarenlijst",    1 => "datum",   2 => "datum",     4 => "type_managementboard", 5 => "SKIP_AGENDA", 6 => "SKIP_INVITEES", 7 => "TIME", 8 => "field");
  } elseif($tabel == "oft_boardmeetings") {
    $arrayVelden     = array(0 => "STATUS",        1 => "FINANCIALYEAR", 2 => "STANDARDDUEDATE",   3 => "DATUM",     4 => "TYPE",   5 => "SKIP_AGENDA");
    $arrayVeldType   = array(0 => "status",        1 => "field",         2 => "datum",             3 => "datum",     4 => "type_managementboard",   5 => "SKIP_AGENDA");
  }  elseif($tabel == "oft_annual_accounts_overzicht_all") {
    $arrayVelden     = array(0 => "STATUS",        1 => "BEDRIJFSID",    2 => "FINANCIALYEAR",     3 => "STANDARDDUEDATE",   4 => "EXTENTION", 5 => "STATUS", 6 => "FILE",                      7 => "FILE",           8 => "AUDIT");
    $arrayVeldnamen  = array(0 => "",              1 => check("default_name_of_entity", "Entity", 1),        2 => "Fiscal year",    3 => "Standard due date", 4 => "Extension", 5 => "Status", 6 => "File",                      7 => "Quantity",       8 => "Audit");
    $arrayVeldType   = array(0 => "status",        1 => "bedrijfsnaam",  2 => "field",             3 => "datum",             4 => "datum",     5 => "field",  6 => "bestand_in_year_from_type", 7 => "nr_of_docs",     8 => "translate");
    $arrayZoekvelden = array(0 => "ENTITY",        1 => "YEAR",          2 => "STATUS");
  } elseif($tabel == "oft_annual_accounts") {
    $arrayVelden     = array(0 => "STATUS",        1 => "FINANCIALYEAR", 2 => "STANDARDDUEDATE",   3 => "EXTENTION", 4 => "MONTH",   5 => "TAGS",  6 => "FILINGDATE", 7 => "STATUS", 8 => "SKIP_ADD_FILES_TYPE", 9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES", 11 => "AUDIT");
    $arrayVeldType   = array(0 => "status",        1 => "field",         2 => "datum",             3 => "datum",     4 => "maanden", 5 => "field", 6 => "datum",      7 => "field",  8 => "SKIP_ADD_FILES_TYPE", 9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES", 11 => "selectjanee");
    $arrayVerplicht  = array(0 => "Nee",           1 => "Nee",           2 => "Nee",            3 => "Nee",       4 => "Nee",     5 => (check('DIF', 'Nee', getMyBedrijfsID()) == 'Ja' ? "Ja" : "Nee"), 6 => "Nee", 7 => "Nee",  8 => "Nee", 9 => "Nee", 10 => "Nee", 11 => "Nee");
  } elseif($tabel == "oft_entitie_transactions_overzicht") {
    $arrayVelden     = array(0 => "DATUM",         1 => "BEDRIJFSID",    2 => "ADMINID",           3 => "AMOUNT",    4 => "DEED");
    $arrayVeldnamen  = array(0 => "Datum",         1 => "From",          2 => "To",                3 => "Amount",    4 => "Deed");
    $arrayVeldType   = array(0 => "datum",         1 => "bedrijfsnaam",  2 => "bedrijfsnaam",      3 => "field",     4 => "bestandsnaam");
  } elseif($tabel == "oft_entitie_contracts_overzicht") {
    $arrayVelden     = array(0 => "REFERENTIE",    1 => "NAAM",          2 => "DATUMSTOP",         3 => "KOSTEN",    4 => "VALUTA");
    $arrayVeldnamen  = array(0 => "Reference",     1 => "Name",          2 => "Due date",          3 => "Total cost",4 => "Currency");
    $arrayVeldType   = array(0 => "field",         1 => "field",         2 => "datum",             3 => "field",     4 => "field");
  } elseif($tabel == "oft_entitie_accounting") {
    $arrayVelden     = array(0 => "AUDITWAIVERFILED",    1 => "CLIENTACCOUNTINGSYSTEM",     2 => "CLIENTACCOUNTINGSYSTEM_CUSTOM",   3 => "BAS",     4 => "BASPAYMENT",  5 => "ESS",         6 => "ESSFILING",   7 => "FBT",         8 => "FBTFILING",   9 => "FBTPAYMENT",  10 => "FISCALYEAREND_SAP",   11 => "MRP",         12 => "PAYROLLTAX",    13 => "PAYROLLTAXEXPIRES",  14 => "PAYROLLTAXSTATE",15 => "SERVICEPROVIDER",  16 => "WORKERSCOMP", 17 => "WORKERSCOMPEXPIRES_VIC", 18 => "WORKERSCOMPEXPIRES_NSW", 19 => "WORKERSCOMPEXPIRES_QLD", 20 => "WORKERSCOMPEXPIRES_TAS", 21 => "WORKERSCOMPEXPIRES_WA", 22 => "WORKERSCOMPEXPIRES_SA",
        23 => "SERVICETYPE", 24 => "SERVICEPROVIDER_ACCOUNTING" , 25 => "SERVICEPROVIDER_PAYROLL", 26 => "SERVICEPROVIDER_CORPSEC", 27 => "SERVICEPROVIDER_AUDITOR", 28 => "SERVICEPROVIDER_LEGAL" , 29 => "SERVICEPROVIDER_VISA", 30 => "INCOMETAXFILING" , 31 => "INCOMETAXPAYMENT");
    $arrayVeldnamen  = array(0 => "Audit Waiver filed",  1 => "Client Accounting System",   2 => "Client Accounting System Custom");
    $arrayVeldType   = array(0 => "datum",               1 => "accounting_systems",         2 => "field",                           3 => "datum",   4 => "datum",       5 => "selectjanee", 6 => "datum",       7 => "selectjanee", 8 => "datum",       9 => "datum",       10 => "field",               11 => "selectjanee", 12 => "selectjanee",   13 => "datum",              14 => "field",          15 => "service_provider", 16 => "selectjanee", 17 => "datum",                  18 => "datum",                  19 => "datum",                  20 => "datum",                  21 => "datum",                 22 => "datum",
        23 => "service_type",24 => "field",                      25 => "field",                    26 => "field",                   27 => "field",                   28 => "field",                  29 => "field",                30 => "datum",            31 => "datum");
  } elseif($tabel == "oft_bedrijf_adres") {
    $arrayVelden     = array(0 => "TYPE",          1 => "ADRES",         2 => "POSTCODE",   3 => "PLAATS", 4 => "LAND", 5 => "TELEFOON", 6 => "FAX",   7 => "STARTDATE", 8 => "STOPDATE",  9 => "SKIP_EXISTING");
    $arrayVeldType   = array(0 => "adres_type",    1 => "field",         2 => "field",      3 => "field",  4 => "land", 5 => "field",    6 => "field", 7 => "datum",     8 => "datumleeg", 9 => "SKIP_EXISTING");
    $arrayVerplicht  = array(0 => "Ja",            1 => "Nee",           2 => "Nee",        3 => "Nee",    4 => "Nee",  5 => "Nee",      6 => "Nee",   7 => "Nee",       8 => "Nee",       9 => "Nee");
  } elseif($tabel == "oft_proxyholders_overzicht") {
    $arrayVelden     = array(0 => "USERID",        1 => "PROXY",         2 => "DATEOFAPPOINTMENT",  3 => "ENDDATE",);
    $arrayVeldnamen  = array(0 => "Name",          1 => "Authorization", 2 => "Date of appointment",3 => "End date");
    $arrayVeldType   = array(0 => "person_name",   1 => "field",         2 => "datum",              3 => "datum");
  } elseif($tabel == "oft_proxyholders") {
    $arrayVelden     = array(0 => "USERID",        1 => "PROXY",         2 => "DATEOFAPPOINTMENT",  3 => "ENDDATE",);
    $arrayVeldType   = array(0 => "personeel",     1 => "field",         2 => "datum",              3 => "datum");
    $rrayVerplicht  = array(0 => "Nee",           1 => "Ja",            2 => "Ja",                 3 => "Nee");
  } elseif($tabel == "oft_transactions_overzicht" || $tabel == "oft_participations_overzicht" || $tabel == "oft_all_transactions_overzicht") {
    $arrayVelden     = array(0 => "DATUM",          1 => "ADMINID",      2 => "ADMINIDFROM",      3 => "CLASSTYPE",      4 => "PERSENTAGE");
    $arrayVeldnamen  = array(0 => "Date",           1 => "From",         2 => "To",               3 => "Class of share", 4 => "Ownership");
    $arrayVeldType   = array(0 => "datum",          1 => "bedrijfsnaam", 2 => "bedrijfsnaam",     3 => "CLASSTYPE",      4 => "persentage");
    $arrayZoekvelden = array(0 => "CLASSTYPE",      1 => "DATUM_VAN",    2 => "DATUM_TOT");
  } elseif($tabel == "oft_transactions") {
    $arrayVelden     = array(0 => "ADMINID",        1 => "AMOUNT",       2 => "CLASSTYPE",     3 => "SKIP_BEDRIJFSNAAM", 4 => "DATUM", 5 => "AMOUNT", 5 => "PERSENTAGE", 6 => "QUANTITY", 7 => "SKIP_AMOUNTPERSENTAGE", 8 => "SKIP_ADD_FILES_TYPE", 9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES", 11 => "ADMINIDFROM", 12 => "TYPE");
    $arrayVeldType   = array(0 => "newentity",      1 => "field",        2 => "CLASSTYPE",     3 => "bedrijfsnaam",      4 => "datum", 5 => "bedrag", 5 => "getal",      6 => "getal",    7 => "AMOUNTPERSENTAGE",      8 => "SKIP_ADD_FILES_TYPE", 9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES", 11 => "newentity",   12 => "TRANSACTIONTYPE");
    $arrayVerplicht  = array(0 => "Ja",             1 => "Nee",          2 => "Nee",           3 => "Nee",               4 => "Nee",   5 => "Nee",    5 => "Nee",        6 => "Nee",      7 => "Nee",                   8 => "Nee",                 9 => "Nee",            10 => "Nee",             11 => "Nee",         12 => "Nee");
  } elseif($tabel == "oft_shareholders_overzicht") {
    $arrayVelden     = array(0 => "ADMINID",        1 => "CLASSTYPE",      2 => "QUANTITY",         3 => "PERSENTAGE");
    $arrayVeldnamen  = array(0 => "Shareholder",    1 => "Class of share", 2 => "Number of shares", 3 => "Ownership");
    $arrayVeldType   = array(0 => "bedrijfsnaam",   1 => "CLASSTYPE",      2 => "field",            3 => "persentage");
  } elseif($tabel == "oft_shareholders") {
    $arrayVelden     = array(0 => "ADMINID",        1 => "AMOUNT",       2 => "CLASSTYPE",     3 => "SKIP_BEDRIJFSNAAM", 4 => "DATUM", 5 => "AMOUNT", 5 => "PERSENTAGE", 6 => "QUANTITY", 7 => "SKIP_AMOUNTPERSENTAGE", 8 => "SKIP_ADD_FILES_TYPE", 9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES", 11 => "ADMINIDFROM", 12 => "TYPE");
    $arrayVeldType   = array(0 => "newentity",      1 => "field",        2 => "CLASSTYPE",     3 => "bedrijfsnaam",      4 => "datum", 5 => "bedrag", 5 => "getal",      6 => "getal",    7 => "AMOUNTPERSENTAGE",      8 => "SKIP_ADD_FILES_TYPE", 9 => "SKIP_ADD_FILES", 10 => "SKIP_LIST_FILES", 11 => "newentity",   12 => "TRANSACTIONTYPE");
    $arrayVerplicht  = array(0 => "Ja",             1 => "Nee",          2 => "Nee",           3 => "Nee",               4 => "Nee",   5 => "Nee",    5 => "Nee",        6 => "Nee",      7 => "Nee",                   8 => "Nee",                 9 => "Nee",            10 => "Nee",             11 => "Nee",         12 => "Nee");
  } elseif($tabel == "oft_finance_overzicht") {
    $arrayVelden     = array(0 => "NAAM",          1 => "JAAR",          2 => "DOCTYPE",      3 => "BEDRIJFSID",   4 => "BESTANDSNAAM");
    $arrayVeldnamen  = array(0 => "Name",          1 => "Year",          2 => "Category",     3 => check("default_name_of_entity", "Entity", 1),       4 => "File");
    $arrayVeldType   = array(0 => "field",         1 => "field",         2 => "field",        3 => "bedrijfsnaam", 4 => "bestand");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "YEAR",          2 => "MONTH",        3 => "DOCTYPE",      4 => "UPDATEDBY");
  } elseif($tabel == "oft_hr_overzicht") {
    $arrayVelden     = array(0 => "NAAM",          1 => "PERSONEELSLID", 2 => "JAAR",         3 => "DOCTYPE",      4 => "BEDRIJFSID",   5 => "BESTANDSNAAM");
    $arrayVeldnamen  = array(0 => "Name",          1 => "Employee",      2 => "Year",         3 => "Category",     4 => check("default_name_of_entity", "Entity", 1),       5 => "File");
    $arrayVeldType   = array(0 => "field",         1 => "person_name",   2 => "field",        3 => "field",        4 => "bedrijfsnaam", 5 => "bestand");
    $arrayZoekvelden = array(0 => "ENTITY_TABLE",  1 => "YEAR",          2 => "MONTH",        3 => "DOCTYPE",      4 => "COUNTRY",      5 => "EMPLOYEE");
  }
  elseif($tabel == "documentbeheer") {
    $arrayVelden     = array(0 => "NAAM",          1 => "DATUM",         2 => "ORDNER",       3 => "STOPDATUM",    4 =>"SKIP_BEDRIJFSNAAM",    5 => "BESTANDSNAAM", 6 => "DOCTYPE",   7 => "TAGS",         8 => "SKIP_ADD_FILES", 9 => "SKIP_LIST_FILES", 10 => "JAAR", 11 => "SKIP_UPLOADED_BY", 12 => "SKIP_VERSIONS", 13 => "SKIP_FILE_HISTORY", 14 => "BEDRIJFSID", 15 => "MONTH",    16 => "COUNTRY", 17 => "EMPLOYEE");
    if(check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
      $arrayVeldType   = array(0 => "field",         1 => "datum",         2 => "stam_ordners", 3 => "datumleeg",    4 => "bedrijfsnaam",        5 => "field",        6 => "DOCTYPE",   7 => "field",        8 => "SKIP_ADD_FILES", 9 => "SKIP_LIST_FILES", 10 => "jaar", 11 => "SKIP_UPLOADED_BY", 12 => "SKIP_VERSIONS", 13 => "SKIP_FILE_HISTORY", 14 => "bedrijf",    15 => "maanden",  16 => "land",    17 => "personeel");
      $arrayVerplicht  = array(0 => "Ja",            1 => "Ja",            2 => "Nee",          3 => "Nee",          4 => "Nee",                 5 => "Nee",          6 => "Nee",       7 => "Ja",          8 => "Ja",            9 => "Nee",             10 => "Nee",  11 => "Nee",              12 => "Nee",           13 => "Nee",               14 => "Nee",        15 => "Nee",      16 => "Nee",     17 => "Nee");
    } else {
      $arrayVeldType   = array(0 => "field",         1 => "datum",         2 => "stam_ordners", 3 => "datumleeg",    4 => "bedrijfsnaam",        5 => "field",        6 => "DOCTYPE",   7 => "field",        8 => "SKIP_ADD_FILES", 9 => "SKIP_LIST_FILES", 10 => "jaar", 11 => "SKIP_UPLOADED_BY", 12 => "SKIP_VERSIONS", 13 => "SKIP_FILE_HISTORY", 14 => "bedrijf",    15 => "maanden",  16 => "land",    17 => "personeel");
      $arrayVerplicht  = array(0 => "Ja",            1 => "Ja",            2 => "Nee",          3 => "Nee",          4 => "Nee",                 5 => "Nee",          6 => "Nee",       7 => "Nee",          8 => "Nee",            9 => "Nee",             10 => "Nee",  11 => "Nee",              12 => "Nee",           13 => "Nee",               14 => "Nee",        15 => "Nee",      16 => "Nee",     17 => "Nee");
    }
  }
  elseif($tabel == "oft_entitie_finance_overzicht") {
    $arrayVelden     = array(0 => "NAAM",           1 => "JAAR",         2 => "DOCTYPE",       3 => "BESTANDSNAAM");
    $arrayVeldnamen  = array(0 => "Name",           1 => "Year",   2 => "Category",      3 => "File");
    $arrayVeldType   = array(0 => "field",          1 => "field",        2 => "field",         3 => "bestand");
    $arrayZoekvelden = array(0 => "YEAR",           1 => "MONTH",        2 => "DOCTYPE",       3 => "UPDATEDBY");
  } elseif($tabel == "oft_entitie_hr_overzicht") {
    $arrayVelden     = array(0 => "NAAM",           1 => "PERSONEELSLID",2 => "JAAR",          3 => "DOCTYPE",     4 => "BESTANDSNAAM");
    $arrayVeldnamen  = array(0 => "Name",           1 => "Employee",     2 => "Year",          3 => "Category",    4 => "File");
    $arrayVeldType   = array(0 => "field",          1 => "person_name",  2 => "field",         3 => "field",       4 => "bestand");
    $arrayZoekvelden = array(0 => "YEAR",           1 => "MONTH",        2 => "DOCTYPE",       3 => "COUNTRY",     4 => "EMPLOYEE");
  } elseif($tabel == "oft_classes_overzicht") {
    $arrayVelden     = array(0 => "TYPE",           1 => "ISSUEDQUANTITY",    2 => "PARVALUE", 3 => "VOTINGRIGHTS");
    $arrayVeldnamen  = array(0 => "Type",           1 => "Issued number",   2 => "Par Value",3 => "Voting rights");
    $arrayVeldType   = array(0 => "type_classes",   1 => "getal",             2 => "getal",    3 => "getal");
  } elseif($tabel == "oft_classes") {
    $arrayVelden     = array(0 => "TYPE",           1 => "ISSUEDQUANTITY",    2 => "PARVALUE", 3 => "VOTINGRIGHTS");
    $arrayVeldType   = array(0 => "type_classes",   1 => "getal",             2 => "getal",    3 => "getal");
    $arrayVerplicht  = array(0 => "Nee",            1 => "Nee",               2 => "Nee",      3 => "Nee");
  } elseif($tabel == "bedrijf") {
    $arrayVelden     = array(0 => "BEDRIJFNR",      1 => "BEDRIJFSNAAM",      2 => "OMSCHRIJVING", 3 => "LAND",  4 => "HANDELSNAAM", 5 => "RECHTSVORM",          6 => "FIRSTBOOKYEAR",            7 => "ENDDATUM", 8 => "ENDREDEN", 9 => "SKIP_ADD_FILES_TYPE", 10 => "SKIP_ADD_FILES", 11 => "SKIP_LIST_FILES", 12 => "ENDOFBOOKYEAR",         13 => "OPRICHTINGSDATUM",   14 => "MONITOR",                                     15 => "INTRIMREPORTING",   16 => "HAVECAPITAL",        17 => "INTERN",       18 => "EXTRAVELD1", 19 => "EXTRAVELD2", 20 => "EXTRAVELD3", 21 => "EXTRAVELD4", 22 => "EXTRAVELD5", 23 => "LABEL",       24 => "BOARDMEETINGS", 25 => "COMPOSITIONBOARD", 26 => "TYPE",        27 => "INTRIMREPORTPERIOD",      28 => "ENTITYNATURAL",       29 => "TAXPERIODMONTH", 30 => "TAXPERIODDAY", 31=> "AGMPERIODMONTH", 32 => "AGMPERIODDAY", 33 => "ANAPERIODMONTH", 34 => "ANAPERIODDAY",   35 => "GIIN",  36 => "LEI",   37 => "ABN", 38 => "REGISTEREDNUMBER");
    $arrayVeldnamen  = array(0 => "Number",         1 => "Name",              2 => "Purpose",   3 => "Country",  4 => "Commercial name", 5 => "Legal form",      6 => "First year of monitoring", 7 => "ENDDATUM", 8 => "ENDREDEN", 9 => "SKIP_ADD_FILES_TYPE", 10 => "SKIP_ADD_FILES", 11 => "SKIP_LIST_FILES", 12 => "End of financial year", 13 => "Incorporation date", 14 => "Monitor the tax filings and annual accounts", 15 => "Interim reporting", 16 => "Entity has capital", 17 => "In group",     18 => "EXTRAVELD1", 19 => "EXTRAVELD2", 20 => "EXTRAVELD3", 21 => "EXTRAVELD4", 22 => "EXTRAVELD5", 23 => "LABEL",       24 => "BOARDMEETINGS", 25 => "COMPOSITIONBOARD", 26 => "Type",        27 => "Intrim reporting period", 28 => "Is a natural person", 29 => "TAXPERIODMONTH", 30 => "TAXPERIODDAY", 31=> "AGMPERIODMONTH", 32 => "AGMPERIODDAY", 33 => "ANAPERIODMONTH", 34 => "ANAPERIODDAY",   35 => "GIIN",  36 => "LEI",   37 => "ABN", 38 => "Registered number");
    $arrayVeldType   = array(0 => "volgnummering",  1 => "field",             2 => "field",     3 => "land",     4 => "field",       5 => "stam_rechtsvormen",   6 => "jarenlijst",               7 => "datum",    8 => "field",    9 => "SKIP_ADD_FILES_TYPE", 10 => "SKIP_ADD_FILES", 11 => "SKIP_LIST_FILES", 12 => "endfinyear",            13 => "datum",              14 => "selectjanee",                                 15 => "selectjanee",       16 => "selectjanee",        17 => "selectjanee",  18 => "extraveld",  19 => "extraveld",  20 => "extraveld",  21 => "extraveld",  22 => "extraveld",  23 => "stam_labels", 24 => "boardmeetings", 25 => "field",            26 => "selectjanee", 27 => "termijn",                 28 => "selectjanee",         29 => "field",          30 => "field",        31=> "field",          32 => "field",        33 => "field",          34 => "field",          35 => "field", 36 => "field", 37 => "field", 38 => "field");
    $arrayVerplicht  = array(0 => "Ja",             1 => "Ja",                2 => "Nee",       3 => "Nee",      4 => "Nee",         5 => "Nee",                 6 => "Nee",                      7 => "Nee",      8 => "Nee",      9 => "Nee",                 10 => "Nee",            11 => "Nee",             12 => "Nee",                   13 => "Nee",                14 => "Nee",                                         15 => "Nee",               16 => "Nee",                17 => "Nee",          18 => "Nee",        19 => "Nee",        20 => "Nee",        21 => "Nee",        22 => "Nee",        23 => "Nee",         24 => "Nee",           25 => "Nee",              26 => "Nee",         27 => "Nee",                     28 => "Nee",                 29 => "Nee",            30 => "Nee",          31=> "Nee",            32 => "Nee",          33 => "Nee",            34 => "Nee",            35 => "Nee",   36 => "Nee",   37 => "Nee", 38 => "Nee");
  } elseif($tabel == "oft_management_nonboard_overzicht") {
    $arrayVelden     = array(0 => "USERID",         1 => "FUNCTION",          3 => "AUTHORIZATION", 4 => "DATEOFAPPOINTENT",    5 => "ENDATE",   /*6 => "DATEOFREELECTION"*/);
    $arrayVeldnamen  = array(0 => "Person",         1 => "Function",          3 => "Authorization", 4 => "Date of appointment", 5 => "End date", /*6 => "Date of reelection"*/);
    $arrayVeldType   = array(0 => "person_name",    1 => "field",             3 => "field",         4 => "datum",               5 => "datum",    /*6 => "datum"*/);
  } elseif($tabel == "oft_management") {
    $arrayVelden     = array(0 => "TYPE",           1 => "ADMINID",           2 => "USERID",    3 => "FUNCTION",  4 => "AUTHORIZATION", 5 => "DATEOFAPPOINTENT", 6 => "DATEOFREELECTION", 7 => "NEWUSER",   8 => "SKIP_MEMBERTYPE", 9 => "ENDATE", 10 => "LAND");
    if(check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
      $arrayVeldType   = array(0 => "managementType", 1 => "bedrijf",         2 => "personeel", 3 => "director_functions",  4 => "Board type", 5 => "datum",            6 => "datumleeg",        7 => "newperson", 8 => "MEMBERTYPE",      9 => "datumleeg",  10 => "land");
    } else {
      $arrayVeldType   = array(0 => "managementType", 1 => "bedrijf",         2 => "personeel", 3 => "field",     4 => "AUTHORIZATION", 5 => "datum",            6 => "datumleeg",        7 => "newperson", 8 => "MEMBERTYPE",      9 => "datum",  10 => "land");
    }
    $arrayVerplicht  = array(0 => "Ja",             1 => "Nee",               2 => "Nee",       3 => "Ja",        4 => "Ja",            5 => "Ja",               6 => "Nee",              7 => "Nee",       8 => "Nee",             9 => "Nee",    10 => "Nee");
  } elseif($tabel == "oft_appointments_overzicht") {
    $arrayVelden     = array(0 => "USERID",         1 => "BEDRIJFSID",        2 => "FUNCTION",  3 => "AUTHORIZATION", 4 => "DATEOFAPPOINTENT",    5 => "ENDDATE",);
    $arrayVeldnamen  = array(0 => "",               1 => check("default_name_of_entity", "Entity", 1),            2 => "Function",  3 => "Authorization", 4 => "Date of appointment", 5 => "End date",);
    $arrayVeldType   = array(0 => "countryofresidence", 1 => "bedrijfsnaam",  2 => "field",     3 => "field",         4 => "datum",               5 => "datum",);
  } elseif($tabel == "oft_proxy_overzicht" || $tabel == "oft_classes_types_overzicht" || $tabel == "oft_transaction_types_overzicht"  || $tabel == "stam_ordners_overzicht" || $tabel == "oft_department_overzicht" || $tabel == "oft_capacity_overzicht"
            || $tabel == "stam_rechtsvormen_overzicht" || $tabel == "valuta_overzicht" || $tabel == "country_overzicht" || $tabel == "stam_trainingsstatus" || $tabel == "stam_trainingsstatus_overzicht"
            || $tabel == "stam_compliantstatus" || $tabel == "stam_compliantstatus_overzicht" || $tabel == "stam_risklevel" || $tabel == "stam_risklevel_overzicht"
            || $tabel == "stam_documenttype_overzicht" || $tabel == "stam_contracttype_overzicht") {
    $arrayVelden     = array(0 => "NAAM");
    $arrayVeldType   = array(0 => "field");
  } elseif($tabel == "oft_proxy" || $tabel == "stam_ordners" || $tabel == "oft_classes_types" || $tabel == "oft_transaction_types" || $tabel == "oft_department" || $tabel == "oft_capacity" || $tabel == "valuta" || $tabel == "stam_rechtsvormen" || $tabel == "stam_contracttype") {
    $arrayVelden     = array(0 => "NAAM");
    $arrayVeldType   = array(0 => "field");
    $arrayVerplicht  = array(0 => "Ja");
  } elseif($tabel == "country") {
    $arrayVelden     = array(0 => "NAAM", 1 => "ZICHTBAAR");
    $arrayVeldType   = array(0 => "field", 1 => "selectjanee");
    $arrayVerplicht  = array(0 => "Ja", 1 => "Nee");
  } elseif($tabel == "stam_documenttype") {
    $arrayVelden     = array(0 => "NAAM", 1 => "SECTIE");
    $arrayVeldType   = array(0 => "field", 1 => "secties");
    $arrayVerplicht  = array(0 => "Ja", 1 => "Ja");
  } elseif($tabel == "oft_trainingsschema" || $tabel == "oft_trainingsschema_overzicht") {
    $arrayVelden     = array(0 => "COUNTRY", 1 => "TRAINING", 2 => "DUEDATE",  3 => "FORWHO",   4 => "STATUS",);
    $arrayVeldnamen  = array(0 => "Country", 1 => "Training", 2 => "Due date", 3 => "For whom", 4 => "Status",);
    $arrayVeldType   = array(0 => "land",    1 => "field",    2 => "datum",    3 => "field",    4 => "stam_trainingsstatus",);
    $arrayVerplicht  = array(0 => "Ja",      1 => "Ja",       2 => "Ja",       3 => "Ja",       4 => "Nee",);
    $arrayZoekvelden = array(0 => "COUNTRY", 1 => "YEAR",     2 => "SEARCH",);
  } elseif($tabel == "oft_entitie_numbers") {
    $arrayVelden     = array(0 => "TYPE",   1 => "VALUE");
    $arrayVeldnamen  = array(0 => "Type",   1 => "Value");
    $arrayVeldType   = array(0 => "numbers_types",  1 => "field");
    $arrayZoekvelden = array();
  } elseif($tabel == "oft_compliance_chart_overzicht") {
    $arrayVelden     = array(0 => "COUNTRY", 1 => "STATUS",               2 => "SCOPE", 3 => "RISHAREA",  4 => "RISKLEVEL",      6 => "UPDATEDATUM");
    $arrayVeldnamen  = array(0 => "Country", 1 => "Type risk",            2 => "Scope", 3 => "Risk Area", 4 => "Risk level",     6 => "Last update");
    $arrayVeldType   = array(0 => "COUNTRY", 1 => "stam_compliantstatus", 2 => "field", 3 => "field",     4 => "stam_risklevel", 6 => "datum");
    $arrayZoekvelden = array(0 => "COUNTRY", 1 => "stam_risklevel", 2 => "stam_compliantstatus", 3 => "SEARCH");
  } elseif($tabel == "oft_compliance_chart") {
    $arrayVelden     = array(0 => "COUNTRY", 1 => "REF",   2 => "SCOPE",  3 => "RISHAREA",  4 => "LAWS",  5 => "DESCOFRISK",          6 => "RISKLEVEL",      7 => "RISKACTIONS",             8 => "POLICY",                   9 => "LASTISSUEDATE",   10 => "REVIEWDATE",                       11 => "DESCRIPTIONPROGRAM",                12 => "STATUS");
    $arrayVeldnamen  = array(0 => "Country", 1 => "Ref",   2 => "Scope",  3 => "Risk Area", 4 => "Laws",  5 => "Description of risk", 6 => "Risk level",     7 => "Risk mitigating actions", 8 => "Policy / procedure owner", 9 => "Last issue date", 10 => "Review date of policy/ procedure", 11 => "Description of monitoring program", 12 => "Status");
    $arrayVeldType   = array(0 => "land",    1 => "field", 2 => "field",  3 => "field",     4 => "field", 5 => "field",               6 => "stam_risklevel", 7 => "field",                   8 => "field",                    9 => "field",           10 => "field",                            11 => "field",                             12 => "stam_compliantstatus");
    $arrayVerplicht  = array(0 => "Ja",      1 => "Nee",   2 => "Nee",    3 => "Nee",       4 => "Nee",   5 => "Nee",                 6 => "Nee",            7 => "Nee",                     8 => "Nee",                      9 => "Nee",             10 => "Nee",                              11 => "Nee",                               12 => "Nee");
  } elseif($tabel == "oft_compliance_chart_excel") {
    $arrayVelden     = array(0 => "COUNTRY", 1 => "REF",   2 => "SCOPE",  3 => "RISHAREA",  4 => "LAWS",  5 => "DESCOFRISK",          6 => "RISKLEVEL",      7 => "RISKACTIONS",             8 => "POLICY",                   9 => "LASTISSUEDATE",   10 => "REVIEWDATE",                       11 => "DESCRIPTIONPROGRAM",                );
    $arrayVeldnamen  = array(0 => "Country", 1 => "Ref",   2 => "Scope",  3 => "Risk Area", 4 => "Laws",  5 => "Description of risk", 6 => "Risk level",     7 => "Risk mitigating actions", 8 => "Policy / procedure owner", 9 => "Last issue date", 10 => "Review date of policy/ procedure", 11 => "Description of monitoring program", );
    $arrayVeldType   = array(0 => "land",    1 => "field", 2 => "field",  3 => "field",     4 => "field", 5 => "field",               6 => "stam_risklevel", 7 => "field",                   8 => "field",                    9 => "field",           10 => "field",                            11 => "field",                             );
    $arrayVerplicht  = array(0 => "Ja",      1 => "Nee",   2 => "Nee",    3 => "Nee",       4 => "Nee",   5 => "Nee",                 6 => "Nee",            7 => "Nee",                     8 => "Nee",                      9 => "Nee",             10 => "Nee",                              11 => "Nee",                               );
    $arrayZoekvelden = array(0 => "COUNTRY", 1 => "stam_risklevel", 2 => "stam_compliantstatus", 3 => "SEARCH");
  } elseif($tabel == "oft_mena_overzicht") {
    $arrayVelden     = array(0 => "TARGETNAME",  1 => "PROJECTCODE",  2 => "STATUS", 3 => "JURISDICTION", 4 => "SERVICE",                     5 => "TARGETSOURCE",   6 => "TARGETSIZE",            7 => "LEADTIME",         8 => "YEARACTIVATED",  9 => "CATEGORY");
    $arrayVeldnamen  = array(0 => "Target Name", 1 => "Project Code", 2 => "Status", 3 => "Jurisdiction", 4 => "Services Offered - Comments", 5 => "Target Source",  6 => "Target Size (&euro;)", 7 => "Lead Time (days)", 8 => "Year Activated", 9 => "Status:Category");
    $arrayVeldType   = array(0 => "field",       1 => "field",        2 => "field",  3 => "field",        4 => "field",                       5 => "field",          6 => "field",                 7 => "field",            8 => "field",          9 => "field");
    $arrayZoekvelden = array(0 => "TARGETNAME",  1 => "STATUS",       2 => "SERVICE", 3 => "TARGETSOURCE");
  } elseif($tabel == "oft_mena_evaluation") {
    $arrayVelden     = array(0 => "WENTWELL_ONE",   1 => "WENTWELL_TWO",  2 => "WENTWELL_THREE", 3 => "WENTWELL_FOUR", 4 => "COULDIMPROVED_ONE", 5 => "COULDIMPROVED_TWO",   6 => "COULDIMPROVED_THREE", 7 => "COULDIMPROVED_FOUR", 8 => "LESSONSLEARNED_ONE",  9 => "LESSONSLEARNED_TWO", 10 => "LESSONSLEARNED_THREE", 11 => "LESSONSLEARNED_FOUR", 12 => "MENAID", 13 => "SKIP_ADD_FILES_TYPE", 14 => "SKIP_ADD_FILES", 15 => "SKIP_LIST_FILES");
    $arrayVeldnamen  = array();
    $arrayVeldType   = array(0 => "field",          1 => "field",         2 => "field",          3 => "field",         4 => "field",             5 => "field",               6 => "field",               7 => "field",              8 => "field",               9 => "field",              10 => "field",                11 => "field",               12 => "field",  13 => "SKIP_ADD_FILES_TYPE", 14 => "SKIP_ADD_FILES", 15 => "SKIP_LIST_FILES");
    $arrayZoekvelden = array(0 => "MENAID");
  } elseif($tabel == "oft_mena_evaluation_overzicht_lessons") {
    $arrayVelden     = array(0 => "ID",             1 => "LESSONS",     2 => "UPDATEDATUM",);
    $arrayVeldnamen  = array(0 => "Evaluation ID",  1 => "Lesson",      2 => "Last update");
    $arrayVeldType   = array(0 => "field",          1 => "field",       2 => "field");
    $arrayZoekvelden = array(0 => "ID",             1 => "LESSONS");
  } elseif($tabel == "oft_mena_evaluation_overzicht_page") {
    $arrayVelden     = array(0 => "ID",       1 => "MENAID",   2 => "UPDATEDATUM",);
    $arrayVeldnamen  = array(0 => "ID",       1 => "Target",   2 => "Last update");
    $arrayVeldType   = array(0 => "field",    1 => "field",    2 => "field");
    $arrayZoekvelden = array(0 => "ID",       1 => "MENAID");
  } elseif($tabel == "oft_mena_evaluation_overzicht") {
    $arrayVelden     = array(0 => "ID",       1 => "MENAID",   2 => "UPDATEDATUM",);
    $arrayVeldnamen  = array(0 => "ID",       1 => "Target",   2 => "Last update");
    $arrayVeldType   = array(0 => "field",    1 => "field",    2 => "field");
    $arrayZoekvelden = array(0 => "ID",       1 => "MENAID");
  } elseif($tabel == "oft_mena") {
    $arrayVelden     = array(0 => "TARGETNAME",  1 => "PROJECTCODE",  2 => "STATUS",        3 => "JURISDICTION", 4 => "SERVICE",                     5 => "SERVICECOMMETS",              6 => "TARGETSOURCE",   7 => "TARGETSIZE",            8 => "LEADTIME",         9 => "ADDITIONALCOMMENTS",  10 => "YEARACTIVATED",  11 => "CATEGORY",        12 => "DOCUMENTID", 13 => "OPENED", 14 => "ACTIVATED", 15 => "CLOSED",     16 => "DEAD", 17 => "SKIP_ADD_FILES_TYPE", 18 => "SKIP_ADD_FILES", 19 => "SKIP_LIST_FILES");
    $arrayVeldnamen  = array(0 => "Target Name", 1 => "Project Code", 2 => "Status",        3 => "Jurisdiction", 4 => "Services Offered - Comments", 5 => "Services Offered - Comments", 6 => "Target Source",  7 => "Target Size (&euro;)", 8 => "Lead Time (days)", 9 => "Additional Comments", 10 => "Year Activated", 11 => "Status:Category", 12 => "DOCUMENTID", 13 => "OPENED", 14 => "ACTIVATED", 15 => "CLOSED",     16 => "DEAD");
    $arrayVeldType   = array(0 => "field",       1 => "field",        2 => "mena_status",   3 => "field",        4 => "mena_services",               5 => "textbox",                     6 => "field",          7 => "field",                 8 => "field",            9 => "textbox",             10 => "field",          11 => "mena_category",   12 => "bestand",    13 => "datum",  14 => "datumleeg", 15 => "datumleeg",  16 => "datumleeg", 17 => "SKIP_ADD_FILES_TYPE", 18 => "SKIP_ADD_FILES", 19 => "SKIP_LIST_FILES");
    $arrayVerplicht  = array(0 => "Ja",          1 => "Nee",          2 => "Nee",           3 => "Nee",          4 => "Nee",                         5 => "Nee",                         6 => "Nee",            7 => "Nee",                   8 => "Nee",              9 => "Nee",                 10 => "Nee",            11 => "Nee",             12 => "Nee",        13 => "Nee",    14 => "Nee",       15 => "Nee",        16 => "Nee");
  } elseif($tabel == "human_resource") {
    $arrayVelden     = array(0 => "USERNAME",   1 => "PERSONEELSLID",     2 => "PROFIEL",   3 => "SUSPEND");
    $arrayVeldnamen  = array(0 => "Name",       1 => "Employee",            2 => "Year",      3 => "File");
    $arrayVeldType   = array(0 => "field",      1 => "get_personeel_email", 2 => "field",   3 => "translate");
    $arrayZoekvelden = array(0 => "ENTITY",     1 => "SUSPEND",);
  } elseif($tabel == "oft_debt_tracker_overzicht") {
    $arrayVelden     = array(0 => "REFERENCE",  1 => "BORROWERNAME", 2 => "PRINCIPALAMOUNT", 3 => "INTERESTRATE", 4 => "STARTDATE", 5 => "TERM",  6 => "NEXTINTERESTDUEDATE", 7 => "NEXTPAYMENTDUEDATE",);
    $arrayVeldnamen  = array(0 => "Debt ref.",  1 => "Borrower",     2 => "Princ. amount",   3 => "Int. rate",    4 => "Start",     5 => "Term",  6 => "Int. due",            7 => "Pay. due");
    $arrayVeldType   = array(0 => "field",      1 => "field",        2 => "field",           3 => "field",        4 => "field",     5 => "field", 6 => "field",               7 => "field", );
  } elseif($tabel == "oft_debt_tracker_detail_overzicht") {
    $arrayVelden     = array(0 => "STATUS",     1 => "CURRENCY", 2 => "INTERESTRATE",  3 => "INTERESTDUEDATE",   4 => "AMOUNT", 5 => "MILESTONE", 6 => "MILESTONEDATE",);
    $arrayVeldnamen  = array(0 => "Status",     1 => "Currency", 2 => "Interest rate", 3 => "Interest due date", 4 => "Amount", 5 => "Milestone", 6 => "Milestone date",);
    $arrayVeldType   = array(0 => "field",      1 => "field",    2 => "field",         3 => "field",             4 => "field",  5 => "field",     6 => "field",);
  } elseif($tabel == "oft_debt_tracker") {
    $arrayVelden     = array(0 => "COUNTRY",    1 => "EXCEPTION",     2 => "DESCRIPTION", 3 => "STATUS",        4 => "ISSUEDATE",  5 => "ACTIONREQUIRED", 6 => "DATECLOSE");
    $arrayVeldnamen  = array(0 => "Country",    1 => "Type",          2 => "Description", 3 => "Status",        4 => "Issue date", 5 => "Action",         6 => "Date close");
    $arrayVeldType   = array(0 => "land",       1 => "field",         2 => "field",       3 => "reg_status",    4 => "datum",      5 => "field",          6 => "datumleeg");
    $arrayVerplicht  = array(0 => "Nee",        1 => "Nee",           2 => "Nee",         3 => "Nee",           4 => "Nee",        5 => "Nee",            6 => "Nee");
  } elseif($tabel == "oft_services_overzicht") {
    $arrayVelden     = array(0 => "TYPE",  1 => "NAME",   2 => "STATUS",       3 => "SERVICEPROVIDERID");
    $arrayVeldnamen  = array(0 => "Type",  1 => "Name",   2 => "Done",         3 => "Provider");
    $arrayVeldType   = array(0 => "field", 1 => "field",  2 => "selectjanee",  3 => "service_provider");
    $arrayZoekvelden = array();
  } elseif($tabel == "oft_services") {
    $arrayVelden     = array(0 => "TYPE",  1 => "NAME",   2 => "STATUS",       3 => "SERVICEPROVIDERID");
    $arrayVeldnamen  = array(0 => "Type",  1 => "Name",   2 => "Done",         3 => "Provider");
    $arrayVeldType   = array(0 => "field", 1 => "field",  2 => "selectjanee",  3 => "service_provider");
    $arrayZoekvelden = array();
  } else {
      include_once 'table_structure.php';
      return getTableStructure($userid, $bedrijfsid, $tabel, $kolom);
  }


  /*
  Training	Due date	For who	status
  */

  if($kolom == "arrayZoekvelden") {
    return $arrayZoekvelden;
  } elseif($kolom == "arrayVeldType") {
    return $arrayVeldType;
  } elseif($kolom == "arrayVeldnamen") {
    return $arrayVeldnamen;
  } elseif($kolom == "arrayVerplicht") {
    return $arrayVerplicht;
  } else {
    return $arrayVelden;
  }
}
