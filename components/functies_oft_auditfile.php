<?php
/*
 *  auditFile ( Volgens belastingdienst )
 *    header
 *      auditfileVersion
 *      companyID
 *      taxRegistrationNr
 *      companyName
 *      companyAddress
 *      companyCity
 *      companyPostalCode
 *      fiscalYear
 *      startDate
 *      endDate
 *      currencyCode
 *      dateCreated
 *      productID
 *      productVersion
 *    generalLedger
 *      taxonomy
 *      ledgerAccount
 *        accountID
 *        accountDesc
 *        accountType
 *        leadCode
 *        leadDescription
 *    customersSuppliers
 *      customerSupplier
 *        custSupID
 *        type
 *        taxRegistrationNr
 *        taxVerificationDate
 *        companyName
 *        contact
 *        streetAddress
 *          address
 *          streetname
 *          number
 *          numberExtension
 *          property
 *          city
 *          postalCode
 *          region
 *          country
 *        postalAddress
 *          address
 *          streetname
 *          number
 *          city
 *          postalCode
 *          region
 *          country
 *          website
 *        telephone
 *        fax
 *        eMail
 *        website
 *    transactions
 *      numberEntries
 *      totalDebit
 *      totalCredit
 *      journal
 *        journalId
 *        description
 *        type
 *        transaction
 *          transactionID
 *          description
 *          period
 *          transactionDate
 *          sourceID
 *          line
 *            recordID
 *            accountID
 *            custSupID
 *            documentID
 *            effectiveDate
 *            description
 *            debitAmount
 *            creditAmount
 *            costDesc
 *            productDesc
 *            projectDesc
 *            vat
 *              vatCode
 *              vatPercentage
 *              vatAmount
 *            currency
 *              currencyCode
 *              currencyDebitAmount
 *              currencyCreditAmount
 */

function oft_audit_teksten( $key ) {
$Teksten = array(
  'titel'                   => "Importeer audit-bestand (XAF 2.0)\n",
  'label_file'              => "Audit bestand",
  'verwerking_fout'         => "Er zijn fouten opgetreden in de verwerking :\n",
  'verwerking_ok'           => "Verwerking is okay\n",
  'geen_backup'             => "Er kon geen backup van de huidige boekhouding gemaakt worden.\n",
  'geen_bestand'            => "Er is geen bestand opgegeven.\n",
  'geen_xml'                => "Het bestand is geen geldige XML file.\n",
  'upload_fout'             => "Het uploaden van de audit bestand is niet gelukt.\n",
  'verstuur'                => "Verstuur",
  'ontvangen1'              => "Uw audit bestand [",
  'ontvangen2'              => "] is ontvangen.\n",
  'geen_header'             => "Er was geen header gevonden. XML document niet bruikbaar.\n",
  'legder_verwerk'          => "Alle ledger-regels zijn ",
  'succes'                  => "succesvol ",
  'geen_succes'             => "onsuccesvol ",
  'gedeeltelijk'            => "gedeeltelijk succesvol ",
  'verwerkt'                => "verwerkt.\n",
  'ledger_incompleet'       => "Niet alle ledger-regels zijn verwerkt.\n",
  'geen_ledger'             => "Er was geen ledger sectie gevonden. Geen boeken.\n",
  'relatie_verwerk'         => "Alle relatie-regels zijn ",
  'relatie_incompleet'      => "Niet alle relatie-regels zijn verwerkt.\n",
  'relatie_leeg'            => "Er is een relatiesectie maar deze heeft geen inhoud.\n",
  'geen_relatie'            => "Er was geen relatie sectie gevonden. Geen relaties.\n",
  'transactie_statistieken' => "De statistieken voor transacties kloppen niet:\n",
  'aantal_transacties'      => "Aantal transacties [verwacht:geteld]",
  'totaal_debet'            => "Totaal debet [verwacht:geteld]",
  'totaal_credit'           => "Totaal credit [verwacht:geteld]",
  'transactie_verwerk'      => "Alle transactie-regels zijn ",
  'transactie_incompleet'   => "Niet alle transactie-regels zijn verwerkt.\n",
  'geen_transactie'         => "Er was geen transactie sectie gevonden. Geen transacties.\n",
  'verwerkingsfout'         => "Er is een fout opgetreden gedurende de verwerking van het audit bestand.\n",
  'periode_uitzondering'    => "De periode kan niet vastgesteld worden. Er is een probleem met het XML bestand.\n",
  'relatie_dummy1'          => "Relatie : ",
  'relatie_dummy2'          => " toegevoegd. Bedrijfsgegevens moeten worden aangevuld.\n",
  'relatie_onvindbaar1'     => "De relatie voor transactie ",
  'relatie_onvindbaar2'     => " kan niet gevonden worden.\n",
  'accountid1'              => "het accountID, ",
  'accountid2'              => ", voor deze transactie, ",
  'accountid3'              => ", kan niet gevonden worden.\n",
  'al_geimporteerd1'        => "Het record met ID",
  'al_geinmporteerd2'       => " is reeds geimporteerd.\n",
  'no_upload'               => "No file uploaded",
  'account_onvindbaar1'     => "Geen accountreferentie gevonden.\n" );
  $Teksten_EN = array(
    'titel'                   => "Import audit-file (XAF 2.0)\n",
    'label_file'              => "Audit file",
    'verwerking_fout'         => "An error has occurred during the processing :\n",
    'verwerking_ok'           => "Processing is okay.\n",
    'geen_backup'             => "No backup of the present administration could be made.\n",
    'geen_bestand'            => "No file was chosen.\n",
    'geen_xml'                => "The file is not a valid XML file.\n",
    'upload_fout'             => "Uploading the audit file has failed.\n",
    'verstuur'                => "Send",
    'ontvangen1'              => "Your audit file [",
    'ontvangen2'              => "] has been received.\n",
    'geen_header'             => "No header was found. The XML is file not usable.\n",
    'legder_verwerk'          => "All ledger-lines have been ",
    'succes'                  => "successfully ",
    'geen_succes'             => "unsuccessfully ",
    'gedeeltelijk'            => "partially successful ",
    'verwerkt'                => "processed.\n",
    'ledger_incompleet'       => "Not all ledger-lines have been processed.\n",
    'geen_ledger'             => "No ledger section was found. No books.\n",
    'relatie_verwerk'         => "All relation-lines have been ",
    'relatie_incompleet'      => "Not all relation-lines have been processed.\n",
    'relatie_leeg'            => "A relation section exists but it is empty.\n",
    'geen_relatie'            => "No relation section was found. No relations.\n",
    'transactie_statistieken' => "Transaction statistics don't match:\n",
    'aantal_transacties'      => "Transaction count [expected:counted]",
    'totaal_debet'            => "Total debet [expected:counted]",
    'totaal_credit'           => "Total credit [expected:counted]",
    'transactie_verwerk'      => "All transaction-lines have been ",
    'transactie_incompleet'   => "Not all transaction-lines have been processed.\n",
    'geen_transactie'         => "No transaction section has been found, No transactions.\n",
    'verwerkingsfout'         => "An error occurred during the processing of the audit file.\n",
    'periode_uitzondering'    => "Could not determine the period. A problem with the XML file has occurred.\n",
    'relatie_dummy1'          => "Relation : ",
    'relatie_dummy2'          => " added. Relation information has to be completed.\n",
    'relatie_onvindbaar1'     => "The relation for transaction  ",
    'relatie_onvindbaar2'     => " can not be found.\n",
    'accountid1'              => "The accountID, ",
    'accountid2'              => ", for this transaction, ",
    'accountid3'              => ", cannot be found.\n",
    'al_geimporteerd1'        => "The record with ID",
    'al_geinmporteerd2'       => " has already been imported.\n",
    'no_upload'               => "No file uploaded",
    'account_onvindbaar1'     => "No accountreference found.\n" );
  if ( array_key_exists( $key, $Teksten_EN ) ) {
    return $Teksten_EN[$key];
  }
  return $key;
}

function oft_audit_import($userid, $bedrijfsid) {
  global $db;

  if (isset($_REQUEST["ID"])) {
    $bedrijfsid = $_REQUEST["ID"];
  }

  $titel = oft_titel_entities($bedrijfsid);
  $submenuitems = "";
  $back_button = oft_back_button_entity();
  $contentFrame = auditfile_form($userid, $bedrijfsid);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function auditfile_form($userid, $bedrijfsid) {
  // Formulier ingestuurd?
  $toonformulier = false;
  $feedback = '';
  $reply = '';
  $filename = null;

  $reply .= "<h2>Import audit-file (XAF 2.0)</h2><br/><br/>";
  if (isset($_REQUEST['VERSTUUR'])) {
    // Haal de auditfile locatie op
    if ( array_key_exists('X_File', $_FILES) && isset( $_FILES['X_File']['tmp_name'] ) && file_exists( $_FILES['X_File']['tmp_name'] ) ) {
      $filename = auditfile_upload($userid, $bedrijfsid, 'X_File');
      if (isset($filename)) {
        if ( auditfile_verifieer($userid, $bedrijfsid, $filename) ) {
          try {
            $jaren = auditfile_periode($userid, $bedrijfsid, $filename);
          } catch ( Exception $e ) {
            $feedback = $e->getMessage();
          }
          if ( !$feedback ) {
            $message = '';
            if (boekhouding_backup($userid, $bedrijfsid, $jaren)) {

              verwijder_boekhouding($userid, $bedrijfsid, null, null);
              $result = auditfile_verwerk($userid, $bedrijfsid, $filename);

              if ($result[1]) {
                $message .= '<li>'. trim( $result[1] ). '</li>';
                while ( strpos( "\n\n", $message ) ) {
                  $message = str_replace( "\n\n", "\n", $message );
                }
                $message = str_replace( "\n", '</li><li>', $message );
              }
              if (!$result[0]) {
                $feedback .= tl( oft_audit_teksten('verwerking_fout') );
                boekhouding_restore($userid, $bedrijfsid, null, null);
                $toonformulier = false;
              } else {
                $feedback .= tl( oft_audit_teksten('verwerking_ok') );
                $toonformulier = true;
              }
              $feedback = trim( $feedback );
              $feedback .= '<ul>'. $message. '</ul>';
            } else {
              $toonformulier = true;
              $feedback = tl( oft_audit_teksten('geen_backup') );
            }
          } else {
            $toonformulier = true;
          }
        } else {
          $toonformulier = true;
          $feedback = tl( oft_audit_teksten('geen_xml') );
        }
        unlink($filename);
      } else {
        $toonformulier = true;
        $feedback = tl( oft_audit_teksten('upload_fout') );
      }
    } else {
      $toonformulier  = true;
      $feedback = tl( oft_audit_teksten('geen_bestand') );
    }
  } else {
    $toonformulier = true;
  }

  if ($toonformulier) {
    $feedback = trim( $feedback );
    if ($feedback != '') {
      while ( strpos( "\n\n", $feedback ) ) {
        str_replace( "\n\n", "\n", $feedback );
      }
      $feedback_list = explode( "\n", $feedback );
      $feedback = '<ul><li>'. implode( '</li><li>', $feedback_list ). '</li></ul>';
      $reply .= "<div class=\"message\">$feedback</div>";
    }
    $reply .= "<form action=\"\" method=\"Post\" enctype=\"multipart/form-data\">";
    $reply .= "<table class=\"tabel\">";
    $reply .= "<tr>";
    $reply .= "<td class=\"oft_label\">". tl( oft_audit_teksten('label_file') ). "</td><td><input type=\"file\" name=\"X_File\" value=\"\" class=\"field\" /></td>";
    $reply .= "</tr><tr>";
    $reply .= "<td></td><td><input type=\"submit\" name=\"VERSTUUR\" value=\"". oft_audit_teksten('verstuur'). "\" class=\"button\" /></td><td></td>";
    $reply .= "</tr>";
    $reply .= "</table>";
    $reply .= "</form>";
  } else {
    $reply .= "<strong>". tl( oft_audit_teksten('ontvangen1') ). $filename. tl( oft_audit_teksten('ontvangen2') ). "</strong><br/>";
    if ($feedback != '') {
      $reply .= "<div class=\"message\">$feedback</div>";
    }
  }
  return $reply;
}

function auditfile_upload($userid, $bedrijfsid, $file_label)
{
  if ( array_key_exists( $file_label, $_FILES ) ) {
    $bestandsnaam = $_FILES[$file_label]['name'];
    $tijdelijke_locatie = $_FILES[$file_label]['tmp_name'];

    $eindlocatie = bestandLocatie();
    $bestandspad = "$eindlocatie/temporary/$bestandsnaam";

    if ( file_exists( $bestandspad ) ) {
      unlink( $bestandspad );
    }

    if ( move_uploaded_file( $tijdelijke_locatie, $bestandspad ) ) {
      return $bestandspad;
    }
  }
  return null;
}

function auditfile_verifieer($userid, $bedrijfsid, $auditfile_locatie)
{ // Dummy
  // Verifieer bestaan
  if (file_exists($auditfile_locatie)) {
    // Verifieer welgevormdheid kan niet met SimpleXMLElement
    $content = file_get_contents($auditfile_locatie);
    try {
      $test = @new SimpleXMLElement($content);
    } catch ( Exception $e ) {
      return false;
    }
    // Verifieer type

    //
    return true;
  } else {
    // Bestand bestaat niet.
    return false;
  }
}

function boekhouding_backup($userid, $bedrijfsid, $startdate = null, $enddate = null )
{ // Dummy
  return true;
}

function boekhouding_weggooien($userid, $bedrijfsid, $startdate = null, $enddate = null )
{ // Dummy

}

function boekhouding_restore($userid, $bedrijfsid, $startdate = null, $enddate = null )
{ // Dummy
  return true;
}

function boekhouding_verifieer($userid, $bedrijfsid, $startdate = null, $enddate = null )
{ // Dummy

}

function auditfile_periode($userid, $bedrijfsid, $filename)
{
  $content = file_get_contents($filename);
  try {
    $xml = @new SimpleXMLElement($content);
  } catch ( Exception $e ) {
    throw new Exception( oft_audit_teksten( 'periode_uitzondering' ), 0, $e);
  }

  $struct = array();

  $header = $xml->xpath('//header');
  if ($header !== false) {
    while (list(, $node) = each($header)) {
      $struct['startdate']  = (string)$node->startDate;
      $struct['enddate']    = (string)$node->endDate;
    }
  } else {
    $struct = false;
  }
  return $struct;
}

function auditfile_verwerk($userid, $bedrijfsid, $filename)
{
  $message = '';
  $content = file_get_contents($filename);
  try {
    $document = @new SimpleXMLElement($content);
  } catch( Exception $e ) {
    $message .= $e->getMessage();
    return array( false, $message );
  }
  $struct = array();

  // Verwerk de header
  /*  <header>
   +    <auditfileVersion>CLAIR2.00.00</auditfileVersion>
   +    <companyID>92144</companyID>
   +    <taxRegistrationNr></taxRegistrationNr>
   +    <companyName>Husk BV</companyName>
   +    <companyAddress></companyAddress>
   +    <companyCity></companyCity>
   +    <companyPostalCode></companyPostalCode>
   +    <fiscalYear>2014</fiscalYear>
   +    <startDate>2014-01-01</startDate>
   +    <endDate>2014-12-31</endDate>
   +    <currencyCode>USD</currencyCode>
        <dateCreated>2014-04-28</dateCreated>
        <productID> Mill7</productID>
        <productVersion>7.3</productVersion>
      </header> */
  $headers = $document->xpath('//header');
  if ($headers != null) {
    while ( list(, $header) = each($headers) ) {
      $node                 = $header->children();
      $struct['version']    = (string)$node->auditfileVersion;
      $struct['year']       = (string)$node->fiscalYear;
      $struct['startdate']  = (string)$node->startDate;
      $struct['enddate']    = (string)$node->endDate;
      $struct['company']    = array( 'id'     => (string)$node->companyId,
                                     'name'   => (string)$node->companyName,
                                     'adress' => (string)$node->companyAdress,
                                     'city'   => (string)$node->companyCity,
                                     'pcode'  => (string)$node->companyPostalCode);
      if ( isset( $node->taxRegistrationNr ) ) {
        $struct['company']['BTWNR'] = (string)$node->taxRegistrationNr;
      }
      if ( isset( $node->currencyCode ) ) {
        $struct['currency'] = (string)$node->currencyCode;
      }
    }
  } else {
    $message .= tl( oft_audit_teksten('geen_header') );
    return array( false, $message );
  }

  // Verwerk de ledger sectie
  $ledgers = $document->xpath('//generalLedger');
  $failed = 0;
  $parsed = 0;
  $total  = 0;
  if ($ledgers != null) {
    while (list(, $ledger) = each($ledgers)) {
      $ledgerAccounts = $ledger->xpath('./ledgerAccount');

      while (list(, $account) = each($ledgerAccounts)) {
        $subnode = $account->children();
        if (isset($subnode->accountID) || isset($subnode->accountDesc) || isset($subnode->accountType)) {
          // Versie CLAIR2.00.00
          $struct['ID']           = (string)$subnode->accountID;
          $struct['OMSCHRIJVING'] = (string)$subnode->accountDesc;
          $struct['TYPE']         = (string)$subnode->accountType;
          if ( isset($subnode->leadCode) ) {
            $struct['LEADCODE'] = (string)$subnode->leadCode;
          }
          if ( isset($subnode->leadDescription) ) {
            $struct['LEADDESC'] = (string)$subnode->leadDescription;
          }
        } elseif (isset($subnode->accID) || isset($subnode->accDesc) || isset($subnode->accTp)) {
          //
          $struct['ID']           = (string)$subnode->accID;
          $struct['OMSCHRIJVING'] = (string)$subnode->accDesc;
          $struct['TYPE']         = (string)$subnode->accTp;
        }

        $resultaat = maakBoek($userid, $bedrijfsid, $struct);
        if ($resultaat[0] == false) {
          $message .= $resultaat[1];
          $failed++;
        } else {
          $parsed++;
        }
        $total++;
      }
    }
    unset($struct['ID']);
    unset($struct['OMSCHRIJVING']);
    unset($struct['TYPE']);
    if ($total == $failed + $parsed) {
      $message .= tl( oft_audit_teksten('legder_verwerk') );
      if ($failed == 0) {
        $message .= tl( oft_audit_teksten('succes') );
      } elseif ($parsed == 0) {
        $message .= tl( oft_audit_teksten('geen_succes') );
      } else {
        $message .= tl( oft_audit_teksten('gedeeltelijk') );
      }
      $message .= tl( oft_audit_teksten('verwerkt') );
    } else {
      $message .= tl( oft_audit_teksten('ledger_incompleet') );
    }
  } else {
    $message .= tl( oft_audit_teksten('geen_ledger') );
    return array(false, $message);
  }

  // Verwerk de relatie sectie
  $relationssection = $document->xpath('//customersSuppliers');
  $failed = 0;
  $parsed = 0;
  $total  = 0;
  if ($relationssection != null) {
    while (list(, $relations) = each($relationssection)) {
      $relationsection = $relations->xpath('./customerSupplier');
      if ($relationsection != null) {
        while (list(, $relation) = each($relations)) {
          $renode = $$relation->children();
          if (isset($renode->custSupID)) {
            $adres = $renode->streetAddress->children();
            $struct['relatie'] = array( 'ID'            => (string)$renode->custSupID,
                                        'NAAM'          => (string)$renode->custSupName,
                                        'CONTACT'       => (string)$renode->contact,
                                        'TELVAST'       => (string)$renode->telefoon,
                                        'FAX'           => (string)$renode->fax,
                                        'EMAIL'         => (string)$renode->email,
                                        'WWW'           => (string)$renode->website,
                                        'KVKNUMMER'     => (string)$renode->commerceNr,
                                        'BELASTINGLAND' => (string)$renode->taxRegistrationCountry,
                                        'BELASINGNR'    => (string)$renode->taxRegIdent,
                                        'RELATIENR'     => (string)$renode->relationshipID,
                                        'STRAAT'        => (string)$adres->streetname,
                                        'NUMMER'        => (string)$adres->number,
                                        'TOEVOEGING'    => (string)$adres->numberExtension,
                                        'PLAATS'        => (string)$adres->city,
                                        'PCODE'         => (string)$adres->postalCode,
                                        'LAND'          => (string)$adres->country);
            $resultaat = maakRelatie($userid, $bedrijfsid, $struct);
            if ($resultaat[0] == false) {
              $message .= $resultaat[1];
              $failed++;
            } else {
              $parsed++;
            }
            $total++;
          }
        }
        if ($total == $failed + $parsed) {
          $message .= tl( oft_audit_teksten('relatie_verwerk') );
          if ($failed == 0) {
            $message .= tl( oft_audit_teksten('succes') );
          } elseif ($parsed == 0) {
            $message .= tl( oft_audit_teksten('geen_succes') );
          } else {
            $message .= tl( oft_audit_teksten('gedeeltelijk') );
          }
          $message .= tl( oft_audit_teksten('verwerkt') );
        } else {
          $message .= tl( oft_audit_teksten('relatie_incompleet') );
        }
      } else {
        $message .= tl( oft_audit_teksten('relatie_leeg') );
        // Als dit betekend dat niet verder gegaan moet worden dan de volgende regel actief maken.
        // anders maakt het systeem dummyregels aan voor elke transactie voor een niet-bestaande
        //relatie.
        //return array(false, $message);
      }
    }
    unset($struct['relatie']);
  } else {
    $message .= tl( oft_audit_teksten('geen_relatie') );
    // Als dit betekend dat niet verder gegaan moet worden dan de volgende regel actief maken.
    // anders maakt het systeem dummyregels aan voor elke transactie voor een niet-bestaande
    //relatie.
    //return array( false, $message );
  }

  // Verwerk de transactie sectie
  $transactions = $document->xpath('//transactions');
  $failed = 0;
  $parsed = 0;
  $total  = 0;
  $totaal_debet  = 0;
  $totaal_credit = 0;
  if ($transactions != null) {
    while (list(, $transaction) = each($transactions)) {
      $renode = $transaction->children();
      if (isset($renode->numberEntries)) {
        // Versie CLAIR2.00.00
        $struct['transactions'] = array( 'count'        => (string)$renode->numberEntries,
                                         'total_debit'  => (string)$renode->totalDebit,
                                         'total_credit' => (string)$renode->totalCredit);
      } elseif (isset($renode->linesCount)) {
        //
        $struct['transactions'] = array( 'count'        => (string)$renode->linesCount,
                                         'total_debit'  => (string)$renode->totalDebit,
                                         'total_credit' => (string)$renode->totalCredit);
      }
      $journals = $transaction->xpath('./journal');
      while (list(, $journal) = each($journals)) {
        $renode = $journal->children();
        if (isset($renode->journalID) && isset($renode->description) && isset($renode->type)) {
          // Versie CLAIR2.00.00
          $struct['journal'] = array( 'ID'            => (string)$renode->journalID,
                                      'OMSCHRIJVING'  => (string)$renode->description,
                                      'TYPE'          => (string)$renode->type);
        } elseif (isset($renode->jrnID) && isset($renode->desc) && isset($renode->jrnTp)) {
          //
          $struct['journal'] = array( 'ID'            => (string)$renode->jrnID,
                                      'OMSCHRIJVING'  => (string)$renode->desc,
                                      'TYPE'          => (string)$renode->jrnTp);
        }
        $tas = $journal->xpath('./transaction');
        while (list(, $ta) = each($tas)) {
          $renode = $ta->children();
          $lines = null;
          if (isset($renode->transactionID) && isset($renode->description) && isset($renode->period) && isset($renode->transactionDate)) {
            // Versie CLAIR2.00.00
            $struct['transaction'] = array( 'ID'            => (string)$renode->transactionID,
                                            'OMSCHRIJVING'  => (string)$renode->description,
                                            'PERIODE'       => (string)$renode->period,
                                            'DATUM'         => (string)$renode->transactionDate);
            $lines = $ta->xpath('./line');
          } elseif (isset($renode->nr) && isset($renode->desc) && isset($renode->periodNumber) && isset($renode->trDt)) {
            //
            $struct['transaction'] = array( 'ID'            => (string)$renode->nr,
                                            'OMSCHRIJVING'  => (string)$renode->desc,
                                            'PERIODE'       => (string)$renode->periodNumber,
                                            'DATUM'         => (string)$renode->trDt);
            $lines = $ta->xpath('./trline');
          } else {
            $message .= "";
          }
          while (list(, $line) = each($lines)) {
            $renode = $line->children();
            if (isset($renode->recordID)) {
              // Versie CLAIR2.00.00
              $struct['ID']             = (string)$renode->recordID;
              $struct['ACCOUNTID']      = (string)$renode->accountID;
              $struct['DATUM']          = (string)$renode->effectiveDate;
              $struct['OMSCHRIJVING']   = (string)$renode->description;
              $struct['DEBET_BEDRAG']   = (string)$renode->debitAmount;
              $struct['CREDIT_BEDRAG']  = (string)$renode->creditAmount;
              if (isset($renode->currency)) {
                $currency = $renode->currency->children();
                $struct['VALUTA']               = (string)$currency->currencyCode;
                $struct['VALUTA_CREDIT_BEDRAG'] = (string)$currency->currencyCreditAmount;
                $struct['VALUTA_DEBET_BEDRAG']  = (string)$currency->currencyDebitAmount;
              } else {
                $struct['VALUTA_CREDIT_BEDRAG'] = $struct['CREDIT_BEDRAG'];
                $struct['VALUTA_DEBET_BEDRAG']  = $struct['DEBET_BEDRAG'];
              }
            } elseif (isset($renode->nr)) {
              //
              $struct['ID']           = (string)$renode->nr;
              $struct['ACCOUNTID']    = (string)$renode->accID;
              $struct['DATUM']        = (string)$renode->effDate;
              $struct['OMSCHRIJVING'] = (string)$renode->desc;
              $currency               = $renode->currency->children();
              $struct['VALUTA']       = (string)$currency->curCode;
              if ($renode->amntTp == 'D') {
                $struct['DEBET_BEDRAG']         = (string)$renode->amnt;
                $struct['VALUTA_DEBET_BEDRAG']  = (string)$currency->curAmnt;
              } elseif ($node4->amntTp == 'C') {
                $struct['CREDIT_BEDRAG']        = (string)$renode->amnt;
                $struct['VALUTA_CREDIT_BEDRAG'] = (string)$currency->curAmnt;
              }
            }
            if ( isset( $renode->vat ) ) {
              $struct['VATCODE'] = (string)$renode->vat->vatCode;
              $struct['VATAMOUNT'] = (string)$renode->vat->vatAmount;
            }
            $total++;
            $totaal_debet   += $struct['DEBET_BEDRAG'];
            $totaal_credit  += $struct['CREDIT_BEDRAG'];
            $resultaat = maakTransactie($userid, $bedrijfsid, $struct);
            $message .= $resultaat[1];
            if ($resultaat[0] == false) {
              $failed++;
            } else {
              $parsed++;
            }
          }
          unset($struct['ID']);
          unset($struct['ACCOUNTID']);
          unset($struct['DATUM']);
          unset($struct['OMSCHRIJVING']);
          unset($struct['DEBET_BEDRAG']);
          unset($struct['CREDIT_BEDRAG']);
          unset($struct['VALUTA']);
          unset($struct['VALUTA_DEBET_BEDRAG']);
          unset($struct['VALUTA_CREDIT_BEDRAG']);
          unset($struct['VATCODE']);
          unset($struct['VATAMOUNT']);
        }
        unset($struct['transaction']);
      }
      unset($struct['journal']);
    }

    if (($struct['transactions']['count'] * 1 - $total) > 0.001 ||
      ($struct['transactions']['total_debit'] * 1 - $totaal_debet) > 0.001 ||
      ($struct['transactions']['total_credit'] * 1 - $totaal_credit) > 0.001
    ) {
      $message .= tl( oft_audit_teksten('transactie_statistieken') );
      $message .= sprintf("\t". tl( oft_audit_teksten('aantal_transacties') ). " [%2d:%2d]\n", $struct['transactions']['count'], $total);
      $message .= sprintf("\t". tl( oft_audit_teksten('totaal_debet') ). " [%4.2f:%4.2f]\n", $struct['transactions']['total_debit'], $totaal_debet);
      $message .= sprintf("\t". tl( oft_audit_teksten('totaal_credit') ). " [%4.2f:%4.2f]\n", $struct['transactions']['total_credit'], $totaal_credit);
      return array(false, $message);
    }
    if ($total == $failed + $parsed) {
      $message .= tl( oft_audit_teksten('transactie_verwerk') );
      if ($failed == 0) {
        $message .= tl( oft_audit_teksten('succes') );
      } elseif ($parsed == 0) {
        $message .= tl( oft_audit_teksten('geen_succes') );
      } else {
        $message .= tl( oft_audit_teksten('gedeeltelijk') );
      }
      $message .= tl( oft_audit_teksten('verwerkt') );
    } else {
      $message .= tl( oft_audit_teksten('transactie_incompleet') );
    }
    return array(true, $message);
  } else {
    $message .= tl( oft_audit_teksten('geen_transactie') );
    return array(false, $message);
  }
  return array(true, tl( oft_audit_teksten('verwerkingsfout') ) );
}

function maakBoek($userid, $bedrijfsid, $structuur) {
  global $pdo;
  $NIETBESCHIKBAARIN  = ';';
  $DAGBOEK            = 'Memoriaal';
  $BALANSKOSTEN       = "Balans";
  $CREDITDEBET        = 'Debet';

  $query = $pdo->prepare('SELECT ID FROM bedrijf WHERE NOT ID = :bedrijfsid;');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->execute();

  foreach ($query->fetchAll() as $bedrijf) {
    $NIETBESCHIKBAARIN .= $bedrijf['ID'] . ';';
  }
  
  $type = strtolower( $structuur['TYPE'] );
  if ( $type == 'balans' ) { // Deze kom ik tegen in de auditfile
    $CREDITDEBET    = 'Debet';
    $BALANSKOSTEN   = "Balans";
    $DAGBOEK        = 'Memoriaal';
  } elseif ( strtolower($type) == 'wv' ) { // Deze kom ik tegen in de auditfile
    $CREDITDEBET    = 'Debet';
    $BALANSKOSTEN   = "Kosten of opbrengst";
    $DAGBOEK        = 'Memoriaal';
  } elseif ( strtolower($type) == "activa") {
    $CREDITDEBET    = 'Debet';
    $BALANSKOSTEN   = "Balans";
    $DAGBOEK        = 'Memoriaal';
  } elseif ( strtolower($type) == "passiva") {
    $CREDITDEBET    = 'Credit';
    $BALANSKOSTEN   = "Balans";
    $DAGBOEK        = 'Memoriaal';
  } elseif ( strtolower($type) == "kosten") {
    $CREDITDEBET    = 'Debet';
    $BALANSKOSTEN   = "Kosten of opbrengst";
    $DAGBOEK        = 'Inkoop';
  } elseif ( strtolower($type) == "opbrengsten") {
    $CREDITDEBET    = 'Credit';
    $BALANSKOSTEN   = "Kosten of opbrengst";
    $DAGBOEK        = 'Verkoop';
  }

  //echo "Type : ".$structuur['TYPE']."<br/>\nCredit/Debet : $CREDITDEBET<br/>\nBalanskosten : $BALANSKOSTEN<br/>\nDagboek : $DAGBOEK<br/>\n";

  $rubriekNaam = '';
  $rubriekId = '';
  $omschrijving = '';
  if (array_key_exists('LEADDESC', $structuur)) {
    $rubriekNaam = trim((string)$structuur['LEADDESC']);
    $rubriekId = '1';
  }
  if ( array_key_exists( 'OMSCHRIJVING', $structuur ) ) {
    $omschrijving = $structuur['OMSCHRIJVING'];
  } else {
    $omschrijving = "rubriek_". $structuur['ID'];
  }
  if ( $rubriekNaam == '' ) {
    $rubriekNaam = $omschrijving;
    $rubriek = 1;
  }

  if ($rubriekNaam != '') {
    $query = $pdo->prepare('SELECT ID FROM balansrubriek WHERE NAAM = :rubrieknaam limit 1;');
    $query->bindValue('rubrieknaam', $rubriekNaam);
    $query->execute();

    $found = false;
    foreach ($query->fetchAll() as $data) {
      $rubriekId = $data["ID"];
      $found = true;
    }
    
    if(!$found) {
      $sql = ";";
      $query = $pdo->prepare('INSERT into balansrubriek SET NAAM  = :naam,
                                            BEDRIJFSID = :bedrijfsid,
                                            OMSCHRIJVING = :omschrijving;');
      $query->bindValue('naam', $rubriekNaam);
      $query->bindValue('bedrijfsid', $bedrijfsid);
      $query->bindValue('omschrijving', $structuur['OMSCHRIJVING']);
      
      $query->execute();
      $rubriekId = $pdo->lastInsertId();
    }
  }

  $insertQuery = $pdo->prepare('INSERT INTO grootboekitems
                  SET GROOTBOEKNR   = :grootboeknr,
                  NAAM              = :naam,
                  OMSCHRIJVING      = :omschrijving,
                  BALANSKOSTEN      = :balanskosten,
                  BEDRIJFSID        = :bedrijfsid,
                  NIETBESCHIKBAARIN = :nietbeschikbaarin,
                  GEBLOKKEERD       = "Nee",
                  TOONINGRAPH       = "Nee",
                  ISSALARIS         = "Nee",
                  OFFERTENR         = "",
                  CREDITDEBET       = creditdebet,
                  AANPASBAAR        = "Ja",
                  DAGBOEK           = dagboek,
                  RUBRIEK           = rubriekid;');
  $insertQuery->bindValue('grootboeknr', $structuur['ID']);
  $insertQuery->bindValue('naam', $structuur['OMSCHRIJVING']);
  $insertQuery->bindValue('omschrijving', $structuur['ID']);
  $insertQuery->bindValue('balanskosten', $BALANSKOSTEN);
  $insertQuery->bindValue('bedrijfsid', $bedrijfsid);
  $insertQuery->bindValue('nietbeschikbaarin', $NIETBESCHIKBAARIN);
  $insertQuery->bindValue('creditdebet', $CREDITDEBET);
  $insertQuery->bindValue('dagboek', $DAGBOEK);
  $insertQuery->bindValue('rubriekid', $rubriekId);

  if ($insertQuery->execute()) {
    return array(true, "Boek aanmaken successvol.");
  }
  return array(false, "Boek aanmaken gefaald");
}

function maakRelatie($userid, $bedrijfsid, $structuur)
{
  global $pdo;
  echo "<h2>Relatie</h2>";
  $land = '';
  if ($structuur['BELASTINGLAND']) {
    $land = $structuur['BELASTINGLAND'];
  } elseif ($structuur['LAND']) {
    $land = $structuur['LAND'];
  }

  $query = $pdo->prepare('INSERT INTO klanten
                  SET DEBITEURNR        = :debiteurnr,
                      ETNISCHEACHTERGR  = :etnischeachtergr,
                      BEDRIJFSNAAM      = :bedrijfsnaam,
                      VOORNAAM          = :voornaam,
                      EMAIL             = :email,
                      TELVAST           = :telvast,
                      ADRES             = :adres,
                      HUISNR            = :huisnr,
                      TOEVOEGING        = :toevoeging,
                      POSTCODE          = :postcode,
                      PLAATS            = :plaats,
                      LAND              = :land,
                      FAX               = :fax,
                      WWW               = :www,
                      KVKNR             = :kvknr,
                      BTWNR             = :btwnr;');
  $query->bindValue('debiteurnr', $structuur['RELATIENR']);
  $query->bindValue('etnischeachtergr', $structuur['RELATIENR']);
  $query->bindValue('bedrijfsnaam', $structuur['NAAM']);
  $query->bindValue('voornaam', $structuur['CONTACT']);
  $query->bindValue('email', $structuur['EMAIL']);
  $query->bindValue('telvast', $structuur['TELVAST']);
  $query->bindValue('adres', $structuur['STRAAT']);
  $query->bindValue('huisnr', $structuur['NUMMER']);
  $query->bindValue('toevoeging', $structuur['TOEVOEGING']);
  $query->bindValue('postcode', $structuur['PCODE']);
  $query->bindValue('plaats', $structuur['PLAATS']);
  $query->bindValue('land', $land);
  $query->bindValue('fax', $structuur['FAX']);
  $query->bindValue('www', $structuur['WWW']);
  $query->bindValue('kvknr', $structuur['KVKNUMMER']);
  $query->bindValue('btwnr', $structuur['BELASINGNR']);
  
  return $query->execute();
}

function maakTransactie($userid, $bedrijfsid, $structuur)
{
  global $pdo;

  $message = '';
  //echo "Journal : ". $structuur['journal']['ID']. "<br/>";
  //echo "Transaction : ". $structuur['transaction']['ID']. "<br/>";
  //echo "Line : ". $structuur['ID']. "<br/>";
  // Validate !
  // Er moeten al boeken en klanten bestaan om deze records ook levend de database in te schieten
  // Bestaat klant?
  if ( ! array_key_exists( 'ACCOUNTID', $structuur ) ) {
    return array( false, oft_audit_teksten('account_onvindbaar1') );
  }
  
  $query = $pdo->prepare('SELECT *
            FROM klanten
           WHERE ETNISCHEACHTERGR = :etnischeachtergr
             AND BEDRIJFSID = :bedrijfsid
           LIMIT 1;');
  $query->bindValue('etnischeachtergr', $structuur['ACCOUNTID']);
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->execute();

  foreach ($query->fetchAll() as $row) {
  
    // Genoeg informatie voor een dummy
    $sql = "";

    $insertQuery = $pdo->prepare('INSERT INTO klanten
                    SET DEBITEURNR = :debiteurnr,
                        BEDRIJFSID = :bedrijfsid,
                        ETNISCHEACHTERGR = :etnischeachtergr,
                        BEDRIJFSNAAM = :bedrijfsnaam,
                        VOORNAAM = "Geen",
                        EMAIL = "Geen",
                        TELVAST = "Geen",
                        ADRES = "Geen",
                        HUISNR = "Geen",
                        TOEVOEGING = "Geen",
                        POSTCODE = "Geen",
                        PLAATS = "Geen",
                        LAND = "Geen",
                        FAX = "Geen",
                        WWW = "Geen",
                        KVKNR = "Geen",
                        BTWNR = "Geen";');
    $insertQuery->bindValue('debiteurnr', $structuur['ACCOUNTID']);
    $insertQuery->bindValue('bedrijfsid', $bedrijfsid);
    $insertQuery->bindValue('etnischeachtergr', $structuur['ACCOUNTID']);
    $insertQuery->bindValue('bedrijfsnaam', 'Tijdelijk-' . $structuur['ACCOUNTID']);
    
    if ($insertQuery->execute()) {
      $selectKlantenQuery = $pdo->prepare('SELECT *
          FROM klanten
         WHERE ETNISCHEACHTERGR = :etnischeachtergr
           AND BEDRIJFSID = :bedrijfsid
         LIMIT 1;');
      $selectKlantenQuery->bindValue('bedrijfsid', $bedrijfsid);
      $selectKlantenQuery->bindValue('etnischeachtergr', $structuur['ACCOUNTID']);
      $selectKlantenQuery->execute();
    } else {
      $message .= tl( oft_audit_teksten('relatie_onvindbaar1') ). $structuur['transaction']['ID'] . tl( oft_audit_teksten('relatie_onvindbaar2') );
      return array(false, $message);
    }
  }
  $klant = $selectKlantenQuery->fetch();
  $klant_id = $klant['ID']*1;

  // Bestaat het boek?
  $selectGrootboekQuery = $pdo->prepare('SELECT *
            FROM grootboekitems
           WHERE GROOTBOEKNR = :grootboeknr
             AND BEDRIJFSID = :bedrijfsid
           LIMIT 1;');
  $selectKlantenQuery->bindValue('grootboeknr', $structuur['ACCOUNTID']);
  $selectKlantenQuery->bindValue('bedrijfsid', $bedrijfsid);
  $selectKlantenQuery->execute();
  
  $boek = $selectGrootboekQuery->fetch();
  if (empty($boek)) {
    $message .= tl( oft_audit_teksten('accountid1') ). ps($structuur['ID']). tl( oft_audit_teksten('accountid2') ). $structuur['transaction']['ID']. tl( oft_audit_teksten('accountid3') );
    return array(false, $message);
  }
  
  $boek_id = $boek['ID']* 1;
  $record_id = $structuur['journal']['ID'] . '|' . $structuur['transaction']['ID'] . '|' . $structuur['ID'];

  if ($boek_id) {
    $bedrag = $structuur['DEBET_BEDRAG']* 1; //$structuur['VALUTA_DEBET_BEDRAG'] * 1;
    if ($bedrag * 1 == 0) {
      $bedrag = $structuur['CREDIT_BEDRAG']* -1; //$structuur['VALUTA_CREDIT_BEDRAG'] * 1;
    }

    $selectBalansQuery = $pdo->prepare('SELECT ID FROM balans WHERE BATCHCODE = :batchcode AND BEDRIJFSID = :bedrijfsid limit 1;');
    $selectBalansQuery->bindValue('batchcode', $record_id);
    $selectBalansQuery->bindValue('bedrijfsid', $bedrijfsid);
    $selectBalansQuery->execute();
    $balansData = $selectBalansQuery->fetch(PDO::FETCH_ASSOC);

    if($balansData !== false) {
      insertIntoMemoriaal('1', $bedrag, '(Geen)', '(Geen)', $boek_id, '10000', $structuur['DATUM'], $structuur['DATUM'], $bedrijfsid, $record_id, $structuur['OMSCHRIJVING'], $klant_id);
    } else {
      $message .= tl( oft_audit_teksten('al_geimporteerd1') ). $record_id. tl( oft_audit_teksten('al_geimporteerd2') );
      return array(false, $message);
    }
  }
  return array(true, $message);
}

function verwijder_boekhouding($userid, $bedrijfsid, $startdatum, $einddatum)
{
  global $pdo;
  // Gooi de klanten leeg.
  // Gooi de boeken leeg en weg.
  // Gooi alle transacties weg.
  $return = true;

  $deleteKlantenQuery = $pdo->prepare('DELETE FROM klanten WHERE BEDRIJFSID = :bedrijfsid;');
  $deleteKlantenQuery->bindValue('bedrijfsid', $bedrijfsid);
  
  if ($deleteKlantenQuery->execute() === false) {
    $return = false;
  }
  
  $deleteGrootBoekQuery = $pdo->prepare('DELETE FROM grootboekitems WHERE BEDRIJFSID = :bedrijfsid;');
  $deleteGrootBoekQuery->bindValue('bedrijfsid', $bedrijfsid);
  
  if ($deleteGrootBoekQuery->execute() === false) {
    $return = false;
  }
  
  $deleteBalansRubriekQuery = $pdo->prepare('DELETE FROM balansrubriek WHERE BEDRIJFSID = :bedrijfsid;');
  $deleteBalansRubriekQuery->bindValue('bedrijfsid', $bedrijfsid);
  
  if ($deleteBalansRubriekQuery->execute() === false) {
    $return = false;
  }
  
  $deleteBalansQuery = $pdo->prepare('DELETE FROM balans WHERE BEDRIJFSID = :bedrijfsid;');
  $deleteBalansQuery->bindValue('bedrijfsid', $bedrijfsid);
  
  if ($deleteBalansQuery->execute() === false) {
    $return = false;
  }
  
  return $return;
}

// Deze functies staan niet in de orangefield tree?
function insertIntoMemoriaal($TYPE, $BEDRAG, $VERKOOPBOEKID, $INKOOPBOEKID, $GROOTBOEKNR, $TEGENREKENING, $DATUM, $BOEKDATUM, $BEDRIJFSID, $BATCHCODE, $OMSCHRIJVING, $KLANTID)
{
  global $pdo;

  if (($TYPE * 1) == 0) {
    $TYPE = 1;
  }

  $return = false;
  if (boekjaaropen($DATUM, $BEDRIJFSID)) {

    $queryInsertBalans = $pdo->prepare('INSERT INTO balans SET TYPE = :type,
                                       BEDRAG = :bedrag,
                                       VERKOOPBOEKID = :verkoopboekid,
                                       INKOOPBOEKID = :inkoopboekid,
                                       GROOTBOEKNR = :grootboeknr,
                                       TEGENREKENING = :tegenrekening,
                                       DATUM = :datum,
                                       BOEKDATUM = :boekdatum,
                                       BATCHCODE = :batchcode,
                                       OMSCHRIJVING = :omschrijving,
                                       KLANTID = :klantid,
                                       BEDRIJFSID = :bedrijfsid;');
    $queryInsertBalans->bindValue('type', $TYPE);
    $queryInsertBalans->bindValue('bedrag', $BEDRAG);
    $queryInsertBalans->bindValue('verkoopboekid', $VERKOOPBOEKID);
    $queryInsertBalans->bindValue('inkoopboekid', $INKOOPBOEKID);
    $queryInsertBalans->bindValue('grootboeknr', $GROOTBOEKNR);
    $queryInsertBalans->bindValue('tegenrekening', $TEGENREKENING);
    $queryInsertBalans->bindValue('datum', $DATUM);
    $queryInsertBalans->bindValue('boekdatum', $BOEKDATUM);
    $queryInsertBalans->bindValue('batchcode', $BATCHCODE);
    $queryInsertBalans->bindValue('omschrijving', str_replace("\xC2\x80", "", $OMSCHRIJVING));
    $queryInsertBalans->bindValue('klantid', $KLANTID);
    $queryInsertBalans->bindValue('bedrijfsid', $BEDRIJFSID);
    
    
    if ($queryInsertBalans->execute()) {
      update_factuur_saldo($VERKOOPBOEKID, $BEDRIJFSID);
      update_factuur_saldo_inkoop('', $BEDRIJFSID, $INKOOPBOEKID);
      delCacheFinance($DATUM, "memoriaal", $BEDRIJFSID, $GROOTBOEKNR);
      delCacheFinance($DATUM, "memoriaal", $BEDRIJFSID, $TEGENREKENING);
    }
  }

  return $return;
}

function update_factuur_saldo($fId, $bedrijfsid)
{
  global $pdo;

  $fId *= 1;
  if ($fId > 0) {
    $array = get_factuur_saldo($fId);

    $TBEDRAG = $array["BEDRAG"];
    $TBETAALD = $array["BETAALD"];
    $TSALDO = $array["SALDO"];

    $betaald = 'Nee'; 
    
    if ((prijsconversie($TSALDO) * 1) == 0) {
      $betaald = 'Ja';
    } 
    
    $query = $pdo->prepare('UPDATE facturen
                   set BETAALD = "Nee", TBEDRAG = :tbedrag, TBETAALD = :tbetaald, TSALDO = :tsaldo
                 WHERE ID = :fid;');
    $query->bindValue('betaald', $betaald);
    $query->bindValue('tbedrag', $TBEDRAG);
    $query->bindValue('tbetaald', $TBETAALD);
    $query->bindValue('tsaldo', $TSALDO);
    $query->bindValue('fid', $fId);
    $query->execute();

    return $TSALDO;
  } else {
    return 0;
  }
}

function update_factuur_saldo_inkoop($REFERENTIENR, $BEDRIJFSID, $ID = 0)
{
  global $pdo;

  if ($REFERENTIENR == '' && $ID > 0) {
    $REFERENTIENR = getNaam("kostenintern", $ID, "REFERENTIENR");
  }

  if ($REFERENTIENR != '') {
    $array = get_factuur_saldo_inkoop($REFERENTIENR, $BEDRIJFSID);

    $TBEDRAG = $array["BEDRAG"];
    $TBETAALD = $array["BETAALD"];
    $TSALDO = $array["SALDO"];
    $TBTW = $array["BTW"];

    $betaald = 'Nee';
    if ((prijsconversie($TSALDO) * 1) == 0) {
      $betaald = 'Ja';
    } 
    
    $query = $pdo->prepare('UPDATE kostenintern
                  set BETAALD = :betaald,
                      TBEDRAG = :tbedrag,
                      TBETAALD = :tbetaald,
                      TBTW = :tbtw,
                      TSALDO = :tsaldo
                WHERE BEDRIJFSID = :bedrijfsid AND REFERENTIENR = :referentienr;');
    $query->bindValue('betaald', $betaald); 
    $query->bindValue('tbedrag', $TBEDRAG);
    $query->bindValue('tbetaald', $TBETAALD);
    $query->bindValue('tbtw', $TBTW);
    $query->bindValue('tsaldo', $TSALDO);
    $query->bindValue('bedrijfsid', $BEDRIJFSID);
    $query->bindValue('referentienr', $REFERENTIENR);
    $query->execute();

    return $TSALDO;
  } else {
    return 0;
  }
}

function delCacheFinance($datum, $type, $bedrijfsid, $GBNR)
{
  return true;
}
