<?php
sercurityCheck();

function oft_entitie_numbers($user, $company)
{
//

    $title = "Corporate Numbers";

    $submenuItems = oft_inputform_link('content.php?SITE=oft_entitie_numbers_add', "Add Numbers");
    $backButton = oft_back_button_entity();
    $contentFrame = oft_entitie_numbers_ajax($user, $company, true);
    echo oft_framework_menu($user, $company, $contentFrame, $title, $submenuItems, $backButton);
}

function oft_entitie_numbers_detail($user, $company)
{
    $titel = tl('Debt register');
    $submenuitems = oft_inputform_link('content.php?SITE=oft_registers_debt_tracker_add', "Add item");
    $submenuitems .= ' | ' . "<a class=\"cursor\" onClick=\"window.location.href='content.php?SITE=oft_registers_debt_tracker_print&PRINT=true&PAGNR=-1" . registers_showdate_url() . "&SRCH_BEDRIJFSNAAM='+document.getElementById('SRCH_BEDRIJFSNAAM').value+'&SRCH_LAND='+document.getElementById('SRCH_LAND').value+'&SRCH_SEARCH='+document.getElementById('SRCH_SEARCH').value;\" target=\"_blank\">Printable version</a>";
    $back_button = '';

    $table = "oft_debt_tracker_detail";
    $arrayVelden = oft_get_structure($user, $company, $table . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($user, $company, $table . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($user, $company, $table . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($user, $company, $table . "_overzicht", "arrayZoekvelden");

    $add = "content.php?SITE=oft_debt_tracker_add";
    $edit = oft_inputform_link_short("content.php?SITE=oft_registers_debt_tracker_edit&ID=-ID-");
    $delete = "";

    $sql = registers_showdate();

    $contentFrame = "<h3>Detail debt overview</h3><br/><table class=\"oft_tabel\" cellpadding=\"5\">
                    <tr><td>Debt reference</td><td align=\"right\">oft.00.1</td>
                    <tr><td>Borrower name</td><td align=\"right\">OrangeField Trust</td>
                    <tr><td>Principal amount</td><td align=\"right\">1.000.000</td>
                    <tr><td>Interest rate</td><td align=\"right\">15%</td>
                    <tr><td>Start date</td><td align=\"right\">1st April</td>
                    <tr><td>Term</td><td align=\"right\">5 years</td>
                    <tr><td>Next interest due date</td><td align=\"right\">1st July</td>
                    <tr><td>Next payment due</td><td align=\"right\">150.000</td>
                   </table><br/><br/>
                   <h3>Payments</h3>";

    $contentFrame .= oft_tabel($user, $company, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $table,
        'ID DESC', $add, $edit, $delete, $sql);

    echo oft_framework_menu($user, $company, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_numbers_ajax($user, $company, $return = false)
{
    $table = "oft_entitie_numbers";
    $order = "TYPE";
    $fields = oft_get_structure($user, $company, "oft_entitie_numbers_overzicht", "all");

    $add = "content.php?SITE=oft_entitie_numbers_add";
    $edit = oft_inputform_link_short("content.php?SITE=oft_registers_debt_tracker_edit&ID=-ID-");
    $delete = "";

    $sql = registers_showdate();

    if ($return) {
        return oft_tabel($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete, $sql);
    }
    echo oft_tabel_content($user, $company,
        $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
        $table, $order, $add, $edit, $delete, $sql);
    return true;
}

function oft_entitie_numbers_print($user, $company)
{
    global $db;

    $table = "oft_debt_tracker";
    $arrayVelden = oft_get_structure($user, $company, $table, "arrayVelden");
    $arrayVeldnamen = oft_get_structure($user, $company, $table, "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($user, $company, $table, "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($user, $company, $table, "arrayZoekvelden");

    $add = "";
    $edit = "";
    $delete = "";

    $sql = registers_showdate();

    echo "<h1>Debt register</h1><br/><br/>";

    echo oft_tabel_print($user, $company, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $table,
        'ID DESC', $add, $edit, $delete, $sql);
}

function oft_entitie_numbers_add($user, $company)
{
    global $db;

    $templateFile = "./templates/oft_numbers.htm";
    $table = "oft_entitie_numbers";

    echo "<h2>Add Number</h2>";

    echo replace_template_content($user, $company, $table, $templateFile);
}

function oft_entitie_numbers_edit($user, $company)
{
    global $db;

    $templateFile = "./templates/oft_numbers.htm";
    $table = "oft_entitie_numbers";

    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }

    echo "<h2>Edit Number</h2>";

    echo replace_template_content_edit($user, $company, $table, $templateFile, $id);
}

function oft_entitie_numbers_1_print($user, $company)
{
    global $db;

    $templateFile = "./templates/oft_debt_tracker_print.htm";
    $table = "oft_debt_tracker";

    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }

    echo "<h2>Print Debt register</h2>";

    echo replace_template_content_edit($user, $company, $table, $templateFile, $id);
}