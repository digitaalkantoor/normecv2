<?php
sercurityCheck();

function oft_document_legenda($userid, $bedrijfsid)
{
    global $db;

    $c = "<div class='oft2_page_centering'>";
    $c .= "<br><br>";
    $c .= "Legend:";
    $c .= "<table class=\"sortable\">";
    $c .= "<tr><td><img border=\"0\" src=\"./images/oft/oft_white_circle.png\"></td><td>Not required or due date more than 30 days left</td></tr>";
    $c .= "<tr><td><img border=\"0\" src=\"./images/oft/oft_green_circle.png\"></td><td>File uploaded</td></tr>";
    $c .= "<tr><td><img border=\"0\" src=\"./images/oft/oft_orange_circle.png\"></td><td>Due date is within 30 days</td></tr>";
    $c .= "<tr><td><img border=\"0\" src=\"./images/oft/oft_red_circle.png\"></td><td>Due date is within 14 days</td></tr>";
    $c .= "<tr><td><img border=\"0\" src=\"./images/oft/oft_black_circle.png\"></td><td>Due date has expired, or document has been filed after deadline</td></tr>";

    $c .= "</table>";
    $c .= "</div>";

    return $c;
}

function oft_document($userid, $bedrijfsid)
{
    global $db;

    getRechten($userid, "BESTANDEN");

    $_REQUEST["SRCH_YEAR"] = "x" . date("Y");
    $_REQUEST["SRCH_JAAR"] = date("Y");

    $titel = tl('Legal documents');
    $tabel = "documentbeheer";
    $toevoegen = "content.php?SITE=oft_document_add&SECTIE=Legal";
    $bewerken = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
    $verwijderen = "";
    $submenuitems = "<a href=\"content.php?SITE=dragAndDrop&SECTION=oft_document\">Upload multiple documents</a>  | " . oft_inputform_link($toevoegen,
            "New document");

    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, getDoctypesBySection("Legal"));

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_document_ajax($userid, $bedrijfsid)
{
    $tabel = "documentbeheer";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $toevoegen = "";
    $bewerken = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
    $verwijderen = "";

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, getDoctypesBySection("Legal"));

}

function oft_document_add($userid, $bedrijfsid)
{
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $templateFile = "./templates/oft_document_dif.htm";
    } else {
        $templateFile = "./templates/oft_document.htm";
    }
    $tabel = "documentbeheer";
    $titel = "Add document(s)";

    oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_documents_add($userid, $bedrijfsid)
{
//    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
//        $templateFile = "./templates/oft_document_dif.htm";
//    } else {
//        $templateFile = "./templates/oft_document.htm";
//    }
//    $tabel = "documentbeheer";
//    $titel = "Add document(s)";
//
//    oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);


    oft_add_page($userid, $bedrijfsid, "./templates/file_management_create.html", "documentbeheer",
        "Add file(s)");
}

function oft_document_edit($userid, $bedrijfsid)
{
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $templateFile = "./templates/oft_document_edit_dif.htm";
    } else {
        $templateFile = "./templates/oft_document_edit.htm";
    }

    $tabel = "documentbeheer";
    $titel = "Edit document";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_documents($userid, $bedrijfsid)
{
    global $db;

    echo oft_standaard_menu_page($userid, $bedrijfsid, "Documents", "oft_entitie_documents", "documentbeheer",
        "BESTANDSNAAM", oft_back_button_entity(), "Add document", "");
}

function oft_entitie_documents_ajax($user, $company)
{
    $table = "documentbeheer";
    $order = "BESTANDSNAAM";

    $fields = oft_get_structure($user, $company, 'oft_entitie_documents_overzicht', "all");

    $add = "content.php?SITE=oft_document_add&SECTIE=Legal";
    $edit = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
    $delete = "";

    $sql = "AND $table.BEDRIJFSID = '" . ps($company, "nr") . "' " . getDoctypesBySection("Legal");

    echo oft_tabel_content($user, $company,
        $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
        $table, $order, $add, $edit, $delete, $sql);
}

function oft_entitie_resolutions($userid, $bedrijfsid)
{
    global $pdo;

    if (isset($_REQUEST["BID"])) {
        $bedrijfsid = ps($_REQUEST["BID"], "nr");
    }

    $titel = oft_titel_entities($bedrijfsid);
    $submenuitems = '';
    if (isRechtenOFT($bedrijfsid, "CRM")) {
        $submenuitems .= oft_inputform_link('content.php?SITE=oft_document_add&SECTIE=Legal&BID=' . $bedrijfsid . '&DOCTYPE=Board resolution, Shareholders resolutions, Board minutes, Shareholder minutes',
            "Upload resolution");
    }
    $back_button = oft_back_button_entity();

    //Search velden
    //0 => "YEAR",         1 => "UPDATEDBY"
    $url_querystring = oft_tabel_querystring();
    $search = "<br/><table class=\"tabel\"><tr>";
    $search .= "<td class=\"oft_label\">Year</td><td>";
    $queryYear = $pdo->prepare('SELECT distinct YEAR(DATUM) As JAAR FROM documentbeheer GROUP BY JAAR ORDER BY JAAR');
    $search .= oft_select($url_querystring . "&SRCH_YEAR=x'+document.getElementById('SRCH_JAAR').value+'", "JAAR",
        "JAAR", $queryYear);
    $search .= "</td>";
    //Zoek bedrijf
    $search .= ("<td class=\"oft_label\">Updated by</td><td>");
    $queryEntity = $pdo->prepare('SELECT distinct personeel.ID, personeel.ACHTERNAAM FROM personeel, documentbeheer WHERE (documentbeheer.PERSONEELSLID) = personeel.ID AND (NOT personeel.DELETED = "Ja" OR personeel.DELETED is null) ORDER BY personeel.ACHTERNAAM');
    $search .= oft_select($url_querystring . "&UPDATEDBY=x'+document.getElementById('SRCH_ACHTERNAAM').value+'&SRCH_BEDRIJFSID=",
        "ID", "ACHTERNAAM", $queryEntity);
    $search .= "</td>";
    $search .= "</tr></table>\n<br/>";

    $contentFrame = "<h1>Resolutions</h1>";

    $contentFrame .= $search;

    $contentFrame .= "<div id=\"contentTable\">";

    $contentFrame .= oft_entitie_resolutions_ajax($userid, $bedrijfsid, "Ja");

    $contentFrame .= "</div>";

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_resolutions_ajax($userid, $bedrijfsid, $return)
{
    if (isset($_REQUEST["BID"])) {
        $bedrijfsid = ps($_REQUEST["BID"], "nr");
    }
    $table = "documentbeheer";

    $arrayVelden = array(0 => "NAAM", 1 => "JAAR", 2 => "DATUM", 4 => "PERSONEELSLID", 5 => "BESTANDSNAAM");
    $arrayVeldnamen = array(0 => "Name", 1 => "Year", 2 => "Start date", 4 => "Uploaded by", 5 => "File");
    $arrayVeldType = array(0 => "field", 1 => "year", 2 => "datum", 4 => "person_name", 5 => "bestand");
    $arrayZoekvelden = array();

    $toevoegen = "content.php?SITE=oft_document_add&SECTIE=Legal&BID=$bedrijfsid&DOCTYPE=Board resolution, Shareholders resolutions, Board minutes, Shareholder minutes";
    $bewerken = oft_inputform_link_short("content.php?SITE=oft_document_edit&BID=$bedrijfsid&ID=-ID-");
    $verwijderen = "";

    $contentFrame = "<h2>Board resolutions</h2>";

    $sql_toevoegen = "AND $table.BEDRIJFSID = '" . ps($bedrijfsid,
            "nr") . "' AND ($table.DOCTYPE = 'Board resolution' OR $table.DOCTYPE = 'Board resolutions' OR $table.DOCTYPE = 'Board minutes')";
    $contentFrame .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $table, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, $sql_toevoegen);

    $contentFrame .= "<br/><Br/><h2>Shareholders resolutions</h2>";

    $sql_toevoegen = "AND $table.BEDRIJFSID = '" . ps($bedrijfsid,
            "nr") . "' AND ($table.DOCTYPE = 'Shareholders resolution' OR $table.DOCTYPE = 'Shareholders resolutions' OR $table.DOCTYPE = 'Shareholder minutes')";
    $contentFrame .= oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType,
        $arrayZoekvelden, $table, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, $sql_toevoegen);

    if ($return == "Ja") {
        return $contentFrame;
    } else {
        echo $contentFrame;
    }
}

function export_files_to_stick($userid, $bedrijfsid, $docType)
{
    global $pdo;

    $bestandlocatie = bestandLocatie();
    $files_to_zip = array();

    $docTypeSql = "";
    if ($docType != '') {
        $docTypeSql = "AND documentbeheer.DOCTYPE = '" . ps($docType) . "' AND JAAR = '2014'"; //Jaar 2014? Voor altijd?
    }

    $query = $pdo->prepare('SELECT documentbeheer.*, bedrijf.LAND, bedrijf.BEDRIJFSNAAM
              FROM documentbeheer, bedrijf
             WHERE true $docTypeSql
               AND bedrijf.ID = documentbeheer.BEDRIJFSID;');
    $query->execute();

    foreach ($query->fetchAll() as $dDocs) {
        if (trim($dDocs["LAND"]) == '') {
            $dDocs["LAND"] = "onbekend";
        }
        $path = $_SERVER["DOCUMENT_ROOT"] . "/" . $bestandlocatie . "/store/" . date("Ymd") . "/" . bestandsnaam_clean($dDocs["LAND"]) . "/" . bestandsnaam_clean($dDocs["BEDRIJFSNAAM"]) . "/";

        if (!file_exists($path)) {
            RecursiveMkdir($path);
        }

        if (file_exists($path)) {
            $file = $path . bestandsnaam_clean($dDocs["BESTANDSNAAM"]);
            $files_to_zip[] = $file;
            echo "<br/>" . $file;
            $fp = fopen($file, "w");
            fwrite($fp, $dDocs["BESTANDCONTENT"]);
            fclose($fp);
        }
    }
}

/* creates a compressed zip file */
function create_zip($files = array(), $destination = '', $overwrite = false)
{
    //if the zip file already exists and overwrite is false, return false
    if (file_exists($destination) && !$overwrite) {
        return false;
    }
    //vars
    $valid_files = array();
    //if files were passed in...
    if (is_array($files)) {
        //cycle through each file
        foreach ($files as $file) {
            //make sure the file exists
            if (file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    //if we have good files...
    if (count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if ($zip->open($destination, ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach ($valid_files as $file) {
            $zip->addFile($file, $file);
        }
        //debug
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

        //close the zip -- done!
        $zip->close();

        //check to make sure the file exists
        return file_exists($destination);
    } else {
        return false;
    }
}

function create_zip_v2($files = array(), $destination = '', $overwrite = false)
{
    if (file_exists($destination) && !$overwrite) {
        return false;
    }

    $zip = new ZipArchive();
    if ($zip->open($destination, ZIPARCHIVE::CREATE) !== true) {
        return false;
    }

    if (is_array($files)) {
        foreach ($files as $file) {
            if (!file_exists($file)) {
                continue;
            } //Skip when file not exists
            $arr = explode('/', $file);
            $localfile = end($arr);

            $zip->addFile($file, $localfile);
        }
    }

    return $zip->close();
}
