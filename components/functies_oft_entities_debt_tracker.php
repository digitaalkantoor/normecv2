<?php
sercurityCheck();

function oft_registers_debt_tracker($userid, $bedrijfsid) {
  global $db;

  $titel           = tl('Debt register');
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_registers_debt_tracker_add', "Add item", 500)
                     . ' | '. "<a class=\"cursor\" onClick=\"window.location.href='content.php?SITE=oft_registers_debt_tracker_print&PRINT=true&PAGNR=-1".registers_showdate_url()."&SRCH_BEDRIJFSNAAM='+document.getElementById('SRCH_BEDRIJFSNAAM').value+'&SRCH_LAND='+document.getElementById('SRCH_LAND').value+'&SRCH_SEARCH='+document.getElementById('SRCH_SEARCH').value;\" target=\"_blank\">Printable version</a>";
  $back_button     = "";

  $tabel           = "oft_debt_tracker";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_debt_tracker_add";
  $bewerken        = "content.php?SITE=oft_registers_debt_tracker_detail&ID=-ID-";
  $verwijderen     = "";
  
  $sql             = registers_showdate();
  
  $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_registers_debt_tracker_detail($userid, $bedrijfsid) {
  global $db;

  $titel           = tl('Debt register');
  $submenuitems    = oft_inputform_link('content.php?SITE=oft_registers_debt_tracker_add', "Add item", 500)
                     . ' | '. "<a class=\"cursor\" onClick=\"window.location.href='content.php?SITE=oft_registers_debt_tracker_print&PRINT=true&PAGNR=-1".registers_showdate_url()."&SRCH_BEDRIJFSNAAM='+document.getElementById('SRCH_BEDRIJFSNAAM').value+'&SRCH_LAND='+document.getElementById('SRCH_LAND').value+'&SRCH_SEARCH='+document.getElementById('SRCH_SEARCH').value;\" target=\"_blank\">Printable version</a>";
  $back_button     = "";

  $tabel           = "oft_debt_tracker_detail";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_debt_tracker_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_debt_tracker_edit&ID=-ID-");
  $verwijderen     = "";

  $sql             = registers_showdate();

  $contentFrame = "<h3>Detail debt overview</h3><br/><table class=\"oft_tabel\" cellpadding=\"5\">
                    <tr><td>Debt reference</td><td align=\"right\">oft.00.1</td>
                    <tr><td>Borrower name</td><td align=\"right\">OrangeField Trust</td>
                    <tr><td>Principal amount</td><td align=\"right\">1.000.000</td>
                    <tr><td>Interest rate</td><td align=\"right\">15%</td>
                    <tr><td>Start date</td><td align=\"right\">1st April</td>
                    <tr><td>Term</td><td align=\"right\">5 years</td>
                    <tr><td>Next interest due date</td><td align=\"right\">1st July</td>
                    <tr><td>Next payment due</td><td align=\"right\">150.000</td>
                   </table><br/><br/>
                   <h3>Payments</h3>";

  $contentFrame    .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_registers_debt_tracker_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_debt_tracker";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_debt_tracker_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_registers_debt_tracker_edit&ID=-ID-");
  $verwijderen     = "";
  
  $sql             = registers_showdate();
  
  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_debt_tracker_print($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_debt_tracker";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  $sql             = registers_showdate();

  echo "<h1>Debt register</h1><br/><br/>";

  echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, $sql);
}

function oft_registers_debt_tracker_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_debt_tracker.htm";
  $tabel = "oft_debt_tracker";

  echo "<h2>Add Debt register</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_registers_debt_tracker_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_debt_tracker.htm";
  $tabel = "oft_debt_tracker";

  $id = "";
  if(isset($_REQUEST["ID"])) { 
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit Debt register</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_debt_tracker_1_print($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_debt_tracker_print.htm";
  $tabel = "oft_debt_tracker";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Print Debt register</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}