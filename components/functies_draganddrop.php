<?php

function dragAndDropScript() {
  //<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js\"></script>
  $return = "<script src=\"./scripts/dropzone.js\"></script>";
  $return .= "<link href=\"./style/dropzone.css\" type=text/css rel=stylesheet />";
  return $return;
}

function selectBoxScript() {
//    $return = "<script src=\"./scripts/fancySelect.js\"></script>";
//    $return .= "<link href=\"./style/fancySelect.css\" type=text/css rel=stylesheet />";
//    return $return;
}

function dragAndDrop($userid, $bedrijfsid) {
  global $pdo;

  $titel        = 'Drag and drop - documents';
  $submenuitems = '';
  $section = isset($_REQUEST["SECTION"]) ? $_REQUEST["SECTION"] : (isset($_REQUEST["SECTION"]) ? $_REQUEST["SITE"] : false);

  if(isset($_REQUEST["BID"])) {
    $bedrijfsid = ps($_REQUEST["BID"], "nr");
  }

  $output = "<form action=\"content.php?SITE=genDragAndDropUpload\" class=\"dropzone\" id=\"my-awesome-dropzone\">
            <table class=\"tabel\">
            <tr id=\"veld4\"><td>".tl("Datum").":</td><td align=\"left\">".datumVeld(date("Y-m-d"), "DOCDATUM", "fieldHalf")."</td></tr>";
  
    $query = $pdo->prepare('SELECT NAAM as DOCTYPE FROM stam_documenttype WHERE DELETED != "Ja"');
  
    if ($section && in_array("section", array("oft_finance", "HR","Legal"))) {
      $query = $pdo->prepare('SELECT NAAM as DOCTYPE FROM stam_documenttype WHERE DELETED != "Ja" AND SECTIE = :sectie;');
      $query->bindValue('sectie', $section);
    }

    $oft_select = oft_select('', "DOCTYPE", "DOCTYPE", $query, true, true, "fieldHalf");

    $output .= ("<tr><td>".tl("Document type").":</td><td align=\"left\">".str_replace("SRCH_DOCTYPE", "DOCTYPE", $oft_select)."</td></tr>");
    
    $_REQUEST["SRCH_BEDRIJFSNAAM"] = $bedrijfsid;
    $queryEntity = $pdo->prepare('SELECT ID, BEDRIJFSNAAM FROM bedrijf
        WHERE (NOT DELETED = "Ja" OR DELETED is null) 
        AND NOT LAND = "" AND NOT LAND is null 
        ORDER BY BEDRIJFSNAAM');
    $oft_select_entity = oft_select('', "ID", "BEDRIJFSNAAM", $queryEntity, true, true, "fieldHalf");
    $output .= ("<tr><td>".tl(check("default_name_of_entity", "Entity", 1)).":</td><td align=\"left\">".str_replace("SRCH_BEDRIJFSNAAM", "BID", $oft_select_entity)."</td></tr>");
    $output .= "</table><br/></form>";

    $output = "<div class='oft2_page_centering' style=\"padding-top: 50px;\">$output</div>";

  echo oft_framework_basic($userid, $bedrijfsid, $output, $titel, $submenuitems);
}

function dragAndDropInline($userid, $bedrijfsid, $KOPPELING, $DOCTYPE, $KLANTID) {
  $return = "<form action=\"content.php?SITE=genDragAndDropUpload\" class=\"dropzone\" id=\"my-awesome-dropzone\">
             <input type=\"hidden\" name=\"KOPPELING\" value=\"$KOPPELING\" />
             <input type=\"hidden\" name=\"DOCTYPE\" value=\"$DOCTYPE\" />
             <input type=\"hidden\" name=\"KLANTID\" value=\"$KLANTID\" />
             </form>";
  return $return;
}

function genDragAndDropUpload($userid, $bedrijfsid) {

  if(uploadBestandInDB($_FILES["file"], $bedrijfsid, $userid, '', "", "") > 0)
  {
    echo "ok";
  } else { 
    echo "error";
  }
}