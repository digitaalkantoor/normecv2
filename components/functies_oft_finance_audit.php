<?php
sercurityCheck();

function oft_finance_audit_import($userid, $bedrijfsid) {
  global $db;

  if(isset($_REQUEST["ID"])) {
    $bedrijfsid = $_REQUEST["ID"];
  }

  $titel           = oft_titel_entities($bedrijfsid);
  $submenuitems    = "";
  $back_button     = oft_back_button_entity();
  $contentFrame    = importeerAuditfile($userid, $bedrijfsid);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function uploadAuditFile() {
  $bestand     = $_FILES['X_File']['name'];
  $bestand_tmp = $_FILES['X_File']['tmp_name'];

  $bestandlocatie       = bestandLocatie();
  $savefile = "./$bestandlocatie/temporary/".$bestand;
  uploadBestand("./$bestandlocatie/temporary/", $bestand_tmp, $bestand, $_FILES['X_File']);

  return $savefile;
}

function importeerAuditfile($userid, $bedrijfsid)
{
  global $pdo;
  
  $return = "<h2>Import audit-file (XAF 2.0)</h2><br/><br/>";

  if(!isset($_REQUEST["OPSLAAN"])) {
    $return .= "<form action=\"\" method=\"Post\" enctype=\"multipart/form-data\">";
    $return .= "".fileVeld("X_File", "field")."";
    $return .= "".opslaanButton('OPSLAAN', 'Opslaan', 'btn-action')."";
    $return .= "</form>";
  } else {

  $file = uploadAuditBestand();
  //echo $file; exit;
  //$file = "./bestanden/Staverxml2010.xaf";

  //Get XML String
  $xmlstr = file_get_contents($file);

  //Generate XML
  $xml = new SimpleXMLElement($xmlstr);

  //Get values - header
  $result = $xml->xpath('//header');
  while(list($elm , $node) = each($result)) {
      $noot          = $node->children();

       /*<header>
        <auditfileVersion>CLAIR2.00.00</auditfileVersion>
        <companyID>TF-10328                           </companyID>
        <taxRegistrationNr>NL801108512B01             </taxRegistrationNr>
        <companyName>Heijting en Weerts B.V.          </companyName>
        <companyAddress>Herculesplein 351a            </companyAddress>
        <companyCity>Utrecht                          </companyCity>
        <companyPostalCode>3584 AA                    </companyPostalCode>
        <fiscalYear>2011                              </fiscalYear>
        <startDate>2011-01-01                         </startDate>
        <endDate>2011-12-31                           </endDate>
        <currencyCode>EUR                             </currencyCode>
        <dateCreated>2011-12-01                       </dateCreated>
        <productID>TriaFin5                           </productID>
        <productVersion>1.3.0                         </productVersion>
       </header>*/

      $BTWNR         = addslashes($noot->taxRegistrationNr);
      $BEDRIJFSNAAM  = addslashes($noot->companyName);
      $ADRRES        = addslashes($noot->companyAddress);
      $PLAATS        = addslashes($noot->companyCity);

      //TODO update bedrijfsgegevens?
  }

  //Grootboekrek niet beschikbaar voor andere administraties
  $NIETBESCHIKBAARIN = ";";
  $queryBedrijf = $pdo->prepare('SELECT ID FROM bedrijf WHERE NOT ID = :bedrijfsid;');
  $queryBedrijf->bindValue('bedrijfsid', $bedrijfsid);
  $queryBedrijf->execute();
  $dBedrijf = $queryBedrijf->fetch(PDO::FETCH_ASSOC);

  if($dBedrijf !== false)
  {
    $NIETBESCHIKBAARIN .= ($dBedrijf["ID"]*1) .";";
  }

  //Get values - grootboekrek schema
  $result = $xml->xpath('//ledgerAccount');
  while(list($elm , $node) = each($result)) {
    $noot = $node->children();

    //insert grootboekitem
    $query = $pdo->prepare('SELECT ID FROM grootboekitems WHERE OMSCHRIJVING = :omschrijving AND BEDRIJFSID = :bedrijfsid limit 1;');
    $query->bindValue('omschrijving', $noot->accountID);
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();
    $resg = $query->fetch(PDO::FETCH_ASSOC);

    if(!$resg)
    {
      $DAGBOEK = 'Memoriaal';
      $BALANSKOSTEN = "Balans";
      $CREDITDEBET = 'Debet';
      if($noot->accountType == "Activa") {
        $CREDITDEBET = 'Debet';
        $BALANSKOSTEN = "Balans";
        $DAGBOEK = 'Memoriaal';
      } elseif ($noot->accountType == "Passiva") {
        $CREDITDEBET = 'Credit';
        $BALANSKOSTEN = "Balans";
        $DAGBOEK = 'Memoriaal';
      } elseif ($noot->accountType == "Kosten") {
        $CREDITDEBET = 'Debet';
        $BALANSKOSTEN = "Kosten of opbrengst"; 
        $DAGBOEK = 'Inkoop';
      } elseif ($noot->accountType == "Opbrengsten") {
        $CREDITDEBET = 'Credit';
        $BALANSKOSTEN = "Kosten of opbrengst";
        $DAGBOEK = 'Verkoop';           
      }

      $rubriekNaam = trim($noot->leadDescription);
      $rubriekId = '1';

      if($rubriekNaam != '') {
        $query = $pdo->prepare('SELECT ID FROM balansrubriek WHERE NAAM = :rubrieknaam limit 1;');
        $query->bindValue('rubrieknaam', $rubriekNaam);
        $query->execute();
        $res = $query->fetch(PDO::FETCH_ASSOC);

        if(!$res)
        {
          $query = $pdo->prepare('INSERT into balansrubriek SET 
                                                  NAAM = :naam,
                                                  BEDRIJFSID = :bedrijfsid;');
          $query->bindValue('naam', $rubriekNaam);
          $query->bindValue('bedrijfsid', $bedrijfsid);
          $query->execute();
          
          $rubriekId = $pdo->lastInsertId();
        } else {
          $rubriekId = $res["ID"];
        }
      }

      $query = $pdo->prepare('INSERT into grootboekitems SET GROOTBOEKNR       = :grootboeknr,
                                                              NAAM              = :naam,
                                                              OMSCHRIJVING      = :omschrijving,
                                                              BALANSKOSTEN      = :balanskosten,
                                                              BEDRIJFSID        = :bedrijfid,
                                                              NIETBESCHIKBAARIN = :nietbeschikbaarin,
                                                              GEBLOKKEERD       = "Nee",
                                                              TOONINGRAPH       = "Nee",
                                                              ISSALARIS         = "Nee", 
                                                              OFFERTENR         = "",
                                                              CREDITDEBET       = :creditdebet,
                                                              AANPASBAAR        = "Ja",
                                                              DAGBOEK           = :dagboek,
                                                              RUBRIEK           = :rubriek');
      $query->bindValue('grootboeknr', $noot->accountID);
      $query->bindValue('naam', $noot->accountDesc);
      $query->bindValue('omschrijving', $noot->accountID);
      $query->bindValue('balanskosten', $BALANSKOSTEN);
      $query->bindValue('bedrijfid', $bedrijfsid);
      $query->bindValue('nietbeschikbaarin', $NIETBESCHIKBAARIN);
      $query->bindValue('creditdebet', $CREDITDEBET);
      $query->bindValue('dagboek', $DAGBOEK);
      $query->bindValue('rubriek', $rubriekId);
      

      
      if($query->execute()) {
        $return .= "<br/>".tl("Nieuwe rubriek").": ".$noot->accountDesc;
      }
    }
  }
  
  $result = $xml->xpath('//customerSupplier');
  while(list($elm , $node) = each($result)) {
    $noot = $node->children();

    $custSupID = '';         if(isset($noot->custSupID)) { $custSupID = $noot->custSupID; }
    $type = 'Klant';         if(isset($noot->type)) { $type = $noot->type; }
    $taxRegistrationNr = ''; if(isset($noot->taxRegistrationNr)) { $taxRegistrationNr = $noot->taxRegistrationNr; }
    $streetname = '';        if(isset($noot->streetAddress->streetname)) { $streetname = $noot->streetAddress->streetname; }
    $number = '';            if(isset($noot->streetAddress->number)) { $number = $noot->streetAddress->number; }
    $city = '';              if(isset($noot->streetAddress->city)) { $city = $noot->streetAddress->city; }
    $postalCode = '';        if(isset($noot->streetAddress->postalCode)) { $postalCode = $noot->streetAddress->postalCode; }
    $country = '';           if(isset($noot->streetAddress->country)) { $country = $noot->streetAddress->country; }
    $telephone = '';         if(isset($noot->telephone)) { $telephone = $noot->telephone; }
    $fax = '';               if(isset($noot->fax)) { $fax = $noot->fax; }
    $eMail = '';             if(isset($noot->eMail)) { $eMail = $noot->eMail; }

    insertKlanten($userid,
                  $bedrijfsid, 
                  $noot->companyName, 
                  array("DEBITEURNR" => $custSupID,
                        "ETNISCHEACHTERGR" => $custSupID,
                        "TYPERELATIE" => $type,
                        "BTWNR" => $taxRegistrationNr,
                        "ADRES" => $streetname,
                        "HUISNR" => $number,
                        "PLAATS" => $city,
                        "POSTCODE" => $postalCode,
                        "LAND" => $country,
                        "TELVAST" => $telephone,
                        "FAX" => $fax,
                        "EMAIL" => $eMail,
                       ));
  }

  //Get values - grootboekrek schema
  $telTransacties = 0;
  $journals = $xml->xpath('//journal');
  while(list($elm, $journal) = each($journals)) {
    $journal_id = $journal->journalID;
    $journal_type = $journal->type;
    $journal_desctiption = $journal->description;

    foreach($journal->transaction As $transNr => $transaction) {
        $transaction_id = $transaction->transactionID;
        $transaction_period = $transaction->period;
        $transaction_date = $transaction->transactionDate;
        $transaction_source = $transaction->sourceID;

        foreach($transaction->line As $lineNr => $line) {
          //Get grootboeknummer
          $GROOTBOEKID = "";
          $queryGB = $pdo->prepare('SELECT ID FROM grootboekitems WHERE GROOTBOEKNR = :accountid AND BEDRIJFSID = :bedrijfsid limit 1;');
          $queryGB->bindValue('accountid', $line->accountID);
          $queryGB->bindValue('bedrijfsid', $bedrijfsid);
          $queryGB->execute();
          $datGB = $queryGB->fetch(PDO::FETCH_ASSOC);

          if($datGB !== false)
          {
            $GROOTBOEKID = $datGB["ID"];
          }

          //Create record Id
          $recordID = $journal_id.'|'.$transaction_id.'|'.$line->recordID;

          if($GROOTBOEKID != "") {
            $bedrag = $line->debitAmount;
            if(($bedrag*1) == 0) {
              $bedrag = $line->creditAmount*-1;
            }

            $btw = $line->vat->vatPercentage;

            $custSupID = "";
            $queryKl = $pdo->prepare('SELECT ID FROM klanten WHERE ETNISCHEACHTERGR = :etnischeachtergrond limit 1;');
            $queryKl->bindValue('etnischeachtergrond', $line->custSupID);
            $queryKl->execute();
            $datKl = $queryKl->fetch(PDO::FETCH_ASSOC);

            if($datKl !== false)
            {
              $custSupID = $datKl["ID"];
            }

            $resg = $pdo->prepare('SELECT ID FROM balans WHERE BATCHCODE = :batchcode AND BEDRIJFSID = :bedrijfsid limit 1;');
            $resg->bindValue('batchcode', $recordID);
            $resg->bindValue('bedrijfsid', $bedrijfsid);
            $resg->execute();
            $resgData = $query->fetch(PDO::FETCH_ASSOC);

            if(!$resgData)
            {
              insertIntoMemoriaal('1', $bedrag, '(Geen)', '(Geen)', $GROOTBOEKID, '10000', $line->effectiveDate, $line->effectiveDate, $bedrijfsid, $recordID, $line->description, $custSupID);
              $telTransacties++;
            } else {
              $return .= "<br/>".tl("recordID reeds geimporteerd").": ". $recordID;
            }

             $telTransacties++;
          } else {
            $return .= "<br/>".tl("accountID niet gevonden").": ". $line->accountID;
          }
       }
     }
   }

    //Verwijder bestand
    unlink($file);
    $return .= "<br/>".tl("Auditfile imported!");
  }

  return $return;
}

function uploadAuditBestand() {
    $bestand = $_FILES['X_File']['name'];
    $bestand_tmp = $_FILES['X_File']['tmp_name'];

    $bestandlocatie = bestandLocatie();
    $savefile = "./$bestandlocatie/temporary/".$bestand;
    //echo $savefile;
    if (file_exists($savefile)) {
    	unlink($savefile);
    }

    uploadBestand("./$bestandlocatie/temporary/", $bestand_tmp, $bestand, $_FILES);

    return $savefile;
}