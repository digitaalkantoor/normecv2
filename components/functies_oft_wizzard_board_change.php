<?php
sercurityCheck();

function oft_wizzard_board_change_entities($userid, $bedrijfsid, $typeform) {
  global $db;

  if(isset($_REQUEST["WZ_OPSLAAN"])) {
    $fields["ENTITYLIST"] = "x";
    if(oft_save_wizzard_step($userid, $bedrijfsid, $typeform, '1', $fields)) {
      rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
    }
  }

  $selectJa = "";
  $selectNee = "checked=\"true\"";
  
  $downloadlink = "";
  $rechtenTot = rechtenTot('board_change', $bedrijfsid, '', true, getWizzardValue($typeform, $bedrijfsid, 'ENTITYLIST'));

  $return = "<form action=\"content.php?SITE=$typeform&STAP=1&BID=$bedrijfsid\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Enities</h2><br/>";
  $return .= $rechtenTot[0];
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Next\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}

function oft_wizzard_board_change_article($userid, $bedrijfsid, $typeform) {
  global $db;

  if(isset($_REQUEST["WZ_OPSLAAN"])) {
    $fields["article"] = "";
    $fields["date of resolution"] = "";
    if(oft_save_wizzard_step($userid, $bedrijfsid, $typeform, '2', $fields)) {
      //Create documets
      $fields["date of resolution"] = getWizzardValue($typeform, $bedrijfsid, 'DATESHAREHOLDERSRESOLUTION');
      $fields["article"] = getWizzardValue($typeform, $bedrijfsid, 'ARTICLE');
      rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=3");
    }
  } elseif(isset($_REQUEST["WZ_BACK"])) {
    rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
  }

  $return = "<form action=\"content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Transaction</h2><br/>";
  $return .= "<table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"5\">";
  $return .= "<tr><td>Date of shareholders&acute; resolution</td><td><input type=\"text\" name=\"DATESHAREHOLDERSRESOLUTION\" value=\"".getWizzardValue($typeform, $bedrijfsid, 'DATESHAREHOLDERSRESOLUTION')."\" class=\"field3\" onClick=\"$(this).datepicker({dateFormat: 'yy-mm-dd'}).datepicker('show');\" /></td></tr>";
  $return .= "<tr><td colspan=\"2\"><br/>Articles and paragraph regarding distribution [interim] dividend</td></tr>
              <tr><td colspan=\"2\"><input type=\"text\" name=\"ARTICLE\" value=\"".getWizzardValue($typeform, $bedrijfsid, 'ARTICLE')."\" class=\"field\" /></td></tr>";
  $return .= "</table>";
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_BACK\" value=\"Back\" class=\"button\" /><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Next\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}

function oft_wizzard_board_change_members($userid, $bedrijfsid, $typeform) {
  global $pdo;
  
  if(isset($_REQUEST["WZ_OPSLAAN"])) {
    $fields["MEMBERLIST"] = "";
    $fields["NEWMEMBERLIST"] = getWizzardValue($typeform, $bedrijfsid, 'NEWMEMBERLIST');
    if(oft_save_wizzard_step($userid, $bedrijfsid, $typeform, '1', $fields)) {
      rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=4");
    }
  } elseif(isset($_REQUEST["WZ_BACK"])) {
    rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
  }

  $MEMBERLIST = getWizzardValue($typeform, $bedrijfsid, 'MEMBERLIST');

  $return = "<form action=\"content.php?SITE=$typeform&STAP=3&BID=$bedrijfsid\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Members</h2>
              <br/>
              Select the members of the Board of Managers who resigned or were dismissed
              <table class=\"oft_tabel\">";

  $query = $pdo->prepare('SELECT ID, VOORNAAM, TUSSENVOEGSEL, ACHTERNAAM, ROL
                  FROM personeel
                 WHERE (NOT personeel.DELETED = "Ja" OR personeel.DELETED is null)
                 ORDER BY personeel.ACHTERNAAM;');
  $query->execute();

  foreach ($query->fetchAll() as $dMembers) {
    $checked = '';
    if(count(explode(($dMembers["ID"]*1), $MEMBERLIST)) > 1) {
      $checked = "checked=\"true\"";
    }

    if(trim($dMembers["VOORNAAM"]." " . $dMembers["TUSSENVOEGSEL"]." " . $dMembers["ACHTERNAAM"]) != '') {
      $return .= "<tr id=\"ID".($dMembers["ID"]*1)."\">
                    <td width=\"300\"><input type=\"checkbox\" $checked id=\"MEM".($dMembers["ID"]*1)."\" name=\"MEM".($dMembers["ID"]*1)."\" value=\"Ja\" /> " . lb($dMembers["VOORNAAM"])." " . lb($dMembers["TUSSENVOEGSEL"])." " . lb($dMembers["ACHTERNAAM"])."</td>
                    <td width=\"50\">".$dMembers["ROL"]."</td>
                   </tr>";
    }
  }
  $return   .= "</table>";
  
  //New members
  $return .= "<br/>Appoint new members of the Executive Board<br/>";

  $NEWMEMBERLIST = getWizzardValue($typeform, $bedrijfsid, 'NEWMEMBERLIST');

  if($NEWMEMBERLIST != '') {
    //TODO : hoe kunnen we dit safe maken?
    $NEWMEMBERLIST = substr($NEWMEMBERLIST, 1, -1);
    $NEWMEMBERLIST = "ID = '".str_replace(";;", "' OR ID = '", $NEWMEMBERLIST)."'";

    $query = $pdo->prepare('SELECT ID
                     FROM personeel
                    WHERE ' . $NEWMEMBERLIST . ';');
    $query->execute();

    foreach ($query->fetchAll() as $dPersoneel) {
      //TODO add remove knop
      $return .= "<br/>".getPersoneelsLidNaam($dPersoneel["ID"]);
    }
  }

  $return .= "<br/><br/><table class=\"oft_tabel\">";
  //Add members
  $personeelsleden = selectbox_tabel("personeel", "ID", "INITIALEN, TUSSENVOEGSEL, ACHTERNAAM", '' , true, "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");

  $teller = 0;
  while($teller <= 10) {
    $display = "style=\"display: none;\"";

    $return .= "<tr id=\"inviteerow$teller\" $display>
                       <td>New member $teller</td>
                       <td><select name=\"NEWMEMBER_$teller\" class=\"field\">$personeelsleden</select></td>
                     </tr>";
    $teller++;
  }

  $return .= "</table>
                   <div id=\"invitees\" style=\"display: none;\">0</div><a id=\"addinvitee\" class=\"cursor\" onclick=\"document.getElementById('invitees').innerHTML=((document.getElementById('invitees').innerHTML*1)+1); toggle('inviteerow'+document.getElementById('invitees').innerHTML);if(document.getElementById('invitees').innerHTML >= $teller) { toggle('addinvitee'); }\">Add member</a>";


  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_BACK\" value=\"Back\" class=\"button\" /><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Next\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;

}

function oft_wizzard_board_change_documents($userid, $bedrijfsid, $typeform) {
  global $pdo;

  if(isset($_REQUEST["WZ_OPSLAAN"])) {

    //Loop entity list
    $ENTITYLIST = getWizzardValue($typeform, $bedrijfsid, 'ENTITYLIST');
    if ($ENTITYLIST) {
      $ent_list = str_replace(";;", ";", $ENTITYLIST);
    }
    else { $ent_list = array($bedrijfsid); }

    //set wizzard row 'final';
    $query = $pdo->prepare('update oft_wizzard
                 set STATUSFORM = "final"
               WHERE BEDRIJFSID IN ("' . implode(",",$ent_list) . '")
                 AND TYPEFORM = :typeform
                 AND NOT STATUSFORM = "final";');
    $query->bindValue('typeform', $typeform);
    $query->execute();

    //Redirect to entity documents
    rd("content.php?SITE=oft_document");
  } elseif(isset($_REQUEST["WZ_BACK"])) {
    rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=3");
  }

  // velden om in template te vervangen
  $fields['date of resolution'] = oft_document_date_format(getWizzardValue($typeform, $bedrijfsid, 'DATESHAREHOLDERSRESOLUTION'));
  $fields['article'] = getWizzardValue($typeform, $bedrijfsid, 'ARTICLE');
  $fields['financial year'] = getWizzardValue($typeform, $bedrijfsid, 'YEAR');

  // who's out
  $discharged = "";
  $nolongerinboard = getWizzardValue($typeform, $bedrijfsid, 'MEMBERLIST');
  if ($nolongerinboard && strstr($nolongerinboard, ";") ) {
    $nolongerinboard = str_replace(";;",";",explode(";", $nolongerinboard));

    foreach ($nolongerinboard as $member) {
      if ((int)$member > 0) {
        $queryPersoneel = $pdo->prepare('SELECT * FROM personeel WHERE ID = :id LIMIT 1');
        $queryPersoneel->bindValue('id', $member);
        $queryPersoneel->execute();
        $result = $queryPersoneel->fetch(PDO::FETCH_ASSOC);
        
        if (strtolower($result['MANVROUW']) == "man") {
          $title = "Mr.";
          $hisher = "his";
        } elseif (strtolower($result['MANVROUW']) == "vrouw") {
          $title = "Mrs.";
          $hisher = "her";
        } else {
          $title = "Mr./Mrs.";
          $hisher = "his/her/its";
        }
        $membername = "<strong>" . $title . " " . ($result['VOORNAAM'] ? $result['VOORNAAM'] : $result['INITIALEN']) . " " . $result['TUSSENVOEGSEL'] . " " . $result['ACHTERNAAM'];
        if ($discharged) $discharged .= "</strong>, and ";
        $discharged .= $membername . "</strong> as per [**], 20[**] and to grant full discharge for ".$hisher." conduct as managing director of the Company up to the date of ".$hisher." discharge";
      }
    }
  }
  else $discharged = " -none- ";

  $fields['dismissed'] = str_replace("discharge", "dismissal", $discharged);
  $fields['resigned'] = str_replace("discharge", "resignation", $discharged);

  // who's in
  $newmember = "";
  $newinboard = getWizzardValue($typeform, $bedrijfsid, 'NEWMEMBERLIST');
  if ($newinboard && strstr($newinboard, ";") ) {

    $newinboard = explode(";", str_replace(";;",";",$newinboard));

    foreach ($newinboard as $key=>$member) {
      if (is_numeric($member) && $member > 0) {

        $queryPersoneel = $pdo->prepare('SELECT * FROM personeel WHERE ID = :id;');
        $queryPersoneel->bindValue('id', $member);
        $queryPersoneel->execute();
        $result = $queryPersoneel->fetch(PDO::FETCH_ASSOC);
        
        if (strtolower($result['MANVROUW']) == "man") {
          $title = "Mr.";
        } elseif (strtolower($result['MANVROUW']) == "vrouw") {
          $title = "Mrs.";
        } else {
          $title = "Mr./Mrs.";
        }
        $membername = "<strong>" . $title . " " . ($result['VOORNAAM'] ? $result['VOORNAAM'] : $result['INITIALEN'])  . " " . $result['TUSSENVOEGSEL'] . " " . $result['ACHTERNAAM'];
        $verjaardag = $result['VERJAARDAG'] && $result['VERJAARDAG'] != "0000-00-00" ? $result['VERJAARDAG'] : "-unknown-";
        $residence = $result['PLAATS'] . ", " .$result['LAND'];
        if ($newmember) $newmember .= "</strong>, and ";
        $newmember .= $membername . "</strong>, born on ".$verjaardag." and residing at ".$residence." as a new managing director of the Company as per [**], 20[**]";
      }
    }
  }
  else $newmember = " -none- ";
  $fields['appointed'] = $newmember;

  // document opbouwen uit template files
  $template_doc = array("shareholders_resolution_header.txt", "shareholders_resolution_content_board_change.txt", "resolution_signature_page.txt");
  oft_save_wizzard_documents($userid, $bedrijfsid, $template_doc, $typeform, "SAVEDOC1", $fields);

  // maak voor andere entities aan
  $ENTITYLIST = getWizzardValue($typeform, $bedrijfsid, 'ENTITYLIST');
  $ent_list = str_replace(";;", ";", explode(';', $ENTITYLIST));
  foreach($ent_list As $k => $entid) {
    if(($entid*1) != 0) {
      // clone row to entity
      if($entid != $bedrijfsid) oft_wizzard_copy_row_to_entity($bedrijfsid, $typeform, $entid);
      //oft_copy_wizzard_documents($userid, $bedrijfsid, $typeform, $kolom, $filename, $templateFile = '');
    }
  }

  //set wizzard row 'final';
  $query = $pdo->prepare('update oft_wizzard
                 set STATUSFORM = "final"
               WHERE BEDRIJFSID = :bedrijfsid
                 AND TYPEFORM = :typeform
                 AND NOT STATUSFORM = "final";');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->bindValue('typeform', $typeform);
  $query->execute();

  $return = "<form action=\"content.php?SITE=$typeform&BID=$bedrijfsid&STAP=4\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Document</h2><br/>";
  $return .= "<table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"5\">";
  
  //Loop entity list
  foreach($ent_list As $k => $entid) { 
    if(($entid*1) != 0) {
      $SAVEDOC = "Shareholders Resulution Board Change (".stripslashes(getBedrijfsnaam($entid)).")";
      $return .= "<tr><td rowspan=\"2\"><img src=\"./images/docx.png\" /></td>
                  <td><a target=\"_blank\" href=\"document.php?WIZZARD=".getWizzardValue($typeform, $entid, 'ID', true)."&KOLOM=SAVEDOC1&NAME=".bestandsnaamcheck(str_replace(" ", "_", $SAVEDOC))."\">$SAVEDOC</a></td></tr>
              <tr><td><input type=\"checkbox\" name=\"SAVEDOC$entid\" checked=\"true\" value=\"Ja\" /> Save this document in the documents section of this entity</td></tr>";
    }
  }

  $return .= "<tr><td colspan=\"2\"><br/><br/>The signed documents have to be uploaden in the<br/>Resolution section of this entity.</td></tr>";
  $return .= "</table>";
  $return .= selectRadioOptions("SAVECHANGES", "Nee", "", array("Ja", "Nee")). "Save changed in Proxyholders?";
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_BACK\" value=\"Back\" class=\"button\" /><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Finish\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}

/**
 * Kopieer/clone de rij uit oft_wizzard naar nieuwe rij voor andere entity
 * Vervangt de bedrijfsnamen in de template text
 *
 * @param $bedrijfsid
 * @param $typeform
 * @param $entid
 * @param $setfinal
 */
function oft_wizzard_copy_row_to_entity($bedrijfsid, $typeform, $entid, $setfinal=false) {
  global $pdo;

  $query = $pdo->prepare('SELECT * FROM oft_wizzard
                      WHERE WHERE TYPEFORM = :typeform
                      AND BEDRIJFSID = :bedrijfsid LIMIT 1');
  $query->bindValue('typeform', $typeform);
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->execute();

  // kolommen ophalen
  foreach ($query->fetchAll() as $key => $val) {
    if (!is_numeric($key)) {
      $kolommen[$key] = $val;
    }
  }
  // kolommen aanpassen
  unset($kolommen['ID']);
  $updatestatement = "";
  $kolommen['BEDRIJFSID'] = $entid;
  if ($setfinal) $kolommen['STATUSFORM'] = "final";
  $findnaam = stripslashes(getBedrijfsnaam($result['BEDRIJFSID']));
  $replacenaam = stripslashes(getBedrijfsnaam($entid));
  if ($kolommen['SAVEDOC1']) $kolommen['SAVEDOC1'] = str_replace($findnaam, $replacenaam, $kolommen['SAVEDOC1']);
  if ($kolommen['SAVEDOC2']) $kolommen['SAVEDOC2'] = str_replace($findnaam, $replacenaam, $kolommen['SAVEDOC2']);
  foreach($kolommen as $kolomnaam => $value) {
    if ($kolomnaam != 'ID') {
      $db_fields[] = "`".$kolomnaam."`";
      $db_values[] = "'".ps($value)."'";
      if ($updatestatement) $updatestatement .= ", ";
      $updatestatement .= "`".$kolomnaam."` = '".ps($value)."'";
    }
  }

  // controleer of rij bestaat
  $resultcheck = db_fetch_array($sqlcheck);
  $queryCheck = $pdo->prepare('SELECT * FROM oft_wizzard
                        WHERE TYPEFORM = :typeform
                        AND BEDRIJFSID = :bedrijfsid
                        AND (STATUSFORM != "final" || UPDATEDATUM = "' . date("Y-m-d") . '")');
  $queryCheck->bindValue('typeform', $typeform);
  $queryCheck->bindValue('bedrijfsid', $entid);
  $queryCheck->execute();
  
  // rij bestaat: updaten
  if ($queryCheck->rowCount() > 0) {
    $updateid = $resultcheck["ID"];
    $query = $pdo->prepare('UPDATE oft_wizzard SET ".$updatestatement." WHERE ID = :id');
    $query->bindValue('id', $updateid);
    $query->execute();
  }
  // rij bestaat niet: invoeren
  elseif($db_fields) {
    $query = $pdo->prepare('INSERT INTO oft_wizzard (' . implode(', ', $db_fields) . ') VALUES(' .implode(', ', $db_values) . ')');
    $query->execute();
  }
}
