<?php
sercurityCheck();

function oft_home($user, $company)
{
    $title = 'Home';
    $submenuItems = '';
    $contentFrame = '';
    if (check('DIF', 'Nee', $company) == 'Ja') {
        $submenuItems .= "<a href=\"content.php?SITE=oft_agenda\">Agenda</a>";
        if (isMaster()) {
            $submenuItems .= "<a href=\"content.php?SITE=import_entities\">Import excel</a>";
        }
    } else {
        if (isRechtenOFT($company, "CRM")) {
            $submenuItems .= "<a href=\"content.php?SITE=oft_entities_add\">" . check("default_name_of_new_entity",
                    "New entity", 1) . "</a>";
        }
        if (isRechtenOFT($company, "CONTRACTEN")) {
            $submenuItems .= oft_inputform_link('content.php?SITE=oft_contracten_add', "New contract");
        }
        if (isRechtenOFT($company, "CRM")) {
            $submenuItems .= oft_inputform_link("content.php?SITE=oft_entitie_transactions_add",
                "New transaction");
        }
        if (isRechtenOFT($company, "CRM")) {
            $submenuItems .= oft_inputform_link('content.php?SITE=oft_document_add', "Upload documents");
        }
    }

    $fields = oft_get_structure($user, $company, 'notifications_overview', 'all');

    $addsql = " AND STATUS = 'Active' AND VERVALDATUM <= '" . date("Y-m-d", strtotime("+14 day")) . "'";

    if (isset($_REQUEST["NOTIFICATION_UPDATE"])) {
        check_annual_accounts($user, $company);
        oft_default_checks($user, $company);
        notificaties_bijwerken($user, $company);
        $contentFrame .= '<br /> executed notification_update';
    }

    if (isset($_GET['update_database']) || isset($_GET['database_update'])) {
        $database_updates = update_database();
        if (count($database_updates)) {
            $result_strings = '';
            foreach ($database_updates as $key => $value) {
                $result_strings .= '<br />Query ' . ++$key . ' result: ' . (int)$value . ' rows affected';
            }
            $contentFrame .= $result_strings;
        }
        $contentFrame .= '<br /> executed update_database';
    }


    $contentFrame .= oft_tabel($user, $company, $fields['arrayVelden'], $fields['arrayVeldnamen'],
        $fields['arrayVeldType'], $fields['arrayZoekvelden'], "notificaties", 'VERVALDATUM', "", "", "", $addsql);


    $contentFrame .= oft_document_legenda($user, $company);

    echo oft_framework_basic($user, $company, $contentFrame, $title, $submenuItems);
}

function oft_home_ajax($user, $company)
{
    $fields = oft_get_structure($user, $company, 'notifications_overview', 'all');
    $addsql = " AND STATUS = 'Active' AND VERVALDATUM <= '" . date("Y-m-d", strtotime("+14 day")) . "'";

    echo oft_tabel_content($user, $company, $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'],
        $fields['arrayZoekvelden'],
        "notificaties", 'VERVALDATUM', "", "", "", $addsql);
}

