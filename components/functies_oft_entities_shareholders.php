<?php
sercurityCheck();

function oft_entitie_shareholdings($userid, $bedrijfsid) {
  global $db;

  $titel         = oft_titel_entities($bedrijfsid);
  $back_button   = oft_back_button_entity();
  
  $MENUID = "";
  if(isset($_REQUEST["MENUID"])) {
    $MENUID = "&MENUID=".$_REQUEST["MENUID"];
  }

  $submenuitems  = '';
  /*
  $submenuitems  = '';
  if(isRechtenOFT($bedrijfsid, "CRM"))        {
    $submenuitems   .= oft_inputform_link('content.php?SITE=oft_document_add&BID='.$bedrijfsid.'&DOCTYPE=Shareholders register', "Upload document", 500) . ' | ';
  }

  $submenuitems .= "<a href=\"content.php?SITE=oft_entitie_participations$MENUID&BID=$bedrijfsid\">View participations</a>";
  if(isRechtenOFT($bedrijfsid, "CRM")) {
    $submenuitems .= ' | '. oft_inputform_link("content.php?SITE=oft_entitie_transactions_add&BID=$bedrijfsid", "Add participation");
    $submenuitems .= ' | '. "<a href=\"content.php?SITE=oft_entity_shareholders_edit$MENUID&BID=$bedrijfsid\">Add changes</a>";
  }
  */

  $contentFrame  = "<h1>Participations</h1>";

  $contentFrame .= "<table border=\"0\" cellspacing=\"1\" cellpadding=\"1\" width=\"100%\" class=\"sortable\">
                    <tr><th>(Ownership) ".check("default_name_of_entity", "Entity", 1)."</th><th>Number</th><th>Country</th></tr>";
  $bedrijfsonderdelen = bedrijfsonderdelen(($bedrijfsid*1), "&nbsp;&nbsp;&nbsp;&nbsp;", true, ";".($bedrijfsid*1));
  $contentFrame .= $bedrijfsonderdelen[0];
  $contentFrame .= "</table>";

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_shareholders($userid, $bedrijfsid) {
  global $db;

  $MENUID = "";
  if(isset($_REQUEST["MENUID"])) {
    $MENUID = "&MENUID=".$_REQUEST["MENUID"];
  }

  $submenuitems  = '';
  if(isRechtenOFT($bedrijfsid, "CRM"))        { 
    $submenuitems   .= oft_inputform_link('content.php?SITE=oft_document_add&BID='.$bedrijfsid.'&DOCTYPE=Shareholders register', "Upload document", 500) . ' | ';
  }

  $submenuitems .= "<a href=\"content.php?SITE=oft_entitie_transactions$MENUID&BID=$bedrijfsid\">View transactions</a>";
  if(isRechtenOFT($bedrijfsid, "CRM")) {
    $submenuitems .= ' | '. oft_inputform_link("content.php?SITE=oft_entitie_transactions_add&BID=$bedrijfsid", "Add shareholder");
    //$submenuitems .= ' | '. "<a href=\"content.php?SITE=oft_entity_shareholders_edit$MENUID&BID=$bedrijfsid\">Add changes</a>";
  }

  $extraContent  = oft_shareholder_warning($bedrijfsid, $userid);
  $extraContent .= "<br/><br/>";
  $extraContent .= oft_getDocuments($userid, $bedrijfsid, "Shareholders register", array("Shareholders register", "Deed of pledge"), "", "", "");

  echo oft_standaard_menu_page($userid, $bedrijfsid, "Shareholders", "oft_shareholders", 'oft_shareholders', "DATUM", oft_back_button_entity(), "Add shareholder", $extraContent, $submenuitems);
}

/**
 * Shareholders information
 *
 * @param $userid
 * @param $bedrijfsid
 */
function oft_entitie_shareholder_info($userid, $bedrijfsid) {
  global $pdo;

  // Shareholder info
  $shareholderid = (int)$_REQUEST['ID'];

  // Link transactie bewerken
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_shareholders_edit&BID=$bedrijfsid&ID=".$shareholderid);
  $bewerkScript = " class=\"cursor\" style=\"color: #417faa;text-decoration: underline;\""; //  onMouseOver=\"this.style.color='#ECEDED';\" onMouseOut=\"this.style.background='#FFFFFF';\"
  $bewerkScript .= " onClick=\"".$bewerken."\"";

  $MENUID        = getMenuItem();
  $titel         = oft_titel_entities($bedrijfsid);
  $submenuitems    = "";

  if(isRechtenOFT($bedrijfsid, "CRM")) {
    $submenuitems   = "<span $bewerkScript>Edit transaction</span>";
    $submenuitems  .= ' | '. "<a href=\"content.php?SITE=oft_entitie_shareholders&MENUID=$MENUID&ID=$bedrijfsid&BID=$bedrijfsid\">View shareholders</a>";
    $submenuitems  .= ' | '. "<a href=\"content.php?SITE=oft_entitie_transactions&MENUID=$MENUID&ID=$bedrijfsid&BID=$bedrijfsid\">View transactions</a>";
    $submenuitems .= ' | '. oft_inputform_link("content.php?SITE=oft_entitie_transactions_add&BID=$bedrijfsid", "Add shareholder");
  }
  $back_button   = oft_back_button_entity();

  $contentFrame = "<h2>Shareholder information</h2>";
  // get entity capital
  
  $queryQuantity = $pdo->prepare('SELECT ISSUEDQUANTITY FROM oft_classes WHERE BEDRIJFSID = :bedrijfsid;');
  $queryQuantity->bindValue('bedrijfsid', $bedrijfsid);
  $queryQuantity->execute();
  $dCapital = $queryQuantity->fetch(PDO::FETCH_ASSOC);

  if($dCapital !== false)
  {
    $capital = $dCapital["ISSUEDQUANTITY"];
  } else { $capital = 0; }

  // Get shareholder information
  $queryShareholder = $pdo->prepare('SELECT
                bedrijf.BEDRIJFSNAAM,
                bedrijf.BEDRIJFNR,
                oft_shareholders.*
              FROM oft_shareholders
              JOIN bedrijf on bedrijf.ID = oft_shareholders.ADMINID
              WHERE oft_shareholders.ID = :shareholderid;');
  $queryShareholder->bindValue('shareholderid', $shareholderid);
  $queryShareholder->execute();
  $dShareholder = $queryShareholder->fetch(PDO::FETCH_ASSOC);

  if($dShareholder !== false)
  {
    $contentFrame .= "<table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";

    $contentFrame .= "<tr>
                        <td class=\"oft_label\" valign=\"top\" align=\"left\">Number, Name</td>
                        <td valign=\"top\" align=\"left\" colspan=\"3\">".stripslashes($dShareholder["BEDRIJFNR"]).", ".stripslashes($dShareholder["BEDRIJFSNAAM"])."</td>
                      </tr>";
    $contentFrame .= "<tr>
                        <td class=\"oft_label\" valign=\"top\" align=\"left\">Transaction date</td>
                        <td valign=\"top\" align=\"left\" colspan=\"3\">" . datumconversiePresentatieKort($dShareholder["DATUM"]) . " </td>
                      </tr>";
    $contentFrame .= "<tr>
                        <td class=\"oft_label\" valign=\"top\" align=\"left\">Ownership</td>
                        <td valign=\"top\" align=\"left\" colspan=\"3\">" .  (isset($dShareholder["PERSENTAGE"]) ? $dShareholder["PERSENTAGE"] : '')  . "% </td>
                      </tr>";
    $contentFrame .= "<tr>
                        <td class=\"oft_label\" valign=\"top\" align=\"left\">Number of shares</td>
                        <td valign=\"top\" align=\"left\" colspan=\"3\">" . (isset($dShareholder["QUANTITY"]) ? $dShareholder["QUANTITY"] : '') . " <span style=\"display:none\">(of total <span id=\"total_amount_of_shares\">".$capital."</span>)</span></td>
                      </tr>";
    $contentFrame .= "<tr>
                        <td class=\"oft_label\" valign=\"top\" align=\"left\">Class of share</td>
                        <td valign=\"top\" align=\"left\" colspan=\"3\">" . (isset($dShareholder["CLASSTYPE"]) ? $dShareholder["CLASSTYPE"] : '') . " </td>
                      </tr>";
    $contentFrame .= "<tr>
                        <td class=\"oft_label\" valign=\"top\" align=\"left\">Deed</td>
                        <td valign=\"top\" align=\"left\" colspan=\"3\">" .  (isset($dShareholder["DEED"]) ? $dShareholder["DEED"] : '') . " </td>
                      </tr>";

    $contentFrame .= "</table><br/>";
    $contentFrame .= '';
  }

  // Pagina weergeven
  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_shareholders_ajax($userid, $bedrijfsid) {
  $tabel           = "oft_shareholders";
  $toevoegen       = "content.php?SITE=oft_entitie_transactions_add&BID=$bedrijfsid";
  $bewerken        = "";//oft_inputform_link_short("content.php?SITE=oft_entity_shareholders_edit&BID=$bedrijfsid&ID=-ID-");
  $verwijderen     = "";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'DATUM', $toevoegen, $bewerken, $verwijderen, "AND ADMINIDFROM = '".ps($bedrijfsid, "nr")."'");
}

function oft_shareholders_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_transactions.htm";
  $tabel = "oft_shareholders";

  $id = "";
  if(isset($_REQUEST["ID"])) { 
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Transaction</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_entity_shareholders_edit($userid, $bedrijfsid) {
  $MENUID          = getMenuItem();

  $titel           = oft_titel_entities($bedrijfsid);
  $submenuitems    = '';
  $back_button     = oft_back_button_entity();

  $tabel           = "oft_shareholders";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = "";
  $verwijderen     = "";

  if(isset($_REQUEST["CHANGE"])) {
    oft_tabel_content_save($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, "oft_shareholders", "PERSENTAGE");

    if ($_REQUEST["SITE"] == "oft_entities_add_5") {
      rd("content.php?SITE=oft_entitie_overview&BID=$bedrijfsid");
    } else {
      rd("content.php?SITE=oft_entitie_shareholders&MENUID=$MENUID&BID=$bedrijfsid");
    }
  }

  $contentFrame    = "<form action=\"\" method=\"POST\">";
  $contentFrame   .= "<h2>Shareholders</h2><br/><br/>";

  $contentFrame   .= oft_tabel_content_insert($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'CLASSTYPE', $toevoegen, $bewerken, $verwijderen, "AND ADMINIDFROM = '".ps($bedrijfsid, "nr")."'");

  $contentFrame   .= oft_shareholder_warning($bedrijfsid, $userid);
  $contentFrame   .= "<br/><br/>";
  $contentFrame   .= oft_getDocuments($userid, $bedrijfsid, "Shareholders register", array("Shareholders register", "Deed of pledge"), "", "", "");

  $contentFrame   .= "<br/><Br/><div align=\"right\">";

  $contentFrame   .= "<input type=\"submit\" name=\"CHANGE\" value=\"Save\" class=\"button\" />
                        <input type=\"button\" class=\"cancelbutton\" onClick=\"window.location = 'content.php?SITE=oft_entitie_shareholders&MENUID=$MENUID&BID=$bedrijfsid';\" value=\"Cancel\" name=\"ANNULEREN\" />
                      </div></form>";

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_transactions($userid, $bedrijfsid) {
  global $db;
       
  $MENUID = "";
  if(isset($_REQUEST["MENUID"])) {
    $MENUID = "&MENUID=".$_REQUEST["MENUID"];
  }

  $submenuitems = "<a href=\"content.php?SITE=oft_entitie_shareholders$MENUID&BID=$bedrijfsid\">View shareholders</a>";
  $submenuitems .= ' | '. "<a href=\"content.php?SITE=oft_all_transactions\">View all transactions</a>";

  if(isRechtenOFT($bedrijfsid, "CRM")) {
    $submenuitems .= ' | '. oft_inputform_link("content.php?SITE=oft_entitie_transactions_add&BID=$bedrijfsid", "Add shareholder");
  }  

  echo oft_standaard_menu_page($userid, $bedrijfsid, "Transactions", "oft_transactions", 'oft_transactions', "DATUM", oft_back_button_entity(), "Add shareholder", '', $submenuitems);
}

function oft_entitie_participations($userid, $bedrijfsid) {
  global $db;
       
  $MENUID = "";
  if(isset($_REQUEST["MENUID"])) {
    $MENUID = "&MENUID=".$_REQUEST["MENUID"];
  }

  $submenuitems = "<a href=\"content.php?SITE=oft_entitie_shareholdings$MENUID&BID=$bedrijfsid\">View participations</a>";
  $submenuitems .= ' | '. "<a href=\"content.php?SITE=oft_all_transactions\">View all transactions</a>";

  if(isRechtenOFT($bedrijfsid, "CRM")) {
    $submenuitems .= ' | '. oft_inputform_link("content.php?SITE=oft_entitie_transactions_add&BID=$bedrijfsid", "Add participation");
  }  

  echo oft_standaard_menu_page($userid, $bedrijfsid, "Transactions", "oft_participations", 'oft_shareholders', "DATUM", oft_back_button_entity(), "Add participation", '', $submenuitems);
}

function oft_all_transactions($userid, $bedrijfsid) {
  global $db;
       
  $titel           = tl('Transactions');
  $tabel           = "oft_transactions";
  $toevoegen       = "";
  if(isRechtenOFT($bedrijfsid, "CONTRACTEN")) {
    $toevoegen     = "content.php?SITE=oft_entitie_transactions_add&BID=$bedrijfsid";
  }
  $bewerken        = oft_inputform_link_short("content.php?SITE=".$tabel."_edit&ID=-ID-");
  $verwijderen     = "";
  $submenuitems    = oft_inputform_link($toevoegen, "New transaction");

  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_all_transactions_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_all_transactions_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_all_transactions_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_all_transactions_overzicht", "arrayZoekvelden");

  $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'DATUM', $toevoegen, $bewerken, $verwijderen);

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_entitie_transactions_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_transactions";
  $toevoegen       = "content.php?SITE=oft_".$tabel."_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=".$tabel."_edit&ID=-ID-");
  $verwijderen     = "";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'DATUM', $toevoegen, $bewerken, $verwijderen);
}

function oft_entitie_transactions_add($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_transactions_add.htm";
  $tabel = "oft_shareholders";

  echo "<h2>Add shareholder</h2>";

  echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_transactions_edit($userid, $bedrijfsid) {
  global $db;

  $templateFile = "./templates/oft_transactions_edit.htm";
  $tabel = "oft_transactions";

  $id = "";
  if(isset($_REQUEST["ID"])) {
    $id = $_REQUEST["ID"];
  }

  echo "<h2>Edit shareholder</h2>";

  echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}