<?php
sercurityCheck();
function create_new_entity($name, $userid)
{
    global $pdo;
    $newBid = 0;
    if ($name != "") {

        $query = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFSNAAM = :name LIMIT 1;');
        $query->bindValue('name', $name);
        $query->execute();
        $found = false;
        foreach ($query->fetchAll() as $data) {

            $newBid = $data ["ID"] * 1;
        }

        if (!$found) {

            // Maak nieuwe entiteit

            $newEntityQuery = $pdo->prepare('INSERT INTO bedrijf set UPDATEDATUM = :updatedatum, DELETED = "Nee", ENDREDEN = NULL, FIRSTBOOKYEAR = :firstbookyear, ENDOFBOOKYEAR = :endbookyear, ACTIVE = "Ja", INTERN = "Nee", 	MONITOR = "Nee", 	HAVECAPITAL = "Nee", 	DEFAULTDIR = "orangefield/home", 	BEDRIJFNR = "Extern", HANDELSNAAM = :handelsnaam, BEDRIJFSNAAM = :bedrijsnaam;');
            $updatedatum = date("Y-m-d");
            $firstbookyear = date("Y-m-d");
            $endbookyear = date("Y-12-31");
            $newEntityQuery->bindValue('updatedatum', $updatedatum);
            $newEntityQuery->bindValue('firstbookyear', $firstbookyear);
            $newEntityQuery->bindValue('endbookyear', $endbookyear);
            $newEntityQuery->bindValue('handelsnaam', $name);
            $newEntityQuery->bindValue('bedrijsnaam', $name);
            if ($newEntityQuery->execute()) {

                $newBid = $pdo->lastInsertId();
                $pdo->query('UPDATE bedrijf SET BEDRIJFSID = "' . ps($newBid, "nr") . '" WHERE ID = "' . ps($newBid,
                        "nr") . '" LIMIT 1;');
                $pdo->query('INSERT INTO oft_rechten_tot SET USERID = "' . ps($userid,
                        "nr") . '", BEDRIJFSID = "' . ps($newBid, "nr") . '";');
            }
        }
    }

    return $newBid;
}

function oft_menu_reports($userid, $bedrijfsid)
{
    $titel = tl('Reports');
    $contentFrame = '<ul>
 <li><a href="content.php?SITE=oft_annual_accounts">Annual accounts</a></li>
 <li><a href="content.php?SITE=oft_clients">(OFG) Black list clients</a></li>
 <li><a href="content.php?SITE=oft_compliance_chart">Risk Assessment</a></li>
 <li><a href="content.php?SITE=oft_registers_exceptions">Exceptions</a></li>
 <li><a href="content.php?SITE=oft_registers_view">Registers</a></li>
 <li><a href="content.php?SITE=oft_tax">Tax overview</a></li>
 <li><a href="content.php?SITE=oft_trainingsschema">Training</a></li>
 </ul>';
    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, '');
}

function replacejanee($txt)
{
    return str_replace("Nee", "No", str_replace("Ja", "Yes", $txt));
}

function oft_delete_item($userid, $bedrijfsid)
{
    global $pdo;
    if (isset ($_REQUEST ["ID"]) && isset ($_REQUEST ["tabel"])) {

        $tableData = getTableData($_REQUEST ["tabel"]);
        if (!empty ($tableData)) {

            auditaction($userid, $bedrijfsid, $_REQUEST ["tabel"], $_REQUEST ["ID"], 'Delete');
            $deleteQuery = $pdo->prepare('UPDATE ' . $_REQUEST ["tabel"] . ' set DELETED = "Ja" WHERE ID = :id LIMIT 1;');
            $deleteQuery->bindValue('id', $_REQUEST ["ID"]);
            if ($deleteQuery->execute()) {

                echo "ok";
            }
        }
    }
}

function create_new_useraccount($userid, $bedrijfsid)
{
    global $pdo;
    $newLoginId = 0;
    if ($userid > 0) {
        $selectLoginQuery = $pdo->prepare('SELECT ID FROM login WHERE PERSONEELSLID = :userid LIMIT 1;');
        $selectLoginQuery->bindValue('userid', $userid);
        $selectLoginQuery->execute();
        $found = false;
        foreach ($selectLoginQuery->fetchAll() as $data) {

            $newLoginId = $data ["ID"] * 1;
            $found = true;
        }

        if (!$found) {

            $nieuweCode = nieuweCodeGen();
            $username = getNaam("personeel", $userid, "ACHTERNAAM");
            if ($username == '') {

                $username = getNaam("personeel", $userid, "VOORNAAM");
            }

            if ($username == '') {
                $username = $nieuweCode;
            }

            $username = strtolower($username);
            // Maak nieuwe entiteit

            $newEntityQuery = $pdo->prepare('INSERT INTO login
 set UPDATEDATUM = :updatedatum, DELETED = "Nee", PERSONEELSLID = :personeelslid, BEDRIJFSID = :bedrijfsid, USERNAME = :username, PASSWORD = :password, HASH = :hash, PROFIEL = "Relationship manager", STYLE = "OFT", SUSPEND = "Nee", MASTER = "Nee", BESTANDEN = "Nee", OPDRACHTEN = "Nee", ADMINISTRATOR = "Nee", DIRECTIE = "Nee", IMPORTEXPORT = "Nee", CRM = "Nee", LEGAL = "Nee", COMPLIANCE = "Nee", BOARDPACK = "Nee", FINANCE = "Nee", HR = "Nee", CONTRACTS = "Nee";');
            $updatedatum = date("Y-m-d");
            $newEntityQuery->bindValue('updatedatum', $updatedatum);
            $newEntityQuery->bindValue('personeelslid', $userid);
            $newEntityQuery->bindValue('bedrijfsid', $bedrijfsid);
            $newEntityQuery->bindValue('username', $username);
            $newEntityQuery->bindValue('password', $nieuweCode);
            $newEntityQuery->bindValue('hash', $nieuweCode);
            if ($newEntityQuery->execute()) {

                $newLoginId = $pdo->lastInsertId();
                $insertRechtenQuery = $pdo->prepare('INSERT INTO oft_rechten_tot SET USERID = :userid, BEDRIJFSID = :bedrijfsid;');
                $insertRechtenQuery->bindValue('userid', $userid);
                $insertRechtenQuery->bindValue('bedrijfsid', $bedrijfsid);
                $insertRechtenQuery->execute();
            }
        }
    }

    return $newLoginId;
}

function isRechtenOFT($bedrijfsid, $RECHTEN, $typeRechten = "Ja")
{
    global $pdo;
    if ($RECHTEN === '') {
        return true;
    }

    $hasRight = false;
    $loginid = getLoginID();
    $query = $pdo->prepare("SELECT $RECHTEN, PERSONEELSLID, PROFIEL FROM login WHERE (ID = :loginid) LIMIT 1;");
    $query->bindValue('loginid', $loginid);
    $query->execute();
    foreach ($query->fetchAll() as $data) {

        if ($RECHTEN == 'MASTER') {

            if ($data ["PROFIEL"] == "Administrator") {

                $hasRight = true;
            }
        } elseif ($data ["PROFIEL"] == "Administrator") {

            $hasRight = true;
        } else {

            $RECHTEN_tot = replaceRechten($RECHTEN);
            if ($RECHTEN != "ALLEENLEESRECHTEN") {

                $sCheck = $pdo->prepare("SELECT * FROM oft_rechten_tot " .
                    "WHERE USERID = :userid AND " .
                    "BEDRIJFSID = :bedrijfsnummer LIMIT 1;");
                $sCheck->bindValue('userid', $data ["PERSONEELSLID"]);
                $sCheck->bindValue('bedrijfsnummer', $bedrijfsid);
                $sCheck->execute();
                if ($sCheck->rowCount() > 0) {
                    $dCheck = $sCheck->fetch(PDO::FETCH_ASSOC);
                    if (isset ($_REQUEST ["DEBUG"])) {
                        echo "<br/>" . $RECHTEN_tot . " - " . $dCheck [$RECHTEN_tot] . " - " . $typeRechten;
                    }

                    if (isset ($dCheck [$RECHTEN_tot]) && ($dCheck [$RECHTEN_tot] == "Ja" || $dCheck [$RECHTEN_tot] == $typeRechten)) {
                        $hasRight = true;
                    }
                }
            }

            if ($data [$RECHTEN] == "Ja") {

                $hasRight = true;
            } elseif ($data [$RECHTEN] == $typeRechten) {

                $hasRight = true;
            }
        }
    }

    if ($RECHTEN != "MASTER") {

        // Zijn er andere entiteiten waar

        $RECHTEN_tot = replaceRechten($RECHTEN);
        $userid = getMySelf();
        $oftRechtenQuery = $pdo->prepare("SELECT $RECHTEN_tot
 FROM oft_rechten_tot
 WHERE USERID = :userid
 AND BEDRIJFSID = :bedrijfsid LIMIT 1;");
        $oftRechtenQuery->bindValue('userid', $userid);
        $oftRechtenQuery->bindValue('bedrijfsid', $bedrijfsid);
        $oftRechtenQuery->execute();
        foreach ($query->fetchAll() as $dCheck) {

            if (isset ($dCheck [$RECHTEN_tot]) && $dCheck [$RECHTEN_tot] == "Ja") {

                $hasRight = true;
            } elseif (isset ($dCheck [$RECHTEN_tot]) && $dCheck [$RECHTEN_tot] == $typeRechten) {

                $hasRight = true;
            }
        }
    }

    return $hasRight;
}

function replaceRechten($rechten)
{
    $rechten = str_replace("CRM", "LEGAL", $rechten);
    $rechten = str_replace("CONTRACTEN", "CONTRACTS", $rechten);
    return $rechten;
}

function getNeededRightsOfSite($siteName)
{
// $RECHTEN = "";
    $masterRechtenArray = array(

        "oft_persons",
        "oft_entities_ended",
        "oft_persons_add",
        "oft_persons_edit",
        "oft_persons_ajax",
        "oft_persons_view",
        "oft_users_edit",
        "oft_users",
        "oft_login",
        "users_samenvoegen",
        "export_files_to_stick",
        "rapport_magaging_director",
        "oft_annual_accounts",
        "oft_annual_accounts_ajax",
        "oft_tax",
        "oft_tax_ajax",
        "extra_velden",
        "oft_stam_edit",
        "oft_stam_add",
        "oft_stam_ajax",
        "oft_mastertables",
        "oft_audit_trail",
        "oft_login_list",
        "oft_mena"

    );
    /*$entityRechtenArray = array(
    "oft_entitie_overview",
    "oft_entitie_overview_ajax",
    "oft_entitie_corporate_data",
    "oft_entitie_corporate_data_edit",
    "oft_entitie_adress",
    "oft_entitie_adress_add",
    "oft_entitie_adress_edit",
    "oft_entitie_capital",
    "oft_entitie_capital_ajax",
    "oft_entitie_capital_edit",
    "oft_entity_shareholders_edit",
    "oft_shareholders_edit",
    "oft_entitie_shareholdings",
    "oft_entitie_shareholders",
    "oft_entitie_shareholder_info",
    "oft_entitie_shareholders_ajax",
    "oft_entitie_transactions",
    "oft_all_transactions",
    "oft_entitie_participations",
    "oft_entitie_transactions_ajax",
    "oft_entitie_transactions_add",
    "oft_transactions_edit",
    "oft_entitie_management",
    "oft_entitie_management_ajax",
    "oft_entitie_management_add",
    "oft_entitie_management_add2",
    "oft_entitie_management_edit",
    "oft_entitie_management_edit2",
    "oft_entitie_proxyholders",
    "oft_proxyholders_add",
    "oft_proxyholders_edit",
    "oft_entitie_compliance",
    "oft_entitie_compliance_ajax",
    "oft_compliance_change_settings",
    "oft_entitie_documents",
    "oft_entitie_resolutions",
    "oft_entitie_resolutions_ajax",
    "oft_entitie_contracts",
    "oft_entitie_contracts_ajax",
    "oft_entities_ended",
    "oft_entities_ended_ajax",
    "oft_trainingsschema"
    );*/
    if (in_array($siteName, $masterRechtenArray)) {

        $RECHTEN = "MASTER";
    } else { // if(in_array($siteName, $entityRechtenArray)) {

        // TODO: Dit moeten we whitelisten ipv blacklisten

        $RECHTEN = "CRM";
    }

    return $RECHTEN;
}

function accessRightsPage($site, $bedrijfsid)
{
    $RECHTEN = getNeededRightsOfSite($site);
    if ($RECHTEN == "CRM" &&
        (substr($site, -4) != 'edit' || substr($site, -3) == 'add')) {
    } else {

        if ($RECHTEN != '' && $site != 'oft_entities' && $site != 'oft_home' && $site != 'oft_registers_view') {

            if (!isRechtenOFT($bedrijfsid, $RECHTEN)) {

                header("HTTP/1.0 404 Not Found");
                exit ();
            }
        }
    }
}

function siteToTable($site)
{
    if ($site == "oft_contracten") {
        $tabel = "contracten";
    } elseif ($site == "oft_persons") {
        $tabel = "personeel";
    } elseif ($site == "oft_users") {
        $tabel = "login";
    } elseif ($site == "oft_document") {
        $tabel = "documentbeheer";
    } elseif ($site == "oft_entitie_adress") {
        $tabel = "oft_bedrijf_adres";
    } elseif ($site == "oft_entitie_proxyholders") {
        $tabel = "oft_proxyholders";
    } elseif ($site == "oft_entities") {
        $tabel = "bedrijf";
    } elseif ($site == "oft_entities_add") {
        $tabel = "bedrijf";
    } else {
        $tabel = $site;
    }

    return $tabel;
}

function deleteDocument($userid, $bedrijfsid)
{
    global $pdo;
    if (isRechtenOFT($bedrijfsid, "CRM")) {

        if (isset ($_REQUEST ["ID"])) {

            auditaction($userid, $bedrijfsid, 'documentbeheer', $_REQUEST ["ID"], 'Delete');
            $query = $pdo->prepare('UPDATE documentbeheer set DELETED = "Ja" WHERE ID = :id LIMIT 1;');
            $query->bindValue('id', $_REQUEST ["ID"]);
            if ($query->execute()) {

                echo "Deleted";
            }
        }
    }
}

function printFaviconLinks()
{
    echo '<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
 <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
 <link rel="manifest" href="/favicon/manifest.json">
 <meta name="msapplication-TileColor" content="#ffffff">
 <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
 <meta name="theme-color" content="#ffffff">';
}

function deleteDocumentarchief($userid, $bedrijfsid)
{
    global $pdo;
    if (isRechtenOFT($bedrijfsid, "CRM")) {
        if (isset ($_REQUEST ["ID"])) {
            auditaction($userid, $bedrijfsid, 'documentbeheer_archief', $_REQUEST ["ID"], 'Delete');
            $query = $pdo->prepare('UPDATE documentbeheer_archief SET DELETED = "Ja" WHERE ID = :id LIMIT 1;');
            $query->bindValue('id', $_REQUEST ["ID"]);
            $query->execute();
        }
    }
}

function head_oft($userid, $bedrijfsid, $site)
{
    if (!isset ($_REQUEST["PDF"]) && !isset($_REQUEST["EXPORTEXCEL"])) {
        if (!isset ($_REQUEST ["PRINT"])) {
            headAlgemeen();
        } else {
            echo '<!DOCTYPE html><html lang="en"><head>';
        }

        if (isset ($_REQUEST ["SITE"]) && ($_REQUEST ["SITE"] == "oft_entitie_agenda" || $_REQUEST ["SITE"] == "oft_agenda")) {
            echo '<link rel="stylesheet" type="text/css" href="./scripts/fullcalendar-2.3.1/fullcalendar.css" />';
            echo '<script type="text/javascript" src="./scripts/fullcalendar-2.3.1/lib/moment.min.js"></script>';
            echo '<script type="text/javascript" src="./scripts/fullcalendar-2.3.1/fullcalendar-min.js"></script>';
        }

        echo '<link href="./style/oft_style.css" type="text/css" rel="stylesheet" />';
        echo '<link href="./style/oft2_style.css" type="text/css" rel="stylesheet" />';
        echo '<link href="style/calendar.css" type="text/css" rel="stylesheet" />';
        echo '<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />';
        echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>';
//        echo '<script src="./ckeditor4.3.1/ckeditor.js"></script>';
        echo '<script src="./scripts/oft2_scripts.js"></script>';
        echo dragAndDropScript();

        if (!isset ($_REQUEST ["PRINT"])) {
            echo oft_validatie_form($userid, $bedrijfsid, siteToTable($site));
        }

        echo("</head><body>");
        if ($site != "setViewMsg") {

            echo '<div id="achtergrond" onClick="oft2ClosePopup();" style="display:none;" ></div>';
        }
    } elseif (isset ($_REQUEST ["EXPORTEXCEL"])) {

        ob_start();
    }
}

function oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems)
{
    if (isset ($_REQUEST ["EXPORTEXCEL"])) {
        $content = $contentFrame;
    } else {
        if ($submenuitems == '') {
            $submenuitems = "<br/>";
        }

        oft_opslaan($userid, $bedrijfsid);
        oft_delete($userid, $bedrijfsid);
        $content = '<div class="oft2_main_menu">';
        $content .= '<div class="oft2_page_centering">';
        $content .= oft_menu($userid, $bedrijfsid);
        $content .= '</div>';
        $content .= '</div>';
        $content .= '<div class="oft2_second_menu">';
        $content .= '<div class="oft2_page_centering">';
        if (is_array($submenuitems)) {
            $submenuitems = implode("|", $submenuitems);
        }
        preg_match_all("#<a.*?</a>#im", $submenuitems, $matches);
        $content .= "<ul class=\"oft2_submenu_ul\">";
        if (isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "entity") {
            $content .= '<li><a href="/content.php?SITE=entity&MENUID=3&NO_RECALL_DATE=SHOW">No recall date filter</a></li>';
        }

//        if (isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "entity_addressbook") {
//            $content .= '<li><a href="/content.php?SITE=entity_addressbook&MENUID=5&ONLY_SHOW=ADVISORS">Only Advisors</a></li>';
//        }
//        if (isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "entity_addressbook") {
//            $content .= '<li><a href="/content.php?SITE=entity_addressbook&MENUID=5&ONLY_SHOW=TARGETS">Only Targets</a></li>';
//        }

        if (!empty($matches)) {
            foreach ($matches[0] as $key => $value) {
                $content .= "<li>" . $value . "</li>";
            }
        }

        $content .= '</ul>';
        $content .= '<h1 class="title">' . verkort(ucfirst($titel), 35) . '</h1>';
        $content .= '</div>';
        $content .= '</div>';
        $content .= '<div class="oft2_main_content">';
        $content .= '<div class="oft2_popup">';
        $content .= '<a class="oft2_popup_exit">close</a>';
        $content .= '<div id="invoerschermContent" class="oft_invoerschermContent"></div>';
        $content .= '</div>';
        $content .= '<div class="oft2_contentFrame">';
        $content .= $contentFrame;
        $content .= '</div>';
        $content .= '</div>';
    }
    return $content;
}

function oft_framework_menu(
    $userid,
    $bedrijfsid,
    $contentFrame,
    $titel,
    $submenuitems,
    $back_button,
    $tabel = '',
    $zoekveldenString = '',
    $toevoegen = '',
    $bewerken = ''
) {
    if ($submenuitems == '') {
        $submenuitems = '<br>';
    } else {
        $submenuitems .= '<br><br>';
    }

    if (isset ($_REQUEST ["EXPORTEXCEL"])) {

        $content = $contentFrame;
    } else {
        oft_opslaan($userid, $bedrijfsid);
        oft_delete($userid, $bedrijfsid);
        $buttons_nivo2 = buttons_oft_nivo2_buttons($userid, $bedrijfsid);
        $content = "";
        $content .= "<div class='oft2_main_menu'>";
        $content .= "<div class='oft2_page_centering'>";
        $content .= oft_menu($userid, $bedrijfsid);
        $content .= "</div>";
        $content .= "</div>";
        $content .= "<div class='oft2_second_menu'>";
        $content .= "<div class='oft2_page_centering'>";
        preg_match_all("#<a.*?</a>#im", $submenuitems, $matches);
        $content .= "<ul class='oft2_submenu_ul'>";
        if (!empty ($matches)) {

            foreach ($matches [0] as $key => $value) {

                $content .= "<li>" . $value . "</li>";
            }
        }

        $content .= "</ul>";
        if (strlen($titel) <= 30) {

            $fontsize = "";
            $verkort = 30;
        } elseif (strlen($titel) <= 50) {

            $fontsize = "style='font-size: 15pt;'";
            $verkort = 50;
        } elseif (strlen($titel) <= 75) {

            $fontsize = "style='font-size: 10pt;'";
            $verkort = 75;
        } elseif (strlen($titel) <= 100) {

            $fontsize = "style='font-size: 15pt;'";
            $verkort = 50;
        }

        $content .= "<h1 class='title' $fontsize>" . verkort(ucfirst($titel), $verkort) . "</h1>";
        $content .= "</div>";
        $content .= "</div>";
        $oft2_main_content = "oft2_main_content";
        if (isset ($_REQUEST ["SITE"]) &&
            ($_REQUEST ["SITE"] == 'oft_entitie_finance' ||
                $_REQUEST ["SITE"] == 'oft_entitie_hr' ||
                $_REQUEST ["SITE"] == 'oft_entitie_contracts' ||
                $_REQUEST ["SITE"] == 'oft_entitie_transactions' ||
                $_REQUEST ["SITE"] == 'oft_entitie_documents' ||
                $_REQUEST ["SITE"] == 'oft_persons_contracts' ||
                $_REQUEST ["SITE"] == 'oft_registers_incident' ||
                $_REQUEST ["SITE"] == 'oft_registers_claim' ||
                $_REQUEST ["SITE"] == 'oft_registers_actiontracking' ||
                $_REQUEST ["SITE"] == 'oft_registers_actiontracking_audit' ||
                $_REQUEST ["SITE"] == 'oft_registers_compliant' ||
                $_REQUEST ["SITE"] == 'oft_entitie_transactions')
        ) {

            $tabel = (!empty ($tabel) ? $tabel : "documentbeheer");
            $arrayVelden = oft_get_structure($userid, $bedrijfsid,
                (!empty ($zoekveldenString) ? $zoekveldenString : "oft_entitie_finance_overzicht"), "arrayVelden");
            $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid,
                (!empty ($zoekveldenString) ? $zoekveldenString : "oft_entitie_finance_overzicht"), "arrayVeldnamen");
            $arrayVeldType = oft_get_structure($userid, $bedrijfsid,
                (!empty ($zoekveldenString) ? $zoekveldenString : "oft_entitie_finance_overzicht"), "arrayVeldType");
            $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid,
                (!empty ($zoekveldenString) ? $zoekveldenString : "oft_entitie_finance_overzicht"), "arrayZoekvelden");
            $toevoegen = (!empty ($toevoegen) ? $toevoegen : "content.php?SITE=oft_document_add&SECTIE=Finance");
            $bewerken = (!empty ($bewerken) ? $bewerken : oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-",
                500));
            $verwijderen = "";
            $sql_toevoeging = "AND BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' " . getDoctypesBySection("Finance");
            $content .= "<div class='oft2_main_content'>";
            $content .= oft_tabel_zoekvelden("Finance", $userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen,
                $arrayVeldType, $arrayZoekvelden, $tabel);
            $content .= "<div class='oft2_popup'>";
            $content .= "<a class='oft2_popup_exit'>close</a>";
            $content .= "<div id=\"invoerschermContent\" class=\"oft_invoerschermContent\"></div>";
            $content .= "</div>";
            $content .= "</div>";
            $oft2_main_content = "oft2_main_content_no_top";
        }

        $content .= "<div class='oft2_page_centering'>";
        $content .= "<div class='$oft2_main_content'>";
        if ($oft2_main_content != "oft2_main_content_no_top") {

            $content .= "<div class='oft2_popup'>";
            $content .= "<a class='oft2_popup_exit'>close</a>";
            $content .= "<div id=\"invoerschermContent\" class=\"oft_invoerschermContent\"></div>";
            $content .= "</div>";
        }

        $content .= "<div class=\"oft2_contentFrame\">";
        $content .= "<div class=\"left\" style=\"margin-right: 120px;\">";
        if ($back_button != '') {

            $content .= "<div class=\"back_button\">" . $back_button . "</div>";
        }

        $content .= oft_menu_nivo2($userid, $bedrijfsid, $buttons_nivo2);
        $content .= "</div>";
        $content .= "<div class=\"oft_framework_frame_content\">";
        $content .= $contentFrame;
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
    }

    return $content;
}

/*
 *
 * Function to generate an url based on current query parameters.
 *
 *
 *
 * @return (string) The generated url query
 *
 */
function url_querystring()
{
    $BID = "";
    if (isset ($_REQUEST ["BID"])) {
        $BID = "&BID=" . $_REQUEST ["BID"];
    }

    $PID = "";
    if (isset ($_REQUEST ["PID"])) {
        $PID = "&PID=" . $_REQUEST ["PID"];
    }

    $MENUID = "";
    if (isset ($_REQUEST ["MENUID"])) {
        $MENUID = "&MENUID=" . $_REQUEST ["MENUID"];
    }

    $ID = "";
    if (isset ($_REQUEST ["ID"])) {
        $ID = "&ID=" . $_REQUEST ["ID"];
    }

    $TABEL = "";
    if (isset ($_REQUEST ["TABEL"])) {
        $TABEL = "&TABEL=" . $_REQUEST ["TABEL"];
    }

    $url_querystring = "content.php?SITE=" . $_REQUEST ["SITE"] . $BID . $PID . $MENUID . $ID . $TABEL;
    return $url_querystring;
}

function oft_inputform_link($url, $titel)
{
    if ($url == "") {
        return '';
    } else {
        return "<a class=\"cursor\" onClick=\"oft2OpenPopup();getPageContent2('$url', 'invoerschermContent', searchReq2);\">" . tl($titel) . "</a>";
    }
}

function oft_inputform_link_short($url)
{
    if ($url == '') {
        return '';
    }
    return "javascript:oft2OpenPopup();getPageContent2('$url', 'invoerschermContent', searchReq2);";
}

function oft_standaard_menu_page(
    $userid,
    $bedrijfsid,
    $subTitel,
    $structureTable,
    $tabel,
    $order,
    $back_button,
    $submenuName,
    $extraContent,
    $extraMenuItems = ''
) {
    $titel = oft_titel_entities($bedrijfsid);
    $toevoegen = "content.php?SITE=" . $structureTable . "_add&BID=$bedrijfsid";
    $bewerken = oft_inputform_link_short("content.php?SITE=" . $structureTable . "_edit&BID=$bedrijfsid&ID=-ID-");
    $verwijderen = "";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $structureTable . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $structureTable . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $structureTable . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $structureTable . "_overzicht", "arrayZoekvelden");
    $contentFrame = "<h1>$subTitel</h1>";
    // echo $structureTable;
    if ($structureTable == "oft_transactions") {
        $sql_toevoeging = "AND (ADMINIDFROM = '" . ps($bedrijfsid, "nr") . "' OR ADMINID = '" . ps($bedrijfsid,
                "nr") . "' OR BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "')";
    } elseif ($structureTable == "oft_participations") {
        $sql_toevoeging = "AND ADMINID = '" . ps($bedrijfsid, "nr") . "'";
    } elseif ($structureTable == "oft_shareholders") {
        $sql_toevoeging = "AND ADMINIDFROM = '" . ps($bedrijfsid, "nr") . "'";
    } elseif ($structureTable == "oft_entitie_finance") {
        $toevoegen .= "&SECTIE=Finance";
        $sql_toevoeging = "AND BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' " . getDoctypesBySection("Finance");
    } elseif ($structureTable == "oft_entitie_hr") {
        $toevoegen .= "&SECTIE=HR";
        $sql_toevoeging = "AND BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' " . getDoctypesBySection("HR");
    } elseif ($structureTable == "oft_entitie_documents") {
        $toevoegen .= "&SECTIE=Legal";
        $sql_toevoeging = "AND BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' " . getDoctypesBySection("Legal");
    } else {
        $sql_toevoeging = "AND BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "'";
    }

    $submenuitems = "";
    if ($extraMenuItems != '') {

        $submenuitems = $extraMenuItems;
    } else {

        if (isRechtenOFT($bedrijfsid, "CRM")) {

            $submenuitems = oft_inputform_link($toevoegen, $submenuName) . $extraMenuItems;
        }
    }

    $contentFrame .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, $order, $toevoegen, $bewerken, $verwijderen, $sql_toevoeging);
    $contentFrame .= $extraContent;
    return oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_getDocuments($userid, $bedrijfsid, $h3, $DOCTYPE, $Did, $Year, $tabelId, $tabel = '')
{
    global $pdo;
    $sql_doctypes = "";
    if ($DOCTYPE != '') {
        foreach ($DOCTYPE as $key => $value) {
            $sql_doctypes .= " DOCTYPE like '" . ps($value) . "' OR ";
        }
    }

    if ($sql_doctypes != "") {
        $sql_doctypes = substr($sql_doctypes, 0, (strlen($sql_doctypes) - 3));
        $sql_doctypes = "AND ($sql_doctypes)";
    }

    if ($Year != "") {
        $sql_doctypes .= " AND JAAR = '" . ps($Year, "nr") . "' ";
    }

    $return = "";
    $pos = 0;
    if ($tabel != '') {
        $query = $pdo->prepare('SELECT ID, BESTANDTYPE, BESTANDSNAAM, BEDRIJFSID from documentbeheer where true AND TABEL = :tabel AND TABELID = :tabelid;');
        $query->bindValue('tabelid', ($tabelId * 1));
        $query->bindValue('tabel', $tabel);
    } elseif ($tabelId > 0) {
        $query = $pdo->prepare('SELECT ID, BESTANDTYPE, BESTANDSNAAM, BEDRIJFSID from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) ".$sql_doctypes." AND TABELID = :tabelid LIMIT 1;');
        $query->bindValue('tabelid', ($tabelId * 1));
    } elseif ($Did > 0) {
        $query = $pdo->prepare('SELECT ID, BESTANDTYPE, BESTANDSNAAM, BEDRIJFSID FROM documentbeheer WHERE ID = :documentid limit 1;');
        $query->bindValue('documentid', $Did);
    } else {
        $query = $pdo->prepare("SELECT ID, BESTANDTYPE, BESTANDSNAAM, BEDRIJFSID FROM documentbeheer WHERE (NOT DELETED = 'Ja' OR DELETED is null) " . $sql_doctypes . " AND NOT BESTANDSNAAM = '' AND BEDRIJFSID = :bedrijfsid ORDER BY DATUM;");
        $query->bindValue('bedrijfsid', $bedrijfsid);
    }

    $query->execute();
    foreach ($query->fetchAll() as $dArticle) {

        if (isRechtenOFT($dArticle ["BEDRIJFSID"], "CRM", "Ja")) {

            if ($pos == 0) {
                $pos = 1;
                $return .= "<table class=\"oft_tabel\" cellpadding=\"2\">";
            }

            $return .= "<tr>
 <td><a onClick=\"if(confirm('Are you sure you want to delete this document?')) { getPageContent2('content.php?SITE=deleteDocument&ID=" . ($dArticle ["ID"] * 1) . "', 'doc" . ($dArticle ["ID"] * 1) . "', searchReq2);}\"><img src=\"./images/verwijderen.gif\" border=\"0\" /></a></td>
 <td class=\"oft_label\"><img src=\"./images/" . getExtentionIcon($dArticle ["BESTANDTYPE"]) . "\" border=\"0\" /></td><td id=\"doc" . ($dArticle ["ID"] * 1) . "\"><a href=\"content.php?SITE=entity_getFileCloudVPS_ajax&ID=" . ($dArticle ["ID"] * 1) . "\" target=\"_blank\">" . lb($dArticle ["BESTANDSNAAM"]) . "</a></td>
 </tr>";
        }
    }

    if ($pos == 1) {
        $return .= "</table>";
    } else {
        $return .= "<em>No documents found</em>";
    }
    if ($h3 != "") {
        $h3 = "<h3>$h3</h3>";
    }

    return $h3 . $return;
}

function getMenuItem()
{
    global $pdo;
    $MENUID = "";
    if (isset ($_REQUEST ["MENUID"])) {
        $MENUID = $_REQUEST ["MENUID"];
    }

    if ($MENUID == '') {
        $query = $pdo->prepare('SELECT menu.ID FROM menu WHERE menu.EIGENNAAM = "Home" AND menu.LOGINID = "8" ORDER By (VOLGORDE*1) LIMIT 1;');
        $query->execute();
        foreach ($query->fetchAll() as $data) {
            $MENUID = ps($data ["ID"], "nr");
        }
    }

    return $MENUID;
}

function check_annual_accounts($userid, $bedrijfsid, $CHECKSPECENTITY = 0)
{
    // Deze functies haalt gigantisch veel queries op
    // ophalen all bedrijven                            (171)
    // ophalen bedrijf > rechtsvormen
    // ophalen bedrijf > rechtsvorm > jaar > annual account
    // update/insert bedrijf > rechtsvorm > jaar > annual account
    // ophalen bedrijf > rechtsvorm > jaar > tax reports
    // update/insert bedrijf > rechtsvorm > jaar > tax reports

    // als een bedrijf gemiddeld 5 jaar bestaat en zich aan 1 regel per land hoeft te houden en gemiddeld 3 bestanden inleveren, dan worden er 5130 queries los uitgevoerd.

//    return;
    global $pdo;
    $finJaar = date("Y");
    require_once "./inc/standardduedates.php";
    // SELECT ID, BEDRIJFSNAAM, BOARDMEETINGS, RECHTSVORM, FIRSTBOOKYEAR, ENDOFBOOKYEAR, TAXPERIODMONTH, TAXPERIODDAY, AGMPERIODMONTH, AGMPERIODDAY, ANAPERIODMONTH, ANAPERIODDAY from bedrijf where (NOT DELETED = "Ja" OR DELETED is null) AND MONITOR = "Ja" ORDER BY BEDRIJFSNAAM;

    $statement = 'SELECT ID, ENDDATUM, BEDRIJFSNAAM, BOARDMEETINGS, RECHTSVORM, FIRSTBOOKYEAR, ENDOFBOOKYEAR, TAXPERIODMONTH, TAXPERIODDAY, AGMPERIODMONTH, AGMPERIODDAY, ANAPERIODMONTH, ANAPERIODDAY from bedrijf where (NOT DELETED = "Ja" OR DELETED is null) AND MONITOR = "Ja"';
    if ($CHECKSPECENTITY) {
        $statement .= ' AND ID = :id LIMIT 1';
    } else {
        $statement .= ' ORDER BY BEDRIJFSNAAM';
    }

    $query = $pdo->prepare($statement);

    if ($CHECKSPECENTITY) {
        $query->bindValue('id', $CHECKSPECENTITY);
    }

    $query->execute();

    foreach ($query->fetchAll() as $dBedrijf) {
        $company_ended = (isset($dBedrijf['ENDDATUM']) && $dBedrijf['ENDDATUM'] && $dBedrijf['ENDDATUM'] <= date('Y-m-d'));
        // Dit kan veel beter worden gedaan door gewoon een simpele join te leggen en niet voor elk resultaat een aparte query uit te voeren.

        $rVquery = $pdo->prepare('SELECT NAAM FROM stam_rechtsvormen WHERE ID = :id LIMIT 1;');
        $rVquery->bindValue('id', $dBedrijf ["RECHTSVORM"]);
        $rVquery->execute();
        foreach ($rVquery->fetchAll() as $dataRV) {

            $FIRSTBOOKYEAR = $finJaar;
            if ($CHECKSPECENTITY > 0) {

                $FIRSTBOOKYEAR = substr($dBedrijf ["FIRSTBOOKYEAR"], 0, 4);
                if ($FIRSTBOOKYEAR < ($finJaar - 5)) {
                    $FIRSTBOOKYEAR = $finJaar - 3;
                }
            }

            for ($j = $FIRSTBOOKYEAR; $j <= $finJaar; $j++) {
                $checkQuery = $pdo->prepare('SELECT ID FROM oft_annual_accounts WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND FINANCIALYEAR = :financialyear LIMIT 1;');
                $checkQuery->bindValue('bedrijfsid', $dBedrijf ["ID"]);
                $checkQuery->bindValue('financialyear', $j);
                $checkQuery->execute();
                $rCheck = $checkQuery->fetch(PDO::FETCH_ASSOC);
                if (!$rCheck || $CHECKSPECENTITY > 0) {
                    $ENDOFBOOKYEAR = substr($dBedrijf ["ENDOFBOOKYEAR"], 5, 10);
                    if (strlen($ENDOFBOOKYEAR) != 5 || substr($ENDOFBOOKYEAR, 0, 2) == '01' || substr($ENDOFBOOKYEAR, 0,
                            2) == '00') {
                        $ENDOFBOOKYEAR = '12-31';
                    }

                    $maand = 0;
                    $dag = 0;
                    if ($dBedrijf ["ANAPERIODMONTH"] != '' || $dBedrijf ["ANAPERIODMONTH"] != '') {
                        $maand = $dBedrijf ["ANAPERIODMONTH"] * 1;
                        $dag = $dBedrijf ["ANAPERIODDAY"] * 1;
                    } else {
                        $standardDueDate = getStandardDueDates('47', $dataRV ["NAAM"]);
                        if (isset ($standardDueDate ["maand"])) {
                            $maand = $standardDueDate ["maand"] * 1;
                        }

                        if (isset ($standardDueDate ["dag"])) {
                            $dag = $standardDueDate ["dag"] * 1;
                        }
                    }

                    $plusDate = "";
                    if ($dag > 0) {
                        $plusDate .= "+$dag day";
                    }

                    if ($maand > 0) {
                        $plusDate .= "+$maand month";
                    }

                    // echo $dBedrijf["BEDRIJFSNAAM"] . ": ". $FIRSTBOOKYEAR . " - ". $finJaar .": plusDate - $plusDate"; exit;
                    if ($plusDate != '') { // Datum is tot dus -1 dag

                        $standardDueDate = date("Y-m-d", strtotime($plusDate, strtotime("$j-$ENDOFBOOKYEAR")));
                        if ($rCheck) { // update

                            // TODO: UNREACHABLE, what is the itent of this update function?

                            $query = $pdo->prepare('UPDATE oft_annual_accounts SET UPDATEDATUM = :updatedatum
 WHERE ID = :id;');
                            $query->bindValue('updatedatum', date("Y-m-d"));
//                            $query->bindValue('standardduedate', $standardDueDate);
                            $query->bindValue('id', $rCheck ["ID"]);
                            $query->execute();
                        } elseif (!$company_ended) { // Insert

                            $query = $pdo->prepare('INSERT INTO oft_annual_accounts SET BEDRIJFSID = :bedrijfsid, UPDATEDATUM = :updatedatum, DELETED = "Nee", FINANCIALYEAR = :financialyear, STANDARDDUEDATE = :standardduedate, EXTENTION = null, STATUS = "", FILE = "", AUDIT = "Nee"');
                            $query->bindValue('bedrijfsid', $dBedrijf ["ID"]);
                            $query->bindValue('updatedatum', date("Y-m-d"));
                            $query->bindValue('financialyear', $j);
                            $query->bindValue('standardduedate', $standardDueDate);
                            $query->execute();
                        }
                    }
                }

                $checkQuery = $pdo->prepare('SELECT ID FROM oft_tax WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND FINANCIALYEAR = :financialyear LIMIT 1;');
                $checkQuery->bindValue('bedrijfsid', $dBedrijf ["ID"]);
                $checkQuery->bindValue('financialyear', $j);
                $checkQuery->execute();
                $rCheck = $checkQuery->fetch(PDO::FETCH_ASSOC);
                if (!$rCheck || $CHECKSPECENTITY > 0) {
                    $ENDOFBOOKYEAR = substr($dBedrijf ["ENDOFBOOKYEAR"], 5, 10);
                    if (strlen($ENDOFBOOKYEAR) != 5 || substr($ENDOFBOOKYEAR, 0, 2) == '01' || substr($ENDOFBOOKYEAR, 0,
                            2) == '00') {
                        $ENDOFBOOKYEAR = '12-31';
                    }

                    $maand = 0;
                    $dag = 0;
                    if ($dBedrijf ["TAXPERIODMONTH"] != '' || $dBedrijf ["TAXPERIODMONTH"] != '') {
                        $maand = $dBedrijf ["TAXPERIODMONTH"] * 1;
                        $dag = $dBedrijf ["TAXPERIODDAY"] * 1;
                    } else {
                        $standardDueDate = getStandardDueDates('48', $dataRV ["NAAM"]);
                        if (isset ($standardDueDate ["maand"])) {
                            $maand = $standardDueDate ["maand"] * 1;
                        }

                        if (isset ($standardDueDate ["dag"])) {
                            $dag = $standardDueDate ["dag"] * 1;
                        }
                    }

                    $plusDate = "";
                    if ($dag > 0) {
                        $plusDate .= "+$dag day";
                    }

                    if ($maand > 0) {
                        $plusDate .= "+$maand month";
                    }

                    if ($plusDate != '') { // Datum is tot dus -1 dag
                        $standardDueDate = date("Y-m-d", strtotime($plusDate, strtotime("$j-$ENDOFBOOKYEAR")));
                        if ($rCheck) { // update
                            $query = $pdo->prepare('UPDATE oft_tax SET UPDATEDATUM = :updatedatum
 WHERE ID = :id;');
                            $query->bindValue('updatedatum', date("Y-m-d"));
//                            $query->bindValue('standardduedate', $standardDueDate);
                            $query->bindValue('id', $rCheck ["ID"]);
                            $query->execute();
                        } elseif (!$company_ended) { // Insert

                            $query = $pdo->prepare('INSERT INTO oft_tax SET BEDRIJFSID = :bedrijfsid, UPDATEDATUM = :updatedatum, DELETED = "Nee", FINANCIALYEAR = :financialyear, STANDARDDUEDATE = :standardduedate, EXTENTION = null, STATUS = "", FILE = ""');
                            $query->bindValue('bedrijfsid', $dBedrijf ["ID"]);
                            $query->bindValue('updatedatum', date("Y-m-d"));
                            $query->bindValue('financialyear', $j);
                            $query->bindValue('standardduedate', $standardDueDate);
                            $query->execute();
                        }
                    }
                }

                if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
                    continue;
                }
                $aantalMeetings = ($dBedrijf ["BOARDMEETINGS"] * 1);
                if ($aantalMeetings <= 0) {
                    continue;
                }

                $query = $pdo->prepare('SELECT ID FROM oft_boardmeetings WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND FINANCIALYEAR = :financialyear limit 1;');
                $query->bindValue('bedrijfsid', $dBedrijf ["ID"]);
                $query->bindValue('financialyear', $j);
                $query->execute();
                $rCheck = $query->fetch(PDO::FETCH_ASSOC);

                if ($rCheck || $company_ended) {
                    continue;
                }

                for ($m = 1; $m <= 12; $m += (12 / $aantalMeetings)) {
                    $month = sprintf('%02d', round($m));
                    $datum = "$j-$month-01";
                    $query = $pdo->prepare('INSERT INTO oft_boardmeetings SET BEDRIJFSID = :bedrijfsid, UPDATEDATUM = :updatedatum, DELETED = "Nee", FINANCIALYEAR = :financialyear, STANDARDDUEDATE = :standardduedate, DATUM = "", STATUS = "";');
                    $query->bindValue('bedrijfsid', $dBedrijf ["ID"]);
                    $query->bindValue('updatedatum', date("Y-m-d"));
                    $query->bindValue('financialyear', $j);
                    $query->bindValue('standardduedate', $datum);
                    $query->execute();
                }
            }
        }
    }
}

function oft_add_page($user, $company, $templateFile, $table = '', $title, $echo = true)
{
    $return = '';
    if ($title) {
        $return .= "<h2>$title</h2>";
    }
    $return .= replace_template_content($user, $company, $table, $templateFile);
    if ($echo) {
        echo $return;
    }
    return $return;
}

function oft_edit_page($user, $company, $templateFile, $table, $title = false, $customValues = array(), $echo = true)
{
    $id = "";
    $return = "";

    if (isset ($_REQUEST ["ID"])) {
        $id = $_REQUEST ["ID"];
    }

    $userCompany = getNaam($table, $id, "BEDRIJFSID");

    if (!isRechtenOFT($userCompany, "LEGAL")) {
        if ($title) {
            $return .= "<h2>$title</h2>";
        }
        $return .= "You don't have access to this page.";
    } else {

        if ($title) {
            $return .= "<h2>$title</h2>";
        } // / $id-$tabel:$editBedrijfsId

        $return .= replace_template_content_edit($user, $company, $table, $templateFile, $id, $customValues);
    }

    if ($echo) {
        echo $return;
    }
    return $return;
}

function oft_default_checks($userid, $bedrijfsid)
{
    global $pdo, $db;
    echo "x";
    /*
    $rCheckQuery = $pdo->prepare('SELECT ID FROM oft_rechten_tot LIMIT 1;');
    $rCheckQuery->execute();
    $rCheck = $rCheckQuery->fetch(PDO::FETCH_ASSOC);
    if (!$rCheck) {
    $userQuery = $pdo->prepare('SELECT PERSONEELSLID FROM login WHERE NOT PERSONEELSLID = "" GROUP by PERSONEELSLID;');
    $userQuery->execute();

    foreach ($userQuery->fetchAll() as $dUser) {
    $companyQuery = $pdo->prepare('select ID from bedrijf;');
    $companyQuery->execute();

    foreach ($companyQuery->fetchAll() as $dBedrijf) {
    $insertQuery = $pdo->prepare('INSERT INTO oft_rechten_tot set USERID = :userid, BEDRIJFSID = :bedrijfsid;');
    $insertQuery->bindValue('userid', $dUser["PERSONEELSLID"]);
    $insertQuery->bindValue('bedrijfsid', $dBedrijf["ID"]);
    $insertQuery->execute();
    }
    }
    }
    */

    // Verwijder dubbele rechten
    $queryRechten = $pdo->prepare('SELECT ID, USERID, BEDRIJFSID FROM oft_rechten_tot WHERE USERID = :userid GROUP BY BEDRIJFSID HAVING COUNT(BEDRIJFSID) > 1;');
    $queryRechten->bindValue('userid', $userid);
    $queryRechten->execute();
    foreach ($queryRechten->fetchAll() as $cRow) {
        db_query("delete from oft_rechten_tot
 WHERE USERID = '" . ps($cRow ["USERID"], "nr") . "'
 AND BEDRIJFSID = '" . ps($cRow ["BEDRIJFSID"], "nr") . "'
 AND NOT ID = '" . ps($cRow ["ID"], "nr") . "';", $db);
    }
}

function checkProfile($type)
{
    global $pdo;
    $value = null;
    $loginId = getLoginID();
    $query = $pdo->prepare('SELECT PROFIEL FROM login WHERE ID = :id limit 1;');
    $query->bindValue('id', $loginId);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);
    if ($data !== false) {

        $value = stripslashes($data ['PROFIEL']);
    }

    return $value != null && $value == $type;
}

function lb($str)
{
    if (!is_array($str)) {

        $return = htmlentities(trim(stripslashes($str)));
        $return = str_replace("Cura&amp;ccedil;ao", "Curacao", $return);
        $return = str_replace("&agrave;", "a", $return);
        $return = str_replace("&iacute;", "i", $return);
        $return = str_replace("&nbsp;", " ", $return);

        if (isset($_REQUEST["EXPORTEXCEL"])) {
            $return = strip_tags($return);

            $return = str_replace("&lt;br /&gt;", " ", $return);
            $return = str_replace("&lt;p&gt;", "", $return);
            $return = str_replace("&lt;/p&gt;", "", $return);
            $return = str_replace("&lt;", "lesser than ", $return);

            // temp fix -> replaces ü with u  (same for capital letters)
            $return = str_replace("&apos;", " ", $return);
            $return = str_replace("&uuml;", "u", $return);
            $return = str_replace("&Uuml;", "U", $return);
            $return = str_replace("&Auml;", "A", $return);
            $return = str_replace("&auml;", "a", $return);
            $return = str_replace("&Euml;", "E", $return);
            $return = str_replace("&euml;", "e", $return);
            $return = str_replace("&Ouml;", "O", $return);
            $return = str_replace("&ouml;", "o", $return);
            $return = str_replace("&eacute;", "e", $return);
            $return = str_replace("&Eacute;", "E", $return);
            $return = str_replace("&Egrave;", "E", $return);
            $return = str_replace("&egrave;", "e", $return);
            $return = str_replace("&Agrave;", "A", $return);
            $return = str_replace("&agrave;", "a", $return);
            $return = str_replace("&quot;", "`", $return);
            $return = str_replace("&rsquo;", "'", $return);
            $return = str_replace("&szlig;", "B", $return);

            $return = str_replace("&amp;apos;", " ", $return);
            $return = str_replace("&amp;uuml;", "u", $return);
            $return = str_replace("&amp;Uuml;", "U", $return);
            $return = str_replace("&amp;Auml;", "A", $return);
            $return = str_replace("&amp;auml;", "a", $return);
            $return = str_replace("&amp;Euml;", "E", $return);
            $return = str_replace("&amp;euml;", "e", $return);
            $return = str_replace("&amp;Ouml;", "O", $return);
            $return = str_replace("&amp;ouml;", "o", $return);
            $return = str_replace("&amp;eacute;", "e", $return);
            $return = str_replace("&amp;Eacute;", "E", $return);
            $return = str_replace("&amp;Egrave;", "E", $return);
            $return = str_replace("&amp;egrave;", "e", $return);
            $return = str_replace("&amp;Agrave;", "A", $return);
            $return = str_replace("&amp;agrave;", "a", $return);
            $return = str_replace("&amp;quot;", "`", $return);
            $return = str_replace("&amp;rsquo;", "'", $return);
            $return = str_replace("&amp;szlig;", "ss", $return);

            $return = str_replace("&amp;amp;", " and ", $return);
            $return = str_replace(" &amp;amp;", " and ", $return);
            $return = str_replace(" &amp;amp; ", " and ", $return);

            $return = str_replace(" &amp; ", " and ", $return);
            $return = str_replace("&amp;", "and", $return);
//            highlight_string($return); echo "--";
        }

        $return = str_replace("&amp;", "&", $return);
        return $return;
    }
}

function mail_versturen($Van, $aan, $Cc, $bcc, $onderwerp, $inhoud, $attachment)
{

    $send = false;
    if ($aan != '') {

        // Versturen met SMTP

        if (file_exists('./phpMailer/class.phpmailer.php')) {

            require_once './phpMailer/class.phpmailer.php';
        } elseif (file_exists('../phpMailer/class.phpmailer.php')) {

            require_once '../phpMailer/class.phpmailer.php';
        } elseif (file_exists('../../phpMailer/class.phpmailer.php')) {

            require_once '../../phpMailer/class.phpmailer.php';
        }

        $mail = new PHPMailer (true); // the true param means it will throw exceptions on errors, which we need to catch

        $mail->IsSMTP(); // telling the class to use SMTP

        try {

            $mail->Host = "mail.minded.nl"; // $dataInstellingen["SMTP"]; // SMTP server

            $mail->SMTPDebug = 0; // enables SMTP debug information (for testing)

            $mail->SMTPAuth = true; // enable SMTP authentication

            $mail->Host = "mail.minded.nl"; // $dataInstellingen["SMTP"]; // sets the SMTP server

            $mail->Port = 825; // set the SMTP port for the GMAIL server

            $mail->Username = "mindednl15"; // $dataInstellingen["GEBRUIKERSNAAM"]; // SMTP account username

            $mail->Password = "99sc22"; // $dataInstellingen["WACHTWOORD"]; // SMTP account password

            $mail->AddReplyTo($Van, $Van);
            // Alle Aan

            $AanArray = explode(",", $aan);
            for ($i = 0; $i < count($AanArray); $i++) {

                if ($AanArray [$i] != "" && $AanArray [$i] != " ") {
                    $mail->AddAddress(trim($AanArray [$i]), trim($AanArray [$i]));
                }
            }

            // Alle CC

            $CcArray = explode(",", $Cc);
            for ($i = 0; $i < count($CcArray); $i++) {

                if ($CcArray [$i] != "" && $CcArray [$i] != " ") {
                    $mail->AddCC(trim($CcArray [$i]), trim($CcArray [$i]));
                }
            }

            // Alle BCC

            $BCcArray = explode(",", $bcc);
            for ($i = 0; $i < count($BCcArray); $i++) {

                if ($BCcArray [$i] != "" && $BCcArray [$i] != " ") {
                    $mail->AddBCC(trim($BCcArray [$i]), trim($BCcArray [$i]));
                }
            }

            $mail->SetFrom($Van, $Van);
            $mail->AddReplyTo($Van, $Van);
            $mail->Subject = $onderwerp;
            // $mail->AltBody = $inhoud; // optional - MsgHTML will create an alternate automatically

            $mail->MsgHTML($inhoud);

            $attachments = explode(";;;", $attachment);

            for ($x = 0; $x < count($attachments); $x++) {

                $file_location = explode(";;", $attachments [$x]);
                if ($file_location [0] != "") {
                    $mail->AddAttachment($file_location [0]); // attachment
                }
            }

            if ($mail->Send()) {

                $send = true;
            }
        } catch (phpmailerException $e) {

            echo $e->errorMessage();
            // exit; //Pretty error messages from PHPMailer
        } catch (Exception $e) {

            echo $e->getMessage();
            // exit; //Boring error messages from anything else!
        }
    }

    return $send;
}

function getValue($tabel, $kolom, $isWaarde, $returnKolom, $toevSQL = '')
{
    global $pdo;
// $tableData = getTableData($tabel);
    $return = "";
    //if (!empty ($tableData)) {
    $statement = "SELECT " . ps($returnKolom) . " FROM " . ps($tabel) . " WHERE `" . ps($kolom) . "` = " . $isWaarde . " " . $toevSQL . " limit 1;";
    $query = $pdo->prepare($statement);
    $query->execute();
    foreach ($query->fetchAll() as $data) {

        if (isset ($data [$returnKolom])) {

            $return = $data [$returnKolom];
        }
    }
    //}
    return lb($return);
}

// NORMEC

function getResponsibleByRole()
{
    $return = "<option value='0'></option>";
    if(isset($_REQUEST['ROLE_ID'])) {
        $roleID = $_REQUEST['ROLE_ID'];
        global $pdo;
        $query = "SELECT ID, NAME FROM integration_checklist_users_role WHERE DELETED = 0 AND ROLE_ID = ".$roleID." ORDER BY `ORDER`";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll($pdo::FETCH_ASSOC);
        foreach($results as $person) {
            $return .= "<option value='".$person['ID']."'>".$person['NAME']."</option>";
        }

    }
    echo $return;
}

function getRoles($name, $selectedID)
{
    $return = "<select name='".$name."' class='form-control form-control-sm checklist_select'>";
    global $pdo;
    $query = "SELECT ID, NAME FROM integration_checklist_roles WHERE DELETED = 0 ORDER BY `ORDER`";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $results = $stmt->fetchAll($pdo::FETCH_ASSOC);

    foreach($results as $result) {
        $selected = '';
        if($result['ID'] == $selectedID) {
            $selected = 'selected';
        }
        $return .= "<option value='".$result['ID']."' ".$selected.">".$result['NAME']."</option>";
    }


    $return .= "</select>";

    return $return;
}

function getCompletedColor($id = 0, $return = '')
{
    global $pdo;
    if($id == 0 && isset($_REQUEST['STATUS_COLOR_ID'])) {
        $id = $_REQUEST['STATUS_COLOR_ID'];
    }
    $select = 'STATUS';
    if($return != '') {
        $select = $return;
    }
    elseif(isset($_REQUEST['RETURN_COLOR']) && $_REQUEST['RETURN_COLOR'] == 'YES') {
        $select = 'COLOR';
    }
    $query = "
        SELECT ".$select." FROM integration_checklist_completed
        JOIN integration_checklist ON integration_checklist.COMPLETED_ID = integration_checklist_completed.ID AND integration_checklist.ID = " . $id . "
    ";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $colorStatus = $stmt->fetchColumn(0);

    if(isset($_REQUEST['RETURN_COLOR']) && $_REQUEST['RETURN_COLOR'] == 'YES') {
        echo $query;
        exit;
    }

    return $colorStatus;
}

function getCompletedColorAjax()
{
    global $pdo;
    if(isset($_REQUEST['STATUS_COLOR_ID'])) {
        $id = $_REQUEST['STATUS_COLOR_ID'];
    }

    $query = "
        SELECT COLOR FROM integration_checklist_completed
        WHERE DELETED = 0 AND  ID = " . $id . "
    ";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $colorStatus = $stmt->fetchColumn(0);

    if(isset($_REQUEST['RETURN_COLOR']) && $_REQUEST['RETURN_COLOR'] == 'YES') {
        echo $colorStatus;
        exit;
    }
}

function getDataChecklist($rowID, $id, $value, $column = 'NAME', $searchWhere = '', $getSingleValue = false)
{
    global $pdo;
    $type = '';
    $whereClause = 'ID';
    $extraWhere = '';
    $deleted = ' AND DELETED = 0';
    $return = '-';
    $cssID = '';
    $name = 'FLD_' . $rowID . '_';

    $width = '150px';
    switch ($value) {
        case 'category':
            $table = 'integration_checklist_categories';
            break;
        case 'sub-category':
            $table = 'integration_checklist_sub_categories';
            break;
        case 'role':
            $type = 'select';
            $table = 'integration_checklist_roles';
            $deleted = 'DELETED = 0';
            $width = '100px';
            $name .= 'role';
            break;
        case 'user':
            $type = 'select';
            $table = 'integration_checklist_users_role';
            $deleted = 'DELETED = 0';
            $width = '200px';
            $name .= 'user';
            break;
        case 'entity':
            $table = 'entities';
            break;
        case 'duration':
            $type = 'select';
            $deleted = "DELETED = 0 ";
            $table = 'integration_checklist_duration';
            $name .= 'duration';
            if ($getSingleValue) {
                $type = '';
                $deleted = " AND DELETED = 0 ";
            }
            break;
        case 'closed_date':
            $table = 'entity_pipeline';
            $whereClause = 'ENTITY_ID';
            $extraWhere = ' AND PIPELINE_TYPE_ID = 10';
            $return = 'Not signed';
            break;
        case 'loi_signed_date':
            $table = 'entity_pipeline';
            $whereClause = 'ENTITY_ID';
            $extraWhere = ' AND PIPELINE_TYPE_ID = 9';
            $return = '1970-01-01';
            break;
        case 'revenue':
            $table = 'integration_checklist_revenue';
            $type = 'select';
            $deleted = 'DELETED = 0 ';
            $name .= 'revenue';
            break;
        case 'completed':
            $table = 'integration_checklist_completed';
            $type = 'select';
            $column = 'VALUE';
            $cssID = "completedSelect";
            $deleted = 'DELETED = 0 ';
            $name .= 'completed';
            break;
        case 'initiation':
            $type = 'select';
            $deleted = "DELETED = 0 ";
            $table = 'integration_checklist_initiation';
            $name .= 'initiation';
            if ($getSingleValue) {
                $type = '';
                $deleted = " AND DELETED = 0 ";
            }
            break;
        default:
            return $return;
    }

    if ($type == 'select') {
        $return = '<select name="' . $name . '" class="form-control form-control-sm checklist_select" style="max-width: ' . $width . ';" id="' . $cssID . '">';
        $query = "SELECT " . $column . ", " . $whereClause . " FROM " . $table . " WHERE " . $deleted . $searchWhere;
//        return $query;
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $options = $stmt->fetchAll($pdo::FETCH_ASSOC);
        foreach ($options as $option) {
            $selected = $option[$whereClause] == $id ? 'selected' : '';
            $return .= '<option value="' . $option[$whereClause] . '" ' . $selected . '>' . $option[$column] . '</option>';
        }
        $return .= '</select>';
        return $return;
    }

    $query = "SELECT " . $column . " FROM " . $table . " WHERE " . $whereClause . " = " . $id . " " . $extraWhere . $deleted;
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    if ($stmt->rowCount()) {
        $value = $stmt->fetchAll($pdo::FETCH_COLUMN)[0];
        if (empty($value)) {
            return $return;
        }
        return $value;
    }
    return $return;
}

function getCompletedByCompanyID($entityID, $subID, $field) {
    global $pdo;
    $query = "SELECT integration_checklist_completed.VALUE, integration_checklist_completed.COLOR
                FROM integration_checklist_completed
                JOIN integration_checklist ON integration_checklist.COMPLETED_ID = integration_checklist_completed.ID
                JOIN integration_checklist_sub_categories ON integration_checklist_sub_categories.ID = integration_checklist.SUB_CATEGORY_ID
                WHERE integration_checklist_sub_categories.ID = ".$subID."
                    AND integration_checklist.ENTITY_ID = ".$entityID;
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll($pdo::FETCH_ASSOC);
    if(isset($result[0])) {
        return $result[0][strtoupper($field)];
    }

    return $result;
}

function checklistGetStartdate() {
    $id = isset($_REQUEST['ID'])? $_GET['ID'] : 0;
    $initiation = isset($_REQUEST['INITIATION_ID'])? $_REQUEST['INITIATION_ID'] : 0;

    $startDate = '-';

    if (getDataChecklist($id, $initiation, 'initiation', 'QUALITY', '',
            true) != '-') {
        $startDate = strtotime(getDataChecklist($id, $_REQUEST['COMPANY_ID'],
            'closed_date',
            'CREATED_AT',
            '', true));
        $quantity_ini = getDataChecklist($id, $initiation,
            'initiation',
            'QUANTITY', '', true);
        $quality_ini = getDataChecklist($id, $initiation, 'initiation',
            'QUALITY',
            '', true);
        $initionCalc = $quantity_ini . " " . $quality_ini;
        $startDate = date('j-M-y', strtotime($initionCalc, $startDate));
    } elseif (getDataChecklist($id, $initiation, 'initiation',
            'QUANTITY', '',
            true) == 'closing') {
        $startDate = date('j-M-y',
            strtotime(getDataChecklist($id, $_REQUEST['COMPANY_ID'],
                'closed_date',
                'CREATED_AT', '',
                true)));
    } elseif (getDataChecklist($id, $initiation, 'initiation',
            'QUANTITY', '',
            true) == 'endyear') {
        $startDate = "31-dec-" . date('y',
                strtotime(getDataChecklist($id, $_REQUEST['COMPANY_ID'],
                    'closed_date',
                    'CREATED_AT')));
    } elseif (getDataChecklist($id, $initiation, 'initiation',
            'QUANTITY', '',
            true) == 'loisigned') {
        $startDate = date('j-M-y',
            strtotime(getDataChecklist($id, $_REQUEST['COMPANY_ID'],
                'loi_signed_date',
                'CREATED_AT')));
    }

    echo $startDate;
    exit;
}

function checklistGetDeadline() {
    $id = isset($_REQUEST['ID'])?$_REQUEST['ID']: 0;
    $duration = isset($_REQUEST['DURATION_ID'])?$_REQUEST['DURATION_ID']: 0;
    $startDate = isset($_REQUEST['STARTDATE'])?$_REQUEST['STARTDATE']: '01-Jan-1970';

    $deadline = '-';

    if (getDataChecklist($id, $duration, 'duration', 'QUANTITY', '',
            true) != '-') {
        $duration = "+" . getDataChecklist($id, $duration, 'duration',
                'QUANTITY', '', true) . " " . getDataChecklist($id,
                $duration,
                'duration',
                'QUALITY', '', true);
        $deadline = date('j-M-y', strtotime($duration, strtotime($startDate)));
    }

    echo $deadline;
    exit;
}

// NORMEC

/**
 *
 * Maakt een fileupload element
 *
 *
 *
 * @param string $veldNaam
 * Waarde voor het HTML 'name' atribuut
 *
 * @param string $class
 * Naam van de CSS klasse
 *
 *
 *
 * @return string HTML voor het fileupload veld
 *
 */
function fileVeld($veldNaam, $class)
{
    if (!isset ($_REQUEST ["EXPORTEXCEL"]) && !isset ($_REQUEST ["PDF"]) && !isset ($_REQUEST ["MAILHEADER"]) && !isset ($_REQUEST ["PRINTEN"])) {

        return "<div class=\"uploadVeld\">
 	<input type=\"file\" class=\"file hidden $class\" name=\"$veldNaam\" onchange='$(this).siblings()[0].value = this.value'>
 	<input class=\"nepVeld $class\">
 </div>";
    }
    return '';
}

function fileVeldNoClasses($veldNaam, $class)
{
    if (!isset ($_REQUEST ["EXPORTEXCEL"]) && !isset ($_REQUEST ["PDF"]) && !isset ($_REQUEST ["MAILHEADER"]) && !isset ($_REQUEST ["PRINTEN"])) {

        return "<div class=\"uploadVeld\">
 	<input type=\"file\" class=\"$class\" name=\"$veldNaam\" onchange='$(this).siblings()[0].value = this.value'>
 	<input class=\"nepVeld $class\">
 </div>";
    }
    return '';
}

function opslaanButton($naam, $value, $class = "btn-action", $extracode = '', $validatie = false)
{
    $return = "";
    if (!isset ($_REQUEST ["EXPORTEXCEL"]) && !isset ($_REQUEST ["PDF"]) && !isset ($_REQUEST ["MAILHEADER"]) && !isset ($_REQUEST ["PRINTEN"])) {

        if ($validatie == true) {

            $return = '<input type="submit" class="' . $class . '" name="' . $naam . '" value="' . tl($value) . ' onClick="' . $extracode . '" />';
        } else {

            $divId = uniqid();
            $return = "<div id=\"a" . $divId . "\" style=\"float: left;\"><input type=\"submit\" class=\"$class\" name=\"$naam\" value=\"" . tl($value) . "\" onClick=\"toggle('a$divId');toggle('b$divId');$extracode\" /></div>
 <div id=\"b" . $divId . "\" style=\"float: left;display: none;\"><img src=./images/bar.gif width=100 /></div>";
        }
    }

    return $return;
}

function contains($haystack, $needle) {
    if (strpos($haystack, $needle) !== false) {
        return true;
    }
    return false;
}

function getDuration($subCatId, $entityId) {
  global $pdo;

  $sql = "SELECT DURATION_ID FROM integration_checklist WHERE SUB_CATEGORY_ID = $subCatId AND ENTITY_ID = $entityId;";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll($pdo::FETCH_ASSOC);
  if(!isset($result[0])) {
      return 0;
  }
  return $result[0]['DURATION_ID'];
}

function getInitiation($subCatId, $entityId) {
  global $pdo;

  $sql = "SELECT INITIATION_ID FROM integration_checklist WHERE SUB_CATEGORY_ID = $subCatId AND ENTITY_ID = $entityId;";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll($pdo::FETCH_ASSOC);
    if(!isset($result[0])) {
        return 0;
    }
  return $result[0]['INITIATION_ID'];
}

function getChecklistId($subCatId, $entityId) {
  global $pdo;

  $sql = "SELECT ID FROM integration_checklist WHERE SUB_CATEGORY_ID = $subCatId AND ENTITY_ID = $entityId;";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll($pdo::FETCH_ASSOC);
    if(!isset($result[0])) {
        return 0;
    }
  return $result[0]['ID'];
}
