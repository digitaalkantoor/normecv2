<?php
sercurityCheck();

function oft_menu($userid, $bedrijfsid)
{
    global $pdo;

    $loginid = getLoginID();
    $userid = getUserID($loginid);
    $bedrijfsid = getBedrijfsID($loginid);
    $isKlant = "0";
    $hoofdmenuid = 0;
    if (isKlant()) {
        $isKlant = "1";
    }
    $isAccountent = "0";
    if (isAccountent()) {
        $isAccountent = "1";
    }

    $selectedId = getNivo1menubuttonId(getMenuItem(), getMenuItem(), getRootId(getLoginId()));

    $content = "";

    //logo
    $content .= "<a href=\"content.php?SITE=entity_home\">";
    $content .= "<img class='oft2_logo' src=\"./images/normec/logo-normec.png\" border=\"0\" style=\"height: 40px\" />";
    $content .= "</a>";

    //zoekveld boven
    $content .= "<div class='oft2_main_float_right'>";
    $content .= "<form class=\"oft2_searchform\" action=\"content.php?SITE=oft_search\" method=\"Post\">
      <input type=\"text\" name=\"SRCH_SEARCH\" placeholder=\"Search\" class='oft2_search_text'/>
      <input type='submit' class='oft2_search_submit' value=''/>
    </form>";

    // required style
    $content .= '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">';
    // add new target
    if($_SESSION['choice'] == 'm&a') {
        $content .= "<a href='content.php?SITE=entity_create'><i class='fas fa-plus' style='color: orange; font-size: 18px; padding-top: 10px;'></i></a>";
        $content .= "<a href='content.php?SITE=import_csv'><li class='fa fa-file-excel' style='color: orange; font-size: 18px; padding-top: 10px; padding-left: 15px;'></li></a>";
    }

//    $content .= "<a href='content.php?SITE=entity_integration_checklist'><i class=\"fas fa-handshake\" style='color: orange; font-size: 18px; padding-top: 10px; padding-left: 15px;'></i></a>";
    //style=\"width: 200px; background: url('./images/loop.png') no-repeat 182px 2px;\"

    //personalia
    $content .= "<ul class='oft2_user_menu'>";
    $content .= "<li class='has_submenu'> <span class='oft2_menu_label'>" . getPersoneelslidNaam($userid) . "</span>";
    $content .= "<ul>";
    $content .= "<li><a href='content.php?SITE=entity_editPassword'>Change password</a></li>";
    $content .= "<li><a href='content.php?SITE=uitloggen'>Log out</a></li>";
    $content .= "</ul></li></ul>";

    $content .= "</div>";

    //menuknoppen
    $content .= "<ul class='oft2_menu_left'>";

    $query = $pdo->prepare('SELECT * FROM login WHERE ID = :loginid;');
    $query->bindValue('loginid', $loginid);
    $query->execute();
    $dataLogin = $query->fetch(PDO::FETCH_ASSOC);

    if ($dataLogin !== false) {
        $menuitems = [
            'M&A Goal' => [
                'MENUID' => 9,
                'LINK' => 'content.php?SITE=entity_home2&VISUAL=start',
                'RECHTEN' => 'CRM'
            ],
            'Top Targets' => [
                'MENUID' => 6,
                'LINK' => 'content.php?SITE=entity_favorites',
                'RECHTEN' => 'CRM'
            ],
            'Metrics' => [
                'MENUID' => 11,
                'LINK' => 'content.php?SITE=entity_home2&VISUAL=metrics',
                'RECHTEN' => 'CRM'
            ],
            'Pipeline' => [
                'MENUID' => 10,
                'LINK' => 'content.php?SITE=entity_home2&VISUAL=pipeline',
                'RECHTEN' => 'CRM'
            ],
            // margin toevoegen
            'My Targets' => [
                'MENUID' => 2,
                'LINK' => 'content.php?SITE=entity_my_pipeline',
                'RECHTEN' => 'CRM',
                'STYLE' => 'margin-left: 60px;'
            ],
            'Filter' => [
                'MENUID' => 3,
                'LINK' => 'content.php?SITE=entity',
                'RECHTEN' => 'CRM'
            ],
            'Contacts Advisors' => [
                'MENUID' => 5,
                'LINK' => 'content.php?SITE=entity_addressbook&ONLY_SHOW=ADVISORS',
                'RECHTEN' => 'CRM'
            ],
            'Contacts Targets' => [
                'MENUID' => 7,
                'LINK' => 'content.php?SITE=entity_addressbook&ONLY_SHOW=TARGETS',
                'RECHTEN' => 'CRM'
            ],

//            'TEST' => [
//                'MENUID' => 10,
//                'LINK' => 'content.php?SITE=entity_addressbook',
//                'RECHTEN' => 'CRM'
//            ],
//            'New Target' => [
//                'MENUID' => 4,
//                'LINK' => 'content.php?SITE=entity_create',
//                'RECHTEN' => 'CRM'
//            ],
        ];

        if($_SESSION['choice'] != 'm&a') {
            $menuitems = [];
        }
//
//        //TODO: rechten toekennen
//        $menuitems["Home"] = array("MENUID" => 1, "LINK" => "content.php?SITE=oft_home", "RECHTEN" => "CRM");
//        $menuitems[check("default_name_of_entities", "Entities", 1)] = array(
//            "MENUID" => 2,
//            "LINK" => "content.php?SITE=oft_entities",
//            "RECHTEN" => "LEGAL"
//        );
//        if (check("hide_section_Legal", "Nee", 1) == "Nee") {
//            $menuitems["Legal"] = array("MENUID" => 3, "LINK" => "content.php?SITE=oft_document", "RECHTEN" => "LEGAL");
//        }
//        if (check("hide_section_Finance", "Nee", 1) == "Nee") {
//            $menuitems["Finance"] = array(
//                "MENUID" => 4,
//                "LINK" => "content.php?SITE=oft_finance",
//                "RECHTEN" => "FINANCE"
//            );
//        }
//            if (check("hide_section_Human Resources", "Nee", 1) == "Nee") {
//                $menuitems["Human Resources"] = array(
//                    "MENUID" => 5,
//                    "LINK" => "content.php?SITE=oft_hr",
//                    "RECHTEN" => "HR"
//                );
//            }
//        //$menuitems["Contracts"] = array("MENUID" => 6, "LINK" => "content.php?SITE=oft_contracten", "RECHTEN" => "CONTRACTS");
//
//        $menuitems["Files"] = array("MENUID" => 6, "LINK" => "content.php?SITE=file_management", "RECHTEN" => "LEGAL");
//
//        if (check("hide_section_Reports", "Nee", 1) == "Nee") {
//            $menuitems["Reports"] = array(
//                "MENUID" => 7,
//                "LINK" => "javascript:toggle('reports_menu');",
//                "RECHTEN" => "Algemeen"
//            );
//        }

        $teller = 0;
        foreach ($menuitems as $naam => $link) {
            $selected = "";
            if (isset($_REQUEST["MENUID"]) && $_REQUEST["MENUID"] == $link["MENUID"]) {
                $selected = "active";
            }
            $url = $link["LINK"];
            if (substr($url, 0, 10) != "javascript") {
                $url .= "&MENUID=" . $link["MENUID"];
            }

            //if (isRechten($link["RECHTEN"]) || isRechten($link["RECHTEN"], 'Lees')) {
            if ($naam == "Reports") { //&& (isRechtenOFT($bedrijfsid, $link["RECHTEN"]) || isRechtenOFT($bedrijfsid, $link["RECHTEN"], 'Lees'))) {
                //$selected .= "has_submenu"; //$naam .= " &nbsp; <img src=\"./images/pijl_hoofdmenu.png\" width=\"7\">";

                $content .= "<li class='$selected has_submenu'><span class='oft2_menu_label'>$naam</span><ul>";

                if (isRechtenOFT($bedrijfsid, getNeededRightsOfSite('oft_annual_accounts'), 'Lees')) {
                    $content .= "<li><a href=\"content.php?SITE=oft_annual_accounts\">Annual accounts</a></li>";
                }
                if (check("environment_orangefield", "Nee", '1') == "Ja") {
                    $content .= "<li><a href=\"content.php?SITE=oft_clients\">Black list clients</a></li>";
                    $content .= "<li><a href=\"content.php?SITE=oft_contracten\">Contracts</a></li>";
                    if (check('TCAM', 'Nee', $bedrijfsid) != 'Ja') {
                        $content .= "<li><a href=\"content.php?SITE=oft_compliance_chart\">Risk Assessment</a></li>";
                    }
                    $content .= "<li><a href=\"content.php?SITE=oft_registers_view\">Registers</a></li>";
                }
                if (isRechten("ADMINISTRATOR") && check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
                    $content .= "<li><a href=\"content.php?SITE=oft_entities_ended\">Ended entities</a></li>";
                }

                if (check('TCAM', 'Nee', $bedrijfsid) != 'Ja') {
                    if (isRechtenOFT($bedrijfsid, getNeededRightsOfSite('oft_annual_accounts'), 'Lees')) {
                        $content .= "<li><a href=\"content.php?SITE=oft_tax\">Tax overview</a></li>";
                    }
                }
                if (check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
                    if (isRechtenOFT($bedrijfsid, getNeededRightsOfSite('rapport_magaging_director'), 'Lees')) {
                        if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
                            $content .= "<li><a href=\"content.php?SITE=rapport_magaging_director\">Directors</a></li>";
                        } else {
                            $content .= "<li><a href=\"content.php?SITE=rapport_magaging_director\">Management board</a></li>";
                        }
                    }
                } else {// Kaspar: This is the code...
                    //if(isRechten("ADMINISTRATOR") || getNaam("login", $loginid, "PROFIEL") == "Administrator") {
                    $content .= "<li><a href=\"content.php?SITE=oft_compliance_report\">Compliance report</a></li>";
                    //}
                    if (isRechten("ADMINISTRATOR")) {
                        $content .= "<li><a href=\"content.php?SITE=oft_bulkdownload\">Bulkdownload documents</a></li>";
                    }
                }
                if (isRechtenOFT($bedrijfsid, getNeededRightsOfSite('oft_mena'), 'Lees') && check('DIF', 'Nee',
                        $bedrijfsid) != 'Ja') {
                    $content .= "<li><a href=\"content.php?SITE=oft_mena\">M&A</a></li>";
                }

                if (check("environment_orangefield", "Nee", '1') == "Ja") {
                    $content .= "<li><a href=\"content.php?SITE=oft_trainingsschema\">Training</a></li>";
                }

                $content .= "</ul></li>";
            } elseif ($naam == "Home" || $naam == "Entities" || $naam == "Companies" || $naam == "Files") {
                $content .= "<li class='$selected'><a href='$url'>$naam</a></li>";
            } elseif (isRechtenOFT($bedrijfsid, $link["RECHTEN"]) || isRechtenOFT($bedrijfsid, $link["RECHTEN"],
                    'Lees')) {
                $id = isset($link["ID"]) ? $link["ID"] : "";
                $style = "";
                if(isset($link['STYLE'])) {
                    $style = "style='".$link['STYLE']."'";
                }
                $content .= "<li id='$id' class='$selected' $style><a href='$url'>$naam</a></li>";
            }
            //} else {
            //    $content .= "<li class='disabled'><a>$naam</a></li>";
            //}
            $teller++;
        }
    }
    $content .= "</ul>";

    //menu rechts
    $content .= "<ul class='oft2_menu_right'>";
    if (isRechtenOFT($bedrijfsid, "MASTER")) {
//        if(isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "entity_home" || $_REQUEST['SITE'] == "entity_edit_visuals") {
//            $content .= "<li><a href='content.php?SITE=entity_edit_visuals'>Edit Dashboard</a></li>";
//        }
        $content .= "<li><a href='content.php?SITE=oft_persons'>Persons</a></li>";
        $content .= "<li><a class='config-btn' href='content.php?SITE=oft_mastertables'><span style='margin-right:18px;'>Configuration</span></a></li>";
        if(isset($_REQUEST['SITE']) && isset($_REQUEST['VISUAL'])) {
            $site = $_REQUEST['SITE'];
            $visual = $_REQUEST['VISUAL'];
            if(($site == "entity_home" || $site == "entity_home2" || $site == "entity_edit_visuals") && $visual == "start") {
                $content .= "<li><a href='content.php?SITE=entity_edit_visuals&VISUAL=start&MENUID=9'><span style='margin-right:18px;'>Edit Dashboard</span></a></li>";
            }
            else if(($site == "entity_home" || $site == "entity_home2" || $site == "entity_edit_visuals") && $visual == "metrics") {
                $content .= "<li><a href='content.php?SITE=entity_edit_visuals&VISUAL=metrics&MENUID=11'><span style='margin-right:18px;'>Edit Metrics</span></a></li>";
            }
            else if(($site == "entity_home" || $site == "entity_home2" || $site == "entity_edit_visuals") && $visual == "pipeline") {
                $content .= "<li><a href='content.php?SITE=entity_edit_visuals&VISUAL=pipeline&MENUID=10'><span style='margin-right:18px;'>Edit Pipeline</span></a></li>";
            }
        }
    }
    $content .= "</ul>";

    return $content;
}

function oft_menu_nivo2($userid, $bedrijfsid, $buttons_nivo2)
{
    global $db;
    $content = '<div class="oft2_inline_menu">';

    $sections = array();
    $lastsection = null;

    foreach ($buttons_nivo2 as $name => $item) {
        if (isset($item['link']) && $item['link'] != '') {
            if (isset($item["SECTIE"]) && $item["SECTIE"] == "true") {
                $lastsection = new stdClass();
                $lastsection->name = $name;
                $lastsection->items = array();
                $lastsection->link = $item["link"];
                $lastsection->selected = ($item['selected'] == 'Ja');
                array_push($sections, $lastsection);
            } else {
                $linkEle = new stdClass();
                $linkEle->name = $name;
                $linkEle->selected = false;

                if ($lastsection == null) {
                    $lastsection = new stdClass();
                    $lastsection->name = $name;
                    $lastsection->items = array();
                    $lastsection->link = $item["link"];
                    $lastsection->selected = ($item['selected'] == 'Ja');
                    array_push($sections, $lastsection);
                }

                if ($item['selected'] == 'Ja') {
                    $linkEle->selected = true;
                    $lastsection->selected = true;
                }
                $linkEle->link = $item['link'];

                array_push($lastsection->items, $linkEle);
            }
        }
    }

    foreach ($sections as $section) {
        $class = "oft2_inline_menu_section";

        if ($section->selected) {
            $class .= " oft2_inline_menu_section_selected";
        }

        if (count($section->items) == 0) {
            $class .= " oft2_inline_menu_no_items";
        }

        $content .= "<div class='" . $class . "'>";
        $content .= "<a href='" . $section->link . "' class='oft2_inline_menu_section_header'>";
        $content .= $section->name;
        $content .= "</a>";

        if ($section->selected) {
            $content .= "<ul>";
            foreach ($section->items as $item) {
                $class = $item->selected ? "oft2_inline_menu_item_selected" : "";
                $content .= "<li class=\"$class\"><a href=\"" . $item->link . "\">" . $item->name . "</a></li>";
            }

            $content .= "</ul>";
        }

        $content .= "</div>";
    }

    $content .= "</div>";

    return $content;
}

function buttons_oft_nivo2_buttons($userid, $bedrijfsid)
{
    global $pdo;

    $returnArray = array();

    if (isset($_REQUEST["SITE"]) &&
        ($_REQUEST["SITE"] == "oft_proxyholder_change"
            || $_REQUEST["SITE"] == "oft_wizzard_agm"
            || $_REQUEST["SITE"] == "oft_wizzard_board_change"
            || $_REQUEST["SITE"] == "oft_approval_transaction"
            || $_REQUEST["SITE"] == "oft_dividend_distribution"
        )) {
        $returnArray = oft_custom_menu_twee($userid, $bedrijfsid, $_REQUEST["SITE"]);
    } elseif (isset($_REQUEST["SITE"]) &&
        ($_REQUEST["SITE"] == "oft_entitie_finance"
            || $_REQUEST["SITE"] == "oft_finance_report"
            || $_REQUEST["SITE"] == "oft_finance_report_balans"
            || $_REQUEST["SITE"] == "oft_finance_report_penl"
            || $_REQUEST["SITE"] == "oft_finance_audit_import"
        )) {
        $returnArray = oft_submenu_entities_finance($userid, $bedrijfsid);
    } elseif (isset($_REQUEST["SITE"]) &&
        ($_REQUEST["SITE"] == "oft_entitie_overview"
            || $_REQUEST["SITE"] == "oft_entitie_corporate_data"
            || $_REQUEST["SITE"] == "oft_entitie_corporate_data_edit"
            || $_REQUEST["SITE"] == "oft_wizzard_board_change"
            || $_REQUEST["SITE"] == "oft_entitie_adress"
            || $_REQUEST["SITE"] == "oft_providers"
            || $_REQUEST["SITE"] == "oft_services"
            || $_REQUEST["SITE"] == "oft_entitie_capital"
            || $_REQUEST["SITE"] == "oft_entitie_capital_edit"
            || $_REQUEST["SITE"] == "oft_entitie_shareholders"
            || $_REQUEST["SITE"] == "oft_entitie_shareholder_info"
            || $_REQUEST["SITE"] == "oft_entitie_shareholdings"
            || $_REQUEST["SITE"] == "oft_entitie_management"
            || $_REQUEST["SITE"] == "oft_entitie_proxyholders"
            || $_REQUEST["SITE"] == "oft_entitie_compliance"
            || $_REQUEST["SITE"] == "oft_entitie_documents"
            || $_REQUEST["SITE"] == "oft_entitie_resolutions"
            || $_REQUEST["SITE"] == "oft_entitie_boardpack"
            || $_REQUEST["SITE"] == "oft_entitie_agenda"
            || $_REQUEST["SITE"] == "oft_entitie_agenda_edit"
            || $_REQUEST["SITE"] == "oft_entitie_numbers"
            || $_REQUEST["SITE"] == "oft_entitie_contracts"
            || $_REQUEST["SITE"] == "oft_entitie_legal"
            || $_REQUEST["SITE"] == "oft_entitie_hr"
            || $_REQUEST["SITE"] == "oft_entitie_accounting"
            || $_REQUEST["SITE"] == "oft_entitie_finance"
            || $_REQUEST["SITE"] == "oft_entitie_transactions"
            || $_REQUEST["SITE"] == "oft_registers_debt_tracker"
            || $_REQUEST["SITE"] == "oft_registers_debt_tracker_detail"
        )) {
        $returnArray = oft_submenu_entities($userid, $bedrijfsid);
    } elseif (isset($_REQUEST["SITE"]) &&
        ($_REQUEST["SITE"] == "oft_notifications"
            || $_REQUEST["SITE"] == "oft_mastertables"
            || $_REQUEST["SITE"] == "oft_audit_trail"
            || $_REQUEST["SITE"] == "oft_login_list"
        )) {
        $returnArray = oft_submenu_config($userid, $bedrijfsid);
    } elseif (isset($_REQUEST["SITE"]) &&
        ($_REQUEST["SITE"] == "oft_persons_view"
            || $_REQUEST["SITE"] == "oft_persons_appointments"
            || $_REQUEST["SITE"] == "oft_persons_contracts"
            || $_REQUEST["SITE"] == "oft_users_edit"
        )) {
        $returnArray = oft_submenu_persons($userid, $bedrijfsid);
    } elseif (isset($_REQUEST["SITE"]) &&
        ($_REQUEST["SITE"] == "oft_registers_incident"
            || $_REQUEST["SITE"] == "oft_registers_claim"
            || $_REQUEST["SITE"] == "oft_registers_actiontracking"
            || $_REQUEST["SITE"] == "oft_registers_actiontracking_audit"
            || $_REQUEST["SITE"] == "oft_registers_compliant"
            || $_REQUEST["SITE"] == "oft_registers_exceptions"
        )) {
        $returnArray = oft_submenu_registers($userid, $bedrijfsid);
    } else {

        $PID = "";
        if (isset($_REQUEST["PID"])) {
            $PID = "&PID=" . $_REQUEST["PID"];
        }

        $query = $pdo->prepare('SELECT HAVECAPITAL, MONITOR FROM bedrijf WHERE ID = :bedrijfsid LIMIT 1;');
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $query->execute();
        $dBedrijf = $query->fetch(PDO::FETCH_ASSOC);

        if ($dBedrijf !== false) {
            $queryLogin = $pdo->prepare('SELECT * FROM login WHERE (PERSONEELSLID) = :userid;');
            $queryLogin->bindValue('userid', $userid);
            $queryLogin->execute();
            $dataLogin = $queryLogin->fetch(PDO::FETCH_ASSOC);

            if ($dataLogin !== false) {
                $teller = 0;

                $hitmenubutton = 0;
                if (isset($_REQUEST["MENUID"])) {
                    $hitmenubutton = $_REQUEST["MENUID"];
                } elseif (isset($_REQUEST["SITE"])) {
                    $query = $pdo->prepare('SELECT menu.ID FROM menu, menuitems WHERE (menu.MENUITEMID*1) = menuitems.ID AND menuitems.LINK LIKE :site AND menu.LOGINID = "8" ORDER BY menuitems.LINK LIMIT 1;');
                    $siteLike = 'content.php?SITE=' . $_REQUEST["SITE"] . '%';
                    $query->bindValue('site', $siteLike);
                    $query->execute();
                    $data = $query->fetch(PDO::FETCH_ASSOC);

                    if ($data !== false) {
                        $hitmenubutton = $data["ID"] * 1;
                    }
                }

                //getNivo2menubuttonId
                $hoofdmenuid = getNivo2menubuttonId($userid, $dataLogin["ID"]);
                $eersteitem = 0;
                $aantalSelected = 0;

                $queryMenu = $pdo->prepare('SELECT menu.ID, menu.MENUITEMID FROM menu WHERE menu.LOGINID = "8" AND (menu.NIVO2*1) = (:menuid) AND NOT LOGINID = "0" ORDER BY (VOLGORDE*1)');
                $queryMenu->bindValue('menuid', $hoofdmenuid);
                $queryMenu->execute();

                foreach ($queryMenu->fetchAll() as $data) {
                    $queryMenu2 = $pdo->prepare('SELECT menuitems.NAAM, menuitems.RECHTEN, menuitems.LINK FROM menuitems WHERE menuitems.ID = :menuitemsid LIMIT 1;');
                    $queryMenu2->bindValue('menuitemsid', $data["MENUITEMID"]);
                    $queryMenu2->execute();
                    $data2 = $queryMenu2->fetch(PDO::FETCH_ASSOC);

                    if ($data2 !== false) {
                        if ($data2["RECHTEN"] == "Algemeen" || $dataLogin[$data2["RECHTEN"]] == "Ja" || $dataLogin[$data2["RECHTEN"]] == "Lees") {
                            $naam = $data2["NAAM"];
                            if ($eersteitem == '') {
                                $eersteitem = $naam;
                            }

                            $tonen = true;
                            if ($naam == "Capital") {
                                if ($dBedrijf["HAVECAPITAL"] == "Nee") {
                                    $tonen = false;
                                }
                            } elseif ($naam == "Compliance") {
                                if ($dBedrijf["MONITOR"] == "Nee") {
                                    $tonen = false;
                                }
                            }

                            if ($tonen) {
                                $returnArray[$naam]["selected"] = "Nee";
                                if (($data["ID"] * 1) == $hitmenubutton) {
                                    $returnArray[$naam]["selected"] = "Ja";
                                    $aantalSelected++;
                                }
                                $returnArray[$naam]["link"] = $data2["LINK"] . "&MENUID=" . ($data["ID"] * 1) . "&BID=" . $bedrijfsid . $PID;
                            }
                        }
                    }
                }
                if ($aantalSelected == 0) {
                    $returnArray[$eersteitem]["selected"] = "Ja";
                }
            }
        }
    }

    return $returnArray;
}

function getRootId($loginId)
{
    global $pdo;

    $hitmenubutton = 0;
    $query = $pdo->prepare('SELECT ID FROM menu WHERE LOGINID = :loginid AND EIGENNAAM = "Hoofdmenu" AND NIVO2 = "0" AND NOT LOGINID = "0" LIMIT 1;');
    $query->bindValue('loginid', $loginId);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        $hitmenubutton = $data["ID"] * 1;
    }

    return $hitmenubutton;
}

function getNivo1menubuttonId($hitmenubutton, $vorigeId, $rootId)
{
    global $pdo;

    $query = $pdo->prepare('SELECT NIVO2, ID FROM menu WHERE ID = :id AND NOT NIVO2 = "" AND NOT NIVO2 = "0" LIMIT 1;');
    $query->bindValue('id', $hitmenubutton);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        if (($data["NIVO2"] * 1) > 0 && ($data["NIVO2"] * 1) > $rootId) {
            $vorigeId = getNivo1menubuttonId($data["NIVO2"], $hitmenubutton, $rootId);
        }
    }

    return $vorigeId;
}

function getNivo2menubuttonId($userid, $loginid)
{
    global $pdo;

    $hitmenubutton = 0;
    if (isset($_REQUEST["MENUID"])) {
        $hitmenubutton = $_REQUEST["MENUID"];
    }
    $hoofdmenuid = $hitmenubutton;

    $query = $pdo->prepare('SELECT menu.NIVO2 FROM menu, menuitems WHERE menu.LOGINID = "8" AND (menu.MENUITEMID*1) = menuitems.ID AND menuitems.LINK LIKE :link AND NOT LOGINID = "0" LIMIT 1;');
    $likeSite = 'content.php?SITE=' . $_REQUEST["SITE"] . '%';
    $query->bindValue('link', $likeSite);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if ($data !== false) {
        $hoofdmenuid = $data["NIVO2"] * 1;
    } else {
        $queryMenu = $pdo->prepare('SELECT NIVO2 FROM menu WHERE ID = :id LIMIT 1;');
        $queryMenu->bindValue('id', $hoofdmenuid);
        $queryMenu->execute();
        $data = $queryMenu->fetch(PDO::FETCH_ASSOC);

        if ($data !== false) {
            //Get hoger nivo Id
            $queryMenu2 = $pdo->prepare('SELECT menu.ID, NIVO2 FROM menu WHERE ID = :id LIMIT 1;');
            $queryMenu2->bindValue('id', $data["NIVO2"]);
            $queryMenu2->execute();
            $data = $queryMenu2->fetch(PDO::FETCH_ASSOC);

            if ($data !== false) {
                $hoofdmenuid = $data["ID"];

                //Mag niet hoofdmenu zijn, maar moet submenu zijn
                $queryMenu3 = $pdo->prepare('SELECT menu.ID FROM menu WHERE ID = :id LIMIT 1;');
                $queryMenu3->bindValue('id', $data["NIVO2"]);
                $queryMenu3->execute();
                $result = $queryMenu3->fetch(PDO::FETCH_ASSOC);

                if (!$result) {
                    $hoofdmenuid = $hitmenubutton;
                }
            }
        }
    }

    return $hoofdmenuid;
}

function oft_custom_menu_twee($userid, $bedrijfsid, $site)
{
    $returnArray = array();

    if ($site == "oft_proxyholder_change") {
        $menuitems = array("Entities", "Article", "Proxyholders", "Documents");
    } elseif ($site == "oft_wizzard_agm") {
        $menuitems = array("Annual meeting", "Documents");
    } elseif ($site == "oft_wizzard_board_change") {
        $menuitems = array("Entities", "Board change", "members", "Documents");
    } elseif ($site == "oft_approval_transaction") {
        $menuitems = array("Checklist", "Transaction", "Documents");
    } elseif ($site == "oft_dividend_distribution") {
        $menuitems = array("Checklist", "Dividend", "Transaction", "Documents");
    }

    foreach ($menuitems as $k => $menuitem) {
        $returnArray[$menuitem]["selected"] = "Nee";
        if (isset($_REQUEST["STAP"]) && ($k + 1) == $_REQUEST["STAP"]) {
            $returnArray[$menuitem]["selected"] = "Ja";
        } elseif (!isset($_REQUEST["STAP"]) && $k == 0) {
            $returnArray[$menuitem]["selected"] = "Ja";
        }
        $returnArray[$menuitem]["link"] = "content.php?SITE=" . $_REQUEST["SITE"] . "&STAP=" . $k;
    }

    return $returnArray;
}

function oft_submenu_entities($userid, $bedrijfsid)
{
    $toevoegingUrl = "&MENUID=2&BID=$bedrijfsid";

    $returnArray["Legal"]["SITE"] = "oft_entitie_overview";
    $returnArray["Legal"]["SECTIE"] = "true";
    $returnArray["Overview"]["SITE"] = "oft_entitie_overview";
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja' || check('USA2EUROPE', 'Nee',
            $bedrijfsid) == 'Ja' || check('oft_entitie_agenda', 'Nee', $bedrijfsid) == 'Ja') {
        $returnArray["Agenda"]["SITE"] = "oft_entitie_agenda";
    }
    $returnArray["Corporate data"]["SITE"] = "oft_entitie_corporate_data";
    $returnArray["Corporate numbers"]["SITE"] = "oft_entitie_numbers";
    $returnArray["Address"]["SITE"] = "oft_entitie_adress";
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja' || check('oft_providers', 'Nee', $bedrijfsid) == 'Ja') {
        $returnArray["Providers"]["SITE"] = "oft_providers";
    }
    if (check('USA2EUROPE', 'Nee', $bedrijfsid) == 'Ja' || check('oft_services', 'Nee', $bedrijfsid) == 'Ja') {
        $returnArray["Services"]["SITE"] = "oft_services";
    }
    if (getNaam("bedrijf", $bedrijfsid, "HAVECAPITAL") == "Ja") {
        $returnArray["Capital"]["SITE"] = "oft_entitie_capital";
    }
    if (check('DIF', 'Nee', $bedrijfsid) == 'Nee') {
        $returnArray["Shareholders"]["SITE"] = "oft_entitie_shareholders";
        $returnArray["Transactions"]["SITE"] = "oft_entitie_transactions";
        $returnArray["Participations"]["SITE"] = "oft_entitie_shareholdings";
    }

    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $returnArray["Directors"]["SITE"] = "oft_entitie_management";
    } else {
        $returnArray["Management"]["SITE"] = "oft_entitie_management";
    }
    if (check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
        $returnArray["Proxyholders"]["SITE"] = "oft_entitie_proxyholders";
    }
    if (getNaam("bedrijf", $bedrijfsid, "MONITOR") == "Ja") {
        $returnArray["Compliance"]["SITE"] = "oft_entitie_compliance";
    }
    $returnArray["Documents"]["SITE"] = "oft_entitie_documents";
    if (check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
        $returnArray["Resolutions"]["SITE"] = "oft_entitie_resolutions";
    }
    if (check('DIF', 'Nee', $bedrijfsid) == 'Nee') {
        $returnArray["Boardpack"]["SITE"] = "oft_entitie_boardpack";
    }

    if (check('USA2EUROPE', 'Nee', $bedrijfsid) == 'Ja') {
        $returnArray["USA2EUROPE"]["SITE"] = "oft_entitie_accounting";
        $returnArray["USA2EUROPE"]["SECTIE"] = "true";
    }
//  $returnArray["Contracts"]["SITE"] = "oft_entitie_contracts";

    if (check("hide_section_Finance", "Nee", 1) == "Nee") {
        $returnArray["Finance"]["SITE"] = "oft_entitie_finance";
        $returnArray["Finance"]["SECTIE"] = "true";
    }

    if (check("hide_section_Human Resources", "Nee", 1) == "Nee") {
        $returnArray["Human Resources"]["SITE"] = "oft_entitie_hr";
        $returnArray["Human Resources"]["SECTIE"] = "true";
    }

    if (check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
        $returnArray["Contracts"]["SITE"] = "oft_entitie_contracts";
        $returnArray["Contracts"]["SECTIE"] = "true";
    }

    if (check('TCAM', 'Nee', $bedrijfsid) != 'Ja') {
        if (check("environment_orangefield", "Nee", '1') == "Ja") {
            $returnArray["Debt Tracker"]["SITE"] = "oft_registers_debt_tracker";
            $returnArray["Debt Tracker"]["SECTIE"] = "true";
        }
    }

    foreach ($returnArray as $k => $menuitem) {
        $returnArray[$k]["selected"] = "Nee";
        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == $menuitem["SITE"]) {
            $returnArray[$k]["selected"] = "Ja";
        }
        $returnArray[$k]["link"] = "content.php?SITE=" . $menuitem["SITE"] . $toevoegingUrl;
    }

    return $returnArray;
}

function oft_submenu_entities_finance($userid, $bedrijfsid)
{
    $toevoegingUrl = "&MENUID=2&BID=$bedrijfsid";

    $returnArray["Legal"]["SITE"] = "oft_entitie_overview";
    $returnArray["Legal"]["SECTIE"] = "true";

    if (check("hide_section_Finance", "Nee", 1) == "Nee") {
        $returnArray["Finance"]["SITE"] = "oft_entitie_finance";
        $returnArray["Finance"]["SECTIE"] = "true";
        $returnArray["Documents"]["SITE"] = "oft_entitie_finance";
        //$returnArray["Audit import"]["SITE"] = "oft_finance_audit_import";
        //$returnArray["Balance"]["SITE"] = "oft_finance_report_balans";
        //$returnArray["P&L"]["SITE"] = "oft_finance_report_penl";
    }

    if (check("hide_section_Human Resources", "Nee", 1) == "Nee") {
        $returnArray["Human Resources"]["SITE"] = "oft_entitie_hr";
        $returnArray["Human Resources"]["SECTIE"] = "true";
    }

    $returnArray["Contracts"]["SITE"] = "oft_entitie_contracts";
    $returnArray["Contracts"]["SECTIE"] = "true";

    foreach ($returnArray as $k => $menuitem) {
        $returnArray[$k]["selected"] = "Nee";
        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == $menuitem["SITE"]) {
            $returnArray[$k]["selected"] = "Ja";
        }
        $returnArray[$k]["link"] = "content.php?SITE=" . $menuitem["SITE"] . $toevoegingUrl;
    }

    return $returnArray;
}

function oft_submenu_config($userid, $bedrijfsid)
{
    $toevoegingUrl = "&MENUID=9&BID=$bedrijfsid";

    $returnArray["Master tables"]["SITE"] = "oft_mastertables";
    $returnArray["Notifications"]["SITE"] = "oft_notifications";
    $returnArray["Audit trail"]["SITE"] = "oft_audit_trail";
    $returnArray["Login list"]["SITE"] = "oft_login_list";

    foreach ($returnArray as $k => $menuitem) {
        $returnArray[$k]["selected"] = "Nee";
        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == $menuitem["SITE"]) {
            $returnArray[$k]["selected"] = "Ja";
        }
        $returnArray[$k]["link"] = "content.php?SITE=" . $menuitem["SITE"] . $toevoegingUrl;
    }

    return $returnArray;
}

function oft_submenu_persons($userid, $bedrijfsid)
{
    $toevoegingUrl = "&MENUID=8&BID=$bedrijfsid";

    if (isset($_REQUEST["PID"])) {
        $toevoegingUrl .= "&PID=" . $_REQUEST["PID"];
    }
    if (isset($_REQUEST["ID"])) {
        $toevoegingUrl .= "&ID=" . $_REQUEST["ID"];
    }

    $returnArray["Person"]["SITE"] = "oft_persons_view";
    $returnArray["Appointments"]["SITE"] = "oft_persons_appointments";
    $returnArray["Contracts"]["SITE"] = "oft_persons_contracts";
    $returnArray["User account"]["SITE"] = "oft_users_edit";

    foreach ($returnArray as $k => $menuitem) {
        $returnArray[$k]["selected"] = "Nee";
        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == $menuitem["SITE"]) {
            $returnArray[$k]["selected"] = "Ja";
        }
        $returnArray[$k]["link"] = "content.php?SITE=" . $menuitem["SITE"] . $toevoegingUrl;
    }

    return $returnArray;
}

function oft_submenu_registers($userid, $bedrijfsid)
{
    $toevoegingUrl = "&MENUID=7&BID=$bedrijfsid";

    $returnArray["Claim and register"]["SITE"] = "oft_registers_claim";
    $returnArray["Incident and register"]["SITE"] = "oft_registers_incident";
    $returnArray["Action tracking register - non-audit"]["SITE"] = "oft_registers_actiontracking";
    $returnArray["Action tracking register - audit"]["SITE"] = "oft_registers_actiontracking_audit";
    $returnArray["Complaint register"]["SITE"] = "oft_registers_compliant";

    foreach ($returnArray as $k => $menuitem) {
        $returnArray[$k]["selected"] = "Nee";
        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == $menuitem["SITE"]) {
            $returnArray[$k]["selected"] = "Ja";
        }
        $returnArray[$k]["link"] = "content.php?SITE=" . $menuitem["SITE"] . $toevoegingUrl;
    }

    return $returnArray;
}
