<?php
sercurityCheck();

function oft_search($userid, $bedrijfsid)
{
    global $pdo;

    //Zoek in documenten + in contracten

    $titel = tl('Search results');
    $tabel = "documentbeheer";
    $toevoegen = "content.php?SITE=oft_document_add";
    $bewerken = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
    $verwijderen = "";
    $submenuitems = oft_inputform_link($toevoegen, "New document");

    $addSql = "";

    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    if (check("search_in_entities", "Nee", $bedrijfsid) == "Ja") {
        $tabel = "entities";
        $bewerken = "";
        $submenuitems = "";

        unset($arrayVelden);
        unset($arrayVeldnamen);
        unset($arrayVeldType);

        $arrayVelden = [
            "target"    => "target",
            "countries|NAME"   => "pivot|COUNTRY_ID|countries|NAME",
            "rating"    => "rating",
            "website"   => "website",
            "stages|NAME"     => "pivot|STAGE_ID|stages|NAME",
            "revenue"   => "revenue",
            "ebitda"    => "ebitda",
            "headcount" => "headcount"
        ];

        $arrayVeldnamen = [
            "target"      => "Target",
            "countries|NAME"      => "Country",
            "rating"   => "Rating",
            "website"   => "Website",
            "stages|NAME"   => "Stage",
            "revenue"   => "Revenue",
            "ebitda"   => "EBITDA",
            "headcount"   => "Headcount"
        ];

        $arrayVeldType = [
            "target"      => "field",
            "countries|NAME"      => "pivot",
            "rating"   => "field",
            "website"   => "field",
            "stages|NAME"   => "pivot",
            "revenue"   => "field",
            "ebitda"   => "field",
            "headcount"   => "field"
        ];

        $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
            $tabel, 'target', $toevoegen, $bewerken, $verwijderen, $addSql);

    } else {
        $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
            $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, $addSql);
    }





    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_search_ajax($userid, $bedrijfsid)
{
    global $db;

    $tabel = "documentbeheer";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $toevoegen = "";
    $bewerken = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
    $verwijderen = "";

    $addSql = "";

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, $addSql);
}

function oft_search_filter($userid, $bedrijfsid, $filtervelden = null)
{
    global $db;


    $toonvelden = array(
        'basic' => explode(',', check('search_basic', 'entity,type', $bedrijfsid)),
        'Legal' => explode(',', check('search_legal', 'entity,type,year,category,uploaded_by', $bedrijfsid)),
        'Finance' => explode(',', check('search_finance', 'entity,type,year,category,uploaded_by', $bedrijfsid)),
        'HR' => explode(',', check('search_hr', 'entity,type,year,category,employee,country', $bedrijfsid)),
        'Contracts' => explode(',', check('search_contracts', 'entity,type,country,reference,partners', $bedrijfsid)),
        '' => explode(',',
            check('search_', 'entity,type,year,category,uploaded_by,employee,country,reference,partners', $bedrijfsid))
    );
    if (!isset($filtervelden)) {
        $doctype = 'basic';
        if (isset($_POST['SRCH_SECTIE'])) {
            $doctype = $_POST['SRCH_SECTIE'];
        }
        if (!array_key_exists($doctype, $toonvelden)) {
            $doctype = 'basic';
        }
        $filtervelden = $toonvelden[$doctype];
    }

    $zoekWaarde = "";
    if (isset($_REQUEST["SRCH_SEARCH"])) {
        $zoekWaarde = $_REQUEST["SRCH_SEARCH"];
    }

    $url_querystring = "";

    $output = "<form method=\"Post\" action=\"content.php?SITE=oft_search\">
               <table class=\"oft_tabel\">
                 <tr>
                   <td colspan=\"3\" class=\"oft_search_input\"><input value=\"$zoekWaarde\" type=\"text\" style=\"width: 422px; height: 30px; color: #fff; border-color: #fff; padding:5px; background: none;\" class=\"field\" placeholder=\"Documents and contracts\" name=\"SRCH_SEARCH\"></td>";
    $searchlink = "";
    if (check("search_in_entities", "Nee", $bedrijfsid) == "Nee") {
        $searchlink = '<a href="./content.php?SITE=oft_search&ADVANCED=Ja">Advanced search</a>';
    }
    if (isset($_REQUEST['ADVANCED']) && $_REQUEST['ADVANCED'] == 'Ja') {
        $output .= "</tr>";
        if (array_search('entity', $filtervelden) !== false) {
            $output .= "   <tr>
                           <td class=\"oft_search_block_vert\">&nbsp;</td>
                           <td class=\"oft_search_block_label\">" . check("default_name_of_entity", "Entity", 1) . "</td>
                           <td align=\"right\">";
            $SQL_ENTITY = "SELECT distinct bedrijf.ID, bedrijf.BEDRIJFSNAAM FROM bedrijf, documentbeheer WHERE documentbeheer.BEDRIJFSID = bedrijf.ID AND NOT bedrijf.LAND = '' AND NOT bedrijf.LAND is null ORDER BY bedrijf.BEDRIJFSNAAM";
            $output .= oft_select_search($url_querystring, "ID", "BEDRIJFSNAAM", $SQL_ENTITY, true);
            $output .= "     </td>";
        }
        if (array_search('type', $filtervelden) !== false) {
            $sectie = "";
            if (isset($_REQUEST["SRCH_SECTIE"])) {
                $sectie = $_REQUEST["SRCH_SECTIE"];
            }
            $output .= "   <tr>
                       <td class=\"oft_search_block_vert\">&nbsp;</td>
                       <td class=\"oft_search_block_label\">Type</td>
                       <td align=\"right\">
                       <select class=\"field\" id=\"SRCH_SECTIE\" name=\"SRCH_SECTIE\">";
            $secties = array(
                "All" => "All",
                "HR" => "HR",
                "Legal" => "Legal",
                "Finance" => "Finance",
                "Contracts" => "Contracts"
            );
            $output .= selectbox($secties, $sectie, false);
            $output .= "     </select></td>
                       <td>&nbsp</td>
                     </tr>";
        }
        if (array_search('year', $filtervelden) !== false) {
            $output .= "   <tr>
                     <td class=\"oft_search_block_vert\">&nbsp;</td>
                     <td class=\"oft_search_block_label\">Year</td>
                     <td align=\"right\">";
            $SQL_JAAR = "SELECT SUBSTRING(JAAR, 1, 4) As JAAR FROM documentbeheer WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY JAAR ORDER BY JAAR";
            $output .= oft_select_search($url_querystring, "JAAR", "JAAR", $SQL_JAAR, true);
            $output .= "     </td>
                     <td>&nbsp</td>
                   </tr>";
        }
        if (array_search('category', $filtervelden) !== false) {
            $output .= "   <tr>
                     <td class=\"oft_search_block_vert\">&nbsp;</td>
                     <td class=\"oft_search_block_label\">Category</td>
                     <td align=\"right\">";
            $SQL_DOCTYPE = "SELECT DOCTYPE FROM documentbeheer WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY DOCTYPE ORDER BY DOCTYPE";
            $output .= oft_select_search($url_querystring, "DOCTYPE", "DOCTYPE", $SQL_DOCTYPE, true);
            $output .= "     </td>
                     <td>&nbsp</td>
                   </tr>";
        }
        if (array_search('uploaded_by', $filtervelden) !== false) {
            $output .= "   <tr>
                     <td class=\"oft_search_block_vert\">&nbsp;</td>
                     <td class=\"oft_search_block_label\">Uploaded by</td>
                     <td align=\"right\">";
            $SQL_PERSONEELSLID = "SELECT personeel.ID, personeel.ACHTERNAAM
                              FROM documentbeheer, personeel
                             WHERE (NOT documentbeheer.DELETED = 'Ja' OR documentbeheer.DELETED is null)
                               AND (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null)
                               AND documentbeheer.PERSONEELSLID = personeel.ID
                          GROUP BY personeel.ID
                          ORDER BY ACHTERNAAM";
            $output .= oft_select_search($url_querystring, "ID", "ACHTERNAAM", $SQL_PERSONEELSLID, true);
            $output .= "     </td>
                     <td>&nbsp</td>
                   </tr>";
        }
        if (array_search('employee', $filtervelden) !== false) {
            $output .= "   <tr>
                     <td class=\"oft_search_block_vert\">&nbsp;</td>
                     <td class=\"oft_search_block_label\">Employee</td>
                     <td align=\"right\">";
            $SQL_PERSONEEL = "SELECT personeel.ID, personeel.ACHTERNAAM
                                      FROM documentbeheer, personeel
                                     WHERE (NOT documentbeheer.DELETED = 'Ja' OR documentbeheer.DELETED is null)
                                       AND (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null)
                                       AND documentbeheer.EMPLOYEE = personeel.ID
                                     GROUP BY personeel.ID
                                     ORDER BY ACHTERNAAM";
            $output .= oft_select_search($url_querystring, "ID", "ACHTERNAAM", $SQL_PERSONEEL, true);
            $output .= "     </td>
                     <td>&nbsp</td>
                   </tr>";
        }
        if (array_search('country', $filtervelden) !== false) {
            $output .= "   <tr>
                     <td class=\"oft_search_block_vert\">&nbsp;</td>
                     <td class=\"oft_search_block_label\">Country</td>
                     <td align=\"right\">";
            $SQL_COUNTRY = "SELECT COUNTRY FROM documentbeheer WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY COUNTRY ORDER BY COUNTRY";
            $output .= oft_select_search($url_querystring, "COUNTRY", "COUNTRY", $SQL_COUNTRY, true);
            $output .= "    </td>
                    <td>&nbsp</td>
                  </tr>";
        }
        if (array_search('uploadedby', $filtervelden) !== false) {
            $output .= "   <tr>
                     <td class=\"oft_search_block_vert\">&nbsp;</td>
                     <td class=\"oft_search_block_label\">Uploaded by</td>
                     <td align=\"right\">";
            $SQL_COUNTRY = "SELECT COUNTRY FROM documentbeheer WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY COUNTRY ORDER BY COUNTRY";
            $output .= oft_select_search($url_querystring, "COUNTRY", "COUNTRY", $SQL_COUNTRY, true);
            $output .= "    </td>
                    <td>&nbsp</td>
                  </tr>";
        }
        $output .= "  <tr>
                  <td class=\"oft_search_block_hoek\"><input type=\"hidden\" name=\"ADVANCED\" value=\"Ja\"/>&nbsp;</td>
                  <td colspan=\"2\" class=\"oft_search_block_hori\">&nbsp;</td>";
        $searchlink = '<a href="./content.php?SITE=oft_search&ADVANCED=0">Normal search</a>';

    }
    $output .= "</tr>
               </table>
              </form>";
    $output .= $searchlink;

    return $output;
}

function oft_select_search($url, $idField, $showField, $sql, $html = false)
{
    global $pdo;

    $query = $pdo->prepare($sql);
    $query->execute();


    $content = "<select name=\"SRCH_$showField\" id=\"SRCH_$showField\" class=\"field\">";
    $content .= "<option value=\"\">All</option>";

    foreach ($query->fetchAll() as $row) {
        if ($row[$showField] != "") {
            $id_string = $row[$idField];
            $showValue = tl(stripslashes($row[$showField]));

            $selected = "";
            if (isset($_REQUEST["SRCH_$showField"]) && $_REQUEST["SRCH_$showField"] == $id_string) {
                $selected = "selected=\"true\"";
            }

            $content .= "<option value=\"" . $id_string . "\" $selected>" . $showValue . "</option>";
        }
    }

    $content .= "</select>";

    return $content;
}