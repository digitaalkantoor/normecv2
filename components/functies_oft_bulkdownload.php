<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
//ini_set('memory_limit', '1024M');

sercurityCheck();

function post_process() {

    global $pdo;

    if (isset($_POST['download'])) {
        $bestandlocatie = bestandLocatie();
        $files_to_zip = array();
        if (count($_POST['files'])) {
            $qMarks = str_repeat('?,', count($_POST['files']) - 1) . '?';
            $query = $pdo->prepare('
                SELECT documentbeheer.*, bedrijf.LAND, bedrijf.BEDRIJFSNAAM, bedrijf.BEDRIJFNR
                FROM documentbeheer, bedrijf
                WHERE bedrijf.ID = documentbeheer.BEDRIJFSID
                AND documentbeheer.ID IN ('.$qMarks.');
            ');
            foreach ($_POST['files'] as $k => $v) {
                $query->bindValue($k+1, $v);
            }

            $query->execute();

            foreach ($query->fetchAll() as $dDocs) {
                if(trim($dDocs["LAND"]) == '') {
                    $dDocs["LAND"] = "onbekend";
                }

                $path = $_SERVER["DOCUMENT_ROOT"] . "/".$bestandlocatie."/store/".date("Ymd")."/".bestandsnaam_clean($dDocs["LAND"])."/".bestandsnaam_clean($dDocs["BEDRIJFSNAAM"])."/";

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                if (file_exists($path)) {
                    $fileparts = explode('.', $dDocs["BESTANDSNAAM"]);
                    $ext = array_values(array_slice($fileparts, -1))[0];
                    $datum = DateTime::createFromFormat('Y-m-d', $dDocs['DATUM'])->format('Ymd');
                    $updatedatum = DateTime::createFromFormat('Y-m-d', $dDocs['UPDATEDATUM'])->format('Ymd');
                    $year = DateTime::createFromFormat('Y-m-d', $dDocs['DATUM'])->format('Y');
                    $month = DateTime::createFromFormat('Y-m-d', $dDocs['DATUM'])->format('m');
                    $subject = !empty($dDocs['TAGS']) ? str_replace(' ', '-', $dDocs['TAGS']) : 'no-subject';
                    $file = $path . bestandsnaam_clean($datum.'_'.$dDocs['BEDRIJFNR'].'_'.str_replace(' ', '-', $dDocs['DOCTYPE']).'_'.$year.'_'.$month.'_'.$subject.'_'.$updatedatum.'_'.intval($dDocs["ID"]).'.'.$ext);
                    $files_to_zip[] = $file;
                    $fp = fopen($file, "w");
                    fwrite($fp, $dDocs["BESTANDCONTENT"]);
                    fclose($fp);
                } else {
                	die( "An error occurred when trying to locate file : [$path]" );
                	// Moet hier niet gemeld worden dat een bestand niet bestaat? Kan gebeuren toch?
                }
            }

            $zipfile = $_SERVER["DOCUMENT_ROOT"] . "/" . $bestandlocatie . "/store/" . date("Ymd") . "/export.zip";

            if (create_zip_v2($files_to_zip, $zipfile, true)) {
                header('Content-Description: File Transfer');
                header("Content-type: application/zip");
                header("Content-Transfer-Encoding: Binary");
                header('Content-Disposition: attachment; filename="'.basename($zipfile).'"');
                header('Expires: 0');
                header('Pragma: no-cache');
                header('Content-Length: ' . filesize($zipfile));
                ob_clean();
                readfile($zipfile);
                unlink($zipfile);
            } else {
            	// moet hier niet gemeld worden dat een zipfile maken niet werkte?
            }
        }
        exit;
    }
}

function oft_bulkdownload($userid, $bedrijfsid) {

    $titel = tl('Bulk download');
    $submenuitems = "";
    $c = "<div class=\"oft2_page_centering\"><br/><br/>";
    $c .= fileList($userid, $bedrijfsid);
    $c .= "</div>";
    echo oft_framework_basic($userid, $bedrijfsid, $c, $titel, $submenuitems);
}

function fileList($userid, $bedrijfsid)
{
    global $pdo;

    $query = $pdo->prepare('
        SELECT documentbeheer.ID, documentbeheer.UPDATEDATUM, documentbeheer.DOCTYPE, documentbeheer.JAAR, documentbeheer.MONTH, documentbeheer.BESTANDSNAAM, bedrijf.BEDRIJFSNAAM
        FROM documentbeheer, bedrijf
        WHERE bedrijf.ID = documentbeheer.BEDRIJFSID
        ORDER BY documentbeheer.UPDATEDATUM DESC
    ');
    $query->execute();

    $content = '
        <form method="post">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 20px"><input onclick="select_all(this, \'check-file-for-download\');" type="checkbox"</td>
                    <td>Title</td>
                    <td style="width: 200px;">Company</td>
                    <td style="width: 150px;">Type</td>
                    <td style="width: 100px;">Date</td>
                    <td style="width: 100px;">Updated on</td>
                </tr>
                <tr><td colspan="6"><hr></td></tr>
    ';

    foreach ($query->fetchAll() as $file) {
        $date = $file['MONTH'];
        if ($date != '') {
            $date .= ' / ';
        }
        $date .= $file['JAAR'];
        $content .= '
            <tr>
                <td><input name="files[]" class="check-file-for-download" type="checkbox" value="'.$file['ID'].'"></td>
                <td>'.$file['BESTANDSNAAM'].'</td>
                <td>'.$file['BEDRIJFSNAAM'].'</td>
                <td>'.$file['DOCTYPE'].'</td>
                <td>'.$date.'</td>
                <td>'.$file['UPDATEDATUM'].'</td>
            </tr>';
    }
    $content .= '
            </table>
            <hr>
            <input type="submit" name="download" value="Download files">
        </form>
        
    ';
    return $content;
}
