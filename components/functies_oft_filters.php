<?php

function periodeSelectie_custom($userid, $bedrijfsid, $url, $jaar, $functionsRow1, $functionsRow2)
{
  $output = '';

  if (!isset($_REQUEST["EXPORTEXCEL"]) && !isset($_REQUEST["MAILHEADER"]) && !isset($_REQUEST["PRINTEN"]) && !isset($_REQUEST["PDF"])) {
    $datumVan = date("Y-01-01");
    $datumTot = date("Y-12-31");
    if (isset($_REQUEST["DATUMVAN"])) {
      $datumVan = ps($_REQUEST["DATUMVAN"], "date");
    } else {
      $_REQUEST["DATUMVAN"] = $datumVan;
    }
    if (isset($_REQUEST["DATUMTOT"])) {
      $datumTot = ps($_REQUEST["DATUMTOT"], "date");
    } else {
      $_REQUEST["DATUMTOT"] = $datumTot;
    }

    $functionNul = $functionsRow1[0]["TYPE"];
    foreach ($functionsRow1 As $functions) {
      if (isset($_REQUEST[$functions["VELDNAAM"]]) && $_REQUEST[$functions["VELDNAAM"]] != '') {
        $url .= "&" . $functions["VELDNAAM"] . "=" . $_REQUEST[$functions["VELDNAAM"]];
      }
    }
    foreach ($functionsRow2 As $functions) {
      if (isset($_REQUEST[$functions["VELDNAAM"]]) && $_REQUEST[$functions["VELDNAAM"]] != '') {
        $url .= "&" . $functions["VELDNAAM"] . "=" . $_REQUEST[$functions["VELDNAAM"]];
      }
    }

    $output .= "<div class=\"dashboardVakje\">
                <form action=\"$url\" method=\"Post\" name=\"form\" id=\"filterform\">
                <table cellSpacing=\"1\" cellPadding=\"10\" border=\"0\" class=\"tabel\">";

    $output .= "<tr>";

    foreach ($functionsRow1 As $functions) {
      $value = "";
      if (isset($_REQUEST[$functions["VELDNAAM"]]) && $_REQUEST[$functions["VELDNAAM"]] != '') {
        $value = ps($_REQUEST[$functions["VELDNAAM"]]);
      }

      if ($functions["TYPE"] == "exportexcel") {
        $value = $url;
      }
      $output .= periodeSelectie_function($userid, $bedrijfsid, $functions["TYPE"], $functions["LABEL"], $functions["VELDNAAM"], $value);
    }

    $output .= "</tr><tr>";

    foreach ($functionsRow2 As $functions) {
      $value = "";
      if (isset($_REQUEST[$functions["VELDNAAM"]]) && $_REQUEST[$functions["VELDNAAM"]] != '') {
        $value = ps($_REQUEST[$functions["VELDNAAM"]]);
      }

      if ($functions["TYPE"] == "exportexcel") {
        $value = $url;
      }

      $output .= periodeSelectie_function($userid, $bedrijfsid, $functions["TYPE"], $functions["LABEL"], $functions["VELDNAAM"], $value);
    }

    if(!isset($_REQUEST["PRINTEN"]) && !isset($_REQUEST["PDF"])) {
      $output .= "<td valign=\"top\"><input type=\"submit\" name=\"PERIODE\" value=\"Show\" class=\"button\" /></td>";
    }
    $output .= "</tr>
             </table>
             </form>
             </div>";
  }
  
  return $output;
}

function periodeSelectie_function($userid, $bedrijfsid, $type, $label, $veldNaam, $value)
{
  global $db;

  $output = '';

  if ($type == "datum" || $type == "datumvan" || $type == "datumtot") {
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
                <td>" . datumveld($value, $veldNaam, "fieldHalf") . "</td>");
  } elseif ($type == "bankbuttons") {

    $opties[''] = "Alles";
    $opties['AFGEBOEKT'] = "Afgeboekt";
    $opties['BIJGEBOEKT'] = "Bijgeboekt";

    $opties['TEBOEKENAF'] = "Afgeboekt (te boeken)";
    $opties['TEBOEKENBIJ'] = "Bijgeboekt (te boeken)";

    $opties['TEBOEKEN'] = "Te boeken";

    $output .= ("<td>" . strtolower(tl($label)) . ":</td>
          <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox($opties, $value, true) . "</select></td>");

    $output .= ("</td>");
  } elseif ($type == "grootboekrekeningen") {
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
          <td>" . searchSelectionBox($veldNaam, "generateGrootboekLijst", $userid, $bedrijfsid, "grootboekrekeningen", $value, getNaam("grootboekitems", $value, "NAAM"), "", "fieldHalf searchselect", true) . "</td>");
  } elseif ($type == "sortering") {
    $opties = array("Aflopend" => "Aflopend", "Oplopend" => "Oplopend", );
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
          <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox($opties, $value, false) . "</select></td>");
  } elseif ($type == "termijnen") {
    $opties = array("Jaar" => "Jaar", "Half jaar" => "Half jaar", "Kwartaal" => "Kwartaal", "Maand" => "Maand",);
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
          <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox($opties, $value, true) . "</select></td>");
  } elseif ($type == "balans_termijnen") {
    $opties = array("Jaar" => "Year", "Half jaar" => "Half year", "Kwartaal" => "Quarter", "Maand" => "Month");
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
            <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox($opties, $value, false) . "</select></td>");
  } elseif ($type == "balans_toon_wat") {
    $opties = array("Balans" => "Balance sheet", "Kosten of opbrengst" => "Profit and Loss");
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
            <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox($opties, $value, false) . "</select></td>");
  } elseif ($type == "balans_toon_in") {
    $opties = array("Scherm" => "Scherm", "XAF-Audit-file 2.0" => "XAF-Auditfile 2.0", "Excel" => "Excel", "PDF" => "PDF");
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
            <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox($opties, $value, false) . "</select></td>");
  } elseif ($type == "balans_toon_niveau") {
    $opties = array("Passiva/Activa" => "Activa/Passiva", "Rubriek" => "Rubriek", "Hoofdrubriek" => "Hoofdrubriek", "Type" => "Type", "Begroting" => "Begroting"); //, "Verschil" => "Verschil"
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
            <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox($opties, $value, false) . "</select></td>");
  } elseif (substr($type, 0, 7) == "status_") {
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
            <td><select name=\"$veldNaam\" class=\"fieldHalf\">");
    $output .= selectbox_tabel(str_replace("status_", "", $type), $veldNaam, $veldNaam, $value, true, "GROUP BY $veldNaam");
    $output .=("</select></td>");
  } elseif ($type == "checkbox") {
    $checked = "";
    if (isset($_REQUEST[$veldNaam]) && $_REQUEST[$veldNaam] == 'Ja') {
      $checked = "checked=\"true\"";
    }
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
            <td><input type=\"checkbox\" name=\"$veldNaam\" value=\"Ja\" $checked /></td>");
  } elseif (substr($type, 0, 5) == "stam_") {
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
            <td><select name=\"$veldNaam\" class=\"fieldHalf\">" . selectbox_tabel($type, "ID", "NAAM", $value, true, '') . "</select></td>");
  } elseif ($type == "exportexcel") {
    $output .=("<td></td>
            <td><a href=\"" . $value . "&EXPORTEXCEL=true\" target=\"_blank\">" . tl($label) . " &#187;</a></td>");
  } else {
    $output .=("<td>" . strtolower(tl($label)) . ":</td>
          <td><input type=\"text\" name=\"$veldNaam\" value=\"" . lb($value) . "\" class=\"fieldHalf\" autofocus=\"true\" /></td>");
  }
  
  return $output;
}

function periodeSelectie_SQLToevoeging($userid, $bedrijfsid, $functionsRow1, $functionsRow2, $tabel)
{
  $return = "";

  foreach ($functionsRow1 As $functions) {
    $return .= periodeSelectie_request($functions, $tabel);
  }

  foreach ($functionsRow2 As $functions) {
    $return .= periodeSelectie_request($functions, $tabel);
  }

  return $return;
}

function periodeSelectie_request($functions, $tabel)
{
  $return = "";

  if ($functions["TYPE"] != "filter" && isset($_REQUEST[$functions["VELDNAAM"]]) && $_REQUEST[$functions["VELDNAAM"]] != '') {
    $operator = "like";
    $typeVeld = "text";
    if ($functions["TYPE"] == "relatie" || $functions["TYPE"] == "medewerker" || $functions["TYPE"] == "activiteit") {
      $typeVeld = "nr";
    } elseif ($functions["TYPE"] == "datum") {
      $typeVeld = "date";
    } elseif ($functions["TYPE"] == "datumvan") {
      $typeVeld = "date";
      $operator = ">=";
    } elseif ($functions["TYPE"] == "datumtot") {
      $typeVeld = "date";
      $operator = "<=";
    } elseif (substr($functions["TYPE"], 0, 7) == "status_") {
      $typeVeld = "text";
      $operator = "=";
    }

    $veldNaam = $functions["VELDNAAM"];
    if ($veldNaam == "DATUMVAN" || $veldNaam == "DATUMTOT") {
      $veldNaam = "DATUM";
    }

    if ($tabel == '' || $tabel == $functions["TABEL"]) {
      if (($typeVeld == "nr" && $_REQUEST[$functions["VELDNAAM"]] > 0) || lb($_REQUEST[$functions["VELDNAAM"]]) != '') {
        if ($typeVeld == "nr") {
          $return = " (" . ps($functions["TABEL"]) . "." . ps($veldNaam) . "*1) $operator '" . ps($_REQUEST[$functions["VELDNAAM"]], $typeVeld) . "' AND ";
        } elseif ($operator == 'like') {
          $return = " " . ps($functions["TABEL"]) . "." . ps($veldNaam) . " $operator '%" . ps($_REQUEST[$functions["VELDNAAM"]], $typeVeld) . "%' AND ";
        } else {
          $return = " " . ps($functions["TABEL"]) . "." . ps($veldNaam) . " $operator '" . ps($_REQUEST[$functions["VELDNAAM"]], $typeVeld) . "' AND ";
        }
      }
    }
  }

  return $return;
}

function datumveld($datum, $veldnaam, $class = "field", $extra = "")
{

  $showdatum = $datum;

  if (taal() == "EN") {
    $format = "yy-mm-dd";
  } else {
    $format = "dd-mm-yy";
  }

  if ($showdatum != "") {
    if (substr($datum, 2, 1) == "-") {
      //Notatie NL
      $tmpdag = substr($datum, 0, 2);
      $tmpmaand = substr($datum, 3, 2);
      $tmpjaar = substr($datum, 6, 4);
    } else {
      //EN notatie
      $tmpdag = substr($datum, 8, 2);
      $tmpmaand = substr($datum, 5, 2);
      $tmpjaar = substr($datum, 0, 4);
    }

    if (taal() == "EN") {
      $showdatum = $tmpjaar . "-" . $tmpmaand . "-" . $tmpdag;
    } else {
      $showdatum = $tmpdag . "-" . $tmpmaand . "-" . $tmpjaar;
    }
  }

  $vervaldatum = "";
  if (isset($_REQUEST["SITE"]) && ($_REQUEST["SITE"] == "inkoopboekinvoerDrie" || $_REQUEST["SITE"] == "inkoopboekinvoerTwee" || $_REQUEST["SITE"] == "inkoopboekinvoerEen")) {
    $standaard_betaaltermijn = check('betaaltermijn_inkoop', '14', 1);
    $vervaldatum = ".change(function(event, ui) {
                         if(document.getElementById('VERVALDATUM').value == '') {
                           $.get('content.php?SITE=generateBetaaltermijnInkoop', { klantid: document.getElementById('KLANTID').value } )
                           .done(function(data) {
                             document.getElementById('VERVALDATUM').value = x_days_after(document.getElementById('DATUM').value, data);
                           });
                         }
                       })";
  } elseif (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "verhuur_contract" && $veldnaam == "PERIODEVAN") {
    $vervaldatum = ".change(function(event, ui) {
                         if(document.getElementById('AFLEVERDATUMTIJD')) {
                           document.getElementById('AFLEVERDATUMTIJD').value = document.getElementById('PERIODEVAN').value;
                         }
                       })";
  } elseif (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "verhuur_contract" && $veldnaam == "PERIODETOT") {
    $vervaldatum = ".change(function(event, ui) {
                         if(document.getElementById('OPHAALDATUMTIJD')) {
                           document.getElementById('OPHAALDATUMTIJD').value = document.getElementById('PERIODETOT').value;
                         }
                       })";
  }

  $class .= " date-picker";

  //field$verplichtVeld
  return field_append("<img src=\"./images/calendar_13.png\" />",
    $veldnaam,
    date('d-m-Y'),
    $showdatum,
    $class,
    "$extra autocomplete=\"off\" onClick=\"$(this).datepicker({dateFormat: '" . $format . "'}).datepicker('show')$vervaldatum;\"",
    "autocomplete=\"off\" onClick=\"$('#$veldnaam').datepicker({dateFormat: '" . $format . "'}).datepicker('show')$vervaldatum;\"",
    "cursor"
  );
}

function field_append($carator, $name, $placeholder, $value, $class, $extra = '', $extraspan = '', $spanclass = '')
{
  if (isset($_REQUEST["PDF"]) || isset($_REQUEST["EXPORTEXCEL"]) || isset($_REQUEST["MAILHEADER"]) || isset($_REQUEST["PRINTEN"])) {
    return $value;
  } else {
    if($carator == "&#128;" || $carator == "$" || $carator == "�") {
      $carator = valuta(1);
    }

    if($carator == '') {
      return "<input class=\"$class\" name=\"$name\" id=\"$name\" type=\"text\" placeholder=\"" . lb($placeholder) . "\" value=\"" . lb($value) . "\" $extra />";
    } else {
      return "<div class=\"input-append\">
                <input class=\"$class\" name=\"$name\" id=\"$name\" type=\"text\" placeholder=\"" . lb($placeholder) . "\" value=\"" . lb($value) . "\" $extra />
              </div>";
    }
  }
}

function tijdselectiebox($url, $jaar, $display = 'none', $position = 'absolute')
{
  return "<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\" id=\"tijdselectiebox\" style=\"position:$position; display:$display; font-size: 10px; background: #dae2ee;\">
             <tr>
               <td rowspan=\"4\"><a href=\"$url&DATUMVAN=" . ($jaar - 2) . "-01-01&DATUMTOT=" . ($jaar - 2) . "-12-31\">" . ($jaar - 2) . "</a></td>
                 <td colspan=\"4\" align=\"center\">&nbsp;</td>
                 <td colspan=\"4\" align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-01-01&DATUMTOT=" . ($jaar - 1) . "-12-31\">" . ($jaar - 1) . "</a></td>
                 <td colspan=\"4\" align=\"center\">&nbsp;</td>
                 <td colspan=\"4\" align=\"center\">&nbsp;</td>
                 <td colspan=\"4\" align=\"center\"><a href=\"$url&DATUMVAN=" . $jaar . "-01-01&DATUMTOT=" . $jaar . "-12-31\"><b>" . $jaar . "</b></a></td>
                 <td colspan=\"4\" align=\"center\">&nbsp;</td>
               <td rowspan=\"4\"><a href=\"$url&DATUMVAN=" . ($jaar + 1) . "-01-01&DATUMTOT=" . ($jaar + 1) . "-12-31\">" . ($jaar + 1) . "</a></td>
             </tr>
             <tr>
                 <td colspan=\"6\" align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-01-01&DATUMTOT=" . ($jaar - 1) . "-06-30\">H1</a></td>
                 <td colspan=\"6\" align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-07-01&DATUMTOT=" . ($jaar - 1) . "-12-31\">H2</a></td>
                 <td colspan=\"6\" align=\"center\"><a href=\"$url&DATUMVAN=$jaar-01-01&DATUMTOT=$jaar-06-30\">H1</a></td>
                 <td colspan=\"6\" align=\"center\"><a href=\"$url&DATUMVAN=$jaar-07-01&DATUMTOT=$jaar-12-31\">H2</a></td>
             </tr>
             <tr>
                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-01-01&DATUMTOT=" . ($jaar - 1) . "-03-31\">" . tl("K1") . "</a></td>
                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-04-01&DATUMTOT=" . ($jaar - 1) . "-06-30\">" . tl("K2") . "</a></td>
                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-07-01&DATUMTOT=" . ($jaar - 1) . "-09-31\">" . tl("K3") . "</a></td>
                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-10-01&DATUMTOT=" . ($jaar - 1) . "-12-31\">" . tl("K4") . "</a></td>

                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=$jaar-01-01&DATUMTOT=$jaar-03-31\">" . tl("K1") . "</a></td>
                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=$jaar-04-01&DATUMTOT=$jaar-06-30\">" . tl("K2") . "</a></td>
                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=$jaar-07-01&DATUMTOT=$jaar-09-31\">" . tl("K3") . "</a></td>
                 <td colspan=\"3\" align=\"center\"><a href=\"$url&DATUMVAN=$jaar-10-01&DATUMTOT=$jaar-12-31\">" . tl("K4") . "</a></td>
             </tr>
             <tr>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-01-01&DATUMTOT=" . ($jaar - 1) . "-01-31\">M1</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-02-01&DATUMTOT=" . ($jaar - 1) . "-02-29\">M2</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-03-01&DATUMTOT=" . ($jaar - 1) . "-03-31\">M3</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-04-01&DATUMTOT=" . ($jaar - 1) . "-04-30\">M4</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-05-01&DATUMTOT=" . ($jaar - 1) . "-05-31\">M5</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-06-01&DATUMTOT=" . ($jaar - 1) . "-06-30\">M6</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-07-01&DATUMTOT=" . ($jaar - 1) . "-07-31\">M7</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-08-01&DATUMTOT=" . ($jaar - 1) . "-08-31\">M8</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-09-01&DATUMTOT=" . ($jaar - 1) . "-09-30\">M9</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-10-01&DATUMTOT=" . ($jaar - 1) . "-10-31\">M10</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-11-01&DATUMTOT=" . ($jaar - 1) . "-11-30\">M11</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=" . ($jaar - 1) . "-12-01&DATUMTOT=" . ($jaar - 1) . "-12-31\">M12</a></td>
  
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-01-01&DATUMTOT=$jaar-01-31\">M1</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-02-01&DATUMTOT=$jaar-02-29\">M2</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-03-01&DATUMTOT=$jaar-03-31\">M3</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-04-01&DATUMTOT=$jaar-04-30\">M4</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-05-01&DATUMTOT=$jaar-05-31\">M5</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-06-01&DATUMTOT=$jaar-06-30\">M6</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-07-01&DATUMTOT=$jaar-07-31\">M7</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-08-01&DATUMTOT=$jaar-08-31\">M8</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-09-01&DATUMTOT=$jaar-09-30\">M9</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-10-01&DATUMTOT=$jaar-10-31\">M10</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-11-01&DATUMTOT=$jaar-11-30\">M11</a></td>
               <td align=\"center\"><a href=\"$url&DATUMVAN=$jaar-12-01&DATUMTOT=$jaar-12-31\">M12</a></td>
             </tr>
           </table>";
}