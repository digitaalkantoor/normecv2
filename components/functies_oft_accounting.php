<?php
sercurityCheck();

function oft_entitie_accounting($userid, $bedrijfsid) {
    global $pdo;

    if(check('USA2EUROPE', 'Nee', $bedrijfsid) == 'Nee') {
        header('Location: /content.php?SITE=oft_home');
        return;
    }

    $MENUID        = getMenuItem();
    $titel         = oft_titel_entities($bedrijfsid);
    $submenuitems    = "";
    if(isRechtenOFT($bedrijfsid, "CRM")) {
        $submenuitems   = oft_inputform_link('content.php?SITE=oft_document_add&DOCTYPE=Accounting Timetable&BID=' . $bedrijfsid, "Upload Accounting Timetable");
        $submenuitems  .= oft_inputform_link('content.php?SITE=oft_document_add&DOCTYPE=Payroll Timetable&BID=' . $bedrijfsid, "Upload Payroll Timetable");
        $submenuitems  .= ' | '. "<a href=\"content.php?SITE=oft_entitie_accounting_edit&MENUID=$MENUID&ID=$bedrijfsid&BID=$bedrijfsid\">Edit USA2EUROPE</a>";
    }
    $back_button   = oft_back_button_entity();

    $contentFrame = "<h1>USA2EUROPE</h1>";

    $query = $pdo->prepare('select * from oft_entitie_accounting where BEDRIJFSID = :bedrijfsid limit 1;');
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();

    foreach ($query->fetchAll() as $dBedrijf) {
        $contentFrame .= "<style type=\"text/css\">
                            .oft_tabel td.oft_label {
                                width: 300px;
                            }
                          </style>";
        $contentFrame .= "<table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";
        $contentFrame .= "<tr>
                            <td class=\"oft_label\" valign=\"top\" align=\"left\">Service</td>
                            <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["SERVICETYPE"])."</td>
                           </tr>
                           <tr>
                            <td class=\"oft_label\" valign=\"top\" align=\"left\">Monthly Reporting Pack</td>
                            <td valign=\"top\" align=\"left\">".translateToYesNo(stripslashes($dBedrijf["MRP"]))."</td>
                           </tr>
                           <tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">Client Accounting System</td>
                             <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["CLIENTACCOUNTINGSYSTEM"]) . (stripslashes($dBedrijf["CLIENTACCOUNTINGSYSTEM"]) == "Other" ? ", " . stripslashes($dBedrijf["CLIENTACCOUNTINGSYSTEM_CUSTOM"]) : "" ) ."</td>
                            </tr>
                            <tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">Fiscal Year End - SAP <br>(Substituted Accounting Period)</td>
                             <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["FISCALYEAREND_SAP"])."</td>
                            </tr>
                            <tr>
                              <td class=\"oft_label\" valign=\"top\" align=\"left\">Audit Waiver filed</td>
                              <td valign=\"top\" align=\"left\" colspan=\"3\">".stripslashes($dBedrijf["AUDITWAIVERFILED"])."</td>
                            </tr>
                            <tr></tr>
                         </table>";
        $contentFrame .= "<table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            ";

        
        $allowedTypes = array("ABN", "TFN");
        
        $query = $pdo->prepare('SELECT * FROM oft_entitie_numbers where BEDRIJFSID = :bedrijfsid AND type IN (:allowedtypes) AND DELETED = "Nee" ORDER BY TYPE ASC;');
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $queryAllowedTypesString = '"' . implode('","', $allowedTypes) . '"';
        $query->bindValue('allowedtypes', $queryAllowedTypesString);
        $query->execute();

        foreach ($query->fetchAll() as $dNumber) {
            $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">".stripslashes($dNumber['TYPE'])."</td>
                         <td valign=\"top\" align=\"left\">".stripslashes($dNumber['VALUE'])."</td>
                        </tr>";
        }
        
        $allowedTypes = array("PAYGW");
        
        $query = $pdo->prepare('SELECT * FROM oft_entitie_numbers where BEDRIJFSID = :bedrijfsid AND type IN (:allowedtypes) AND DELETED = "Nee" ORDER BY TYPE ASC;');
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $queryAllowedTypesString = '"' . implode('","', $allowedTypes) . '"';
        $query->bindValue('allowedtypes', $queryAllowedTypesString);
        $query->execute();

        foreach ($query->fetchAll() as $dNumber) {
            $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">".stripslashes($dNumber['TYPE'])."</td>
                         <td valign=\"top\" align=\"left\">".stripslashes($dNumber['VALUE'])."</td>
                        </tr>";
        }
        $contentFrame .= "</table>";

        $contentFrame .= "<h3>Business Activity Statements</h3>
                          <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">Filing Due Date</td>
                             <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["BAS"])."</td>
                            </tr>
                            <tr>
                               <td class=\"oft_label\" valign=\"top\" align=\"left\">Payment Due Date</td>
                               <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["BASPAYMENT"])."</td>
                             </tr>
                         </table>";
        $contentFrame .= "<h3>Service Provider</h3>
                          <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <thead>
                                <tr>
                                    <th>Type</th><th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">Accounting</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["SERVICEPROVIDER_ACCOUNTING"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">Payroll</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["SERVICEPROVIDER_PAYROLL"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">Corp Sec</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["SERVICEPROVIDER_CORPSEC"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">Auditor</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["SERVICEPROVIDER_AUDITOR"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">Legal</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["SERVICEPROVIDER_LEGAL"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">Visa</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["SERVICEPROVIDER_VISA"])."</td>
                                </tr>
                            </tbody>
                          </table>";

        $contentFrame .= "<h3>Workers Comp</h3>
                          <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">Applicable</td>
                             <td valign=\"top\" align=\"left\">".translateToYesNo(stripslashes($dBedrijf["WORKERSCOMP"]))."</td>
                            </tr>
                         </table>
                         <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <thead>
                                <tr>
                                    <th>Type</th><th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">VIC</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["WORKERSCOMPEXPIRES_VIC"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">NSW</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["WORKERSCOMPEXPIRES_NSW"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">QLD</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["WORKERSCOMPEXPIRES_QLD"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">TAS</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["WORKERSCOMPEXPIRES_TAS"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">WA</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["WORKERSCOMPEXPIRES_WA"])."</td>
                                </tr>
                                <tr>
                                   <td class=\"oft_label\" valign=\"top\" align=\"left\">SA</td>
                                   <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["WORKERSCOMPEXPIRES_SA"])."</td>
                                </tr>
                            </tbody>
                          </table>";

        $contentFrame .= "<h3>ESS - Employment Share Scheme</h3>
                          <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">Applicable</td>
                             <td valign=\"top\" align=\"left\">".translateToYesNo(stripslashes($dBedrijf["ESS"]))."</td>
                            </tr>
                            <tr>
                               <td class=\"oft_label\" valign=\"top\" align=\"left\">Filing Due Date</td>
                               <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["ESSFILING"])."</td>
                            </tr>
                         </table>";

        $contentFrame .= "<h3>FBT - Fringe Benefit Tax</h3>
                          <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">Applicable</td>
                             <td valign=\"top\" align=\"left\">".translateToYesNo(stripslashes($dBedrijf["FBT"]))."</td>
                            </tr>
                            <tr>
                               <td class=\"oft_label\" valign=\"top\" align=\"left\">Filing Due Date</td>
                               <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["FBTFILING"])."</td>
                            </tr>
                            <tr>
                               <td class=\"oft_label\" valign=\"top\" align=\"left\">Payment Due Date</td>
                               <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["FBTPAYMENT"])."</td>
                            </tr>
                         </table>";

        $contentFrame .= "<h3>Payroll Tax</h3>
                          <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">Applicable</td>
                             <td valign=\"top\" align=\"left\">".translateToYesNo(stripslashes($dBedrijf["PAYROLLTAX"]))."</td>
                            </tr>
                            <tr>
                               <td class=\"oft_label\" valign=\"top\" align=\"left\">Expires on</td>
                               <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["PAYROLLTAXEXPIRES"])."</td>
                            </tr>
                            <tr>
                              <td class=\"oft_label\" valign=\"top\" align=\"left\">State</td>
                              <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["PAYROLLTAXSTATE"])."</td>
                            </tr>
                         </table>
                         <br/>";

        $contentFrame .= "<h3>Income Tax Return</h3>
                          <table class=\"oft_content_tabel oft_tabel\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
                            <tr>
                               <td class=\"oft_label\" valign=\"top\" align=\"left\">Filing Due Date</td>
                               <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["INCOMETAXFILING"])."</td>
                            </tr>
                            <tr>
                              <td class=\"oft_label\" valign=\"top\" align=\"left\">Payment Due Date</td>
                              <td valign=\"top\" align=\"left\">".stripslashes($dBedrijf["INCOMETAXPAYMENT"])."</td>
                            </tr>
                         </table>
                         <br/>";
    }

    $contentFrame   .= oft_getDocuments($userid, $bedrijfsid, "Accounting Timetable", array("Accounting Timetable"), "", "", "") . "<br/>";
    $contentFrame   .= oft_getDocuments($userid, $bedrijfsid, "Payroll Timetable", array("Payroll Timetable"), "", "", "") . "<br/>";

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_accounting_edit($userid, $bedrijfsid) {
    global $pdo;

    $MENUID        = getMenuItem();

    $titel         = oft_titel_entities($bedrijfsid);
    $submenuitems  = "";
    $back_button   = oft_back_button_entity();

    $templateFile  = "./templates/oft_entitie_accounting.htm";
    $tabel         = "oft_entitie_accounting";

    $bid            = "";
    $id             = "";
    if(isset($_REQUEST["BID"])) {
        $bid = $_REQUEST["BID"];

        $query = $pdo->prepare('SELECT ID FROM oft_entitie_accounting WHERE BEDRIJFSID = :bedrijfsid LIMIT 1');
        $query->bindValue('bedrijfsid', $bid);
        $query->execute();

        foreach ($query->fetchAll() as $row) {
          $id = $row['ID'];
        }

        if(empty($id)) {
            $query = $pdo->prepare('INSERT INTO oft_entitie_accounting(BEDRIJFSID) VALUES (:bedrijfsid);');
            $query->bindValue('bedrijfsid', $bid);
            $query->execute();

            $id = $pdo->lastInsertId();
        }
    }

    $contentFrame = "<h2>USA2EUROPE</h2>";

    $contentFrame .= replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}


function translateToYesNo($string) {
    if($string == "Ja") {
        return "Yes";
    } elseif($string == "Nee") {
        return "No";
    }

    return $string;
}