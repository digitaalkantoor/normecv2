<?php
sercurityCheck();

function oft_entities($userid, $bedrijfsid)
{
    global $pdo;

    getRechten($userid, "COMPLIANCE");

    if(isset($_REQUEST['DELETE']) && isset($_REQUEST['opslaan_ID']) && $_REQUEST['DELETE'] == "true" ) {
        $id = $_REQUEST['opslaan_ID'];
        $query = "UPDATE bedrijf SET `DELETED` = 'Ja' WHERE ID = ".$id;
        $stmt = $pdo->prepare($query);
        if($stmt->execute()) {
            // myabe notification delete was succesfull?
        }
    }
    $toevoegen = "content.php?SITE=oft_entities_add";
    $bewerken = "content.php?SITE=oft_entitie_overview";
    $verwijderen = "";

    $titel = tl(check("default_name_of_entities", "Entities", 1));
    $submenuitems = "";
    if (isRechtenOFT($bedrijfsid, "CRM")) {
        //$submenuitems    = "<a href=\"content.php?SITE=oft_entities_add\">check("default_name_of_new_entity", "New entity", 1)</a> | <a class=\"cursor\" onClick=\"toggle('corporate_action');\">Corporate action <img src=\"./images/pijl_hoofdmenu.png\" width=\"10\" /></a> | <a href=\"content.php?SITE=oft_entities&EXPORTEXCEL=true&PAGNR=-1\">Export to Excel</a>";
        if (check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
            $submenuitems = "<a href=\"content.php?SITE=oft_entities_add\">" . check("default_name_of_new_entity",
                    "New entity",
                    1) . "</a> | <a href=\"content.php?SITE=oft_organogram\">Structure chart</a> | <a href=\"content.php?SITE=oft_entities&EXPORTEXCEL=true&PAGNR=-1\">Export to Excel</a>";
        }
    }

    $contentFrame = "";

    $tabel = "oft_entities";
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $fields = oft_get_structure($userid, $bedrijfsid, "oft_entities_dif_overzicht", "all");
        $arrayVelden = $fields['arrayVelden'];
        $arrayVeldnamen = $fields['arrayVeldnamen'];
        $arrayVeldType = $fields['arrayVeldType'];
        $arrayZoekvelden = $fields['arrayZoekvelden'];

        if (!checkProfile("Administrator")) {
            $sqlToevoeging = " AND ID IN (SELECT BEDRIJFSID FROM oft_rechten_tot WHERE USERID = '" . getMySelf() . "' AND LEGAL='Ja')";
        } else {
            $sqlToevoeging = "";
        }
    } elseif (check("USA2EUROPE", "Nee", $bedrijfsid) == "Ja") {
        $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayVelden");
        $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayVeldnamen");
        $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayVeldType");
        $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayZoekvelden");
        $sqlToevoeging = "";
    } else {
        $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
        $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
        $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
        $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");
        $sqlToevoeging = "";
    }

    $tabel = "bedrijf";

    $contentFrame .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'BEDRIJFSNAAM', $toevoegen, $bewerken, $verwijderen, $sqlToevoeging);

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);

    //optie 2: overzichtadministraties($userid, $bedrijfsid, $groupID, $toonHeader) -> tree

    //optie 3: organogramPrinten($userid, $bedrijfsid)
}

function oft_entity_buttons($button)
{
    return '';

    if ($button == 'list') {
        $list = "<a href=\"content.php?SITE=oft_entities\"><b>List</b></a>";
    } else {
        $list = "<a href=\"content.php?SITE=oft_entities\">List</a>";
    }

    $list .= " | ";
    if ($button == 'tree') {
        $list .= "<a href=\"content.php?SITE=oft_overzichtadministraties\"><b>Tree</b></a>";
    } else {
        $list .= "<a href=\"content.php?SITE=oft_overzichtadministraties\">Tree</a>";
    }

    $list .= " | ";
    if ($button == 'organogram') {
        $list .= "<a target=\"_blank\" href=\"content.php?SITE=oft_organogram\"><b>Organization chart</b></a>";
    } else {
        $list .= "<a target=\"_blank\" href=\"content.php?SITE=oft_organogram\">Organization chart</a>";
    }

    $list .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    return $list;
}

function oft_entities_ajax($userid, $bedrijfsid)
{
    $tabel = "oft_entities";
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_dif_overzicht", "arrayVelden");
        $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_dif_overzicht", "arrayVeldnamen");
        $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_dif_overzicht", "arrayVeldType");
        $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_dif_overzicht", "arrayZoekvelden");
        if (!checkProfile("Administrator")) {
            $sqlToevoeging = " AND ID IN (SELECT BEDRIJFSID FROM oft_rechten_tot WHERE USERID = '" . getMySelf() . "' AND LEGAL='Ja')";
        } else {
            $sqlToevoeging = "";
        }
    } elseif (check("USA2EUROPE", "Nee", $bedrijfsid) == "Ja") {
        $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayVelden");
        $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayVeldnamen");
        $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayVeldType");
        $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_usa2eu_overzicht", "arrayZoekvelden");
        $sqlToevoeging = "";
    } else {
        $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
        $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
        $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
        $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");
        $sqlToevoeging = "";
    }

    $toevoegen = "content.php?SITE=oft_entities_add";
    $bewerken = "content.php?SITE=oft_entitie_overview";
    $verwijderen = "";

    $tabel = "bedrijf";

    $sqlToevoeging = "";
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        if (!checkProfile("Administrator")) {
            $sqlToevoeging = " AND ID IN (SELECT BEDRIJFSID FROM oft_rechten_tot WHERE USERID = '" . getMySelf() . "' AND LEGAL='Ja')";
        }
    }
    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'BEDRIJFSNAAM', $toevoegen, $bewerken, $verwijderen,
        $sqlToevoeging); //, "AND (ENDDATUM >= '".date("Y-m-d")."' OR ENDDATUM is null)"
}

function oft_entitie_end($userid, $bedrijfsid)
{
    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }

    $templateFile = "./templates/oft_entitie_end.htm";
    $tabel = "bedrijf";

    echo "<h2>End entity</h2>";

    echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);

}

function oft_entities_add($userid, $bedrijfsid)
{
    if (isset($_REQUEST["ID"])) {
        $bedrijfsid = $_REQUEST["ID"];
    }

    if (isset($_REQUEST["PREVIOUS"])) {
        oft_entities_edit($userid, $bedrijfsid);
    } else {
        $titel = check("default_name_of_new_entity", "New entity", 1);
        $submenuitems = "";
        $back_button = "";
        $contentFrame = "";

        $templateFile = "./templates/oft_wzzrd_1.htm";
        $tabel = "bedrijf";

        $contentFrame .= "<h1>Name</h1>";

        $contentFrame .= replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
        echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
    }
}

function oft_entities_add_2($userid, $bedrijfsid)
{
    $titel = oft_titel_entities($bedrijfsid);
    $submenuitems = "";
    $back_button = "";

    $templateFile = "./templates/oft_wzzrd_2.htm";
    $tabel = "bedrijf";

    $id = "";
    if (isset($_REQUEST["BID"])) {
        $id = $_REQUEST["BID"];
    }

    $contentFrame = "<h1>Corporate data</h1>";

    $contentFrame .= replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entities_add_3($userid, $bedrijfsid)
{
    global $pdo;

    $id = 0;
    $query = $pdo->prepare('SELECT ID FROM oft_bedrijf_adres WHERE BEDRIJFSID = :bedrijfsid LIMIT 1;');
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

    if (isset($data["ID"])) {
        $id = $data["ID"];
    }


    if ($id > 0) {
        oft_entities_edit_3($userid, $bedrijfsid, $id);
    } else {
        $templateFile = "./templates/oft_wzzrd_3.htm";
        $tabel = "oft_bedrijf_adres";
        $submenuitems = "";
        $back_button = "";
        $titel = oft_titel_entities($bedrijfsid);

        $contentFrame = "<h1>Add address</h1>";

        $contentFrame .= replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);

        echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
    }
}

function oft_entities_add_4($userid, $bedrijfsid)
{
    oft_entitie_capital_edit($userid, $bedrijfsid, "Ja");
}

function oft_entities_add_5($userid, $bedrijfsid)
{
    oft_entity_shareholders_edit($userid, $bedrijfsid);
}

function oft_entities_edit($userid, $bedrijfsid)
{
    $titel = oft_titel_entities($bedrijfsid);
    $submenuitems = "";
    $back_button = "";

    $templateFile = "./templates/oft_wzzrd_1.htm";
    $tabel = "bedrijf";

    $id = "";
    if (isset($_REQUEST["BID"])) {
        $id = $_REQUEST["BID"];
    }

    $contentFrame = "<h2>" . check("default_name_of_new_entity", "New entity", 1) . "</h2>";

    $contentFrame .= replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entities_edit_3($userid, $bedrijfsid, $id)
{
    $titel = oft_titel_entities($bedrijfsid);
    $submenuitems = "";
    $back_button = "";

    $templateFile = "./templates/oft_wzzrd_3.htm";
    $tabel = "oft_bedrijf_adres";

    $contentFrame = "<h2>Add address</h2>";

    $contentFrame .= replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_overview($userid, $bedrijfsid)
{
    global $pdo;

    if (isset($_REQUEST["ID"])) {
        $bedrijfsid = $_REQUEST["ID"];
    }

    check_annual_accounts($userid, $bedrijfsid, $bedrijfsid);

    $titel = oft_titel_entities($bedrijfsid);
    $submenuitems = "";
    $back_button = oft_back_button_entity();
    $contentFrame = "";

    $query = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :bedrijfsid LIMIT 1;');
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();
    $dBedrijf = $query->fetch(PDO::FETCH_ASSOC);

    if ($dBedrijf !== false) {
        if (check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
            if (isRechtenOFT($bedrijfsid, "CRM")) {
                //$submenuitems .= "<a class=\"cursor\" onClick=\"toggle('corporate_action');\">Corporate action <img src=\"./images/pijl_hoofdmenu.png\" width=\"10\" /></a>";
                //$submenuitems .= ' | '. "<a href=\"content.php?SITE=oft_entities_add\">New entity</a>";
            }
            if (isRechtenOFT($bedrijfsid, "CONTRACTEN")) {
                $submenuitems .= ' | ' . oft_inputform_link('content.php?SITE=oft_contracten_add', "New contract");
            }
            if (isRechtenOFT($bedrijfsid, "CRM")) {
                $submenuitems .= ' | ' . oft_inputform_link("content.php?SITE=oft_entitie_transactions_add",
                        "New transaction");
            }
            if (isRechtenOFT($bedrijfsid, "CRM")) {
                $submenuitems .= ' | ' . oft_inputform_link('content.php?SITE=oft_document_add', "Upload documents",
                        500);
            }
            if ($dBedrijf["ENDDATUM"] == "0000-00-00" || $dBedrijf["ENDDATUM"] == "") {
                if (isRechtenOFT($bedrijfsid, "CRM")) {
                    $submenuitems .= ' | ' . oft_inputform_link("content.php?SITE=oft_entitie_end&ID=" . $bedrijfsid,
                            "End entity");
                }
            }
            if (isRechtenOFT($bedrijfsid, "CRM")) {
                $submenuitems .= ' | ' . "<a href=\"content.php?SITE=oft_entities&DELETE=true&opslaan_ID=$bedrijfsid&opslaan_tabel=bedrijf\" onClick=\"return confirm('By deleting the entity, all data and notifications will be removed. Only the administator can undo.');\">Delete entity</a>";
            }
        } else {
            //if($dBedrijf["ENDDATUM"] == "0000-00-00" || $dBedrijf["ENDDATUM"] == "") {
            if (isRechtenOFT($bedrijfsid, "CRM")) {
                $submenuitems .= ' | ' . oft_inputform_link("content.php?SITE=oft_entitie_end&ID=" . $bedrijfsid,
                        "End entity");
            }
            //}
        }

        $contentFrame .= "<div id=\"corporate_action\" style=\"display: none;\">
                        <b>Shareholders resolutions</b>
                        <br/><a href=\"content.php?SITE=oft_dividend_distribution&BID=$bedrijfsid\">Dividend distribution</a>
                        <br/><a href=\"content.php?SITE=oft_approval_transaction&BID=$bedrijfsid\">Approval transaction</a>
                        <br/><a href=\"content.php?SITE=oft_wizzard_agm&BID=$bedrijfsid\">Annual general meeting</a>
                        <br/><a href=\"content.php?SITE=oft_wizzard_board_change&BID=$bedrijfsid\">Board change</a>
                        <hr size=\"1\" />
                        <b>Board resolutions</b>
                        <br/><a href=\"content.php?SITE=oft_dividend_distribution&BID=$bedrijfsid\">Dividend distribution</a>
                        <br/><a href=\"content.php?SITE=oft_approval_transaction&BID=$bedrijfsid\">Approval transaction</a>
                        <br/><a href=\"content.php?SITE=oft_proxyholder_change&BID=$bedrijfsid\">Proxyholder change</a>
                      </div>";

        $contentFrame .= "<h1>Legal overview</h1>";
        $contentFrame .= "<table class=\"oft_tabel\" width=\"700\" cellspacing=\"0\" cellpadding=\"10\">";
        $contentFrame .= "<tr><td class=\"oft_label\" valign=\"top\" align=\"left\">Name</td><td valign=\"top\" align=\"left\" colspan=\"3\">" . lb($dBedrijf["BEDRIJFSNAAM"]) . "</td></tr>";
        $contentFrame .= "<tr><td class=\"oft_label\" valign=\"top\" align=\"left\">Short code</td><td valign=\"top\" align=\"left\" colspan=\"3\">" . lb($dBedrijf["BEDRIJFNR"]) . "</td></tr>";
        $contentFrame .= "<tr><td class=\"oft_label\" valign=\"top\" align=\"left\">Purpose</td><td valign=\"top\" align=\"left\" colspan=\"3\">" . lb($dBedrijf["OMSCHRIJVING"]) . "</td></tr>";
        $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Country</td>
                         <td valign=\"top\" align=\"left\">" . lb($dBedrijf["LAND"]) . "</td>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Commercial name</td>
                         <td valign=\"top\" align=\"left\">" . lb($dBedrijf["HANDELSNAAM"]) . "</td>
                     </tr>";
        $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Legal form</td>
                         <td valign=\"top\" align=\"left\">" . getNaam("stam_rechtsvormen", $dBedrijf["RECHTSVORM"],
                "NAAM") . "</td>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Incorporation date</td>
                         <td valign=\"top\" align=\"left\">" . datumconversiePresentatieKort($dBedrijf["OPRICHTINGSDATUM"]) . "</td>
                     </tr>";
        if ($dBedrijf["ENDDATUM"]) {
            $contentFrame .= "<tr>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">End date</td>
                             <td valign=\"top\" align=\"left\">" . datumconversiePresentatieKort($dBedrijf["ENDDATUM"]) . "</td>
                             <td class=\"oft_label\" valign=\"top\" align=\"left\">End by</td>
                             <td valign=\"top\" align=\"left\">" . lb($dBedrijf["ENDREDEN"]) . "</td>
                         </tr>";
        }
        $contentFrame .= "</table><br/><br/>";
    }
    if (!$dBedrijf["ENDDATUM"] && $dBedrijf["MONITOR"] != "Nee") {

        // Notificaties van deze entiteit updaten
        notificaties_bijwerken($userid, $bedrijfsid, $bedrijfsid);

        // Weergeven
        $tabel = "notificaties";
        $arrayVelden = array(0 => "VERVALDATUM", 1 => "EVENT");
        $arrayVeldnamen = array(0 => "Due date", 1 => "Event");
        $arrayVeldType = array(0 => "datum", 1 => "field");
        $arrayZoekvelden = array();

        $toevoegen = "";
        $bewerken = "";
        $verwijderen = "";

        $contentFrame .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType,
            $arrayZoekvelden, $tabel, 'VERVALDATUM', $toevoegen, $bewerken, $verwijderen,
            "AND notificaties.BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' AND notificaties.STATUS = 'Active'");

    }

    if (check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
        $warnings = oft_entity_warnings($bedrijfsid, $userid);
        if ($warnings != "") {
            $contentFrame .= "<br/><br/><br/><h2>Warnings</h2>$warnings";
        }
    }

    //Relationship
    $relationschip = "";
    $pos = 0;

    $queryRelatie = $pdo->prepare('SELECT personeel.* FROM login, personeel WHERE (NOT login.DELETED = "Ja" OR login.DELETED is null)
                                  AND (NOT personeel.DELETED = "Ja" OR personeel.DELETED is null)
                                  AND (login.PERSONEELSLID) = personeel.ID
                                  AND personeel.BEDRIJFSID = :bedrijfsid
                                  AND login.PROFIEL = "Relationship manager" LIMIT 2;');
    $queryRelatie->bindValue('bedrijfsid', $bedrijfsid);
    $queryRelatie->execute();

    foreach ($queryRelatie->fetchAll() as $dRelatie) {
        if ($pos == 0) {
            $pos = 1;
            $relationschip .= "<br/><br/><table class=\"oft_tabel\">";
        }
        $relationschip .= "<td><span class=\"clr_orange\">" . $dRelatie["TYPE"] . "</span>
                          <br/><b style=\"color: #ba381d;\">Relationship manager</b>
                          <br/><b>" . getPersoneelslidNaam($dRelatie["ID"]) . "</b>
                          <br/>" . $dRelatie["EMAIL"] . "
                          <br/>" . $dRelatie["TELVAST"] . "
                          </div>
                       </td>";
    }
    if ($pos == 1) {
        $relationschip .= "</table>";
        $contentFrame .= $relationschip;
    }

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_overview_ajax($bedrijfsid, $userid)
{
    if (isset($_REQUEST["BID"])) {
        $bedrijfsid = $_REQUEST["BID"];
    }

    $tabel = "notificaties";
    $arrayVelden = array(0 => "VERVALDATUM", 1 => "EVENT");
    $arrayVeldnamen = array(0 => "Due date", 1 => "Event");
    $arrayVeldType = array(0 => "datum", 1 => "field");
    $arrayZoekvelden = array();

    $toevoegen = "";
    $bewerken = "";
    $verwijderen = "";

    $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType,
        $arrayZoekvelden, $tabel, 'VERVALDATUM', $toevoegen, $bewerken, $verwijderen,
        "AND notificaties.BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "' AND notificaties.STATUS = 'Active'");

    echo $contentFrame;
}

function oft_entity_warnings($bedrijfsid, $userid)
{
    $return = "";

    //TOTAAL van aandeelhouders != 100%
    $return .= oft_shareholder_warning($bedrijfsid, $userid);

    //TOTAAL van issued capital != onderverdeeld capital
    $return .= oft_capital_warning($bedrijfsid, $userid);

    //TOTAAL van issued capital != onderverdeeld capital
    $return .= oft_board_composition_warning($bedrijfsid, $userid);

    return $return;
}

function checkCountryOfResidence($bedrijfsid, $userid, $otherentity = 0)
{
    if (($otherentity * 1) > 0) {
        $land1 = getNaam("bedrijf", $bedrijfsid, "LAND");
        $land2 = getNaam("bedrijf", $otherentity, "LAND");
    } else {
        $land1 = getNaam("bedrijf", $bedrijfsid, "LAND");
        $land2 = getNaam("personeel", $userid, "LAND");
    }

    if ($land1 == $land2) {
        return true;
    } else {
        return false;
    }
}

function oft_board_composition_warning($bedrijfsid, $userid)
{
    global $pdo;

    $land = getNaam("bedrijf", $bedrijfsid, "LAND");
    $drempelpers = getNaam("bedrijf", $bedrijfsid, "COMPOSITIONBOARD");
    $drempelpers = str_replace("%", "", $drempelpers);
    $drempelpers = ps($drempelpers, "amount");

    //TOTAAL van aandeelhouders != 100%
    $executive = 0;
    $query = $pdo->prepare('SELECT COUNT(*) as Aantal
                                  FROM oft_management, personeel
                                 WHERE oft_management.BEDRIJFSID = :bedrijfsid
                                   AND oft_management.TYPE = "Executive Board"
                                   AND oft_management.USERID = personeel.ID
                                   AND personeel.LAND = :land
                                   AND (NOT oft_management.DELETED = "Ja" OR oft_management.DELETED is null)
                                 GROUP BY oft_management.BEDRIJFSID;');
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->bindValue('land', $land);
    $query->execute();
    $dExecutive = $query->fetch(PDO::FETCH_ASSOC);

    if ($dExecutive !== false) {
        $executive += $dExecutive["Aantal"] * 1;
    }

    $queryExecutive = $pdo->prepare('SELECT count(*) As Aantal
                                  FROM oft_management, bedrijf
                                 WHERE oft_management.BEDRIJFSID = :bedrijfsid
                                   AND oft_management.TYPE = "Executive Board"
                                   AND oft_management.ADMINID = bedrijf.ID
                                   AND bedrijf.LAND = :land
                                   AND (NOT oft_management.DELETED = "Ja" OR oft_management.DELETED is null)
                                 GROUP BY oft_management.BEDRIJFSID;');
    $queryExecutive->bindValue('bedrijfsid', $bedrijfsid);
    $queryExecutive->bindValue('land', $land);
    $queryExecutive->execute();

    $dExecutive = $queryExecutive->fetch(PDO::FETCH_ASSOC);
    if ($dExecutive !== false) {
        $executive += $dExecutive["Aantal"] * 1;
    }

    $totalboard = 0;

    $queryExecutive = $pdo->prepare('SELECT count(*) As Aantal
      FROM oft_management
      WHERE BEDRIJFSID = :bedrijfsid
      AND TYPE = "Executive Board" AND (NOT DELETED = "Ja" OR DELETED is null)
      GROUP BY BEDRIJFSID;');
    $queryExecutive->bindValue('bedrijfsid', $bedrijfsid);
    $queryExecutive->execute();
    $dExecutive = $queryExecutive->fetch(PDO::FETCH_ASSOC);

    if ($dExecutive !== false) {
        $totalboard = $dExecutive["Aantal"] * 1;
    }

    $persentage = 0;
    if ($totalboard > 0) {
        $persentage = ceil((($executive / $totalboard) * 100));
    }

    $image = "./images/oft/oft_red_circle.png";
    if ($persentage >= $drempelpers) {
        $image = "./images/oft/oft_green_circle.png";
    }
    $return = "<br/><a href=\"content.php?SITE=oft_entitie_management&BID=$bedrijfsid\"><img src=\"$image\" border=\"0\" /> " . $persentage . "% of members reside in country of residence of the company (" . $drempelpers . "% mandatory)</a>";

    return $return;
}

function oft_shareholder_warning($bedrijfsid, $userid)
{
    global $pdo;

    $MENUID = getMenuItem();

    $return = "";

    //TOTAAL van aandeelhouders != 100%
    $PERSENTAGE = 0;
    $queryWarning = $pdo->prepare('SELECT PERSENTAGE from oft_shareholders where ADMINIDFROM = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null);');
    $queryWarning->bindValue('bedrijfsid', $bedrijfsid);
    $queryWarning->execute();

    foreach ($queryWarning->fetchAll() as $dWarning) {
        $PERSENTAGE += ps(str_replace("%", "", str_replace(" ", "", $dWarning["PERSENTAGE"])), "amount");
    }

    if (($PERSENTAGE * 1) != 100) {
        $return = "<br/><a href=\"content.php?SITE=oft_entitie_shareholders&MENUID=$MENUID&BID=$bedrijfsid\"><img src=\"./images/oft/warning.png\" border=\"0\" /> The total percentage of ownership is not 100%</a>";
    }

    return $return;
}

function oft_capital_warning($bedrijfsid, $userid)
{
    global $pdo;


    $MENUID = getMenuItem();

    //TOTAAL van issued capital != onderverdeeld capital
    //Issued = Som (Issued Quantyty * Par value) (zie pagina 22)
    $return = "";

    $TOTALIssueed = 0;
    $queryWarning = $pdo->prepare('SELECT ISSUED FROM oft_capital where BEDRIJFSID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null) LIMIT 1;');
    $queryWarning->bindValue('bedrijfsid', $bedrijfsid);
    $queryWarning->execute();
    $dWarning = $queryWarning->fetch(PDO::FETCH_ASSOC);

    if ($dWarning !== false) {
        $TOTALIssueed += $dWarning["ISSUED"];
    }

    $onderverdeeld = 0;
    $queryWarning = $pdo->prepare('select ISSUEDQUANTITY, PARVALUE from oft_classes where BEDRIJFSID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null);');
    $queryWarning->bindValue('bedrijfsid', $bedrijfsid);
    $queryWarning->execute();

    foreach ($queryWarning->fetchAll() as $dWarning) {
        $onderverdeeld += ($dWarning["ISSUEDQUANTITY"] * $dWarning["PARVALUE"]);
    }

    if (getNaam("bedrijf", $bedrijfsid, "HAVECAPITAL") == "Ja") {
        if ($TOTALIssueed != $onderverdeeld) {
            $return = "<br/><a href=\"content.php?SITE=oft_entitie_capital_edit&MENUID=$MENUID&BID=$bedrijfsid\"><img src=\"./images/oft/warning.png\" border=\"0\" /> The issued capital is not equal to the shareholders ownership</a>";
        }
    }

    return $return;
}

function oft_entitie_adress($userid, $bedrijfsid)
{
    global $pdo;

    $titel = oft_titel_entities($bedrijfsid);

    $submenuitems = oft_inputform_link('content.php?SITE=oft_entitie_adress_add', "New address", 400);

    $back_button = oft_back_button_entity();

    $contentFrame = "<h1>Address</h1>";

    $teller = 0;

    $queryAdres = $pdo->prepare('SELECT * FROM oft_bedrijf_adres WHERE BEDRIJFSID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null) AND (STOPDATE >= :currentdate OR STOPDATE = "0000-00-00" OR STOPDATE IS NULL);');
    $queryAdres->bindValue('bedrijfsid', $bedrijfsid);
    $queryAdres->bindValue('currentdate', date("Y-m-d"));
    $queryAdres->execute();

    foreach ($queryAdres->fetchAll() as $dAdres) {

        $onclick = " onClick=\"" . oft_inputform_link_short('content.php?SITE=oft_entitie_adress_edit&ID=' . $dAdres["ID"]) . "\"";
        $contentFrame .= "<div onMouseOver=\"this.style.borderColor='#ff6300';this.style.borderWidth='1px'; this.style.borderStyle='solid';\" onMouseOut=\"this.style.borderColor='#fff';\"><table class=\"oft_tabel cursor\" width=\"100%\" $onclick>";
        $contentFrame .= "<tr><td width=\"200\"><b>" . $dAdres["TYPE"] . "</b></td><td width=\"300\">" . $dAdres["ADRES"] . "</td><td class=\"oft_label\" width=\"200\">Phone</td><td>" . $dAdres["TELEFOON"] . "</td></tr>";
        $contentFrame .= "<tr><td></td><td>" . $dAdres["POSTCODE"] . " " . $dAdres["PLAATS"] . "</td><td class=\"oft_label\">Fax</td><td>" . $dAdres["FAX"] . "</td></tr>";
        $contentFrame .= "<tr><td></td><td>" . $dAdres["LAND"] . "</td></tr>";

        if ($dAdres["STOPDATE"] != "" && $dAdres["STOPDATE"] != "0000-00-00") {
            //Get status adres
            $statusAdres = getNotificatieStatus(array(
                'status' => 'Open',
                'due_date' => $dAdres["STOPDATE"],
                'table' => "oft_bedrijf_adres",
                'table_id' => $dAdres["ID"]
            ));
            $endsIn = ceil(dateDiffVS('-', $dAdres["STOPDATE"], date("Y-m-d")) / 30);
            $contentFrame .= "<tr><td colspan=\"4\">$statusAdres End date within " . $endsIn . " months<Br/><br/></td></tr>";
        }
        $contentFrame .= "</table></div><Br/><br/>";

        $teller++;
    }
    if ($teller == 0) {
        $contentFrame .= "<em>No address available</em>";
    }

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_adress_add($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_entitie_adress.htm";
    $tabel = "oft_bedrijf_adres";
    $titel = "Add address";

    oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_adress_edit($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_entitie_adress_edit.htm";
    $tabel = "oft_bedrijf_adres";
    $titel = "Edit address";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_management($user, $company, $ajax = false)
{
    $title = oft_titel_entities($company);
    $submenuItems = "";

    if (check('DIF', 'Nee', $company) == 'Ja' || isRechtenOFT($company, "CRM")) {
        $submenuItems = oft_inputform_link("content.php?SITE=oft_entitie_management_add&TYPE=Executive Board",
            "New member");
    }

    $back_button = oft_back_button_entity();
    $contentFrame = "<h1>Directors</h1>";

    if (check('DIF', 'Nee', $company) != 'Ja') {
        $contentFrame = "<h1>Management</h1><h3>Executive Board</h3><div style=\"clear:both;\"></div>";
    }

    $fields = oft_get_structure($user, $company, "oft_management_overzicht", "all");
    $table = "oft_management";
    $order = "FUNCTION";
    $add = "";
    $edit = oft_inputform_link_short("content.php?SITE=oft_entitie_management_edit&ID=-ID-");
    $delete = "";
    $sql = "AND $table.TYPE = 'Executive Board' AND $table.BEDRIJFSID = '" . ps($company, "nr") . "'";

    if ($ajax) {
        return oft_tabel_content($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete, $sql);
    }

    $contentFrame .= oft_tabel($user, $company,
        $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
        $table, $order, $add, $edit, $delete, $sql);

    if (check('DIF', 'Nee', $company) != 'Ja') {
        $fields = oft_get_structure($user, $company, "oft_management_nonboard_overzicht", "all");
        $edit = oft_inputform_link_short("content.php?SITE=oft_entitie_management_edit2&ID=-ID-");
        $sql = "AND $table.TYPE = 'Non-executive Board' AND $table.BEDRIJFSID = '" . ps($company, "nr") . "'";

        $contentFrame .= "<br/><br/><h3>Non-executive Board</h3>";
        if (isRechtenOFT($company, "CRM")) {
            $contentFrame .= oft_inputform_link("content.php?SITE=oft_entitie_management_add2&TYPE=Non-executive Board",
                "New non-executive board member");
        }
        $contentFrame .= "<div style=\"clear:both;\"></div>";
        $contentFrame .= oft_tabel($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete, $sql);
    }
    echo oft_framework_menu($user, $company, $contentFrame, $title, $submenuItems, $back_button);
}

function oft_entitie_management_ajax($user, $company)
{
    echo oft_entitie_management($user, $company, true);
}

function oft_entitie_management_add($userid, $bedrijfsid)
{
    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $templateFile = "./templates/oft_management_dif.htm";
    } else {
        $templateFile = "./templates/oft_management.htm";
    }
    $tabel = "oft_management";

    echo "<h2>New member Executive Board</h2>";

    echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_entitie_management_add2($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_management_nonboard.htm";
    $tabel = "oft_management";

    echo "<h2>New member Non-executive Board</h2>";

    echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_entitie_management_edit($userid, $bedrijfsid)
{

    if (check('DIF', 'Nee', $bedrijfsid) == 'Ja') {
        $templateFile = "./templates/oft_management_edit_dif.htm";
    } else {
        $templateFile = "./templates/oft_management_edit.htm";
    }

    $tabel = "oft_management";
    $titel = "Edit member";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_management_edit2($userid, $bedrijfsid)
{

    $templateFile = "./templates/oft_management_edit_nonboard.htm";
    $tabel = "oft_management";
    $titel = "Edit member";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_proxyholders($userid, $bedrijfsid)
{
    $submenuitems = "";
    if (isRechtenOFT($bedrijfsid, "CRM")) {
        $submenuitems = oft_inputform_link("content.php?SITE=oft_proxyholders_add", "Add proxyholder");
    }

    echo oft_standaard_menu_page($userid, $bedrijfsid, "Proxyholders", "oft_proxyholders", "oft_proxyholders", "USERID",
        oft_back_button_entity(), "Add proxyholder", "", $submenuitems);
}

function oft_proxyholders_add($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_proxyholders.htm";
    $tabel = "oft_proxyholders";
    $titel = "New proxyholder";

    oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_proxyholders_edit($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_proxyholders.htm";
    $tabel = "oft_proxyholders";
    $titel = "Edit proxyholder";

    oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_human_resource($userid, $bedrijfsid)
{
    $submenuitems = "";
    if (isRechtenOFT($bedrijfsid, "CRM")) {
        $submenuitems = oft_inputform_link("content.php?SITE=oft_document_add", "Add documents");
    }

    echo oft_standaard_menu_page($userid, $bedrijfsid, "Human Resources", "oft_proxyholders", "oft_proxyholders",
        "USERID", oft_back_button_entity(), "Add proxyholder", "", $submenuitems);
}

function oft_titel_entities($bedrijfsid)
{
    return getNaam("bedrijf", $bedrijfsid, "BEDRIJFSNAAM");
}

function oft_back_button_entity()
{
    return "<a href=\"content.php?SITE=oft_entities\"><span>All entities</span></a>";
}

function oft_entities_ended($userid, $bedrijfsid)
{
    $toevoegen = "";
    $bewerken = "content.php?SITE=oft_entitie_overview";
    $verwijderen = "";

    $titel = tl('Ended entities');
    $submenuitems = "";

    $contentFrame = "";

    $tabel = "oft_entities_ended";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $tabel = "bedrijf";
    $contentFrame .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'BEDRIJFSNAAM', $toevoegen, $bewerken, $verwijderen, "AND NOT ENDREDEN = 'Active'");

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_entities_ended_ajax($userid, $bedrijfsid)
{
    $tabel = "oft_entities_ended";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $toevoegen = "content.php?SITE=oft_entities_add";
    $bewerken = "content.php?SITE=oft_entitie_overview";
    $verwijderen = "";

    $tabel = "bedrijf";
    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'BEDRIJFSNAAM', $toevoegen, $bewerken, $verwijderen, "AND ENDREDEN IS NOT NULL");
}
