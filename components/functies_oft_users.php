<?php
sercurityCheck();

function oft_login($user, $company, $ajax = false)
{
    $title = 'Users';
    $submenuItems = "<a href=\"content.php?SITE=oft_persons_add\">Add person</a>";
    $contentFrame = oft_login_ajax($user, $company, true);
    echo oft_framework_basic($user, $company, $contentFrame, $title, $submenuItems);
}

function oft_login_ajax($user, $company, $return = false)
{
    $table = "login";
    $order = "USERNAME";
    $fields = oft_get_structure($user, $company, "login_overzicht", 'all');

    if (!$return) {
        echo oft_tabel_content($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, "", "", "");
    } else {
        return oft_tabel($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, "", "", "");
    }
}

function oft_persons($user, $company)
{
    getRechten($user, "HR");
    $submenuItems = "<a href=\"content.php?SITE=oft_persons_add\">Add person</a>";

    $contentFrame = oft_persons_ajax($user, $company, true);
    $title = 'Persons';
    echo oft_framework_basic($user, $company, $contentFrame, $title, $submenuItems);
}

function oft_persons_ajax($user, $company, $return = false)
{
    $fields = oft_get_structure($user, $company, 'persons_overview', 'all');
    $table = "personeel";
    $order = "ACHTERNAAM";
    $add = "content.php?SITE=oft_persons_add";
    $edit = "content.php?SITE=oft_persons_view";
    $delete = "";

    if (!$return) {
        echo oft_tabel_content($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete);
    } else {
        return oft_tabel($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order, $add, $edit, $delete);
    }
}

function oft_persons_add($userid, $bedrijfsid)
{
    $titel = oft_titel_entities($bedrijfsid);
    $submenuitems = '';
    $back_button = oft_back_button_persons();
    $contentFrame = '';

    $templateFile = "./templates/oft_persons.htm";
    $tabel = "personeel";

    $contentFrame .= "<h2>Add users</h2><br/><br/>";
    $contentFrame .= replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);

}

function oft_persons_view($userid, $bedrijfsid)
{
    global $pdo;

    $titel = "";
    $back_button = oft_back_button_persons();
    $contentFrame = '';

//    Not in use?
//    $templateFile = "./templates/oft_persons.htm";
//    $tabel = "personeel";

    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = ps($_REQUEST["ID"], "nr");
    } else {
        if (isset($_REQUEST["PID"])) {
            $id = ps($_REQUEST["PID"], "nr");
        }
    }

    $_REQUEST["PID"] = $id;

    $submenuitems = oft_inputform_link("content.php?SITE=oft_persons_edit&ID=$id", "Edit person");

    $query = $pdo->prepare('select * from personeel where ID = :id;');
    $query->bindValue('id', $id);
    $query->execute();
    $dPers = $query->fetch(PDO::FETCH_ASSOC);

    if ($dPers !== false) {
        $titel = "Person: " . $dPers["VOORNAAM"] . " " . $dPers["ACHTERNAAM"];
        $contentFrame .= "<table class=\"oft_tabel\" cellpadding=\"5\">";
        $contentFrame .= "<tr><td class=\"oft_label\">Name</td><td>" . $dPers["VOORNAAM"] . " " . $dPers["ACHTERNAAM"] . "</td></tr>";
        $contentFrame .= "<tr><td class=\"oft_label\">Email address</td><td>" . $dPers["EMAIL"] . "</td></tr>";
        $contentFrame .= "<tr><td class=\"oft_label\">Mobile</td><td>" . $dPers["MOB"] . "</td></tr>";
        $contentFrame .= "<tr><td class=\"oft_label\">Phone</td><td>" . $dPers["TELVAST"] . "</td></tr>";
        $contentFrame .= "<tr><td class=\"oft_label\">Country</td><td>" . $dPers["LAND"] . "</td></tr>";
        $contentFrame .= "<tr><td class=\"oft_label\">Working for entity</td><td>" . getNaam("bedrijf",
                $dPers["BEDRIJFSID"], "BEDRIJFSNAAM") . "</td></tr>";
        $contentFrame .= "</table>";
    }

    $appointment = 0;
    $queryPers = $pdo->prepare('select count(*) As Aantal from oft_management where USERID = :userid AND ADMINID = "0" AND (NOT DELETED = "Ja" OR DELETED is null);');
    $queryPers->bindValue('userid', $id);
    $queryPers->execute();
    $dPers = $queryPers->fetch(PDO::FETCH_ASSOC);

    if ($dPers !== false) {
        $appointment = $dPers["Aantal"];
    }

    $contracts = 0;
    $queryPers = $pdo->prepare('select count(*) As Aantal from contracten where ONDERTEKENDDOOR = :ondertekenddoor;');
    $queryPers->bindValue('ondertekenddoor', $id);
    $queryPers->execute();
    $dPers = $queryPers->fetch(PDO::FETCH_ASSOC);

    if ($dPers !== false) {
        $contracts = $dPers["Aantal"];
    }

    $contentFrame .= "<br/><br/>$appointment appointments";
    $contentFrame .= "<br/>$contracts contracts";

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_persons_appointments($userid, $bedrijfsid)
{
    if (isset($_REQUEST["PID"])) {
        $id = ps($_REQUEST["PID"], "nr");
    }

    $titel = oft_title_person($id);
    $submenuitems = '';
    $back_button = oft_back_button_persons();

    $contentFrame = '';
    $contentFrame .= "<h2>Appointments</h2>";

    $tabel = "oft_management";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayZoekvelden");

    $toevoegen = "";
    $bewerken = "";
    $verwijderen = "";

    $sqlToev = "AND USERID = '" . ps($id) . "' AND ADMINID = '0'
              AND (ENDATE IS NULL OR ENDATE = '0000-00-00' OR ENDATE = '' OR ENDATE > '" . date('Y-m-d') . "')
              ";
    $contentFrame .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'DATEOFAPPOINTENT', $toevoegen, $bewerken, $verwijderen, $sqlToev);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_persons_appointments_ajax($userid, $bedrijfsid)
{
    global $db;

    // $id??? (zie hieronder, is niet gedefinieerd.. is dit userid? of iets vuit een post/get?

    $tabel = "oft_management";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_appointments_overzicht", "arrayZoekvelden");

    $toevoegen = "";
    $bewerken = "";
    $verwijderen = "";

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'DATEOFAPPOINTENT', $toevoegen, $bewerken, $verwijderen,
        "AND USERID = '" . ps($id) . "' AND ADMINID = '0' AND (NOT DELETED = 'Ja' OR DELETED is null)");
}

function oft_persons_contracts($userid, $bedrijfsid)
{
    global $db;

    if (isset($_REQUEST["PID"])) {
        $id = ps($_REQUEST["PID"], "nr");
    }

    $titel = oft_title_person($id);
    $submenuitems = "";
    $back_button = oft_back_button_persons();

    $contentFrame = '';
    $contentFrame .= "<h2>Contracts</h2>";

    $tabel = "contracten";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $toevoegen = "";
    $bewerken = "";
    $verwijderen = "";

    $contentFrame .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'DATUMSTART', $toevoegen, $bewerken, $verwijderen, "AND ONDERTEKENDDOOR = '" . ps($id) . "'");

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_persons_edit($userid, $bedrijfsid)
{
    $templateFile = "./templates/oft_persons.htm";
    $tabel = "personeel";

    echo "<h2>Edit person</h2>";

    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = ps($_REQUEST["ID"], "nr");
    } else {
        if (isset($_REQUEST["PID"])) {
            $id = ps($_REQUEST["PID"], "nr");
        }
    }

    echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_users($userid, $bedrijfsid)
{
    global $db;

    if (!checkProfile("Administrator")) {
        rd("content.php?SITE=oft_home");
    }

    $titel = tl('Users');
    $submenuitems = "<a class=\"cursor\" onClick=\"oft2OpenPopup();getPageContent2('content.php?SITE=oft_users_add', 'invoerschermContent', searchReq2);\">New user</a>";

    $tabel = "login";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $toevoegen = "content.php?SITE=oft_users_add";
    $bewerken = oft_inputform_link_short("content.php?SITE=oft_users_edit&ID=-ID-");
    $verwijderen = "";

    $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, "USERNAME", $toevoegen, $bewerken, $verwijderen, " AND NOT USERNAME = '' ");

    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_users_ajax($userid, $bedrijfsid)
{
    global $db;

    if (!checkProfile("Administrator")) {
        rd("content.php?SITE=oft_home");
    }

    $tabel = "login";
    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVelden");
    $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel . "_overzicht", "arrayZoekvelden");

    $toevoegen = "content.php?SITE=oft_users_add";
    $bewerken = oft_inputform_link_short("content.php?SITE=oft_users_edit&ID=-ID-");
    $verwijderen = "";

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'USERNAME', $toevoegen, $bewerken, $verwijderen, " AND NOT USERNAME = '' ");
}

function oft_users_add($userid, $bedrijfsid)
{
    global $db;

    if (!checkProfile("Administrator")) {
        rd("content.php?SITE=oft_home");
    }

    $templateFile = "./templates/oft_users.htm";
    $tabel = "login";

    echo "<h2>Add user</h2>";

    echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_users_edit($userid, $bedrijfsid)
{
    global $db;
    $id = "";
    if (isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }

    $titel = 'New person';
    $submenuitems = '';
    $back_button = oft_back_button_persons();

    $templateFile = "./templates/oft_users_edit_2.htm";
    $tabel = "login";

    if ($id > 0) {
        //TODO: check of er een account is, en zo nee dan ook direct een nieuwe maken
        $loginid = create_new_useraccount($id, $bedrijfsid);

        //$loginid = getValue("login", "PERSONEELSLID", $id, "ID");

        $titel = oft_title_person($id);
        $contentFrame = replace_template_content_edit($id, $bedrijfsid, $tabel, $templateFile, $loginid);
    } else {
        $contentFrame = "Not a valid user selected";
    }

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_title_person($id)
{
    global $pdo;

    $titel = "";
    $query = $pdo->prepare('select * from personeel where ID = :id;');
    $query->bindValue('id', $id);
    $query->execute();
    $dPers = $query->fetch(PDO::FETCH_ASSOC);

    if ($dPers !== false) {
        $titel = "Person: " . $dPers["VOORNAAM"] . " " . $dPers["ACHTERNAAM"];
    }

    return $titel;
}

function oft_back_button_persons()
{
    return "<a href=\"content.php?SITE=oft_persons\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All persons</a>";
}

// This function sets the profile of loginid 'to' to loginid 'from'.
function setProfile($from, $to, $bedrijfsid)
{
    global $pdo;

    // Get profile rights from loginid 'from'
    $query = $pdo->prepare('SELECT * FROM login WHERE ID = :id limit 1;');
    $query->bindValue('id', $from);
    $query->execute();
    $data_r = $query->fetch(PDO::FETCH_ASSOC);

    if ($data_r !== false) {
        $profielId = $data_r["ID"] * 1;

        // Set profile rights to loginid 'to'
        $sql = "";

        $koloms = site("getlogin", 1);
        for ($k = 0; $k < count($koloms); $k++) {
            if ($koloms[$k] != "ID" && $koloms[$k] != "USERNAME" && $koloms[$k] != "PASSWORD" && $koloms[$k] != "KOPJE" && $koloms[$k] != "PROFIEL" && $koloms[$k] != "PERSONEELSLID" && $koloms[$k] != "BEDRIJFSID") {
                $sql .= " " . $koloms[$k] . " = '" . $data_r[$koloms[$k]] . "',";
            }
        }

        if ($sql != '') {
            $sql = substr($sql, 0, (strlen($sql) - 1));

            $query = $pdo->prepare('UPDATE login SET ' . $sql . ' WHERE ID = :id');
            $query->bindValue('id', $to);
            if ($query->execute()) {
                $userid = getPersoneelsid($to);
            }
        }
    }
    writeMenu($from, $to, $bedrijfsid, "Ja");
}


//TODO : rewrite without query param!
function getTabelArraySite($tabel, $kolom, $bedrijfsid, $query = "")
{
    global $pdo;

    $array = array();
    $dbKolom = taal();
    if ($kolom == 1) {
        $dbKolom = "KOLOM";
    }

    $teller = 0;
    $queryDB = $pdo->prepare('SELECT * FROM dbstructuur WHERE TABEL = :tabel ' . $query . ' ORDER BY VOLGORDE;');
    $queryDB->bindValue('tabel', $tabel);
    $queryDB->execute();

    foreach ($queryDB->fetchAll() as $data_dbstruct) {
        if ($kolom == 2) {
            $dbKolom = "FUNCTIE";
            if ($data_dbstruct["TONEN"] == "Nee") {
                $dbKolom = "FUNCTIEHIDDEN";
            }
        }
        $array[$teller] = $data_dbstruct[$dbKolom];
        $teller++;
    }

    return $array;
}

function writeMenu($from, $to, $bedrijfsid, $default)
{
    global $pdo;

    // Remove users current menu
    $queryDelete = $pdo->prepare('DELETE FROM menu WHERE LOGINID = :loginid');
    $queryDelete->bindValue('loginid', $to);
    $queryDelete->execute();

    //Stap 1
    $query = $pdo->prepare('SELECT * FROM menu WHERE LOGINID = :loginid;');
    $query->bindValue('loginid', $from);
    $query->execute();

    foreach ($query->fetchAll() as $data) {
        $naamVader = "";
        if ($default == "Ja") {
            //Als het geen nummer is, maar een naam..
            $naamVader = $data["NIVO2"];
        } else {
            $queryMenu = $pdo->prepare('SELECT * FROM menu WHERE ID = :id limit 1');
            $queryMenu->bindValue('id', $data["NIVO2"]);
            $queryMenu->execute();
            $data_m = $queryMenu->fetch(PDO::FETCH_ASSOC);

            if ($data_m !== false) {
                $naamVader = $data_m["EIGENNAAM"];
            }
        }
        $menuitem = "";
        if ($data["MENUITEMID"] != "0") {
            $menuitem = $data["MENUITEMID"];
        }

        //Maak nieuwe gebruiker aan..
        $sql = "";
        $queryMenu = $pdo->prepare('insert into menu SET
                                      NIVO2       = :nivo2,
                                      MENUITEMID  = :menuitemid,
                                      EIGENNAAM   = :eigennaam,
                                      EN          = :en,
                                      EIGENLINK   = :eigenlink,
                                      PLAATJE     = :plaatje,
                                      ROLLOVER    = :rollover,
                                      VOLGORDE    = :volgorde,
                                      LOGINID     = :loginid,
                                      BEDRIJFSID  = :bedrijfsid;');
        $queryMenu->bindValue('nivo2', $naamVader);
        $queryMenu->bindValue('menuitemid', $menuitem);
        $queryMenu->bindValue('eigennaam', $data['EIGENNAAM']);
        $queryMenu->bindValue('en', $data['EN']);
        $queryMenu->bindValue('eigenlink', $data['EIGENLINK']);
        $queryMenu->bindValue('plaatje', $data['PLAATJE']);
        $queryMenu->bindValue('rollover', $data['ROLLOVER']);
        $queryMenu->bindValue('volgorde', $data['VOLGORDE']);
        $queryMenu->bindValue('loginid', $to);
        $queryMenu->bindValue('bedrijfsid', $bedrijfsid);
        $queryMenu->execute();

    }

    //Stap 2
    $queryLogin = $pdo->prepare('SELECT * FROM menu WHERE LOGINID = :loginid AND EIGENLINK = "-map-";');
    $queryLogin->bindValue('loginid', $to);
    $queryLogin->execute();

    foreach ($queryLogin->fetchAll() as $data) {
        $query = $pdo->prepare('UPDATE menu SET NIVO2 = :nivo2 WHERE LOGINID = :loginid AND NIVO2 = :nivo2b;');
        $query->bindValue('nivo2', $data["ID"]);
        $query->bindValue('loginid', $to);
        $query->bindValue('nivo2b', $data["EIGENNAAM"]);
        $query->execute();

    }
}

function users_samenvoegen($userid, $bedrijfsid)
{
    global $pdo;

    $contentFrame = "";

    $oudId = 0;

    if (isset($_REQUEST["USERID"])) {
        $oudId = $_REQUEST["USERID"];
    }

    $newId = 0;
    if (isset($_REQUEST["USERID2"])) {
        $newId = $_REQUEST["USERID2"];
    }

    if (isset($_REQUEST["OPSLAAN"])) {
        $array = array(
            "00oft_rechten_tot" => "USERID",
            "00oft_relatiebeheerder" => "USERID",
            "00oft_management" => "USERID",
            "00oft_proxyholders" => "USERID",
            //"00oft_boardpack_invitees" => "USERID",
            "00oft_trainingsschema" => "FORWHO",
            //"00oft_boardpack_items" => "USERID",
        );

        if ($oudId > 0 && $newId > 0 && $oudId != $newId) {
            foreach ($array as $tabel => $kolom) {
                $tabel = substr($tabel, 2, strlen($tabel));
                $query = $pdo->prepare('update ' . $tabel . ' set ' . $kolom . ' = :newid WHERE ($kolom*1) = :oudid;');
                $query->bindValue('newid', $newId);
                $query->bindValue('oudid', $oudId);
                $query->execute();
            }
        }

        $queryLogin = $pdo->prepare('update login set DELETED = "Ja", SUSPEND = "Ja" where PERSONEELSLID = :personeelslid limit 3;');
        $queryLogin->bindValue('personeelslid', $oudId);
        $queryLogin->execute();

        auditaction($userid, $bedrijfsid, 'personeel', $oudId, 'Delete');
        $queryPersoneel = $pdo->prepare('update personeel set DELETED = "Ja" where ID = :id limit 1;');
        $queryPersoneel->bindValue('id', $oudId);
        $queryPersoneel->execute();

        watisergebeurtEdit("User $oudId is deleted and is now: $newId", $bedrijfsid, "REMOVE", "personeel", $oudId);

        echo "<h3>" . tl("Combine users is done") . "</h3>";
        exit;
    }

    $titel = "personeel";
    $submenuitems = '';
    $back_button = oft_back_button_persons();

    $contentFrame = "<h2>Combine users</h2>";

    $contentFrame .= "<form method=\"Post\" action=\"content.php?SITE=users_samenvoegen\">
        <div align=\"left\">
        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"tabel\">
        <tr><td>" . tl("User 1") . "</td><td><input type=\"text\" name=\"USERID\" value=\"\" class=\"field\" /></td></tr>
        <tr><td>" . tl("Should be user") . "</td><td><input type=\"text\" name=\"USERID2\" value=\"\" class=\"field\" /></td></tr>
        <tr><td align=\"right\" colspan=\"2\"><input type=\"submit\" name=\"OPSLAAN\" value=\"Opslaan\" class=\"button2\" /></td></tr>
        </table>
        </div>
        </form>";

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_persons_account($userid, $bedrijfsid)
{
    //TODO
}
