<?php
sercurityCheck();

function oft_check_shares_are_pledged($bedrijfsid) {
  global $db;
  
  //TODO: check inbouwen
}

function getWizzardValue($typeform, $bedrijfsid, $kolom, $includefinal = false) {
  global $pdo;
  
  $return = "";
  $tableData = getTableData('oft_wizzard');
      
  if(!isset($tableData[$kolom])) {
    return;
  }

  $query = $pdo->prepare('SELECT `$kolom`
            FROM oft_wizzard
           WHERE BEDRIJFSID = :bedrijfsid
             AND TYPEFORM = :typeform');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->bindValue('typeform', $typeform);
  
  if (!$includefinal) {
    $query = $pdo->prepare('SELECT `' . $kolom . '`
            FROM oft_wizzard
           WHERE BEDRIJFSID = :bedrijfsid
            AND TYPEFORM = :typeform
            AND NOT STATUSFORM = "final";');
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->bindValue('typeform', $typeform);
  }
  
  $query->execute();
  $data = $query->fetch(PDO::FETCH_ASSOC);

  if($data !== false) {
    $return = lb($data[$kolom]);
  }
  
  return $return;
}

function oft_copy_wizzard_documents($userid, $bedrijfsid, $typeform, $kolom, $filename, $templateFile = '', $includefinal = false) {
  global $pdo;
  
  $tableData = getTableData('oft_wizzard');
  if(!isset($tableData[$kolom])) {
    return;
  }

  $query = $pdo->prepare('SELECT `$kolom` As DOC
                        FROM oft_wizzard 
                       WHERE BEDRIJFSID = :bedrijfsid 
                         AND TYPEFORM = :typeform
                         AND NOT STATUSFORM = "final";');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->bindValue('typeform', $typeform);
  $query->execute();
  $data = $query->fetch(PDO::FETCH_ASSOC);

  if($data !== false) {
    $DOCTYPE = "Board resulution";
    $TABEL = "bedrijf";
    $TABELID = $bedrijfsid;
    $bestand_type = "application/msword";
    if($templateFile != '' && isset($_REQUEST["BEDRIJFSID"])) {
      $template = "";
      if(file_exists($templateFile)) {
        $template  = file_get_contents($templateFile);
      }
      $bestand_content = oft_wizzard_create_doc($userid, $bedrijfsid, $template, $templateFile, $fields);
    } else {
      $bestand_content = $data["DOC"];
    }
    $bestand_size = strlen($bestand_content);
    $JAAR = $data["YEAR"];
    if($JAAR == '') { 
      $JAAR = date("Y");
    }
    $SQL_doc_insert = "";
    $query = $pdo->prepare('insert into documentbeheer set DELETED   = "Nee",
                                                      CATAGORIEID    = "0",
                                                      UPDATEDATUM    = "' . date("Y-m-d") . '",
                                                      DOCTYPE        = :doctype,
                                                      TABEL          = :tabel,
                                                      TABELID        = :tabelid,
                                                      NAAM           = :naam,
                                                      PERSONEELSLID  = :personeelslid,
                                                      BESTAND        = :bestand,
                                                      BESTANDSNAAM   = :bestandsnaam,
                                                      BESTANDTYPE    = :bestandstype,
                                                      BESTANDSIZE    = :bestandsize,
                                                      BESTANDCONTENT = :bestandcontent,
                                                      VERSIENR       = "1",
                                                      BEKEKEN        = "0",
                                                      JAAR           = :jaar,
                                                      DATUM          = "' . date("Y-m-d") . '",
                                                      BEDRIJFSID     = :bedrijfsid;');
    $query->bindValue('doctype', $DOCTYPE);
    $query->bindValue('tabel', $TABEL);
    $query->bindValue('tabelid', $TABELID);
    $query->bindValue('naam', $filename);
    $query->bindValue('personeelslid', $userid);
    $query->bindValue('bestand', $filename);
    $query->bindValue('bestandsnaam', $filename);
    $query->bindValue('bestandstype', $bestand_type);
    $query->bindValue('bestandsize', $bestand_size);
    $query->bindValue('bestandcontent', $bestand_content);
    $query->bindValue('jaar', $JAAR);
    $query->bindValue('bedrijfsid', $bedrijfsid);
    
    return $query->execute();
  }
}

function oft_save_wizzard_documents($userid, $bedrijfsid, $templateFile, $typeform, $kolom, $fields) {
  global $pdo;
  
  $tableData = getTableData('oft_wizzard');
  if(!isset($tableData[$kolom])) {
    return;
  }
  
  $query = $pdo->prepare('SELECT * FROM oft_wizzard
                       WHERE BEDRIJFSID = :bedrijfsid
                         AND TYPEFORM = :typeform
                         AND NOT STATUSFORM = "final";');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->bindValue('typeform', $typeform);
  $query->execute();
  $data = $query->fetch(PDO::FETCH_ASSOC);

  if(!$data) {
    // standaard onderdelen bij elkaar zoeken
    $template  = false;
    if (is_array($templateFile)) {
      foreach($templateFile as $part) {
        $tpl_part = "sjablonen/oft_wizzard/".$part;
        if(file_exists($tpl_part)) $template  .= file_get_contents($tpl_part);
        else die ($tpl_part . " not found");
      }
    }
    // standaar documment
    elseif(file_exists($templateFile)) {
      $template  = file_get_contents($templateFile);
    }
    else die("template file not found; ".$templateFile);

    $content = oft_wizzard_create_doc($userid, $data["BEDRIJFSID"], $template, $templateFile, $fields);

    /*/ bestendsnaam
    $bestandsnaam = substr(strrchr($templateFile, "/"), 1);
    $bestandsnaam = str_replace('.txt', '.docx', $bestandsnaam);
    $bestandsnaam = bestandsnaamcheck($bestandsnaam);*/

    // geen ps om content toe te voegen... alles moet exact blijven staan
    $query = $pdo->prepare('UPDATE oft_wizzard SET ' . $kolom . ' = :content WHERE ID = :id;');
    $query->bindValue('content', $content);
    $query->bindValue('id', $data["ID"]);
    
    return $query->execute();
  }
  
  return false;
}

function oft_wizzard_create_doc($userid, $bedrijfsid, $template, $templateFile, $fields) {
  global $pdo;

    // vervangingen
  $find =     array("\n", "'",      "`",      "’"); //      ,"\"",
  $replace =  array("", "&#39;", "&lquot;","&rquot;" ); // ,"&dquot;"

  $template = str_replace($find, $replace, $template);

  // Bedrijfsgegevens ophalen
  $query = $pdo->prepare('SELECT
    bedrijf.BEDRIJFSNAAM,
    bedrijf.LAND,
    bedrijf.ENDOFBOOKYEAR,
    oft_management.USERID,
    oft_management.ADMINID,
    (SELECT CONCAT (ADRES, ", ", POSTCODE, ", ", PLAATS, ", ", LAND)
        FROM oft_bedrijf_adres
        WHERE BEDRIJFSID = :bedrijfsid1
          AND TYPE = "registered")
      as REGISTEREDADDRESS,
    (SELECT CONCAT (ADRES, ", ", POSTCODE, ", ", PLAATS, ", ", LAND)
        FROM oft_bedrijf_adres
        WHERE BEDRIJFSID = :bedrijfsid2
          AND TYPE = "business")
      as OFFICEADDRESS,
      oft_classes.ISSUEDQUANTITY,
      oft_classes.PARVALUE,
      oft_classes.CURRENCY
    FROM oft_management
      LEFT JOIN bedrijf on oft_management.BEDRIJFSID = bedrijf.ID
      LEFT JOIN oft_classes on oft_classes.BEDRIJFSID = oft_management.BEDRIJFSID
    WHERE oft_management.BEDRIJFSID = :bedrijfsid3
      AND oft_management.DELETED != "Ja"
      AND (oft_management.ENDATE IS NULL OR oft_management.ENDATE = "0000-00-00" OR oft_management.ENDATE > "' . date('Y-m-d') . '")');
  $query->bindValue('bedrijfsid1', $bedrijfsid);
  $query->bindValue('bedrijfsid2', $bedrijfsid);
  $query->bindValue('bedrijfsid3', $bedrijfsid);
  $query->execute();

  // general fields
  $fields["civil code reference"] = "<i>(In accordance with article 2:227 paragraph 7 Dutch Civil Code the directors and the supervisory board have an advisory vote in the general meeting and should be consulted to cast their advice.) </i>";

  // corporate fields
  $fields["this year"] = date("Y");
  //$fields["managing director list"] = "<ol type=\"1\">";
  $fields["managing director list"] = false;
  $fields["shareholders list"] = false;
  $fields["name managing director"] = "";
  $fields["name shareholder"] = "";


  foreach ($query->fetchAll() as $dBedrijf) {
    $fields["name entity"] = $dBedrijf["BEDRIJFSNAAM"] ? $dBedrijf["BEDRIJFSNAAM"] : "-unknown-";

    // begin en eind van boekjaar
    if ($fields["financial year"]) {
      $endofyear = $fields["financial year"] . "-" . substr($dBedrijf["ENDOFBOOKYEAR"], 5);
      $daysinyear = date("z", mktime(0,0,0,12,31,$fields["financial year"]));
      $timeStamp = StrToTime($endofyear);
      $startofyear = date("Y-m-d", StrToTime('-' . $daysinyear . ' days', $timeStamp));
      $fields["start financial year"] = oft_document_date_format($startofyear);
      $fields["end financial year"] = oft_document_date_format($endofyear);
    }

    $registered_address = str_replace(", , ", ", ", $dBedrijf["REGISTEREDADDRESS"]);
    $office_address = str_replace(", , ", ", ", $dBedrijf["OFFICEADDRESS"]);

    $fields["office address"] = $office_address ? $office_address : ($registered_address ? $registered_address : "-unknown-");
    $fields["corporate seat"] = $registered_address ? $registered_address : ($office_address ? $office_address : "-unknown-");

    $fields["address"] = $fields["corporate seat"]? $fields["corporate seat"] : $fields["office address"];
    if (($fields["registered address"] && $fields["office address"]) && ($fields["registered address"] != $fields["office address"])) {
      $fields["address"] .= " and its office address at " .$fields["office address"];
    }

    $fields["country of incorporation"] = $dBedrijf["LAND"] ? $dBedrijf["LAND"] : "-unknown-";
    $address = null;

    if (!$dBedrijf["USERID"] && $dBedrijf["ADMINID"]) $managingDirector = getBedrijfsnaam($dBedrijf["ADMINID"]);
    elseif ($dBedrijf["USERID"]) $managingDirector = getPersoneelsLidNaam($dBedrijf["USERID"]);
    else $managingDirector = "-unknown-";
    if (!strstr($fields["name managing director"], $managingDirector)) {
      $fields["name managing director"] .= $fields["name managing director"] ? ", ".$managingDirector : $managingDirector;
      $fields["managing director list"] .= "<li>" . $managingDirector . ";</li>";
    }

    // shares
    $fields["number of issued shares"] = $dBedrijf["ISSUEDQUANTITY"] ? $dBedrijf["ISSUEDQUANTITY"] : "-unknown-";
    $fields["nominal value"] = $dBedrijf["PARVALUE"] ? $dBedrijf["PARVALUE"] : "-unknown-";
    $fields["currency"] = $dBedrijf["CURRENCY"] ? $dBedrijf["CURRENCY"] : "EUR";

    // add shareholders list
    $shareholder = false;
    $fields["sole shareholder"] = "sole shareholder";
    $fields["name shareholder"] = false;
    $fields["shareholders list"] = false;
  } // close while bedrijf

  //@FinanceTODO()
  //percentages and dividend amounts
  // Shareholders gegevens ophalen
  // omdat een bedrijf meerdere adressen (office/business) kan hebben kunnen er twee rijen van 1 bedrijf opgehaald worden.
  // Daarom gegevens ordenen per bedrijf
  // Gegevens van shareholders ophalen

  $queryShareholders = $pdo->prepare('SELECT
      bedrijf.ID,
      bedrijf.BEDRIJFSNAAM,
      bedrijf.LAND,
      oft_shareholders.ADMINID,
      oft_shareholders.PERSENTAGE,
      CONCAT (oft_bedrijf_adres.ADRES, ", ", oft_bedrijf_adres.POSTCODE, ", ", oft_bedrijf_adres.PLAATS, ", ", oft_bedrijf_adres.LAND) as ADDRESS,
      oft_bedrijf_adres.TYPE
    FROM oft_shareholders
      LEFT JOIN bedrijf on bedrijf.ID = oft_shareholders.ADMINID
      LEFT JOIN oft_bedrijf_adres on oft_bedrijf_adres.BEDRIJFSID = oft_shareholders.ADMINID
    WHERE oft_shareholders.ADMINIDFROM = :bedrijfsid
      AND oft_bedrijf_adres.TYPE = "registered"
      AND oft_shareholders.DELETED != "Ja"');
  $queryShareholders->bindValue('bedrijfsid', $bedrijfsid);
  $queryShareholders->execute();

  $sData = array();
  foreach ($queryShareholders->fetchAll() as $dShare) {
    $sData[$dShare["ID"]]['name shareholder'] = $dShare["BEDRIJFSNAAM"]; // !!! Kan ook een persoon zijn? hoe dan ophalen?
    $sData[$dShare["ID"]]['country'] = $dShare["LAND"];
    $sData[$dShare["ID"]]['registered address'] = $dShare["TYPE"] == "Registered" ? str_replace(", , ", ", ", $dShare["ADDRESS"]) : false;
    $sData[$dShare["ID"]]['office address'] = $dShare["TYPE"] == "Business" ? str_replace(", , ", ", ", $dShare["ADDRESS"]) : false;
    $sData[$dShare["ID"]]['number of shares held'] = is_numeric($fields["number of issued shares"]) ? round($fields["number of issued shares"]*$dShare["PERSENTAGE"]/100): "-unknown-";
  }

  foreach ($sData as $bedrijfsid => $s) {
    // multiple shareholders
    if ($fields["name shareholder"]) {
      $fields["sole shareholder"] = "shareholders";
      $fields["name shareholder"] .= ", " . $s['name shareholder'];
    }
    // one or first shareholder
    else $fields["name shareholder"] = $s['name shareholder'];

    $address = $s['registered address'] ? $s['registered address'] : $s['office address'];
    if (($s['registered address'] && $s['office address']) && ($s['registered address'] != $s['office address'])) {
      $address .= " and its office address at " .$s['office address'];
    }
    $fields["shareholders data"] = '
        <strong>'.$s['name shareholder'].'</strong>,
        a company organised and existing under the laws of '.$s['country'].',
        with its registered office at '.$address.',
        acting in its capacity as shareholder of '.$s['number of shares held'].' issued and paid-up shares
        (each share with a nominal value of '.$fields["currency"].' '.$fields["nominal value"].')
        in the share capital of ';
    $fields["shareholders list"] = '<li>'.$fields["shareholders data"].' the <strong>&#34;Company&#34;</strong></li>';
  }

  // geen shareholders gevonden
  if (!$fields["name shareholder"]) {
    $fields["name shareholder"] = " -unknown- ";
    $fields["shareholders list"] = "<li> - unknown - </li>";
  }

  //print_r($fields);
  //die($query);

  // velden vervangen
  foreach ($fields as $key =>$field) {
    $template = str_replace("[".$key."]", $field, $template);
  }

  // basis css
  $template = '<p style="family:Arial, sans-serif">' . trim($template) . '</p>';
  return $template;
}

function oft_save_wizzard_step($userid, $bedrijfsid, $typeform, $nr, $fields) {
  global $pdo;
  
  $sql = "";
  foreach($fields As $kolom => $type) {
    if($kolom == 'ENTITYLIST') {
      $ENTETIYLIST = '';
      $queryBedrijf = $pdo->prepare('SELECT bedrijf.ID
                        FROM bedrijf
                       WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
                         AND (ENDDATUM >= "' . date("Y-m-d") . '" OR ENDDATUM is null OR ENDDATUM = "0000-00-00");');
      $queryBedrijf->execute();

      foreach ($queryBedrijf->fetchAll() as $dBedrijf) {
        if(isset($_REQUEST['ENT'.($dBedrijf["ID"]*1)])) {
          $ENTETIYLIST .= ';'. ($dBedrijf["ID"]*1) . ';';
        }
      }
      
      $sql .= " `$kolom` = '".ps($ENTETIYLIST)."', ";
    } elseif($kolom == 'MEMBERLIST') {
      $MEMBERLIST = '';
      $queryPersoneel = $pdo->prepare('SELECT ID
                       FROM personeel
                      WHERE (NOT personeel.DELETED = "Ja" OR personeel.DELETED is null);');
      $queryPersoneel->execute();

      foreach ($queryPersoneel->fetchAll() as $dPersoneel) {
        if(isset($_REQUEST['MEM'.($dPersoneel["ID"]*1)])) {
          $MEMBERLIST .= ';'. ($dPersoneel["ID"]*1) . ';';
        }
      }
      
      $sql .= " `$kolom` = '".ps($MEMBERLIST)."', ";
    } elseif($kolom == 'NEWMEMBERLIST') {
      $NEWMEMBERLIST = $fields['NEWMEMBERLIST'];
      
      for($teller = 0; $teller <= 10; $teller++) {
        if(isset($_REQUEST['NEWMEMBER_'.$teller]) && ($_REQUEST['NEWMEMBER_'.$teller]*1) > 0) {
          $NEWMEMBERLIST .= ';'. ($_REQUEST['NEWMEMBER_'.$teller]*1) . ';';
        }
      }

      $sql .= " `$kolom` = '".ps($NEWMEMBERLIST)."', ";
    } elseif(isset($_REQUEST[$kolom])) {
      $sql .= " `$kolom` = '".ps($_REQUEST[$kolom], $type)."', ";
    }
  }
  
  if($sql != '') {
    $sql = ", " . substr($sql, 0, (strlen($sql)-2));
  }
  
  $queryWizzard = $pdo->prepare('SELECT *
                        FROM oft_wizzard 
                       WHERE BEDRIJFSID = :bedrijfsid
                         AND TYPEFORM = :typeform
                         AND NOT STATUSFORM = "final"');
  $queryWizzard->bindValue('bedrijfsid', $bedrijfsid);
  $queryWizzard->bindValue('typeform', $typeform);
  $queryWizzard->execute();
  $data = $queryWizzard->fetch(PDO::FETCH_ASSOC);

  if($data !== false) {
    //Update
    $sql = "";
    $query = $pdo->prepare('UPDATE oft_wizzard
               SET UPDATEDATUM = "' . date("Y-m-d") . '", 
                   STATUSFORM = :statusform
                   ' . $sql . '
             WHERE ID = :id;');
    $query->bindValue('statusform', $nr);
    $query->bindValue('id', $data["ID"]);

  } else {
    //Insert
    $query = $pdo->prepare('INSERT oft_wizzard
               SET TYPEFORM = :typeform,
                   USERID = :userid,
                   BEDRIJFSID = :bedrijfsid,
                   UPDATEDATUM = "' . date("Y-m-d") . '",
                   STATUSFORM = :statusform,
                   DELETED = "Nee" 
                   ' . $sql);
    $query->bindValue('typeform', $typeform);
    $query->bindValue('userid', $userid);
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->bindValue('statusform', $nr);
  }
  
  return $query->execute();
}

function oft_wizzard($userid, $bedrijfsid, $type) {
  global $db;

  $titel           = '';
  $MENUID          = getMenuItem();
  $submenuitems    = "";
  $back_button     = "";
  $contentFrame    = "";

  if($type == "oft_dividend_distribution") {
    $titel = 'Dividend distribution';
    if(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '2') {
      $contentFrame .= oft_dividend_distribution_stap_dividend($userid, $bedrijfsid, $type);
    } elseif(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '3') {
      $contentFrame .= oft_dividend_distribution_stap_article($userid, $bedrijfsid, $type);
    } elseif(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '4') {
      $contentFrame .= oft_dividend_distribution_stap_documents($userid, $bedrijfsid, $type);
    } else {
      $contentFrame .= oft_dividend_distribution_stap_checklist($userid, $bedrijfsid, $type);
    }
  } elseif($type == "oft_approval_transaction") {
    $titel = 'Approval transaction';
    if(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '2') {
      $contentFrame .= oft_approval_transaction_stap_article($userid, $bedrijfsid, $type);
    } elseif(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '3') {
      $contentFrame .= oft_approval_transaction_stap_documents($userid, $bedrijfsid, $type);
    } else {
      $contentFrame .= oft_approval_transaction_stap_checklist($userid, $bedrijfsid, $type);
    }
  } elseif($type == "oft_wizzard_agm") {
    $titel = 'Annual general meeting';
    if(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '2') {
      $contentFrame .= oft_wizzard_agm_documents($userid, $bedrijfsid, $type);
    } else {
      $contentFrame .= oft_wizzard_agm_checklist($userid, $bedrijfsid, $type);
    }
  } elseif($type == "oft_wizzard_board_change") {
    $titel = 'Board change';

    if(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '2') {
      $contentFrame .= oft_wizzard_board_change_article($userid, $bedrijfsid, $type);
    } elseif(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '3') {
      $contentFrame .= oft_wizzard_board_change_members($userid, $bedrijfsid, $type);
    } elseif(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '4') {
      $contentFrame .= oft_wizzard_board_change_documents($userid, $bedrijfsid, $type);
    } else {
      $contentFrame .= oft_wizzard_board_change_entities($userid, $bedrijfsid, $type);
    }

  } elseif($type == "oft_proxyholder_change") {
    $titel = 'Proxyholder change';
    
    if(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '2') {
      $contentFrame .= oft_wizzard_proxyholder_change_article($userid, $bedrijfsid, $type);
    } elseif(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '3') {
      $contentFrame .= oft_wizzard_proxyholder_change_members($userid, $bedrijfsid, $type);
    } elseif(isset($_REQUEST["STAP"]) && $_REQUEST["STAP"] == '4') {
      $contentFrame .= oft_wizzard_proxyholder_change_documents($userid, $bedrijfsid, $type);
    } else {
      $contentFrame .= oft_wizzard_proxyholder_change_entities($userid, $bedrijfsid, $type);
    }
  }
  
  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}
