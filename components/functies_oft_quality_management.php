<?php
sercurityCheck();

function oft_quality_management($userid, $bedrijfsid)
{
    global $db;

    $titel = "Quality Management";
    $submenuitems = "<a href=content.php?SITE=oft_quality_management&TYPE=edit>Create new</a>";

    $back_button = "";
    $baseURL = "content.php?SITE=oft_quality_management";

    $contentFrame = "<link rel=\"stylesheet\" href=\"./templates/bootstrap.css\"><div class='bootstrap-400'>";
    if (isset($_REQUEST['TYPE'])) {
        $id = 0;
        if (isset($_REQUEST['ID'])) {
            $id = $_REQUEST['ID'];
        }
        if ($_REQUEST['TYPE'] == 'edit') {
            $info = getInfo($id);
            $submenuitems .= "<a href=content.php?SITE=oft_quality_management&TYPE=destroy&ID=$id>Delete this ...</a>";
            $contentFrame .= oft_quality_management_edit($baseURL, $id, $info);
        } elseif ($_REQUEST['TYPE'] == 'store') {
            oft_quality_management_store($baseURL, $id);
        } elseif ($_REQUEST['TYPE'] == 'destroy') {
            oft_quality_management_destroy($baseURL, $id);
        } elseif ($_REQUEST['TYPE'] == 'show') {
            $submenuitems .= "<a href=content.php?SITE=oft_quality_management&TYPE=edit&ID=$id>Edit this ...</a>";
            $submenuitems .= "<a href=content.php?SITE=oft_quality_management&TYPE=destroy&ID=$id>Delete this ...</a>";
            $contentFrame .= oft_quality_management_show($baseURL, $id);
        } else {
            $contentFrame .= oft_quality_management_view($baseURL);
        }
    } else {
        $contentFrame .= oft_quality_management_view($baseURL);
    }

    $contentFrame .= "</div>";
    $contentFrame = "<div class='oft2_page_centering' style=\"padding-top: 50px;\">$contentFrame</div>";

    $submenuitems .= "<a href=content.php?SITE=oft_quality_management>Show all</a>";
    echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_quality_management_view($baseURL = '')
{
    // overview for all claims ordered on einddatum most recent up top showing: project-locatie-afwijkingsnummer-einddatum
    global $db;
    $html = "";
    $result = getInfo();
    $table = "<table class='table table-dark'>";
    $table .= "<tr><th>Project</th><th>Branch / Company name</th><th>Deviation nr.</th><th>End date</th><th></th></tr>";
    while ($row = db_fetch_array($result)) {
        $color = "transparent";
        if($row['deviation_lifted'] == "No" || $row['deviation_lifted'] == "Nee") {
            $date = $row['end_date'];
            dpm($date);
            if($date == '') {
                $date = "1970-01-01";
            }
            $date7 = date('Y-m-d', strtotime("+7 day"));
            $date14 = date('Y-m-d',strtotime("+14 day"));
            if($date < $date7) {
                $color = "red";
            }
            elseif($date < $date14) {
                $color = "orange";
            }
        }

        $table .= "<tr onclick=\"document.location = '" . $baseURL . "&TYPE=show&ID=" . $row['id'] . "';\" style='background-color: $color; cursor: pointer;'>
                    <td>" . $row['project'] . "</td>
                    <td>" . $row['location'] . "</td>
                    <td>" . $row['deviationnr'] . "</td>
                    <td>" . date('d-m-Y', strtotime($row['end_date'])) . "</td>
                    <td>
                    <a href='" . $baseURL . "&TYPE=edit&ID=" . $row['id'] . "'><i class=\"fas fa-edit\" style='float: left;'></i></a>
                    <a href='" . $baseURL . "&TYPE=destroy&ID=" . $row['id'] . "'><i class=\"fas fa-trash-alt\" style='padding-left: 20px;'></i></a>
                    </td>
                   </tr>";
    }
    $table .= "</table>";
    $html .= $table;
    return $html;
}

function oft_quality_management_edit($baseURL = '', $id, $info = array())
{
    // create / edit page
    $yesNoArray = array("Yes" => "Yes", "No" => "No");

    $info = isset($info) ? db_fetch_array($info) : array();
    $project = isset($info['project']) ? $info['project'] : '';
    $location = isset($info['location']) ? $info['location'] : '';
    $dates_rating = isset($info['dates_rating']) ? $info['dates_rating'] : '';
    $deviationnr = isset($info['deviationnr']) ? $info['deviationnr'] : '';
    $treatmentround = isset($info['treatmentround']) ? $info['treatmentround'] : '';
    $scope = isset($info['scope']) ? $info['scope'] : '';
    $ratings_element = isset($info['ratingselement']) ? $info['ratingselement'] : '';
    $accreditation_requirements = isset($info['accreditation_requirements']) ? $info['accreditation_requirements'] : '';
    $cancel_term = isset($info['cancel_term']) ? $info['cancel_term'] : '';
    $deviation = isset($info['deviation']) ? $info['deviation'] : '';
    $cause = isset($info['cause']) ? $info['cause'] : '';
    $magnitude = isset($info['magnitude']) ? $info['magnitude'] : '';
    $solution = isset($info['solution']) ? $info['solution'] : '';
    $operationality = isset($info['operationality']) ? $info['operationality'] : '';
    $teamleader = isset($info['teamleader']) ? $info['teamleader'] : '';
    $deviation_lifted = isset($info['deviation_lifted']) ? $info['deviation_lifted'] : '';
    $q_continued = isset($info['q_continued']) ? $info['q_continued'] : '';
    $enddate = isset($info['end_date']) ? $info['end_date'] : '1970-01-01';

    $title = empty($info) ? 'Add new' : 'Edit ' . $project;

    $html = "<script src=\" ./libraries/ckeditor/ckeditor.js\"></script>";
    $html .= "<div class='row'><div class='col-md-5'>";
    $html .= "<h4>$title</h4>";
    $html .= "<form method='post' action='" . $baseURL . "&TYPE=store&ID=" . $id . "'>";
    $html .= "<input value='$id' type='hidden'>";
    $html .= "<div class=\"form-group\">
                <label for=\"project\">Project</label>
                <input value='$project' type=\"text\" class=\"form-control\" id=\"project\" placeholder=\"Enter project name\" name='project' required>
             </div>";
    $html .= "<div class=\"form-group\">
                <label for=\"location\">Branch / Company name</label>
                <input value='$location' type=\"text\" class=\"form-control\" id=\"location\" placeholder=\"Enter branch or company name\" name='location'>
             </div>";
    $html .= "<div class=\"form-group\">
                <label for=\"datums_beoordeling\">Audit date(s)</label>
                <input value='$dates_rating' type=\"text\" class=\"form-control\" id=\"datums_beoordeling\" placeholder=\"Enter date(s) (dd-mm-yyyy) - comma seperated\" name='dates_rating'>
             </div>";
    $html .= "<div class=\"form-group\">
                <label for=\"deviationnumber\">Deviation Number</label>
                <input value='$deviationnr' type=\"text\" class=\"form-control\" id=\"deviationnumber\" placeholder=\"Enter number\" name='deviationnr'>
             </div>";
    $html .= "<div class=\"form-group\">
                <label for=\"treatmentround\">First or second round</label>
                <select class='form-control' id='treatmentround' name='treatmentround'>";
    for ($i = 1; $i <= 2; $i++) {
        $selected = $treatmentround == $i ? 'selected' : '';
        $html .= "<option value='$i' $selected>Round $i</option>";
    }
    $html .= "  </select>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"scope\">Scope</label>
                <input value='$scope' type=\"text\" class=\"form-control\" id=\"scope\" placeholder=\"Enter scope\" name='scope'>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"ratings_element\">Concerns</label>
                <textarea id='editor1' name='ratingselement' placeholder='Enter text' class='ckeditor'>$ratings_element</textarea>
             </div>";

    $html .= "<div class=\"form-group\">
                    <label for=\"accreditation_requirements\">Accreditation requirements</label>
                <input value='$accreditation_requirements' type=\"text\" class=\"form-control\" id=\"accreditation_requirements\" placeholder=\"Enter code\" name='accreditation_requirements'>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"cancel_term\">Solution lead time <small>(in months)</small></label>
                <input value='$cancel_term' type=\"number\" class=\"form-control\" id=\"cancel_term\" placeholder=\"Enter cancel term (in months)\" name='cancel_term'>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"deviation\">Deviation</label>
                <textarea id='editor2' name='deviation' placeholder='Enter text' class='ckeditor'>$deviation</textarea>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"cause\">Cause</label>
                <textarea id='editor3' name='cause' placeholder='Enter text' class='ckeditor'>$cause</textarea>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"magnitude\">Magnitude</label>
                <textarea id='editor4' name='magnitude' placeholder='Enter text' class='ckeditor'>$magnitude</textarea>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"solution\">Solution</label>
                <textarea id='editor5' name='solution' placeholder='Enter text' class='ckeditor'>$solution</textarea>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"operationality\">Operationality</label>
                <textarea id='editor6' name='operationality' placeholder='Enter text' class='ckeditor'>$operationality</textarea>
             </div>";

    // misschien select hier?? @TODO
    $html .= "<div class=\"form-group\">
                <label for=\"teamleader\">Teamleader</label>
                <input value='$teamleader' type=\"text\" class=\"form-control\" id=\"teamleader\" placeholder=\"Enter teamleader\" name='teamleader'>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"deviation_lifted\">Deviation lifted?</label>
                <select class='form-control' id='deviation_lifted' name='deviation_lifted'>";
    foreach ($yesNoArray as $key => $value) {
        $selected = $deviation_lifted == $key ? 'selected' : '';
        $html .= "<option value='$key' $selected>$value</option>";
    }
    $html .= "  </select>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"q_continued\">Q continued?</label>
                <select class='form-control' id='q_continued' name='q_continued'>";
    foreach ($yesNoArray as $key => $value) {
        $selected = $q_continued == $key ? 'selected' : '';
        $html .= "<option value='$key' $selected>$value</option>";
    }
    $html .= "</select>
             </div>";

    $html .= "<div class=\"form-group\">
                <label for=\"enddate\">End date</label>
                <input value='$enddate' type=\"date\" class=\"form-control\" id=\"enddate\" placeholder=\"Enter enddate\" name='enddate'>
             </div>";

    $html .= "<input class='form-control' type='submit' name='submit_form' id='submit_form' value='Submit'>";

    $html .= "<br /><br />";
    $html .= "</form>";
    $html .= "</div></div>";

    $html .= "<script>
                CKEDITOR.replace('editor1');
                CKEDITOR.replace('editor2');
                CKEDITOR.replace('editor3');
                CKEDITOR.replace('editor4');
                CKEDITOR.replace('editor5');
                CKEDITOR.replace('editor6');
              </script>";
    return $html;

}

function oft_quality_management_store($baseURL = '', $id)
{
    global $db;
    if (isset($_REQUEST['SITE']) && $_REQUEST['SITE'] == "oft_quality_management" && isset($_REQUEST['TYPE']) && $_REQUEST['TYPE'] == "store") {
        $project = isset($_REQUEST['project']) ? $_REQUEST['project'] : '';
        $location = isset($_REQUEST['location']) ? $_REQUEST['location'] : '';
        $dates_rating = isset($_REQUEST['dates_rating']) ? $_REQUEST['dates_rating'] : '';
        $deviationnr = isset($_REQUEST['deviationnr']) ? $_REQUEST['deviationnr'] : '';
        $treatmentround = isset($_REQUEST['treatmentround']) ? $_REQUEST['treatmentround'] : 0;
        $scope = isset($_REQUEST['scope']) ? $_REQUEST['scope'] : '';
        $ratingselement = isset($_REQUEST['ratingselement']) ? $_REQUEST['ratingselement'] : '';
        $accreditation_requirements = isset($_REQUEST['accreditation_requirements']) ? $_REQUEST['accreditation_requirements'] : '';
        $cancel_term = isset($_REQUEST['cancel_term']) ? $_REQUEST['cancel_term'] : 0;
        $deviation = isset($_REQUEST['deviation']) ? $_REQUEST['deviation'] : '';
        $cause = isset($_REQUEST['cause']) ? $_REQUEST['cause'] : '';
        $magnitude = isset($_REQUEST['magnitude']) ? $_REQUEST['magnitude'] : '';
        $solution = isset($_REQUEST['solution']) ? $_REQUEST['solution'] : '';
        $operationality = isset($_REQUEST['operationality']) ? $_REQUEST['operationality'] : '';
        $teamleader = isset($_REQUEST['teamleader']) ? $_REQUEST['teamleader'] : '';
        $deviation_lifted = isset($_REQUEST['deviation_lifted']) ? $_REQUEST['deviation_lifted'] : '';
        $q_continued = isset($_REQUEST['q_continued']) ? $_REQUEST['q_continued'] : '';
        $enddate = isset($_REQUEST['enddate']) ? $_REQUEST['enddate'] : '';
        $enddate = date('Y-m-d', strtotime($enddate));
        $deleted = 0;

        if (isset($id) && $id != "0") {
            $sql = "UPDATE oft_quality_management SET ";
            $sql .= "project = '$project', ";
            $sql .= "location = '$location', ";
            $sql .= "dates_rating = '$dates_rating', ";
            $sql .= "deviationnr = '$deviationnr', ";
            $sql .= "treatmentround = '$treatmentround', ";
            $sql .= "scope = '$scope', ";
            $sql .= "ratingselement = '$ratingselement', ";
            $sql .= "accreditation_requirements = '$accreditation_requirements', ";
            $sql .= "cancel_term = '$cancel_term', ";
            $sql .= "deviation = '$deviation', ";
            $sql .= "cause = '$cause', ";
            $sql .= "magnitude = '$magnitude', ";
            $sql .= "solution = '$solution', ";
            $sql .= "operationality = '$operationality', ";
            $sql .= "teamleader = '$teamleader', ";
            $sql .= "deviation_lifted = '$deviation_lifted', ";
            $sql .= "q_continued = '$q_continued', ";
            $sql .= "end_date = '$enddate', ";
            $sql .= "UPDATED_AT = NOW() ";
            $sql .= "WHERE ID = $id ";
        } else {
            $sql = "INSERT INTO oft_quality_management VALUES('', '$project', '$location', '$dates_rating', '$deviationnr', $treatmentround, '$scope', '$ratingselement',
          '$accreditation_requirements', $cancel_term, '$deviation', '$cause', '$magnitude', '$solution', '$operationality', '$teamleader', '$deviation_lifted', '$q_continued',
          '$enddate', $deleted, NOW(), NOW())";
        }

        $result = db_query($sql, $db);
        if ($result) {
            $_SESSION['success'] = "Successfully saved!";
        } else {
            $_SESSION['error'] = "Something went wrong...";
        }
    }
    header("Location: " . $baseURL);
}

function oft_quality_management_show($baseURL = '', $id)
{
    // show existing details
    $html = "";
    if ($info = db_fetch_array(getInfo($id))) {
        $array = explode(',', $info['dates_rating']);

        $html .= "<div class='row' style='padding-left: 20px; margin-top: -50px;'>";
        $html .= "<div class='col-md-12'>";
        $html .= "<h3>" . $info['project'] . "</h3>";
        $html .= "<h4>" . $info['location'] . "</h4>";
        $html .= "</div>"; // end col-md-12
        $html .= "</div>"; // end row

        $html .= "<div class='row' style='padding-left: 20px; padding-top: 65px;'>";
        $html .= "<div class='col-md-4'>";
        $html .= "<h5>Concerns</h5>";
        $html .= "<p>".$info['ratingselement']."&nbsp;</p>";
        $html .= "<h5>Deviation</h5>";
        $html .= "<p>".$info['deviation']."&nbsp;</p>";
        $html .= "<h5>Cause</h5>";
        $html .= "<p>".$info['cause']."&nbsp;</p>";
        $html .= "<h5>Magnitude</h5>";
        $html .= "<p>".$info['magnitude']."&nbsp;</p>";
        $html .= "<h5>Solution</h5>";
        $html .= "<p>".$info['solution']."&nbsp;</p>";
        $html .= "<h5>Operationality</h5>";
        $html .= "<p>".$info['operationality']."&nbsp;</p>";
        $html .= "</div>"; // end col-md-4

        $html .= "<div class='col-md-2'>";

        $html .= "</div>";
        $html .= "<div class='col-md-2'>";
        $html .= "<p>End date <small>(dd-mm-yyyy)</small></p>";
        $html .= "<p>Deviation lifted?</p>";
        $html .= "<p>Accreditation requirements</p>";
        $html .= "<p>Teamleader</p>";
        $html .= "<p>Audit date(s)</p>";
        for ($i = 1; $i < count($array); $i++) {
            $html .= "<p>&nbsp;</p>";
        }
        $html .= "<p>First or second round</p>";
        $html .= "<p>Solution lead time <small>(in months)</small></p>";
        $html .= "<p>Scope</p>";
        $html .= "<p>Deviation nr.</p>";
        $html .= "<p>Q continued</p>";
        $html .= "</div>";

        $html .= "<div class='col-md-2'>";
        $html .= "<p>" . date('d-m-Y', strtotime($info['end_date'])) . "&nbsp;</p>";
        $html .= "<p>" . $info['deviation_lifted'] . "&nbsp;</p>";
        $html .= "<p>" . $info['accreditation_requirements'] . "&nbsp;</p>";
        $html .= "<p>" . $info['teamleader'] . "&nbsp;</p>";
        foreach ($array as $date) {
            $html .= "<p>" . $date . "&nbsp;</p>";
        }
        $html .= "<p>" . $info['treatmentround'] . "&nbsp;</p>";
        $html .= "<p>" . $info['cancel_term'] . "&nbsp;</p>";
        $html .= "<p>" . $info['scope'] . "&nbsp;</p>";
        $html .= "<p>" . $info['deviationnr'] . "&nbsp;</p>";
        $html .= "<p>" . $info['q_continued'] . "&nbsp;</p>";
        $html .= "</div>"; // end col-md-2

        $html .= "<div class='col-md-6'>&nbsp;</div>";
        $html .= "</div>";

        $html .= "</div>";
    }
    return $html;
}

function oft_quality_management_destroy($baseURL = '', $id)
{
    // soft delete existing
    global $db;
    $sql = "UPDATE oft_quality_management SET `DELETED` = '1' , UPDATED_AT = NOW() WHERE id = " . $id;
    $result = db_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = "Successfully saved!";
    } else {
        $_SESSION['error'] = "Something went wrong...";
    }
    header("Location: " . $baseURL);
}

function getInfo($id = "TRUE")
{
    global $db;
    if ($id != "TRUE") {
        $id = "ID = " . $id;
    }
    $sql = "SELECT * FROM oft_quality_management WHERE " . $id . " AND DELETED = 0 ORDER BY end_date ASC";
    $result = db_query($sql, $db);

    return $result;
}