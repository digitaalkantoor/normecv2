<?php

function oft_audit_trail($user, $company)
{

    if (!checkProfile("Administrator")) {
        rd("content.php?SITE=oft_home");
    }

    if (isset($_REQUEST["ID"])) {
        $company = $_REQUEST["ID"];
    }

    $title = 'Audit trail';
    $submenuItems = '<a href="content.php?SITE=oft_audit_trail_ajax&CLEANLIST=true" onClick="return confirm(\'Are you sure?\');">Clean list</a>';

    $contentFrame = oft_audit_trail_ajax($user, $company, false);

    echo oft_framework_basic($user, $company, $contentFrame, $title, $submenuItems);
}

function oft_audit_trail_ajax($user, $company, $echo = true)
{
    global $db;

    if (isset($_REQUEST["CLEANLIST"])) {
        db_query("update audittrail set DELETED = 'Ja' WHERE APPROVED = 'Ja';", $db);
        rd("content.php?SITE=oft_audit_trail");
    }

    $table = "audittrail";
    $order = "UPDATEDATUM";

    $fields = oft_get_structure($user, $company, $table, "all");

    if ($echo) {
        echo oft_tabel_content($user, $company,
            $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
            $table, $order);
    }
    return oft_tabel($user, $company,
        $fields['arrayVelden'], $fields['arrayVeldnamen'], $fields['arrayVeldType'], $fields['arrayZoekvelden'],
        $table, $order);
}

function approved($value, $id)
{
    if ($value == "Ja") {
        return "<img src='./images/oft/oft_green_circle.png' />";
    } else {
        $divId = uniqid();
        $return = "<div id='slt" . $divId . "'>"
        . "<a class='btn-action' onClick=\"if(confirm('" . tl("Weet u het zeker?") . "')) { getPageContent('content.php?SITE=setApproved&ID=" . ($id * 1) . "', 'slt" . $divId . "'); }\">"
        . "<img src='./images/oft/oft_red_circle.png' /></a></div>";
        return $return;
    }
}

function setApproved($userid, $bedrijfsid)
{
    global $db;

    if (isset($_REQUEST["ID"])) {
        if (db_query("update audittrail set APPROVED = 'Ja', APPROVEDBYUSERID = '" . ps($userid,
                "nr") . "', APPROVEDDATE = NOW() WHERE ID = '" . ps($_REQUEST["ID"], "nr") . "';", $db)) {
            echo "<img src='./images/oft/oft_green_circle.png' />";
        }
    }
}

function setRollBack($userid, $bedrijfsid)
{
    global $pdo, $db;

    if (isset($_REQUEST["ID"])) {
        $query = $pdo->prepare('SELECT * FROM audittrail WHERE ID = :id LIMIT 1;');
        $query->bindValue('id', $_REQUEST["ID"]);
        $query->execute();
        if ($data = $query->fetch(PDO::FETCH_ASSOC)) {
            if (db_query("update " . $data["TABEL"] . " set `" . $data["KOLOM"] . "` = '" . ps($data["OLDVALUE"]) . "' WHERE ID = '" . ps($data["TABELID"],
                    "nr") . "';", $db)) {
                if (db_query("update audittrail set DELETED = 'Ja' WHERE ID = '" . ps($_REQUEST["ID"], "nr") . "';",
                    $db)) {
                    $BATCH[$data["KOLOM"]]["OLD"] = $data["NEWVALUE"];
                    $BATCH[$data["KOLOM"]]["NEW"] = $data["OLDVALUE"];
                    audittrailBatch($userid, $data["BEDRIJFSID"], $data["TABEL"], $data["TABELID"], $BATCH);
                    echo "Restored";
                }
            }
        }
    }
}

function getBetterSourceName($tabel)
{

    if ($tabel == "bedrijf") {
        $tabel = "Entities";
    } elseif ($tabel == "documentbeheer") {
        $tabel = "Documents";
    }

    $tabel = str_replace("Entities", check("default_name_of_entities", "Entities", 1), $tabel);

    return lb($tabel);
}

function audittrailBatch($USERID, $BEDRIJFSID, $TABEL, $TABELID, $BATCH)
{
    if ($TABEL != '' && count($BATCH) > 0) {
        foreach ($BATCH As $KOLOM => $VALUE) {
            if (isset($VALUE["NEW"]) && isset($VALUE["OLD"]) && $VALUE["OLD"] != $VALUE["NEW"]) {
                audittrail($USERID, $BEDRIJFSID, $TABEL, $TABELID, $KOLOM, $VALUE["OLD"], $VALUE["NEW"]);
            }
        }
    }
}

function audittrail($USERID, $BEDRIJFSID, $TABEL, $TABELID, $KOLOM, $OLDVALUE, $NEWVALUE)
{
    global $db;

    $sInsert = "insert into audittrail
                 set USERID = '" . ps($USERID, "nr") . "',
                     BEDRIJFSID = '" . ps($BEDRIJFSID, "nr") . "',
                     DELETED = 'Nee',
                     TABEL = '" . ps($TABEL) . "',
                     TABELID = '" . ps($TABELID, "nr") . "',
                     KOLOM = '" . ps($KOLOM) . "',
                     OLDVALUE = '" . ps($OLDVALUE) . "',
                     NEWVALUE = '" . ps($NEWVALUE) . "',
                     UPDATEDATUM = '" . date("Y-m-d H:i:s") . "',
                     ACTION = 'Change',
                     APPROVED = 'Nee',
                     APPROVEDBYUSERID = '0',
                     APPROVEDDATE = null;";
    if (db_query($sInsert, $db)) {
        return true;
    } else {
        return false;
    }
}

function auditaction($USERID, $BEDRIJFSID, $TABEL, $TABELID, $ACTION)
{
    global $db;

    if ($TABEL != '' && $TABELID > 0) {
        $sInsert = "insert into audittrail
                   set USERID = '" . ps($USERID, "nr") . "',
                       BEDRIJFSID = '" . ps($BEDRIJFSID, "nr") . "',
                       DELETED = 'Nee',
                       TABEL = '" . ps($TABEL) . "',
                       TABELID = '" . ps($TABELID, "nr") . "',
                       KOLOM = '',
                       OLDVALUE = '',
                       NEWVALUE = '',
                       UPDATEDATUM = '" . date("Y-m-d H:i:s") . "',
                       ACTION = '" . ps($ACTION) . "',
                       APPROVED = 'Nee',
                       APPROVEDBYUSERID = '0',
                       APPROVEDDATE = null;";
        if (db_query($sInsert, $db)) {
            return true;
        } else {
            return false;
        }
    }
}

function oft_login_list($userid, $bedrijfsid)
{
    global $db;

    if (!checkProfile("Administrator")) {
        rd("content.php?SITE=oft_home");
    }

    if (isset($_REQUEST["ID"])) {
        $bedrijfsid = $_REQUEST["ID"];
    }

    $titel = 'Login list';
    $submenuitems = '';
    $back_button = '';

    $tabel = "ingelogt";
    $arrayVelden = array(0 => "IP", 1 => "DATUM", 2 => "TIJD", 3 => "PERSONEELSLID");
    $arrayVeldnamen = array(0 => "IP", 1 => "Date", 2 => "Time", 3 => "User");
    $arrayVeldType = array(0 => "field", 1 => "field", 2 => "field", 3 => "medewerker");
    $arrayZoekvelden = array();

    $toevoegen = "";
    $bewerken = "";
    $verwijderen = "";
    $sql_toevoeging = "";

    $contentFrame = oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType,
        $arrayZoekvelden, $tabel, 'ID', $toevoegen, $bewerken, $verwijderen, $sql_toevoeging);

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_login_list_ajax($userid, $bedrijfsid)
{
    global $db;

    $tabel = "ingelogt";
    $arrayVelden = array(0 => "IP", 1 => "DATUM", 2 => "TIJD", 3 => "PERSONEELSLID");
    $arrayVeldnamen = array(0 => "IP", 1 => "Date", 2 => "Time", 3 => "User");
    $arrayVeldType = array(0 => "field", 1 => "field", 2 => "field", 3 => "medewerker");
    $arrayZoekvelden = array();

    $toevoegen = "";
    $bewerken = "";
    $verwijderen = "";
    $sql_toevoeging = "";

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden,
        $tabel, 'ID', $toevoegen, $bewerken, $verwijderen);
}
