<?php
sercurityCheck();

function usa2eu_home($userid, $bedrijfsid) {
  global $pdo;

  //$_REQUEST["SRCH_YEAR"] = "x".date("Y");
  //$_REQUEST["SRCH_JAAR"] = date("Y");

  $titel        = 'Home';
  $submenuitems = "<a href=\"/content.php?SITE=oft_home&MENUID=1&EXPORTEXCEL=true&PAGNR=-1\">Export to excel</a>";
  if(check('DIF', 'Nee', $bedrijfsid) == 'Ja') { $submenuitems .= " | <a href=\"content.php?SITE=import_entities\">Import excel</a>"; }
  if(isRechtenOFT($bedrijfsid, "CRM"))        { $submenuitems .= " | <a href=\"content.php?SITE=oft_entities_add\">".check("default_name_of_new_entity", "New entity", 1)."</a>"; }
  if(isRechtenOFT($bedrijfsid, "CONTRACTEN")) { $submenuitems .= ' | '. oft_inputform_link('content.php?SITE=oft_contracten_add', "New contract"); }
  if(isRechtenOFT($bedrijfsid, "CRM"))        { $submenuitems .= ' | '. oft_inputform_link("content.php?SITE=oft_entitie_transactions_add", "New transaction"); }
  if(isRechtenOFT($bedrijfsid, "CRM"))        { $submenuitems .= ' | '. oft_inputform_link('content.php?SITE=oft_document_add', "Upload documents"); }

  $queryTables = array(
      "b" => "bedrijf",
      "oea" => "oft_entitie_accounting",
      "sr" => "stam_rechtsvormen",
  );
  $queryFields = array(
      "Id" => "b.ID",
      "Company" => "b.BEDRIJFSNAAM",
      "Country" => "b.LAND",
      "Legal Form" => "sr.NAAM",
      "Financial Year End" => "DATE_FORMAT(b.ENDOFBOOKYEAR, '%d %b')",
      "ESS Date" => "oea.ESSFILING",
  );
  // "" => "",
  
  $sql = "SELECT " . implode(", ", $queryFields) . " FROM (bedrijf AS b LEFT JOIN oft_entitie_accounting AS oea ON (b.ID=oea.BEDRIJFSID)) LEFT JOIN stam_rechtsvormen AS sr ON (b.RECHTSVORM=sr.ID) WHERE b.DELETED='Nee' OR b.DELETED IS NULL ORDER BY b.BEDRIJFSNAAM;";
  
  $query = $pdo->prepare($sql);
  $query->execute();
  
  if($query->errorCode() > 0) {
    dpm($query->errorInfo());exit;
  }
  
  $contentFrame = "";
  
  if($query->rowCount() > 0) {
    
    $contentFrame .= '<div class="oft2_page_centering"><table class="usa2eu-overview sortable"><thead><tr>'
            . '<th>Company</th>'
            . '<th>Country</th>'
            . '<th>HQ Country</th>'
            . '<th>Legal Form</th>'
            . '<th>Financial Year End</th>'
            . '<th>ESS Date</th>'
            . '<th>ABN</th>'
            . '<th>TFN</th>'
            . '<th>PAYGW</th>'
            . '<th>VAT</th>'
            . '</thead>' . PHP_EOL . '<tbody>';
    // . "<th></th>"
    while(($row = $query->fetch(PDO::FETCH_NUM)) !== false) {

      // get the numbers for this entity
      $numbersQuery = $pdo->prepare("SELECT * FROM oft_entitie_numbers WHERE BEDRIJFSID = :id AND DELETED != 'Ja';");
      $numbersQuery->bindValue('id', $row[0]);
      $numbersQuery->execute();
      $numbersResult = $numbersQuery->fetchAll(PDO::FETCH_ASSOC);
      
      $numbers = array(
          "ABN" => "",
          "TFN" => "",
          "PAYGW" => "",
          "VAT" => "",
      );
      foreach($numbersResult as $num) {
        $numbers[$num['TYPE']] = $num['VALUE'];
      }
      
      $contentFrame .= '<tr class="table-border">'
              . '<td><a href=\'content.php?SITE=oft_entitie_overview&ID='.($row[0]*1).'&MENUID=2\'>'.$row[1].'</a></td>'
              . '<td>' . $row[2] . '</td>'
              . '<td>N/A</td>'
              . '<td>' . $row[3] . '</td>'
              . '<td>' . $row[4] . '</td>'
              . '<td>' . $row[5] . '</td>'
              . '<td>' . $numbers['ABN'] . '</td>'
              . '<td>' . $numbers['TFN'] . '</td>'
              . '<td>' . $numbers['PAYGW'] . '</td>'
              . '<td>' . $numbers['VAT'] . '</td>'
              . '</tr>'. PHP_EOL;
    }
    $contentFrame .= '</tbody></table></div>';
  } else {
    $contentFrame .= "<p>No results</p>";
  }

  $contentFrame .= "<div class='oft2_page_centering'>";
  $contentFrame .= "";
  $contentFrame .= "";
  $contentFrame .= "</div>";

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}
