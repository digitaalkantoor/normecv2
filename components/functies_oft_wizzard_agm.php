<?php
sercurityCheck();

function oft_wizzard_agm_checklist($userid, $bedrijfsid, $typeform) {
  global $db;

  if(isset($_REQUEST["WZ_OPSLAAN"])) {

    $fields["TYPE"] = "";
    $fields["extraordinary"] = "";
    $fields["date meeting"] = "";
    $fields["financial year"] = "";

    if(oft_save_wizzard_step($userid, $bedrijfsid, $typeform, '1', $fields)) {

      //Create documets
      $type = strtolower(getWizzardValue($typeform, $bedrijfsid, 'TYPE'));
      $fields["extraordinary"] = getWizzardValue($typeform, $bedrijfsid, 'EXTRAORDINARY');
      $fields["date meeting"] = oft_document_date_format(getWizzardValue($typeform, $bedrijfsid, 'DATEOFMEETING'));
      $fields["financial year"] = getWizzardValue($typeform, $bedrijfsid, 'YEAR');

      // the file is build up from standard parts
      $minutes_doc = array("minutes_header.txt", "minutes_content_".$type.".txt", "minutes_footer.txt");
      $poa_doc = array("poa_minutes_header.txt", "poa_minutes_content_".$type.".txt", "minutes_footer.txt");

      oft_save_wizzard_documents($userid, $bedrijfsid, $minutes_doc, $typeform, "SAVEDOC1", $fields);
      oft_save_wizzard_documents($userid, $bedrijfsid, $poa_doc, $typeform, "SAVEDOC2", $fields);
      rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
    }
  }
  
  $YEAR = getWizzardValue($typeform, $bedrijfsid, 'YEAR');
  if($YEAR == '') {
    $YEAR = date("Y");
  }

  for($y = (date("Y")+1); $y >= (date("Y")-3); $y--) {
   $maanden[$y] = $y;
  }
  $yearoptions  = selectbox($maanden, $YEAR, false);

  $return = "<form action=\"content.php?SITE=$typeform&BID=$bedrijfsid&STAP=1\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Annual meeting</h2><br/>";
  $return .= "<table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"5\">";
  $return .= "<tr><td colspan=\"2\">Type of meeting</td></tr>
              <tr><td colspan=\"2\">".selectRadioOptions("TYPE", "Abbreviate", getWizzardValue($typeform, $bedrijfsid, 'TYPE'), array("Abbreviate", "Extensive", "Summarized"))."</td></tr>";
  $return .= "<tr><td>Extraordinary general meeting</td><td>".selectRadioOptions("EXTRAORDINARY", "Nee", getWizzardValue($typeform, $bedrijfsid, 'EXTRAORDINARY'), array("Ja", "Nee"))."</td></tr>";
  $return .= "<tr><td>Fiscal year</td><td><select name=\"YEAR\" class=\"field\">$yearoptions</select></td></tr>";
  $return .= "<tr><td>Date of meeting</td><td><input type=\"text\" name=\"DATEOFMEETING\" value=\"".getWizzardValue($typeform, $bedrijfsid, 'DATEOFMEETING')."\" class=\"field\" onClick=\"$(this).datepicker({dateFormat: 'yy-mm-dd'}).datepicker('show');\" /></td></tr>";
  $return .= "</table>";
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Next\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}

function oft_wizzard_agm_documents($userid, $bedrijfsid, $typeform) {
  global $pdo;
  
  $YEAR = date("Y");
  $type =  str_replace(" ", "_", getWizzardValue($typeform, $bedrijfsid, 'TYPE'));

  $SAVEDOC1 = "General Meeting $type $YEAR.docx";
  $SAVEDOC2 = "Power of Attorney $type $YEAR.docx";

  if(isset($_REQUEST["WZ_OPSLAAN"])) {
    //Copy content from wizzard_doc to documents
    if(oft_copy_wizzard_documents($userid, $bedrijfsid, $typeform, 'SAVEDOC1', $SAVEDOC1)) {
      if(oft_copy_wizzard_documents($userid, $bedrijfsid, $typeform, 'SAVEDOC2', $SAVEDOC2)) {
       //set wizzard row 'final';
        $query = $pdo->prepare('update oft_wizzard
                     set STATUSFORM = "final"
                   WHERE BEDRIJFSID = :bedrijfsid
                     AND TYPEFORM = :typeform
                     AND NOT STATUSFORM = "final";');
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $query->bindValue('typeform', $typeform);
        $query->execute();
        
      }
    }

    //Redirect to entity documents
    rd("content.php?SITE=oft_entitie_documents&BID=$bedrijfsid");
  } elseif(isset($_REQUEST["WZ_BACK"])) {
    rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
  }

  $return = "<form action=\"content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Document</h2><br/>";
  $return .= "<table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"5\">";
  $return .= "<tr><td rowspan=\"2\"><img src=\"./images/docx.png\" /></td>
                  <td><a target=\"_blank\" href=\"document.php?WIZZARD=".getWizzardValue($typeform, $bedrijfsid, 'ID')."&KOLOM=SAVEDOC1&NAME=".str_replace(" ", "_", $SAVEDOC1)."\"\">$SAVEDOC1</a></td></tr>
              <tr><td><input type=\"checkbox\" name=\"SAVEDOC1\" checked=\"true\" value=\"Ja\" /> Save this document in the documents section of this entity</td></tr>";

  $return .= "<tr><td><br/><br/></td></tr>
              <tr><td rowspan=\"2\"><img src=\"./images/docx.png\" /></td>
                  <td><a target=\"_blank\" href=\"document.php?WIZZARD=".getWizzardValue($typeform, $bedrijfsid, 'ID')."&KOLOM=SAVEDOC2&NAME=".str_replace(" ", "_", $SAVEDOC2)."\"\">$SAVEDOC2</a></td></tr>
              <tr><td><input type=\"checkbox\" name=\"SAVEDOC2\" checked=\"true\" value=\"Ja\" /> Save this document in the documents section of this entity</td></tr>";

  $return .= "<tr><td colspan=\"2\"><br/><br/>The signed documents have to be uploaden in the<br/>Resolution section of this entity.</td></tr>";
  $return .= "</table>";
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_BACK\" value=\"Back\" class=\"button\" /><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Finish\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}
