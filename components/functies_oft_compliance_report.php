<?php

sercurityCheck();

/*
 * Wie: Bob Salm
 * Wanneer: 01-11-2016
 * Waarom: Compliance report
 */

function oft_compliance_report($user, $company)
{
    global $pdo;
    $company = "";
    $title = tl('Compliance report');
    $submenuItems = "";

    if (isset($_REQUEST["JAAR"])) {
        $year = $_REQUEST["JAAR"];
    } else {
        $year = date("Y") - 1;
    }


    $query = $pdo->prepare('
        SELECT oft_annual_accounts.FINANCIALYEAR
        FROM oft_annual_accounts
        JOIN bedrijf on oft_annual_accounts.BEDRIJFSID = bedrijf.ID
        WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
        AND bedrijf.BEDRIJFSNAAM <> ""
        AND bedrijf.LAND <> ""
        GROUP BY oft_annual_accounts.FINANCIALYEAR
        ORDER BY oft_annual_accounts.FINANCIALYEAR DESC');
    $query->execute();
    $max = 0;
    $c = 0;
    foreach ($query->fetchAll() as $country) {
        $c++;
        if ($max && $c > $max){
            continue;
        }
        $y = $country['FINANCIALYEAR'];
        $class = ($y == $year) ? ' class="active"' : '';
        $submenuItems .= "<a$class href=\"content.php?SITE=oft_compliance_report&JAAR=$y\">$y</a> ";
    }
//    for ($y = (date("Y") + 1); $y > (date("Y") - 3); $y--) {
//        $class = ($y == $year) ? ' class="active"' : '';
//        $submenuItems .= "<a$class href=\"content.php?SITE=oft_compliance_report&JAAR=$y\">$y</a> ";
//    }


    $title .= " ($year)";

    $c = "<div class=\"oft2_page_centering\"><br/><br/>";
    $c .= countryList($user, $company, $year);
    $c .= "</div>";

    $c .= oft_document_legenda($user, $company);

    echo oft_framework_basic($user, $company, $c, $title, $submenuItems);
}

function countryList($user, $company, $year)
{
    global $pdo;

    $query = $pdo->prepare('
        SELECT bedrijf.LAND
        FROM bedrijf
        JOIN oft_annual_accounts on oft_annual_accounts.BEDRIJFSID = bedrijf.ID
        WHERE (NOT bedrijf.DELETED = "Ja" OR bedrijf.DELETED is null)
        AND bedrijf.BEDRIJFSNAAM <> ""
        AND bedrijf.LAND <> ""
        AND oft_annual_accounts.FINANCIALYEAR = '.$year.'
        GROUP BY bedrijf.LAND
        ORDER BY bedrijf.LAND');
    $query->execute();

    $content = "<table style='width:100%;'>
                    <thead>
                        <tr>
                            <td width='30'></td>
                            <td width='500'></td>
                            <td width='50'></td>
                            <td width='50'></td>
                            <td width='50'></td>
                            <td width='50'></td>
                            <td width='50'></td>
                            <td width='150'>Deadline</td>
                            <td width='150'>Date of filing</td>
                            <td width='50'>Audit</td>
                        </tr>
                    </thead>";

    $c = 0;
    foreach ($query->fetchAll() as $country) {
        $contentList = '';

        $tWhite = 0;
        $tGreen = 0;
        $tOrange = 0;
        $tRed = 0;
        $tBlack = 0;

        $statement = "SELECT bedrijf.ID, bedrijf.BEDRIJFSNAAM, bedrijf.BEDRIJFNR, bedrijf.LAND
            FROM bedrijf
            JOIN oft_annual_accounts on oft_annual_accounts.BEDRIJFSID = bedrijf.ID
            WHERE (NOT bedrijf.DELETED = 'Ja' OR bedrijf.DELETED is null)
            AND bedrijf.LAND = :land
            AND bedrijf.ID IN (select BEDRIJFSID from oft_rechten_tot where USERID = '$user' AND LEGAL = 'Ja')
            AND bedrijf.BEDRIJFSNAAM <> ''
        AND oft_annual_accounts.FINANCIALYEAR = $year
            GROUP BY bedrijf.BEDRIJFSNAAM
            ORDER BY (bedrijf.GROUPID*1), bedrijf.BEDRIJFSNAAM;";

        $query = $pdo->prepare($statement);
        $query->bindValue('land', $country["LAND"]);
        $query->execute();
        foreach ($query->fetchAll() as $company) {

            $color = getAnnualAccountColor($user, $company["ID"], $year);

            $annualAccount = getAnnualAccount($user, $company["ID"], $year);

            $tWhite += $color == 'white';
            $tGreen += $color == 'green';
            $tOrange += $color == 'orange';
            $tRed += $color == 'red';
            $tBlack += $color == 'black';

            $contentList .= "<tr id='ID" . ($company["ID"] * 1) . "'>
                          <td width='30'></td>
                          <td width='500'><a href='content.php?SITE=oft_entitie_compliance&ID=" . lb($company["ID"]) . "&BID=" . lb($company["ID"]) . "'>" . lb($company["BEDRIJFSNAAM"]) . "</a></td>
                          <td width='50'>" . ($color == 'white' ? "<img src='./images/oft/oft_white_circle.png'/> " : '') . "</td>
                          <td width='50'>" . ($color == 'green' ? "<img src='./images/oft/oft_green_circle.png'/> " : '') . "</td>
                          <td width='50'>" . ($color == 'orange' ? "<img src='./images/oft/oft_orange_circle.png'/> " : '') . "</td>
                          <td width='50'>" . ($color == 'red' ? "<img src='./images/oft/oft_red_circle.png'/> " : '') . "</td>
                          <td width='50'>" . ($color == 'black' ? "<img src='./images/oft/oft_black_circle.png'/> " : '') . "</td>
                          <td width='150'>" . $annualAccount['deadline'] . "</td>
                          <td width='150'>" . $annualAccount['filingdate'] . "</td>
                          <td width='50'>" . $annualAccount['audit'] . "</td>
                        </tr>";
        }

        if ($contentList != '') {

            $content .= "<tr onclick=\"toggleArrow('table$c', 'arrow$c');\">
                        <td><img id='arrow$c' border='0' width='11' src='./images/oft/pijlright.jpg'></td>
                        <td> " . $country["LAND"] . "</td>
                        <td>" . ($tWhite ? "<img src='./images/oft/oft_white_circle.png'/> " . $tWhite : '') . "</td>
                        <td>" . ($tGreen ? "<img src='./images/oft/oft_green_circle.png'/> " . $tGreen : '') . "</td>
                        <td>" . ($tOrange ? "<img src='./images/oft/oft_orange_circle.png'/> " . $tOrange : '') . "</td>
                        <td>" . ($tRed ? "<img src='./images/oft/oft_red_circle.png'/> " . $tRed : '') . "</td>
                        <td>" . ($tBlack ? "<img src='./images/oft/oft_black_circle.png'/> " . $tBlack : '') . "</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>";

            $content .= "<tr id='table$c' style='display: none;'>
                            <td colspan='10'>
                                <table style='width: 100%;'>
                                    $contentList
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='10'><hr style='margin: 5px;'></td>
                        </tr>";
        }
        $c++;
    }
    $content .= "</table>";

    return $content;
}

function GetAnnualAccount($user, $company, $year)
{
    global $pdo;

    $return = [
        'deadline' => 'N/A',
        'filingdate' => '',
        'audit' => ''
    ];

    $statement = "SELECT FILINGDATE, AUDIT,
(case when (EXTENTION IS NOT NULL AND EXTENTION != '0000-00-00' AND EXTENTION != '') THEN EXTENTION ELSE STANDARDDUEDATE END) as deadline
FROM oft_annual_accounts
WHERE BEDRIJFSID = :company AND FINANCIALYEAR = :year limit 1";

    $query = $pdo->prepare($statement);
    $query->bindValue('company', $company);
    $query->bindValue('year', $year);
    $query->execute();

    $annualAccount = $query->fetch();
    if (is_array($annualAccount)) {
        if ($annualAccount["deadline"]) {
            $return['deadline'] = $annualAccount["deadline"];
        }
        if ($annualAccount["FILINGDATE"] && $annualAccount["FILINGDATE"] != '' && $annualAccount["FILINGDATE"] != '0000-00-00') {
            $return['filingdate'] = $annualAccount["FILINGDATE"];
        }
        if ($annualAccount["AUDIT"]) {
            $return['audit'] = $annualAccount["AUDIT"] == 'Ja' ? 'Yes' : 'No';
        }
    }
    if ($return['deadline'] == 'N/A') {
        //update database if there is old data
        check_annual_accounts(false, $company, $company);
    }
    return $return;
}

function getAnnualAccountColor($user, $company, $year)
{
    foreach (["green", "white", "orange", "red", "black"] as $color) {
        if (getSpecificAnnualAccountColor($user, $company, $year, $color)) {
            return $color;
        }
    }
    return false;
    // if all the query=ies don't work out, show nothing.
}

function getSpecificAnnualAccountColor($user, $company, $year, $color)
{
    global $pdo;

    if (!$color) {
        return 0;
    }

    $statement = 'SELECT * FROM oft_annual_accounts WHERE BEDRIJFSID = :company AND FINANCIALYEAR = :year';

    $not_required = "STATUS = 'Not required'";
    $extention_not_empty = "(EXTENTION IS NOT NULL AND EXTENTION != '0000-00-00' AND EXTENTION != '')";
    $extention_empty = "(EXTENTION IS NULL OR EXTENTION = '0000-00-00' OR EXTENTION = '')";
    $filing_not_empty = "(FILINGDATE IS NOT NULL AND FILINGDATE != '0000-00-00' AND FILINGDATE != '')";
    $filing_empty = "(FILINGDATE IS NULL OR FILINGDATE = '0000-00-00' OR FILINGDATE = '')";
    $thirty_days = "date_add(now(), interval 30 day)";
    $fourteen_days = "date_add(now(), interval 14 day)";
    $today = "cast(now() as date)";

    //  WHITE   Not required or due date more than 30 days left
    //  GREEN   File uploaded
    //  ORANGE  Due date is within 30 days
    //  RED     Due date is within 14 days
    //  BLACK   Due date has expired, or document has been filed after deadline
    switch ($color) {
        case "green":
            $statement .= " AND $filing_not_empty AND ( FILINGDATE <= STANDARDDUEDATE OR FILINGDATE <= EXTENTION )";
            break;
        case "white":
            $statement .= " AND ($not_required OR ( $filing_empty AND ( ( $extention_not_empty AND EXTENTION >= $thirty_days ) OR  ( $extention_empty AND STANDARDDUEDATE >= $thirty_days ) ) ) )";
            break;
        case "orange":
            $statement .= " AND $filing_empty AND ( ( $extention_not_empty AND EXTENTION >= $fourteen_days AND EXTENTION < $thirty_days) OR ( $extention_empty AND STANDARDDUEDATE >= $fourteen_days AND STANDARDDUEDATE < $thirty_days ) )";
            break;
        case "red":
            $statement .= " AND $filing_empty AND ( ( $extention_not_empty AND EXTENTION >= $today AND EXTENTION < $fourteen_days ) OR ( $extention_empty AND STANDARDDUEDATE >= $today AND STANDARDDUEDATE < $fourteen_days ) )";
            break;
        case "black":
            $statement .= " AND ( ( $filing_not_empty AND FILINGDATE > STANDARDDUEDATE AND ($extention_empty OR FILINGDATE > EXTENTION) ) OR ( $filing_empty AND ( $extention_not_empty AND EXTENTION < $today OR $extention_empty AND STANDARDDUEDATE < $today ) ) )";
            break;
    }
    $statement .= ' LIMIT 1';

    $query = $pdo->prepare($statement);
    $query->bindValue('company', $company);
    $query->bindValue('year', $year);
    if ($query->execute()) {
        return count($query->fetchAll());
    }
    return 0;
}