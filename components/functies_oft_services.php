<?php
sercurityCheck();

function oft_services($userid, $bedrijfsid) {
    global $db;

    $titel           = "Corporate services";
    $submenuitems    = oft_inputform_link('content.php?SITE=oft_services_add', "Add item");
    $back_button     = "";

    $tabel           = "oft_services";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

    $toevoegen       = "content.php?SITE=oft_services_add";
    $bewerken        = oft_inputform_link_short("content.php?SITE=oft_services_edit&ID=-ID-");
    $verwijderen     = "";
    
    $contentFrame    = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");

    echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_services_ajax($userid, $bedrijfsid) {
    global $db;

    $tabel           = "oft_services";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

    $toevoegen       = "content.php?SITE=oft_services_add";
    $bewerken        = oft_inputform_link_short("content.php?SITE=oft_services_edit&ID=-ID-");
    $verwijderen     = "";

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");
}

function oft_services_print($userid, $bedrijfsid) {
    global $db;

    $tabel           = "oft_services";
    $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
    $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
    $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
    $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayZoekvelden");

    $toevoegen       = "";
    $bewerken        = "";
    $verwijderen     = "";

    echo "<h1>Services</h1><br/><br/>";

    echo oft_tabel_print($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'ID DESC', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");
}

function oft_services_add($userid, $bedrijfsid) {
    global $db;

    $templateFile = "./templates/oft_services.htm";
    $tabel = "oft_services";

    echo "<h2>Add service</h2>";

    echo replace_template_content($userid, $bedrijfsid, $tabel, $templateFile);
}

function oft_services_edit($userid, $bedrijfsid) {
    global $db;

    $templateFile = "./templates/oft_services.htm";
    $tabel = "oft_services";

    $id = "";
    if(isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }

    echo "<h2>Edit service</h2>";

    echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}

function oft_services_1_print($userid, $bedrijfsid) {
    global $db;

    $templateFile = "./templates/oft_services_print.htm";
    $tabel = "oft_services";

    $id = "";
    if(isset($_REQUEST["ID"])) {
        $id = $_REQUEST["ID"];
    }

    echo "<h2>Print service</h2>";

    echo replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);
}