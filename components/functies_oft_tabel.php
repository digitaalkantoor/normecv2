<?php
sercurityCheck();

function oft_tabel_zoekvelden(
    $sectie,
    $userid,
    $bedrijfsid,
    $arrayVelden,
    $arrayVeldnamen,
    $arrayVeldType,
    $arrayZoekvelden,
    $tabel
) {
    global $pdo;
    $content = "";
    if (count($arrayZoekvelden) > 0) {
        $url_querystring = oft_tabel_querystring();

        // Exceptions
        $url_querystring .= searchFieldsConverter($arrayZoekvelden);

        //Toon zoekfunctie
        $content .= "<div class=\"oft2_page_header\">";
        $content .= "<div class='oft2_page_centering'>";


        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "oft_search") {
            $content .= oft_search_filter($userid, $bedrijfsid);
        } else {
            $telZv = 0;
            foreach ($arrayZoekvelden as $Veld) {
                if ($Veld == "ENTITY" || $Veld == "ENTITY_TABLE") {
                    //Zoek bedrijf
                    if (!empty($tabel)) {
                        $tableData = getTableData($tabel);
                        if (empty($tableData)) {
                            echo 'ERROR 002';
                            return;
                        }
                    }

                    $content .= "<div class='oft2_combo_box'><label for='SRCH_BEDRIJFSNAAM'>" . check("default_name_of_entity",
                            "Entity", 1) . "</label>";

                    $SQL_ENTITY = "SELECT ID, BEDRIJFSNAAM FROM bedrijf WHERE (NOT DELETED = 'Ja' OR DELETED is null) AND NOT LAND = '' AND NOT LAND is null ORDER BY BEDRIJFSNAAM";
                    if ($Veld == "ENTITY_TABLE") {
                        $SQL_ENTITY = "SELECT distinct bedrijf.ID, bedrijf.BEDRIJFSNAAM FROM bedrijf, $tabel WHERE $tabel.BEDRIJFSID = bedrijf.ID AND NOT bedrijf.LAND = '' AND NOT bedrijf.LAND is null ORDER BY bedrijf.BEDRIJFSNAAM";
                    }
                    $content .= oft_select($url_querystring, "ID", "BEDRIJFSNAAM", $pdo->prepare($SQL_ENTITY), true,
                        true, "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "UPDATEDBY") {
                    //Zoek bedrijf
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_ACHTERNAAM'>Updated by</label>";
                    $SQL_ENTITY = "SELECT distinct personeel.ID, personeel.ACHTERNAAM FROM personeel, $tabel WHERE (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null) AND  ($tabel.PERSONEELSLID) = personeel.ID AND (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null) ORDER BY personeel.ACHTERNAAM";
                    //echo $SQL_ENTITY;
                    $content .= oft_select($url_querystring, "ID", "ACHTERNAAM", $pdo->prepare($SQL_ENTITY), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "EMPLOYEE") {
                    //Zoek bedrijf
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_ACHTERNAAM'>Employee</label>";
                    $SQL_ENTITY = "SELECT distinct personeel.ID, personeel.ACHTERNAAM FROM personeel, $tabel WHERE (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null) AND  ($tabel.EMPLOYEE*1) = personeel.ID AND (NOT personeel.DELETED = 'Ja' OR personeel.DELETED is null) ORDER BY personeel.ACHTERNAAM";
                    $content .= oft_select($url_querystring, "ID", "ACHTERNAAM", $pdo->prepare($SQL_ENTITY), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "COUNTRY" || $Veld == "COUNTRY_TABLE") {
                    //Zoek land in tabel bedrijf
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_LAND'>Country</label>";
                    if ($Veld == "COUNTRY_TABLE") {
                        $SQL_COUNTRY = "SELECT bedrijf.LAND FROM bedrijf, $tabel WHERE (NOT bedrijf.DELETED = 'Ja' OR bedrijf.DELETED is null) AND  NOT bedrijf.LAND = '' AND NOT bedrijf.LAND is null AND $tabel.BEDRIJFSID = bedrijf.ID GROUP BY bedrijf.LAND ORDER BY bedrijf.LAND";
                    } elseif ($tabel == "oft_compliance_chart" || $tabel == "oft_trainingsschema") {
                        $SQL_COUNTRY = "SELECT COUNTRY As LAND FROM $tabel WHERE (!DELETED AND DELETED = 'Nee') AND NOT COUNTRY = '' AND NOT COUNTRY is null GROUP BY COUNTRY ORDER BY COUNTRY";
                    } else {
                        $SQL_COUNTRY = "SELECT LAND FROM bedrijf WHERE NOT LAND = '' AND NOT LAND is null GROUP BY LAND ORDER BY LAND";
                    }
                    $content .= oft_select($url_querystring, "LAND", "LAND", $pdo->prepare($SQL_COUNTRY), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "DOCTYPE") {
                    //Zoek land in tabel bedrijf
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_DOCTYPE'>Category</label>";

                    $sql_add = "";
                    if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "oft_finance") {
                        $sql_add = getDoctypesBySection("Finance");
                    } elseif (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "oft_hr") {
                        $sql_add = getDoctypesBySection("HR");
                    } elseif (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "oft_document") {
                        $sql_add = getDoctypesBySection("Legal");
                    }
                    $SQL_EVENT = "SELECT DOCTYPE FROM documentbeheer WHERE (NOT DELETED = 'Ja' OR DELETED is null) $sql_add GROUP BY DOCTYPE ORDER BY DOCTYPE";
                    $content .= oft_select($url_querystring, "DOCTYPE", "DOCTYPE", $pdo->prepare($SQL_EVENT), true,
                        true, false, $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "EVENT") {
                    //Zoek land in tabel bedrijf
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_EVENT'>Type</label>";
                    $SQL_EVENT = "SELECT EVENT FROM notificaties WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY EVENT ORDER BY EVENT";
                    $content .= oft_select($url_querystring, "EVENT", "EVENT", $pdo->prepare($SQL_EVENT), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "STATUS" && ($tabel == "oft_annual_accounts" || $tabel == "oft_tax")) {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_STATUS'>Status</label>";
                    $SQL_YEAR = "SELECT STATUS FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY STATUS ORDER BY STATUS";
                    $content .= oft_select($url_querystring, "STATUS", "STATUS", $pdo->prepare($SQL_YEAR), true, false,
                        false, $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "YEAR" && ($tabel == "oft_annual_accounts" || $tabel == "oft_tax")) {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_JAAR'>Year</label>";
                    $SQL_YEAR = "SELECT DATE_FORMAT(STANDARDDUEDATE, '%Y') As JAAR FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) " . getDoctypesBySection($sectie) . " GROUP BY (JAAR*1) ORDER BY (JAAR*1)";
                    $content .= oft_select($url_querystring, "JAAR", "JAAR", $pdo->prepare($SQL_YEAR), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "YEAR" && $tabel == "oft_trainingsschema") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_JAAR'>Year</label>";
                    $SQL_YEAR = "SELECT DATE_FORMAT(DUEDATE, '%Y') As JAAR FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) " . getDoctypesBySection($sectie) . " GROUP BY (JAAR*1) ORDER BY (JAAR*1)";
                    $content .= oft_select($url_querystring, "JAAR", "JAAR", $pdo->prepare($SQL_YEAR), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "YEAR" && $tabel == "notificaties") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_JAAR'>Fiscal year</label>";
                    $SQL_YEAR = "(
                             SELECT FINANCIALYEAR As JAAR
                               FROM notificaties, oft_tax, bedrijf
                              WHERE notificaties.BEDRIJFSID = bedrijf.ID
                                  AND bedrijf.MONITOR = 'Ja'
                                  AND notificaties.TABELID = oft_tax.ID
                                AND notificaties.STATUS = 'Active'
                                AND (NOT bedrijf.DELETED = 'Ja' OR bedrijf.DELETED is null)
                              GROUP BY (JAAR*1)
                            )
                            union (
                              SELECT FINANCIALYEAR As JAAR
                               FROM notificaties, oft_annual_accounts, bedrijf
                              WHERE notificaties.BEDRIJFSID = bedrijf.ID
                                AND bedrijf.MONITOR = 'Ja'
                                AND notificaties.TABELID = oft_annual_accounts.ID
                                AND notificaties.STATUS = 'Active'
                                AND (NOT bedrijf.DELETED = 'Ja' OR bedrijf.DELETED is null)
                              GROUP BY (JAAR*1)
                            )

                            ORDER BY (JAAR*1);";

                    //echo $SQL_YEAR;
                    $content .= oft_select($url_querystring, "JAAR", "JAAR", $pdo->prepare($SQL_YEAR), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "MONTH") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_MONTH'>Month</label>";
                    $sMonth = "SELECT ID, MONTH FROM $tabel WHERE (!DELETED AND DELETED = 'Nee') " . getDoctypesBySection($sectie) . " GROUP BY MONTH ORDER BY MONTH";
                    $content .= oft_select($url_querystring, "MONTH", "MONTH", $pdo->prepare($sMonth), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "YEAR") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_JAAR'>Year</label>";
                    $SQL_YEAR = "SELECT (JAAR*1) As JAAR FROM $tabel WHERE (!DELETED AND DELETED = 'Nee') " . getDoctypesBySection($sectie) . " GROUP BY (JAAR*1) ORDER BY (JAAR*1)";
                    $content .= oft_select($url_querystring, "JAAR", "JAAR", $pdo->prepare($SQL_YEAR), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "SUSPEND") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_SUSPEND'>Suspend</label>";
                    $SQL_YEAR = "SELECT SUSPEND FROM $tabel WHERE (!DELETED AND DELETED = 'Nee') GROUP BY SUSPEND";
                    $content .= oft_select($url_querystring, "SUSPEND", "SUSPEND", $pdo->prepare($SQL_YEAR), true,
                        false, false, $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "TYPE" && $tabel == "contracten") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_NAAM'>Type</label>";
                    $SQL_YEAR = "SELECT ID, NAAM FROM stam_contracttype WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY NAAM";
                    $content .= oft_select($url_querystring, "ID", "NAAM", $pdo->prepare($SQL_YEAR), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif (substr($Veld, 0, 5) == "stam_") {

                    $txt = "Category";
                    if ($Veld == "stam_risklevel") {
                        $txt = "Risklevel";
                    } elseif ($Veld == "stam_compliantstatus") {
                        $txt = "Status";
                    }
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_$Veld'>$txt</label>";
                    $SQL_STAM = "SELECT ID, NAAM FROM $Veld WHERE (!DELETED AND DELETED = 'Nee') ORDER BY NAAM";
                    $oft_select = oft_select($url_querystring, "ID", "NAAM", $pdo->prepare($SQL_STAM), true, true,
                        "field3", $tabel);
                    $content .= str_replace("SRCH_NAAM", "SRCH_$Veld", $oft_select);
                    $content .= "</div>";
                } elseif ($Veld == "STATUS" && ($tabel == "oft_complaint" || $tabel == "oft_action_tracking_audit" || $tabel == "oft_action_tracking" || $tabel == "oft_incident_register")) {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_STATUS'>Status</label>";
                    $SQL_STAM = "SELECT STATUS FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY STATUS ORDER BY STATUS";
                    $oft_select = oft_select($url_querystring, "STATUS", "STATUS", $pdo->prepare($SQL_STAM), true,
                        false, false, $tabel);
                    $content .= str_replace("SRCH_NAAM", "SRCH_$Veld", $oft_select);
                    $content .= "</div>";
                } elseif ($Veld == "CLASSTYPE") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_$Veld'>Class of shares</label>";
                    //<select name=\"CLASSTYPE\" class=\"fieldHalf\">".selectbox(array('Common' => 'Common', 'Sale of share' => 'Sale of share', 'Pledge of shre' => 'Pledge of share', 'Certification of share' => 'Certification of share'), 'Common', false)."</select>
                    $SQL_COUNTRY = "SELECT $Veld FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY $Veld ORDER BY $Veld";
                    $content .= oft_select($url_querystring, $Veld, $Veld, $pdo->prepare($SQL_COUNTRY), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "SOORT") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_$Veld'>Incidents or claims</label>";
                    $SQL_COUNTRY = "SELECT $Veld FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY $Veld ORDER BY $Veld";
                    $content .= oft_select($url_querystring, $Veld, $Veld, $pdo->prepare($SQL_COUNTRY), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "ENDREDEN") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_$Veld'>Status</label>";
                    $SQL_COUNTRY = "SELECT $Veld FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY $Veld ORDER BY $Veld";
                    $content .= oft_select($url_querystring, $Veld, $Veld, $pdo->prepare($SQL_COUNTRY), true, true,
                        "field3", $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "DATUM_VAN") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_DATUM_VAN'>From</label>";
                    $content .= "<input type=\"text\" class=\"field4\" value=\"\" name=\"SRCH_DATUM_VAN\" onchange=\"getPageContent2('$url_querystring', 'contentTable_$tabel', searchReq2);\" onClick=\"$(this).datepicker({dateFormat: 'yy-mm-dd'}).datepicker('show');\" /></div>";
                } elseif ($Veld == "DATUM_TOT") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_DATUM_TOT'>Till</label>";
                    $content .= "<input type=\"text\" class=\"field4\" value=\"\" name=\"SRCH_DATUM_VAN\" onchange=\"getPageContent2('$url_querystring', 'contentTable_$tabel', searchReq2);\" onClick=\"$(this).datepicker({dateFormat: 'yy-mm-dd'}).datepicker('show');\" /></div>";
                } elseif ($Veld == "SEARCH") {
                    //Zoek Naam, ($arrayZoekvelden)??
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_SEARCH'>Search</label>";
                    $content .= "<input type=\"text\" class=\"field\" value=\"\" id=\"SRCH_SEARCH\" name=\"SRCH_SEARCH\" onkeyup=\"getPageContent2('$url_querystring', 'contentTable_$tabel', searchReq2);\" /></div>"; //&SRCH_SEARCH='+document.getElementById('SRCH_SEARCH').value
                } elseif ($Veld == "TARGETNAME") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_TARGETNAME'>Target name</label>";
                    $content .= "<input type=\"text\" class=\"field\" value=\"\" id=\"SRCH_TARGETNAME\" name=\"SRCH_SEARCH\" onkeyup=\"getPageContent2('$url_querystring', 'contentTable_$tabel', searchReq2);\" /></div>"; //&SRCH_SEARCH='+document.getElementById('SRCH_SEARCH').value
                } elseif ($Veld == "STATUS") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_STATUS'>Status</label>";
                    $SQL = "SELECT $Veld FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY $Veld ORDER BY $Veld";
                    $content .= oft_select($url_querystring, $Veld, $Veld, $pdo->prepare($SQL), true, true, "field3",
                        $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "SERVICE") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_SERVICE'>Service</label>";
                    $SQL = "SELECT $Veld FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY $Veld ORDER BY $Veld";
                    $content .= oft_select($url_querystring, $Veld, $Veld, $pdo->prepare($SQL), true, true, "field3",
                        $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "TARGETSOURCE") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_TARGETNAME'>Target source</label>";
                    $SQL = "SELECT $Veld FROM $tabel WHERE (NOT DELETED = 'Ja' OR DELETED is null) GROUP BY $Veld ORDER BY $Veld";
                    $content .= oft_select($url_querystring, $Veld, $Veld, $pdo->prepare($SQL), true, true, "field3",
                        $tabel);
                    $content .= "</div>";
                } elseif ($Veld == "ID") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_ID'>ID</label>";
                    $content .= "<input type=\"text\" class=\"field\" value=\"\" id=\"SRCH_ID\" name=\"SRCH_ID\" onkeyup=\"getPageContent2('$url_querystring', 'contentTable_$tabel', searchReq2);\" /></div>";
                } elseif ($Veld == "MENAID") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_MENAID'>Target ID</label>";
                    $content .= "<input type=\"text\" class=\"field\" value=\"\" id=\"SRCH_MENAID\" name=\"SRCH_MENAID\" onkeyup=\"getPageContent2('$url_querystring', 'contentTable_$tabel', searchReq2);\" /></div>";
                } elseif ($Veld == "LESSONS") {
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_LESSON'>Lesson</label>";
                    $content .= "<input type=\"text\" class=\"field\" value=\"\" id=\"SRCH_LESSON\" name=\"SRCH_LESSON\" onkeyup=\"getPageContent2('$url_querystring', 'contentTable_$tabel', searchReq2);\" /></div>";
                } else {
                    $arrayKey = array_search($Veld, $arrayVelden);
                    $content .= "<div class='oft2_combo_box'><label for='SRCH_$Veld'>$arrayVeldnamen[$arrayKey]</label>";
                    $SQL = "SELECT $Veld FROM $tabel WHERE (!DELETED AND DELETED = 'Nee') GROUP BY $Veld ORDER BY $Veld";
                    $content .= oft_select($url_querystring, $Veld, $Veld, $pdo->prepare($SQL), true, true, "field3",
                        $tabel);
                    $content .= "</div>";

                }

                if (!strstr($content, "No result")) {
                    //$content = str_replace("getPageContent2", "document.getElementById('contentTable_$tabel').innerHTML='<center><br/><br/><br/><img src=./images/wait30trans.gif /></center>'; getPageContent2", $content);
                }

                $telZv++;

            }
        }
        $content .= "</div>";
        $content .= "</div>";
    }

    return $content;
}

function oft_tabel(
    $userid = false,
    $bedrijfsid = false,
    $arrayVelden = array(),
    $arrayVeldnamen = array(),
    $arrayVeldType = array(),
    $arrayZoekvelden = array(),
    $tabel = "",
    $order = "ID",
    $toevoegen = "",
    $bewerken = "",
    $verwijderen = "",
    $sql_toevoeging = "",
    $multiselect = false
) {
    $content = "";

    $sectie = "";
    if ($tabel == "documentbeheer" && $_REQUEST["SITE"] != "oft_search") {
        $sectie = "Legal";
        if (isset($_REQUEST["SITE"]) && ($_REQUEST["SITE"] == "oft_finance" || $_REQUEST["SITE"] == "oft_entitie_finance")) {
            $sectie = "Finance";
        } elseif (isset($_REQUEST["SITE"]) && ($_REQUEST["SITE"] == "oft_hr" || $_REQUEST["SITE"] == "oft_entitie_hr")) {
            $sectie = "HR";
        }
    }

    if (isset($_REQUEST["SITE"])
        && $_REQUEST["SITE"] != 'oft_entitie_finance'
        && $_REQUEST["SITE"] != 'oft_entitie_hr'
        && $_REQUEST["SITE"] != 'oft_entitie_contracts'
        && $_REQUEST["SITE"] != 'oft_entitie_transactions'
        && $_REQUEST["SITE"] != 'oft_entitie_documents'
        && $_REQUEST["SITE"] != 'oft_persons_contracts'
        && $_REQUEST["SITE"] != 'oft_registers_incident'
        && $_REQUEST["SITE"] != 'oft_registers_claim'
        && $_REQUEST["SITE"] != 'oft_registers_actiontracking'
        && $_REQUEST["SITE"] != 'oft_registers_actiontracking_audit'
        && $_REQUEST["SITE"] != 'oft_registers_compliant'
        && $_REQUEST["SITE"] != 'oft_entitie_transactions'
    ) {
        $content .= oft_tabel_zoekvelden($sectie, $userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType,
            $arrayZoekvelden, $tabel);
    }

    if ($tabel == "oft_mena") {
        $content .= oft_mena_map();
    }

    $content .= PHP_EOL . "<div class='oft2_page_centering'>";

    if ($tabel == "documentbeheer" && $_REQUEST["SITE"] != "oft_search") {
        $content .= "<div id=\"contentTable_$tabel\">";
    }

    $content .= oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType,
        $arrayZoekvelden, $tabel, $order, $toevoegen, $bewerken, $verwijderen, $sql_toevoeging, array(),
        $multiselect);

    if ($tabel == "documentbeheer" && $_REQUEST["SITE"] != "oft_search") {
        $content .= "</div>\n";
    }

    $content .= "</div>\n";

    return $content;
}

function oft_select(
    $url,
    $idField,
    $showField,
    $sqlQuery,
    $html = false,
    $include_all = true,
    $class = "field3",
    $tabel = ""
) {
    $onChange = "";
    if ($url != '') {
        $tableId = "contentTable";
        if ($tabel) {
            $tableId .= "_$tabel";
        }
        $onChange = "onchange=\"getPageContent2('$url', '$tableId', searchReq2);\"";
    }
//  $class .= " select-with-search";

    $content = "<select name=\"SRCH_$showField\" id=\"SRCH_$showField\" class=\"$class\" $onChange>";

    if ($include_all) {
        $content .= "<option value=\"\">All</option>";
    }

    $sqlQuery->execute();

    foreach ($sqlQuery->fetchAll() as $row) {
        if ($row[$showField] != "") {
            $id_string = $row[$idField];
            if ($html) {
                // $id_string = htmlentities($id_string); de value niet omzetten: dan wordt hij niet meer gevonden...
            }

            if ($showField == "MONTH") {
                $maanden = array(
                    "01" => "January",
                    "02" => "February",
                    "03" => "March",
                    "04" => "April",
                    "05" => "May",
                    "06" => "June",
                    "07" => "July",
                    "08" => "August",
                    "09" => "September",
                    "10" => "October",
                    "11" => "November",
                    "12" => "December"
                );
                $showValue = $maanden[$row["MONTH"]];
            } else {
                $showValue = tl(stripslashes($row[$showField]));
            }

            $selected = "";
            if (isset($_REQUEST["SRCH_$showField"]) && ($_REQUEST["SRCH_$showField"] == $id_string || $_REQUEST["SRCH_$showField"] == "x" . $id_string)) {
                $selected = "selected=\"true\"";
            }

            $content .= "<option $selected value=\"" . $id_string . "\">" . lb($showValue) . "</option>";
        }
    }

    $content .= "</select>";

    return $content;
}

function oft_tabel_querystring()
{
    $BID = "";
    if (isset($_REQUEST["BID"])) {
        $BID = "&BID=" . ps($_REQUEST["BID"], "nr");
    }
    $PID = "";
    if (isset($_REQUEST["PID"])) {
        $PID = "&PID=" . ps($_REQUEST["PID"], "nr");
    }
    $MENUID = "";
    if (isset($_REQUEST["MENUID"])) {
        $MENUID = "&MENUID=" . ps($_REQUEST["MENUID"], "nr");
    }
    $ID = "";
    if (isset($_REQUEST["ID"])) {
        $ID = "&ID=" . ps($_REQUEST["ID"], "nr");
    }

    $TABEL = "";
    if (isset($_REQUEST["TABEL"])) {
        $TABEL = "&TABEL=" . ps($_REQUEST["TABEL"], "tabel");
    }

    $url_querystring = "content.php?SITE=" . str_replace("_ajax", "",
            $_REQUEST["SITE"]) . "_ajax" . $BID . $PID . $MENUID . $ID . $TABEL;

    if (isset($_REQUEST["SHOWNEW"])) {
        $url_querystring .= "&SHOWNEW=" . $_REQUEST["SHOWNEW"];
    }
    if (isset($_REQUEST["SHOWOPEN"])) {
        $url_querystring .= "&SHOWOPEN=" . $_REQUEST["SHOWOPEN"];
    }
    if (isset($_REQUEST["SHOWCLOSED"])) {
        $url_querystring .= "&SHOWCLOSED=" . $_REQUEST["SHOWCLOSED"];
    }

    return $url_querystring;
}

function oft_getSearchSQL($userid, $bedrijfsid, $tabel)
{
    global $pdo;

    if (!empty($tabel)) {
        $tableData = getTableData($tabel);
        if (empty($tableData)) {
            return;
        }
    }

    $sql_toevoeging = array();
    if (isset($_REQUEST["SRCH_COUNTRY"]) && $_REQUEST["SRCH_COUNTRY"] != "x" && $_REQUEST["SRCH_COUNTRY"] != "") {
        $_REQUEST["SRCH_COUNTRY"] = htmlspecialchars_decode($_REQUEST["SRCH_COUNTRY"]);

        $landen = "";
        $query = $pdo->prepare('SELECT ID from bedrijf where LAND like :land;');
        $likeLand = '%' . substr($_REQUEST["SRCH_COUNTRY"], 1, strlen($_REQUEST["SRCH_COUNTRY"])) . '%';
        $query->bindValue('land', $likeLand);
        $query->execute();

        foreach ($query->fetchAll() as $dataL) {
            $landen .= " OR $tabel.ID = '" . ps($dataL["ID"], "nr") . "' ";
        }
        if ($landen != "") {
            $landen = substr($landen, 3, strlen($landen));
            $sql_toevoeging[] = array('AND ' . $landen);
        }
    }

    if (isset($_REQUEST["SITE"]) && isset($_REQUEST["SRCH_LAND"]) && $_REQUEST["SRCH_LAND"] != "x" && $_REQUEST["SRCH_LAND"] != ""
        && ($_REQUEST["SITE"] == "oft_trainingsschema" || $_REQUEST["SITE"] == "oft_trainingsschema_ajax" || $_REQUEST["SITE"] == "oft_trainingsschema_print"
            || $_REQUEST["SITE"] == "oft_compliance_chart" || $_REQUEST["SITE"] == "oft_compliance_chart_ajax" || $_REQUEST["SITE"] == "oft_compliance_chart_print")) {
        $sql_toevoeging[] = array(
            ' AND ' . $tabel . '.COUNTRY = :country ',
            array('country' => substr($_REQUEST["SRCH_LAND"], 1, strlen($_REQUEST["SRCH_LAND"])))
        );
    } elseif (isset($_REQUEST["SRCH_LAND"]) && $_REQUEST["SRCH_LAND"] != "x" && $_REQUEST["SRCH_LAND"] != "") {
        $landen = "";

        // problemen met cedile in Curaçao: daarom zoeken op 'cura', gaat altijd goed.
        $srch_land = '%' . (strstr($_REQUEST["SRCH_LAND"], "Cura") ? "Cura" : substr($_REQUEST["SRCH_LAND"], 1,
                strlen($_REQUEST["SRCH_LAND"]))) . '%';

        $query = $pdo->prepare('select ID from bedrijf where LAND like :land;');
        $query->bindValue('land', $srch_land);
        $query->execute();

        foreach ($query->fetchAll() as $dataL) {
            $landen .= " OR $tabel.BEDRIJFSID = '" . ps($dataL["ID"], "nr") . "' ";
        }
        if ($landen != "") {
            $landen = substr($landen, 3, strlen($landen));
            $sql_toevoeging [] = array(" AND ($landen) ");
        }
    }
    if (isset($_REQUEST["SRCH_BEDRIJFSNAAM"]) && $_REQUEST["SRCH_BEDRIJFSNAAM"] != "*" && $_REQUEST["SRCH_BEDRIJFSNAAM"] != "") {
        $_REQUEST["SRCH_BEDRIJFSNAAM"] = htmlspecialchars_decode($_REQUEST["SRCH_BEDRIJFSNAAM"]);

        if (is_numeric($_REQUEST["SRCH_BEDRIJFSNAAM"])) {
            $query = $pdo->prepare('select ID from bedrijf where ID = :id limit 1;');
            $query->bindValue('id', $_REQUEST["SRCH_BEDRIJFSNAAM"]);
        } else {
            $query = $pdo->prepare('select ID from bedrijf where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSNAAM like :bedrijfsnaam limit 1;');
            $likeBedrijfsnaam = '%' . substr($_REQUEST["SRCH_BEDRIJFSNAAM"], 1,
                    strlen($_REQUEST["SRCH_BEDRIJFSNAAM"])) . '%';
            $query->bindValue('bedrijfsnaam', $likeBedrijfsnaam);
        }
        $query->execute();
        $dBedrijf = $query->fetch(PDO::FETCH_ASSOC);

        if ($dBedrijf !== false) {
            $sql_toevoeging[] = array(" AND $tabel.BEDRIJFSID = :bedrijfsid ", array('bedrijfsid' => $dBedrijf["ID"]));
        }
    }
    if (isset($_REQUEST["SRCH_YEAR"]) && $_REQUEST["SRCH_YEAR"] != "x" && $_REQUEST["SRCH_YEAR"] != "") {
        $_REQUEST["SRCH_YEAR"] = htmlspecialchars_decode($_REQUEST["SRCH_YEAR"]);

        if ($tabel == "oft_trainingsschema") {
            $sql_toevoeging[] = array(
                " AND $tabel.DUEDATE like :likeduedate ",
                array('likeduedate' => substr($_REQUEST["SRCH_YEAR"], 1, strlen($_REQUEST["SRCH_YEAR"])) . '-%')
            );
        } elseif ($tabel == "oft_annual_accounts" || $tabel == "oft_tax") {
            $sql_toevoeging[] = array(
                " AND $tabel.STANDARDDUEDATE like :standardduedate",
                array('standardduedate' => substr($_REQUEST["SRCH_YEAR"], 1, strlen($_REQUEST["SRCH_YEAR"])) . '-%')
            );
        } elseif ($tabel == "notificaties") {
            $sql_toevoeging[] = array(
                " AND $tabel.VERVALDATUM like :vervaldatumlike ",
                array('vervaldatumlike' => substr($_REQUEST["SRCH_YEAR"], 1, strlen($_REQUEST["SRCH_YEAR"])) . '-%')
            );
        } else {
            $sql_toevoeging[] = array(
                " AND ($tabel.JAAR*1) = :jaar ",
                array('jaar' => substr($_REQUEST["SRCH_YEAR"], 1, strlen($_REQUEST["SRCH_YEAR"])))
            );
        }
    }
    if (isset($_REQUEST["SRCH_FINANCIALYEAR"]) && $_REQUEST["SRCH_FINANCIALYEAR"] != "x" && $_REQUEST["SRCH_FINANCIALYEAR"] != "") {
        $sql_toevoeging[] = array(
            " AND $tabel.FINANCIALYEAR like :FINANCIALYEAR ",
            array(
                'FINANCIALYEAR' => substr($_REQUEST["SRCH_FINANCIALYEAR"], 1, strlen($_REQUEST["SRCH_FINANCIALYEAR"]))
            )
        );
    }
    if (isset($_REQUEST["SRCH_ENDREDEN"]) && $_REQUEST["SRCH_ENDREDEN"] != "x" && $_REQUEST["SRCH_ENDREDEN"] != "") {
        $_REQUEST["SRCH_ENDREDEN"] = htmlspecialchars_decode($_REQUEST["SRCH_ENDREDEN"]);
        $SRCH_ENDREDEN = substr($_REQUEST["SRCH_ENDREDEN"], 1, strlen($_REQUEST["SRCH_ENDREDEN"]));

        $sql_toevoeging[] = array(" AND $tabel.ENDREDEN = :endreden ", array('endreden' => $SRCH_ENDREDEN));
    }
    if (isset($_REQUEST["SRCH_MONTH"]) && $_REQUEST["SRCH_MONTH"] != "x" && $_REQUEST["SRCH_MONTH"] != "") {
        $_REQUEST["SRCH_MONTH"] = htmlspecialchars_decode($_REQUEST["SRCH_MONTH"]);
        $SRCH_MONTH = substr($_REQUEST["SRCH_MONTH"], 1, strlen($_REQUEST["SRCH_MONTH"]));
        if (strlen($SRCH_MONTH) == 1) {
            $SRCH_MONTH = "0" . $SRCH_MONTH;
        }

        $sql_toevoeging[] = array(" AND $tabel.MONTH = :month ", array('month' => $SRCH_MONTH));
    }
    if (isset($_REQUEST["SRCH_DATUM_VAN"]) && trim($_REQUEST["SRCH_DATUM_VAN"]) != "") {
        $_REQUEST["SRCH_DATUM_VAN"] = htmlspecialchars_decode($_REQUEST["SRCH_DATUM_VAN"]);

        $sql_toevoeging[] = array(
            " AND $tabel.DATUM >= :datum ",
            array('datum' => substr($_REQUEST["SRCH_DATUM_VAN"], 0, strlen($_REQUEST["SRCH_DATUM_VAN"])))
        );
    }
    if (isset($_REQUEST["SRCH_DATUM_TOT"]) && trim($_REQUEST["SRCH_DATUM_TOT"]) != "") {
        $_REQUEST["SRCH_DATUM_TOT"] = htmlspecialchars_decode($_REQUEST["SRCH_DATUM_TOT"]);

        $sql_toevoeging[] = array(
            " AND $tabel.DATUM <= :datumtot ",
            array('datumtot' => substr($_REQUEST["SRCH_DATUM_TOT"], 0, strlen($_REQUEST["SRCH_DATUM_TOT"])))
        );
    }
    if (isset($_REQUEST["SRCH_SUSPEND"]) && $_REQUEST["SRCH_SUSPEND"] != "x" && $_REQUEST["SRCH_SUSPEND"] != "") {
        $_REQUEST["SRCH_SUSPEND"] = htmlspecialchars_decode($_REQUEST["SRCH_SUSPEND"]);

        $sql_toevoeging[] = array(
            " AND $tabel.SUSPEND = :suspend ",
            array('suspend' => substr($_REQUEST["SRCH_SUSPEND"], 1, strlen($_REQUEST["SRCH_SUSPEND"])))
        );
    }
    if (isset($_REQUEST["SRCH_DOCTYPE"]) && $_REQUEST["SRCH_DOCTYPE"] != "" && $_REQUEST["SRCH_DOCTYPE"] != "x") {
        $_REQUEST["SRCH_DOCTYPE"] = htmlspecialchars_decode($_REQUEST["SRCH_DOCTYPE"]);

        $sql_toevoeging[] = array(
            " AND $tabel.DOCTYPE = :doctype ",
            array('doctype' => substr($_REQUEST["SRCH_DOCTYPE"], 1, strlen($_REQUEST["SRCH_DOCTYPE"])))
        );
    }
    if (isset($_REQUEST["UPDATEDBY"]) && $_REQUEST["UPDATEDBY"] != "" && $_REQUEST["UPDATEDBY"] != "x") {
        $_REQUEST["UPDATEBY"] = htmlspecialchars_decode($_REQUEST["UPDATEDBY"]);

        $sql_toevoeging[] = array(
            " AND ($tabel.PERSONEELSLID) = :personeelslid ",
            array('personeelslid' => substr($_REQUEST["UPDATEDBY"], 1, strlen($_REQUEST["UPDATEDBY"])))
        );
    }
    if (isset($_REQUEST["EMPLOYEE"]) && $_REQUEST["EMPLOYEE"] != "" && $_REQUEST["EMPLOYEE"] != "x") {
        $_REQUEST["EMPLOYEE"] = htmlspecialchars_decode($_REQUEST["EMPLOYEE"]);

        $sql_toevoeging[] = array(
            " AND ($tabel.EMPLOYEE*1) = :employee ",
            array('employee' => substr($_REQUEST["EMPLOYEE"], 1, strlen($_REQUEST["EMPLOYEE"])))
        );
    }
    if (isset($_REQUEST["SRCH_EVENT"]) && $_REQUEST["SRCH_EVENT"] != "**" && $_REQUEST["SRCH_EVENT"] != "" && $_REQUEST["SRCH_EVENT"] != "x") {
        $_REQUEST["SRCH_EVENT"] = htmlspecialchars_decode($_REQUEST["SRCH_EVENT"]);

        $sql_toevoeging[] = array(
            " AND $tabel.EVENT = :event ",
            array('event' => substr($_REQUEST["SRCH_EVENT"], 1, strlen($_REQUEST["SRCH_EVENT"])))
        );
    }
    if (isset($_REQUEST["SRCH_SOORT"]) && $_REQUEST["SRCH_SOORT"] != "x" && $_REQUEST["SRCH_SOORT"] != "") {
        $_REQUEST["SRCH_SOORT"] = htmlspecialchars_decode($_REQUEST["SRCH_SOORT"]);

        $sql_toevoeging[] = array(
            " AND $tabel.SOORT = :soort ",
            array('soort' => substr($_REQUEST["SRCH_SOORT"], 1, strlen($_REQUEST["SRCH_SOORT"])))
        );
    }
    if (isset($_REQUEST["SRCH_TYPE"]) && $_REQUEST["SRCH_TYPE"] != "x" && $_REQUEST["SRCH_TYPE"] != "") {
        $_REQUEST["SRCH_TYPE"] = htmlspecialchars_decode($_REQUEST["SRCH_TYPE"]);

        $sql_toevoeging[] = array(
            " AND $tabel.TYPE = :type ",
            array('type' => substr($_REQUEST["SRCH_TYPE"], 1, strlen($_REQUEST["SRCH_TYPE"])))
        );
    }
    if (isset($_REQUEST["SRCH_CLASSTYPE"]) && $_REQUEST["SRCH_CLASSTYPE"] != "x" && $_REQUEST["SRCH_CLASSTYPE"] != "") {
        $_REQUEST["SRCH_CLASSTYPE"] = htmlspecialchars_decode($_REQUEST["SRCH_CLASSTYPE"]);

        $sql_toevoeging[] = array(
            " AND $tabel.CLASSTYPE = :classtype ",
            array('classtype' => substr($_REQUEST["SRCH_CLASSTYPE"], 1, strlen($_REQUEST["SRCH_CLASSTYPE"])))
        );
    }
    if (isset($_REQUEST["SRCH_BEDRIJFSID"]) && $_REQUEST["SRCH_BEDRIJFSID"] != "x" && $_REQUEST["SRCH_BEDRIJFSID"] != "") {
        $_REQUEST["SRCH_BEDRIJFSID"] = htmlspecialchars_decode($_REQUEST["SRCH_BEDRIJFSID"]);

        $sql_toevoeging[] = array(
            " AND $tabel.BEDRIJFSID = :bedrijfsid ",
            array('bedrijfsid' => substr($_REQUEST["SRCH_BEDRIJFSID"], 1, strlen($_REQUEST["SRCH_BEDRIJFSID"])))
        );
    }
    if (isset($_REQUEST["SRCH_stam_risklevel"]) && $_REQUEST["SRCH_stam_risklevel"] != "x" && $_REQUEST["SRCH_stam_risklevel"] != "") {
        $_REQUEST["SRCH_stam_risklevel"] = htmlspecialchars_decode($_REQUEST["SRCH_stam_risklevel"]);

        $sql_toevoeging[] = array(
            " AND ($tabel.RISKLEVEL*1) = :risklevel' ",
            array('risklevel' => substr($_REQUEST["SRCH_stam_risklevel"], 1, strlen($_REQUEST["SRCH_stam_risklevel"])))
        );
    }
    if (isset($_REQUEST["SRCH_stam_compliantstatus"]) && $_REQUEST["SRCH_stam_compliantstatus"] != "x" && $_REQUEST["SRCH_stam_compliantstatus"] != "") {
        $_REQUEST["SRCH_stam_compliantstatus"] = htmlspecialchars_decode($_REQUEST["SRCH_stam_compliantstatus"]);

        $sql_toevoeging[] = array(
            " AND ($tabel.STATUS*1) = :status' ",
            array(
                'status' => substr($_REQUEST["SRCH_stam_compliantstatus"], 1,
                    strlen($_REQUEST["SRCH_stam_compliantstatus"]))
            )
        );
    }
    if (isset($_REQUEST["SRCH_TARGETNAME"]) && $_REQUEST["SRCH_TARGETNAME"] != "x" && $_REQUEST["SRCH_TARGETNAME"] != "") {
        $_REQUEST["SRCH_TARGETNAME"] = htmlspecialchars_decode($_REQUEST["SRCH_TARGETNAME"]);
        $sql_toevoeging[] = array(
            " AND LOWER($tabel.TARGETNAME) LIKE LOWER(:targetname) ",
            array(
                'targetname' => '%' . substr($_REQUEST["SRCH_TARGETNAME"], 1,
                        strlen($_REQUEST["SRCH_TARGETNAME"])) . '%'
            )
        );
    }
    if (isset($_REQUEST["SRCH_STATUS"]) && $_REQUEST["SRCH_STATUS"] != "x" && $_REQUEST["SRCH_STATUS"] != "") {
        $_REQUEST["SRCH_STATUS"] = htmlspecialchars_decode($_REQUEST["SRCH_STATUS"]);
        $sql_toevoeging[] = array(
            " AND $tabel.STATUS LIKE :statuslike ",
            array('statuslike' => substr($_REQUEST["SRCH_STATUS"], 1, strlen($_REQUEST["SRCH_STATUS"])) . '%')
        );
    }
    if (isset($_REQUEST["SRCH_SERVICE"]) && $_REQUEST["SRCH_SERVICE"] != "x" && $_REQUEST["SRCH_SERVICE"] != "") {
        $_REQUEST["SRCH_SERVICE"] = htmlspecialchars_decode($_REQUEST["SRCH_SERVICE"]);
        $sql_toevoeging[] = array(
            " AND $tabel.SERVICE = :service ",
            array('service' => substr($_REQUEST["SRCH_SERVICE"], 1, strlen($_REQUEST["SRCH_SERVICE"])))
        );
    }
    if (isset($_REQUEST["SRCH_TARGETSOURCE"]) && $_REQUEST["SRCH_TARGETSOURCE"] != "x" && $_REQUEST["SRCH_TARGETSOURCE"] != "") {
        $_REQUEST["SRCH_TARGETSOURCE"] = htmlspecialchars_decode($_REQUEST["SRCH_TARGETSOURCE"]);
        $sql_toevoeging[] = array(
            " AND $tabel.TARGETSOURCE = :targetsource ",
            array('targetsource' => substr($_REQUEST["SRCH_TARGETSOURCE"], 1, strlen($_REQUEST["SRCH_TARGETSOURCE"])))
        );
    }
    if (isset($_REQUEST["SRCH_JURISDICTION"]) && $_REQUEST["SRCH_JURISDICTION"] != "x" && $_REQUEST["SRCH_JURISDICTION"] != "") {
        $_REQUEST["SRCH_JURISDICTION"] = htmlspecialchars_decode($_REQUEST["SRCH_JURISDICTION"]);
        $sql_toevoeging[] = array(
            " AND $tabel.JURISDICTION = :jurisdiction ",
            array('jurisdiction' => substr($_REQUEST["SRCH_JURISDICTION"], 1, strlen($_REQUEST["SRCH_JURISDICTION"])))
        );

    }
    if (isset($_REQUEST["SRCH_MENAID"]) && $_REQUEST["SRCH_MENAID"] != "" && $_REQUEST["SRCH_MENAID"] != "x") {
        $menaID = htmlspecialchars_decode($_REQUEST["SRCH_MENAID"]);

        if (substr($menaID, 0, 1) == "x") {
            $menaID = substr($menaID, 1, strlen($menaID));
        }
        $sql_toevoeging[] = array(" AND $tabel.MENAID = :menaid ", array('menaid' => $menaID));
    }
    if (isset($_REQUEST["SRCH_LESSON"]) && $_REQUEST["SRCH_LESSON"] != "" && $_REQUEST["SRCH_LESSON"] != "x") {
        $lessonQuery = htmlspecialchars_decode($_REQUEST["SRCH_LESSONID"]);

        if (substr($lessonQuery, 0, 1) == "x") {
            $lessonQuery = substr($lessonQuery, 1, strlen($lessonQuery));
        }
        $sql_toevoeging[] = array(
            " AND ($tabel.LESSONSLEARNED_ONE LIKE :lessonq1 OR $tabel.LESSONSLEARNED_TWO LIKE :lessonq2 OR $tabel.LESSONSLEARNED_THREE LIKE :lessonq3 OR $tabel.LESSONSLEARNED_FOUR LIKE :lessonq4 ",
            array(
                'lessonq1' => '%' . $lessonQuery . '%',
                'lessonq2' => '%' . $lessonQuery . '%',
                'lessonq3' => '%' . $lessonQuery . '%',
                'lessonq4' => '%' . $lessonQuery . '%'
            )
        );
    }
    if (isset($_REQUEST["SRCH_ID"]) && $_REQUEST["SRCH_ID"] != "" && $_REQUEST["SRCH_ID"] != "x") {
        $ID = htmlspecialchars_decode($_REQUEST["SRCH_ID"]);

        if (substr($ID, 0, 1) == "x") {
            $ID = substr($ID, 1, strlen($ID));
        }
        $sql_toevoeging[] = array(" AND $tabel.ID = :id ", array('id' => $ID));
    }
    if (isset($_REQUEST["SRCH_SEARCH"])) {
        $ZOEKVELD = "NAAM";
        if ($tabel == "notificaties") {
            $ZOEKVELD = array("NOTIFICATIENR");
        } elseif ($tabel == "oft_action_tracking") {
            $ZOEKVELD = array("FINDING");
        } elseif ($tabel == "oft_complaint" || $tabel == "oft_incident_register") {
            $ZOEKVELD = array("DESCRIPTION");
        } elseif ($tabel == "documentbeheer") {
            $ZOEKVELD = array("TAGS", "BESTANDSNAAM");
        } elseif ($tabel == "personeel") {
            $ZOEKVELD = array("INITIALEN", "VOORNAAM", "ACHTERNAAM");
        } elseif ($tabel == "personeel") {
            $ZOEKVELD = array("INITIALEN", "VOORNAAM", "ACHTERNAAM");
        } elseif ($tabel == "oft_action_tracking" || $tabel == "oft_action_tracking_audit") {
            $ZOEKVELD = array("REFKEY", "FINDING", "ACTIONTAKEN");
        } elseif ($tabel == "oft_complaint") {
            $ZOEKVELD = array("REFKEY", "DESCRIPTION");
        } elseif ($tabel == "oft_incident_register") {
            $ZOEKVELD = array("INCIDENTNR", "DESCRIPTION");
        } elseif ($tabel == "oft_trainingsschema") {
            $ZOEKVELD = array("TRAINING");
        } elseif ($tabel == "oft_compliance_chart") {
            $ZOEKVELD = array("SCOPE", "LAWS", "RISHAREA");
        } elseif ($tabel == "oft_clients") {
            $ZOEKVELD = array("OFFICE", "NAMECLIENT", "NAMEUBO");
        }

        $sql_zoekvelden = "";
        if (is_array($ZOEKVELD)) {
            foreach ($ZOEKVELD As $key => $ZV) {
                if (trim($_REQUEST["SRCH_SEARCH"]) != "" && trim($_REQUEST["SRCH_SEARCH"]) != "x") {
                    $sql_zoekvelden .= " $ZV LIKE '%" . ps(substr($_REQUEST["SRCH_SEARCH"], 1,
                            strlen($_REQUEST["SRCH_SEARCH"]))) . "%' OR ";
                }
            }
        } elseif ($ZOEKVELD != "" && substr($_REQUEST["SRCH_SEARCH"], 1, strlen($_REQUEST["SRCH_SEARCH"])) != '') {
            $sql_zoekvelden .= " $ZOEKVELD LIKE '%" . ps(substr($_REQUEST["SRCH_SEARCH"], 1,
                    strlen($_REQUEST["SRCH_SEARCH"]))) . "%' OR ";
        }
        if ($sql_zoekvelden != "") {
            $sql_zoekvelden = " AND (" . substr($sql_zoekvelden, 0, (strlen($sql_zoekvelden) - 3)) . ")";
        }

        $sql_toevoeging[] = array($sql_zoekvelden);
    }
    return $sql_toevoeging;
}

function oft_tabel_content(
    $userid = false,
    $bedrijfsid = false,
    $arrayVelden = array(),
    $arrayVeldnamen = array(),
    $arrayVeldType = array(),
    $arrayZoekvelden = array(),
    $tabel = "",
    $order = "ID",
    $toevoegen = "",
    $bewerken = "",
    $verwijderen = "",
    $sql_toevoeging = "",
    $arrayGroup = array(),
    $multiselect = false
) {
    global $pdo;

    $content = "";

    // Check if $table is an existing table.
    if (!empty($tabel)) {
        $tableData = getTableData($tabel);
        if (empty($tableData)) {
            return;
        }
    }

    //Default vars

    // lines per page
    $aantaldocs = 20;
    // current page
    $pagina = 1;

    if (isset($_REQUEST["PAGNR"])) {
        $pagina = $_REQUEST["PAGNR"];
    }

    $url_querystring = oft_tabel_querystring() . "&BID=$bedrijfsid";

    // Exceptions
    $url_querystring .= searchFieldsConverter($arrayZoekvelden);

    $MENUID = getMenuItem();
    $userId = getMySelf();
    $bewLinkToev = "&BID=" . $bedrijfsid;
    if ($MENUID != "") {
        $bewLinkToev .= "&MENUID=" . $MENUID;
    }
    $lSorteerop = $order;
    $lSorteeropAlt = false;
    $distinct = "distinct";
    $join = "";

    if (isset($_REQUEST["SORTEEROP"]) && $_REQUEST["SORTEEROP"] != "") {
        $lSorteerop = $_REQUEST["SORTEEROP"];
        if (str_starts_with($lSorteerop, 'pivot|')) {
            $fields = explode('|', $lSorteerop);
            if (count($fields) == 4) {
                // Exception, because notifications already joins the bedrijf table
                if ($tabel != "notificaties" || $fields[2] != 'bedrijf') {

                    // IF using this, Audittrail doesnt work when sorting on company, when there are rows, with bedrijfsid = 0
                    // $join .= "LEFT JOIN $fields[2] ON $fields[2].ID = $tabel.$fields[1]";

                    $join .= "JOIN $fields[2] ON $fields[2].ID = $tabel.$fields[1]";
                }
                $distinct = "";
                $lSorteeropAlt = $lSorteerop;
                $lSorteerop = $fields[2] . "." . $fields[3];
            }
        }
    }

    $lOrder = "ASC";
    if (isset($_REQUEST["SORTORDER"])) {
        $lOrder = (($_REQUEST["SORTORDER"] == "ASC") ? "DESC" : "ASC");
    }

    $query_limit_rsdetail = "";
    if (($pagina != -1) && ($aantaldocs * ($pagina - 1)) >= 0) {
        $query_limit_rsdetail = " limit " . ($aantaldocs * ($pagina - 1)) . ", $aantaldocs;";
    }

    //Opbouw query
    $whereString = '';
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $query) {
        $whereString .= $query[0];
    }

    $where = "WHERE (!$tabel.DELETED AND $tabel.DELETED = 'Nee')" . $whereString . " " . $sql_toevoeging;
    if ($tabel == "stam_contracttype" || $tabel == "ingelogt") {
        $where = "WHERE TRUE " . $whereString . " " . $sql_toevoeging;
    }

    $orderby = " ORDER BY $lSorteerop $lOrder";

    if ($tabel == "login") {
        $where .= " GROUP BY USERNAME";
    }

    if ($tabel == "notificaties") {
        if (check("DIF", "Nee", $bedrijfsid) == "Ja") {
            $sql_clean = "SELECT $distinct $tabel.*
              FROM $tabel, bedrijf
              " . $join . "
              " . $where . "
              AND bedrijf.ID IN (SELECT BEDRIJFSID FROM oft_rechten_tot WHERE USERID = '" . $userId . "' AND LEGAL='Ja')
              AND notificaties.BEDRIJFSID = bedrijf.ID
              AND STATUS = 'Active'
              AND (NOT bedrijf.DELETED = 'Ja' OR bedrijf.DELETED is null)
              AND bedrijf.MONITOR = 'Ja'";
        } else {
            $sql_clean = "SELECT $distinct $tabel.*
              FROM $tabel, bedrijf
              " . $join . "
              " . $where . "
              AND bedrijf.ID IN (SELECT BEDRIJFSID FROM oft_rechten_tot WHERE USERID = '" . $userId . "' AND LEGAL='Ja')
              AND notificaties.BEDRIJFSID = bedrijf.ID
              AND bedrijf.MONITOR = 'Ja'
              AND STATUS = 'Active'
              AND (NOT bedrijf.DELETED = 'Ja' OR bedrijf.DELETED is null)
              AND (bedrijf.ENDDATUM = '0000-00-00' OR bedrijf.ENDDATUM is null OR bedrijf.ENDDATUM = '')
              AND bedrijf.MONITOR = 'Ja'";
        }

        $sql = $sql_clean . $orderby . $query_limit_rsdetail;
        //echo $sql;
        $query = $pdo->prepare($sql);

        $sql_count = str_replace("distinct notificaties.*", "COUNT(notificaties.ID)", $sql_clean);
        $sql_count = str_replace("notificaties.*", "COUNT(notificaties.ID)", $sql_count);
        $queryCount = $pdo->prepare($sql_count);
        foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $queryPart) {
            if (isset($queryPart[1])) {
                foreach ($queryPart[1] as $key => $value) {
                    //echo "<Br/>".$key .":".$value;
                    $queryCount->bindValue($key, $value);
                }
            }
        }
        $queryCount->execute();
        $totaalAantalRegels = $queryCount->fetchColumn();
    }
    elseif (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "oft_search") {
        $sqlSEARCHDoc = "";
        $sqlSEARCHContract = "";
        if (isset($_REQUEST["SRCH_SEARCH"]) && $_REQUEST["SRCH_SEARCH"] != '') {
            $sqlSEARCHDoc = "AND (NAAM LIKE '%" . ps($_REQUEST["SRCH_SEARCH"]) . "%' OR BESTANDSNAAM LIKE '%" . ps($_REQUEST["SRCH_SEARCH"]) . "%')";
            $sqlSEARCHContract = "AND (REFERENTIE LIKE '%" . ps($_REQUEST["SRCH_SEARCH"]) . "%' OR ONDERTEKENDDOOR LIKE '%" . ps($_REQUEST["SRCH_SEARCH"]) . "%' OR NAAM LIKE '%" . ps($_REQUEST["SRCH_SEARCH"]) . "%')";
        }

        if (isset($_REQUEST["SRCH_BEDRIJFSNAAM"]) && $_REQUEST["SRCH_BEDRIJFSNAAM"] != '') {
            $sqlSEARCHDoc .= " AND BEDRIJFSID = '" . ps($_REQUEST["SRCH_BEDRIJFSNAAM"], "nr") . "'";
            $sqlSEARCHContract .= " AND BEDRIJFSID = '" . ps($_REQUEST["SRCH_BEDRIJFSNAAM"], "nr") . "'";
        }

        if (isset($_REQUEST["SRCH_JAAR"]) && $_REQUEST["SRCH_JAAR"] != '') {
            $sqlSEARCHDoc .= " AND JAAR like '" . ps($_REQUEST["SRCH_JAAR"]) . "%'";
            $sqlSEARCHContract .= " AND DATUMSTART like '" . ps($_REQUEST["SRCH_JAAR"]) . "-%'";
        }

        if (isset($_REQUEST["SRCH_DOCTYPE"]) && $_REQUEST["SRCH_DOCTYPE"] != '') {
            $sqlSEARCHDoc .= " AND DOCTYPE = '" . ps($_REQUEST["SRCH_DOCTYPE"]) . "'";
            $sqlSEARCHContract .= " AND 'CONTRACT' = '" . ps($_REQUEST["SRCH_DOCTYPE"]) . "'";
        }

        if (isset($_REQUEST["SRCH_ACHTERNAAM"]) && $_REQUEST["SRCH_ACHTERNAAM"] > 0) {
            $sqlSEARCHDoc .= " AND EMPLOYEE = '" . ps($_REQUEST["SRCH_ACHTERNAAM"], "nr") . "'";
            $sqlSEARCHContract .= " AND FALSE";
        }

        if (isset($_REQUEST["SRCH_COUNTRY"]) && $_REQUEST["SRCH_COUNTRY"] > 0) {
            $sqlSEARCHDoc .= " AND COUNTRY = '" . ps($_REQUEST["SRCH_COUNTRY"], "nr") . "'";
            $sqlSEARCHContract .= " AND FALSE";
        }

        if (isset($_REQUEST["SRCH_SECTIE"]) && $_REQUEST["SRCH_SECTIE"] != "*" && $_REQUEST["SRCH_SECTIE"] != "") {
            if ($_REQUEST["SRCH_SECTIE"] == "Contracts") {
                $sqlSEARCHDoc .= " AND FALSE";
                $sqlSEARCHContract .= " AND TRUE";
            } else {
                $sqlSEARCHDoc .= getDoctypesBySection($_REQUEST["SRCH_SECTIE"], false);
                $sqlSEARCHContract .= " AND FALSE";
            }
        }

        //TODO: uitbreiden met extra zoekvelden.
        // check op m&a overview
        if(check("oft_search_m&a", "Nee", $bedrijfsid) == "Ja") {
            $query = $pdo->prepare('SELECT ID, NAAM, MONTH, SUBSTRING(JAAR, 1, 4) As JAAR, DATUM, DOCTYPE, BEDRIJFSID, BESTANDSNAAM
               FROM documentbeheer
              WHERE (NOT DELETED = "Ja" OR DELETED is null) 
              ' . $sqlSEARCHDoc . '
              ORDER BY NAAM' . $query_limit_rsdetail);
        }
        elseif(check("search_in_entities", "Nee", $bedrijfsid) == "Ja") {
            $search = "";
            if(isset($_REQUEST['SRCH_SEARCH']) && $_REQUEST['SRCH_SEARCH'] != "") {
                $search = "AND 
					 	(entities.NAME LIKE '%".$_REQUEST['SRCH_SEARCH']."%' 
						 OR entities.WEBSITE LIKE '%".$_REQUEST['SRCH_SEARCH']."%'
						 OR entities.ADDRESS LIKE '%".$_REQUEST['SRCH_SEARCH']."%'
						 OR entities.HEADLINE_COMMENT LIKE '%".$_REQUEST['SRCH_SEARCH']."%'
						 OR entities.DESCRIPTION LIKE '%".$_REQUEST['SRCH_SEARCH']."%') ";
            }
            $query = $pdo->prepare('SELECT entities.ID, entities.NAME as target, countries.NAME as country, entities.RATE_POTENTIAL as rating, entities.WEBSITE as website, stages.NAME as stage, entities.REVENUE as revenue, entities.EBITDA as ebitda, entities.HEADCOUNT as headcount
                FROM entities
                LEFT JOIN countries ON entities.COUNTRY_ID = countries.ID
                LEFT JOIN stages ON entities.STAGE_ID = stages.ID
                WHERE entities.DELETED = 0 '.$search.'ORDER BY entities.NAME');
        }
        else {
            $query = $pdo->prepare('SELECT ID, NAAM, MONTH, SUBSTRING(JAAR, 1, 4) As JAAR, DATUM, DOCTYPE, BEDRIJFSID, BESTANDSNAAM
               FROM documentbeheer
              WHERE (NOT DELETED = "Ja" OR DELETED is null) 
              ' . $sqlSEARCHDoc . '
              ORDER BY NAAM' . $query_limit_rsdetail);
        }

        /*
    $query = $pdo->prepare('SELECT ID, NAAM, MONTH, SUBSTRING(JAAR, 1, 4) As JAAR, DATUM, DOCTYPE, BEDRIJFSID, BESTANDSNAAM
               FROM documentbeheer
              WHERE (NOT DELETED = "Ja" OR DELETED is null)
              '. $sqlSEARCHDoc . '
              ORDER BY NAAM
             ) UNION (
              SELECT ID, NAAM, DATE_FORMAT(DATUMSTART, "%m") As MONTH, DATUMSTART As DATUM, DATE_FORMAT(DATUMSTART, "%Y") As JAAR, "Contract" As DOCTYPE, BEDRIJFSID, "" As BESTANDSNAAM
                FROM contracten
               WHERE (NOT DELETED = "Ja" OR DELETED is null)
               ' . $sqlSEARCHContract . '
               ORDER BY NAAM
             )
             ORDER BY NAAM'  . ' ' . $query_limit_rsdetail);
    */

        //Reken snel uit wat het aantal regel is.
        $queryAantalRegels = $pdo->prepare('SELECT COUNT(' . ps($tabel, "tabel") . '.ID) FROM ' . ps($tabel,
                "tabel") . ' ' . $where . ';');
        foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $queryPart) {
            if (isset($queryPart[1])) {
                foreach ($queryPart[1] as $key => $value) {
                    $queryAantalRegels->bindValue($key, $value);
                }
            }
        }
        $queryAantalRegels->execute();
        $totaalAantalRegels = $queryAantalRegels->fetchColumn();
    } else {
        
        if ($tabel == 'documentbeheer' && !hasRights('administrator')) {
            $where .= " AND PERSONEELSLID = $userId ";
        }
        $statement = "SELECT $distinct $tabel.* FROM $tabel $join $where $orderby $query_limit_rsdetail";
//        die($statement);
//        $content .= $statement;
        $query = $pdo->prepare($statement);

        //Reken snel uit wat het aantal regel is.
        //echo "sql: ".'SELECT COUNT(' . ps($tabel, "tabel") . '.ID) FROM ' . ps($tabel, "tabel") . ' ' . $where . ';';
        $queryAantalRegels = $pdo->prepare('SELECT COUNT(' . ps($tabel, "tabel") . '.ID) FROM ' . ps($tabel,
                "tabel") . ' ' . $where . ';');
        foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $queryPart) {
            if (isset($queryPart[1])) {
                foreach ($queryPart[1] as $key => $value) {
                    $queryAantalRegels->bindValue($key, $value);
                }
            }
        }
        $queryAantalRegels->execute();
        $totaalAantalRegels = $queryAantalRegels->fetchColumn();
    }

    //Paginanummering
//    if ($tabel == "documentbeheer") {
//        $content .= "<div>";
//    } else {
    $content .= "<div id=\"contentTable_$tabel\">";
//    }

    //Toon header
    $content .= "<table name=\"exportexcel\" class=\"sortable\" width=\"100%\">"; //fixed_headers
    $content .= "<thead><tr>\n";

    $arrayVeldGrootte = array();

    if ($multiselect) {
        $content .= "<th class='sorttable_nosort'><input type='checkbox' name='select_all' value='multi-row-select' onclick='toggleAllCheckboxes(this);'></th>";
    }
    if (isset($_REQUEST["SITE"])) {
        $arrayVeldGrootte["STATUS"] = 80;
        if ($_REQUEST["SITE"] == "oft_entitie_document"
            || $_REQUEST["SITE"] == "oft_entitie_finance"
            || $_REQUEST["SITE"] == "oft_entitie_hr"
            || $_REQUEST["SITE"] == "oft_entitie_document_ajax"
            || $_REQUEST["SITE"] == "oft_entitie_finance_ajax"
            || $_REQUEST["SITE"] == "oft_entitie_hr_ajax") {
            $arrayVeldGrootte["NAAM"] = 150;
            $arrayVeldGrootte["JAAR"] = 75;
            $arrayVeldGrootte["DOCTYPE"] = 80;
            $arrayVeldGrootte["PERSONEELSLID"] = 120;
            $arrayVeldGrootte["DATUM"] = 100;
            $arrayVeldGrootte["BEDRIJFSID"] = 120;
        }
        if ($_REQUEST["SITE"] == "oft_document"
            || $_REQUEST["SITE"] == "oft_finance"
            || $_REQUEST["SITE"] == "oft_hr"
            || $_REQUEST["SITE"] == "oft_document_ajax"
            || $_REQUEST["SITE"] == "oft_finance_ajax"
            || $_REQUEST["SITE"] == "oft_hr_ajax") {
            $arrayVeldGrootte["NAAM"] = 200;
            $arrayVeldGrootte["JAAR"] = 75;
            $arrayVeldGrootte["DOCTYPE"] = 80;
            $arrayVeldGrootte["PERSONEELSLID"] = 120;
            $arrayVeldGrootte["DATUM"] = 100;
            $arrayVeldGrootte["BEDRIJFSID"] = 120;
        }
    }

    //Bepaal ordering
    foreach ($arrayVeldnamen as $Key => $Veldnaam) {

        $aParameters = "";
        $lOrder = "";
//        if ($lSorteerop == $arrayVelden[$Key]) {
        if ($lSorteerop == $arrayVelden[$Key] || $lSorteeropAlt == $arrayVelden[$Key]) {
            if (isset($_REQUEST["SORTORDER"])) {
                $lOrder = (($_REQUEST["SORTORDER"] == "ASC") ? "DESC" : "ASC");
            } else {
                $lOrder = "ASC";
                $aParameters = "&SORTORDER=$lOrder";
            }
        }

        $breedte = "";
        if (isset($arrayVeldGrootte[$arrayVelden[$Key]])) {
            $breedte = "width: " . $arrayVeldGrootte[$arrayVelden[$Key]] . "px;";
        }
        $sortIcon = "";
        if ($Veldnaam) {
            $sortIcon = "<span class='filter-icon'>&#9650;</span>";
        }
        if ($lSorteerop == $arrayVelden[$Key] || $lSorteeropAlt == $arrayVelden[$Key]) {
            $sortIcon = "<span class='filter-icon active'>&#9650;</span>";
            if ($lOrder == "DESC") {
                $sortIcon = "<span class='filter-icon active'>&#9660;</span>";
            }
        }

        $onClickUrl = "$url_querystring&tableid=$tabel&PAGNR=$pagina&SORTEEROP={$arrayVelden[$Key]}$aParameters";
        $title = trim("$Veldnaam $sortIcon");

        if ($arrayVeldType[$Key] == "bedrag" || $arrayVeldType[$Key] == "year") {
            $breedte .= "text-align: right;";
        }
        if (($arrayVeldType[$Key] == "bedrag" || $arrayVeldType[$Key] == "year")
            || (substr($arrayVelden[$Key], 0, 4) != "SKIP" || $arrayVelden[$Key] == "SKIP_INCIDENT_ACTIONS")) {
            $content .= "<th style='$breedte'><a class='cursor' onClick=\"getPageContent2('$onClickUrl', 'contentTable_$tabel', searchReq2);\">$title</a></th>\n";
        }

        $SortOrder[$Key] = $lOrder;
    }

    $content .= "</tr></thead><tbody>\n";

//Toon resultaten
    $sectionChange = array();
    foreach ($arrayGroup as $field) {
        $sectionChange[$field] = '';
    }

    $teller = 0;
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $queryPart) {
        $whereString .= $queryPart[0];
        if (isset($queryPart[1])) {
            foreach ($queryPart[1] as $key => $value) {
                $query->bindValue($key, $value);
            }
        }
    }

    $query->execute();

    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $data) {
        !isset($data["BEDRIJFSID"]) ? $data["BEDRIJFSID"] = $bedrijfsid : "";
        $row = $data;
        $twistregel = false;
        foreach ($arrayGroup as $field) {
            if ($row[$field] != $sectionChange[$field]) {
                $sectionChange[$field] = $row[$field];
                $twistregel = true;
            }
        }
        $tekst = 'Group';
        if ($twistregel) {
            $group = array();
            foreach ($arrayGroup as $field) {
                if ($row[$field]) {
                    $group[] = $row[$field];
                }
            }
            if (count($group)) {
                $tekst = implode('/', $group);
            }
            $content .= "<tr><td>twistie</td><td>" . $tekst . "</td><td colspan=\"6\"></td></tr>";
        }

        if ($tabel == "bedrijf") {
            //In geval van lijst entiteiten alle resultanten tonen
            $resultaten = 1;
        } else {
            //Check of bedrijven verwijderd zijn.
            $qCheck = $pdo->prepare('select ID from bedrijf where ID = :bedrijfsid AND (NOT DELETED = "Ja" OR DELETED is null)');
            if (isset($data['COMPANY_ID'])) {
                $data["BEDRIJFSID"] = $data['COMPANY_ID'];
            }

            $qCheck->bindValue('bedrijfsid', $data["BEDRIJFSID"]);


            $qCheck->execute();
            $resultaten = $qCheck->rowCount();
        }
        if ($resultaten > 0) {
            $bewerkScriptTr = "";
            $bewerkScript = "";
            if ($tabel == "notificaties") {
                //TODO if geen schrijfrechten, dan andere link
                if ($data["TABEL"] == "oft_annual_accounts") {
                    if (isRechtenOFT($data["BEDRIJFSID"], "LEGAL")) {
                        $bewerken = oft_inputform_link_short("content.php?SITE=oft_annual_accounts_edit&ID=" . ($data["TABELID"] * 1) . "$bewLinkToev");
                    }
                }
                if ($data["TABEL"] == "contracten") {
                    if (isRechtenOFT($data["BEDRIJFSID"], "CONTRACTS")) {
                        $bewerken = oft_inputform_link_short("content.php?SITE=oft_contracten_edit&ID=" . ($data["TABELID"] * 1) . "$bewLinkToev");
                    }
                }
                if ($data["TABEL"] == "personeel") {
                    if (isRechtenOFT($data["BEDRIJFSID"], "HR")) {
                        $bewerken = oft_inputform_link_short("content.php?SITE=oft_persons_edit&ID=" . ($data["TABELID"] * 1) . "$bewLinkToev");
                    }
                }
                if ($data["TABEL"] == "oft_management") {
                    if (isRechtenOFT($data["BEDRIJFSID"], "CRM")) {
                        $bewerken = oft_inputform_link_short("content.php?SITE=oft_entitie_management_edit&ID=" . ($data["TABELID"] * 1) . "$bewLinkToev");
                    }
                }
                if ($data["TABEL"] == "oft_bedrijf_adres") {
                    if (isRechtenOFT($data["BEDRIJFSID"], "LEGAL")) {
                        $bewerken = oft_inputform_link_short("content.php?SITE=oft_entitie_adress_edit&ID=" . ($data["TABELID"] * 1) . "$bewLinkToev");
                    }
                }
                if ($data["TABEL"] == "oft_tax") {
                    if (isRechtenOFT($data["BEDRIJFSID"], "LEGAL")) {
                        $bewerken = oft_inputform_link_short("content.php?SITE=oft_tax_edit&ID=" . ($data["TABELID"] * 1) . "$bewLinkToev");
                    }
                }
                //TODO: wat als je meerdere documenten pagina's hebt. -> wat als je alleen leesrechten hebt?
            }

            if (!isset($_REQUEST["PRINT"]) && !isset($_REQUEST["EXPORTEXCEL"])) {
                if (isRechtenOFT($data["BEDRIJFSID"], "CRM") || isRechtenOFT($data["BEDRIJFSID"],
                        "LEGAL") || $tabel == "notificaties") {
                    if (substr($bewerken, 0, 10) == "javascript") {
                        $bewerkScriptTr = " style=\"\" class=\"table-border cursor\" ";
                        $bewerkScript = "onClick=\"" . str_replace("-ID-", ($data["ID"] * 1) . "$bewLinkToev",
                                $bewerken) . "\"";
                        $tablerow = "<tr data-dk-bedrijfsid=\"1-{$data['BEDRIJFSID']}\" $bewerkScriptTr>";
                    } elseif ($bewerken != "") {
                        $bewerkScriptTr = " style=\"\" class=\"table-border cursor\" ";
                        $bewerkScript = "onClick=\"window.location.href='" . $bewerken . "&ID=" . ($data["ID"] * 1) . $bewLinkToev . "';\"";
                        $tablerow = "<tr data-dk-bedrijfsid=\"2-{$data['BEDRIJFSID']}\" $bewerkScriptTr>";
                    } else {
                        $tablerow = '<tr data-dk-bedrijfsid="3-' . $data['BEDRIJFSID'] . '">';
                    }
                } else {
                    $tablerow = '<!-- no rights to BID: ' . $data['BEDRIJFSID'] . '  -->';
                }
            } else {
                $tablerow = '<!-- print or export BID: ' . $data['BEDRIJFSID'] . ' --><tr>';
            }

            if ($multiselect) {
                $tablerow .= "<td><input type='checkbox' class='multi-row-select' name='row_select[]' value='" . $data['ID'] . "' data-filename='" . $data['NAAM'] . "'></td>";
            }
            foreach ($arrayVelden as $Key => $Veld) { // $content .= "\n<!-- ${Veld} : ${Key} : ${arrayVeldType[$Key]} -->\n";
//                echo "FIELDTYPE: $arrayVeldType[$Key] KEY: $Key<br />";
                if ($arrayVeldType[$Key] == "pivot") {
                    $fields = explode('|', $Veld);
//                    print_r($fields);
//                    echo "<br />";
                    if (count($fields) == 4) {
                        $pivot_results = getPivotValue($data['ID'], $tabel, $fields[1], $fields[2], $fields[3]);
                        $single_result = "";
                        if (is_array($pivot_results)) {
                            if (isset($pivot_results[$fields[3]])) {
                                $single_result = $pivot_results[$fields[3]];
                            }
                        } else {
                            $single_result = $pivot_results;
                        }
                        $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . $single_result . "</td>";
                    }
                } elseif (substr($arrayVeldType[$Key], 0, 20) == "oft_entitie_numbers_") {
                    $query = $pdo->prepare('select * from oft_entitie_numbers where (NOT DELETED = "Ja" OR DELETED is null) AND TYPE = :type AND BEDRIJFSID = :id order by ID;');
                    $query->bindValue('type', str_replace("oft_entitie_numbers_", "", $arrayVeldType[$Key]));
                    $query->bindValue('id', $data["BEDRIJFSID"]);
                    $query->execute();
                    $numbers = $query->fetch(PDO::FETCH_ASSOC);

                    $actionsTaken = '';
                    if ($numbers !== false) {
                        $actionsTaken .= lb($numbers['VALUE']);
                    }
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($actionsTaken) . "</td>";
                } elseif ($arrayVelden[$Key] == "SKIP_INCIDENT_ACTIONS") {
                    $query = $pdo->prepare('select * from oft_incident_register_task where (NOT DELETED = "Ja" OR DELETED is null) AND INCIDENTID = :id order by ID;');
                    $query->bindValue('id', $data["ID"]);
                    $query->execute();
                    $d_actions_taken = $query->fetch(PDO::FETCH_ASSOC);

                    if ($d_actions_taken !== false) {
                        $actionsTaken .= "[" . lb($d_actions_taken["DATUM"]) . " - " . lb($d_actions_taken["STATUS"]) . "] " . lb($d_actions_taken["TASK"]) . "<br/><br/>";
                    }

                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($actionsTaken) . "</td>";
                } elseif ($Veld == "MENAID" && $tabel == "oft_mena_evaluation") {
                    $mena = '';

                    $query = $pdo->prepare('SELECT * FROM oft_mena WHERE ID = :id LIMIT 0,1;');
                    $query->bindValue('id', $data[$Veld]);
                    $query->execute();
                    $d_mena = $query->fetch(PDO::FETCH_ASSOC);

                    if ($d_mena !== false) {
                        $mena = "[" . $d_mena["ID"] . " - " . $d_mena["PROJECTCODE"] . "] " . lb($d_mena["JURISDICTION"]);
                    }
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($mena) . "</td>";
                } elseif ($Veld == "LESSONS" && $tabel == "oft_mena_evaluation") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($mena) . "</td>";
                } elseif ($Veld == "JAAR" && ($tabel == "documentbeheer" || $tabel = "entities")) {
                    if ($data["MONTH"] != '') {
                        $arraymaand = maandenvanhetjaar();
                        $maand_nom = $arraymaand[($data["MONTH"] * 1) - 1]; // TODO: Zeker weten dat dit bedoeld wordt.
                    } else {
                        $maand_nom = '';
                    }

                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . substr($data[$Veld], 0,
                            4) . " " . lb($maand_nom) . "</td>";
                } elseif (substr($arrayVelden[$Key], 0, 4) == "SKIP") {
                } elseif ($arrayVeldType[$Key] == "persentage") {
                    $tablerow .= "<td $bewerkScript valign=\"top\">" . verkort(stripslashes($data[$Veld]),
                            50) . " %</td>";
                } elseif ($arrayVeldType[$Key] == "field") {
                    if (isset($data[$Veld])) {
                        if ($data[$Veld] == "Nee") {
                            $data[$Veld] = "No";
                        } elseif ($data[$Veld] == "Ja") {
                            $data[$Veld] = "Yes";
                        }
                        $url = verkort(lb($data[$Veld]), 70);
                        if($Veld == "target") {
                            $url = "<a href='/content.php?SITE=entity_view&ID=".$data["ID"]."'>".verkort(lb($data[$Veld]), 70)."</a>";
                        }
                        $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . $url . "</td>";
                    } else {
                        $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\"></td>";
                    }
                } elseif ($arrayVeldType[$Key] == "rollback") {
                    if ($data["APPROVED"] != 'Ja' && $data["ACTION"] == 'Change') {
                        $divId = uniqid();
                        $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\"><div id=\"slt" . $divId . "\"><a class=\"btn-action cursor\" onClick=\"if(confirm('" . tl("Weet u het zeker?") . "')) { getPageContent('content.php?SITE=setRollBack&ID=" . ($data[$Veld] * 1) . "', 'slt" . $divId . "'); }\">Undo</a></td>";
                    } else {
                        $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\"></td>";
                    }
                } elseif ($arrayVeldType[$Key] == "home_country") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . getNaam("bedrijf",
                            $data["BEDRIJFSID"], "LAND") . "</td>";
                } elseif ($arrayVeldType[$Key] == "approved") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . approved($data[$Veld],
                            $data["ID"]) . "</td>";
                } elseif ($arrayVeldType[$Key] == "tabelSource") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . getBetterSourceName($data[$Veld]) . "</td>";
                } elseif ($arrayVeldType[$Key] == "auditAction") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . substr($data[$Veld], 0,
                            1) . "</td>";
                } elseif ($arrayVeldType[$Key] == "columnName") {
                    //Vertaling van Veld naar leesbare waardes
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . vertaalDatabaseKolomNaarLeesbaar($userid,
                            $bedrijfsid, $data["TABEL"], $data[$Veld]) . "</td>";
                } elseif ($arrayVeldType[$Key] == "bestand") {
                    $tablerow .= "<td align=\"left\">";
                    if ($data['ID'] > 0) {
                        $tablerow .= "<a href=\"content.php?SITE=entity_getFileCloudVPS_ajax&ID=" . ($data["ID"] * 1) . "\">" . verkort($data[$Veld],
                                50) . "</a>";
                    } else {
                        $tablerow .= verkort($data[$Veld], 50);
                    }
                    $tablerow .= "</td>";
                } elseif ($arrayVeldType[$Key] == "bestand_from_type") {
                    $bestand_from_type = "";

                    $sql_bestand_from_type = "";
                    $query = $pdo->prepare('select ID, BESTANDSNAAM from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND DOCTYPE = "" order by ID limit 1;');
                    $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                    if ($tabel == "oft_shareholders") {
                        $DOCTYPE = "Deed of pledge";
                        $sql_bestand_from_type = "";
                        $query = $pdo->prepare('select ID, BESTANDSNAAM from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND DOCTYPE = :doctype order by ID limit 1;');
                        $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                        $query->bindValue('doctype', $DOCTYPE);
                    } elseif ($tabel == "contracten") {
                        $DOCTYPE = "Contract";
                        $sql_bestand_from_type = "";
                        $query = $pdo->prepare('select ID, BESTANDSNAAM from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND TABELID = :tabelid order by ID limit 1;');
                        $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                        $query->bindValue('tabelid', $data["ID"]);
                    }

                    $query->execute();
                    $dbestand_from_type = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dbestand_from_type !== false) {
                        $bestand_from_type = "<a href=\"document.php?DID=" . $dbestand_from_type["ID"] . "\">" . verkort($dbestand_from_type["BESTANDSNAAM"],
                                50) . "</a>";
                    }
                    $tablerow .= "<td align=\"left\" valign=\"top\">$bestand_from_type</td>";
                } elseif ($arrayVeldType[$Key] == "nr_of_docs") {
                    $nr_of_docs = "";
                    $DOCTYPE = "";
                    if ($tabel == "oft_tax") {
                        $DOCTYPE = "Tax filing";
                    } elseif ($tabel == "oft_annual_accounts") {
                        $DOCTYPE = "Annual account";
                    }
                    $sql_bestand_from_type = "";
                    $query = $pdo->prepare('select COUNT(*) As Aantal from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND JAAR = :jaar AND DOCTYPE = :doctype GROUP BY BEDRIJFSID;');
                    $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                    $query->bindValue('jaar', $data["FINANCIALYEAR"]);
                    $query->bindValue('doctype', $DOCTYPE);
                    $query->execute();
                    $dbestand_from_type = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dbestand_from_type !== false) {
                        $nr_of_docs = $dbestand_from_type["Aantal"];
                    }
                    $tablerow .= "<td align=\"left\" valign=\"top\">$nr_of_docs</td>";
                } elseif ($arrayVeldType[$Key] == "bestand_in_year_from_type") {
                    $bestand_from_type = "";
                    $DOCTYPE = "";
                    if ($tabel == "oft_tax") {
                        $DOCTYPE = "Tax %";
                    } elseif ($tabel == "oft_annual_accounts") {
                        $DOCTYPE = "Annual account%";
                    }

                    $sql_bestand_from_type = "";
                    $query = $pdo->prepare("select ID, BESTANDSNAAM
                                              from documentbeheer 
                                             where (NOT DELETED = 'Ja' OR DELETED is null)
                                               AND BEDRIJFSID = :bedrijfsid
                                               AND JAAR = :jaar
                                               AND DOCTYPE like :doctype
                                             order by ID DESC 
                                             limit 1;");

                    $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                    $query->bindValue('jaar', $data["FINANCIALYEAR"]);
                    $query->bindValue('doctype', $DOCTYPE);
                    $query->execute();
                    $dbestand_from_type = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dbestand_from_type !== false) {            // $content .= "\n<!-- ${dbestand_from_type['ID']} -->\n";
                        $bestand_from_type = "<a href=\"document.php?DID=" . $dbestand_from_type["ID"] . "\">" . verkort($dbestand_from_type["BESTANDSNAAM"],
                                50) . "</a>";
                    }
                    $tablerow .= "<td align=\"left\" valign=\"top\">$bestand_from_type</td>";
                } elseif ($arrayVeldType[$Key] == "datum" || $arrayVeldType[$Key] == "datumleeg") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . datumconversiePresentatieKort($data[$Veld]) . "</td>";
                } elseif ($arrayVeldType[$Key] == "year") {
                    $tablerow .= "<td $bewerkScript align=\"right\" valign=\"top\">" . getJaar($data[$Veld]) . "</td>";
                } elseif ($arrayVeldType[$Key] == "status") {
                    if ($tabel == "notificaties") {
                        $status_img = getNotificatieStatus(array(
                            'status' => $data[$Veld],
                            'due_date' => $data["VERVALDATUM"],
                            'table' => $data["TABEL"],
                            'table_id' => $data["TABELID"]
                        ));
                    } else {
                        if (isset($data["EXTENTION"]) && $data["EXTENTION"] != "" && $data["EXTENTION"] != "0000-00-00") {
                            $vervaldatum = $data["EXTENTION"];
                        } else {
                            $vervaldatum = $data["STANDARDDUEDATE"];
                        }
                        $status_img = getNotificatieStatus(array(
                            'status' => $data[$Veld],
                            'due_date' => $vervaldatum,
                            'table' => $tabel,
                            'table_id' => $data["ID"]
                        ));
                    }
                    $tablerow .= "<td $bewerkScript align=\"right\" valign=\"top\" width=\"20\">" . $status_img . "</td>";
                } elseif ($arrayVeldType[$Key] == "countryofresidence") {
                    $image = "<img src=\"./images/oft/oft_red_circle.png\" border=\"0\" />";
                    if (isset($_REQUEST["EXPORTEXCEL"])) {
                        $image = "To do";
                    }
                    if (checkCountryOfResidence($bedrijfsid, $data[$Veld])) {
                        $image = "<img src=\"./images/oft/oft_green_circle.png\" border=\"0\" />";
                        if (isset($_REQUEST["EXPORTEXCEL"])) {
                            $image = "Done";
                        }
                    }
                    if (($data[$Veld] * 1) == 0 && isset($data["ADMINID"])) {
                        $image = "<img src=\"./images/oft/oft_red_circle.png\" border=\"0\" />";
                        if (isset($_REQUEST["EXPORTEXCEL"])) {
                            $image = "To do";
                        }
                        if (checkCountryOfResidence($bedrijfsid, 0, $data["ADMINID"])) {
                            $image = "<img src=\"./images/oft/oft_green_circle.png\" border=\"0\" />";
                            if (isset($_REQUEST["EXPORTEXCEL"])) {
                                $image = "Done";
                            }
                        }
                    }
                    $tablerow .= "<td $bewerkScript>$image</td>";
                } elseif ($arrayVeldType[$Key] == "medewerker" || $arrayVeldType[$Key] == "personeel") {
                    $tablerow .= "<td $bewerkScript>" . getPersoneelslidNaam($data[$Veld]) . "</td>";
                } elseif ($arrayVeldType[$Key] == "bedrijfsnaam") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . getNaam("bedrijf", $data[$Veld],
                            "BEDRIJFSNAAM", 50, "") . "</td>";
                } elseif ($arrayVeldType[$Key] == "relatie") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . getNaamRelatie($data[$Veld]) . "</td>";
                } elseif (substr($arrayVeldType[$Key], 0, 5) == "stam_") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . getNaam($arrayVeldType[$Key],
                            $data[$Veld], "NAAM", 30, "") . "</td>";
                } elseif ($arrayVeldType[$Key] == "person_name") {

                    $nNaam = "";
                    $query = $pdo->prepare('SELECT INITIALEN, TUSSENVOEGSEL, ACHTERNAAM FROM personeel WHERE ID = :id limit 1;');
                    $query->bindValue('id', $data[$Veld]);
                    $query->execute();
                    $dPers = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dPers !== false) {
                        $nNaam = $dPers["INITIALEN"] . " " . $dPers["TUSSENVOEGSEL"] . " " . $dPers["ACHTERNAAM"];
                    }

                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($nNaam) . "</td>";
//                } elseif ($arrayVeldType[$Key] == "fullname") {
//                    $statement = 'SELECT INITIALEN, TUSSENVOEGSEL, ACHTERNAAM FROM personeel WHERE ID = :id limit 1;';
//                    $query = $pdo->prepare($statement);
//                    $query->bindValue('id', $data["USERID"]);
//                    $query->execute();
//                    $names = $query->fetch(PDO::FETCH_ASSOC);
//                    $fullname = "";
//                    $fullname .= (isset($names["INITIALEN"]) && $names["INITIALEN"]) ? "" : $names["INITIALEN"];
//                    $fullname .= (isset($names["TUSSENVOEGSEL"]) && $names["TUSSENVOEGSEL"]) ? "" : $names["TUSSENVOEGSEL"];
//                    $fullname .= (isset($names["ACHTERNAAM"]) && $names["ACHTERNAAM"]) ? "" : $names["ACHTERNAAM"];
//                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($fullname) . "</td>";
                } elseif ($arrayVeldType[$Key] == "get_personeel_email") {

                    $nEmail = "";
                    $query = $pdo->prepare('SELECT EMAIL FROM personeel WHERE ID = :id limit 1;');
                    $query->bindValue('id', $data[$Veld]);
                    $query->execute();
                    $dPers = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dPers !== false) {
                        $nEmail = $dPers["EMAIL"];
                    }

                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($nEmail) . "</td>";
                } elseif ($arrayVeldType[$Key] == "bedrag") {
                    $tablerow .= "<td $bewerkScript align=\"right\" valign=\"top\">&#128; " . prijsconversiePuntenKommas($data[$Veld]) . "</td>";
                } elseif ($arrayVeldType[$Key] == "country") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . getNaam("bedrijf",
                            $data["BEDRIJFSID"], "LAND", 30, "") . "</td>";
                } elseif ($arrayVeldType[$Key] == "boardpacklink") {
                    $username = getValue("login", "PERSONEELSLID", $userid, "USERNAME");
                    $password = getValue("login", "PERSONEELSLID", $userid, "PASSWORD");
                    $database = getCompanyDatabase();
                    //$row .= "<td $bewerkScript align=\"left\" valign=\"top\"><a target=\"_blank\" href=\"https://www.orangefieldonline.com/boardpack/index.php?USERNAME=$username&PASSWORD=$password&DB=$database\">Open</a></td>";
                    //$row .= "<td $bewerkScript align=\"left\" valign=\"top\"><a target=\"_blank\" href=\"./boardpack/index.php?APP_ACTIVE=true\">Open</a></td>";
                } elseif ($arrayVeldType[$Key] == "translate") {
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . tl(lb($data[$Veld])) . "</td>";
                } elseif ($arrayVeldType[$Key] == "AUTHORIZATION") {
                    // Show the value corresponding to the database value
                    $options = getListOptions("AUTHORIZATION");
                    if (array_key_exists($data[$Veld], $options)) {
                        // Show value found in array
                        $value = $options[$data[$Veld]];
                    } else {
                        // Show original value when value not found in array
                        $value = $data[$Veld];
                    }
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . tl(lb($value)) . "</td>";
                } else {
                    if ($data[$Veld] == "Nee") {
                        $data[$Veld] = "No";
                    } elseif ($data[$Veld] == "Ja") {
                        $data[$Veld] = "Yes";
                    }
                    $tablerow .= "<td $bewerkScript align=\"left\" valign=\"top\">" . lb($data[$Veld]) . "</td>";
                }
            }
            $tablerow .= "</tr>" . PHP_EOL;

            if ((($tabel == "contracten"
                        || $tabel == "documentbeheer"
                        || $tabel == 'notificaties'
                        || $tabel == "oft_action_tracking_audit"
                        || $tabel == "oft_action_tracking"
                        || $tabel == "oft_incident_register"
                        || $tabel == "oft_complaint"
                    )
                    && isRechtenOFT($data["BEDRIJFSID"], "CRM"))
                ||
                ($tabel != "documentbeheer"
                    && $tabel != "contracten"
                    && $tabel != 'notificaties'
                    && $tabel != "oft_action_tracking_audit"
                    && $tabel != "oft_action_tracking"
                    && $tabel != "oft_incident_register"
                    && $tabel != "oft_complaint"
                )) {
                $content .= $tablerow;
            }
            // reset $tablerow for next iteration
            $tablerow = "";
            // reset $row for next iteration
            $row = "";
            $teller++;
        }
    }

    if ($teller == 0) {
        $content .= "<tr><td colspan=\"" . count($arrayVelden) . "\"><em>No results</em></td><tr>";
    }

    $content .= "</tbody></table>\n";

    $excelUrl = str_replace("_ajax", "", $url_querystring);
    if (isset($_REQUEST["SITE"]) &&
        (
            $_REQUEST["SITE"] == "oft_registers_actiontracking" || $_REQUEST["SITE"] == "oft_registers_actiontracking_ajax"
            || $_REQUEST["SITE"] == "oft_registers_actiontracking_audit" || $_REQUEST["SITE"] == "oft_registers_actiontracking_audit_ajax"
            || $_REQUEST["SITE"] == "oft_registers_incident" || $_REQUEST["SITE"] == "oft_registers_incident_ajax"
            || $_REQUEST["SITE"] == "oft_registers_claim" || $_REQUEST["SITE"] == "oft_registers_claim_ajax"
            || $_REQUEST["SITE"] == "oft_registers_compliant" || $_REQUEST["SITE"] == "oft_registers_compliant_ajax"
        )
    ) {
        $excelUrl = str_replace("_ajax", "_print", $excelUrl);
    }
    if ($totaalAantalRegels > 0) {
        if ($_REQUEST["SITE"] != 'rapport_magaging_director') {

            $url = $url_querystring;
            if (isset($_REQUEST["SORTEEROP"]) && $_REQUEST["SORTEEROP"]) {
                $url .= "&SORTEEROP=" . $_REQUEST["SORTEEROP"];
                if (isset($_REQUEST["SORTORDER"]) && $_REQUEST["SORTORDER"]) {
                    $url .= "&SORTORDER=" . $_REQUEST["SORTORDER"];
                }
            }
            $content .= "<br/><br/>" . paginate($url, $totaalAantalRegels, $aantaldocs, $pagina,
                    ['ajax' => true, 'table' => $tabel]);


            $_REQUEST["SITE"] = str_replace("_ajax", "", $_REQUEST["SITE"]);
            if ($_REQUEST["SITE"] == "oft_registers_incident"
                || $_REQUEST["SITE"] == "oft_registers_actiontracking"
                || $_REQUEST["SITE"] == "oft_registers_actiontracking_audit"
                || $_REQUEST["SITE"] == "oft_registers_compliant"
                || $_REQUEST["SITE"] == "oft_registers_claim"
                || $_REQUEST["SITE"] == "oft_registers_exceptions"
            ) {
                $excelUrl = str_replace($_REQUEST["SITE"], $_REQUEST["SITE"] . "_print", $excelUrl);
                $content .= " <a class=\"button\" onClick=\"window.open('" . $excelUrl . "&PAGNR=$pagina');\">Printable version</a>";
                $content .= " <a class=\"button\" onClick=\"window.open('" . $excelUrl . "&PAGNR=$pagina&EXPORTEXCEL=true');\">Open in excel (1)</a>";
                $content .= " <a class=\"button\" onClick=\"window.open('" . str_replace("_print", "",
                        $excelUrl) . "&PAGNR=$pagina&EXPORTEXCEL=true');\">Open in excel (2)</a>";
            } else {
                $content .= " <a class=\"button\" onClick=\"window.open('" . $excelUrl . "&PAGNR=$pagina&EXPORTEXCEL=true');\">Open in excel</a>";
            }
            $content .= "<br/><br/><br/>$totaalAantalRegels results found. ";
        }
    } else {
        if ($_REQUEST["SITE"] != 'rapport_magaging_director') {
//            $content .= "<br><br>No results found.";
        }
    }

    $content .= "</div>\n";

    return $content;
}

function oft_tabel_content_insert(
    $userid,
    $bedrijfsid,
    $arrayVelden,
    $arrayVeldnamen,
    $arrayVeldType,
    $arrayZoekvelden,
    $tabel,
    $order,
    $toevoegen,
    $bewerken,
    $verwijderen,
    $sql_toevoeging = ""
) {
    global $pdo;

    //Toon header
    $content = PHP_EOL . "<table name=\"exportexcel\" class=\"tabel\" width=\"100%\">" . PHP_EOL;
    $content .= "<tr><th></th>\n";

    //Bepaal ordering
    foreach ($arrayVeldnamen as $Key => $Veldnaam) {
        if ($arrayVeldType[$Key] == "bedrag") {
            $content .= "<th style=\"text-align: right;\">" . lb($Veldnaam) . "</th>\n";
        } else {
            $content .= "<th>" . lb($Veldnaam) . "</th>\n";
        }
    }

    $content .= "</tr>\n";

    //Opbouw query
    //TODO: aanpassen oftgetsearchSQL
    $whereString = '';
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $query) {
        $whereString .= $query[0];
    }
    $where = "WHERE (!DELETED AND DELETED = 'Nee') " . $whereString . " " . $sql_toevoeging;
    $orderby = " ORDER BY $order ";
    $query_limit_rsdetail = "SELECT * FROM " . ps($tabel, "tabel") . " $where $orderby";

    //echo $query_limit_rsdetail;
    $teller = 0;

    $query = $pdo->prepare($query_limit_rsdetail);
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $queryPart) {
        $whereString .= $queryPart[0];
        if (isset($queryPart[1])) {
            foreach ($queryPart[1] as $key => $value) {
                $query->bindValue($key, $value);
            }
        }
    }

    $query->execute();
    foreach ($query->fetchAll() as $data) {
        $content .= "<tr id=\"row" . ($data["ID"] * 1) . "\"><td width=\"20\"><a onclick=\"if(confirm('Do you want to delete?')) { getPageContent2('content.php?SITE=oft_delete_item&tabel=$tabel&ID=" . ($data["ID"] * 1) . "', 'row" . ($data["ID"] * 1) . "', searchReq2);}\"><img border=\"0\" src=\"./images/verwijderen.gif\"></a></td>";

        foreach ($arrayVelden as $Key => $Veld) {
            if ($arrayVeldType[$Key] == "CLASSTYPE") {
                $content .= "<td align=\"left\"><select name=\"" . $tabel . "_" . $Veld . "_" . ($data["ID"] * 1) . "\" class=\"field\" style=\"width: 100%;\">" . selectbox_tabel("oft_transaction_types",
                        "NAAM", "NAAM", $data[$Veld], false,
                        "WHERE (!DELETED AND DELETED = 'Nee')") . "</select></td>";
            } elseif ($arrayVeldType[$Key] == "bedrijfsnaam") {
                $content .= "<td align=\"left\"><select name=\"" . $tabel . "_" . $Veld . "_" . ($data["ID"] * 1) . "\" class=\"field\" style=\"width: 100%;\">" . selectbox_tabel("bedrijf",
                        "ID", "BEDRIJFSNAAM", $data[$Veld], false,
                        "WHERE (!DELETED AND DELETED = 'Nee')") . "</select></td>";
            } elseif ($arrayVeldType[$Key] == "type_classes") {
                $content .= "<td align=\"left\"><select name=\"" . $tabel . "_" . $Veld . "_" . ($data["ID"] * 1) . "\" class=\"field\" style=\"width: 100%;\">" . selectbox_tabel("oft_classes_types",
                        "NAAM", "NAAM", $data[$Veld], false,
                        "WHERE (!DELETED AND DELETED = 'Nee')") . "</select></td>";
            } else {
                $content .= "<td align=\"left\"><input type=\"text\" name=\"" . $tabel . "_" . $Veld . "_" . ($data["ID"] * 1) . "\" class=\"field\" value=\"" . lb($data[$Veld]) . "\" style=\"width: 100%;\" /></td>";
            }
        }

        $content .= "</tr>";
        $teller++;
    }

    //Add blank field
    for ($n = 0; $n <= 10; $n++) {
        $content .= "<tr id=\"regel" . $tabel . "$n\" style=\"display: none;\"><td width=\"20\"></td>";

        foreach ($arrayVelden as $Key => $Veld) {
            if ($arrayVeldType[$Key] == "CLASSTYPE") {
                $content .= "<td align=\"left\"><select name=\"new_" . $tabel . "_" . $Veld . "_" . $n . "\" class=\"field\" style=\"width: 100%;\">" . selectbox(array(
                        'Common' => 'Common',
                        "Sale of share" => "Sale of share",
                        "Pledge of share" => "Pledge of share",
                        "Certification of share" => "Certification of share"
                    ), "Common", false) . "</select></td>";
            } elseif ($arrayVeldType[$Key] == "type_classes") {
                $content .= "<td align=\"left\"><select name=\"new_" . $tabel . "_" . $Veld . "_" . $n . "\" class=\"field\" style=\"width: 100%;\">" . selectbox(array(
                        "Common" => "Common",
                        "Preferred" => "Preferred",
                        "Ordinary" => "Ordinary",
                        "A" => "A",
                        "B" => "B",
                        "C" => "C",
                        "D" => "D",
                        "E" => "E",
                        "F" => "F",
                        "G" => "G",
                        "H" => "H",
                        "I" => "I",
                        "J" => "J",
                        "K" => "K",
                        "L" => "L",
                        "M" => "M",
                        "N" => "N",
                        "O" => "O",
                        "P" => "P",
                        "Q" => "Q",
                        "R" => "R",
                        "S" => "S",
                        "T" => "T",
                        "U" => "U",
                        "V" => "V",
                        "W" => "W",
                        "X" => "X",
                        "Y" => "Y",
                        "Z" => "Z",
                    ), "Common", false) . "</select></td>";
            } elseif ($arrayVeldType[$Key] == "bedrijfsnaam") {
                $content .= "<td align=\"left\"><select name=\"new_" . $tabel . "_" . $Veld . "_" . $n . "\" class=\"field\" style=\"width: 100%;\">" . selectbox_tabel("bedrijf",
                        "ID", "BEDRIJFSNAAM", '', false,
                        "WHERE (!DELETED AND DELETED = 'Nee')") . "</select></td>";
            } else {
                //".$arrayVeldType[$Key]."
                $content .= "<td align=\"left\"><input type=\"text\" name=\"new_" . $tabel . "_" . $Veld . "_" . $n . "\" class=\"field\" style=\"width: 100%;\" /></td>";
            }
        }

        $content .= "</tr>";
    }

    $content .= "</table>";
    if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] != "oft_entity_shareholders_edit") {
        $content .= "<br/><div id=\"counter$tabel\" style=\"display: none;\">1</div><div class=\"cursor\" onClick=\"document.getElementById('counter$tabel').innerHTML=((document.getElementById('counter$tabel').innerHTML*1)+1);toggle('regel$tabel'+document.getElementById('counter$tabel').innerHTML);\">Add</div>\n";
    }

    return $content;
}

function oft_tabel_content_save(
    $userid,
    $bedrijfsid,
    $arrayVelden,
    $arrayVeldnamen,
    $arrayVeldType,
    $arrayZoekvelden,
    $tabel,
    $checkVeldIsEmpty,
    $sql_toevoeging = ""
) {
    global $pdo;

    if (!empty($tabel)) {
        $tableData = getTableData($tabel);
        if (empty($tableData)) {
            return;
        }
    }

    $whereString = '';
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $query) {
        $whereString .= $query[0];
    }

    $where = "WHERE TRUE " . $whereString . " " . $sql_toevoeging;
    $query_limit_rsdetail = "SELECT * FROM $tabel $where";

    //echo $query_limit_rsdetail;
    $teller = 0;

    $query = $pdo->prepare($query_limit_rsdetail);
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $queryPart) {
        $whereString .= $queryPart[0];
        if (isset($queryPart[1])) {
            foreach ($queryPart[1] as $key => $value) {
                $query->bindValue($key, $value);
            }
        }
    }

    $query->execute();

    foreach ($query->fetchAll() as $data) {
        if (isset($_REQUEST[$tabel . "_" . $checkVeldIsEmpty . "_" . ($data["ID"] * 1)]) && trim($_REQUEST[$tabel . "_" . $checkVeldIsEmpty . "_" . ($data["ID"] * 1)]) != '') {
            $sql_regel = "update `" . ps($tabel, "tabel") . "` set UPDATEDATUM = '" . date("Y-m-d") . "', ";
            $params = array();
            foreach ($arrayVelden as $Key => $Veld) {
                $naamVeld = $tabel . "_" . $Veld . "_" . ($data["ID"] * 1);
                $sql_regel .= '`' . $Veld . '` = :' . $naamVeld . ', ';
                $params[$naamVeld] = ps($_REQUEST[$naamVeld]);
            }
            $sql_regel = substr($sql_regel, 0, (strlen($sql_regel) - 2));
            $sql_regel .= " WHERE ID = '" . ps($data["ID"], "nr") . "';";
            $query = $pdo->prepare($sql_regel);
            foreach ($params as $paramKey => $paramValue) {
                $query->bindValue($paramKey, $paramValue);
            }
            $query->execute();

        }
    }

    //New fields
    for ($n = 0; $n <= 10; $n++) {
        if (isset($_REQUEST["new_" . $tabel . "_" . $checkVeldIsEmpty . "_" . ($data["ID"] * 1)]) && trim($_REQUEST["new_" . $tabel . "_" . $checkVeldIsEmpty . "_" . $n]) != '') {
            $sql_regel = "insert into $tabel set BEDRIJFSID = '" . ps($bedrijfsid) . "', UPDATEDATUM = '" . date("Y-m-d") . "', DELETED = 'Nee', ";

            $params = array();
            foreach ($arrayVelden as $Key => $Veld) {
                $naamVeld = "new_" . $tabel . "_" . $Veld . "_" . $n;
                $sql_regel .= '`' . $Veld . '` = :' . $naamVeld . ', ';
                $params[$naamVeld] = ps($_REQUEST[$naamVeld]);
            }

            $sql_regel = substr($sql_regel, 0, (strlen($sql_regel) - 2)) . ";";
            $query = $pdo->prepare($sql_regel);
            foreach ($params as $paramKey => $paramValue) {
                $query->bindValue($paramKey, $paramValue);
            }
            $query->execute();
        }
    }

    //New item
    if ($tabel == "oft_shareholders") {
        $pdo->query('update oft_shareholders set ADMINIDFROM = BEDRIJFSID WHERE ADMINIDFROM is null;');
    }
}

function oft_tabel_print(
    $userid,
    $bedrijfsid,
    $arrayVelden,
    $arrayVeldnamen,
    $arrayVeldType,
    $arrayZoekvelden,
    $tabel,
    $order,
    $toevoegen,
    $bewerken,
    $verwijderen,
    $sql_toevoeging = ""
) {
    global $pdo;

    if (!empty($tabel)) {
        $tableData = getTableData($tabel);
        if (empty($tableData)) {
            return;
        }
    }

    //Default vars
    $aantaldocs = 30;
    $pagina = 1;
    if (isset($_REQUEST["PAGNR"])) {
        $pagina = $_REQUEST["PAGNR"];
    }

    $url_querystring = oft_tabel_querystring();
    $MENUID = getMenuItem();
    $bewLinkToev = "&BID=" . $bedrijfsid;
    if ($MENUID != "") {
        $bewLinkToev .= "&MENUID=" . $MENUID;
    }
    $lSorteerop = $order;
    if (isset($_REQUEST["SORTEEROP"]) && $_REQUEST["SORTEEROP"] != "") {
        $lSorteerop = $_REQUEST["SORTEEROP"];
    }

    if (isset($_REQUEST["SORTORDER"])) {
        $lOrder = (($_REQUEST["SORTORDER"] == "ASC") ? "DESC" : "ASC");
    } else {
        if ($order == "FINANCIALYEAR") {
            $lOrder = "DESC";
        } else {
            $lOrder = "";//"ASC";
        }
    }

    //Opbouw query
    //TODO: aanpassen oftgetsearchSQL
    $whereString = '';
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $query) {
        $whereString .= $query[0];
    }

    $where = "WHERE (!DELETED AND DELETED = 'Nee') " . $whereString . " " . $sql_toevoeging;
    $orderby = " ORDER BY $lSorteerop $lOrder";

    if ($tabel == "oft_incident_register") {
        $orderby = " ORDER BY INCIDENTNR";
    }

    if ($tabel == "login") {
        $orderby .= " GROUP BY USERNAME";
    }

    $Sql = "SELECT * FROM $tabel $where $orderby";


    //Reken snel uit wat het aantal regel is.
    $queryAantalRegels = $pdo->prepare('SELECT SQL_CALC_FOUND_ROWS ' . $tabel . '.ID FROM ' . $tabel . ' ' . $where . ' LIMIT 1');
    $queryAantalRegels->execute();
    $totaalAantalRegels = $queryAantalRegels->rowCount();

    //Paginanummering
    $content = "<div id=\"contentTable_$tabel\">";

    if ($totaalAantalRegels > 0) {
        $content .= paginate($url_querystring, $totaalAantalRegels, $aantaldocs, $pagina,
            ['ajax' => true, 'table' => $tabel]);
    } else {
        $content .= "No results found";
    }


    $query_limit_rsdetail = $Sql;
    if (($pagina != -1) && ($aantaldocs * ($pagina - 1)) >= 0) {
        $query_limit_rsdetail = "$Sql limit " . ($aantaldocs * ($pagina - 1)) . ", $aantaldocs;";
    }

    //Toon header
    $content .= "<table name=\"exportexcel\" class=\"sortable\" width=\"100%\">";

    //Toon resultaten
    $teller = 0;
    $query = $pdo->prepare($query_limit_rsdetail);
    foreach (oft_getSearchSQL($userid, $bedrijfsid, $tabel) as $queryPart) {
        $whereString .= $queryPart[0];
        if (isset($queryPart[1])) {
            foreach ($queryPart[1] as $key => $value) {
                $query->bindValue($key, $value);
            }
        }
    }

    $query->execute();

    foreach ($query->fetchAll() as $data) {
        if (($teller % 10) == 0) {
            $content .= "</table><table name=\"exportexcel\" class=\"sortable\" width=\"100%\">";
        }

        foreach ($arrayVelden as $Key => $Veld) {
            $waarde = "";
            if (substr($arrayVelden[$Key], 0, 6) != "SKIP_X") {

                if ($arrayVelden[$Key] == "SKIP_INCIDENT_ACTIONS") {
                    $actionsTaken = "";

                    $sql_Actions_taken = "select * from oft_incident_register_task where (NOT DELETED = 'Ja' OR DELETED is null) AND INCIDENTID = :id order by ID;";
                    $query = $pdo->prepare($sql_Actions_taken);
                    $query->bindValue('id', $data["ID"]);
                    $query->execute();
                    $d_actions_taken = $query->fetch(PDO::FETCH_ASSOC);

                    if ($d_actions_taken !== false) {
                        if ($d_actions_taken["DATUM"] != '' && $d_actions_taken["DATUM"] != '0000-00-00') {
                            $actionsTaken .= "[" . $d_actions_taken["DATUM"] . " - " . $d_actions_taken["STATUS"] . "] " . $d_actions_taken["TASK"] . "<br/><br/>";
                        } else {
                            $actionsTaken .= "[" . $d_actions_taken["STATUS"] . "] " . $d_actions_taken["TASK"] . "<br/><br/>";
                        }
                    }

                    $waarde = lb($actionsTaken);
                } elseif (substr($arrayVelden[$Key], 0, 4) == "SKIP") {
                } elseif ($arrayVeldType[$Key] == "persentage") {
                    $waarde = lb($data[$Veld]) . " %";
                } elseif ($arrayVeldType[$Key] == "field") {
                    $waarde = lb($data[$Veld]);
                } elseif ($arrayVeldType[$Key] == "fieldLang") {
                    $waarde = lb($data[$Veld]);
                } elseif ($arrayVeldType[$Key] == "bestand") {
                    $waarde = "<a href=\"document.php?DID=" . ($data["ID"] * 1) . "\">" . lb($data[$Veld]) . "</a>";
                } elseif ($arrayVeldType[$Key] == "bestand_from_type") {
                    $bestand_from_type = "";
                    $DOCTYPE = "";
                    if ($tabel == "oft_shareholders") {
                        $DOCTYPE = "Deed of pledge";
                    }
                    $sql_bestand_from_type = "";
                    $query = $pdo->prepare('select ID, BESTANDSNAAM from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND DOCTYPE = :doctype order by ID limit 1;');
                    $query->bindValue('doctype', $DOCTYPE);
                    $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                    $query->execute();
                    $data = $query->fetch(PDO::FETCH_ASSOC);

                    $dbestand_from_type = $sql_bestand_from_type;
                    if ($dbestand_from_type !== false) {
                        $bestand_from_type = "<a href=\"document.php?DID=" . $dbestand_from_type["ID"] . "\">" . verkort($dbestand_from_type["BESTANDSNAAM"],
                                50) . "</a>";
                    }
                    $waarde = $bestand_from_type;
                } elseif ($arrayVeldType[$Key] == "nr_of_docs") {
                    $nr_of_docs = "";
                    $DOCTYPE = "";
                    if ($tabel == "oft_tax") {
                        $DOCTYPE = "Tax filing";
                    } elseif ($tabel == "oft_annual_accounts") {
                        $DOCTYPE = "Annual account";
                    }
                    $sql_bestand_from_type = "";
                    $query = $pdo->prepare('select COUNT(*) As Aantal from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND JAAR = :jaar AND DOCTYPE = :doctype GROUP BY BEDRIJFSID;');
                    $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                    $query->bindValue('jaar', $data["FINANCIALYEAR"]);
                    $query->bindValue('doctype', $DOCTYPE);
                    $query->execute();
                    $dbestand_from_type = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dbestand_from_type !== false) {
                        $nr_of_docs = $dbestand_from_type["Aantal"];
                    }
                    $waarde = $nr_of_docs;
                } elseif ($arrayVeldType[$Key] == "bestand_in_year_from_type") {
                    $bestand_from_type = "";
                    $DOCTYPE = "";
                    if ($tabel == "oft_tax") {
                        $DOCTYPE = "Tax filing";
                    } elseif ($tabel == "oft_annual_accounts") {
                        $DOCTYPE = "Annual account";
                    }
                    $sql_bestand_from_type = "";

                    $query = $pdo->prepare('select ID, BESTANDSNAAM, UPDATEDATUM from documentbeheer where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND JAAR = :jaar AND DOCTYPE = :doctype order by ID  DESC limit 1;');
                    $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                    $query->bindValue('jaar', $data["FINANCIALYEAR"]);
                    $query->bindValue('doctype', $DOCTYPE);
                    $query->execute();
                    $dbestand_from_type = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dbestand_from_type !== false) {
                        $bestand_from_type = "<a href=\"document.php?DID=" . $dbestand_from_type["ID"] . "\">" . verkort($dbestand_from_type["BESTANDSNAAM"],
                                50) . "</a>";
                    }
                    $waarde = $bestand_from_type;
                } elseif ($arrayVeldType[$Key] == "datum" || $arrayVeldType[$Key] == "datumleeg") {
                    $waarde = datumconversiePresentatieKort($data[$Veld]);
                } elseif ($arrayVeldType[$Key] == "year") {
                    $waarde = getJaar($data[$Veld]);
                } elseif ($arrayVeldType[$Key] == "status") {
                    if ($tabel == "notificaties") {
                        $status_img = getNotificatieStatus(array(
                            'status' => $data[$Veld],
                            'due_date' => $data["VERVALDATUM"],
                            'table' => $data["TABEL"],
                            'table_id' => $data["TABELID"]
                        ));
                    } else {
                        if (isset($data["EXTENTION"]) && $data["EXTENTION"] != "" && $data["EXTENTION"] != "0000-00-00") {
                            $vervaldatum = $data["EXTENTION"];
                        } else {
                            $vervaldatum = $data["STANDARDDUEDATE"];
                        }
                        $status_img = getNotificatieStatus(array(
                            'status' => $data[$Veld],
                            'due_date' => $vervaldatum,
                            'table' => $tabel,
                            'table_id' => $data["ID"]
                        ));
                    }
                    $waarde = $status_img;
                } elseif ($arrayVeldType[$Key] == "medewerker") {
                    $waarde = getPersoneelslidNaam($data[$Veld]);
                } elseif ($arrayVeldType[$Key] == "bedrijfsnaam") {
                    $waarde = getNaam("bedrijf", $data[$Veld], "BEDRIJFSNAAM", 30, "");
                } elseif ($arrayVeldType[$Key] == "relatie") {
                    $waarde = getNaamRelatie($data[$Veld]);
                } elseif (substr($arrayVeldType[$Key], 0, 5) == "stam_") {
                    $waarde = getNaam($arrayVeldType[$Key], $data[$Veld], "NAAM", 30, "");
                } elseif ($arrayVeldType[$Key] == "person_name") {
                    $nNaam = "";
                    $query = $pdo->prepare('SELECT INITIALEN, TUSSENVOEGSEL, ACHTERNAAM FROM personeel WHERE ID = :id limit 1;');
                    $query->bindValue('id', $data[$Veld]);
                    $query->execute();
                    $dPers = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dPers !== false) {
                        $nNaam = $dPers["INITIALEN"] . " " . $dPers["TUSSENVOEGSEL"] . " " . $dPers["ACHTERNAAM"];
                    }

                    $waarde = lb($nNaam);
                } elseif ($arrayVeldType[$Key] == "get_personeel_email") {
                    $nEmail = "";
                    $query = $pdo->prepare('SELECT EMAIL FROM personeel WHERE ID = :id limit 1;');
                    $query->bindValue('id', $data[$Veld]);
                    $query->execute();
                    $dPers = $query->fetch(PDO::FETCH_ASSOC);

                    if ($dPers !== false) {
                        $nEmail = $dPers["EMAIL"];
                    }

                    $waarde = lb($nEmail);
                } elseif ($arrayVeldType[$Key] == "bedrag") {
                    $waarde = "&#128; " . prijsconversiePuntenKommas($data[$Veld]);
                } elseif ($arrayVeldType[$Key] == "country") {
                    $waarde = getNaam("bedrijf", $data["BEDRIJFSID"], "LAND", 30, "");
                } elseif ($arrayVeldType[$Key] == "translate") {
                    $waarde = tl(lb($data[$Veld]));
                } elseif (isset($data[$Veld])) {
                    if ($data[$Veld] == "Ja") {
                        $waarde = "Yes";
                    } elseif ($data[$Veld] == "Ne") {
                        $waarde = "No";
                    } else {
                        $waarde = lb($data[$Veld]);
                    }
                }

                if ((($tabel == "contracten"
                            || $tabel == "documentbeheer"
                            || $tabel == 'notificaties'
                            || $tabel == "oft_action_tracking_audit"
                            || $tabel == "oft_action_tracking"
                            || $tabel == "oft_incident_register"
                            || $tabel == "oft_complaint"
                        )
                        && isRechtenOFT($data["BEDRIJFSID"], "CRM"))
                    ||
                    ($tabel != "documentbeheer"
                        && $tabel != "contracten"
                        && $tabel != 'notificaties'
                        && $tabel != "oft_action_tracking_audit"
                        && $tabel != "oft_action_tracking"
                        && $tabel != "oft_incident_register"
                        && $tabel != "oft_complaint"
                    )) {

                    if ($waarde != '') {
                        $content .= "<tr><td valign=\"top\" align=\"left\" class=\"oft_label\">" . $arrayVeldnamen[$Key] . "</td>";
                        $content .= "<td align=\"left\" valign=\"top\">" . $waarde . "</td>";
                        $content .= "</tr>";
                    }
                }
            }

            $teller++;
        }

        if ((($tabel == "contracten"
                    || $tabel == "documentbeheer"
                    || $tabel == 'notificaties'
                    || $tabel == "oft_action_tracking_audit"
                    || $tabel == "oft_action_tracking"
                    || $tabel == "oft_incident_register"
                    || $tabel == "oft_complaint"
                )
                && isRechtenOFT($data["BEDRIJFSID"], "CRM"))
            ||
            ($tabel != "documentbeheer"
                && $tabel != "contracten"
                && $tabel != 'notificaties'
                && $tabel != "oft_action_tracking_audit"
                && $tabel != "oft_action_tracking"
                && $tabel != "oft_incident_register"
                && $tabel != "oft_complaint"
            )) {
            if (isset($_REQUEST["EXPORTEXCEL"])) {
                $content .= "<tr><td>_________________________________________________</td><td>__________________________________________________________________________________________________</td></tr>";
            } else {
                $content .= "<tr><td colspan=\"100\"><hr size=\"1\" /></td></tr>";
            }
        }
    }

    if ($teller == 0) {
        $content .= "<tr><td colspan=\"" . count($arrayVelden) . "\"><em>No results</em></td><tr>";
    }

    $content .= "</tbody></table></div>\n";

    return $content;
}

function searchFieldsConverter($searchFields)
{
    $return = "";
    if (!is_array($searchFields) || !count($searchFields)) {
        return $return;
    }

    foreach ($searchFields as $value) {
        if ($value == "entity_buttons") {
            continue;
        }
        switch ($value) {
            case "ENTITY":
            case "ENTITY_TABLE":
                $showField = "SRCH_BEDRIJFSNAAM";
                $searchVeld = "SRCH_BEDRIJFSID";
                break;
            case "UPDATEDBY":
                $showField = "SRCH_ACHTERNAAM";
                $searchVeld = "SRCH_PERSONEELSLID";
                break;
            case "COUNTRY":
            case "COUNTRY_TABLE":
                $showField = "SRCH_LAND";
                $searchVeld = "SRCH_LAND";
                break;
            case "YEAR":
                $showField = "SRCH_JAAR";
                $searchVeld = "SRCH_YEAR";
                break;
            default:
                $showField = "SRCH_" . $value;
                $searchVeld = "SRCH_" . $value;
                break;
        }
        $return .= "&$searchVeld=x'+encodeURIComponent(document.getElementById('$showField').value)+'";
    }

    return $return;
}