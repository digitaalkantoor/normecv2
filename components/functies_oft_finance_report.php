<?php
sercurityCheck();

function oft_finance_report($userid, $bedrijfsid, $typeBalans = "Balans") {
  global $db;

  if(isset($_REQUEST["ID"])) {
    $bedrijfsid = $_REQUEST["ID"];
  }

  $titel           = oft_titel_entities($bedrijfsid);
  $submenuitems    = "";
  $back_button     = oft_back_button_entity();
  $contentFrame    = balans($userid, $bedrijfsid, date("Y"), $typeBalans);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function balans($userid, $bedrijfsid, $jaar, $typeBalans)
{
  global $db;

  $maxKolBalansen = 12;

  if (isset($_REQUEST["TOONWAT"])) {
    $typeBalans = $_REQUEST["TOONWAT"];
  } else {
    if (isset($_REQUST["SITE"]) && $_REQUST["SITE"] == "resultaatrekening") {
      $typeBalans = "Kosten of opbrengst";
    }
  }
  
  if(isset($_REQUEST["BEDRIJFSID"])) {
    $bedrijfsid = $_REQUEST["BEDRIJFSID"];
  }
  
  $_REQUEST["BEDRIJFSID"] = $bedrijfsid;

  if ($typeBalans == "Kostenofopbrengst") {
    $typeBalans = "Kosten of opbrengst";
  }

  $_REQUEST["TOONWAT"] = $typeBalans;

  if ($typeBalans == "Kosten of opbrengst") {
    $Cumulative = "Nee";
  } else {
    $Cumulative = "Ja";
  }

  if (isset($_REQUEST["Cumulative"])) {
    $Cumulative = "Ja";
  }

  //Choose periode
  $datumVan = date("Y") . "-01-01";
  $datumTot = date("Y") . "-12-31";
  $tijdspan = "Jaar";
  if (isset($_REQUEST["DATUMVAN"])) {
    $datumVan = datumconversienaarVS($_REQUEST["DATUMVAN"]);
  }
  if (isset($_REQUEST["DATUMTOT"])) {
    $datumTot = datumconversienaarVS($_REQUEST["DATUMTOT"]);
  }
  if (isset($_REQUEST["TERMIJNEN"])) {
    $tijdspan = $_REQUEST["TERMIJNEN"];
    $tijdspan = str_replace("Halfjaar", "Half jaar", $tijdspan);
  }

  $toonNiveau = "Passiva/Activa";
  if (isset($_REQUEST["TOONNIVEAU"])) {
    $toonNiveau = $_REQUEST["TOONNIVEAU"];
  }
  
  $kostenplaats = "";
  if(isset($_REQUEST["KOSTENPLAATS"])) {
    $kostenplaats = $_REQUEST["KOSTENPLAATS"];
  }

  $functionsRow1 = array(
    0 => array("TYPE" => "datumvan", "LABEL" => "from", "VELDNAAM" => "DATUMVAN", "TABEL" => "uren"),
    1 => array("TYPE" => "balans_termijnen", "LABEL" => "per", "VELDNAAM" => "TERMIJNEN", "TABEL" => "filter"),
    //2 => array("TYPE" => "balans_toon_niveau", "LABEL" => "Niveau", "VELDNAAM" => "TOONNIVEAU", "TABEL" => "filter"),
  );
  $functionsRow2 = array(
    0 => array("TYPE" => "datumtot", "LABEL" => "till", "VELDNAAM" => "DATUMTOT", "TABEL" => "uren"),
    1 => array("TYPE" => "balans_toon_wat", "LABEL" => "what", "VELDNAAM" => "TOONWAT", "TABEL" => "filter"),
  );

  //TOONNIVEAU
  //Rubriek en hoofdrubriek

  //https://secure.ezora.com/visservisser/reports
  $hoofdrubrieken = balans_cijfers($userid, $bedrijfsid, $datumVan, $datumTot, $tijdspan, $typeBalans, $Cumulative, $toonNiveau, $kostenplaats);

  $stappen = getStappenDatumVanTot($datumVan, $datumTot, $tijdspan);

  $maanden = maandenvanhetjaar("3");

  $conditie = "";
  
  $output = "";

  //Fout in balans?
  if ($typeBalans == "Balans") {
    if(isset($hoofdrubrieken["Result"]["Result"]['-1']['gbnummer']["foutInResultaat"]) && prijsconversie(($hoofdrubrieken["Result"]["Result"]['-1']['gbnummer']["foutInResultaat"]*1)) != '0.00') {
      $meldingen[] = "The total debet and credit amount are not equal. The difference is ".prijsconversie($hoofdrubrieken["Result"]["Result"]['-1']['gbnummer']["foutInResultaat"]).".";
    }
  }

  $output .= "<br/>".periodeSelectie_custom(
    $userid,
    $bedrijfsid,
    "content.php?SITE=oft_finance_report&MENUID=2&BID=$bedrijfsid",
    substr($datumTot, 0, 4),
    $functionsRow1,
    $functionsRow2
  );

  $output .= "<br/><br/>";

  $output .= "<div class=\"dashboardVakje\">";
  $output .= "<table name=\"exportexcel\" class=\"rtable\" border=\"0\" width=\"100%\">";
  $output .= "<tr><th></th>";
  for ($p = 0; $p < $stappen && $p < $maxKolBalansen; $p++) {
    $periodeDatumTot = getDatumVanTotBalans($datumVan, $datumTot, $tijdspan, $p, $Cumulative);
    $datumVanTot = explode(";", $periodeDatumTot);
    $periodeDatumTot = $datumVanTot[1];

    if ($tijdspan == "Jaar") {
      $periode = substr($periodeDatumTot, 0, 4);
    } elseif ($tijdspan == "Dag") {
      $periode = (substr($periodeDatumTot, 8, 2) * 1);
    } elseif ($tijdspan == "Maand") {
      $periode = $maanden[(substr($periodeDatumTot, 5, 2) * 1)];
    } elseif ($tijdspan == "Kwartaal") {
      $periode = substr($periodeDatumTot, 5, 2) * 1;
      if ($periode == "3") {
        $periode = "K1";
      } else {
        if ($periode == "6") {
          $periode = "K2";
        } else {
          if ($periode == "9") {
            $periode = "K3";
          } else {
            if ($periode == "12") {
              $periode = "K4";
            } else {
              $periode = "K";
            }
          }
        }
      }
    } elseif ($tijdspan == "Half jaar") {
      $periode = substr($periodeDatumTot, 5, 2) * 1;
      if ($periode == "6") {
        $periode = "H1";
      } else {
        if ($periode == "12") {
          $periode = "H2";
        } else {
          $periode = "H";
        }
      }
    } else {
      $periode = substr($periodeDatumTot, 0, 4);
    }

    $output .= "<td colspan=\"2\" style=\"text-align: center; font-size:16px;font-weight:bold;\">" . lb($periode) . "</th>";
  }
  $output .= "</tr>";

  //titelbar
  $output .= "<tr>";
  $output .= "<td style=\"font-size: 13px;font-weight: normal; border-bottom: 1px solid #000;\"></td>";
  for ($p = 0; $p < $stappen && $p < $maxKolBalansen; $p++) {
    $output .= "<td style=\"text-align:right;font-size: 13px;font-weight: normal; border-bottom: 1px solid #000;\">" . tl("Debet") . "</td>";
    $output .= "<td style=\"text-align:right;font-size: 13px;font-weight: normal; border-bottom: 1px solid #000;\">" . tl("Credit") . "</td>";
  }
  $output .= "</tr>";

  $totaal = array();
  for ($p = 0; $p < $stappen && $p < $maxKolBalansen; $p++) {
    $totaal[$p]['debet'] = 0;
    $totaal[$p]['credit'] = 0;
  }

  foreach($hoofdrubrieken As $hoofdrubriek => $subrubrieken) {
    if(is_array($subrubrieken)) {
      if($hoofdrubriek != "Result") {
        $hoofdrubriekenResult = print_balans_regel($hoofdrubrieken, $hoofdrubriek,$datumVan,$datumTot,$tijdspan,$p,$stappen,$typeBalans,$Cumulative,$maxKolBalansen,$toonNiveau, 14, "");
      }
      if(isset($hoofdrubriekenResult["xml"]) && $hoofdrubriek != "Result") {
        $output .= "<tr><td style=\"font-size: 16px; font-weight: bold;\">$hoofdrubriek</td></tr>";;
      }

      foreach($subrubrieken As $subrubriek => $cijfers) {
        if(is_array($cijfers)) {

          $subrubriekResult = print_balans_regel($subrubrieken, $subrubriek,$datumVan,$datumTot,$tijdspan,$p,$stappen,$typeBalans,$Cumulative,$maxKolBalansen,$toonNiveau, 12, "");
          if(isset($subrubriekResult["xml"]) && $subrubriekResult["xml"] != '' && $subrubriek != "Result") {
            $output .= "<tr><td style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;$subrubriek</td></tr>";
          }

          foreach($cijfers As $nr => $cijfer) {
            if($nr > 0) {
              $result = print_balans_regel($cijfers,$nr,$datumVan,$datumTot,$tijdspan,$p,$stappen,$typeBalans,$Cumulative,$maxKolBalansen,$toonNiveau, 10, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

              if(isset($result["xml"]) && $result["xml"] != '') {
                $output .= $result["xml"];
              }
            }
          }

          if(isset($subrubriekResult["xml"]) && $subrubriekResult["xml"] != '' && $subrubriek != "Result") {
            $output .= $subrubriekResult["xml"];
          }
        }
      }
    }
    if($hoofdrubriek != "Result" && $hoofdrubriek != 'FEE INCOME') {
      if(isset($hoofdrubriekenResult["xml"]) && $hoofdrubriekenResult["xml"] != '') {
        $output .= $hoofdrubriekenResult["xml"];
      }
    }
  }

  $output .= "</table></div>";

  return $output;
}

function print_balans_regel(
  $cijfers,
  $grootboekId,
  $datumVan,
  $datumTot,
  $tijdspan,
  $p,
  $stappen,
  $typeBalans,
  $Cumulative,
  $maxKolBalansen,
  $toonNiveau,
  $fontsize,
  $prefix
)
{
  $output = "";
  $regelLeeg = false;
  //$grootboekId *= 1;

  //echo "<br/>$grootboekId - ".$cijfers[$grootboekId];

  if (isset($cijfers[$grootboekId])) {
    $row_printed = false;
    $totaal = array();
    for ($p = 0; $p < $stappen && $p < $maxKolBalansen; $p++) {
      $datumVanTot = explode(";", getDatumVanTotBalans($datumVan, $datumTot, $tijdspan, $p, $Cumulative));
      $periodeDatumVan = $datumVanTot[0];
      $periodeDatumTot = $datumVanTot[1];

      $debet = $cijfers[$grootboekId][$periodeDatumTot]['debet'];
      $credit = $cijfers[$grootboekId][$periodeDatumTot]['credit'];
      
      if($debet != 0 && $credit != 0) {
        //Als grootste bedrag is debet
        if($debet > $credit) {
          //Ga uit van debet
          $debet = $debet - $credit;
          $credit = 0;
        } else {
          //Ga uit van credit
          $credit = $credit - $debet;
          $debet = 0;
        }
      }

      if (isset($totaal[$p]['debet'])) {
        $totaal[$p]['debet'] += $debet;
      } else {
        $totaal[$p]['debet'] = $debet;
      }

      if (isset($totaal[$p]['credit'])) {
        $totaal[$p]['credit'] += $credit;
      } else {
        $totaal[$p]['credit'] = $credit;
      }

      if ($debet == 0) {
        $debet = '';
      } else {
        $debet = prijsconversiePuntenKommas($debet);
      }

      if ($credit == 0) {
        $credit = '';
      } else {
        $credit = prijsconversiePuntenKommas($credit);
      }

      $extra_style = "";
      if ($grootboekId == 0) {
        $extra_style = "font-size: ".$fontsize."px; font-weight: bold;";
      }

      $extra_style_td = "";
      if($fontsize == 12) {
        $extra_style_td = "border-top: 1px solid #000;padding-bottom: 20px;";
      } elseif($fontsize == 14) {
        $extra_style_td = "border-top: 3px solid #000;padding-bottom: 20px;";
      } elseif($cijfers[$grootboekId]["gbnaam"] == "Result") {
        $prefix = "&nbsp;&nbsp;";
        $extra_style_td = "border-top: 3px solid #000;padding-bottom: 20px;";
        $extra_style = "font-size: 13px; font-weight: bold;";
      }

      if ($row_printed == false) {
        $output .= "<tr style=\"$extra_style\">";
        if($fontsize == 12 || $fontsize == 14) {
          $output .= "<td></td>";
        } else {
          $output .= "<td valign=\"top\">$prefix" . lb($cijfers[$grootboekId]["gbnummer"]) . " ". lb($cijfers[$grootboekId]["gbnaam"]) . "</td>";
        }
        $row_printed = true;
      }

      $td_functies = "";
      if (!isset($_REQUEST["EXPORTEXCEL"])) {
        $td_functies = "align=\"right\""; 
      }

      if($debet != '' || $credit != '') {
        $regelLeeg = true;
      }

      if($debet == '') {
        $output .= "<td></td>";
      } else {
        $output .= "<td $td_functies style=\"$extra_style_td\">" . $debet . "</td>";
      }
      if($credit == '') {
        $output .= "<td></td>";
      } else {
        $output .= "<td $td_functies style=\"$extra_style_td\">" . $credit . "</td>";
      }


      if ($row_printed) {
        $output .= "</tr>";
      }
    }
  }

  if($regelLeeg) {
    return array("xml" => $output, "totaal" => $totaal);
  } else {
    return array("xml" => '', "totaal" => 0);
  }
}

function getDatumVanTotBalans($datumVan, $datumTot, $tijdspan, $stap, $Cumulative)
{
  //return getDatumVanTot($datumVan, $datumTot, $tijdspan, $stap, '', '', $Cumulative);

  $returnDatumVan = "";
  $returnDatumTot = "";

  if ($tijdspan == "Jaar") {
    $returnDatumVan = date("Y-m-d", strtotime("+" . $stap . " year", strtotime($datumVan)));
    $returnDatumTot = date("Y-m-d", strtotime("+" . ($stap+1) . " year -1 day", strtotime($datumVan)));
  } elseif ($tijdspan == "Half jaar") {
    $returnDatumVan = date("Y-m-d", strtotime("+" . ($stap * 6) . " month", strtotime($datumVan)));
    $returnDatumTot = date("Y-m-d", strtotime("+" . (($stap + 1) * 6) . " month -1 day", strtotime($datumVan)));
  } elseif ($tijdspan == "Kwartaal") {
    $returnDatumVan = date("Y-m-d", strtotime("+" . ($stap * 3) . " month", strtotime($datumVan)));
    $returnDatumTot = date("Y-m-d", strtotime("+" . (($stap + 1) * 3) . " month -1 day", strtotime($datumVan)));
  } elseif ($tijdspan == "Maand") {
    $returnDatumVan = date("Y-m-d", strtotime("+$stap month", strtotime($datumVan)));
    $returnDatumTot = date("Y-m-d", strtotime("+" . ($stap + 1) . " month -1 day", strtotime($datumVan)));
  } elseif ($tijdspan == "Dag") {
    $returnDatumVan = date("Y-m-d", strtotime("+$stap day -2 day", strtotime($datumVan)));
    $returnDatumTot = date("Y-m-d", strtotime("+" . ($stap + 1) . " day -1 day", strtotime($datumVan)));
  }

  $returnDatumVan = date("Y-m-d", max(strtotime($returnDatumVan), strtotime($datumVan)));
  $returnDatumTot = date("Y-m-d", min(strtotime($returnDatumTot), strtotime($datumTot)));

  //echo "<br/>$returnDatumVan . ; . $returnDatumTot";

  return $returnDatumVan . ";" . $returnDatumTot;

}

//Genereer balanscijfers
function balans_cijfers($userid, $bedrijfsid, $datumVan, $datumTot, $tijdspan, $typeBalans, $Cumulative, $toonNiveau, $kostenplaats) {
  global $pdo;

  $array = array();

  $stappen = getStappenDatumVanTot($datumVan, $datumTot, $tijdspan);
  $maxKolBalansen = 12;

  //Bereken balans waardes
  $query = $pdo->prepare('SELECT grootboekitems.ID, CREDITDEBET
                        FROM grootboekitems, balansrubriek
                       WHERE grootboekitems.BALANSKOSTEN = :typebalans
                         AND grootboekitems.BEDRIJFSID = :bedrijfsid
                         AND ((balansrubriek.ID*1) = (grootboekitems.RUBRIEK*1))
                       ORDER BY grootboekitems.GROOTBOEKNR;');
  $query->bindValue('typebalans', $typeBalans);
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->execute();

  foreach ($query->fetchAll() as $data) {
     //Check alle velden
     $totaalRegel = 0;
     for($p = 0; $p < $stappen && $p < $maxKolBalansen; $p++)
     {
         $tmpDatumVanTot = explode(";", getDatumVanTotBalans($datumVan, $datumTot, $tijdspan, $p, $Cumulative));
         $tmpDatumVan    = $tmpDatumVanTot[0];
         $tmpDatumTot    = $tmpDatumVanTot[1];
         $periodeBedrag  = telPostPerPeriodeOp($bedrijfsid, $tmpDatumVan, $tmpDatumTot, ($data["ID"]*1), "Ja", $data["CREDITDEBET"], '', 0, $kostenplaats);

         $array          = add_balans_cijfers_array($bedrijfsid, $array, $tmpDatumVan, $tmpDatumTot, $periodeBedrag[0], $periodeBedrag[1], $data["ID"], $toonNiveau);
     }
  }
  
  //Winstberekening Eind
  for($p = 0; $p < $stappen && $p < $maxKolBalansen; $p++) {
   $tmpDatumVanTot        = explode(";", getDatumVanTotBalans($datumVan, $datumTot, $tijdspan, $p, $Cumulative));
   $tmpDatumVan           = $tmpDatumVanTot[0];
   $tmpDatumTot           = $tmpDatumVanTot[1];

   $winst                 = winst($bedrijfsid, $tmpDatumVan, $tmpDatumTot, "");

   //echo "$bedrijfsid, $tmpDatumVan, $tmpDatumTot, $winst";
   if ($typeBalans == "Kosten of opbrengst") {
     if ($winst < 0 ) {
       $debet = 0;
       $credit = $winst*-1;
     } else {
       $debet = $winst;
       $credit = 0;
     }
   } else {
     if ($winst < 0 ) {
       $credit = 0;
       $debet = $winst*-1;
     } else {
       $debet = 0;
       $credit = $winst;
     }
   }

   $array = add_balans_cijfers_array($bedrijfsid, $array, $tmpDatumVan, $tmpDatumTot, $debet, $credit, "0", $toonNiveau);
  }

  //Verwijder lege regels
  $totaalDebetPosten = 0;
  $totaalCreditPosten = 0;
  $returnArray = array();
  foreach($array As $hoofdrubriek => $subrubrieken) {
    foreach($subrubrieken As $subrubriek => $array) {
      if(is_array($array)) {
        foreach($array As $gbid => $gbcijfers) {
          $isLeeg = true;
          if(is_array($gbcijfers)) {
            foreach($gbcijfers As $periode => $cijfers) {
              if(isset($cijfers['debet']) && (($cijfers['debet']*1) != '0' || ($cijfers['credit']*1) != '0')) {
                $isLeeg = false;
                //echo "<br>$periode - $gbid: ".$cijfers['debet'] . " - " . $cijfers['credit'];
              }
              if($gbid != "0") {
                if(isset($cijfers['debet'])) {
                  $totaalDebetPosten += $cijfers['debet'];
                }
                if(isset($cijfers['credit'])) {
                  $totaalCreditPosten += $cijfers['credit'];
                }
              }
            }
          }
          if(!$isLeeg) {
            $returnArray[$hoofdrubriek][$subrubriek][$gbid] = $gbcijfers;
            $returnArray[$hoofdrubriek][$subrubriek] = $array;
            $returnArray[$hoofdrubriek] = $subrubrieken;
          }
        }
      }
    }
  }

  return $returnArray;
}

function add_balans_cijfers_array($bedrijfsid, $array, $datumVan, $datumTot, $debet, $credit, $grootboekid, $toonNiveau) {
  global $pdo;

  $grootboekid *= 1;

  $debet = prijsconversie($debet);
  $credit = prijsconversie($credit);

  if($grootboekid == 0) {
    //Result
    if((isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "oft_finance_report_penl") || (isset($_REQUEST["TOONWAT"]) && $_REQUEST["TOONWAT"] == "Kosten of opbrengst")) {
      $mapping = mapping(11000000);
    } else {
      $mapping = mapping(10520001);
    }
    $grootboekid = $mapping["NR"];
    $toprubriek = $mapping["TOPRUBRIEK"];
    $subrubriek = $mapping["SUBRUBRIEK"];
    $array[$toprubriek][$subrubriek][$grootboekid]['gbnaam'] = $mapping["NAAM"];
    $array[$toprubriek][$subrubriek][$grootboekid]['gbnummer'] = $mapping["NR"];

    if($debet < 0) {
      $credit = $debet * - 1;
      $debet = 0;
    }

    $array[$toprubriek][$subrubriek][$grootboekid]['gbnaam'] = $mapping["NAAM"];
    $array[$toprubriek][$subrubriek]['gbnaam'] = lb($subrubriek);
    $array[$toprubriek]['gbnaam'] = lb($toprubriek);

    $array[$toprubriek][$subrubriek][$grootboekid]['gbnummer'] = '';
    $array[$toprubriek][$subrubriek]['gbnummer'] = '';
    $array[$toprubriek]['gbnummer'] = '';

    $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['balanspositie'] = "Debet";
    $array[$toprubriek][$subrubriek][$datumTot]['balanspositie'] = "Debet";
    $array[$toprubriek][$datumTot]['balanspositie'] = "Debet";

    $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['balansresultaat'] = "Balans";
    $array[$toprubriek][$subrubriek][$datumTot]['balansresultaat'] = "Balans";
    $array[$toprubriek][$datumTot]['balansresultaat'] = "Balans";

    
    if(!isset($array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['credit'])) {
      $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['credit'] = 0;
      $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['debet'] = 0;
    }
    if(!isset($array[$toprubriek][$subrubriek][$datumTot]['credit'])) {
      $array[$toprubriek][$subrubriek][$datumTot]['credit'] = 0;
      $array[$toprubriek][$subrubriek][$datumTot]['debet'] = 0;
    }
    if(!isset($array[$toprubriek][$datumTot]['credit'])) {
      $array[$toprubriek][$datumTot]['credit'] = 0;
      $array[$toprubriek][$datumTot]['debet'] = 0;
    }

    $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['debet'] += $debet;
    $array[$toprubriek][$subrubriek][$datumTot]['debet'] += $debet;
    $array[$toprubriek][$datumTot]['debet'] += $debet;

    $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['credit'] += $credit;
    $array[$toprubriek][$subrubriek][$datumTot]['credit'] += $credit;
    $array[$toprubriek][$datumTot]['credit'] += $credit;
  } else {
    $queryGrootboekitems = $pdo->prepare('select NAAM, GROOTBOEKNR, CREDITDEBET, BALANSKOSTEN from grootboekitems where ID = :grootboekid limit 1;');
    $queryGrootboekitems->bindValue('grootboekid', $grootboekid);
    $queryGrootboekitems->execute();
    $dGrootboekitems = $queryGrootboekitems->fetch(PDO::FETCH_ASSOC);

    if($dGrootboekitems !== false)
    {
      //grootboekid
      $mapping = mapping($dGrootboekitems["GROOTBOEKNR"]);
      
      if(isset($mapping["NR"]) && $mapping["NR"] != '') {
        $toprubriek = $mapping["TOPRUBRIEK"];
        $subrubriek = $mapping["SUBRUBRIEK"];

        $array[$toprubriek][$subrubriek][$mapping["NR"]]['detail'] .= "($grootboekid - ".$dGrootboekitems["CREDITDEBET"].") ";

        $grootboekid = $mapping["NR"];
        
        $dGrootboekitems["NAAM"] = $mapping["NAAM"];
        $dGrootboekitems["GROOTBOEKNR"] = $mapping["NR"];

        $array[$toprubriek][$subrubriek][$grootboekid]['gbnaam'] = lb($dGrootboekitems["NAAM"]);
        $array[$toprubriek][$subrubriek]['gbnaam'] = lb($subrubriek);
        $array[$toprubriek]['gbnaam'] = lb($toprubriek);

        $array[$toprubriek][$subrubriek][$grootboekid]['gbnummer'] = '';
        $array[$toprubriek][$subrubriek]['gbnummer'] = '';
        $array[$toprubriek]['gbnummer'] = '';

        $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['balanspositie'] = $dGrootboekitems["CREDITDEBET"];
        $array[$toprubriek][$subrubriek][$datumTot]['balanspositie'] = $dGrootboekitems["CREDITDEBET"];
        $array[$toprubriek][$datumTot]['balanspositie'] = $dGrootboekitems["CREDITDEBET"];

        $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['balansresultaat'] = $dGrootboekitems["BALANSKOSTEN"];
        $array[$toprubriek][$subrubriek][$datumTot]['balansresultaat'] = $dGrootboekitems["BALANSKOSTEN"];
        $array[$toprubriek][$datumTot]['balansresultaat'] = $dGrootboekitems["BALANSKOSTEN"];

        if(!isset($array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['credit'])) {
          $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['credit'] = 0;
          $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['debet'] = 0;
        }
        if(!isset($array[$toprubriek][$subrubriek][$datumTot]['credit'])) {
          $array[$toprubriek][$subrubriek][$datumTot]['credit'] = 0;
          $array[$toprubriek][$subrubriek][$datumTot]['debet'] = 0;
        }
        if(!isset($array[$toprubriek][$datumTot]['credit'])) {
          $array[$toprubriek][$datumTot]['credit'] = 0;
          $array[$toprubriek][$datumTot]['debet'] = 0;
        }
    
        $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['debet'] += $debet;
        $array[$toprubriek][$subrubriek][$datumTot]['debet'] += $debet;
        $array[$toprubriek][$datumTot]['debet'] += $debet;
    
        $array[$toprubriek][$subrubriek][$grootboekid][$datumTot]['credit'] += $credit;
        $array[$toprubriek][$subrubriek][$datumTot]['credit'] += $credit;
        $array[$toprubriek][$datumTot]['credit'] += $credit;
      } else {
        //echo "</br>Mapping not found! (".$dGrootboekitems["GROOTBOEKNR"].")";
      }
    } else {
      echo "</br>General Ledger not found! ($grootboekid)";
    }
  }

  return $array;
}

function telPostPerPeriodeOp(
  $bedrijfsid,
  $datumVan,
  $datumTot,
  $GROOTBOEKID,
  $gescheiden,
  $kolom,
  $bankid = "",
  $beginsaldo = 0,
  $kostenplaats = ''
)
{
  global $pdo;

  //$GROOTBOEKID = 31 = debiteuren
  //               10 =  crediteuren

  $credit = 0;
  $debet = 0;
  $creditSchrijf = 0;
  $debetSchrijf = 0;
  $GROOTBOEKID *= 1;
  $toongrootboekidSQL = "x";

  $queryqwe = $pdo->prepare('SELECT ID FROM grootboekitems WHERE ID = :id LIMIT 1;');
  $queryqwe->bindValue('id', $GROOTBOEKID);
  $queryqwe->execute();
  $resultqwe = $queryqwe->fetch(PDO::FETCH_ASSOC);

  if($resultqwe !== false)
  {  
    
    $cacheDebet = 0;
    $cacheCredit = 0;
    $queryBalans = $pdo->prepare('SELECT BEDRAG
                     FROM balans
                    WHERE (DATUM >= :datumvan)
                      AND (DATUM <= :datumtot)
                      AND ((GROOTBOEKNR*1) = :grootboekid)
                      AND BEDRIJFSID = :bedrijfsid;');
    $queryBalans->bindValue('datumvan', $datumVan);
    $queryBalans->bindValue('datumtot', $datumTot);
    $queryBalans->bindValue('grootboekid', $GROOTBOEKID);
    $queryBalans->bindValue('bedrijfsid', $bedrijfsid);
    $queryBalans->execute();

    foreach ($queryBalans->fetchAll() as $data_balans) {
      $bedrag = prijsconversie($data_balans["BEDRAG"]);
      if ($data_balans["BEDRAG"] < 0) {
        $credit += ($bedrag * -1);
        $cacheCredit += ($bedrag * -1);
      } else {
        $debet += $bedrag;
        $cacheDebet += $bedrag;
      }
      if ($GROOTBOEKID == $toongrootboekidSQL) {
        echo "<br/>Memoriaal 1: D: $debet, C: $credit";
      }
    }

    $querybalans = $pdo->prepare('SELECT BEDRAG
                     FROM balans
                    WHERE (DATUM >= :datumvan)
                      AND (DATUM <= :datumtot)
                      AND (TEGENREKENING*1) = :tegenrekening
                      AND BEDRIJFSID = :bedrijfsid;');
    $querybalans->bindValue('datumvan', $datumVan);
    $querybalans->bindValue('datumtot', $datumTot);
    $querybalans->bindValue('tegenrekening', $GROOTBOEKID);
    $querybalans->bindValue('bedrijfsid', $bedrijfsid);
    $querybalans->execute();

    foreach ($querybalans->fetchAll() as $data_balans) {
      $bedrag = prijsconversie($data_balans["BEDRAG"]);
      if ($data_balans["BEDRAG"] < 0) {
        $debet += ($bedrag * -1);
        $cacheDebet += ($bedrag * -1);
      } else {
        $credit += $bedrag;
        $cacheCredit += $bedrag;
      }
      if ($GROOTBOEKID == $toongrootboekidSQL) {
        echo "<br/>Memoriaal 2: D: $debet, C: $credit";
      }
    }

    //} //einde cache
  }
  
  $debet = prijsconversie($debet);
  $credit = prijsconversie($credit);

  if ($kolom == "Debet") {
    $bedrag = $debet - $credit;

    if ($bedrag < 0) {
      $bedrag = ($bedrag * -1);
      $creditSchrijf = $bedrag;
    } else {
      $debetSchrijf = $bedrag;
    }
  } elseif ($kolom == "Credit") {
    $bedrag = $credit - $debet;
    if ($bedrag < 0) {
      $bedrag = ($bedrag * -1);
      $debetSchrijf = $bedrag;
    } else {
      $creditSchrijf = $bedrag;
    }
  }
  
  if ($GROOTBOEKID == $toongrootboekidSQL) {
    echo "<br/>credit: $credit, debet: $debet";
    echo "<br/>bedrag: $bedrag";
    echo "<br/>debetSchrijf: $debetSchrijf, creditSchrijf: $creditSchrijf";
  }

  if ($gescheiden == "Nee" || $gescheiden == "Verschil") {
    return $bedrag;
  } else {
    $return = array(0 => $debetSchrijf, 1 => $creditSchrijf);
    return $return;
  }
}

function getStappenDatumVanTot($datumVan, $datumTot, $tijdspan)
{
  $stappen = 0;

  $jaarvan = substr($datumVan, 0, 4);
  $maandvan = substr($datumVan, 5, 2);
  $dagvan = substr($datumVan, 8, 4);

  $jaartot = substr($datumTot, 0, 4);
  $maandtot = substr($datumTot, 5, 2);
  $dagtot = substr($datumTot, 8, 4);


  if ($tijdspan == "Jaar") {
    $stappen = substr($jaartot, 0, 4) - substr($jaarvan, 0, 4) + 1;
  } elseif ($tijdspan == "Half jaar") {
    $stappen = (substr($jaartot, 0, 4) - substr($jaarvan, 0, 4) + 1) * 2;
  } elseif ($tijdspan == "Kwartaal") {
    //Bereken hoeveel kwartalen.
    //$stappen = (substr($datumTot, 0, 4) - substr($datumVan, 0, 4) + 1) * 4;
    
    if ($jaartot == $jaarvan) {
      $stappen = (substr($maandtot, 0, 4) - substr($maandvan, 0, 4) + 1);
    } else {
      $stappen = (13 - $maandvan) + $maandtot + (($jaartot - ($jaarvan + 1)) * 12); //1 jaarovergang + 12 maanden voor overige jaren
    }
    $stappen = ceil(($stappen / 3));
  } elseif ($tijdspan == "Maand") {
    if ($jaartot == $jaarvan) {
      $stappen = (substr($maandtot, 0, 4) - substr($maandvan, 0, 4) + 1);
    } else {
      $stappen = (13 - $maandvan) + $maandtot + (($jaartot - ($jaarvan + 1)) * 12); //1 jaarovergang + 12 maanden voor overige jaren
    }
  } elseif ($tijdspan == "Dag") {
    $stappen = diff_days($datumVan, $datumTot) + 2;
  }

  return $stappen;
}

function winst($bedrijfsid, $datumVan, $datumTot, $conditie)
{
  $debetResulatenrek = resultaatResulatenrek($bedrijfsid, $datumVan, $datumTot, "Debet", $conditie);
  //echo "<br/> W1: $debetResulatenrek";
  $creditResultatenrek = resultaatResulatenrek($bedrijfsid, $datumVan, $datumTot, "Credit", $conditie);
  //echo "<br/> W2: $creditResultatenrek";

  return $creditResultatenrek - $debetResulatenrek;
}

function resultaatResulatenrek($bedrijfsid, $datumVan, $datumTot, $kolom, $conditie)
{
  global $pdo;

  $credit = 0;
  $debet = 0;

  $tegenkolom = "Debet";
  if ($kolom == "Debet") {
    $tegenkolom = "Credit";
  }

  $query = $pdo->prepare('SELECT ID FROM grootboekitems 
           WHERE BALANSKOSTEN = "Kosten of opbrengst"
             AND CREDITDEBET = :kolom
             AND grootboekitems.BEDRIJFSID = :bedrijfsid :conditie;');
  $query->bindValue('kolom', $kolom);
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->bindValue('conditie', $conditie);
  $query->execute();

  foreach ($query->fetchAll() as $data) {
    $tmpbedrag = telPostPerPeriodeOp($bedrijfsid, $datumVan, $datumTot, ($data["ID"] * 1), "Ja", $kolom);
    //Debet en credit kunnen niet kleiner zijn dan 0, omdat dat in de andere functie al omgedraait wordt.
    $debet += $tmpbedrag[0];
    $credit += $tmpbedrag[1];
  }

  //Reken restje uit van tegenrekening.
  $query = $pdo->prepare('SELECT ID 
            FROM grootboekitems
           WHERE BALANSKOSTEN = "Kosten of opbrengst""
             AND grootboekitems.BEDRIJFSID = :bedrijfsid
             AND CREDITDEBET = :creditdebet :conditie;');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->bindValue('creditdebet', $tegenkolom);
  $query->bindValue('conditie', $conditie);
  $query->execute();

  foreach ($query->fetchAll() as $data) {
    $tmpbedrag = telPostPerPeriodeOp($bedrijfsid, $datumVan, $datumTot, ($data["ID"] * 1), "Ja", $tegenkolom);

    //Debet en credit kunnen niet kleiner zijn dan 0, omdat dat in de andere functie al omgedraait wordt.
    $debet += $tmpbedrag[0];
    $credit += $tmpbedrag[1];
  }

  if ($kolom == "Debet") {
    $bedrag = $debet;
  } else {
    $bedrag = $credit;
  }

  return $bedrag;
}

function oft_grootboek_mutatie_kaart( $userid, $bedrijfsid ) {
  global $pdo;

  $datum_van = date( 'Y-m-d', 0 );
  $datum_tot = date( 'Y-m-d' );
  $grootboek_id = 0;
  $toevoeging = 0;

  if ( isset( $_REQUEST['DATUMVAN'] ) ) {
    $datum_van = $_REQUEST['DATUMVAN'];
  }
  if ( isset( $_REQUEST['DATUMTOT'] ) ) {
    $datum_tot = $_REQUEST['DATUMTOT'];
  }
  if ( isset( $_REQUEST['GROOTID'] ) ) {
    $identifier = $_REQUEST['GROOTID'];
    list( $grootboek_id, $toevoeging ) = explode( ':', $identifier );
  }

  if ( $grootboek_id ) {
    $query = $pdo->prepare('SELECT * FROM balans WHERE GROOTBOEKNR = :grootboekid AND DATUM > :datumvan AND DATUM <= :datumtot');
    $query->bindValue('grootboekid', $grootboek_id);
    $query->bindValue('datumvan', $datum_van);
    $query->bindValue('datumtot', $datum_tot);
    $query->execute();

    foreach ($query->fetchAll() as $line) {
      echo "mutatie kaart voor IDENTIFIER[$grootboek_id] EXT[$toevoeging]<br/>";
    }
  }
}

function mapping($nr) {
  $mapping = array();

  $mapping[] = array("NR" => 1000000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Intangible Assets", "NAAM" => "Goodwill", "VAN" => 10000000, "TOT" => 10000999);
  $mapping[] = array("NR" => 1000100, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Intangible Assets", "NAAM" => "Development", "VAN" => 10001000, "TOT" => 10001999);
  $mapping[] = array("NR" => 1000200, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Intangible Assets", "NAAM" => "Consessions / Licences / Patent", "VAN" => 10002000, "TOT" => 10002999);
  $mapping[] = array("NR" => 1001000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Tangible Assets", "NAAM" => "Buildings/Territories", "VAN" => 10010000, "TOT" => 10010999);
  $mapping[] = array("NR" => 1001100, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Tangible Assets", "NAAM" => "Machines / Installations", "VAN" => 10011000, "TOT" => 10011999);
  $mapping[] = array("NR" => 1001200, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Tangible Assets", "NAAM" => "Inventory", "VAN" => 10012000, "TOT" => 10012999);
  $mapping[] = array("NR" => 1001300, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Tangible Assets", "NAAM" => "Other Material Immovable Assets", "VAN" => 10013000, "TOT" => 10013999);
  $mapping[] = array("NR" => 1001400, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Tangible Assets", "NAAM" => "Land", "VAN" => 10014000, "TOT" => 10014999);
  $mapping[] = array("NR" => 1001500, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Tangible Assets", "NAAM" => "Cars and Transportation", "VAN" => 10015000, "TOT" => 10015999);
  $mapping[] = array("NR" => 1001600, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Tangible Assets", "NAAM" => "Stocks and Materials", "VAN" => 10016000, "TOT" => 10016999);
  $mapping[] = array("NR" => 1002000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "Dom. Participations with Exemption (Grp)", "VAN" => 10020000, "TOT" => 10020999);
  $mapping[] = array("NR" => 1002100, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "For. Participations with exemption (Grp)", "VAN" => 10021000, "TOT" => 10021999);
  $mapping[] = array("NR" => 1002200, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "Dom. Participations without Exemption", "VAN" => 10022000, "TOT" => 10022999);
  $mapping[] = array("NR" => 1002300, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "For. Participations without exemption", "VAN" => 10023000, "TOT" => 10023999);
  $mapping[] = array("NR" => 1002400, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "Other Investments", "VAN" => 10024000, "TOT" => 10024999);
  $mapping[] = array("NR" => 1002500, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "Loans to Group companies", "VAN" => 10025000, "TOT" => 10025999);
  $mapping[] = array("NR" => 1002600, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "Loans to Shareholders", "VAN" => 10026000, "TOT" => 10026999);
  $mapping[] = array("NR" => 1002700, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Financial Fixed Assets", "NAAM" => "Loans to Other", "VAN" => 10027000, "TOT" => 10027999);
  $mapping[] = array("NR" => 1010000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "I/C Shareholder Total", "VAN" => 10100000, "TOT" => 10100999);
  $mapping[] = array("NR" => 1010001, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "I/C Shareholder", "VAN" => 10100000, "TOT" => 10100490);
  $mapping[] = array("NR" => 1010002, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "I/C Shareholder Royalties", "VAN" => 10100500, "TOT" => 10100999);
  $mapping[] = array("NR" => 1010100, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Interest I/C Shareholder (100)", "VAN" => 10101000, "TOT" => 10101999);
  $mapping[] = array("NR" => 1012000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "I/C Group companies Total", "VAN" => 10120000, "TOT" => 10120999);
  $mapping[] = array("NR" => 1012001, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "I/C Groupcompanies", "VAN" => 10120000, "TOT" => 10120490);
  $mapping[] = array("NR" => 1012002, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "I/C Groupcompanies Royalties", "VAN" => 10120500, "TOT" => 10120999);
  $mapping[] = array("NR" => 1012100, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Interest I/C Group companies (120)", "VAN" => 10121000, "TOT" => 10121999);
  $mapping[] = array("NR" => 1012500, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Interest Loans Short (025)", "VAN" => 10125000, "TOT" => 10125999);
  $mapping[] = array("NR" => 1012600, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Interest Loans Short (026)", "VAN" => 10126000, "TOT" => 10126999);
  $mapping[] = array("NR" => 1012700, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Interest Loans Short (027)", "VAN" => 10127000, "TOT" => 10127999);
  $mapping[] = array("NR" => 1013000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Accounts Receivable", "VAN" => 10130000, "TOT" => 10130999);
  $mapping[] = array("NR" => 1015000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Other receivables", "VAN" => 10150000, "TOT" => 10150999);
  $mapping[] = array("NR" => 1015001, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Other receivables witholding Tax ref/rec", "VAN" => 10150401, "TOT" => 10150490);
  $mapping[] = array("NR" => 1016000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Taxes receivable", "VAN" => 10160000, "TOT" => 10160999);
  $mapping[] = array("NR" => 1017000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Pending Assets", "VAN" => 10170000, "TOT" => 10170999);
  $mapping[] = array("NR" => 1018000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Liquid Assets", "VAN" => 10180000, "TOT" => 10180999);
  $mapping[] = array("NR" => 1019000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "Current Assets", "NAAM" => "Other Investments", "VAN" => 10190000, "TOT" => 10190999);

  $mapping[] = array("NR" => 1020000, "TOPRUBRIEK" => "ASSETS", "SUBRUBRIEK" => "I do not know", "NAAM" => "I really do not know", "VAN" => 10200000, "TOT" => 10299999);

  $mapping[] = array("NR" => 1030000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "I/C Shareholder Total", "VAN" => 10300000, "TOT" => 10300999);
  $mapping[] = array("NR" => 1030001, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "I/C Shareholder", "VAN" => 10300000, "TOT" => 10300490);
  $mapping[] = array("NR" => 1030002, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "I/C Shareholder Royalties", "VAN" => 10300500, "TOT" => 10300999);
  $mapping[] = array("NR" => 1030100, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest I/C Shareholder (300)", "VAN" => 10301000, "TOT" => 10301999);
  $mapping[] = array("NR" => 1032000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "I/C Group companies Total", "VAN" => 10320000, "TOT" => 10320999);
  $mapping[] = array("NR" => 1032001, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "I/C Groupcompanies", "VAN" => 10320000, "TOT" => 10320490);
  $mapping[] = array("NR" => 1032002, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "I/C Groupcompanies Royalties", "VAN" => 10320500, "TOT" => 10320999);
  $mapping[] = array("NR" => 1032100, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest I/C Group companies (320)", "VAN" => 10321000, "TOT" => 10321999);
  $mapping[] = array("NR" => 1032200, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Debts to Credit-Institutions (short)", "VAN" => 10322000, "TOT" => 10322999);
  $mapping[] = array("NR" => 1032300, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Debts to Credit-Institutions", "VAN" => 10323000, "TOT" => 10323999);
  $mapping[] = array("NR" => 1032401, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Loans Short (400)", "VAN" => 10324000, "TOT" => 10324090);
  $mapping[] = array("NR" => 1032402, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Loans Short (401)", "VAN" => 10324100, "TOT" => 10324190);
  $mapping[] = array("NR" => 1032500, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Loans Short (402)", "VAN" => 10325000, "TOT" => 10325999);
  $mapping[] = array("NR" => 1032600, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Loans Short (403)", "VAN" => 10326000, "TOT" => 10326999);
  $mapping[] = array("NR" => 1032700, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Loans Short (404)", "VAN" => 10327000, "TOT" => 10327999);
  $mapping[] = array("NR" => 1032800, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Loans Short (405)", "VAN" => 10328000, "TOT" => 10328999);
  $mapping[] = array("NR" => 1032900, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Interest Loans Short (406)", "VAN" => 10329000, "TOT" => 10329999);
  $mapping[] = array("NR" => 1033000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Accounts Payable", "VAN" => 10330000, "TOT" => 10330999);
  $mapping[] = array("NR" => 1034000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Liabilities Wages", "VAN" => 10340000, "TOT" => 10340999);
  $mapping[] = array("NR" => 1035000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Other Liabilities", "VAN" => 10350000, "TOT" => 10350999);
  $mapping[] = array("NR" => 1036000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Taxes", "VAN" => 10360000, "TOT" => 10360999);
  $mapping[] = array("NR" => 1037000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Pending Liabilities", "VAN" => 10370000, "TOT" => 10370999);
  $mapping[] = array("NR" => 1038000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Costs Payable", "VAN" => 10380000, "TOT" => 10380999);
  $mapping[] = array("NR" => 1039000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Current Liabilities", "NAAM" => "Other Short Term Debts", "VAN" => 10390000, "TOT" => 10390999);
  $mapping[] = array("NR" => 1040000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Longterm Liabilities", "NAAM" => "Loans from Groupcompanies", "VAN" => 10400000, "TOT" => 10400999);
  $mapping[] = array("NR" => 1040100, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Longterm Liabilities", "NAAM" => "Loans from Shareholders", "VAN" => 10401000, "TOT" => 10401999);
  $mapping[] = array("NR" => 1040200, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Longterm Liabilities", "NAAM" => "Loans Subordinated", "VAN" => 10402000, "TOT" => 10402999);
  $mapping[] = array("NR" => 1040300, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Longterm Liabilities", "NAAM" => "Loans Convertible", "VAN" => 10403000, "TOT" => 10403999);
  $mapping[] = array("NR" => 1040400, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Longterm Liabilities", "NAAM" => "Bonds", "VAN" => 10404000, "TOT" => 10404999);
  $mapping[] = array("NR" => 1040500, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Longterm Liabilities", "NAAM" => "Debts to Credit-Institutions", "VAN" => 10405000, "TOT" => 10405999);
  $mapping[] = array("NR" => 1040600, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Longterm Liabilities", "NAAM" => "Other Long Term Debts", "VAN" => 10406000, "TOT" => 10406999);
  $mapping[] = array("NR" => 1040700, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Pension Provisions", "VAN" => 10407000, "TOT" => 10407999);
  $mapping[] = array("NR" => 1040800, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Backservice Obligations", "VAN" => 10408000, "TOT" => 10408999);
  $mapping[] = array("NR" => 1040900, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Deferred Taxes", "VAN" => 10409000, "TOT" => 10409999);
  $mapping[] = array("NR" => 1041000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Redeeming Losses Partcipations (020)", "VAN" => 10410000, "TOT" => 10410999);
  $mapping[] = array("NR" => 1041100, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Redeeming Losses Partcipations (021)", "VAN" => 10411000, "TOT" => 10411999);
  $mapping[] = array("NR" => 1041200, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Redeeming Losses Partcipations (022)", "VAN" => 10412000, "TOT" => 10412999);
  $mapping[] = array("NR" => 1041300, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Redeeming Losses Partcipations (023)", "VAN" => 10413000, "TOT" => 10413999);
  $mapping[] = array("NR" => 1042000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Provisions", "NAAM" => "Other Provisions", "VAN" => 10420000, "TOT" => 10420999);
  $mapping[] = array("NR" => 1050000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Share Capital", "VAN" => 10500000, "TOT" => 10500999);
  $mapping[] = array("NR" => 1051000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Share Premium", "VAN" => 10510000, "TOT" => 10510999);
  $mapping[] = array("NR" => 1051100, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Legal Reserve", "VAN" => 10511000, "TOT" => 10511999);
  $mapping[] = array("NR" => 1051200, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Equalized Costreserve", "VAN" => 10512000, "TOT" => 10512999);
  $mapping[] = array("NR" => 1051300, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Reserve Insurance Domestic Risc", "VAN" => 10513000, "TOT" => 10513999);
  $mapping[] = array("NR" => 1051400, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Export Reserve", "VAN" => 10514000, "TOT" => 10514999);
  $mapping[] = array("NR" => 1051500, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Capital Replacement Reserve", "VAN" => 10515000, "TOT" => 10515999);
  $mapping[] = array("NR" => 1051600, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Capital Risc Reserve", "VAN" => 10516000, "TOT" => 10516999);
  $mapping[] = array("NR" => 1051700, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Statutorial Capital Reserve", "VAN" => 10517000, "TOT" => 10517999);
  $mapping[] = array("NR" => 1051800, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Agio Reserve", "VAN" => 10518000, "TOT" => 10518999);
  $mapping[] = array("NR" => 1051900, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Revaluation Reserve", "VAN" => 10519000, "TOT" => 10519999);
  $mapping[] = array("NR" => 1052000, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Other Reserve", "VAN" => 10520001, "TOT" => 10520999);
  $mapping[] = array("NR" => 1052100, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Dividend", "VAN" => 10521000, "TOT" => 10521999);
  $mapping[] = array("NR" => 1052200, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Other Capital Withdrawels", "VAN" => 10522000, "TOT" => 10522999);
  $mapping[] = array("NR" => 1052300, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Capital Third Parties", "VAN" => 10523000, "TOT" => 10523999);
  $mapping[] = array("NR" => 1052400, "TOPRUBRIEK" => "EQUITY AND LIABILITIES", "SUBRUBRIEK" => "Equity", "NAAM" => "Tax Credit", "VAN" => 10524000, "TOT" => 10524999);
  $mapping[] = array("NR" => 1060000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest income; Participations and Grp", "VAN" => 10600000, "TOT" => 10600999);
  $mapping[] = array("NR" => 1060100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest income; Shareholders", "VAN" => 10601000, "TOT" => 10601999);
  $mapping[] = array("NR" => 1060200, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest income; Bank", "VAN" => 10602000, "TOT" => 10602999);
  $mapping[] = array("NR" => 1060300, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest income; Other", "VAN" => 10603000, "TOT" => 10603999);
  $mapping[] = array("NR" => 1061000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest exp; Participations and Groupco", "VAN" => 10610000, "TOT" => 10610999);
  $mapping[] = array("NR" => 1061100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest exp; Shareholders", "VAN" => 10611000, "TOT" => 10611999);
  $mapping[] = array("NR" => 1061200, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest exp; Bank", "VAN" => 10612000, "TOT" => 10612999);
  $mapping[] = array("NR" => 1061300, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Interest Income", "NAAM" => "Interest exp; Other", "VAN" => 10613000, "TOT" => 10613999);
  $mapping[] = array("NR" => 1070000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "General and Administrative Expenses", "NAAM" => "General and Administrative Expenses", "VAN" => 10700000, "TOT" => 10700999);
  $mapping[] = array("NR" => 1072500, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Salaries", "NAAM" => "Salaries", "VAN" => 10725000, "TOT" => 10725999);
  $mapping[] = array("NR" => 1075000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Depreciation Intangible assets", "NAAM" => "Depreciation Intangible", "VAN" => 10750000, "TOT" => 10750999);
  $mapping[] = array("NR" => 1075100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Depreciation Intangible assets", "NAAM" => "Depreciation Tangible EQUITY AND LIABILITIES", "VAN" => 10751000, "TOT" => 10751999);
  $mapping[] = array("NR" => 1076000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Capital Tax", "NAAM" => "Capital Tax", "VAN" => 10760000, "TOT" => 10760999);
  $mapping[] = array("NR" => 1077500, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Special Alterations", "NAAM" => "Extra Ordinary Val.  Intangible", "VAN" => 10775000, "TOT" => 10775999);
  $mapping[] = array("NR" => 1077600, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Special Alterations", "NAAM" => "Extra Ordinary Val. Tangible EQUITY AND LIABILITIES", "VAN" => 10776000, "TOT" => 10776999);
  $mapping[] = array("NR" => 1077700, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Special Alterations", "NAAM" => "Extra Ordinary Val. Liquid EQUITY AND LIABILITIES", "VAN" => 10777000, "TOT" => 10777999);
  $mapping[] = array("NR" => 1077800, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Special Alterations", "NAAM" => "Extra Ordinary Val. Receivables", "VAN" => 10778000, "TOT" => 10778999);
  $mapping[] = array("NR" => 1077900, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Special Alterations", "NAAM" => "Extra Ordinary Val. Stocks", "VAN" => 10779000, "TOT" => 10779999);
  $mapping[] = array("NR" => 1078000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Special Alterations", "NAAM" => "Profit on EQUITY AND LIABILITIES", "VAN" => 10780000, "TOT" => 10780999);
  $mapping[] = array("NR" => 1078100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Special Alterations", "NAAM" => "Result on Sales EQUITY AND LIABILITIES", "VAN" => 10781000, "TOT" => 10781999);
  $mapping[] = array("NR" => 1079900, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Costprice Revenue", "NAAM" => "Costprice Revenue Total", "VAN" => 10799000, "TOT" => 10799999);
  $mapping[] = array("NR" => 1079901, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Costprice Revenue", "NAAM" => "Costprice Revenue Excl Royalty", "VAN" => 10799000, "TOT" => 10799090);
  $mapping[] = array("NR" => 1079902, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Costprice Revenue", "NAAM" => "Costprice Revenue Royalty", "VAN" => 10799100, "TOT" => 10799190);
//  $mapping[] = array("NR" => 1080001, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Revenue", "NAAM" => "Revenue Total", "VAN" => 1080000, "TOT" => 1080000);
  $mapping[] = array("NR" => 1080001, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Revenue", "NAAM" => "Revenue excl Royalty", "VAN" => 10800000, "TOT" => 10800090);
  $mapping[] = array("NR" => 1080002, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Revenue", "NAAM" => "Revenue Royalty", "VAN" => 10800100, "TOT" => 10800190);
  $mapping[] = array("NR" => 1080003, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Revenue", "NAAM" => "Revenue Royalty tax", "VAN" => 10800200, "TOT" => 10800290);
  $mapping[] = array("NR" => 1080004, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Revenue", "NAAM" => "Revenue Rent Real Estate", "VAN" => 10800300, "TOT" => 10800390);
  $mapping[] = array("NR" => 1080005, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Revenue", "NAAM" => "Revenue Securities", "VAN" => 10800400, "TOT" => 10800490);
  $mapping[] = array("NR" => 1082000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 020", "VAN" => 10820000, "TOT" => 10820999);
  $mapping[] = array("NR" => 1082001, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revalution Grp 020", "VAN" => 10820000, "TOT" => 10820200);
  $mapping[] = array("NR" => 1082002, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Dividend Grp 020", "VAN" => 10820201, "TOT" => 10820400);
  $mapping[] = array("NR" => 1082003, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Sale Grp 020", "VAN" => 10820601, "TOT" => 10820800);
  $mapping[] = array("NR" => 1082100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 021", "VAN" => 10821000, "TOT" => 10821999);
  $mapping[] = array("NR" => 1082101, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revalution Grp 021", "VAN" => 10821000, "TOT" => 10821200);
  $mapping[] = array("NR" => 1082102, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Dividend Grp 021", "VAN" => 10821201, "TOT" => 10821400);
  $mapping[] = array("NR" => 1082103, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Sale Grp 021", "VAN" => 10821601, "TOT" => 10821800);
  $mapping[] = array("NR" => 1082200, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 022", "VAN" => 10822000, "TOT" => 10822999);
  $mapping[] = array("NR" => 1082201, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revalution Grp 022", "VAN" => 10822000, "TOT" => 10822200);
  $mapping[] = array("NR" => 1082202, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Dividend Grp 022", "VAN" => 10822201, "TOT" => 10822400);
  $mapping[] = array("NR" => 1082203, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Sale Grp 022", "VAN" => 10822601, "TOT" => 10822800);
  $mapping[] = array("NR" => 1082300, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 023", "VAN" => 10823000, "TOT" => 10823999);
  $mapping[] = array("NR" => 1082301, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revalution Grp 023", "VAN" => 10823000, "TOT" => 10823200);
  $mapping[] = array("NR" => 1082302, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Dividend Grp 023", "VAN" => 10823201, "TOT" => 10823400);
  $mapping[] = array("NR" => 1082303, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Sale Grp 023", "VAN" => 10823601, "TOT" => 10823800);
  $mapping[] = array("NR" => 1082400, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 024", "VAN" => 10824000, "TOT" => 10824999);
  $mapping[] = array("NR" => 1082401, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 024", "VAN" => 10824000, "TOT" => 10824200);
  $mapping[] = array("NR" => 1082402, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Dividend Grp 024", "VAN" => 10824201, "TOT" => 10824400);
  $mapping[] = array("NR" => 1082403, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 024", "VAN" => 10824401, "TOT" => 10824600);
  $mapping[] = array("NR" => 1082404, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Sale Grp 024", "VAN" => 10824601, "TOT" => 10824800);
  $mapping[] = array("NR" => 1082500, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 025", "VAN" => 10825000, "TOT" => 10825999);
  $mapping[] = array("NR" => 1082501, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revalution Grp 025", "VAN" => 10825000, "TOT" => 10825200);
  $mapping[] = array("NR" => 1082502, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 025", "VAN" => 10825201, "TOT" => 10825400);
  $mapping[] = array("NR" => 1082600, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 026", "VAN" => 10826000, "TOT" => 10826999);
  $mapping[] = array("NR" => 1082601, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 026", "VAN" => 10826000, "TOT" => 10826200);
  $mapping[] = array("NR" => 1082602, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 026", "VAN" => 10826201, "TOT" => 10826400);
  $mapping[] = array("NR" => 1082700, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 027", "VAN" => 10827000, "TOT" => 10827999);
  $mapping[] = array("NR" => 1082701, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 027", "VAN" => 10827000, "TOT" => 10827200);
  $mapping[] = array("NR" => 1082702, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 027", "VAN" => 10827201, "TOT" => 10827400);
  $mapping[] = array("NR" => 1084000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result grp 400", "VAN" => 10840000, "TOT" => 10840999);
  $mapping[] = array("NR" => 1084001, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 400", "VAN" => 10840000, "TOT" => 10840200);
  $mapping[] = array("NR" => 1084002, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 400", "VAN" => 10840201, "TOT" => 10840400);
  $mapping[] = array("NR" => 1084100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 401", "VAN" => 10841000, "TOT" => 10841999);
  $mapping[] = array("NR" => 1084101, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 401", "VAN" => 10841000, "TOT" => 10841200);
  $mapping[] = array("NR" => 1084102, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 401", "VAN" => 10841201, "TOT" => 10841400);
  $mapping[] = array("NR" => 1084200, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 402", "VAN" => 10842000, "TOT" => 10842999);
  $mapping[] = array("NR" => 1084201, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 402", "VAN" => 10842000, "TOT" => 10842200);
  $mapping[] = array("NR" => 1084202, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 402", "VAN" => 10842201, "TOT" => 10842400);
  $mapping[] = array("NR" => 1084300, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 403", "VAN" => 10843000, "TOT" => 10843999);
  $mapping[] = array("NR" => 1084301, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 403", "VAN" => 10843000, "TOT" => 10843200);
  $mapping[] = array("NR" => 1084302, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 403", "VAN" => 10843201, "TOT" => 10843400);
  $mapping[] = array("NR" => 1084400, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 404", "VAN" => 10844000, "TOT" => 10844999);
  $mapping[] = array("NR" => 1084401, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 404", "VAN" => 10844000, "TOT" => 10844200);
  $mapping[] = array("NR" => 1084402, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 404", "VAN" => 10844201, "TOT" => 10844400);
  $mapping[] = array("NR" => 1084500, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 405", "VAN" => 10845000, "TOT" => 10845999);
  $mapping[] = array("NR" => 1084501, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 405", "VAN" => 10845000, "TOT" => 10845200);
  $mapping[] = array("NR" => 1084502, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 405", "VAN" => 10845201, "TOT" => 10845400);
  $mapping[] = array("NR" => 1084600, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Result Grp 406", "VAN" => 10846000, "TOT" => 10846999);
  $mapping[] = array("NR" => 1084601, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Revaluation Grp 406", "VAN" => 10846000, "TOT" => 10846200);
  $mapping[] = array("NR" => 1084602, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Participations", "NAAM" => "Interest Grp 406", "VAN" => 10846201, "TOT" => 10846400);
  $mapping[] = array("NR" => 1085000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Exceptional Income", "NAAM" => "Extra Ordinary Income", "VAN" => 10850000, "TOT" => 10850999);
  $mapping[] = array("NR" => 1085100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Exceptional Expenses", "NAAM" => "Extra Ordinary Expenses", "VAN" => 10851000, "TOT" => 10851999);
  $mapping[] = array("NR" => 1087500, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Taxes", "NAAM" => "Taxes", "VAN" => 10875000, "TOT" => 10875999);
  $mapping[] = array("NR" => 1090000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Rate differences", "NAAM" => "Rate differences by Currency", "VAN" => 10900000, "TOT" => 10900999);
  $mapping[] = array("NR" => 1090100, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Rate differences", "NAAM" => "Rate Differences by Account", "VAN" => 10901000, "TOT" => 10906100);
  $mapping[] = array("NR" => 1095000, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Rate differences", "NAAM" => "Currency differences", "VAN" => 10950000, "TOT" => 10950999);
  $mapping[] = array("NR" => 1099900, "TOPRUBRIEK" => "FEE INCOME", "SUBRUBRIEK" => "Other", "NAAM" => "Other Diffrences", "VAN" => 10999993, "TOT" => 10999999);
  $mapping[] = array("NR" => 1100000, "TOPRUBRIEK" => "Result", "SUBRUBRIEK" => "Result", "NAAM" => "Result", "VAN" => 11000000, "TOT" => 11000000);

  $return = array();

  foreach($mapping as $rowNr => $map) {
    if($nr >= $map["VAN"] && $nr <= $map["TOT"]) {
      $return = $map;
    }
  }
  
  return $return;
}