<?php
sercurityCheck();

function oft_clients($userid, $bedrijfsid) {
  global $db;

  $titel           = tl('Clients');
  $tabel           = "oft_clients";
  $toevoegen       = "";
  if(isRechtenOFT($bedrijfsid, "CONTRACTEN")) {
    $toevoegen     = "content.php?SITE=".$tabel."_add";
  }
  $bewerken        = oft_inputform_link_short("content.php?SITE=".$tabel."_edit&ID=-ID-");
  $verwijderen     = "";
  $submenuitems    = oft_inputform_link($toevoegen, "New client");

  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'DATEREJECTION', $toevoegen, $bewerken, $verwijderen);

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_clients_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_clients";
  $toevoegen       = "content.php?SITE=".$tabel."_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_".$tabel."_edit&ID=-ID-");
  $verwijderen     = "";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'DATEREJECTION', $toevoegen, $bewerken, $verwijderen);
}

function oft_clients_add($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_clients.htm";
  $tabel        = "oft_clients";
  $titel        = "New client";

  if(isset($_REQUEST["BID"])) {
    $bedrijfsid = $_REQUEST["BID"];
  }

  oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_clients_edit($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_clients.htm";
  $tabel        = "oft_clients";
  $titel        = "Edit client";

  oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}
