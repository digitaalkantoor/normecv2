<?php

function getTableStructure($user = 0, $company = 0, $table = "", $type = "", $convert = true)
{
    global $pdo;
    if(isset($_REQUEST['SITE']) && ($_REQUEST['SITE'] == "entity_addressbook" || $_REQUEST['SITE'] == "entity_addressbook_ajax")) {
        $table = "entity_persons";
    }

    $return = [
        "arrayVelden" => [],
        "arrayVeldnamen" => [],
        "arrayVeldType" => [],
        "arrayZoekvelden" => [],
    ];
    $fields = [];

    $entity_title = ucfirst(check("default_name_of_entity", "Entity", $company));

//  New structure that"s more fail safe. And overal easier to read and beter in use.

    switch ($table) {

        case "entity_favorites":
            $fields = [
                "NAME" => [
                    "title" => "Target",
                ],
                "countries|ISO" => [
                    "title" => "Country",
                    "type" => "pivot",
                    "pivot" => ["field" => "COUNTRY_ID"],
                ],
                "stages|NAME" => [
                    "title" => "Stage",
                    "type"  => "pivot",
                    "pivot" => ["field" => "STAGE_ID"],
                ],
                "activities|NAME" => [
                    "title" => "Activity",
                    "type" => "pivot",
                    "pivot" => ["field" => "ACTIVITY_ID"],
                    "searchable" => "ACTIVITY_ID"
                ],
                "segments|NAME" => [
                    "title" => "Sector",
                    "type" => "pivot",
                    "pivot" => ["field" => "SEGMENT_ID"],
                    "searchable" => "SEGMENT_ID"
                ],
                "REVENUE" => [
                    "title" => "Revenue",
                ],
                "EBITDA" => [
                    "title" => "EBITDA",
                ],
                "HEADCOUNT" => [
                    "title" => "Headcount",
                ],
                "VALUATION" => [
                    "title" => "Valuation",
                ],
            ];

            break;
        case "entity_pipeline":
            $fields = [
//                "ID" => ["title" => "ID"],
                "HEADLINE_DATE" => [
                    "title" => "Recall Date",
                    "type" => "date",
                ],
                "RATE_POTENTIAL" => [
                    "title" => "Rating",
                ],
                "NAME" => [
                    "title" => "Target Name",
                ],
                "countries|ISO" => [
                    "title" => "Geo",
                    "type" => "pivot",
                    "pivot" => ["field" => "COUNTRY_ID"],
                ],
                "stages|NAME" => [
                    "title" => "Stage",
                    "type"  => "pivot",
                    "pivot" => ["field" => "STAGE_ID"],
                ],
                "HEADLINE_COMMENT" => [
                    "title" => "Comment",
                ],

                //
//                "NAME" => [
//                    "title" => $entity_title,
//                ],
//                "countries|ISO" => [
//                    "title" => "Country",
//                    "filtername" => "countries|NAME",
//                    "type" => "pivot",
//                    "pivot" => ["field" => "COUNTRY_ID"],
//                ],
//                "RATE_POTENTIAL" => [
//                    "title" => "Potential",
//                    "searchable" => "RATE_POTENTIAL"
//                ],
//                "stages|NAME" => [
//                    "title" => "Stage",
//                    "type" => "pivot",
//                    "pivot" => ["field" => "STAGE_ID"],
//                    "searchable" => "STAGE_ID"
//                ],
//                "REVENUE" => [
//                    "title" => "Revenue",
//                ],
//                "EBITDA" => [
//                    "title" => "Ebitda",
//                ],
//                "VALUATION" => [
//                    "title" => "Valuation",
//                ],
//                "GROWTH" => [
//                    "title" => "Growth",
//                ],
//                "MARGIN" => [
//                    "title" => "Margin",
//                ],
//                "strategics|NAME" => [
//                    "title" => "Strategic",
//                    "type" => "pivot",
//                    "pivot" => ["field" => "STRATEGIC_ID"],
//                ],
//                "managements|NAME" => [
//                    "title" => "Management",
//                    "type" => "pivot",
//                    "pivot" => ["field" => "MANAGEMENT_ID"],
//                ],
//                "SYNERGIES" => [
//                    "title" => "Synergies",
//                ],
//                "SKIP_activities|NAME" => [
//                    "title" => "Activity",
//                    "type" => "pivot",
//                    "pivot" => ["field" => "ACTIVITY_ID"],
//                    "searchable" => "ACTIVITY_ID"
//                ],
//                "SKIP_segments|NAME" => [
//                    "title" => "Sector",
//                    "type" => "pivot",
//                    "pivot" => ["field" => "SEGMENT_ID"],
//                    "searchable" => "SEGMENT_ID"
//                ],
//                "SKIP_entity_tags|TAG_ID|tags|NAME" => [
//                    "title" => "Tag",
//                    "type" => "pivot",
//                    "pivot" => ["field" => "entity_tags|ENTITY_ID"],
//                    "searchable" => "PIVOT_TAG_ID",
//                ],
//                "SKIP_FAVOURITE" => [
//                    "title" => "Favourite",
//                    "type" => "bool",
//                    "searchable" => "FAVOURITE",
//                ],
//                "HEADLINE_ACTION" => [
//                    "title" => "Action",
//                ],
//                "HEADLINE_DATE" => [
//                    "title" => "",
//                ],
//                "SKIP_HEADLINE_COMMENT" => [
//                    "title" => "Comment",
//                    "type" => 'secondrow'
//                ],
            ];
            $return["arrayZoekvelden"] = ["SEARCH"];
            break;

        case "persons_overview":
            $fields = ["VOORNAAM", "ACHTERNAAM", "bedrijf|BEDRIJFSNAAM"];
            $return['arrayZoekvelden'] = ["SEARCH"];
            break;

        case "entity_persons":
            $fields = [
                "ADVISOR" => [
                    "title" => "Advisor/Target",
                ],
                "NAME" => [
                    "title" => "Company",
                ],
                "entity_persons|NAME" => [
                    "title" => "Name",
                    "type"  => "pivot",
                    "pivot" => ["field" => "ID"],
                ],
                "entity_persons|FUNCTION" => [
                    "title" => "Function",
                    "type"  => "pivot",
                    "pivot" => ["field" => "ID"],
                ],
                "countries|ISO" => [
                    "title" => "Country",
                    "type" => "pivot",
                    "pivot" => ["field" => "COUNTRY_ID"],
                ],
                "entity_persons|EMAIL" => [
                    "title" => "Email",
                    "type"  => "pivot",
                    "pivot" => ["field" => "ID"],
                ],
                "entity_persons|PHONE" => [
                    "title" => "Phone",
                    "type"  => "pivot",
                    "pivot" => ["field" => "ID"],
                ]
            ];

            break;

        case "documentbeheer":
        case "file_management":
            $fields = [
                "NAAM" => [],
                "JAAR" => [],
                "DOCTYPE" => [],
                "DATUM" => [
                    "title" => "Start date",
                    "type" => "datum"
                ],
                "bedrijf|BEDRIJFSNAAM" => [],
                "BESTANDSNAAM" => [],
            ];
            $return['arrayZoekvelden'] = ["SEARCH"];
            break;

        case "oft_entities_dif_overzicht":
            $fields = ["BEDRIJFNR", "BEDRIJFSNAAM", "OMSCHRIJVING", "LAND", "ENDREDEN"];
            break;

        case "notifications_overview":
            $fields = ["STATUS", "FINANCIALYEAR", "VERVALDATUM", "EVENT", "bedrijf|LAND", "bedrijf|BEDRIJFSNAAM"];
            break;

        case "oft_management_overzicht":
            $fields = [
                "bedrijf|BEDRIJFSNAAM" => [
                    "pivot" => [
//                        "field" => "ADMINID"
                    ],
                ],
                "personeel|ACHTERNAAM" => [],
                "FUNCTION" => [],
                "AUTHORIZATION" => [],
                "DATEOFAPPOINTENT" => [],
                "ENDATE" => [],
            ];
            break;

        case "oft_tax_overzicht":
            $fields = [
                "STATUS" => ['searchable' => false],
                "FINANCIALYEAR" => ['searchable' => false],
                "STANDARDDUEDATE" => [],
                "EXTENTION" => [],
                "FILE" => [
                    "title" => "File",
                    "type" => "bestand_in_year_from_type"
                ],
            ];
            break;

        case "oft_annual_accounts_overzicht":
            $fields = [
                "STATUS" => ['searchable' => false],
                "FINANCIALYEAR" => ['searchable' => false],
                "STANDARDDUEDATE" => [],
                "EXTENTION" => [],
                "FILE" => [
                    "title" => "File",
                    "type" => "bestand_in_year_from_type"
                ],
                "AUDIT" => [
                    "title" => "Audit",
                    "type" => "translate"
                ],
            ];
            break;

        case "oft_boardmeetings_overzicht":
            $fields = ["STATUS", "FINANCIALYEAR", "STANDARDDUEDATE", "DATUM", "TYPE"];
            break;

        case "documentbeheer_overzicht":
            $fields = ["NAAM", "JAAR", "DOCTYPE", "DATUM", "bedrijf|BEDRIJFSNAAM", "DELETED", "BESTANDSNAAM"];
            $return['arrayZoekvelden'] = ["SEARCH"];
            break;

        case "oft_entitie_documents_overzicht":
            $fields = [
                "BESTANDSNAAM" => [],
                "JAAR" => [],
                "DOCTYPE" => [],
                "DATUM" => [],
                "PERSONEELSLID" => [
                    "title" => "Updated by",
                    "type" => "person_name",
                    "searchable" => "UPDATEDBY"
                ],
            ];
            break;

        case "audittrail":
            $fields = [
                "UPDATEDATUM" => [
                    "title" => "Date",
                    "type" => "datum"
                ],
                "ACTION" => [
                    "title" => "Action",
                    "type" => "auditAction"
                ],
                "TABEL" => [
                    "title" => "Source",
                    "type" => "tabelSource"
                ],
                "KOLOM" => [
                    "title" => "Field",
                    "type" => "columnName"
                ],
                "OLDVALUE" => [
                    "title" => "Old value",
                ],
                "NEWVALUE" => [
                    "title" => "New value",
                ],
                "USERID" => [
                    "title" => "Changed by",
                    "type" => "medewerker"
                ],
                "bedrijf|BEDRIJFSNAAM" => [],
                "APPROVED" => [
                    "title" => "Approved",
                    "type" => "approved"
                ],
                "ID" => [
                    "title" => "Roll back",
                    "type" => "rollback"
                ],
                "APPROVEDBYUSERID" => [
                    "title" => "By",
                    "type" => "medewerker"
                ],
            ];
            break;

        case "oft_providers":
        case "oft_providers_overzicht":
            $fields = ["TYPE", "NAAM"];
            break;

        case "oft_entitie_numbers_overzicht":
            $fields = ["TYPE", "VALUE"];
            break;


        case "login_overzicht":
            $fields = [
                "USERNAME" => [],
                "PERSONEELSLID" => ["title" => "Person", "type" => "person_name"],
                "PROFIEL" => [],
                "SUSPEND" => [],
            ];
            break;
        case "login":
            $fields = [
                "USERNAME" => ["type" => "field", "required" => "Ja"],
                "PERSONEELSLID" => ["type" => "field", "required" => "Ja"],
                "PROFIEL" => ["type" => "PROFIEL", "required" => "Ja"],
                "SUSPEND" => ["type" => "selectjanee", "required" => "Nee"],
                "CRM" => ["type" => "rechten", "required" => "Ja"],
                "CONTRACTEN" => ["type" => "rechten", "required" => "Ja"],
                "MASTER" => ["type" => "selectJa", "required" => "Ja"],
                "NEWUSER" => ["type" => "newuser", "required" => "Nee"],
                "RECHTENTOT" => ["type" => "rechtentot", "required" => "Nee"],
                "BEDRIJFSID" => ["type" => "bedrijf", "required" => "Ja"],
                "LEGAL" => ["type" => "rechten", "required" => "Ja"],
                "COMPLIANCE" => ["type" => "rechten", "required" => "Ja"],
                "BOARDPACK" => ["type" => "rechten", "required" => "Ja"],
                "FINANCE" => ["type" => "rechten", "required" => "Ja"],
                "HR" => ["type" => "rechten", "required" => "Ja"],
                "CONTRACTS" => ["type" => "rechten", "required" => "Ja"],
                "MA" => ["type" => "rechten", "required" => "Ja"],
                "INTEGRATIONCHECKLIST" => ["type" => "rechten", "required" => "Ja"],
            ];
            break;
        case "personeel":
            $fields = [
                "VOORNAAM" => ["required" => "Nee"],
                "ACHTERNAAM" => ["required" => "Nee"],
                "EMAIL" => ["required" => "Nee"],
                "TELVAST" => ["required" => "Nee"],
                "BEDRIJFSID" => ["type" => "bedrijf", "required" => "Ja"],
                "TOTWANNEERCONTRACT" => ["type" => "datum", "required" => "Nee"],
                "MOB" => ["required" => "Nee"],
                "LAND" => ["type" => "land", "required" => "Nee"]
            ];
            break;
        case "oft_entities_ended_overzicht":
            $fields = ["BEDRIJFNR", "BEDRIJFSNAAM", "ENDDATUM", "LAND", "ENDREDEN"];
            break;
    }

    // Try to get some (dynamic) fields from the database
    if (!$fields) {
        $query = "SELECT * FROM table_structure "
            . "WHERE COMPANY_ID = $company AND `TABLE` = '$table' AND !DELETED AND (SHOW_COLUMN || SHOW_FILTER) "
            . "ORDER BY `ORDER`";
        $stmt = $pdo->query($query);
        if ($stmt->execute()) {
            foreach ($stmt->fetchAll($pdo::FETCH_OBJ) as $result) {
                $key = $result->COLUMN;
                $item = ['title' => $result->TITLE];
                if ($result->FILTERNAME) {
                    $item['filtername'] = $result->FILTERNAME;
                }
                if ($result->TYPE) {
                    $item['type'] = $result->TYPE;
                }
                if ($result->PIVOT) {
                    $item['pivot'] = ["field" => $result->PIVOT];
                }
                if (!$result->SHOW_COLUMN) {
                    $key = 'SKIP_' . $key;
                }
                if ($result->SHOW_FILTER && $result->SEARCHABLE) {
                    $item['searchable'] = $result->SEARCHABLE;
                }
                if ($result->PREFIX) {
                    $item['prefix'] = $result->PREFIX;
                }
                if ($result->SUFFIX) {
                    $item['suffix'] = $result->SUFFIX;
                }
                $fields[$key] = $item;
            }
            $return["arrayZoekvelden"] = ["SEARCH"];
        }
    }

//    echo "<pre>";
//    var_dump($fields);
//    echo "</pre>";
//    die();

    $defaults = [
        "ACHTERNAAM" => ["title" => "Surname", "required" => "Nee"],
        "AUTHORIZATION" => ["title" => "Board type", "type" => "AUTHORIZATION"],
        "BEDRIJFNR" => ["title" => "Short code"],
        "BEDRIJFSNAAM" => ["title" => $entity_title, "searchable" => "ENTITY"],
        "bedrijf|BEDRIJFSNAAM" => [
            "title" => $entity_title,
            "type" => "pivot",
            "pivot" => ["field" => "BEDRIJFSID"],
            "searchable" => "ENTITY_TABLE"
        ],
        "bedrijf|LAND" => [
            "title" => "Country",
            "type" => "pivot",
            "pivot" => ["field" => "BEDRIJFSID"],
            "searchable" => "COUNTRY_TABLE"
        ],
        "BESTANDSNAAM" => ["title" => "Filename", "type" => "bestand"],
        "DOCTYPE" => ["title" => "Category", "searchable" => "DOCTYPE"],
        "DATEOFAPPOINTENT" => ["title" => "Date of appointment", "type" => "datum"],
        "DATUM" => ["title" => "Start date", "type" => "datum"],
        "DELETED" => ["title" => "Deleted"],
        "ENDDATUM" => ["title" => "End date", "type" => "datum"],
        "ENDATE" => ["title" => "End date", "type" => "datum"],
        "ENDREDEN" => ["title" => "Status", "searchable" => "ENDREDEN"],
        "EVENT" => ["title" => "Type", "searchable" => "EVENT"],
        "EXTENTION" => ["title" => "Extension", "type" => "datum"],
        "FINANCIALYEAR" => ["title" => "Fiscal year", "searchable" => "FINANCIALYEAR"],
        "FUNCTION" => ["title" => "Function"],
        "JAAR" => ["title" => "Year", "searchable" => "YEAR"],
        "LAND" => ["title" => "Country", "searchable " => "COUNTRY"],
        "NAAM" => ['title' => "Name"],
        "OMSCHRIJVING" => ["title" => "Purpose", "searchable" => "OMSCHRIJVING"],
        "USERNAME" => ['title' => "Username"],
        "personeel|ACHTERNAAM" => ["title" => "Person", "type" => "pivot", "pivot" => ["field" => "USERID"]],
        "PROFIEL" => ['title' => "Role"],
        "STANDARDDUEDATE" => ["title" => "Due date", "type" => "datum"],
        "STATUS" => ["title" => "Status", "type" => "status"],
        "SUSPEND" => ["title" => "Suspend", "searchable" => "SUSPEND"],
        "TYPE" => ['title' => "Type"],
        "VALUE" => ['title' => "Value"],
        "VERVALDATUM" => ["title" => "Due date", "type" => "datum"],
        "VOORNAAM" => ["title" => "Given name", "required" => "Nee"],
    ];

    if (!isset($fields) || !$fields) {
        return [];
    }
    $return['fields'] = $fields;

// Begin Bobsoft converter.
    if ($convert) {
        $c = 0;
        foreach ($fields as $key => $properties) {
            // if properties are given and key is a position key, get the value and set is as a key
            if (is_numeric($key) && !is_array($properties)) {
                $key = $properties;
            }
            // if properties is empty and properties exist in defaults array, get the default values
            if (($properties == [] || !is_array($properties)) && isset($defaults[$key])) {
                $properties = $defaults[$key];
            }

            // get title from specific or defaults
            if (!isset($properties['title']) && isset($defaults[$key]) && isset($defaults[$key]['title'])) {
                $properties['title'] = $defaults[$key]['title'];
            }
            // get required from specific or defaults
            if (!isset($properties['required']) && isset($defaults[$key]) && isset($defaults[$key]['required'])) {
                $properties['required'] = $defaults[$key]['required'];
            }
            if (!isset($properties['title']) && !isset($properties['required'])) {
                continue;
            }

            if (isset($properties['title'])) {
                $return['arrayVeldnamen'][$key] = $properties['title'];
            }

            // get type from specific or defaults, of regular
            if (!isset($properties['type'])) {
                if (isset($defaults[$key]) && isset($defaults[$key]['type'])) {
                    $fieldType = $defaults[$key]['type'];
                } else {
                    $fieldType = "field";
                }
            } else {
                $fieldType = $properties['type'];
            }
            $return['arrayVeldType'][$key] = $fieldType;

            if (isset($properties['required'])) {
                $return['arrayVerplicht'][$key] = $properties['required'];
            }

            $field = "";
            // pivot implementation get defaults if specific isn't found.
            if ($fieldType == "pivot") {
                if ((!isset($properties['pivot']) || !isset($properties['pivot']['field']))
                    && (isset($defaults[$key]) && isset($defaults[$key]['pivot']))) {
                    $properties['pivot'] = $defaults[$key]['pivot'];
                }
                if (isset($properties['pivot'])) {
                    $field = "$fieldType|" . $properties['pivot']['field'] . "|" . $key;
                }
            }
            $return['arrayVelden'][$key] = $field ?: $key;

            if (isset($properties['searchable']) && $properties['searchable']) {
                $return['arrayZoekvelden'][$key] = $properties['searchable'];
            }
            $c++;
        }

        if ($type) {
            if ($type == "all") {
                return $return;
            } elseif (isset($return[$type])) {
                return $return[$type];
            }
            // Type to return is not available
            return false;
        }
        return $return['arrayVelden'];
    }
// End Bobsoft converter.

    return $return['fields'];
}