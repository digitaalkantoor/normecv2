<?php

function site_site_oft($site, $loginid, $userid)
{
    if ($site == "oft_entitie_overview" && isset($_REQUEST["ID"])) {
        $bedrijfsid = ps($_REQUEST["ID"], "nr");
    } elseif (isset($_REQUEST["BID"])) {
        $bedrijfsid = ps($_REQUEST["BID"], "nr");
    } else {
        $bedrijfsid = ps(getBedrijfsID($loginid), "nr");
    }

    //accessRightsPage($site, $bedrijfsid);

    if ($site == "oft_home") {
        if ($homepage = getValueFromStandaardWaarde('homepage')) {
            rd('/content.php?SITE='.$homepage);
        }
        head_oft($userid, $bedrijfsid, $site);
        oft_home($userid, $bedrijfsid);
        footer1();
        footer1();

    } elseif ($site == "oft_home_ajax") {
        oft_home_ajax($userid, $bedrijfsid);
    } elseif ($site == "deleteDocument") {
        deleteDocument($userid, $bedrijfsid);
    } elseif ($site == "oft_delete_item") {
        oft_delete_item($userid, $bedrijfsid);
    } elseif ($site == "deleteDocumentarchief") {
        deleteDocumentarchief($userid, $bedrijfsid);
    } elseif ($site == "users_samenvoegen") {
        head_oft($userid, $bedrijfsid, $site);
        users_samenvoegen($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_menu_reports") {
        head_oft($userid, $bedrijfsid, $site);
        oft_menu_reports($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_compliance_report") {
        head_oft($userid, $bedrijfsid, $site);
        oft_compliance_report($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_bulkdownload") {
        if (isset($_POST['download'])) {
            post_process();
        } else {
            head_oft($userid, $bedrijfsid, $site);
            oft_bulkdownload($userid, $bedrijfsid);
            footer1();
        }
    } elseif ($site == "oft_audit_trail") {
        head_oft($userid, $bedrijfsid, $site);
        oft_audit_trail($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_audit_trail_ajax") {
        oft_audit_trail_ajax($userid, $bedrijfsid);
    } elseif ($site == "setRollBack") {
        setRollBack($userid, $bedrijfsid);
    } elseif ($site == "setApproved") {
        setApproved($userid, $bedrijfsid);
    } elseif ($site == "oft_agenda") {
        head_oft($userid, $bedrijfsid, $site);
        oft_agenda($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_agenda") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_agenda($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "generate_agenda_json") {
        generate_agenda_json($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_agenda_new") {
        oft_entitie_agenda_new($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_agenda_edit") {
        oft_entitie_agenda_edit($userid, $bedrijfsid);
    } elseif ($site == "dragAndDrop") {
        head_oft($userid, $bedrijfsid, $site);
        dragAndDrop($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "draganddropupload") {
        draganddropupload($userid, $loginid, $bedrijfsid);
    } elseif ($site == "genDragAndDropUpload") {
        genDragAndDropUpload($userid, $bedrijfsid);
    } //  elseif ($site == "dragAndDrop_inkoop") { dragAndDrop_inkoop($userid, $bedrijfsid); }
    elseif ($site == "export_files_to_stick") {
        export_files_to_stick($userid, $bedrijfsid, "");
    } elseif ($site == "import_entities") {
        head_oft($userid, $bedrijfsid, $site);
        $obj = new import_export_excel_entities();
        $obj->import_excel_entities($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "rapport_magaging_director") {
        head_oft($userid, $bedrijfsid, $site);
        rapport_magaging_director($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_annual_accounts") {
        head_oft($userid, $bedrijfsid, $site);
        oft_annual_accounts($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_annual_accounts_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_annual_accounts_ajax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_tax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_tax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_tax_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_tax_ajax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_contracten") {
        head_oft($userid, $bedrijfsid, $site);
        oft_contracten($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_contracten_ajax") {
        oft_contracten_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_contracten_add") {
        oft_contracten_add($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_contracts_add") {
        oft_contracten_add($userid, $bedrijfsid);
    } elseif ($site == "oft_contracten_edit" || $site == "oft_entitie_contracts_edit") {
        oft_contracten_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_numbers") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_numbers($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_numbers_add") {
        oft_entitie_numbers_add($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_numbers_ajax") {
        oft_entitie_numbers_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_numbers_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_numbers_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_providers") {
        head_oft($userid, $bedrijfsid, $site);
        oft_providers($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_providers_add") {
        oft_providers_add($userid, $bedrijfsid);
    } elseif ($site == "oft_providers_ajax") {
        oft_providers_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_providers_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_providers_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_services") {
        head_oft($userid, $bedrijfsid, $site);
        oft_services($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_services_add") {
        oft_services_add($userid, $bedrijfsid);
    } elseif ($site == "oft_services_ajax") {
        oft_services_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_services_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_services_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_accounting") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_accounting($userid, $bedrijfsid);
        footer1();
    }
//  elseif ($site == "oft_entitie_accounting_add") {oft_entitie_accounting_add($userid, $bedrijfsid);}
//  elseif ($site == "oft_entitie_accounting_ajax") {oft_entitie_accounting_ajax($userid, $bedrijfsid);}
    elseif ($site == "oft_entitie_accounting_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_accounting_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_clients") {
        head_oft($userid, $bedrijfsid, $site);
        oft_clients($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_clients_ajax") {
        oft_clients_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_clients_add") {
        oft_clients_add($userid, $bedrijfsid);
    } elseif ($site == "oft_clients_edit" || $site == "oft_oft_clients_edit") { // BUGFIX/... kon niet vinden waar de extra oft_ vandaan komt...
        oft_clients_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_users") {
        head_oft($userid, $bedrijfsid, $site);
        oft_users($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_users_ajax") {
        oft_users_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_users_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_users_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_users_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_users_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == 'gen_oft_users_edit_AJAX') {
        oft_users_edit_AJAX($userid, $bedrijfsid);
    } elseif ($site == "oft_persons") {
        head_oft($userid, $bedrijfsid, $site);
        oft_persons($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_persons_ajax") {
        oft_persons_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_persons_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_persons_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_persons_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_persons_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_documents_ajax") {
        oft_entitie_documents_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_persons_view") {
        head_oft($userid, $bedrijfsid, $site);
        oft_persons_view($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_persons_appointments") {
        head_oft($userid, $bedrijfsid, $site);
        oft_persons_appointments($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_persons_appointments_ajax") {
        oft_persons_appointments_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_persons_contracts") {
        head_oft($userid, $bedrijfsid, $site);
        oft_persons_contracts($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_document") {
        head_oft($userid, $bedrijfsid, $site);
        oft_document($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_document_ajax") {
        oft_document_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_document_add") {
        oft_document_add($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_documents_add" || $site == "oft_entitie_finance_add") {
        oft_entitie_documents_add($userid, $bedrijfsid);
    } elseif ($site == "oft_document_edit" || $site == "oft_entitie_finance_edit") {
        oft_document_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_documents_edit") {
        oft_document_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_finance_audit_import") {
        //head_oft($userid, $bedrijfsid, $site); oft_finance_audit_import($userid, $bedrijfsid); footer1();
        head_oft($userid, $bedrijfsid, $site);
        oft_audit_import($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_finance_report_balans" || $site == "oft_finance_report") {
        head_oft($userid, $bedrijfsid, $site);
        oft_finance_report($userid, $bedrijfsid, "Balans");
        footer1();
    } elseif ($site == "oft_finance_report_penl") {
        head_oft($userid, $bedrijfsid, $site);
        oft_finance_report($userid, $bedrijfsid, "Kosten of opbrengst");
        footer1();
    } elseif ($site == "oft_finance") {
        head_oft($userid, $bedrijfsid, $site);
        oft_finance($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_finance_ajax") {
        oft_finance_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_finance") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_finance($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_finance_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_finance_ajax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_hr") {
        head_oft($userid, $bedrijfsid, $site);
        oft_hr($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_hr_ajax") {
        oft_hr_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_hr_documents_add" || $site == "oft_entitie_hr_add") {
        oft_hr_documents_add($userid, $bedrijfsid);
    } elseif ($site == "oft_hr_documents_edit" || $site == "oft_entitie_hr_edit") {
        oft_hr_documents_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_hr") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_hr($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_hr_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_hr_ajax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_search") {
        head_oft($userid, $bedrijfsid, $site);
        oft_search($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_search_ajax") {
        oft_search_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_grootboek_mutatie_kaart") {
        head_oft($userid, $bedrijfsid, $site);
        oft_grootboek_mutatie_kaart($userid, $bedrijfsid);
        footer1();
    }
    if ($site == 'clean-ajax') {
        header('Content-type: application/json');
        $status = 'failed';
        $type = '-';
        if (oft_opslaan($userid, $bedrijfsid)) {
            $status = "success";
            $type   = "save";
        }
        if (oft_delete($userid, $bedrijfsid)) {
            $status = "success";
            $type   = "delete";
        }
        echo json_encode(['status' => $status, 'type' => $type]);
        exit;
    }
//  elseif ($site == "oft_commercial") {
//      head_oft($userid, $bedrijfsid, $site); oft_commercial($userid, $bedrijfsid); footer1();
//  } elseif ($site == "oft_commercial_ajax") {
//      oft_commercial_ajax($userid, $bedrijfsid);
//  } elseif ($site == "oft_commercial_documents_add" || $site == "oft_commercial_add") {
//      oft_commercial_documents_add($userid, $bedrijfsid);
//  } elseif ($site == "oft_commercial_documents_edit" || $site == "oft_commercial_edit") {
//      oft_commercial_documents_edit($userid, $bedrijfsid);
//  } elseif ($site == "oft_operations") {
//      head_oft($userid, $bedrijfsid, $site); oft_operations($userid, $bedrijfsid); footer1();
//  } elseif ($site == "oft_operations_ajax") {
//      oft_operations_ajax($userid, $bedrijfsid);
//  } elseif ($site == "oft_operations_documents_add" || $site == "oft_operations_add") {
//      oft_operations_documents_add($userid, $bedrijfsid);
//  } elseif ($site == "oft_commercial_documents_edit" || $site == "oft_operations_edit") {
//      oft_operations_documents_edit($userid, $bedrijfsid);
//  }
    elseif ($site == "oft_entities") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entities_ajax") {
        oft_entities_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entities_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entities_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entities_add_2") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities_add_2($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entities_add_3") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities_add_3($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entities_add_4") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities_add_4($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entities_add_5") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities_add_5($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_end") {
        oft_entitie_end($userid, $bedrijfsid);
    } elseif ($site == "oft_overzichtadministraties") {
        head_oft($userid, $bedrijfsid, $site);
        oft_overzichtadministraties($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_organogram") {
        head_oft($userid, $bedrijfsid, $site);
        oft_organogram($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_overview") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_overview($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_overview_ajax") {
        oft_entitie_overview_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_corporate_data") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_corporate_data($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_corporate_data_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_corporate_data_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_adress") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_adress($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_adress_add") {
        oft_entitie_adress_add($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_adress_edit") {
        oft_entitie_adress_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_capital") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_capital($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_capital_ajax") {
        oft_entitie_capital_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_capital_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_capital_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entity_shareholders_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entity_shareholders_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_shareholders_edit") {
        oft_shareholders_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_shareholdings") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_shareholdings($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_shareholders") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_shareholders($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_shareholder_info") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_shareholder_info($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_shareholders_ajax") {
        oft_entitie_shareholders_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_transactions") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_transactions($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_all_transactions") {
        head_oft($userid, $bedrijfsid, $site);
        oft_all_transactions($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_participations") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_participations($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_transactions_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_transactions_ajax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_transactions_add") {
        oft_entitie_transactions_add($userid, $bedrijfsid);
    } elseif ($site == "oft_transactions_edit") {
        oft_transactions_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_management") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_management($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_management_ajax") {
        oft_entitie_management_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_management_add") {
        oft_entitie_management_add($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_management_add2") {
        oft_entitie_management_add2($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_management_edit") {
        oft_entitie_management_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_management_edit2") {
        oft_entitie_management_edit2($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_proxyholders") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_proxyholders($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_proxyholders_add") {
        oft_proxyholders_add($userid, $bedrijfsid);
    } elseif ($site == "oft_proxyholders_edit") {
        oft_proxyholders_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_compliance") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_compliance($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_compliance_ajax") {
        oft_entitie_compliance_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_compliance_change_settings") {
        oft_compliance_change_settings($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_documents") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_documents($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_resolutions") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_resolutions($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_resolutions_ajax") {
        oft_entitie_resolutions_ajax($userid, $bedrijfsid, "Nee");
    } elseif ($site == "oft_entitie_contracts") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_contracts($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_contracts_ajax") {
        oft_entitie_contracts_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_entities_ended") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entities_ended($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entities_ended_ajax") {
        oft_entities_ended_ajax($userid, $bedrijfsid);
    } elseif ($site == "extra_velden") {
        head_oft($userid, $bedrijfsid, $site);
        extra_velden($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_boardpack_edit" || $site == "oft_boardpack_edit") {
        oft_entitie_boardpack_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_boardpack_add") {
        oft_entitie_boardpack_add($userid, $bedrijfsid);
    } elseif ($site == "oft_entitie_boardpack_copy") {
        oft_entitie_boardpack_copy();
    } elseif ($site == "oft_entitie_boardpack") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_boardpack($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_entitie_boardpack_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_entitie_boardpack_ajax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_boardmeetings_edit") {
        oft_boardmeetings_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_tax_add") {
        oft_tax_add($userid, $bedrijfsid, $loginid);
    } elseif ($site == "oft_tax_edit") {
        oft_tax_edit($userid, $bedrijfsid, $loginid);
    } elseif ($site == "oft_annual_accounts_add") {
        oft_annual_accounts_add($userid, $bedrijfsid, $loginid);
    } elseif ($site == "oft_annual_accounts_edit") {
        oft_annual_accounts_edit($userid, $bedrijfsid, $loginid);
    } elseif ($site == "oft_registers_actiontracking" || $site == "oft_action_tracking") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_actiontracking_ajax" || $site == "oft_action_tracking_ajax") {
        oft_registers_actiontracking_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_actiontracking_print" || $site == "oft_action_tracking_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_actiontracking_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_actiontracking_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_action_tracking_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_action_tracking_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_actiontracking_audit" || $site == "oft_action_tracking_audit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking_audit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_actiontracking_audit_ajax" || $site == "oft_action_tracking_audit_ajax") {
        oft_registers_actiontracking_audit_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_actiontracking_audit_print" || $site == "oft_action_tracking_audit_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking_audit_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_actiontracking_audit_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking_audit_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_actiontracking_audit_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_actiontracking_audit_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_action_tracking_audit_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_action_tracking_audit_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_view") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_view($userid, $bedrijfsid);
        footer1();
    }elseif ($site == "oft_quality_management") {
        head_oft($userid, $bedrijfsid, $site);
        oft_quality_management($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_login") {
        head_oft($userid, $bedrijfsid, $site);
        oft_login($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_login_ajax") {
        oft_login_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_login_list") {
        head_oft($userid, $bedrijfsid, $site);
        oft_login_list($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_login_list_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_login_list_ajax($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_incident") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_incident($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_incident_ajax") {
        oft_registers_incident_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_incident_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_incident_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_incident_add") {
        oft_registers_incident_add($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_incident_edit") {
        oft_registers_incident_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_incident_register_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_incident_register_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_claim") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_claim($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_claim_ajax") {
        oft_registers_claim_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_claim_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_claim_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_claim_add") {
        oft_registers_claim_add($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_claim_edit") {
        oft_registers_claim_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_claim_register_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_claim_register_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_compliant" || $site == "oft_compliant") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_compliant($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_compliant_ajax") {
        oft_registers_compliant_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_compliant_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_compliant_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_compliant_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_compliant_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_compliant_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_compliant_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_complaint_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_complaint_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_exceptions" || $site == "oft_exceptions") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_exceptions($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_exceptions_ajax") {
        oft_registers_exceptions_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_exceptions_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_exceptions_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_exceptions_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_exceptions_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_exceptions_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_exceptions_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_exceptions_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_exceptions_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_debt_tracker" || $site == "oft_debt_tracker") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_debt_tracker($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_debt_tracker_detail" || $site == "oft_debt_tracker") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_debt_tracker_detail($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_debt_tracker_ajax") {
        oft_registers_debt_tracker_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_registers_debt_tracker_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_debt_tracker_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_debt_tracker_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_debt_tracker_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_registers_debt_tracker_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_registers_debt_tracker_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_debt_tracker_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_debt_tracker_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mastertables") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mastertables($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_notifications") {
        head_oft($userid, $bedrijfsid, $site);
        oft_notifications($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_stam_ajax") {
        oft_stam_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_stam_add") {
        oft_stam_add($userid, $bedrijfsid);
    } elseif ($site == "oft_stam_edit") {
        oft_stam_edit($userid, $bedrijfsid);
    } elseif ($site == "oft_trainingsschema") {
        head_oft($userid, $bedrijfsid, $site);
        oft_trainingsschema($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_trainingsschema_ajax") {
        oft_trainingsschema_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_trainingsschema_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_trainingsschema_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_trainingsschema_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_trainingsschema_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_trainingsschema_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_trainingsschema_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_trainingsschema_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_trainingsschema_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_compliance_chart") {
        head_oft($userid, $bedrijfsid, $site);
        oft_compliance_chart($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_compliance_chart_ajax") {
        oft_compliance_chart_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_compliance_chart_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_compliance_chart_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_compliance_chart_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_compliance_chart_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_compliance_chart_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_compliance_chart_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_compliance_chart_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_compliance_chart_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_ajax") {
        oft_mena_ajax($userid, $bedrijfsid);
    } elseif ($site == "oft_mena_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_add") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_add($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_edit") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_edit($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_1_print") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_1_print($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_add_evaluation") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_add_evaluation($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_edit_evaluation") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_edit_evaluation($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_evaluation" || $site == "oft_mena_evaluation_page_ajax") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_evaluation($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_evaluation_page") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_evaluation_page($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_lessonslearned_page") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_lessonslearned_page($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_mena_reports_page") {
        head_oft($userid, $bedrijfsid, $site);
        oft_mena_reports_page($userid, $bedrijfsid);
        footer1();
    } elseif ($site == "oft_dividend_distribution"
        || $site == "oft_wizzard_board_change"
        || $site == "oft_approval_transaction"
        || $site == "oft_wizzard_agm"
        || $site == "oft_proxyholder_change") {
        head_oft($userid, $bedrijfsid, $site);
        oft_wizzard($userid, $bedrijfsid, $site);
        footer1();
    } elseif($site == "get_responsible_by_role") {
        getResponsibleByRole();
    } elseif($site == "get_status_color_id") {
        getCompletedColorAjax();
    }
    elseif($site == 'checklist_get_startdate') {
        checklistGetStartdate();
    }
    elseif($site == 'checklist_get_deadline') {
        checklistGetDeadline();
    }
    elseif($site == 'forgot_password') {
        echo 'ok';
    }

    else {
        include_once 'route.php';
        route($site, $userid, $bedrijfsid);
    }
}
