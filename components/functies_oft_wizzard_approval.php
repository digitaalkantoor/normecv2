<?php
sercurityCheck();

function oft_approval_transaction_stap_checklist($userid, $bedrijfsid, $typeform) {
  global $db;

  if(isset($_REQUEST["WZ_OPSLAAN"])) {
    $fields["CHECKLIST"] = "";
    $fields["BESTAND"] = "file"; //TODO
    if(oft_save_wizzard_step($userid, $bedrijfsid, $typeform, '1', $fields)) {
      rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
    }
  }

  $selectJa = "";
  $selectNee = "checked=\"true\"";

  $downloadlink = ""; //TODO

  $return = "<form action=\"content.php?SITE=$typeform&STAP=1&BID=$bedrijfsid\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Checklist</h2><br/>";
  $return .= "<table width=\"100%\" class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"5\">";
  $return .= "<tr><td>First you need to finish the checklist</td></tr>";
  $return .= "<tr><td><a href=\"$downloadlink\">Download checklist</a></td></tr>";

  $return .= "<tr><td><br/><br/>Did you finish the checklist?</td><td>".selectRadioOptions("CHECKLIST", "Nee", getWizzardValue($typeform, $bedrijfsid, 'CHECKLIST'), array("Ja", "Nee", "Not applicable"))."</td></tr>";
  $return .= "<tr><td><br/>Upload the checklist</td></tr>";
  $return .= "<tr><td><input type=\"file\" name=\"BESTAND\" value=\"\" class=\"file\" /></td></tr>";
  $return .= "<tr><td>The checklist doesn't need to be signed in this stage.<br/>The checklist is saved in the Document section of this entity.</td></tr>";
  $return .= "</table>";
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Next\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}

function oft_approval_transaction_stap_article($userid, $bedrijfsid, $typeform) {
  global $db;

  if(isset($_REQUEST["WZ_OPSLAAN"])) {
    $fields["article"] = "";
    $fields["date resolution"] = "";
    if(oft_save_wizzard_step($userid, $bedrijfsid, $typeform, '2', $fields)) {
      //Create documents
      $fields["date resolution"] = oft_document_date_format(getWizzardValue($typeform, $bedrijfsid, 'DATEOFBOARDRESOLUTION'));
      $fields["article"] = getWizzardValue($typeform, $bedrijfsid, 'ARTICLE');
      $shareholder_doc = array("shareholders_resolution_header.txt", "shareholders_resolution_content_general.txt", "resolution_signature_page.txt");
      $board_doc = array("board_resolution_header.txt", "board_resolution_content_general.txt", "resolution_signature_page.txt");
      oft_save_wizzard_documents($userid, $bedrijfsid, $shareholder_doc, $typeform, "SAVEDOC1", $fields);
      oft_save_wizzard_documents($userid, $bedrijfsid, $board_doc, $typeform, "SAVEDOC2", $fields);
      rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=3");
    }
  } elseif(isset($_REQUEST["WZ_BACK"])) {
    rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
  }

  $return = "<form action=\"content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Transaction</h2><br/>";
  $return .= "<table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"5\">";
  $return .= "<tr><td>Date of board resolution</td><td><input type=\"text\" name=\"DATEOFBOARDRESOLUTION\" value=\"".getWizzardValue($typeform, $bedrijfsid, 'DATEOFBOARDRESOLUTION')."\" class=\"field3\" onClick=\"$(this).datepicker({dateFormat: 'yy-mm-dd'}).datepicker('show');\" /></td></tr>";
  $return .= "<tr><td colspan=\"2\">Articles and paragraph regarding distribution [interim] dividend</td></tr>
              <tr><td colspan=\"2\"><input type=\"text\" name=\"ARTICLE\" value=\"".getWizzardValue($typeform, $bedrijfsid, 'ARTICLE')."\" class=\"field\" /></td></tr>";
  $return .= "</table>";
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_BACK\" value=\"Back\" class=\"button\" /><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Next\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}

function oft_approval_transaction_stap_documents($userid, $bedrijfsid, $typeform) {
  global $pdo;

  $YEAR = date("Y");
  $SAVEDOC1 = "Shareholders Resolution (transaction) $YEAR.docx";
  $SAVEDOC2 = "Managing Board Resolution (transaction) $YEAR.docx";

  if(isset($_REQUEST["WZ_OPSLAAN"])) {
    //Copy content from wizzard_doc to documents
    if(oft_copy_wizzard_documents($userid, $bedrijfsid, $typeform, 'SAVEDOC1', $SAVEDOC1)) {
      if(oft_copy_wizzard_documents($userid, $bedrijfsid, $typeform, 'SAVEDOC2', $SAVEDOC2)) {
       //set wizzard row 'final';
        $query = $pdo->prepare('update oft_wizzard
                     set STATUSFORM = "final"
                   WHERE BEDRIJFSID = :bedrijfsid
                     AND TYPEFORM = :typeform
                     AND NOT STATUSFORM = "final";');
        $query->bindValue('bedrijfsid', $bedrijfsid);
        $query->bindValue('typeform', $typeform);
        $query->execute();
      }
    }

    //Redirect to entity documents
    rd("content.php?SITE=oft_entitie_documents&BID=$bedrijfsid");
  } elseif(isset($_REQUEST["WZ_BACK"])) {
    rd("content.php?SITE=$typeform&BID=$bedrijfsid&STAP=2");
  }

  $return = "<form action=\"content.php?SITE=$typeform&BID=$bedrijfsid&STAP=3\" method=\"Post\" enctype=\"multipart/form-data\">";
  $return .= "<h2>Document</h2><br/>";
  $return .= "<table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"5\">";
  $return .= "<tr><td rowspan=\"2\"><img src=\"./images/docx.png\" /></td>
                  <td><a target=\"_blank\" href=\"document.php?WIZZARD=".getWizzardValue($typeform, $bedrijfsid, 'ID')."&KOLOM=SAVEDOC1&NAME=".str_replace(" ", "_", $SAVEDOC1)."\">$SAVEDOC1</a></td></tr>
              <tr><td><input type=\"checkbox\" name=\"SAVEDOC1\" checked=\"true\" value=\"Ja\" /> Save this document in the documents section of this entity</td></tr>";

  $return .= "<tr><td><br/><br/></td></tr>
              <tr><td rowspan=\"2\"><img src=\"./images/docx.png\" /></td>
                  <td><a target=\"_blank\" href=\"document.php?WIZZARD=".getWizzardValue($typeform, $bedrijfsid, 'ID')."&KOLOM=SAVEDOC2&NAME=".str_replace(" ", "_", $SAVEDOC2)."\">$SAVEDOC2</a></td></tr>
              <tr><td><input type=\"checkbox\" name=\"SAVEDOC2\" checked=\"true\" value=\"Ja\" /> Save this document in the documents section of this entity</td></tr>";

  $return .= "<tr><td colspan=\"2\"><br/><br/>The signed documents have to be uploaded in the<br/>Resolution section of this entity.</td></tr>";
  $return .= "</table>";
  $return .= "<div align=\"right\" class=\"opslagbuttons\"><input type=\"submit\" name=\"WZ_BACK\" value=\"Back\" class=\"button\" /><input type=\"submit\" name=\"WZ_OPSLAAN\" value=\"Finish\" class=\"button\" /><input type=\"submit\" name=\"WZ_ANNULEREN\" value=\"Cancel\" class=\"cancelbutton\" /></div>";
  $return .= "</form>";

  return $return;
}
