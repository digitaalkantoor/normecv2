<?php
sercurityCheck();

function oft_entitie_corporate_data($userid, $bedrijfsid) {
  global $pdo;

  $MENUID        = getMenuItem();
  $titel         = oft_titel_entities($bedrijfsid);
  $submenuitems    = "";
  if(isRechtenOFT($bedrijfsid, "CRM")) {
    if(check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
      $submenuitems   = oft_inputform_link('content.php?SITE=oft_document_add', "Upload documents");
      $submenuitems  .= ' | '. "<a href=\"content.php?SITE=oft_entitie_corporate_data_edit&MENUID=$MENUID&ID=$bedrijfsid&BID=$bedrijfsid\">Edit corporate data</a>";
    }
  }
  $back_button   = oft_back_button_entity();

  $contentFrame = "<h1>Corporate data</h1>";

  $query = $pdo->prepare('select * from bedrijf where ID = :bedrijfsid limit 1;');
  $query->bindValue('bedrijfsid', $bedrijfsid);
  $query->execute();
  $dBedrijf = $query->fetch(PDO::FETCH_ASSOC);

  if($dBedrijf !== false)
  {  
    $contentFrame .= "<table class=\"oft_content_tabel oft_tabel\" width=\"700\" cellspacing=\"0\" cellpadding=\"0\">";
    $contentFrame .= "<tr><td class=\"oft_label\" valign=\"top\" align=\"left\">Number, Name</td><td valign=\"top\" align=\"left\" colspan=\"3\">".lb($dBedrijf["BEDRIJFNR"]).", ".lb($dBedrijf["BEDRIJFSNAAM"])."</td></tr>";

    $contentFrame .= "<tr><td class=\"oft_label\" valign=\"top\" align=\"left\">Purpose</td><td valign=\"top\" align=\"left\" colspan=\"3\">".lb($dBedrijf["OMSCHRIJVING"])."</td></tr>";

    $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Country</td>
                         <td valign=\"top\" align=\"left\">".lb($dBedrijf["LAND"])."</td>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Commercial name</td>
                         <td valign=\"top\" align=\"left\">".lb($dBedrijf["HANDELSNAAM"])."</td>
                     </tr>";
    $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Legal form</td>
                         <td valign=\"top\" align=\"left\">".getNaam("stam_rechtsvormen", $dBedrijf["RECHTSVORM"], "NAAM")."</td>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Incorporation date</td>
                         <td valign=\"top\" align=\"left\">".datumconversiePresentatieKort($dBedrijf["OPRICHTINGSDATUM"])."</td>
                     </tr>";
    $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Registered number</td>
                         <td valign=\"top\" align=\"left\">".lb($dBedrijf["REGISTEREDNUMBER"])."</td>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">&nbsp;</td>
                         <td valign=\"top\" align=\"left\">&nbsp;</td>
                     </tr>";
    $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">".check("default_name_of_entity", "Entity", 1)." has capital</td>
                         <td valign=\"top\" align=\"left\">".replacejanee($dBedrijf["HAVECAPITAL"])."</td>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">".check("default_name_of_entity", "Entity", 1)." is part of group</td>
                         <td valign=\"top\" align=\"left\">".replacejanee($dBedrijf["INTERN"])."</td>
                     </tr>";

    $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">End of financial year</td>
                         <td valign=\"top\" align=\"left\">".str_replace("01", "31", str_replace(substr($dBedrijf["ENDOFBOOKYEAR"],0,4), "", datumconversiePresentatieKort($dBedrijf["ENDOFBOOKYEAR"])))."</td>";

    if(check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
      $contentFrame .= "<td class=\"oft_label\" valign=\"top\" align=\"left\">Is a natural person</td>
                        <td valign=\"top\" align=\"left\">".replacejanee($dBedrijf["ENTITYNATURAL"])."</td>";
    }
    $contentFrame .= "<tr>";

    $contentFrame .= "<tr>
                         <td class=\"oft_label\" valign=\"top\" align=\"left\">Monitor compliance</td>
                         <td valign=\"top\" align=\"left\">".replacejanee($dBedrijf["MONITOR"])."</td>
                     </tr>";
    if(check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
      $contentFrame .= "<tr>
                           <td class=\"oft_label\" valign=\"top\" align=\"left\">Board meetings (per year)</td>
                           <td valign=\"top\" align=\"left\">".tl($dBedrijf["BOARDMEETINGS"])."</td>
                       </tr>";
    }
    if(check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
      $contentFrame .= "<tr>
                           <td class=\"oft_label\" valign=\"top\" align=\"left\">Composition board (min. requirement)</td>
                           <td valign=\"top\" align=\"left\" colspan=\"2\">".tl($dBedrijf["COMPOSITIONBOARD"])." % of members reside <br/>in country of residence <br/>of the company</td>
                       </tr>";
    }

    $additional    = "";
    for($i = 1; $i <= 5; $i++) {
      if($dBedrijf["EXTRAVELD$i"] != "") {
         $additional .= "<tr>
                            <td class=\"oft_label\" valign=\"top\" align=\"left\">".getNaam("extra_velden", $i, "VELDOMSCHRIJVING")."</td>
                            <td valign=\"top\" align=\"left\" colspan=\"3\">".lb($dBedrijf["EXTRAVELD$i"])."</td>
                        </tr>";
      }
    }
    if($additional != "") {
       $additional = "<tr><td><br/><h3>Additional fields</h3></td></tr>" . $additional;
    }
    
    $contentFrame .= $additional;

    $contentFrame .= "</table><br/>";
  }

  if(check('DIF', 'Nee', $bedrijfsid) != 'Ja') {
    $contentFrame   .= oft_getDocuments($userid, $bedrijfsid, "Articles of association", array("Articles of association"), "", "", "") . "<br/>";

    //Trade register
    $traderegister   = "";
    $pos             = 0;
    $queryTrade = $pdo->prepare('select * from oft_trade_register where (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid ORDER BY REGISTRATIONDATE;');
    $queryTrade->bindValue('bedrijfsid', $bedrijfsid);
    $queryTrade->execute();
  
    foreach ($queryTrade->fetchAll() as $dtradere) {  
      if($pos == 0) {
        $pos = 1;
        $traderegister .= "<table class=\"oft_tabel\">";
      }
      $traderegister .= "<tr><td class=\"oft_label\">".lb($dtradere["NUMBER"])."</td><td>".datumconversiePresentatieKort($dtradere["REGISTRATIONDATE"])."</td></tr>";
    }
    if($pos == 1) {
      $traderegister .= "</table>";
    } else {
      //$traderegister .= "<em>Nothing found</em>";
    }
    $contentFrame  .= "<h3>Trade register</h3>". $traderegister;
  
    $contentFrame  .= oft_getDocuments($userid, $bedrijfsid, "", array("Extract trade register"), "", "", "") . "<br/><br/>";
  }

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_corporate_data_edit($userid, $bedrijfsid) {
  global $db;

  $MENUID        = getMenuItem();

  $titel         = oft_titel_entities($bedrijfsid);
  $submenuitems  = "";
  $back_button   = oft_back_button_entity();

  $templateFile  = "./templates/oft_corporate_data.htm";
  $tabel         = "bedrijf";

  $id             = "";
  if(isset($_REQUEST["ID"])) {
    $id          = $_REQUEST["ID"];
  }

  $contentFrame = "<h2>Corporate data</h2>";

  $contentFrame .= replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id);

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}
