<?php
sercurityCheck();

function oft_entitie_boardpack($userid, $bedrijfsid) {
  global $db;

  $MENUID          = getMenuItem();

  $titel           = oft_titel_entities($bedrijfsid);
  $submenuitems    = "";
  if(isRechtenOFT($bedrijfsid, "CRM")) {
    $submenuitems   = oft_inputform_link('content.php?SITE=oft_entitie_boardpack_add', "New boardpack", 870);
    $submenuitems  .= ' | '. "<a href=\"content.php?SITE=oft_entitie_boardpack_copy&MENUID=$MENUID&BID=$bedrijfsid\">Copy last boardpack</a>";
  }
  $back_button     = oft_back_button_entity();

  $id              = "";
  if(isset($_REQUEST["ID"])) {
    $id            = $_REQUEST["ID"];
  }

  $contentFrame    = "<h1>Boardpack</h1>";

  $tabel           = "oft_boardpack";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_boardpack_edit&ID=-ID-", 870);
  $verwijderen     = "";

  $contentFrame   .= oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'DATUM', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");

  echo oft_framework_menu($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems, $back_button);
}

function oft_entitie_boardpack_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "oft_boardpack";
  $toevoegen       = "";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_boardpack_edit&ID=-ID-", 870);
  $verwijderen     = "";

  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'DATUM', $toevoegen, $bewerken, $verwijderen, "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."'");
}

function oft_entitie_boardpack_add($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_entitie_boardpack.htm";
  $tabel        = "oft_boardpack";
  $titel        = "Boardpack";

  oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_boardpack_edit($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_entitie_boardpack.htm";
  $tabel        = "oft_boardpack";
  $titel        = "Boardpack";

  oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_send_boardpack($userid, $bedrijfsid, $opslaan_ID) {
  global $pdo;

  $type = getNaam("oft_boardpack", $opslaan_ID, "TYPE");
  $bedrijfsnaam = getNaam("bedrijf", $bedrijfsid, "BEDRIJFSNAAM");

  $datum = substr(getNaam("oft_boardpack", $opslaan_ID, "DATUM"),0,10);
  $mailcontent = "<h1>Boardpack $bedrijfsnaam $datum</h1>
  There is a new boardpack available for the $type management board. Look into the Orangefield App to read the attached documents.
  <br/>
  <br/>
  <h2>Agenda</h2>
  <ul>";

  
  $queryBoardPackItems = $pdo->prepare('SELECT * FROM oft_boardpack_items WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BPID = :bpid ORDER BY ID;');
  $queryBoardPackItems->bindValue('bpid', $opslaan_ID);
  $queryBoardPackItems->execute();

  foreach ($queryBoardPackItems->fetchAll() as $dBoardPackItems) {
    $mailcontent .= "<li>".lb($dBoardPackItems["ITEM"])."</li>";
  }
  
  $mailcontent .= "</ul>";
  
  $registerSenDate = $pdo->prepare('UPDATE oft_boardpack SET DATUMSEND = NOW() WHERE ID = :id;');
  $registerSenDate->bindValue('id', $opslaan_ID);
  $registerSenDate->execute();
 
  $queryRInvitees = $pdo->prepare('SELECT * FROM oft_boardpack_invitees WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BPID = :bpid ORDER BY ID;');
  $queryRInvitees->bindValue('bpid', $opslaan_ID);
  $queryRInvitees->execute();

  foreach ($queryRInvitees->fetchAll() as $dInvitees) {
    $email = getNaam("personeel", $dInvitees["USERID"], "EMAIL");
    if(isset($_REQUEST["BP_INVITEE_CHECK_".($dInvitees["ID"]*1)]) && $email != '') {
       //Send boardpack
       mail_versturen('no-reply@orangefieldonline.org', $email, '', '', "Boardpack $datum", $mailcontent, '');
    }
  }
}

/**
 * COPY LAST BOARDPACK
 */
function oft_entitie_boardpack_copy() {
  global $pdo;
  $BID = $_REQUEST['BID'];

  $query = $pdo->prepare('SELECT ID FROM oft_boardpack WHERE BEDRIJFSID = :bedrijfsid AND DELETED != "Ja" ORDER BY ID DESC LIMIT 1');
  $query->bindValue('bedrijfsid', $BID);
  $query->execute();
  $last_result = $query->fetch(PDO::FETCH_ASSOC);

  if(!$last_result) {
    return "error - not found: ".$BID;
  }
  
  $last_boardpack_id = $last_result['ID'];

  // copy last boardpack form this company
  $copy_boardpack = $pdo->prepare('INSERT INTO oft_boardpack
      (`BEDRIJFSID`, `UPDATEDATUM`, `DELETED`, `FINANCIALYEAR`, `DATUM`, `TYPE`, `TIME`)
      SELECT `BEDRIJFSID`, NOW(), `DELETED`, `FINANCIALYEAR`, `DATUM`, `TYPE`, `TIME`
      FROM oft_boardpack
      WHERE `ID`  = :id');
  $copy_boardpack->bindValue('id', $last_boardpack_id);

  if ($copy_boardpack->execute()) {
    $new_boardpack_id = $pdo->lastInsertId();
  } else {
    return "error - not copied";
  }

  // copy invities
  $copy_invitees = $pdo->prepare('INSERT INTO oft_boardpack_invitees
      (`BEDRIJFSID`, `UPDATEDATUM`, `DELETED`, `BPID`, `USERID`)
      SELECT `BEDRIJFSID`, NOW(), `DELETED`, :bpidnew, `USERID`
      FROM oft_boardpack_invitees
      WHERE `BPID`  = :bpid');
  $copy_invitees->bindValue('bpidnew', $new_boardpack_id);
  $copy_invitees->bindValue('bpid', $last_boardpack_id);
  $copy_invitees->execute();
  
  // copy items
  $copy_items = $pdo->prepare('INSERT INTO oft_boardpack_items
      (`BEDRIJFSID`, `USERID`, `DELETED`, `BPID`, `ITEM`, `FILE1`, `FILE2`, `FILE3`)
      SELECT `BEDRIJFSID`, `USERID`, `DELETED`, :bpidnew, `ITEM`, `FILE1`, `FILE2`, `FILE3`
      FROM oft_boardpack_items
      WHERE `BPID`  = :bpid');
  $copy_items->bindValue('bpidnew', $new_boardpack_id);
  $copy_items->bindValue('bpid', $last_boardpack_id);
  $copy_items->execute();
  
  // redirect to boardpack
  rd("/content.php?SITE=oft_entitie_boardpack&BID=142&MENUID=30209");
}
