<?php
sercurityCheck();

function oft_delete($userid, $bedrijfsid)
{
    global $pdo;
    if (isset($_REQUEST["submit"]) && $_REQUEST['submit'] == 'delete' || isset($_REQUEST["delete"]) && $_REQUEST['delete'] == 'delete') {
        $url_querystring = url_querystring();
        $opslaan_ID = ps($_REQUEST["opslaan_ID"]);
        $opslaan_tabel = ps($_REQUEST["opslaan_tabel"]);
        $opslaan_tabel_array = explode(",", $opslaan_tabel);

        if (isset($opslaan_tabel_array[0])) {
            $tabel = $opslaan_tabel_array[0];
            if (!empty($tabel)) {
                $tableData = getTableData($tabel);
                if (empty($tableData)) {
                    return false;
                }
            }

            $query = $pdo->prepare('update `' . $tabel . '` set UPDATEDATUM = NOW(), DELETED = "Ja" WHERE ID = :id;');
            $query->bindValue('id', $opslaan_ID);
            if ($query->execute()) {
                auditaction($userid, $bedrijfsid, $tabel, $opslaan_ID, 'Delete');

                $query = $pdo->prepare('update notificaties SET DELETED = "Ja", STATUS = "Passive" where TABEL = :tabel AND TABELID = :tabelid limit 10;');
                $query->bindValue('tabel', $opslaan_tabel_array[0]);
                $query->bindValue('tabelid', $opslaan_ID);
                $query->execute();

                //Delete entity, also delete notifications, documents, etc
               rd($url_querystring);
                return true;
            }
        }
    }
    return false;
}

/*
 * Function to handle form submissions and redirect the user.
 */
function oft_opslaan($userid, $bedrijfsid)
{
    global $pdo;

//    echo "<pre>";var_dump($_FILES['oft_annual_accounts_SKIP_ADD_FILES']);echo "</pre>";die();

    if ((isset($_REQUEST['submit']) && ($_REQUEST['submit'] == 'save' || $_REQUEST['submit'] == 'next' || $_REQUEST['submit'] == 'send')) || isset($_REQUEST["OPSLAAN"]) || isset($_REQUEST["NEXT"]) || isset($_REQUEST["SEND"])) {
//        //TODO: trackingid werkt niet goed!!
//        if (isset($_REQUEST["opslaan_TRACKINGID"]) && $_REQUEST["opslaan_TRACKINGID"] == getTrackingId()) {
//            //Is oke en mag worden opgeslagen
//        } else {
//            //return false;
//        }

        $inserted_BID = 0;
        $BATCH = array();
        $opslaan_ID = ps($_REQUEST["opslaan_ID"]);

        $opslaan_tabel = ps($_REQUEST["opslaan_tabel"]);
        $opslaan_tabel_array = explode(",", $opslaan_tabel);

        $url_querystring = url_querystring();
        $inserted_ids = array();
        /*
        * When $_REQUEST['BID'] is missing, files of notifications from home are
        * stored with the wrong BEDRIJFSID.
        */
        if (($opslaan_tabel == 'oft_tax' || $opslaan_tabel == 'oft_annual_accounts') && !empty($opslaan_ID)) {
            $getCorrectIdQyery = $pdo->prepare("SELECT BEDRIJFSID FROM $opslaan_tabel WHERE ID = :id LIMIT 1");
            $getCorrectIdQyery->bindValue('id', $opslaan_ID);
            if ($getCorrectIdQyery->execute()) {
                $bedrijfsid = $getCorrectIdQyery->fetchColumn();
            } else {
                return false;
            }
        }

        $originalData = array();
        $tableData = getTableData($opslaan_tabel);
        if ($opslaan_ID > 0 && $opslaan_tabel != '' && count($opslaan_tabel_array) < 2 && !empty($tableData)) {
            $query = $pdo->prepare('select * from ' . $opslaan_tabel . ' WHERE ID = :id;');
            $query->bindValue('id', $opslaan_ID);
            $query->execute();
            $originalData = $query->fetch(PDO::FETCH_ASSOC);
        }
        //insert both tables
        foreach ($opslaan_tabel_array as $tabel) {
            $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
// $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
            $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");

            if (count($arrayVelden)) {
                $SQL_als_tweede_uitvoeren = "";

                if ($opslaan_ID != "-ID-" && $opslaan_ID != "0" && $opslaan_ID != "") {
                    //Hisotry opstlaan
                    if (isset($_FILES[$tabel . "_SKIP_FILE_HISTORY"]['name'])) {
                        $query = $pdo->prepare('SELECT * FROM documentbeheer WHERE ID = :id limit 1;');
                        $query->bindValue('id', $opslaan_ID);
                        $query->execute();
                        $data = $query->fetch(PDO::FETCH_ASSOC);
                        if ($data !== false) {
                            //TBV versie beheer
                            $query = $pdo->prepare('insert into documentbeheer_archief set AID = :aid,
 UPDATEDATUM = :updatedatum,
 DELETED = :deleted,
 NAAM = :naam,
 OMSCHRIJVING = :omschrijving,
 CATAGORIEID = :categorieid,
 RECHTEN = :rechten,
 PERSONEELSLID = :personeelslid,
 BESTAND = :bestand,
 BESTANDSNAAM = :bestandsnaam,
 BESTANDTYPE = :bestandtype,
 BESTANDSIZE = :bestandsize,
 BESTANDCONTENT = :bestandcontent,
 TAGS = :tags,
 DATUM = :datum,
 STOPDATUM = :stopdatum,
 VERSIENR = :versienr,
 BEKEKEN = :bekeken,
 BEDRIJFSID = :bedrijfsid,
 ORDNER = :ordner,
 DOCTYPE = :doctype,
 TABEL = :tabel,
 TABELID = :tabelid,
 JAAR = :jaar;');

                            $query->bindValue('aid', $data["ID"]);
                            $query->bindValue('updatedatum', $data["UPDATEDATUM"]);
                            $query->bindValue('deleted', $data["DELETED"]);
                            $query->bindValue('naam', $data["NAAM"]);
                            $query->bindValue('omschrijving', $data["OMSCHRIJVING"]);
                            $query->bindValue('categorieid', $data["CATAGORIEID"]);
                            $query->bindValue('rechten', $data["RECHTEN"]);
                            $query->bindValue('personeelslid', $data["PERSONEELSLID"]);
                            $query->bindValue('bestand', $data["BESTAND"]);
                            $query->bindValue('bestandsnaam', $data["BESTANDSNAAM"]);
                            $query->bindValue('bestandtype', $data["BESTANDTYPE"]);
                            $query->bindValue('bestandsize', $data["BESTANDSIZE"]);
                            $query->bindValue('bestandcontent', $data["BESTANDCONTENT"]);
                            $query->bindValue('tags', $data["TAGS"]);
                            $query->bindValue('datum', $data["DATUM"]);
                            $query->bindValue('stopdatum', $data["STOPDATUM"]);
                            $query->bindValue('versienr', $data["VERSIENR"]);
                            $query->bindValue('bekeken', $data["BEKEKEN"]);
                            $query->bindValue('bedrijfsid', $data["BEDRIJFSID"]);
                            $query->bindValue('ordner', $data["ORDNER"]);
                            $query->bindValue('doctype', $data["DOCTYPE"]);
                            $query->bindValue('tabel', $data["TABEL"]);
                            $query->bindValue('tabelid', $data["TABELID"]);
                            $query->bindValue('jaar', $data["JAAR"]);

                            //($FILES, $bedrijfsid, $userid, $DOCTYPE, $TABEL, $TABELID, $DID)
                            if ($files = $_FILES[$tabel . "_SKIP_FILE_HISTORY"]) {
                                uploadBestandInDB_2($files, $bedrijfsid, $userid, $data["DOCTYPE"], "documentbeheer",
                                    $opslaan_ID, $opslaan_ID, $data["JAAR"]);

                            }
                            $query->execute();
                        }
                    }
                    //var_dump($_REQUEST);

                    $SQL = "update `" . ps($tabel) . "` set UPDATEDATUM = NOW(), ";
                } else {
                    $bedrijfsidOpslaan = "";
                    if (!isset($_REQUEST[$tabel . "_BEDRIJFSID"])) {
                        $bedrijfsidOpslaan = "BEDRIJFSID = '" . ps($bedrijfsid, "nr") . "',";
                    }

                    $SQL = "insert into `" . ps($tabel) . "` set $bedrijfsidOpslaan UPDATEDATUM = NOW(), DELETED = 'Nee', ";
                }
                // bestand(en) verwijderen
                if (isset($_REQUEST["delete_doc"])) {
                    foreach ($_REQUEST["delete_doc"] as $delete) {
                        if ($delete) {
                            auditaction($userid, $bedrijfsid, 'documentbeheer', $delete, 'Delete');

                            $query = $pdo->prepare('delete from documentbeheer where ID = :id');
                            $query->bindValue('id', $delete);
                            $query->execute();
                        }
                    }
                }

                //doorloop fields
                foreach ($arrayVelden as $key => $value) {
                    if (isset($originalData[$value])) {
                        $BATCH[$value]["OLD"] = $originalData[$value];
                    }

                    if ($value != "" && substr($value, 0, 5) != "SKIP_") {
                        $naam_veld = $tabel . "_" . $value;

                        //echo "<Br/>$naam_veld: ".$arrayVeldType[$key];
                        if (isset($_REQUEST[$naam_veld]) || $arrayVeldType[$key] == "SIGNATORY" || $arrayVeldType[$key] == "rechtentot" || $arrayVeldType[$key] == "newperson") {
                            //echo "<br/>Gevonden: ".$naam_veld;
                            if ($arrayVeldType[$key] == "datum" || $arrayVeldType[$key] == "datumleeg") {
                                if (trim($_REQUEST[$naam_veld]) == "") {
                                    $SQL .= "$value = null, ";
                                    $BATCH[$value]["NEW"] = null;
                                } else {
                                    $SQL .= "$value = '" . ps($_REQUEST[$naam_veld], "date") . "', ";
                                }
                                $BATCH[$value]["NEW"] = ps($_REQUEST[$naam_veld], "date");
                            } elseif ($arrayVeldType[$key] == "updatedatum") {
                                $SQL .= "$value = NOW(), ";
                                $BATCH[$value]["NEW"] = date("Y-m-d H:i:s");
                            } elseif ($arrayVeldType[$key] == "bedrag") {
                                $SQL .= "$value = '" . ps($_REQUEST[$naam_veld], "amount") . "', ";
                                $BATCH[$value]["NEW"] = ps($_REQUEST[$naam_veld], "amount");
                            } elseif ($arrayVeldType[$key] == "selectNee") {
                                $SQL .= "$value = 'Nee', ";
                                $BATCH[$value]["NEW"] = 'Nee';
                            } elseif ($arrayVeldType[$key] == "selectJa") {
                                $SQL .= "$value = 'Ja', ";
                                $BATCH[$value]["NEW"] = 'Ja';
                            } elseif ($arrayVeldType[$key] == "rechtentot") {
                                if (isset($_REQUEST["login_PERSONEELSLID"]) && $_REQUEST["login_PERSONEELSLID"] > 0) {
                                    oft_insert_entiteit_rechten($userid, $bedrijfsid, $_REQUEST["login_PERSONEELSLID"]);
                                }
                            } elseif ($arrayVeldType[$key] == "SIGNATORY") {
                                if ($opslaan_ID > 0) {
                                    auditaction($userid, $bedrijfsid, 'contractpartijen', $opslaan_ID, 'Delete');
                                    $query = $pdo->prepare('delete from contractpartijen where CID = :cid limit 5;'); // waarom de limit 5?
                                    $query->bindValue('cid', $opslaan_ID);
                                    $query->execute();
                                }
                                for ($i = 1; $i <= 5; $i++) {
                                    if (isset($_REQUEST["PH$i"]) && $_REQUEST["PH$i"] > 0) {
                                        $query = $pdo->prepare('insert into contractpartijen set CID = :cid,
 DATUM = NOW(),
 USERID = :userid,
 TYPE = :type,
 BEDRIJFSID = :bedrijfsid;');
                                        $query->bindValue('cid', $opslaan_ID);
                                        $query->bindValue('userid', $_REQUEST["PH$i"]);
                                        $query->bindValue('type', $_REQUEST["PH_CAPACITY$i"]);
                                        $query->bindValue('bedrijfsid', $bedrijfsid);
                                        $query->execute();
                                    }
                                }
                            } elseif ($arrayVeldType[$key] == "newuser") {
                                $uid = 0;
                                if (isset($_REQUEST[$tabel . "_NEWUSER"]) && $_REQUEST[$tabel . "_NEWUSER"] == "Ja") {
                                    //Maak nieuwe user
                                    $sql_newuser = "";
                                    $query = $pdo->prepare('insert into personeel set INITIALEN = :initialen,
 TUSSENVOEGSEL = :tussenvoegsel,
 ACHTERNAAM = :achternaam,
 MOB = :mob,
 LAND = :land,
 EMAIL = :email,
 UPDATEDATUM = NOW(),
 DELETED = "Nee",
 BEDRIJFSID = :bedrijfsid;');
                                    $query->bindValue('initialen', $_REQUEST[$tabel . "_N_INITIALEN"]);
                                    $query->bindValue('tussenvoegsel', $_REQUEST[$tabel . "_N_TUSSENVOEGSEL"]);
                                    $query->bindValue('achternaam', $_REQUEST[$tabel . "_N_ACHTERNAAM"]);
                                    $query->bindValue('mob', $_REQUEST[$tabel . "_N_MOB"]);
                                    $query->bindValue('land', $_REQUEST[$tabel . "_N_LAND"]);
                                    $query->bindValue('email', $_REQUEST[$tabel . "_N_EMAIL"]);
                                    $query->bindValue('bedrijfsid', $_REQUEST[$tabel . "_BEDRIJFSID"]);

                                    if ($query->execute()) {
                                        $uid = getLastId('personeel');
                                    }
                                } elseif (isset($_REQUEST[$tabel . "_N_PERSONEELSLID"])) {
                                    $uid = ps($_REQUEST[$tabel . "_N_PERSONEELSLID"], "nr");
                                }
                                if ($uid != 0) {
                                    $SQL .= "PERSONEELSLID = '$uid', PASSWORD = 'welcome01', SUSPEND = 'Nee', STYLE = 'OFT', MASTER = 'Ja', ";
                                }
                            } elseif ($arrayVeldType[$key] == "newentity") {
                                $newBid = ps($_REQUEST[$naam_veld], "nr");
                                if (isset($_REQUEST[$naam_veld . "_OTHERENTITY"]) && trim($_REQUEST[$naam_veld . "_OTHERENTITY"]) != "") {
                                    $otherBid = create_new_entity($_REQUEST[$naam_veld . "_OTHERENTITY"], $userid);
                                    if ($otherBid > 0) {
                                        $newBid = $otherBid;
                                    }
                                }
                                $SQL .= "$value = '" . ps($newBid, "nr") . "', ";
                                $BATCH[$value]["NEW"] = ps($newBid, "nr");
                            } elseif ($arrayVeldType[$key] == "newperson") {
                                $uid = 0;
                                if (isset($_REQUEST[$tabel . "_SKIP_MEMBERTYPE"]) && $_REQUEST[$tabel . "_SKIP_MEMBERTYPE"] == "new person" && $_REQUEST[$tabel . "_N_ACHTERNAAM"] != "" && $_REQUEST[$tabel . "_N_ACHTERNAAM"] != "Last name") {
                                    //Maak nieuwe user
                                    $sql_newuser = "";
                                    $query = $pdo->prepare('insert into personeel set INITIALEN = :initialen,
 TUSSENVOEGSEL = :tussenvoegsel,
 ACHTERNAAM = :achternaam,
 MOB = :mob,
 LAND = :land,
 EMAIL = :email,
 UPDATEDATUM = NOW(),
 DELETED = "Nee",
 BEDRIJFSID = :bedrijfsid;');

                                    $query->bindValue('initialen', $_REQUEST[$tabel . "_N_INITIALEN"]);
                                    $query->bindValue('tussenvoegsel', $_REQUEST[$tabel . "_N_TUSSENVOEGSEL"]);
                                    $query->bindValue('achternaam', $_REQUEST[$tabel . "_N_ACHTERNAAM"]);
                                    $query->bindValue('mob', $_REQUEST[$tabel . "_N_MOB"]);
                                    $query->bindValue('land', $_REQUEST[$tabel . "_N_LAND"]);
                                    $query->bindValue('email', $_REQUEST[$tabel . "_N_EMAIL"]);
                                    $query->bindValue('bedrijfsid', $_REQUEST[$tabel . "_BEDRIJFSID"]);

                                    if ($query->execute()) {
                                        $uid = getLastId('personeel');
                                    }
                                }
                                if ($uid != 0) {
                                    $SQL_als_tweede_uitvoeren = "update $tabel set USERID = '" . ps($uid,
                                            "nr") . "' WHERE ID = 'XIDX'";
                                }
                            } elseif ($arrayVeldType[$key] == "bedrijf" || ($arrayVeldType[$key] == "personeel" || $arrayVeldType[$key] == "login")) {
                                $SQL .= "$value = '" . ps($_REQUEST[$naam_veld], "nr") . "', ";
                                $BATCH[$value]["NEW"] = ps($_REQUEST[$naam_veld], "nr");
                            } else {
                                $field = $_REQUEST[$naam_veld];
                                if ($value == 'PERSONEELSLID') {
                                    $field = intval($_REQUEST[$naam_veld]);
                                    if ($field <= 0) {
                                        $field = $_REQUEST['PID'];
                                    }
                                }
                                $SQL .= "$value = '" . ps($field) . "', ";
                                $BATCH[$value]["NEW"] = ps($field);
                            }
                        }
                    }
                }
                /// Bad code! - Martijn S.
                $SQL = substr($SQL, 0, strlen($SQL) - 2);

                if ($opslaan_ID != "-ID-" && $opslaan_ID != "0" && $opslaan_ID != "") {
                    $SQL .= " WHERE ID = " . ps($opslaan_ID) . " limit 1";
                    audittrailBatch($userid, $bedrijfsid, $tabel, $opslaan_ID, $BATCH);
                }

                $SQL .= ";";

                /*
                * Alvast controlleren of bestanden juiste rechten heeft
                */

                if (isset($_FILES[$tabel . "_SKIP_ADD_FILES"]['name']) && !empty($_FILES[$tabel . "_SKIP_ADD_FILES"]['name'])) {
                    $files = $_FILES[$tabel . "_SKIP_ADD_FILES"];
                    if (!is_array($files['name'])) {
                        // Change to array.
                        foreach ($files as $key => $value) {
                            $files[$key] = [$value];
                        }
                    }

                    foreach ($files['name'] as $key => $name) {
                        if (!checkAllowedFileType($name, $files['tmp_name'][$key])) {
                            continue;
//                            ob_clean();
//                            header("HTTP/1.0 400 Bad Request");
//                            echo "File not found or not allowed extension. Please go back and try again.";
//                            die;
                        }
                    }
                }
//                echo $SQL;die();
                $query = $pdo->prepare($SQL);
                if ($query->execute()) {

                    $inserted_ID = getLastId($tabel);
                    $inserted_ids[$tabel] = $inserted_ID;
                    $inserted_BID = 0;

                    if ($opslaan_ID == "-ID-" || $opslaan_ID == "0" || $opslaan_ID == "") {
                        auditaction($userid, $bedrijfsid, $tabel, $inserted_ID, 'Insert');
                    }

                    if ($SQL_als_tweede_uitvoeren != "") {
                        $SQL_als_tweede_uitvoeren = str_replace("XIDX", $inserted_ID, $SQL_als_tweede_uitvoeren);
                        $query = $pdo->prepare($SQL_als_tweede_uitvoeren);
                        $query->execute();
                    }

                    //Neem adres over
                    if ($tabel == "oft_bedrijf_adres" && (isset($_REQUEST[$tabel . "_SKIP_EXISTING"]) && $_REQUEST[$tabel . "_SKIP_EXISTING"])) {
                        $query = $pdo->prepare('SELECT * FROM oft_bedrijf_adres WHERE ID = :id;');
                        $query->bindValue('id', $_REQUEST[$tabel . "_SKIP_EXISTING"]);
                        $query->execute();
                        $dAdres = $query->fetch(PDO::FETCH_ASSOC);

                        if ($dAdres !== false) {
                            $query = $pdo->prepare('update oft_bedrijf_adres set ADRES = :adres,
 POSTCODE = :postcode,
 PLAATS = :plaats,
 LAND = :land,
 TELEFOON = :telefoon,
 FAX = :fax
 WHERE ID = :id;');
                            $query->bindValue('adres', $dAdres["ADRES"]);
                            $query->bindValue('postcode', $dAdres["POSTCODE"]);
                            $query->bindValue('plaats', $dAdres["PLAATS"]);
                            $query->bindValue('land', $dAdres["LAND"]);
                            $query->bindValue('telefoon', $dAdres["TELEFOON"]);
                            $query->bindValue('fax', $dAdres["FAX"]);
                            $query->bindValue('id', $inserted_ID);
                            $query->execute();
                        }
                    }

                    //Tabel bedrijf
                    if ($tabel == "bedrijf") {
                        $inserted_BID = $inserted_ID;
                        $queryBedrijf = $pdo->prepare('update bedrijf set BEDRIJFSID = :bedrijfsid WHERE ID = :id;');
                        $queryBedrijf->bindValue('bedrijfsid', $inserted_ID);
                        $queryBedrijf->bindValue('id', $inserted_ID);
                        $queryBedrijf->execute();

                        if ($opslaan_ID == "-ID-" || $opslaan_ID == "0" || $opslaan_ID == "") {
                            //Schrijfrechten toevoegen bij iedereen die schrijfrechten heeft.
                            $query = $pdo->prepare('SELECT * FROM login WHERE CRM = "Ja";');
                            $query->execute();

                            //foreach ($query->fetchAll() as $data) {
                            // $query = $pdo->prepare('insert into oft_rechten_tot set USERID = :userid, BEDRIJFSID = :bedrijfsid;');
                            // $query->bindValue('userid', $data["PERSONEELSLID"]);
                            // $query->bindValue('bedrijfsid', $inserted_ID);
                            // $query->execute();
                            //}
                        }

                        check_annual_accounts($userid, $inserted_ID);

                        $query = $pdo->prepare('SELECT * FROM bedrijf WHERE ID = :id limit 1;');
                        $query->bindValue('id', $bedrijfsid);
                        $query->execute();
                        $data = $query->fetch(PDO::FETCH_ASSOC);

                        if ($data !== false) {
                            if (isset($data[$inserted_ID])) {
                                $queryDir = $pdo->prepare('update bedrijf set DEFAULTDIR = :defaultdir WHERE ID = :id;');
                                $queryDir->bindValue('defaultdir', $data["DEFAULTDIR"]);
                                $queryDir->bindValue('id', $data[$inserted_ID]);
                                $queryDir->execute();
                            }

                            $queryHangtOnder = $pdo->prepare('update bedrijf set HANGTONDER = :hangtonder WHERE ID = :id;');
                            $queryHangtOnder->bindValue('hangtonder', $data["HANGTONDER"]);
                            $queryHangtOnder->bindValue('id', $inserted_ID);
                            $queryHangtOnder->execute();

                            $queryActive = $pdo->prepare('update bedrijf set ACTIVE = "Ja" WHERE ID = :id;');
                            $queryActive->bindValue('id', $inserted_ID);
                            $queryActive->execute();

                            $queryHandelsnaam = $pdo->prepare('update bedrijf set HANDELSNAAM = BEDRIJFSNAAM WHERE ID = :id;');
                            $queryHandelsnaam->bindValue('id', $inserted_ID);
                            $queryHandelsnaam->execute();
                        }

                        // enddate bedrijf, ook doorvoeren in managemnet
                        if (isset($_REQUEST['bedrijf_ENDDATUM']) && $_REQUEST['bedrijf_ENDDATUM']) {
                            $enddatum = $_REQUEST['bedrijf_ENDDATUM'];
                            $queryManagement = $pdo->prepare('UPDATE oft_management SET ENDATE = :enddate WHERE BEDRIJFSID=:bedrijfsid AND (ENDATE IS NULL OR ENDATE="0000-00-00" OR ENDATE > :enddate2)');
                            $queryManagement->bindValue('enddate', $enddatum);
                            $queryManagement->bindValue('bedrijfsid', $bedrijfsid);
                            $queryManagement->bindValue('enddate2', $enddatum);
                            $queryManagement->execute();
                        }
                    } elseif ($tabel == 'oft_shareholders' && ($opslaan_ID == "-ID-" || $opslaan_ID == "0" || $opslaan_ID == "")) {
                        //Doe ook insert transaction bij add shareholder
                        $sTransaction = str_replace("oft_shareholders", "oft_transactions", $SQL);
                        $sTransaction = str_replace("ADMINIDFROM", "XADMINIDFROMX", $sTransaction);
                        $sTransaction = str_replace("ADMINID =", "ADMINIDFROM =", $sTransaction);
                        $sTransaction = str_replace("XADMINIDFROMX", "ADMINID", $sTransaction);
                        $queryTransaction = $pdo->prepare($sTransaction);
                        $queryTransaction->execute();

                    } elseif ($tabel == 'oft_shareholders') {
                        //Als oft_shareholders_PERSENTAGE != 100% dan kopieregel toevoegen
                        if ($_REQUEST["oft_shareholders_PERSENTAGE"] != '100') {
                            //Bij transaction (dus edit shareholder)
                            $newPERSENTAGE = $originalData["PERSENTAGE"] - $_REQUEST["oft_shareholders_PERSENTAGE"];
                            $newQUANTITY = $originalData["QUANTITY"] - $_REQUEST["oft_shareholders_QUANTITY"];
                            if (($newPERSENTAGE * 1) != 0) {
                                $sAddTransaction = "";

                                $query = $pdo->prepare('insert into `oft_shareholders`
 set BEDRIJFSID = :bedrijfsid,
 UPDATEDATUM = :updatedatum,
 DELETED = "Nee",
 ADMINIDFROM = :adminidfrom,
 ADMINID = :adminid,
 CLASSTYPE = :classtype,
 DATUM = :datum,
 PERSENTAGE = :percentage,
 QUANTITY = :quantity;');
                                $query->bindValue('bedrijfsid', $originalData["BEDRIJFSID"]);
                                $query->bindValue('updatedatum', $originalData["UPDATEDATUM"]);
                                $query->bindValue('adminidfrom', $originalData["ADMINIDFROM"]);
                                $query->bindValue('adminid', $originalData["ADMINID"]);
                                $query->bindValue('classtype', $originalData["CLASSTYPE"]);
                                $query->bindValue('datum', $originalData["DATUM"]);
                                $query->bindValue('percentage', $newPERSENTAGE);
                                $query->bindValue('quantity', $newQUANTITY);
                                $query->execute();
                            }
                        }
                        //Bij transaction (dus edit shareholder)
                        $sAddTransaction = "";
                        $query = $pdo->prepare('insert into `oft_transactions`
 set BEDRIJFSID = :bedrijfsid,
 UPDATEDATUM = :updatedatum,
 DELETED = "Nee",
 ADMINIDFROM = :adminidfrom,
 ADMINID = :adminid,
 CLASSTYPE = :classtype,
 DATUM = :datum,
 PERSENTAGE = :percentage,
 QUANTITY = :quantity;');
                        $query->bindValue('bedrijfsid', $bedrijfsid);
                        $query->bindValue('updatedatum', $_REQUEST["oft_shareholders_DATUM"]);
                        $query->bindValue('adminidfrom', $_REQUEST["oft_shareholders_ADMINID"]);
                        $query->bindValue('adminid', $originalData["ADMINID"]);
                        $query->bindValue('classtype', $_REQUEST["oft_shareholders_CLASSTYPE"]);
                        $query->bindValue('datum', $_REQUEST["oft_shareholders_DATUM"]);
                        $query->bindValue('percentage', $_REQUEST["oft_shareholders_PERSENTAGE"]);
                        $query->bindValue('quantity', $_REQUEST["oft_shareholders_QUANTITY"]);
                        //echo "<br/>$sAddTransaction";
                        $query->execute();
                    }

                    if ($tabel == "login" && ($opslaan_ID == "-ID-" || $opslaan_ID == "0" || $opslaan_ID == "")) {
                        $query = $pdo->prepare('SELECT * FROM login WHERE ID = :id limit 1;');
                        $query->bindValue('id', $inserted_ID);
                        $query->execute();
                        $data = $query->fetch(PDO::FETCH_ASSOC);

                        if ($data !== false) {
                            setProfile(getLoginId(), $inserted_ID, $_REQUEST["login_BEDRIJFSID"]);
                            oft_insert_entiteit_rechten($userid, $bedrijfsid, $inserted_ID);
                        }
                    }

                    if ($tabel == "oft_boardpack" && isset($_REQUEST["BP_ITEM_1"])) {
                        if ($opslaan_ID == 0) {
                            $opslaan_ID = $inserted_ID;
                        }
                        //Boardpack items opslaan
                        for ($a = 0; $a <= 30; $a++) {
                            if (isset($_REQUEST["BP_ITEM_" . $a]) && $_REQUEST["BP_ITEM_" . $a] != '') {
                                if (isset($_REQUEST["BP_ITEM_ID_" . $a]) && $_REQUEST["BP_ITEM_ID_" . $a] > 0) {
                                    //Update item
                                    $query = $pdo->prepare('update oft_boardpack_items
 set ITEM = :item
 where ID = :id;');
                                    $query->bindValue('item', $_REQUEST["BP_ITEM_" . $a]);
                                    $query->bindValue('id', $_REQUEST["BP_ITEM_ID_" . $a]);
                                    $query->execute();
                                    $boardpack_item_id = intval($_REQUEST["BP_ITEM_ID_" . $a]);
                                } else {
                                    //Insert item
                                    //$opslaan_ID = $inserted_ID; neeee, opslaan id gebruiken, indien niet beschikbaar inserted id
                                    $query = $pdo->prepare('insert into oft_boardpack_items
 set USERID = :userid,
 BEDRIJFSID = :bedrijfsid,
 DELETED = "Nee",
 ITEM = :item,
 BPID = :bpid;');
                                    $query->bindValue('userid', $userid);
                                    $query->bindValue('bedrijfsid', $bedrijfsid);
                                    $query->bindValue('item', $_REQUEST["BP_ITEM_" . $a]);
                                    $query->bindValue('bpid', $opslaan_ID);
                                    $query->execute();

                                    $boardpack_item_id = $pdo->lastInsertId();
                                }

                                //check file 1 (max 15)
                                for ($f = 1; $f <= 15; $f++) {

                                    //BoardPack
                                    if (isset($_FILES["BP_ITEM_FILE" . $f . "_" . $a]) && $_FILES["BP_ITEM_FILE" . $f . "_" . $a]['size'] > 0) {

                                        if ($files = $_FILES["BP_ITEM_FILE" . $f . "_" . $a]) {
                                            $docid = uploadBestandInDB_2($files,
                                                $bedrijfsid, $userid, "Boardpack", "oft_boardpack_items",
                                                $boardpack_item_id, '', $_REQUEST["oft_boardpack_FINANCIALYEAR"]);
                                            $query = $pdo->prepare('update oft_boardpack_items set FILE".$f." = :file where ID = :id;');
                                            $query->bindValue('file', $docid);
                                            $query->bindValue('id', $boardpack_item_id);
                                            $query->execute();
                                        }
                                    }
                                }
                            } elseif (isset($_REQUEST["BP_ITEM_ID_" . $a]) && $_REQUEST["BP_ITEM_ID_" . $a] > 0) {
                                //Delete item
                                auditaction($userid, $bedrijfsid, 'oft_boardpack_items',
                                    $_REQUEST["BP_ITEM_ID_" . $a],
                                    'Delete');
                                $query = $pdo->prepare('delete from oft_boardpack_items where ID = :id;');
                                $query->bindValue('id', $_REQUEST["BP_ITEM_ID_" . $a]);
                                $query->execute();
                            }
                        }
                    }

                    if ($tabel == "oft_boardpack" && isset($_REQUEST["BP_INVITEE_1"])) {
                        for ($a = 0; $a <= 20; $a++) {
                            if (isset($_REQUEST["BP_INVITEE_" . $a]) && ($_REQUEST["BP_INVITEE_" . $a] * 1) > 0) {
                                if ($opslaan_ID == 0) {
                                    $opslaan_ID = $inserted_ID;
                                }
                                $query = $pdo->prepare('insert into oft_boardpack_invitees
 set UPDATEDATUM = "' . date("Y-m-d") . '",
 BEDRIJFSID = :bedrijfsid,
 DELETED = "Nee",
 USERID = :userid,
 BPID = :bpid;');
                                $query->bindValue('bedrijfsid', $bedrijfsid);
                                $query->bindValue('userid', $_REQUEST["BP_INVITEE_" . $a]);
                                $query->bindValue('bpid', $opslaan_ID);
                                $query->execute();
                            }
                        }
                    }

                    if ((isset($_REQUEST[$tabel . "_SKIP_ADD_FILES_TYPE"]) || isset($_REQUEST[$tabel . "_DOCTYPE"])) && isset($_FILES[$tabel . "_SKIP_ADD_FILES"]['name'])) {
                        $year = date("Y");
                        $doctype = "";

                        if (isset($_REQUEST[$tabel . "_FINANCIALYEAR"])) {
                            $year = substr($_REQUEST[$tabel . "_FINANCIALYEAR"], 0, 4);
                        } elseif (isset($_REQUEST[$tabel . "_STANDARDDUEDATE"])) {
                            $year = substr($_REQUEST[$tabel . "_STANDARDDUEDATE"], 0, 4);
                        } elseif (isset($_REQUEST[$tabel . "_DATUMSTART"])) {
                            $year = substr($_REQUEST[$tabel . "_DATUMSTART"], 0, 4);
                        } elseif (isset($_REQUEST[$tabel . "_DATUM"])) {
                            $year = substr($_REQUEST[$tabel . "_DATUM"], 0, 4);
                        } elseif (isset($_REQUEST[$tabel . "_ENDDATUM"])) {
                            $year = substr($_REQUEST[$tabel . "_ENDDATUM"], 0, 4);
                        }

                        if (isset($_REQUEST[$tabel . "_SKIP_ADD_FILES_TYPE"])) {
                            $doctype = $_REQUEST[$tabel . "_SKIP_ADD_FILES_TYPE"];
                        } elseif (isset($_REQUEST[$tabel . "_DOCTYPE"])) {
                            $doctype = $_REQUEST[$tabel . "_DOCTYPE"];
                        }
                        $docId = "";
                        if ($tabel == "documentbeheer") {
                            $docId = getLastId($tabel);
                        }

                        if ($files = $_FILES[$tabel . "_SKIP_ADD_FILES"]) {
                            uploadBestandInDB_2($files, $bedrijfsid, $userid, $doctype, $tabel, $opslaan_ID, $docId,
                                $year);
                        }
                    }

                    if ($tabel == "oft_incident_register") {
                        if ($opslaan_ID != "-ID-" && $opslaan_ID != "0" && $opslaan_ID != "") {
                            //Update incidents
                            $query = $pdo->prepare("SELECT * FROM oft_incident_register_task WHERE INCIDENTID = :incidentid ORDER BY DATUM;");
                            $query->bindValue('incidentid', $opslaan_ID);
                            $query->execute();

                            foreach ($query->fetchAll() as $data_ireg) {
                                $data_ireg_id = $data_ireg["ID"] * 1;

                                if (isset($_REQUEST["INCIDENT_ACTIONS_$data_ireg_id"])) {
                                    $iDatum = null;
                                    if ($_REQUEST["INCIDENT_ACTIONS_DATUM_$data_ireg_id"] != "") {
                                        $iDatum = $_REQUEST["INCIDENT_ACTIONS_DATUM_$data_ireg_id"];
                                    }
                                    if (trim($_REQUEST["INCIDENT_ACTIONS_$data_ireg_id"]) == "") {
                                        auditaction($userid, $bedrijfsid, 'oft_incident_register_task',
                                            $data_ireg_id,
                                            'Delete');
                                        $query = $pdo->prepare('delete from oft_incident_register_task WHERE ID = :id;');
                                        $query->bindValue('id', $data_ireg_id);
                                        $query->execute();
                                    } else {
                                        $query = $pdo->prepare('update oft_incident_register_task set UPDATEDATUM = "' . date("Y-m-d") . '",
 TASK = :task,
 STATUS = :status,
 DATUM = :datum
 WHERE ID = :id;');
                                        $query->bindValue('task', $_REQUEST["INCIDENT_ACTIONS_$data_ireg_id"]);
                                        $query->bindValue('status',
                                            $_REQUEST["INCIDENT_ACTIONS_STATUS_$data_ireg_id"]);
                                        $query->bindValue('datum', $iDatum);
                                        $query->bindValue('id', $data_ireg_id);
                                        $query->execute();
                                    }
                                }
                            }
                        }

                        //Update new Incident
                        if (isset($_REQUEST["INCIDENT_ACTIONS_n"]) && trim($_REQUEST["INCIDENT_ACTIONS_n"]) != "") {
                            $iDatum = null;
                            if ($_REQUEST["INCIDENT_ACTIONS_DATUM_n"] != "") {
                                $iDatum = $_REQUEST["INCIDENT_ACTIONS_DATUM_n"];
                            }
                            if ($opslaan_ID != "-ID-" && $opslaan_ID != "0" && $opslaan_ID != "") {
                                $inserted_ID = $opslaan_ID;
                            }

                            $query = $pdo->prepare('insert into oft_incident_register_task set BEDRIJFSID = :bedrijfsid,
 UPDATEDATUM = "' . date("Y-m-d") . '",
 DELETED = "Nee",
 INCIDENTID = :incidentid,
 TASK = :task,
 STATUS = :status,
 DATUM = :datum;');
                            $query->bindValue('bedrijfsid', $bedrijfsid);
                            $query->bindValue('incidentid', $inserted_ID);
                            $query->bindValue('task', $_REQUEST["INCIDENT_ACTIONS_n"]);
                            $query->bindValue('status', $_REQUEST["INCIDENT_ACTIONS_STATUS_n"]);
                            $query->bindValue('datum', $iDatum);
                            $query->execute();
                        }
                    }
                } else {
                    return false;
//                    header("HTTP/1.0 400 Bad Request");
//                    ob_clean();
//                    echo $query->errorInfo()[2];
                }
            }
        }

        if (isset($_REQUEST["SEND"]) && $tabel == "oft_boardpack") {
            oft_send_boardpack($userid, $bedrijfsid, $opslaan_ID);
        }

        if (isset($_REQUEST["SITE"]) && $_REQUEST["SITE"] == "oft_persons_add" && $inserted_ID > 0) {
            rd("content.php?SITE=oft_users_edit&PID=$inserted_ID&ID=$inserted_ID");
        } elseif (isset($_REQUEST["NEXT_STEP"]) && $inserted_BID > 0) {
            $_REQUEST["NEXT_STEP"] = str_replace("&amp;", "&", $_REQUEST["NEXT_STEP"]);
            $BID = "&BID=$inserted_BID";
            if (count(explode('BID=', $_REQUEST["NEXT_STEP"])) > 1) {
                $BID = "";
            }
            rd($_REQUEST["NEXT_STEP"] . $BID);
        } elseif (isset($_REQUEST["NEXT_STEP"])) {
            $BID = $bedrijfsid;
            if (isset($_REQUEST["BID"])) {
                $BID = "&BID=" . $_REQUEST["BID"];
            }
            rd($_REQUEST["NEXT_STEP"] . $BID);
        } else {
//            rd($url_querystring);
        }
    } elseif (isset($_REQUEST["PREVIOUS"])) {
        if (isset($_REQUEST["PREV_STEP"])) {
            $BID = $bedrijfsid;
            if (isset($_REQUEST["BID"])) {
                $BID = "&PREVIOUS=true&BID=" . $_REQUEST["BID"];
            }
            rd($_REQUEST["PREV_STEP"] . $BID);
        }
    }
    return true;
} // close opslaan


/**
 * RECHTEN ENTITEIT OPSLAAN
 *
 * @param int   $userid
 * @param int   $bedrijfsid
 * @param int   $update_user
 * @param array $ENT alle aangevinktje ids [ENT][id] = "Ja"
 */
function oft_insert_entiteit_rechten($userid, $bedrijfsid, $update_user)
{
    global $pdo;
    // standaardrechten
    $LEGAL = isset($_REQUEST['login_LEGAL']) ? $_REQUEST['login_LEGAL'] : false;
    $COMPLIANCE = isset($_REQUEST['login_COMPLIANCE']) ? $_REQUEST['login_COMPLIANCE'] : false;
    $BOARDPACK = isset($_REQUEST['login_BOARDPACK']) ? $_REQUEST['login_BOARDPACK'] : false;
    $FINANCE = isset($_REQUEST['login_FINANCE']) ? $_REQUEST['login_FINANCE'] : false;
    $HR = isset($_REQUEST['login_HR']) ? $_REQUEST['login_HR'] : false;
    $CONTRACTS = isset($_REQUEST['login_CONTRACTS']) ? $_REQUEST['login_CONTRACTS'] : false;

    $MAILNOTIFICATIONS = isset($_REQUEST['login_MAILNOTIFICATIONS']) ? $_REQUEST['login_MAILNOTIFICATIONS'] : false;

    // entiteit ids filteren uit request
    $entity_ids = false;
    foreach ($_POST as $key => $val) {
        if (strstr($key, 'ENT') && is_numeric(substr($key, 3))) {
            $bid = substr($key, 3);
            $entity_ids[$bid] = $bid;
        }
    }

    // Controleer of er al rechten aangemaakt zijn, zoniet:aanmaken met standaardrechten van login
    if (!is_array($entity_ids)) {
        return;
    }
    // controleer bestaande rechten
    $statement = 'SELECT BEDRIJFSID FROM oft_rechten_tot WHERE USERID = :userid AND BEDRIJFSID IN ("' . implode(', ',
            $entity_ids) . '") ';
    $queryRechten = $pdo->prepare($statement);
    $queryRechten->bindValue('userid', $update_user);
    $queryRechten->execute();

    foreach ($queryRechten->fetchAll() as $cRow) {
        unset($entity_ids[$cRow['BEDRIJFSID']]);
    }

    // insert aangevinke entities
    if (!is_array($entity_ids)) {
        return;
    }
    $insert_rows = array();
    foreach ($entity_ids as $bid) {
        $insert_rows[] = "('" . $update_user . "', '" . $bid . "', '" . $LEGAL . "', '" . $COMPLIANCE . "', '" . $BOARDPACK . "', '" . $FINANCE . "', '" . $HR . "', '" . $CONTRACTS . "', '" . $MAILNOTIFICATIONS . "')";
    }
    $statement = 'INSERT INTO oft_rechten_tot (USERID, BEDRIJFSID, LEGAL, COMPLIANCE, BOARDPACK, FINANCE, HR, CONTRACTS, MAILNOTIFICATIONS) VALUES ' . implode(', ',
            $insert_rows) . ';';
    $query = $pdo->prepare($statement);
    $query->execute();


}
