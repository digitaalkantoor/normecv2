<?php
sercurityCheck();

function oft_hr_documents_add($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_document_hr.htm";
  $tabel        = "documentbeheer";
  $titel        = "Add document";

  oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_hr_documents_edit($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_document_hr.htm";
  $tabel        = "documentbeheer";
  $titel        = "Edit document";

  oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_hr($userid, $bedrijfsid) {
  global $db;
  
  $_REQUEST["SRCH_YEAR"] = "x".date("Y");
  $_REQUEST["SRCH_JAAR"] = date("Y");

  $titel           = tl('Human Resources');
  $tabel           = "documentbeheer";
  $toevoegen       = "content.php?SITE=oft_hr_documents_add&SECTIE=HR";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_hr_documents_edit&ID=-ID-");
  $verwijderen     = "";
  $submenuitems    = "<a href=\"content.php?SITE=dragAndDrop&SECTION=oft_hr\">Upload multiple documents</a>  | ". oft_inputform_link($toevoegen, "New document");

  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayZoekvelden");

  $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, getDoctypesBySection("HR"));

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_hr_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "documentbeheer";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_hr_overzicht", "arrayZoekvelden");

  $toevoegen       = "";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
  $verwijderen     = "";

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, getDoctypesBySection("HR")); 
}

function oft_entitie_hr($userid, $bedrijfsid) {
  global $db;

  echo oft_standaard_menu_page($userid, $bedrijfsid, "Human Resources", "oft_entitie_hr", "documentbeheer", "BESTANDSNAAM", oft_back_button_entity(), "Add document", "");
}

function oft_entitie_hr_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "documentbeheer";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, "oft_entitie_hr_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, "oft_entitie_hr_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, "oft_entitie_hr_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, "oft_entitie_hr_overzicht", "arrayZoekvelden");

  $toevoegen       = "content.php?SITE=oft_document_add&SECTIE=HR";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_document_edit&ID=-ID-");
  $verwijderen     = "";

  $sql_toevoeging  = "AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."' ". getDoctypesBySection("HR");

    echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'BESTANDSNAAM', $toevoegen, $bewerken, $verwijderen, $sql_toevoeging); 
}

