<?php
sercurityCheck();

function oft_contracten($userid, $bedrijfsid) {
  global $db;

  getRechten($userid, "CONTRACTS");

  $titel           = tl('Contracts');
  $tabel           = "contracten";
  $toevoegen       = "";
  if(isRechtenOFT($bedrijfsid, "CONTRACTEN")) {
    $toevoegen     = "content.php?SITE=oft_".$tabel."_add";
  }
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_".$tabel."_edit&ID=-ID-");
  $verwijderen     = "";
  $submenuitems    = oft_inputform_link($toevoegen, "New contract");

  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $contentFrame = oft_tabel($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'REFERENTIE', $toevoegen, $bewerken, $verwijderen);

  echo oft_framework_basic($userid, $bedrijfsid, $contentFrame, $titel, $submenuitems);
}

function oft_contracten_ajax($userid, $bedrijfsid) {
  global $db;

  $tabel           = "contracten";
  $toevoegen       = "content.php?SITE=oft_".$tabel."_add";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_".$tabel."_edit&ID=-ID-");
  $verwijderen     = "";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'REFERENTIE', $toevoegen, $bewerken, $verwijderen);
}

function oft_contracten_add($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_contract_add.htm";
  $tabel        = "contracten";
  $titel        = "New contract";

  if(isset($_REQUEST["BID"])) {
    $bedrijfsid = $_REQUEST["BID"];
  }

  oft_add_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_contracten_edit($userid, $bedrijfsid) {
  $templateFile = "./templates/oft_contract.htm";
  $tabel        = "contracten";
  $titel        = "Edit contract";

  oft_edit_page($userid, $bedrijfsid, $templateFile, $tabel, $titel);
}

function oft_entitie_contracts($userid, $bedrijfsid) {
  echo oft_standaard_menu_page($userid, $bedrijfsid, "Contracts", "oft_entitie_contracts", 'contracten', "REFERENTIE", oft_back_button_entity(), "New contract", "");
}

function oft_entitie_contracts_ajax($userid, $bedrijfsid) {
  global $db;

  $toevoegen       = "content.php?SITE=oft_contracten_add&BID=$bedrijfsid";
  $bewerken        = oft_inputform_link_short("content.php?SITE=oft_contracten_edit&ID=-ID-");
  $verwijderen     = "";
  $tabel           = "oft_entitie_contracts";
  $arrayVelden     = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVelden");
  $arrayVeldnamen  = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldnamen");
  $arrayVeldType   = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayVeldType");
  $arrayZoekvelden = oft_get_structure($userid, $bedrijfsid, $tabel."_overzicht", "arrayZoekvelden");

  $tabel           = "contracten";
  echo oft_tabel_content($userid, $bedrijfsid, $arrayVelden, $arrayVeldnamen, $arrayVeldType, $arrayZoekvelden, $tabel, 'REFERENTIE', $toevoegen, $bewerken, $verwijderen, " AND BEDRIJFSID = '".ps($bedrijfsid, "nr")."' ");
}
