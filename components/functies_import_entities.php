<?php
/**
 * Created by JetBrains PhpStorm.
 * User: MindedPC5
 * Date: 22-01-14
 * Time: 10:28
 * To change this template use File | Settings | File Templates.
 */
class import_export_excel_entities {

  // Variables
  private $knownWorksheets = array("Basics", "Registered address", "Postal address", "Visiting address", "Compliance settings", "Directors", "Providers", "Users", "Agenda", "User rights");
  
  /*
   * Public function Entry of proces.
   * 
   * @param (int) $userid The Id of logged in user
   * @param (int) $bedrijfsid The Id of selected company
   * 
   * @author Bob Salm
   */
  public function import_excel_entities($userid, $bedrijfsid) {

    $content = "";

    if (isset($_REQUEST['excel_import_send'])) {

      $tmpFile = $_FILES['excel']['tmp_name'];
      
      $content .= "<div class=\"oft2_page_centering\"><h3>Importing Excel</h3>";
      if($this->checkFileExtension($tmpFile)) {
        $content .= "<p>Please check the log of the import below.</p>";
        $content .= $this->importExcel($tmpFile);
      } else {
        $content .= "<p>The uploaded file is not an Excel file.</p>";
        $content .= $this->wizzard_import_entities();
      }
      $content .= "</div>";

    } else {
      $content .= "<div class=\"dashboardVakje\">".$this->wizzard_import_entities()."</div>";
    }

    echo oft_framework_basic($userid, $bedrijfsid, $content, "Import entities", '');
  }

  /*
   * Public function to show the form for an Excel file
   * 
   * @return (string) Html form.
   * 
   * @author Bob Salm
   */
  public function wizzard_import_entities() {

    $content = "<div class=\"oft2_page_centering\"><br/><br/>"; //<a href=\"./templates/voorbeeld_entities.xls\">".tl("Download example file")." &#187;</a><br/><br/>

    $content .= "Please upload an Excel file with the following worksheets:</p>"
                . "<ul><li>" . implode("</li><li>", $this->knownWorksheets) .  "</li></ul>";
    
    $content .= "<form action='content.php?SITE=".$_REQUEST["SITE"]."' method='post' enctype='multipart/form-data' >";
    $content .= fileVeld('excel', "field");
    $content .= "<br/>";

    $content .= "<input class='btn-action' type='submit' name='excel_import_send' value='".tl("Import file")."' />";
    $content .= "</form></div>";
  
    return $content;
  }

  /*
   * Private function Startpoint of the Excel import. Iterate through known
   * worksheets. Each worksheet has its own import function. One worksheet can
   * update or insert records in multiple database tables.
   * 
   * @param (string) $file The uploaded Excel file
   * 
   * @return (string) Html feedback of each row of each imported worksheet.
   * 
   * @author RJP Granneman
   */
  private function importExcel($file) {
      
    // Variables    
    $useHeaderLine = false;
    $bedrijfsid = getMyBedrijfsID();
    $userid = getUserID(getLoginID());
    $content = "";
    $worksheetsFound = array();

    // Import classes and functions
    require_once("./koppelingen/Importers/Reader.php");
    require_once("./koppelingen/Importers/Excel.php");
    
    $excel = new Importer\Excel();
    $excel->connect( array( 'path' => $file, 'filename' => '', 'header_line' => $useHeaderLine ) );
    
    //Je kunt me deze class ook werken met de variabelen: $worksheet = null; zie Excel.php    
    // getWorksheetIterator
    $worksheets = $excel->sheetNames();
    
    // Overwrite worksheets for testing purposes
    #$this->knownWorksheets = array("Registered address");

    foreach ($this->knownWorksheets as $sheetName) {
      if (in_array($sheetName, $worksheets)) {
        $worksheetsFound[] = $sheetName;
        $index = array_search($sheetName, $worksheets);
        $excel->openWorksheet( $index );
        
        
        switch($sheetName) {
          case 'Basics': //'Company address and bascis':
            $content .= $this->importEntity($userid, $bedrijfsid, $excel, $useHeaderLine);
            $this->correctEntityBedrijfsId();
            break;
          case 'Directors':
            $content .= $this->importDirectors($userid, $bedrijfsid, $excel, $useHeaderLine);
            break;
          case 'Providers':
            $content .= $this->importProviders($userid, $bedrijfsid, $excel, $useHeaderLine);
            break;
          case 'Compliance settings':
            $content .= $this->importComplianceSettings($userid, $bedrijfsid, $excel, $useHeaderLine);
            break;
          case 'Users':
            $content .= $this->importUsers($userid, $bedrijfsid, $excel, $useHeaderLine);
            break;
          case 'Agenda':
            $content .= $this->importAgendaItems($userid, $bedrijfsid, $excel, $useHeaderLine);
            break;
          case 'Registered address':
            $content .= $this->importAddress($userid, $bedrijfsid, $excel, $useHeaderLine, 'Registered');
            break;
          case 'Postal address':
            $content .= $this->importAddress($userid, $bedrijfsid, $excel, $useHeaderLine, 'Postal address');
            break;
          case 'Visiting address':
            $content .= $this->importAddress($userid, $bedrijfsid, $excel, $useHeaderLine, 'Visiting address');
            break;
          case 'User rights':
            $content .= $this->importUserRights($userid, $bedrijfsid, $excel, $useHeaderLine);
            break;
        }
      }
    }
    
    // Show which worksheet weren't found
    $notFound = array_diff($this->knownWorksheets, $worksheetsFound);
    if(!empty($notFound)) {
      $content .= "<p>The following worksheets are not found:</p><ul><li>" . implode("</li><li>", $notFound) . "</li></ul>";
    }
    
    return $content;
  }
  
  /*
   * Private function to map Excel to Database and return feedback.
   * 
   * @param (int) $userid The Id of current logged in user
   * @param (int) $bedrijfsid The current selected bedrijf Id
   * @param (phpExcel) $excel The excel with correct worksheet active
   * 
   * @return (string) HTML table of imported Excel rows
   * 
   * @author RJP Granneman
   */
  private function importEntity($userid, $bedrijfsid, $excel, $useHeaderLine) {
    
    // Variables
    $content = '<h2>Basics</h2><table class="imported-entities"><thead><tr><th>ID</th><th>Name</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $excelHeaders = array();
      $importeerdeze = true;
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
          $i++;
          $excelHeaders = $excel_row;
          continue;
      }
      
      // Map Excel fields to database fields
      $entityData = $this->mappingEntity($userid, $bedrijfsid, $excel_row);
      
      // TODO: welke voorwaarden hangen hieraan?
      if($entityData["BEDRIJFSNAAM"] == '') {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No entity name.") . '</td></tr>';
        $importeerdeze = false;
      }
      if($entityData["BEDRIJFNR"] == '') {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No short code.") . '</td></tr>';
        $importeerdeze = false;
      }

      // execute query
      if ($importeerdeze) {
        // Als er geen foutmeldingen zijn
        $insertResult = $this->insertEntity($userid, $bedrijfsid, $entityData, "BEDRIJFSNAAM", true);
          $content .= "<tr><td>" . $insertResult['id'] . "</td>"
                  . "<td>" . $insertResult['name'] . "</td>"
                  . "<td>" . $insertResult['action'] . "</td>"
                  . "<td>" . ($insertResult['actionStatus'] ? 'success' : 'failed') . "</td></tr>";
      } else {
        // To do: fetch error msg and put in $content
      }
      
      $i++;
      
    } // End of while loop
    
    $content .= '</tbody></table>';
    
    return $content;
  }
  
  /*
   * Private function to get the id of a Stam-value, insert when not available.
   * 
   * @param (string) $stamtabel The DB table to search from or insert in
   * @param (string) $value The name to search for
   * @param (int) $bedrijfsid The entity the value corresponds to
   * 
   * @return (int) The Id of the record
   * 
   * @author Bob Salm
   */
  private function getStamtabel_value($stamtabel, $value, $bedrijfsid) {
    global $pdo;
  
    if(!preg_match("/^stam_[a-z]*$/i", $stamtabel)) {
      throw new Exception("Database error: table name does not exists.");
    }
    
    $id = '';
    if (lb($value) != '') {
      $query = $pdo->prepare("SELECT ID FROM $stamtabel WHERE NAAM = :naam LIMIT 1;");
      $query->bindValue('naam', $value);
      $query->execute();
      
      if ($query->rowCount() > 0) {
        $dStamtabel = $query->fetch(PDO::FETCH_ASSOC);
        $id = ($dStamtabel["ID"] * 1);
      }
      
      if(empty($id)) {
        $queryInsert = $pdo->prepare(" INSERT INTO $stamtabel
                     SET `UPDATEDATUM` = NOW(), 
                     `DELETED` = 'Nee', 
                     `NAAM` = :naam, 
                     `BEDRIJFSID` =  :bedrijfsid;");
        $queryInsert->bindValue('naam', $value);
        $queryInsert->bindValue('bedrijfsid', $bedrijfsid);
        $queryInsert->execute();
        
        $id = $pdo->lastInsertId();
      }
    }
  
    return $id;
  }
  
  /*
   * Public function to insert or update an entity
   * Entities are stored in the DB bedrijf
   * 
   * @param $userid (int) Id of logged in user
   * @param $bedrijfsid (int) Id of selected company
   * @param $data (array) The data of the entity
   * @param $checkKolom (string) The DB field to check if entity already exists
   * @param $update (bool) Should an existing record be updated
   * 
   * @return (array) The action that was done (string),
   *                 the status of that action (bool),
   *                 the Id of the inserted or updated record (int)
   * 
   * @author Bob Salm, RJP Granneman
   */
  private function insertEntity($userid, $bedrijfsid, $data = array(), $checkKolom = "BEDRIJFSNAAM", $update = false) {
    
    global $pdo;
  
    // ToDo: Wat doet deze check?
    $checkValue = $data["BEDRIJFSNAAM"];
    if($checkKolom != "BEDRIJFSNAAM" && isset($data[$checkKolom])) {
      $checkValue = $data[$checkKolom];
    }
  
    // Is there already a database record with checkValue
    if(isset($data["BEDRIJFNR"]) && !empty($data["BEDRIJFNR"])) {
      $sKlant = "SELECT ID "
              . "FROM bedrijf "
              . "WHERE $checkKolom = '" . ps($checkValue, "text") . "' "
              . "OR BEDRIJFNR = '".ps($data["BEDRIJFNR"])."' "
              . "LIMIT 1;";
      
      $query = $pdo->prepare('SELECT ID 
              FROM bedrijf
              WHERE :checkKolom = :checkvalue
              OR BEDRIJFNR = :bedrijfsid
              LIMIT 1;');
      $query->bindValue('checkKolom', $checkKolom);
      $query->bindValue('checkvalue', $checkValue);
      $query->bindValue('bedrijfsid', $data["BEDRIJFNR"]);
    } else {
      $sKlant = "SELECT ID "
              . "FROM bedrijf "
              . "WHERE $checkKolom = '" . ps($checkValue, "text") . "' "
              . "LIMIT 1;";
      $query = $pdo->prepare('SELECT ID
              FROM bedrijf
              WHERE :checkKolom = :checkvalue
              LIMIT 1;');
      $query->bindValue('checkKolom', $checkKolom);
      $query->bindValue('checkvalue', $checkValue);
    }

    $query->execute();
    
    $found = false;
    foreach ($query->fetchAll() as $dKlant) {
      $ID = $dKlant['ID'] * 1;
      $found = true;
  
      if($update) {
        $queryInsert = $pdo->prepare('UPDATE bedrijf SET UPDATEDATUM = CURDATE()
              , DELETED = :deleted
              , ENTITYNATURAL = :entitynatural
              , ACTIVE = :active
              , ENDREDEN = :endreden
              , BEDRIJFSNAAM = :bedrijfsnaam
              , BEDRIJFNR = :bedrijfsnr
              , HANDELSNAAM = :handelsnaam
              , RECHTSVORM = :rechtsvorm
              , ENDOFBOOKYEAR = :endofbookyear
              , REGISTEREDNUMBER = :registerednumber
              , LAND = :land
              , OMSCHRIJVING = :omschrijving
              , OPRICHTINGSDATUM = :oprichtingsdatum
              , LEI = :lei
              , GIIN = :giin
              WHERE ID = :id LIMIT 1
              ;');
        $queryInsert->bindValue('deleted', $data["DELETED"]);
        $queryInsert->bindValue('entitynatural', $data["ENTITYNATURAL"]);  
        $queryInsert->bindValue('active', $data["ACTIVE"]);
        $queryInsert->bindValue('endreden', $data["ENDREDEN"]);
        $queryInsert->bindValue('bedrijfsnaam', $data["BEDRIJFSNAAM"]);
        $queryInsert->bindValue('bedrijfsnr', $data["BEDRIJFNR"]);
        $queryInsert->bindValue('handelsnaam', $data["HANDELSNAAM"]);
        $queryInsert->bindValue('rechtsvorm', $data["RECHTSVORM"]);
        $queryInsert->bindValue('endofbookyear', $data["ENDOFBOOKYEAR"]);
        $queryInsert->bindValue('registerednumber', $data["REGISTEREDNUMBER"]);
        $queryInsert->bindValue('land', $data["LAND"]);
        $queryInsert->bindValue('omschrijving', $data["OMSCHRIJVING"]);
        $queryInsert->bindValue('oprichtingsdatum', $data["OPRICHTINGSDATUM"]);
        $queryInsert->bindValue('lei', $data["LEI"]);
        $queryInsert->bindValue('giin', $data["GIIN"]);
        $queryInsert->bindValue('id', $ID);
        
        $action = 'update';
        $actionStatus = $queryInsert->execute();
        if($actionStatus) {
          audittrailBatch($userid, $bedrijfsid, 'bedrijf', $ID, $data);
        }
      } else {
          $action = 'ignore';
          $actionStatus = true; // Ignoring will always work :-)
      }
  
    } 
    
    if(!$found) {
      
      $queryInsert = $pdo->prepare('INSERT INTO bedrijf SET UPDATEDATUM = CURDATE()
              , DELETED = :deleted
              , ENTITYNATURAL = :entitynatural
              , ACTIVE = :active
              , ENDREDEN = :endreden
              , BEDRIJFSNAAM = :bedrijfsnaam
              , BEDRIJFSID = :bedrijfsid
              , BEDRIJFNR = :bedrijfsnr
              , HANDELSNAAM = :handelsnaam
              , RECHTSVORM = :rechtsvorm
              , ENDOFBOOKYEAR = :endofbookyear
              , REGISTEREDNUMBER = :registerednumber
              , LAND = :land
              , MONITOR = "Ja"
              , OMSCHRIJVING = :omschrijving
              , OPRICHTINGSDATUM = :oprichtingsdatum
              , LEI = :lei
              , GIIN = :giin;');
      $queryInsert->bindValue('deleted', $data["DELETED"]);
      $queryInsert->bindValue('entitynatural', $data["ENTITYNATURAL"]);  
      $queryInsert->bindValue('active', $data["ACTIVE"]);
      $queryInsert->bindValue('endreden', $data["ENDREDEN"]);
      $queryInsert->bindValue('bedrijfsnaam', $data["BEDRIJFSNAAM"]);
      $queryInsert->bindValue('bedrijfsid', $bedrijfsid);
      $queryInsert->bindValue('bedrijfsnr', $data["BEDRIJFNR"]);
      $queryInsert->bindValue('handelsnaam', $data["HANDELSNAAM"]);
      $queryInsert->bindValue('rechtsvorm', $data["RECHTSVORM"]);
      $queryInsert->bindValue('endofbookyear', $data["ENDOFBOOKYEAR"]);
      $queryInsert->bindValue('registerednumber', $data["REGISTEREDNUMBER"]);
      $queryInsert->bindValue('land', $data["LAND"]);
      $queryInsert->bindValue('omschrijving', $data["OMSCHRIJVING"]);
      $queryInsert->bindValue('oprichtingsdatum', $data["OPRICHTINGSDATUM"]);
      $queryInsert->bindValue('lei', $data["LEI"]);
      $queryInsert->bindValue('giin', $data["GIIN"]);
        
      
      $ID = 0;
      $action = 'insert';
      $actionStatus = $queryInsert->execute();
      if ($actionStatus) {
        $ID = getLastID("bedrijf");
        auditaction($userid, $bedrijfsid, 'bedrijf', $ID, 'Insert');
      }
    }
    
    // Insert or update Fiscal number in oft_entity_numbers
    if(!empty($data['FISCAL'])) {
      $this->insertEntityNumber($ID, 'Fiscal', $data['FISCAL']);
    }
    
    // Insert or update VAT in oft_entity_numbers
    if(!empty($data['VAT'])) {
      $this->insertEntityNumber($ID, 'VAT', $data['VAT']);
    }
    
    // Insert or update LEI in oft_entitiy_numbers
    if(!empty($data['LEI'])) {
      $this->insertEntityNumber($ID, 'LEI', $data['LEI']);
    }
    
    // Insert or update GIIN in oft_entity_numbers
    if(!empty($data['GIIN'])) {
      $this->insertEntityNumber($ID, 'GIIN', $data['GIIN']);
    }
  
    return array(
        'action' => $action,
        'actionStatus' => $actionStatus,
        'id' => $ID,
        'name' => $data['BEDRIJFSNAAM'],
    );
  }
  
  /*
   * Private funtion to map Excel columns to database fields
   * 
   * @param (array) $excel_row The row of fields from Excel
   * 
   * @return (array) The values for the database query
   * 
   * @author RJP Granneman
   */
  private function mappingEntity($userid, $bedrijfsid, $excel_row) {
    // Default Values (not available in Excel)
    $entityData['DELETED'] = 'Nee';
    $entityData['ENTITYNATURAL'] = 'Nee';
    $entityData['ACTIVE'] = 'Ja';
    $entityData['ENDREDEN'] = 'Active';

    /* Field headers as in Excel
      array(29) {
      [0]=> string(7) "Company"
      [1]=> string(4) "IQID"
      [2]=> string(10) "Short code"
      [3]=> string(8) "Known as"
      [4]=> string(6) "Status"
      [5]=> string(10) "Legal form"
      [6]=> string(16) "Original country"
      [7]=> string(8) "Currency"
      [8]=> string(18) "Fiscal year end"
      [9]=> string(16) "Registration N°"
      [10]=> string(13) "Fiscal number"
      [11]=> string(11) "LEI  Number"
      [12]=> string(21) "Date of Incorporation"
      [13]=> string(7) "VAT N°"
      [14]=> string(11) "GIIN Number"
      }
    */

    $entityData["BEDRIJFSNAAM"] = "";
    if(isset($excel_row[0])) {
        $entityData["BEDRIJFSNAAM"] = lb($excel_row[0]);
    }

    //$entity_data["BEDRIJFSNR"] = ""; if(isset($excel_row[1])) { $entity_data["BEDRIJFSNR"] = lb($excel_row[1]); } //WAT IS DIT? VELD AANMAKEN?

    $entityData["BEDRIJFNR"] = "";
    if(isset($excel_row[2])) {
        //WAT IS DIT? VELD AANMAKEN?
        $entityData["BEDRIJFNR"] = lb($excel_row[2]);
    }

    $entityData["HANDELSNAAM"] = "";
    if(isset($excel_row[3])) {
        $entityData["HANDELSNAAM"] = lb($excel_row[3]);
    }

    $entityData["OMSCHRIJVING"] = "";
    if(isset($excel_row[4])) {
      $entityData["OMSCHRIJVING"] = lb($excel_row[4]);
    }

    $entityData["RECHTSVORM"] = "";
    if(isset($excel_row[5]) && !empty($excel_row[5])) {
        $entityData["RECHTSVORM"] = $this->getStamtabel_value("stam_rechtsvormen", $excel_row[5], $bedrijfsid);
    }

    $entityData["LAND"] = "";
    if(isset($excel_row[6])) {
      $entityData["LAND"] = lb($excel_row[6]);
    }

    $entityData["ENDOFBOOKYEAR"] = "";
    if(!empty($excel_row[8])) {
        // Get last day of given month.
        $endOfBookYear = date("Y-m-t", strtotime('1 ' . $excel_row[8] . date(" Y")));
        $entityData["ENDOFBOOKYEAR"] = $endOfBookYear;
    }

    $entityData["REGISTEREDNUMBER"] = "";
    if(isset($excel_row[9])) {
        //WAT IS DIT? VELD AANMAKEN?
        $entityData["REGISTEREDNUMBER"] = lb($excel_row[9]);
    }

    $entityData["FISCAL"] = "";
    if(isset($excel_row[10])) {
        $entityData["FISCAL"] = lb($excel_row[10]);
    }

    $entityData["LEI"] = "";
    if(isset($excel_row[11])) {
        $entityData["LEI"] = lb($excel_row[11]);
    }

    $entityData["OPRICHTINGSDATUM"] = "";
    if(isset($excel_row[12]) && is_float($excel_row[12])) {
        // Excel date is a float, need to be converted to PHP Date
        $date =  date("Y-m-d", \PHPExcel_Shared_Date::ExcelToPHP($excel_row[12]));
        $entityData["OPRICHTINGSDATUM"] = lb($date);
    }

    // [13] VAT nr is empty in Excel -> what's the difference between Fiscal Number and VAT Number?

    $entityData["VAT"] = "";
    if(isset($excel_row[13])) {
        $entityData["VAT"] = lb($excel_row[13]);
    }

    $entityData["GIIN"] = "";
    if(isset($excel_row[14])) {
        $entityData["GIIN"] = lb($excel_row[14]);
    }

    return $entityData;
  }
  
  /*
   * 
   * 
   */
  private function importAddress($userid, $bedrijfsid, $excel, $useHeaderLine, $type = 'Registered') {
    
    global $pdo;
    
    // Variables
    $content = '<h2>' . $type . ' address</h2><table class="imported-entities"><thead><tr><th>ID</th><th>Address</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $addressData = array();
      $excelHeaders = array();
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
        $i++;
        $excelHeaders = $excel_row;
        continue;
      }
      
      // Map Excel fields to database fields
      $address = array();
      # Address
      if(isset($excel_row[6]) && !empty($excel_row[6])) {
        $address[] = $excel_row[6];
      }
      # Address2
      if(isset($excel_row[7]) && !empty($excel_row[7])) {
        $address[] = $excel_row[7];
      }
      # Address3
      if(isset($excel_row[8]) && !empty($excel_row[8])) {
        $address[] = $excel_row[8];
      }
      # Address4
      if(isset($excel_row[9]) && !empty($excel_row[9])) {
        $address[] = $excel_row[9];
      }
      $addressData["ADRES"] = implode(" ", $address);
      
      # City
      $addressData["PLAATS"] = "";
      if(isset($excel_row[10])) {
        $addressData["PLAATS"] = $excel_row[10];
      }
      
      # Zipcode
      $addressData["POSTCODE"] = "";
      if(isset($excel_row[11])) {
        $addressData["POSTCODE"] = $excel_row[11];
      }
      
      # Country
      $addressData["LAND"] = "";
      if(isset($excel_row[12])) {
        $addressData["LAND"] = $excel_row[12];
      }
      
      if(isset($excel_row[3]) && !empty($excel_row[3])) {
        $query = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFNR = :shortcode LIMIT 1');
        $query->bindValue('shortcode', $excel_row[3]);
        $query->execute();
        $found = false;
        if($query->rowCount() > 0) {
          $entityRow = $query->fetch(PDO::FETCH_ASSOC);
          $entityId = $entityRow['ID'];
          $addressData['BEDRIJFSID'] = $entityId;
          $found = true;
        }
        
        if($found) {
          $insertResult = $this->insertBedrijfAdres($addressData['BEDRIJFSID'], $addressData, $type);
          $content .= "<tr><td>".$insertResult['id'] . "</td>"
                    . "<td>" . $insertResult['name'] . "</td>"
                    . "<td>" . $insertResult['action'] . "</td>"
                    . "<td>" . ($insertResult['actionStatus'] ? 'success' : 'failed') . "</td></tr>";
        } else {
          $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No entity found by short code") . '</td></tr>';
        }
      } else {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No short code.") . '</td></tr>';
      }
      
      $i++;
      
    } // End while loop
    
    $content .= '</tbody></table>';
    
    return $content;
    
  }
  
  /*
   * Private function to update or insert an Adres in oft_entity_adres
   * 
   * @param $id (int) entity ID
   * @param $data (array) data of entity address
   * 
   * @return (bool)
   * 
   * @author RJP Granneman
   */
  private function insertBedrijfAdres($bedrijfsId, $data, $type = 'Registered') {
      global $pdo;
      
      // To do: check if $id is valid
      
      // Check if it exists
      $query = $pdo->prepare('SELECT ID FROM oft_bedrijf_adres WHERE BEDRIJFSID = :id AND TYPE = :type LIMIT 1');
      $query->bindValue('id', $bedrijfsId);
      $query->bindValue('type', $type);
      $query->execute();

      if ($query->rowCount() > 0) {
        $adres = $query->fetch(PDO::FETCH_ASSOC);
        $queryAdres = $pdo->prepare('UPDATE oft_bedrijf_adres SET
                BEDRIJFSID = :id, 
                UPDATEDATUM = NOW(), 
                DELETED = "Nee", 
                TYPE = :type, 
                ADRES = :adres, 
                POSTCODE = :postcode, 
                PLAATS = :plaats, 
                LAND = :land, 
                STARTDATE = NOW(), 
                STOPDATE = null 
                WHERE id = :adresid;');
        $queryAdres->bindValue('adresid', $adres['ID']);

        $action = 'update';
        $id = $adres['ID'];
      } else {
        // Insert
        $queryAdres = $pdo->prepare('INSERT INTO oft_bedrijf_adres SET 
                BEDRIJFSID = :id, 
                UPDATEDATUM = NOW(),
                DELETED = "Nee",
                TYPE = :type,
                ADRES = :adres,
                POSTCODE = :postcode,
                PLAATS = :plaats,
                LAND = :land,
                STARTDATE = NOW(),
                STOPDATE = null;');

        $action = 'insert';
      }
      
      $queryAdres->bindValue('id', $bedrijfsId);
      $queryAdres->bindValue('type', $type);
      $queryAdres->bindValue('adres', $data['ADRES']);
      $queryAdres->bindValue('postcode', $data['POSTCODE']);
      $queryAdres->bindValue('plaats', $data['PLAATS']);
      $queryAdres->bindValue('land', $data['LAND']);
      
      $actionStatus = $queryAdres->execute();
      if(!$actionStatus) {
        var_dump($pdo->errorInfo());
        exit;
      }
      if($action == 'insert') {
        $id = $pdo->lastInsertId();
      }
      
      return array('id' => $id,
        'name' => $data['ADRES'],
        'action' => $action,
        'actionStatus' => $actionStatus);
  }
  
  /*
   * Private function to insert or update Entity Numbers in oft_entitie_numbers
   * 
   * @param (int) $bedrijfsid The Id of entity the number belongs to
   * @param (string) $type The type of the number
   * @param (string) $value The value of the number
   * 
   * @return (bool)
   * 
   * @author RJP Granneman
   */
  private function insertEntityNumber($bedrijfsid, $type, $value) {
    global $pdo;

    $query = $pdo->prepare('SELECT ID FROM oft_entitie_numbers WHERE type = :type AND BEDRIJFSID = :bedrijfsid LIMIT 1');
    $query->bindValue('type', $type);
    $query->bindValue('bedrijfsid', $bedrijfsid);
    $query->execute();
    
    if($query->rowCount() > 0) {
      // update
      $queryEntitieNumbers = $pdo->prepare('UPDATE oft_entitie_numbers SET
              VALUE = :value, UPDATEDATUM = NOW()
              WHERE TYPE = :type AND BEDRIJFSID = :bedrijfsid
              LIMIT 1');
      $queryEntitieNumbers->bindValue('value', $value);
      $queryEntitieNumbers->bindValue('type', $type);
      $queryEntitieNumbers->bindValue('bedrijfsid', $bedrijfsid);
      

    } else {
      // insert
      $queryEntitieNumbers = $pdo->prepare('INSERT INTO oft_entitie_numbers SET
              TYPE = :type,
              VALUE = :value,
              DELETED = "Nee",
              BEDRIJFSID = :bedrijfsid,
              UPDATEDATUM = NOW()');
      $queryEntitieNumbers->bindValue('value', $value);
      $queryEntitieNumbers->bindValue('type', $type);
      $queryEntitieNumbers->bindValue('bedrijfsid', $bedrijfsid);
      
    }
    
    return $queryEntitieNumbers->execute();
  }
  
  /*
   * Private function to map Excel to Database and return feedback.
   * 
   * @param (int) $userid The Id of current logged in user
   * @param (int) $bedrijfsid The current selected bedrijf Id
   * @param (phpExcel) $excel The excel with correct worksheet active
   * 
   * @return (string) HTML table of imported Excel rows
   * 
   * @author RJP Granneman
   */
  private function importDirectors($userid, $bedrijfsid, $excel, $useHeaderLine) {
    
    // Global Variables
    global $pdo;
    
    // Variables
    $content = '<h2>Directors</h2><table class="imported-entities"><thead><tr><th>ID</th><th>Name</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $importeerdeze = true;
      $directorData = array();
      $excelHeaders = array();
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
          $i++;
          $excelHeaders = $excel_row;
          continue;
      }
      
      // Default values
      $directorData['DELETED'] = 'Nee';
      $directorData['TYPE'] = 'Executive Board';
      $directorData['AUTHORIZATION'] = 'Jointly';
      
      /* array(13) {
       * 0	Company
       * 1	IQId
       * 2	Short code
       * 3	Last name
       * 4	First name
       * 5	Contact Full Name
       * 6	Email
       * 7	Company as director
       * 8	Role
       * 9	Start date
       * 10	End date
       * 11	Board type

       * [0]=> string(7) "Company"
       * [1]=> string(10) "Short code"
       * [2]=> string(8) "Divested"
       * [3]=> string(8) "Known as"
       * [4]=> string(6) "Status"
       * [5]=> string(10) "Legal form"
       * [6]=> string(16) "Registration N°"
       * [7]=> string(16) "Original country"
       * [8]=> string(17) "Contact Full Name"
       * [9]=> string(19) "Company as director"
       * [10]=> string(4) "Role"
       * [11]=> string(10) "Start date"
       * [12]=> string(8) "End date" }
       */
      
      $directorData["COMPANY"] = "";
      if(isset($excel_row[0]) && !empty($excel_row[0])) {
        $directorData["COMPANY"] = $excel_row[0];
      }
      $directorData["FUNCTION"] = "";
      if(isset($excel_row[8]) && !empty($excel_row[8])) {
        $function = str_replace(" DIF", "", $excel_row[8]);
        $directorData["FUNCTION"] = $function;
      }
      $directorData["DATEOFAPPOINTENT"] = "";
      if(isset($excel_row[9]) && is_float($excel_row[9])) {
        // Excel date is a float, need to be converted to PHP Date
        $date =  date("Y-m-d", \PHPExcel_Shared_Date::ExcelToPHP($excel_row[9]));
        $directorData["DATEOFAPPOINTENT"] = lb($date);
      }
      $directorData["ENDATE"] = "";
      if(isset($excel_row[10]) && is_float($excel_row[10])) {
        // Excel date is a float, need to be converted to PHP Date
        $date =  date("Y-m-d", \PHPExcel_Shared_Date::ExcelToPHP($excel_row[10]));
        $directorData["ENDATE"] = lb($date);
      }
      $directorData["AUTHORIZATION"] = "";
      if(isset($excel_row[11]) && !empty($excel_row[11])) {
        
          $options = getListOptions('AUTHORIZATION');
          if(in_array($excel_row[11], $options)) {
            $director = array_search($excel_row[11], $options);
          } else {
            // todo: check for valid values in listoptions
            $director = $excel_row[11];
          }
          $directorData["AUTHORIZATION"] = $director;
      }
      
      // get company id from shortcode
      $entityId = false;
      if(empty($excel_row[2])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No short code.") . '</td></tr>';
        $importeerdeze = false;
      } else {
        $query = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFNR = :shortcode LIMIT 1');
        $query->bindValue('shortcode', $excel_row[2]);
        $query->execute();

        $found = false;
        if($query->rowCount() > 0) {
          $entityRow = $query->fetch(PDO::FETCH_ASSOC);
          $entityId = $entityRow['ID'];
          $directorData['BEDRIJFSID'] = $entityId;
          $found = true;
        }
        if(!$found){
          $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No entity found by short code.") . '</td></tr>';
          $importeerdeze = false;
        }
      }
      
      // Check for user
      if($entityId !== false && isset($excel_row[5]) && !empty($excel_row[5])) {
        $userData['fullName'] = $excel_row[5];
        $userData['firstName'] = $excel_row[4];
        $userData['lastName'] = $excel_row[3];
        $userData['email'] = $excel_row[6];
        $personeelsId = $this->importPersoneel($userid, $entityId, $userData);
        $directorData["USERID"] = $personeelsId;
      } else {
        $personeelsId = false;
        $directorData["USERID"] = "";
      }
      
      // Check for Company as director
      if($entityId !== false && isset($excel_row[7]) && !empty($excel_row[7])) {
        $query = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFSNAAM = :bedrijfsnaam LIMIT 1');
        $query->bindValue('bedrijfsnaam', $excel_row[7]);
        $query->execute();

        $directorData["ADMINID"] = "";
        if ($query->rowCount() > 0) {
          $adminRow = $query->fetch(PDO::FETCH_ASSOC);
          $directorData["ADMINID"] = $adminRow['ID'];
        }
      } else {
        $directorData["ADMINID"] = "";
      }
      
      // execute query
      if ($importeerdeze) {
        // Als er geen foutmeldingen zijn
        $insertResult = $this->insertDirector($userid, $entityId, $directorData, "SHORTCODE", true);
        $content .= "<tr><td>".$insertResult['id'] . "</td>"
                . "<td>" . $insertResult['name'] . "</td>"
                . "<td>" . $insertResult['action'] . "</td>"
                . "<td>" . ($insertResult['actionStatus'] ? 'success' : 'failed') . "</td></tr>";
      }
      
      $i++;
      
    } // End of while loop
    
    $content .= '</tbody></table>';
    
    return $content;
  }
  
  /*
   * 
   * @return (array) The action that was done (string),
   *                 the status of that action (bool),
   *                 the Id of the inserted or updated record (int)
   */
  private function insertDirector($userid, $bedrijfsid, $data = array(), $checkKolom = "ID", $update = false) {
    
    // $checkKolom niet nodig
    // $update wordt niet gebruikt.
    
    global $pdo;
    
    // does it already exists
    if(!empty($data['USERID'])) {
      // Check based on BEDRIJFSID and USERID
      $queryCheck = $pdo->prepare('SELECT ID
              FROM oft_management
              WHERE BEDRIJFSID = :bedrijfsid
              AND USERID = :userid
              LIMIT 1;');
      $queryCheck->bindValue('bedrijfsid', $data['BEDRIJFSID']);
      $queryCheck->bindValue('userid', $data['USERID']);
      
    } elseif (!empty($data['ADMINID'])) {
      // Check based on BEDRIJFSID and ADMINID
      $queryCheck = $pdo->prepare('SELECT ID 
              FROM oft_management 
              WHERE BEDRIJFSID = :bedrijfsid 
              AND ADMINID = :adminid 
              LIMIT 1;');
      $queryCheck->bindValue('bedrijfsid', $data['BEDRIJFSID']);
      $queryCheck->bindValue('adminid', $data['ADMINID']);
    } else {
      $id = "N/A";
      $action = 'no director';
      $actionStatus = false;
    }
    
    if(isset($queryCheck)) {
      $queryCheck->execute();
      
      $found = false;
      foreach ($queryCheck->fetchAll() as $director) {
        // update
        $queryManagement = $pdo->prepare('UPDATE oft_management SET UPDATEDATUM = NOW(), 
                DELETED = :deleted,
                TYPE = :type, 
                ADMINID = :adminid, 
                USERID = :userid, 
                FUNCTION = :function, 
                AUTHORIZATION = :authorization, 
                DATEOFAPPOINTENT = :dateofappointent, 
                ENDATE = :enddate 
                WHERE ID = :directorid;');
        $queryManagement->bindValue('deleted', $data['DELETED']);
        $queryManagement->bindValue('type', $data['TYPE']);
        $queryManagement->bindValue('adminid', $data['ADMINID']);
        $queryManagement->bindValue('userid', $data['USERID']);
        $queryManagement->bindValue('function', $data['FUNCTION']);
        $queryManagement->bindValue('authorization', $data['AUTHORIZATION']);
        $queryManagement->bindValue('dateofappointent', $data['DATEOFAPPOINTENT']);
        $queryManagement->bindValue('enddate', $data['ENDATE']);
        $queryManagement->bindValue('directorid', $director['ID']);
        
        $id = $director['ID'];
        $actionStatus = $queryManagement->execute();
        $action = 'update';
        $found = true;
        if($actionStatus) { audittrailBatch($userid, $bedrijfsid, 'oft_management', $id, $data); }
      }
      
      if(!$found) {
        // insert
        $queryManagement = $pdo->prepare('INSERT INTO oft_management SET UPDATEDATUM = NOW(), 
                BEDRIJFSID = :bedrijfsid, 
                DELETED = :deleted, 
                TYPE = :type, 
                ADMINID = :adminid, 
                USERID = :userid, 
                FUNCTION = :function, 
                AUTHORIZATION = :authorization, 
                DATEOFAPPOINTENT = :dateofappointent, 
                ENDATE = :enddate;');
        $queryManagement->bindValue('bedrijfsid', $data['BEDRIJFSID']);
        $queryManagement->bindValue('deleted', $data['DELETED']);
        $queryManagement->bindValue('type', $data['TYPE']);
        $queryManagement->bindValue('adminid', $data['ADMINID']);
        $queryManagement->bindValue('userid', $data['USERID']);
        $queryManagement->bindValue('function', $data['FUNCTION']);
        $queryManagement->bindValue('authorization', $data['AUTHORIZATION']);
        $queryManagement->bindValue('dateofappointent', $data['DATEOFAPPOINTENT']);
        $queryManagement->bindValue('enddate', $data['ENDATE']);
        
        $actionStatus = $queryManagement->execute();
        $id = getLastID('oft_management');
        $action = 'insert';
        if($actionStatus) {
            auditaction($userid, $bedrijfsid, 'oft_management', $id, 'Insert'); 
        }
      }
    }
  
    return array('id' => $id,
        'name' => $data['COMPANY'],
        'action' => $action,
        'actionStatus' => $actionStatus);
  }
  
  /*
   * Private function to set compliance settings of a company linked by shortcode
   * 
   * @param (int) $userid The Id of current logged in user
   * @param (int) $bedrijfsid The current selected bedrijf Id
   * @param (phpExcel) $excel The excel with correct worksheet active
   * @param (bool) $useHeaderLine Whether Excel Handler used the headerline
   * 
   * @return (string) HTML table of imported Excel rows
   * 
   * @author RJP Granneman
   */
  private function importComplianceSettings($userid, $bedrijfsid, $excel, $useHeaderLine) {
    
    // Global Variables
    global $pdo;
    
    // Variables
    $content = '<h2>Compliance settings</h2><table class="imported-entities"><thead><tr><th>ID</th><th>Name</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $importeerdeze = true;
      $msg = "";
      $complianceData = array();
      $excelHeaders = array();
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
          $i++;
          $excelHeaders = $excel_row;
          continue;
      }
      
      /*
       * 0 Company name
       * 1 Shortcode
       * 2 First year of monitoring
       * 3 Tax months
       * 4 Tax days
       * 5 Annual account months
       * 6 Annual account days
       */
      
      // 2 First year of monitoring
      $complianceData['FIRSTBOOKYEAR'] = "";
      if(isset($excel_row[2]) && !empty($excel_row[2])) {
        if(preg_match("/^[0-9]{4}$/", $excel_row[2])) {
          $complianceData['FIRSTBOOKYEAR'] = $excel_row[2] . '-1-1';
        } else {
          $importeerdeze = false;
          $msg = 'First year of monitoring not in format YYYY';
        }
      }
      
      // 3 Tax months
      $complianceData['TAXPERIODMONTH'] = "";
      if(isset($excel_row[3]) && !empty($excel_row[3])) {
        $complianceData['TAXPERIODMONTH'] = $excel_row[3];
      }
      
      // 4 Tax days
      $complianceData['TAXPERIODDAY'] = "";
      if(isset($excel_row[4]) && !empty($excel_row[4])) {
        $complianceData['TAXPERIODDAY'] = $excel_row[4];
      }
      
      // 5 Annual account months
      $complianceData['ANAPERIODMONTH'] = "";
      if(isset($excel_row[5]) && !empty($excel_row[5])) {
        $complianceData['ANAPERIODMONTH'] = $excel_row[5];
      }
      
      // 5 Annual account days
      $complianceData['ANAPERIODDAY'] = "";
      if(isset($excel_row[6]) && !empty($excel_row[6])) {
        $complianceData['ANAPERIODDAY'] = $excel_row[6];
      }
      
      $queryEntity = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFNR = :shortcode LIMIT 1');
      $queryEntity->bindValue('shortcode', $excel_row[1]);
      $queryEntity->execute();
      
      if($queryEntity->rowCount() > 0) {
        $entityRow = $queryEntity->fetch();
        $entityId = $entityRow['ID'];
        $complianceData['BEDRIJFSID'] = $entityId;
        $found = true;
      } else {
        $found = false;
        $msg = "No entity found by short code.";
      }

      if($found && $importeerdeze) {
        $query = $pdo->prepare("UPDATE bedrijf SET FIRSTBOOKYEAR = :firstbookyear, "
                . "TAXPERIODMONTH = :taxperiodmonth, TAXPERIODDAY = :taxperiodday, "
                . "ANAPERIODMONTH = :anaperiodmonth, ANAPERIODDAY = :anaperiodday "
                . "WHERE ID = :id");
        $query->bindValue('firstbookyear', $complianceData['FIRSTBOOKYEAR']);
        $query->bindValue('taxperiodmonth', $complianceData['TAXPERIODMONTH']);
        $query->bindValue('taxperiodday', $complianceData['TAXPERIODDAY']);
        $query->bindValue('anaperiodmonth', $complianceData['ANAPERIODMONTH']);
        $query->bindValue('anaperiodday', $complianceData['ANAPERIODDAY']);
        $query->bindValue('id', $complianceData['BEDRIJFSID']);
        $actionstatus = $query->execute();
        $content .= "<tr><td>".$complianceData['BEDRIJFSID'] . "</td>"
                  . "<td>" . $excel_row[0] . "</td>"
                  . "<td>update</td>"
                  . "<td>" . ($actionstatus ? 'success' : 'failed') . "</td></tr>";
      } else {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl($msg) . '</td></tr>';
        $importeerdeze = false;
      }
      
      $i++;
      
    } // End of while loop
    
    return $content . '</tbody></table>';
    
  }
  
  
  
  /*
   * Private function to check if person exists, if not insert into personeel
   * 
   * @param ()
   * @param (int) $bedriijfsid The entity id the user belongs to
   * @param (array) $data Person's data to check for/add in table personeel
   */
  private function importPersoneel($userid, $bedrijfsid, $data) {
    
    // Variables
    global $pdo;
    $found = false;
    
    // Check for existing User by E-mail
    $queryEmail = $pdo->prepare('SELECT ID FROM personeel WHERE EMAIL = :email LIMIT 1');
    $queryEmail->bindValue('email', $data['email']);
    $queryEmail->execute();
    
    if($queryEmail->rowCount() > 0) {
      $user = $queryEmail->fetch(PDO::FETCH_ASSOC);
      $found = true;
      $id = $user['ID'];
    }
    if (!$found) {
        // Check for existing User by Full Name
        $queryName = $pdo->prepare('SELECT ID 
                FROM personeel 
                WHERE CONCAT_WS(" ", VOORNAAM, TUSSENVOEGSEL, ACHTERNAAM) LIKE :name
                LIMIT 1');
        $queryName->bindValue('name', '%' . $data['fullName']);
        $queryName->execute();
        foreach ($queryName->fetchAll() as $user) {
            $found = true;
            $id = $user['ID'];
        }
    }
    
    
    if(!$found){
      // Add person
      $query = $pdo->prepare('INSERT INTO personeel SET 
              DELETED = "Nee", 
              VOORNAAM = :voornaam, 
              ACHTERNAAM = :achternaam, 
              BEDRIJFSID = :bedrijfsid, 
              EMAIL = :email, 
              UPDATEDATUM = NOW()');
      $query->bindValue('voornaam', $data['lastName']);
      $query->bindValue('achternaam', $data['lastName']);
      $query->bindValue('bedrijfsid', $bedrijfsid);
      $query->bindValue('email', $data['email']);
      $query->execute();

      $id = $pdo->lastInsertId();
    }
    return $id;
  }
  
  /*
   * Private function to map Excel to Database and return feedback.
   * 
   * @param (int) $userid The Id of current logged in user
   * @param (int) $bedrijfsid The current selected bedrijf Id
   * @param (phpExcel) $excel The excel with correct worksheet active
   * 
   * @return (string) HTML table of imported Excel rows
   * 
   * @author Bob Salm
   */
  private function importProviders($userid, $bedrijfsid, $excel, $useHeaderLine) {
    // Variables
    $content = '<h2>Providers</h2>';
    $content .= '<table class="imported-providers"><thead><tr><th>ID</th><th>Name</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $importeerdeze = true;
      $providerData = array();
      $excelHeaders = array();
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
          $i++;
          $excelHeaders = $excel_row;
          continue;
      }
      
      // Default values
      $providerData['DELETED'] = 'Nee';

      $providerData["COMPANY"] = "";
      if(isset($excel_row[0]) && !empty($excel_row[0])) {
        $providerData["COMPANY"] = $excel_row[0];
      }

      if(isset($excel_row[8]) && !empty($excel_row[8])) {
        $providerData["TYPE"] = $excel_row[7];
        $providerData["NAAM"] = $excel_row[8];
      }

      // get company id from shortcode
      $entityId = false;
      if(empty($excel_row[8])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No provider.") . '</td></tr>';
        $importeerdeze = false;
      } elseif (empty($excel_row[4])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No short code.") . '</td></tr>';
        $importeerdeze = false;
      } else {
        global $pdo;
        $queryEntity = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFNR = :shortcode LIMIT 1');
        $queryEntity->bindValue('shortcode', $excel_row[4]);
        $queryEntity->execute();

        $found = false;
        if($queryEntity->rowCount() > 0) {
          $entityRow = $queryEntity->fetch();
          $entityId = $entityRow['ID'];
          $providerData['BEDRIJFSID'] = $entityId;
          $found = true;
        }
        
        if(!$found) {
          $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No entity found by short code.") . '</td></tr>';
          $importeerdeze = false;
        }
      }

      // execute query
      if ($importeerdeze) {
        
        // Als er geen foutmeldingen zijn
        if(isset($providerData["TYPE"])) {
          $insertResult = $this->insertProvider($userid, $entityId, $providerData, "SHORTCODE", true);
          $content .= "<tr><td>".$insertResult['id'] . "</td>"
                  . "<td>" . $insertResult['name'] . "</td>"
                  . "<td>" . $insertResult['action'] . "</td>"
                  . "<td>" . ($insertResult['actionStatus'] ? 'success' : 'failed') . "</td></tr>";
        }
      }
      
      $i++;
      
    } // End of while loop
    
    $content .= '</tbody></table>';
    
    return $content;
  }
  
  /*
   * Private function to insert service prividers of a company
   * 
   * @param@param $userid (int) Id of logged in user
   * @param $bedrijfsid (int) Id of selected company
   * @param $data (array) The data of the provider
   * @param $checkKolom (string) The DB field to check if entity already exists (not used)
   * @param $update (bool) Should an existing record be updated (not used)
   * 
   * @return (array) The action that was done (string),
   *                 the status of that action (bool),
   *                 the Id of the inserted or updated record (int)
   * @author Bob Salm
   */
  private function insertProvider($userid, $bedrijfsid, $data = array(), $checkKolom = "ID", $update = false) {
    
    // $checkKolom niet nodig
    // $update wordt niet gebruikt.
    global $pdo;
    
    $query = $pdo->prepare('SELECT ID FROM oft_providers WHERE BEDRIJFSID = :bedrijfsid AND TYPE = :type;');
    $query->bindValue('bedrijfsid', $data['BEDRIJFSID']);
    $query->bindValue('type', $data['TYPE']);
    $query->execute();

    $found = false;
    foreach ($query->fetchAll() as $provider) {
      $found = true;
      // update
      $updateQuery = $pdo->prepare('update oft_providers SET UPDATEDATUM = NOW(),
              BEDRIJFSID = :bedrijfsid, 
              DELETED = :deleted, 
              TYPE = :type, 
              NAAM = :naam
              WHERE ID = :providerid;');
      $updateQuery->bindValue('bedrijfsid', $data['BEDRIJFSID']);
      $updateQuery->bindValue('type', $data['TYPE']);
      $updateQuery->bindValue('deleted', $data['DELETED']);
      $updateQuery->bindValue('naam', $data['NAAM']);
      $updateQuery->bindValue('providerid', $provider['ID']);
      
      $actionStatus = $updateQuery->execute();
      $id = $provider['ID'];
      $action = 'update';
      if($actionStatus) {
          audittrailBatch($userid, $bedrijfsid, 'oft_providers', $id, $data);
      }
    }
    if(!$found) {
      // insert
      $insertQuery = $pdo->prepare('INSERT INTO oft_providers SET UPDATEDATUM = NOW(),
              BEDRIJFSID = :bedrijfsid, 
              DELETED = :deleted,
              TYPE = :type,
              NAAM = :naam;');
      $insertQuery->bindValue('bedrijfsid', $data['BEDRIJFSID']);
      $insertQuery->bindValue('type', $data['TYPE']);
      $insertQuery->bindValue('deleted', $data['DELETED']);
      $insertQuery->bindValue('naam', $data['NAAM']);
      
      $actionStatus = $insertQuery->execute();
      $id = $pdo->lastInsertID();
      $action = 'insert';
      if($actionStatus) {
          auditaction($userid, $bedrijfsid, 'oft_providers', $id, 'Insert');
      }
    }

    // Is there already a database record with checkValue
    return array('id' => $id,
        'name' => $data['NAAM'],
        'action' => $action,
        'actionStatus' => $actionStatus);
    
  }
  
  /*
   * Private function to map Excel to Database and return feedback.
   * 
   * @param (int) $userid The Id of current logged in user
   * @param (int) $bedrijfsid The current selected bedrijf Id
   * @param (phpExcel) $excel The excel with correct worksheet active
   * 
   * @return (string) HTML table of imported Excel rows
   * 
   * @author Bob Salm
   */
  private function importAgendaItems($userid, $bedrijfsid, $excel, $useHeaderLine) {
    // Variables
    $content = '<h2>Agenda</h2>';
    $content .= '<table class="imported-providers"><thead><tr><th>ID</th><th>Name</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $importeerdeze = true;
      $providerData1 = array();
      $providerData2 = array();
      $excelHeaders = array();
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
          $i++;
          $excelHeaders = $excel_row;
          continue;
      }
      
      // Default values
      $import['DELETED'] = 'Nee';
      $import['USERID'] = $userid;

      //Date	Alert name	Entity	User
      $import["DATUM"] = "";
      if(isset($excel_row[0]) && !empty($excel_row[0])) {
        $import["DATUM"] = $excel_row[0];
      }
      
      $import["ITEM"] = "";
      if(isset($excel_row[1]) && !empty($excel_row[1])) {
        $import["ITEM"] = $excel_row[1];
      }
      
      $import["BEDRIJFSID"] = "";
      if(isset($excel_row[2]) && !empty($excel_row[2])) {
        $import["BEDRIJFSID"] = $excel_row[2];
      }

      $import["ITEMOFUSERID"] = "";
      if(isset($excel_row[3]) && !empty($excel_row[3])) {
        $import["ITEMOFUSERID"] = $excel_row[3];
      }

      // get company id from shortcode
      $entityId = false;
      if(empty($import["ITEM"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No alert name.") . '</td></tr>';
        $importeerdeze = false;
      } elseif (empty($import["ITEM"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No date.") . '</td></tr>';
        $importeerdeze = false;
      } elseif (empty($import["ITEMOFUSERID"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No user.") . '</td></tr>';
        $importeerdeze = false;
      } elseif (empty($import["BEDRIJFSID"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No entity.") . '</td></tr>';
        $importeerdeze = false;
      } else {
        global $pdo;
        
        //find entity
        $queryEntity = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFSNAAM = :BEDRIJFSNAAM LIMIT 1');
        $queryEntity->bindValue('BEDRIJFSNAAM', $import["BEDRIJFSID"]);
        $queryEntity->execute();

        $found = false;
        foreach ($queryEntity->fetchAll() as $entityRow) {
          $entityId = $entityRow['ID'];
          $import['BEDRIJFSID'] = $entityId;
          $found = true;
        } 
        
        if(!$found) {
          $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No entity found by name.") . '</td></tr>';
          $importeerdeze = false;
        }
        
        //Find user
        $query = $pdo->prepare('SELECT ID FROM personeel WHERE ACHTERNAAM = :ACHTERNAAM LIMIT 1');
        $query->bindValue('ACHTERNAAM', $import["ITEMOFUSERID"]);
        $query->execute();

        $found = false;
        foreach ($query->fetchAll() as $row) {
          $personeelsLidId = $row['ID'];
          $import['ITEMOFUSERID'] = $personeelsLidId;
          $found = true;
        } 
        
        if(!$found) {
          $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No user not found.") . '</td></tr>';
          $importeerdeze = false;
        }
      }

      // execute query
      if ($importeerdeze) {
        // Als er geen foutmeldingen zijn
        $insertResult = $this->insertAgendaItem($userid, $entityId, $import, "ID", true);
        $content .= "<tr><td>".$insertResult['id'] . "</td>"
                . "<td>" . $insertResult['name'] . "</td>"
                . "<td>" . $insertResult['action'] . "</td>"
                . "<td>" . ($insertResult['actionStatus'] ? 'success' : 'failed') . "</td></tr>";
      }
      
      $i++;
      
    } // End of while loop
    
    $content .= '</tbody></table>';
    
    return $content;
  }
  
  /*
   * Private function to insert alerts of a company
   * 
   * @param $userid (int) Id of logged in user
   * @param $bedrijfsid (int) Id of selected company
   * @param $data (array) The data of the provider
   * @param $checkKolom (string) The DB field to check if entity already exists (not used)
   * @param $update (bool) Should an existing record be updated (not used)
   * 
   * @return (array) The action that was done (string),
   *                 the status of that action (bool),
   *                 the Id of the inserted or updated record (int)
   * @author Bob Salm
   */
  private function insertAgendaItem($userid, $bedrijfsid, $data = array(), $checkKolom = "ID", $update = false) {
    
    // $checkKolom niet nodig
    // $update wordt niet gebruikt.
    global $pdo;
    
    $query = $pdo->prepare('SELECT ID FROM alerts WHERE BEDRIJFSID = :BEDRIJFSID AND DATUM = :DATUM AND ITEM = :ITEM;');
    $query->bindValue('BEDRIJFSID', $data['BEDRIJFSID']);
    $query->bindValue('DATUM', $data['DATUM']);
    $query->bindValue('ITEM', $data['ITEM']);
    $query->execute();

    $found = false;
    foreach ($query->fetchAll() as $alert) {
      $found = true;
      // update
      $updateQuery = $pdo->prepare('update alerts SET UPDATEDATUM = NOW(),
              USERID = :USERID,
              BEDRIJFSID = :BEDRIJFSID,
              DELETED = :DELETED,
              DATUM = :DATUM,
              TIJD = :TIJD,
              ITEM = :ITEM,
              ITEMOFUSERID = :ITEMOFUSERID
              WHERE ID = :ID;');
      $updateQuery->bindValue('USERID', $data['USERID']);
      $updateQuery->bindValue('BEDRIJFSID', $data['BEDRIJFSID']);
      $updateQuery->bindValue('DELETED', $data['DELETED']);
      $updateQuery->bindValue('DATUM', $data['DATUM']);
      $updateQuery->bindValue('TIJD', $data['TIJD']);
      $updateQuery->bindValue('ITEM', $data['ITEM']);
      $updateQuery->bindValue('ITEMOFUSERID', $data['ITEMOFUSERID']);
      $updateQuery->bindValue('ID', $alert['ID']);
      
      $actionStatus = $updateQuery->execute();
      $id = $alert['ID'];
      $action = 'update';
      if($actionStatus) { audittrailBatch($userid, $bedrijfsid, 'alerts', $id, $data); }
    } 
    if(!$found) {
      // insert
      $insertQuery = $pdo->prepare('INSERT INTO alerts SET UPDATEDATUM = NOW(),
              USERID = :USERID,
              BEDRIJFSID = :BEDRIJFSID,
              DELETED = :DELETED,
              DATUM = :DATUM,
              TIJD = :TIJD,
              ITEM = :ITEM,
              ITEMOFUSERID = :ITEMOFUSERID');
      $insertQuery->bindValue('USERID', $data['USERID']);
      $insertQuery->bindValue('BEDRIJFSID', $data['BEDRIJFSID']);
      $insertQuery->bindValue('DELETED', $data['DELETED']);
      $insertQuery->bindValue('DATUM', $data['DATUM']);
      $insertQuery->bindValue('TIJD', '09:00:00');
      $insertQuery->bindValue('ITEM', $data['ITEM']);
      $insertQuery->bindValue('ITEMOFUSERID', $data['ITEMOFUSERID']);

      $actionStatus = $insertQuery->execute();
      $id = $pdo->lastInsertID();
      $action = 'insert';
      if($actionStatus) {
          auditaction($userid, $bedrijfsid, 'alerts', $id, 'Insert');
      }
    }

    // Is there already a database record with checkValue
    return array('id' => $id,
                 'name' => $data['ITEM'],
                 'action' => $action,
                 'actionStatus' => $actionStatus);
    
  }
  
  /*
   * Private function to map Excel to Database and return feedback.
   * 
   * @param (int) $userid The Id of current logged in user
   * @param (int) $bedrijfsid The current selected bedrijf Id
   * @param (phpExcel) $excel The excel with correct worksheet active
   * 
   * @return (string) HTML table of imported Excel rows
   * 
   * @author Bob Salm
   */
  private function importUsers($userid, $bedrijfsid, $excel, $useHeaderLine) {
    // Variables
    $content = '<h2>Users</h2>';
    $content .= '<table class="imported-providers"><thead><tr><th>ID</th><th>Name</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $importeerdeze = true;
      $providerData1 = array();
      $providerData2 = array();
      $excelHeaders = array();
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
          $i++;
          $excelHeaders = $excel_row;
          continue;
      }
      
      // Default values
      $import['DELETED'] = 'Nee';
      $import['BEDRIJFSID'] = $bedrijfsid;

      //First name	Last name	Contact Full Name	Email	Contact type	Phone

      $import["VOORNAAM"] = "";
      if(isset($excel_row[0]) && !empty($excel_row[0])) {
        $import["VOORNAAM"] = $excel_row[0];
      }
      
      $import["ACHTERNAAM"] = "";
      if(isset($excel_row[1]) && !empty($excel_row[1])) {
        $import["ACHTERNAAM"] = $excel_row[1];
      }
      
      $import["EMAIL"] = "";
      if(isset($excel_row[3]) && !empty($excel_row[3])) {
        $import["EMAIL"] = $excel_row[3];
      }

      $import["MOB"] = "";
      if(isset($excel_row[4]) && !empty($excel_row[4])) {
        $import["MOB"] = $excel_row[4];
      }

      // get company id from shortcode
      $entityId = false;
      if(empty($import["VOORNAAM"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No first name.") . '</td></tr>';
        $importeerdeze = false;
      } elseif (empty($import["ACHTERNAAM"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No last name.") . '</td></tr>';
        $importeerdeze = false;
      } elseif (empty($import["EMAIL"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No e-mail.") . '</td></tr>';
        $importeerdeze = false;
      } elseif (empty($import["MOB"])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No phone.") . '</td></tr>';
        $importeerdeze = false;
      }

      // execute query
      if ($importeerdeze) {
        // Als er geen foutmeldingen zijn
        $insertResult = $this->insertUsers($userid, $entityId, $import, "ID", true);
        $content .= "<tr><td>".$insertResult['id'] . "</td>"
                . "<td>" . $insertResult['name'] . "</td>"
                . "<td>" . $insertResult['action'] . "</td>"
                . "<td>" . ($insertResult['actionStatus'] ? 'success' : 'failed') . "</td></tr>";
      }
      
      $i++;
      
    } // End of while loop
    
    $content .= '</tbody></table>';
    
    return $content;
  }
  
  /*
   * Private function to insert alerts of a company
   * 
   * @param@param $userid (int) Id of logged in user
   * @param $bedrijfsid (int) Id of selected company
   * @param $data (array) The data of the provider
   * @param $checkKolom (string) The DB field to check if entity already exists (not used)
   * @param $update (bool) Should an existing record be updated (not used)
   * 
   * @return (array) The action that was done (string),
   *                 the status of that action (bool),
   *                 the Id of the inserted or updated record (int)
   * @author Bob Salm
   */
  private function insertUsers($userid, $bedrijfsid, $data = array(), $checkKolom = "ID", $update = false) {
    
    // $checkKolom niet nodig
    // $update wordt niet gebruikt.
    global $pdo;

    $query = $pdo->prepare('SELECT ID FROM personeel WHERE ACHTERNAAM = :ACHTERNAAM AND EMAIL = :EMAIL;');
    $query->bindValue('ACHTERNAAM', $data['ACHTERNAAM']);
    $query->bindValue('EMAIL', $data['EMAIL']);
    $query->execute();

    $found = false;
    foreach ($query->fetchAll() as $personeel) {
      $found = true;
      // update
      $updateQuery = $pdo->prepare('update personeel SET UPDATEDATUM = NOW(),
              BEDRIJFSID = :BEDRIJFSID,
              DELETED = :DELETED,
              MANVROUW = \"Man\",
              VOORNAAM = :VOORNAAM,
              ACHTERNAAM = :ACHTERNAAM,
              EMAIL = :EMAIL,
              MOB = :MOB
              WHERE ID = :ID;');
      $updateQuery->bindValue('BEDRIJFSID', $data['BEDRIJFSID']);
      $updateQuery->bindValue('DELETED', $data['DELETED']);
      $updateQuery->bindValue('VOORNAAM', $data['VOORNAAM']);
      $updateQuery->bindValue('ACHTERNAAM', $data['ACHTERNAAM']);
      $updateQuery->bindValue('EMAIL', $data['EMAIL']);
      $updateQuery->bindValue('MOB', $data['MOB']);
      $updateQuery->bindValue('ID', $personeel['ID']);
      
      $actionStatus = $updateQuery->execute();
      $id = $personeel['ID'];
      $action = 'update';
      if($actionStatus) { audittrailBatch($userid, $bedrijfsid, 'personeel', $id, $data); }
    }
    if(!$found) {
      // insert
      $insertQuery = $pdo->prepare('INSERT INTO personeel SET UPDATEDATUM = NOW(),
                                    BEDRIJFSID = :BEDRIJFSID,
                                    DELETED = :DELETED,
                                    VOORNAAM = :VOORNAAM,
                                    ACHTERNAAM = :ACHTERNAAM,
                                    EMAIL = :EMAIL,
                                    MOB = :MOB');
      $insertQuery->bindValue('BEDRIJFSID', $data['BEDRIJFSID']);
      $insertQuery->bindValue('DELETED', $data['DELETED']);
      $insertQuery->bindValue('VOORNAAM', $data['VOORNAAM']);
      $insertQuery->bindValue('ACHTERNAAM', $data['ACHTERNAAM']);
      $insertQuery->bindValue('EMAIL', $data['EMAIL']);
      $insertQuery->bindValue('MOB', $data['MOB']);

      $actionStatus = $insertQuery->execute();
      $id = $pdo->lastInsertID();
      $action = 'insert';
      if($actionStatus) {
          auditaction($userid, $bedrijfsid, 'personeel', $id, 'Insert');
      }
      else {
        echo "<br/>".$insertQuery->debugDumpParams();
      }
    }

    // Is there already a database record with checkValue
    return array('id' => $id,
                 'name' => $data['ACHTERNAAM'],
                 'action' => $action,
                 'actionStatus' => $actionStatus);
    
  }

  /*
   * Private function to map Excel to Database and return feedback.
   * 
   * @param (int) $userid The Id of current logged in user
   * @param (int) $bedrijfsid The current selected bedrijf Id
   * @param (phpExcel) $excel The excel with correct worksheet active
   * 
   * @return (string) HTML table of imported Excel rows
   * 
   * @author Bob Salm
   */
  private function importUserRights($userid, $bedrijfsid, $excel, $useHeaderLine) {
    
    global $pdo;
    
    // Variables
    $content = '<h2>Users rights</h2>';
    $content .= '<table class=""><thead><tr><th>User ID</th><th>Company ID</th><th>Action</th><th>Status</th></tr></thead><tbody>';
    
    // Start looping excel rows
    $i = 1;
    while($excel->has_next()) {
      
      // Variables
      $importeerdeze = true;
      $rightsData = array();
      $excelHeaders = array();
      $excel_row = $excel->read();
      
      // Skip headerline
      if($i==1 && !$useHeaderLine) {
          $i++;
          $excelHeaders = $excel_row;
          continue;
      }
      
      /* Structure of worksheet
       * 0. e-mail address
       * 1. shortcode
       * 2. legal
       * 3. compliance
       * 4. boardpack
       * 5. finance
       * 6. human resource
       * 7. contracts
       * 8. notifications
       */
      
      // User (USERID)
      if(isset($excel_row[0]) && !empty($excel_row[0])) {
        $queryEmail = $pdo->prepare('SELECT ID FROM personeel WHERE EMAIL = :email LIMIT 1');
        $queryEmail->bindValue('email', $excel_row[0]);
        $queryEmail->execute();
        if($queryEmail->rowCount() > 0) {
          $email = $queryEmail->fetch(PDO::FETCH_ASSOC);
          $rightsData["USERID"] = $email['ID'];
        } else {
          $importeerdeze = false;
          $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No e-mail address") . '</td></tr>';
        }
      } else {
        $importeerdeze = false;
      }
      
      // Company (BEDRIJFSID)
      $entityId = false;
      if(empty($excel_row[1])) {
        $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No short code.") . '</td></tr>';
        $importeerdeze = false;
      } else {
        $queryCompany = $pdo->prepare('SELECT ID FROM bedrijf WHERE BEDRIJFNR = :shortcode LIMIT 1');
        $queryCompany->bindValue('shortcode', $excel_row[1]);
        $queryCompany->execute();

        if($queryCompany->rowCount() > 0) {
          $entityRow = $queryCompany->fetch(PDO::FETCH_ASSOC);
          $entityId = $entityRow['ID'];
          $rightsData['BEDRIJFSID'] = $entityId;
          $found = true;
        } else {
          $found = false;
          $content .= '<tr><td colspan="4">' . $i . ': ' . tl("No company found by short code.") . '</td></tr>';
        }
      }
      
      // legal
      $rightsData["LEGAL"] = "";
      if(isset($excel_row[2]) && !empty($excel_row[2])) {
        $rightsData["LEGAL"] = $excel_row[2];
      }
      // compliance
      $rightsData["COMPLIANCE"] = "";
      if(isset($excel_row[3]) && !empty($excel_row[3])) {
        $rightsData["COMPLIANCE"] = $excel_row[3];
      }
      // boardpack
      $rightsData["BOARDPACK"] = "";
      if(isset($excel_row[4]) && !empty($excel_row[4])) {
        $rightsData["BOARDPACK"] = $excel_row[4];
      }
      // finance
      $rightsData["FINANCE"] = "";
      if(isset($excel_row[5]) && !empty($excel_row[5])) {
        $rightsData["FINANCE"] = $excel_row[5];
      }
      // human resource
      $rightsData["HR"] = "";
      if(isset($excel_row[6]) && !empty($excel_row[6])) {
        $rightsData["HR"] = $excel_row[6];
      }
      // human resource
      $rightsData["CONTRACTS"] = "";
      if(isset($excel_row[7]) && !empty($excel_row[7])) {
        $rightsData["CONTRACTS"] = $excel_row[7];
      }
      // notification
      $rightsData["MAILNOTIFCATIONS"] = "";
      if(isset($excel_row[8]) && !empty($excel_row[8])) {
        $rightsData["MAILNOTIFCATIONS"] = $excel_row[8];
      }
      
      if($found && $importeerdeze) {
        // check for existing entries
        $queryCheck = $pdo->prepare("SELECT * FROM oft_rechten_tot WHERE USERID = :userid AND BEDRIJFSID = :bedrijfsid LIMIT 1");
        $queryCheck->bindValue('userid', $rightsData['USERID']);
        $queryCheck->bindValue('bedrijfsid', $rightsData['BEDRIJFSID']);
        $queryCheck->execute();
        if($queryCheck->rowCount() > 0) {
          // bestaal al
          $action = 'update';
          $query = $pdo->prepare("UPDATE oft_rechten_tot SET "
                  . "LEGAL = :legal, "
                  . "COMPLIANCE = :compliance, "
                  . "BOARDPACK = :boardpack, "
                  . "FINANCE = :finance, "
                  . "HR = :hr, "
                  . "CONTRACTS = :contracts, "
                  . "MAILNOTIFICATIONS = :mailnotifications "
                  . "WHERE USERID = :userid AND BEDRIJFSID = :bedrijfsid LIMIT 1;");
          $query->bindvalue('userid', $rightsData['USERID']);
          $query->bindvalue('bedrijfsid', $rightsData['BEDRIJFSID']);
          $query->bindvalue('legal', $rightsData['LEGAL']);
          $query->bindvalue('compliance', $rightsData['COMPLIANCE']);
          $query->bindvalue('boardpack', $rightsData['BOARDPACK']);
          $query->bindvalue('finance', $rightsData['FINANCE']);
          $query->bindvalue('hr', $rightsData['HR']);
          $query->bindvalue('contracts', $rightsData['CONTRACTS']);
          $query->bindvalue('mailnotifications', $rightsData['MAILNOTIFCATIONS']);
          $actionstatus = $query->execute();
        } else {
          // nieuwe aanmaken
          $action = 'insert';
          $query = $pdo->prepare("INSERT INTO oft_rechten_tot SET "
                  . "USERID = :userid, "
                  . "BEDRIJFSID = :bedrijfsid, "
                  . "LEGAL = :legal, "
                  . "COMPLIANCE = :compliance, "
                  . "BOARDPACK = :boardpack, "
                  . "FINANCE = :finance, "
                  . "HR = :hr, "
                  . "CONTRACTS = :contracts, "
                  . "MAILNOTIFICATIONS = :mailnotifications;");
          $query->bindvalue('userid', $rightsData['USERID']);
          $query->bindvalue('bedrijfsid', $rightsData['BEDRIJFSID']);
          $query->bindvalue('legal', $rightsData['LEGAL']);
          $query->bindvalue('compliance', $rightsData['COMPLIANCE']);
          $query->bindvalue('boardpack', $rightsData['BOARDPACK']);
          $query->bindvalue('finance', $rightsData['FINANCE']);
          $query->bindvalue('hr', $rightsData['HR']);
          $query->bindvalue('contracts', $rightsData['CONTRACTS']);
          $query->bindvalue('mailnotifications', $rightsData['MAILNOTIFCATIONS']);
          $actionstatus = $query->execute();
        }
        $content .= "<tr><td>" . $rightsData['USERID'] . "</td>"
                  . "<td>" . $rightsData['BEDRIJFSID'] . "</td>"
                  . "<td>" . $action . "</td>"
                  . "<td>" . ($actionstatus ? 'success' : 'failed') . "</td></tr>";
      }
      
      
    } // End while
    
    return $content;
  }
  
  /*
   * Private function to check the file extension.
   * 
   * @param (string) $file The file to check from.
   * 
   * @return (bool)
   * 
   * @author RJP Granneman
   */
  private function checkFileExtension( $file ) {
    $return = null;
    
    // Valid mime types
    $mimeTypes = array(
        'application/vnd.ms-office',
        'application/x-excel',
        'application/x-msexcel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    );
    
    if( file_exists( $file ) ) {
      if( in_array( mime_content_type( $file ), $mimeTypes ) ) {
        // Mime type is valid
        $return = true;
      } else {
        // Mime type not in array
        $return = false;
      }
    } else {
      // No valid file
      $return = false;
    }
    
    return $return;
  }
  
  /*
   * Private function to update entire table bedrijf, set bedrijfsId = id
   *   makes you wonder why to have the column bedrijfsid in the first place
   *   and not only use column id... I don't know.
   * 
   * @return {void)
   * 
   * @author RJP Granneman
   */
  function correctEntityBedrijfsId() {
    global $pdo;

    $queryUpdate = $pdo->query("UPDATE bedrijf SET BEDRIJFSID = ID;");
    if (!$queryUpdate->execute()) {
      throw new Exception("Database Error: update entities went wrong.");
    }
  }
}