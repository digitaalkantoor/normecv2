<?php
sercurityCheck();

function get_template_content($templateFile)
{
    $template = "";
    if (file_exists($templateFile)) {
        $template = file_get_contents($templateFile);
    }
    return $template;
}

function replace_template_content_edit($userid, $bedrijfsid, $tabel, $templateFile, $id, $customValues = array())
{

    global $pdo;
    $templateContent = get_template_content($templateFile);

    if ($templateContent == "") {
        return false;
    }

    foreach ($customValues as $customKey => $customValue) {
        $templateContent = str_replace("-$customKey-", $customValue, $templateContent);
    }

    $templateContent = str_replace("-TRACKINGID-", getTrackingId(), $templateContent);

    $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
//        $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
    $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");

    $templateContent = str_replace("-ID-", $id, $templateContent);

    if ($tabel != "bedrijf") {
        $templateContent = str_replace("-DELETE-",
            "<input onClick=\"if(confirm('Are you sure you want to delete this entry?')) { $('<input />').attr('type', 'hidden').attr('name', 'delete').attr('value', 'delete').appendTo(this); form.submit(); }\" type=\"button\" name=\"delete\" value=\"Delete\" class=\"button\" />",
            $templateContent);
    } else {
        $templateContent = str_replace("-DELETE-", "", $templateContent);
    }
    $templateContent = str_replace("-BID-", $bedrijfsid, $templateContent);
    $templateContent = str_replace("-MENUID-", getMenuItem(), $templateContent);
    $templateContent = replace_template_algemeen($userid, $bedrijfsid, $tabel, $id, $templateContent);

    if (!empty($tabel)) {
        $tableData = getTableData($tabel);
        if (empty($tableData)) {
            return "No table data: $tabel";
        }
    }

    $query = $pdo->prepare('select * from ' . $tabel . ' where ID = :id limit 1;');
    $query->bindValue('id', $id);
    $query->execute();
    $data = $query->fetch(PDO::FETCH_ASSOC);

//        if($data == false) {
//            return $templateContent;
//        }
    foreach ($arrayVelden as $key => $kolom) {
        $c = "";
        if (isset($data[$kolom])) {
            $c = lb($data[$kolom]);
        }
        $replacement = "";

        if ($arrayVeldType[$key] == "bedrijfsnaam") {
            $replacement = getNaam("bedrijf", $bedrijfsid, "BEDRIJFSNAAM");
        } elseif ($arrayVeldType[$key] == "bedrijfsid") {
            $replacement = $bedrijfsid;
        } elseif ($arrayVeldType[$key] == "SKIP_UPLOADED_BY") {
            $uploadedBy = "<span style=\"font-size: 10px;\">Uploaded on " . datumconversiePresentatie($data["DATUM"]) . " by " . getPersoneelslidNaam($data["PERSONEELSLID"]) . "</span>";
            $templateContent = str_replace($tabel . "_" . $kolom . "_value", $uploadedBy, $templateContent);
        } elseif ($arrayVeldType[$key] == "SKIP_RENEW" || $arrayVeldType[$key] == "SKIP_CANCELPERIOD") {
            if ($data["HOE"] == "Ja") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value", "", $templateContent);
            } else {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value", " style=\"display: none;\" ",
                    $templateContent);
            }
        } elseif ($arrayVeldType[$key] == "SKIP_LIST_FILES") {

            if ($tabel == "contracten") {
                $doctype = 'Contract';
            } elseif ($tabel == "bedrijf") {
                $doctype = 'Articles of association';
            } elseif ($tabel == "oft_annual_accounts") {
                $doctype = 'Annual account%';
            } elseif ($tabel == "oft_tax") {
                $doctype = 'Tax %';
            } elseif ($tabel == "oft_mena") {
                $doctype = 'Articles of association';
            } elseif ($tabel == "oft_incident_register") {
                $doctype = 'Tax %';
            } else {
                $doctype = 'Deed of pledge';
            }

            if ($tabel == "oft_incident_register") {
                $list = oft_getDocuments($userid, $data["BEDRIJFSID"], '', '', '', '', $data["ID"], $tabel);
            } elseif ($tabel == "documentbeheer") {
                $list = oft_getDocuments($userid, $data["BEDRIJFSID"], "", array($doctype), $data["ID"], '', '');
            } elseif (isset($data["FINANCIALYEAR"]) && ($doctype == 'Annual account%' || $doctype == 'Articles of association' || $doctype == 'Tax %')) {
                $list = oft_getDocuments($userid, $data["BEDRIJFSID"], "", array($doctype), '', $data["FINANCIALYEAR"],
                    '');
            } elseif ($tabel == "contracten" || $tabel == "oft_incident_register") {
                $list = oft_getDocuments($userid, $data["BEDRIJFSID"], "", array($doctype), '', '', $data["ID"]);
            } else {
                $list = oft_getDocuments($userid, $data["BEDRIJFSID"], "", array($doctype), '', '', '');
            }

            $templateContent = str_replace($tabel . "_" . $kolom . "_value", $list, $templateContent);
        } elseif ($arrayVeldType[$key] == "SKIP_ADD_FILES_TYPE") {
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", "checked=\"true\"", $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", "", $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_3", "", $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_4", "", $templateContent);
        } elseif ($arrayVeldType[$key] == "AMOUNTPERSENTAGE") {
            $checked1 = "";
            $checked2 = "";
            if ($c == "Amount") {
                $checked1 = "checked=\"true\"";
            } else {
                $checked2 = "checked=\"true\"";
            }

            $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", $checked1, $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", $checked2, $templateContent);
        } elseif ($arrayVeldType[$key] == "SKIP_VERSIONS") {
            $telversies = 0;
            $query = $pdo->prepare('SELECT ID, DATUM, STOPDATUM, BESTANDSNAAM FROM documentbeheer_archief WHERE (NOT DELETED = "Ja" OR DELETED is null) AND AID = :aid ORDER BY DATUM;');
            $query->bindValue('aid', $data["ID"]);
            $query->execute();

            foreach ($query->fetchAll() as $data_2) {
                if ($telversies == 0) {
                    $replacement .= "<br/><h3>All versions</h3><br/><table class=\"oft_tabel\" cellspacing=\"2\" cellpadding=\"0\"><tr><th></th><th>Start date</th><th>End date</th><th>File</th></tr>";
                }

                $replacement .= "<tr>
                                  <td id=\"remove" . ($data_2["ID"] * 1) . "\"><a class=\"cursor\" onClick=\"if(confirm('Do you want to delete?')) { getPageContent2('content.php?SITE=deleteDocumentarchief&ID=" . ($data_2["ID"] * 1) . "', 'remove" . ($data_2["ID"] * 1) . "', searchReq2);}\"><img src=\"./images/verwijderen.gif\" border=\"0\" /></a></td>
                                  <td>" . datumconversiePresentatie($data_2["DATUM"]) . "</td>
                                  <td>" . datumconversiePresentatie($data_2["STOPDATUM"]) . "</td>
                                  <td><a href=\"document.php?AID=" . ($data_2["ID"] * 1) . "\">" . verkort($data_2["BESTANDSNAAM"],
                        50) . "</td>
                                </tr>";
                $telversies++;
            }
            if ($telversies > 0) {
                $replacement .= "</table>";
            }
        } elseif ($arrayVeldType[$key] == "director_functions") {
            $options = getListOptions('director_functions');
            $replacement = selectbox($options, $c, true);
        } elseif ($arrayVeldType[$key] == "incident_soort") {
            $options = getListOptions('incident_soort');
            $replacement = selectbox($options, $c, true);
        } elseif ($arrayVeldType[$key] == "accounting_systems") {
            $options = getListOptions('accounting_systems');
            $replacement = selectbox($options, $c, true);
        } elseif ($arrayVeldType[$key] == "type_managementboard") {
            $options = getListOptions('type_managementboard');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "secties") {
            $options = getListOptions('secties');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "service_type") {
            $options = getListOptions('service_type');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "service_provider") {
            $options = getListOptions('service_provider');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "mena_status") {
            $options = getListOptions('mena_status');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "mena_category") {
            $options = getListOptions('mena_category');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "mena_services") {
            $options = getListOptions('mena_services');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "SKIP_AGENDA") {
            $replacement .= "<br/><h3>Agenda</h3>
                             <br/><table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"10\"><tr><th></th><th></th><th>Agenda item</th><th>Files</th></tr>";

            $teller = 1;
            $hiddenvanaf = 0;
            $query = $pdo->prepare('SELECT * FROM oft_boardpack_items WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BPID = :bpid ORDER BY ID;');
            $query->bindValue('bpid', $data["ID"]);
            $query->execute();

            foreach ($query->fetchAll() as $data_2) {
                $replacement .= "<tr id=\"row" . ($data_2["ID"] * 1) . "\">
                                  <td><a onclick=\"if(confirm('Do you want to delete?')) { getPageContent2('content.php?SITE=oft_delete_item&tabel=oft_boardpack_items&ID=" . ($data_2["ID"] * 1) . "', 'row" . ($data_2["ID"] * 1) . "', searchReq2);}\"><img border=\"0\" src=\"./images/verwijderen.gif\"></a></td>
                                  <td>$teller</td>
                                  <td>
                                    <input type=\"hidden\" name=\"BP_ITEM_ID_$teller\" value=\"" . ($data_2["ID"] * 1) . "\">";
                $replacement .= createEditor("BP_ITEM_$teller", $data_2["ITEM"], "300", "100", $barType = "small");
                $replacement .= "</td>
                                  <td valign=\"top\">";
                for ($f = 1; $f <= 3; $f++) {

                    if (($data_2["FILE$f"] * 1) > 0 && getNaam("documentbeheer", $data_2["FILE$f"],
                            "DELETED") != "Ja") {
                        $docid = getNaam("documentbeheer", $data_2["FILE$f"], "ID");
                        $replacement .= "<div id=\"row" . ($docid * 1) . "\">
                                      <a href=\"content.php?SITE=entity_getFileCloudVPS_ajax&ID=" . ($data_2["FILE$f"] * 1) . "\">" . getNaam("documentbeheer",
                                $data_2["FILE$f"], "BESTANDSNAAM") . "</a>
                                      <a style=\"display:inline-block;float: right;\" onclick=\"if(confirm('Do you want to delete?')) { getPageContent2('content.php?SITE=oft_delete_item&tabel=documentbeheer&ID=" . ($docid * 1) . "', 'row" . ($docid * 1) . "', searchReq2);}\">
                                      <img border=\"0\" src=\"./images/verwijderen.gif\"></a>
                                   </div>";
                    } else {
                        $replacement .= "<input type=\"file\" name=\"BP_ITEM_FILE" . $f . "_$teller\" value=\"\" class=\"field\" /><br/>";
                    }
                }
                $replacement .= "</td></tr>";
                $teller++;
                $hiddenvanaf++;
            }

            while ($teller <= 15) {
                $display = '';
                $display = "style=\"display: none;\"";
                $replacement .= "<tr id=\"agendarow$teller\" $display>
                                  <td></td>
                                  <td>$teller.</td>
                                  <td>";
                $replacement .= createEditor("BP_ITEM_$teller", "", "300", "100", $barType = "xssmall");
                // In tpl_opslaan kunnen max 15 bestanden worden opgelslagen
                $replacement .= "</td>
                                  <td valign=\"top\">
                                    <input type=\"file\" id=\"ID1_$teller\" name=\"BP_ITEM_FILE1_$teller\" value=\"\" class=\"field\" />
                                    <input type=\"file\" id=\"ID2_$teller\" name=\"BP_ITEM_FILE2_$teller\" value=\"\" class=\"field\" />
                                    <input type=\"file\" id=\"ID3_$teller\" name=\"BP_ITEM_FILE3_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <input type=\"file\" id=\"ID4_$teller\" name=\"BP_ITEM_FILE4_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <input type=\"file\" id=\"ID5_$teller\" name=\"BP_ITEM_FILE5_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <input type=\"file\" id=\"ID6_$teller\" name=\"BP_ITEM_FILE6_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <input type=\"file\" id=\"ID7_$teller\" name=\"BP_ITEM_FILE7_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <input type=\"file\" id=\"ID8_$teller\" name=\"BP_ITEM_FILE8_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <input type=\"file\" id=\"ID9_$teller\" name=\"BP_ITEM_FILE9_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <input type=\"file\" id=\"ID10_$teller\" name=\"BP_ITEM_FILE10_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                    <div id=\"SHOWID3_$teller\" class=\"cursor\" style= \"display:block\"/ onclick=\"show_next_file(this.id, 10)\">Add document</div>
                                  </td>
                                </tr>";
                $teller++;
            }

            $replacement .= "</table>
                             <div id=\"agendas\" style=\"display: none;\">$hiddenvanaf</div><a id=\"addagenda\" class=\"cursor\" onclick=\"document.getElementById('agendas').innerHTML=((document.getElementById('agendas').innerHTML*1)+1); toggle('agendarow'+document.getElementById('agendas').innerHTML);if(document.getElementById('agendas').innerHTML >= $teller) { toggle('addagenda'); }\">Add agenda item</a>";
        } elseif ($arrayVeldType[$key] == "SKIP_INVITEES") {
            $replacement .= "<br/><h3>Invitees meeting</h3>
                             <br/><table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"10\">";

            $teller = 0;
            $hiddenvanaf = 0;

            $query = $pdo->prepare('SELECT * FROM oft_boardpack_invitees WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BPID = :bpid ORDER BY ID;');
            $query->bindValue('bpid', $data["ID"]);
            $query->execute();

            foreach ($query->fetchAll() as $data_2) {
                $email = getNaam("personeel", $data_2["USERID"], "EMAIL");
                if (strlen($email) < 6) {
                    $email .= "</td><td><font color=\"#DF3821\">No e-mail address available!</font></td>";
                }
                $replacement .= "<tr id=\"row" . ($data_2["ID"] * 1) . "\">
                                  <td><input type=\"checkbox\" name=\"BP_INVITEE_CHECK_" . ($data_2["ID"] * 1) . "\"</td>
                                  <td><a onclick=\"if(confirm('Do you want to delete?')) { getPageContent2('content.php?SITE=oft_delete_item&tabel=oft_boardpack_invitees&ID=" . ($data_2["ID"] * 1) . "', 'row" . ($data_2["ID"] * 1) . "', searchReq2);}\"><img border=\"0\" src=\"./images/verwijderen.gif\"></a></td>
                                  <td>" . getPersoneelsLidNaam($data_2["USERID"]) . "</td>
                                  <td>" . getNaam("personeel", $data_2["USERID"], "ROL") . "</td>
                                  <td>" . $email . "</td>
                                </tr>";
                $teller++;
                $hiddenvanaf++;
            }

            $personeelsleden = selectbox_tabel("personeel", "ID", "INITIALEN, TUSSENVOEGSEL, ACHTERNAAM", '', true,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");

            $teller = 0;
            while ($teller <= 15) {
                $replacement .= "<tr id=\"inviteerow$teller\" $display>
                                 <td></td>
                                 <td></td>
                                 <td>Invitee $teller</td>
                                 <td colspan=\"2\"><select name=\"BP_INVITEE_$teller\" class=\"field\">$personeelsleden</select></td>
                               </tr>";
                $teller++;
            }

            $replacement .= "</table>
                             <div id=\"invitees\" style=\"display: none;\">$hiddenvanaf</div><a id=\"addinvitee\" class=\"cursor\" onclick=\"document.getElementById('invitees').innerHTML=((document.getElementById('invitees').innerHTML*1)+1); toggle('inviteerow'+document.getElementById('invitees').innerHTML);if(document.getElementById('invitees').innerHTML >= $teller) { toggle('addinvitee'); }\">Add invitee</a>";
        } elseif ($arrayVeldType[$key] == "MEMBERTYPE") {
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", "checked=\"true\"", $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", "", $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_3", "", $templateContent);
        } elseif ($arrayVeldType[$key] == "extraveld") {
            $extraveld = "";

            $query = $pdo->prepare('SELECT * FROM extra_velden WHERE VELDNAAM = :veldnaam AND ACTIVE = "Ja" limit 1;');
            $query->bindValue('veldnaam', $kolom);
            $query->execute();
            $dExtraVeld = $query->fetch(PDO::FETCH_ASSOC);

            if ($dExtraVeld !== false) {
                $extraveld = "<tr>
                                <td>" . $dExtraVeld["VELDOMSCHRIJVING"] . "</td>
                                <td colspan=\"10\"><input type=\"text\" name=\"" . $tabel . "_" . $kolom . "\" value=\"" . $c . "\" class=\"field\" /></td>
                             </tr>";
            }
            $templateContent = str_replace($tabel . "_" . $kolom . "_value", $extraveld, $templateContent);
        } elseif ($arrayVeldType[$key] == "rechten") {
            $checked1 = "";
            $checked2 = "";
            $checked3 = "";
            if ($c == "Geen" || $c == "Nee") {
                $checked1 = "checked=\"true\"";
            } elseif ($c == "Lees") {
                $checked2 = "checked=\"true\"";
            } elseif ($c == "Ja") {
                $checked3 = "checked=\"true\"";
            }

            $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", $checked1, $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", $checked2, $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_3", $checked3, $templateContent);
        } elseif ($arrayVeldType[$key] == "rechtentot") {
            $rechtenTot = rechtenTot($userid, $bedrijfsid, '');
            if (isset($rechtenTot[0])) {
                $replacement .= "<br/><br/>Select the entities the user is authorized to:
                              <br/>" . $rechtenTot[0];
            }
        } elseif ($arrayVeldType[$key] == "selectjanee") {
            $checked1 = "";
            $checked2 = "";
            if ($c == "Ja") {
                $checked1 = "checked=\"checked\"";
            } else {
                $checked2 = "checked=\"checked\"";
            }

            $templateContent = str_replace($tabel . "_" . $kolom . "_value_ja", $checked1, $templateContent);
            $templateContent = str_replace($tabel . "_" . $kolom . "_value_nee", $checked2, $templateContent);
        } elseif ($arrayVeldType[$key] == "AUTHORIZATION" || $arrayVelden[$key] == "AUTHORIZATION") {
            $options = getListOptions('AUTHORIZATION');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "PRIORITY") {
            $options = getListOptions('PRIORITY');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "status") {
            $options = getListOptions('status');
            $replacement = selectbox($options, $c, true);
        } elseif ($arrayVeldType[$key] == "reg_status") {
            $options = getListOptions('reg_status');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "numbers_types") {
            $options = getListOptions('numbers_types');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "jaar") {
            $jarenArray = array();
            for ($j = date("Y") + 1; $j >= (date("Y") - 10); $j--) {
                $jarenArray[$j] = $j;
            }

            $replacement = selectbox($jarenArray, $c, false);
        } elseif ($arrayVeldType[$key] == "DOCTYPE") {
            $sectie = "";
            $query = $pdo->prepare('SELECT SECTIE FROM stam_documenttype WHERE NAAM = :naam limit 1;');
            $query->bindValue('naam', $c);
            $query->execute();
            $dSectie = $query->fetch(PDO::FETCH_ASSOC);

            if ($dSectie !== false) {
                $sectie = "AND SECTIE like '" . ps($dSectie["SECTIE"]) . "'";
            }

            $replacement = selectbox_tabel("stam_documenttype", "NAAM", "NAAM", $c, false,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null) $sectie");
        } elseif ($arrayVeldType[$key] == "SKIP_EXISTING") {
            $list = array();
            $query = $pdo->prepare('SELECT * FROM oft_bedrijf_adres GROUP BY ADRES ORDER BY ADRES;');
            $query->execute();

            foreach ($query->fetchAll() as $data_2) {
                $list[($data_2["ID"] * 1)] = trim(lb($data_2["ADRES"]) . ", " . lb($data_2["PLAATS"]));
            }
            $replacement = selectbox($list, $c, true);
        } elseif ($arrayVeldType[$key] == "SIGNATORY") {
            $capicities = array("Proxyholder" => "Proxyholder");
            $queryCapicities = $pdo->prepare('SELECT * FROM oft_capacity GROUP BY NAAM;');
            $queryCapicities->execute();

            foreach ($queryCapicities->fetchAll() as $data_2) {
                $capicities[trim($data_2["NAAM"])] = trim($data_2["NAAM"]);
            }

            $replacement .= "<table class=\"oft_table\"><tr><th>Signatory</th><th>Capacity</th></tr>";

            $iterator = 1;
            $aanwezig = 1;
            $queryContract = $pdo->prepare('SELECT * FROM contractpartijen WHERE CID = :cid ORDER BY ID;');
            $queryContract->bindValue('cid', $data["ID"]);
            $queryContract->execute();

            foreach ($queryContract->fetchAll() as $data_ireg) {
                $replacement .= "<tr id=\"PH$iterator\"><td><select name=\"PH$iterator\" class=\"field\">" . selectbox_tabel("oft_proxyholders",
                        "ID", "USERID", $data_ireg["USERID"], true,
                        "WHERE (NOT DELETED = 'Ja' OR DELETED is null)") . "</select></td><td><select name=\"PH_CAPACITY$iterator\" class=\"fieldHalf\">" . selectbox($capicities,
                        $data_ireg["TYPE"], false) . "</select></td></tr>";
                $iterator++;
                $aanwezig++;
            }

            $oft_proxyholders = selectbox_tabel("oft_proxyholders", "ID", "USERID", $userid, true,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
            while ($iterator <= 5) {
                $replacement .= "<tr id=\"PH$iterator\" style=\"display: none;\"><td><select name=\"PH$iterator\" class=\"field\">$oft_proxyholders</select></td><td><select name=\"PH_CAPACITY$iterator\" class=\"fieldHalf\">" . selectbox($capicities,
                        "Proxy holder", false) . "</select></td></tr>";
                $iterator++;
            }
            $replacement .= "</table><div id=\"signatories\" style=\"display: none;\">$aanwezig</div><a id=\"addsig\" class=\"cursor\" onclick=\"document.getElementById('signatories').innerHTML=((document.getElementById('signatories').innerHTML*1)+1); toggle('PH'+document.getElementById('signatories').innerHTML);if(document.getElementById('signatories').innerHTML == $iterator) { toggle('addsig'); }\">Add</a>";
        } elseif ($arrayVeldType[$key] == "adres_type") {
            $options = getListOptions('adres_type');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "PROFIEL") {
            $options = getListOptions('PROFIEL');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "TRANSACTIONTYPE") {
            $replacement = "<option value=\"Sale of share\">Sale of share</option><option value=\"Pledge of share\">Pledge of share</option><option value=\"Subscription of share\">Subscription of share</option><option value=\"Redemption of share\">Redemption of share</option>";
        } elseif ($arrayVeldType[$key] == "owner") {
            $options = getListOptions('owner');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "CLASSTYPE") {
            $replacement = selectbox_tabel("oft_classes_types", "NAAM", "NAAM", $c, false,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
        } elseif ($arrayVeldType[$key] == "land") {
            $replacement = selectbox_tabel("country", "NAAM", "NAAM", $c, false,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null) AND ZICHTBAAR='Ja'");
        } elseif ($arrayVeldType[$key] == "valuta") {
            $replacement = selectbox_tabel("valuta", "NAAM", "NAAM", $c, false,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
        } elseif ($arrayVeldType[$key] == "bedrijf" || $arrayVeldType[$key] == "newentity") {
            $replacement = selectbox_tabel("bedrijf", "ID", "BEDRIJFSNAAM", $c, false,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
        } elseif ($arrayVeldType[$key] == "oft_department") {
            $replacement = selectbox_tabel("oft_department", "ID", "NAAM", $c, false,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
        } elseif ($arrayVeldType[$key] == "personeel") {
            $replacement = selectbox_tabel("personeel", "ID", "INITIALEN, TUSSENVOEGSEL, ACHTERNAAM", $c, false,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
        } elseif ($arrayVeldType[$key] == "reg_recoverable") {
            $options = getListOptions('reg_recoverable');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "reg_typeofaction") {
            $options = getListOptions('reg_typeofaction');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "reg_incidenttype") {
            $options = getListOptions('reg_incidenttype');
            $replacement = selectbox($options, $c, false);
        } elseif ($arrayVeldType[$key] == "termijn") {
            $options = getListOptions('termijn');
            $replacement = selectbox($options, $c, false);
        } elseif (substr($arrayVeldType[$key], 0, 5) == "stam_") {
            $replacement = selectbox_tabel($arrayVeldType[$key], "ID", "NAAM", $c, true,
                "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
        } elseif ($arrayVeldType[$key] == "datum" || $arrayVeldType[$key] == "datumleeg") {
            if ($c == "0000-00-00") {
                $c = "";
            }

            if ($kolom == "DATEOFREELECTION" && $c != "") {
                $personeelsStatus = getNotificatieStatus(array('due_date' => $c, 'table' => $tabel, 'table_id' => $id));
                $templateContent = str_replace($tabel . "_" . $kolom . "_status", $personeelsStatus, $templateContent);
            } elseif ($kolom == "DATEOFREELECTION" && $c == "") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_status", "", $templateContent);
            }

            $replacement = substr($c, 0, 10);
        } elseif ($arrayVeldType[$key] == "jarenlijst") {
            $jaren = array();
            for ($j = date("Y") + 1; $j >= date("Y") - 11; $j--) {
                $jaren["$j-01-01"] = $j;
            }
            $replacement = selectbox($jaren, $c, false);
        } elseif ($arrayVeldType[$key] == "alleenjarenlijst") {
            $jaren = array();
            for ($j = date("Y") + 1; $j >= date("Y") - 11; $j--) {
                $jaren[$j] = $j;
            }
            $replacement = selectbox($jaren, $c, false);
        } elseif ($arrayVeldType[$key] == "boardmeetings") {
            $lijst = array();
            for ($j = 0; $j <= 4; $j++) {
                $lijst[$j] = $j;
            }
            $replacement = selectbox($lijst, 1, false);
        } elseif ($arrayVeldType[$key] == "SKIP_INCIDENT_ACTIONS") {
            if (isset($_REQUEST["PRINT"])) {
                $query = $pdo->prepare('SELECT * FROM oft_incident_register_task WHERE INCIDENTID = :incidentid ORDER BY DATUM;');
                $query->bindValue('incidentid', $data["ID"]);
                $query->execute();

                foreach ($query->fetchAll() as $data_ireg) {
                    $iDatum = "";
                    if ($data_ireg["DATUM"] != "0000-00-00") {
                        $iDatum = $data_ireg["DATUM"];
                    }
                    $replacement .= "<tr><td class=\"oft_label\">Actions taken - text</td>
                                    <td></td>
                                    <td>" . stripslashes($data_ireg["TASK"]) . "</td>
                                 </tr>
                                 <tr><td class=\"oft_label\">Actions taken - status</td>
                                    <td></td>
                                    <td>" . selectbox(array("Open" => "Open", "Closed" => "Closed"),
                            $data_ireg["STATUS"], false) . "</td>
                                 </tr>
                                 <tr><td class=\"oft_label\">Actions taken - last update</td>
                                     <td></td>
                                     <td>$iDatum</td>
                                 </tr>";
                }
            } else {
                $tTasks = 1;
                $query = $pdo->prepare('SELECT * FROM oft_incident_register_task WHERE INCIDENTID = :incidentid ORDER BY DATUM;');
                $query->bindValue('incidentid', $data["ID"]);
                $query->execute();

                foreach ($query->fetchAll() as $data_ireg) {
                    $iDatum = "";
                    if ($data_ireg["DATUM"] != "0000-00-00") {
                        $iDatum = lb($data_ireg["DATUM"]);
                    }
                    $replacement .= "<tr><td class=\"oft_label\">$tTasks. Actions taken - text</td>
                                      <td></td>
                                      <td><textarea name=\"INCIDENT_ACTIONS_" . ($data_ireg["ID"] * 1) . "\" class=\"textbox\">" . stripslashes($data_ireg["TASK"]) . "</textarea></td>
                                   </tr>
                                   <tr><td class=\"oft_label\">Actions taken - status</td>
                                      <td></td>
                                      <td><select name=\"INCIDENT_ACTIONS_STATUS_" . ($data_ireg["ID"] * 1) . "\" class=\"field\">" . selectbox(array(
                            "Open" => "Open",
                            "Closed" => "Closed"
                        ), $data_ireg["STATUS"], false) . "</select></td>
                                   </tr>
                                   <tr><td class=\"oft_label\">Actions taken - last update</td>
                                       <td></td>
                                       <td><input type=\"text\" class=\"field3\" value=\"" . $iDatum . "\" name=\"INCIDENT_ACTIONS_DATUM_" . ($data_ireg["ID"] * 1) . "\" onClick=\"$(this).datepicker().datepicker( 'show' );\" /></td>
                                   </tr>";
                    $tTasks++;
                }

                $replacement .= "<tr><td class=\"oft_label\">Actions taken - text</td>
                                    <td></td>
                                    <td><textarea name=\"INCIDENT_ACTIONS_n\" class=\"textbox\"></textarea></td>
                                 </tr>
                                 <tr><td class=\"oft_label\">Actions taken - status</td>
                                    <td></td>
                                    <td><select name=\"INCIDENT_ACTIONS_STATUS_n\" class=\"field\">" . selectbox(array(
                        "Open" => "Open",
                        "Closed" => "Closed"
                    ), "Open", false) . "</select></td>
                                 </tr>
                                 <tr><td class=\"oft_label\">Actions taken - last update</td>
                                     <td></td>
                                     <td><input type=\"text\" class=\"field3\" value=\"\" name=\"INCIDENT_ACTIONS_DATUM_n\" id=\"INCIDENT_ACTIONS_DATUM_n\" onClick=\"$(this).datepicker().datepicker( 'show' );\" /></td>
                                 </tr>";
            }
        } elseif ($arrayVeldType[$key] == "endfinyear") {
            $maanden[date("Y") . "-01-31"] = "31 Jan";
            $maanden[date("Y") . "-02-28"] = "28 Feb"; // Todo: Add leap year handling
            $maanden[date("Y") . "-03-31"] = "31 Mrt";
            $maanden[date("Y") . "-04-30"] = "30 Apr";
            $maanden[date("Y") . "-05-31"] = "31 May";
            $maanden[date("Y") . "-06-30"] = "30 Jun";
            $maanden[date("Y") . "-07-31"] = "31 Jul";
            $maanden[date("Y") . "-08-31"] = "31 Aug";
            $maanden[date("Y") . "-09-30"] = "30 Sep";
            $maanden[date("Y") . "-10-31"] = "31 Oct";
            $maanden[date("Y") . "-11-30"] = "30 Nov";
            $maanden[date("Y") . "-12-31"] = "31 Dec";
            $replacement = selectbox($maanden, $c, false);
        } elseif ($arrayVeldType[$key] == "maanden") {
            $maanden = array(
                "01" => "January",
                "02" => "February",
                "03" => "March",
                "04" => "April",
                "05" => "May",
                "06" => "June",
                "07" => "July",
                "08" => "August",
                "09" => "September",
                "10" => "October",
                "11" => "November",
                "12" => "December"
            );
            $replacement = selectbox($maanden, $c, true);
        } else {
            $replacement = stripslashes($c);
        }

        $templateContent = str_replace($tabel . "_" . $kolom . "_value", $replacement, $templateContent);
    }

    return $templateContent;
}

function getTrackingId()
{
    return sha1(date("Ymdh") . $_SERVER['REMOTE_ADDR']);
}

function replace_template_content($userid, $bedrijfsid, $tabel, $templateFile, $customValues = array())
{
    global $pdo;
    $templateContent = get_template_content($templateFile);
    if ($templateContent != "") {
        //$templateContent

        foreach ($customValues as $customKey => $customValue) {
            $templateContent = str_replace("-$customKey-", $customValue, $templateContent);
        }

        $templateContent = str_replace("-TRACKINGID-", getTrackingId(), $templateContent);

        $arrayVelden = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVelden");
        $arrayVeldnamen = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldnamen");
        $arrayVeldType = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVeldType");
        $arrayVerplicht = oft_get_structure($userid, $bedrijfsid, $tabel, "arrayVerplicht");

        $templateContent = str_replace($tabel . "_ID_value", "", $templateContent);
        $templateContent = str_replace("-DELETE-", "", $templateContent);
        $templateContent = str_replace("-BID-", $bedrijfsid, $templateContent);
        $templateContent = replace_template_algemeen($userid, $bedrijfsid, $tabel, '', $templateContent);
        foreach ($arrayVelden as $key => $kolom) {
            $replacement = "";

            if ($arrayVeldType[$key] == "volgnummering") {
                $replacement = getNieuwNr($tabel, $kolom, $bedrijfsid);
            } elseif ($arrayVeldType[$key] == "bedrijfsnaam") {
                $replacement = getNaam("bedrijf", $bedrijfsid, "BEDRIJFSNAAM");
            } elseif ($arrayVeldType[$key] == "bedrijfsid") {
                $replacement = $bedrijfsid;
            } elseif ($arrayVeldType[$key] == "datum") {
                if ($kolom == "DATEOFREELECTION") {
                    $templateContent = str_replace($tabel . "_" . $kolom . "_status", "", $templateContent);
                }
                $replacement = date("Y-m-d");
            } elseif ($arrayVeldType[$key] == "datumleeg") {
            } elseif ($arrayVeldType[$key] == "director_functions") {
                $options = getListOptions('director_functions');
                $replacement = selectbox($options, '', true);
            } elseif ($arrayVeldType[$key] == "incident_soort") {
                $options = getListOptions('incident_soort');
                $replacement = selectbox($options, '', true);
            } elseif ($arrayVeldType[$key] == "secties") {
                $options = getListOptions('secties');
                $replacement = selectbox($options, 'Legal', false);
            } elseif ($arrayVeldType[$key] == "type_managementboard") {
                $options = getListOptions('type_managementboard');
                $replacement = selectbox($options, "Executive", false);
            } elseif ($arrayVeldType[$key] == "SKIP_AGENDA") {
                $replacement .= "<br/><h3>Agenda</h3>
                             <br/><table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"10\"><tr><th></th><th>Agenda item</th><th>Files</th></tr>";

                $teller = 1;
                $hiddenvanaf = 0;
                while ($teller <= 15) {
                    $display = '';
                    if ($teller > 2) {
                        $display = "style=\"display: none;\"";
                    } else {
                        $hiddenvanaf++;
                    }
                    //<textarea name=\"BP_ITEM_$teller\" class=\"textbox\" style=\"height: 50px\"></textarea>
                    $replacement .= "<tr id=\"agendarow$teller\" $display>
                                  <td>$teller.</td>
                                  <td>";
                    $replacement .= createEditor("BP_ITEM_$teller", "", "300", "100", $barType = "xssmall");
                    // In tpl_opslaan kunnen max 15 bestanden worden opgelslagen
                    $replacement .= "</td>
                                  <td valign=\"top\">
                                       <input type=\"file\" id=\"ID1_$teller\" name=\"BP_ITEM_FILE1_$teller\" value=\"\" class=\"field\" />
                                        <input type=\"file\" id=\"ID2_$teller\" name=\"BP_ITEM_FILE2_$teller\" value=\"\" class=\"field\" />
                                        <input type=\"file\" id=\"ID3_$teller\" name=\"BP_ITEM_FILE3_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <input type=\"file\" id=\"ID4_$teller\" name=\"BP_ITEM_FILE4_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <input type=\"file\" id=\"ID5_$teller\" name=\"BP_ITEM_FILE5_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <input type=\"file\" id=\"ID6_$teller\" name=\"BP_ITEM_FILE6_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <input type=\"file\" id=\"ID7_$teller\" name=\"BP_ITEM_FILE7_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <input type=\"file\" id=\"ID8_$teller\" name=\"BP_ITEM_FILE8_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <input type=\"file\" id=\"ID9_$teller\" name=\"BP_ITEM_FILE9_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <input type=\"file\" id=\"ID10_$teller\" name=\"BP_ITEM_FILE10_$teller\" value=\"\" class=\"field\" style= \"display:none\"/>
                                        <div id=\"SHOWID3_$teller\" class=\"cursor\" style= \"display:block\"/ onclick=\"show_next_file(this.id, 10)\">Add document</div>
                                  </td>
                                </tr>";
                    $teller++;
                }

                $replacement .= "</table>
                             <div id=\"agendas\" style=\"display: none;\">$hiddenvanaf</div><a id=\"addagenda\" class=\"cursor\" onclick=\"document.getElementById('agendas').innerHTML=((document.getElementById('agendas').innerHTML*1)+1); toggle('agendarow'+document.getElementById('agendas').innerHTML);if(document.getElementById('agendas').innerHTML >= $teller) { toggle('addagenda'); }\">Add agenda item</a>";
            } elseif ($arrayVeldType[$key] == "SKIP_INVITEES") {
                $replacement .= "<br/><h3>Invitees meeting</h3>
                             <br/><table class=\"oft_tabel\" cellspacing=\"0\" cellpadding=\"10\">";

                $personeelsleden = selectbox_tabel("personeel", "ID", "INITIALEN, TUSSENVOEGSEL, ACHTERNAAM", '', true,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");

                $teller = 0;
                $hiddenvanaf = 0;
                while ($teller <= 15) {
                    $display = '';
                    if ($teller > 2) {
                        $display = "style=\"display: none;\"";
                    } else {
                        $hiddenvanaf++;
                    }
                    $replacement .= "<tr id=\"inviteerow$teller\" $display>
                                 <td>Invitee</td>
                                 <td><select name=\"BP_INVITEE_$teller\" class=\"field\">$personeelsleden</select></td>
                               </tr>";
                    $teller++;
                }

                $replacement .= "</table>
                             <div id=\"invitees\" style=\"display: none;\">$hiddenvanaf</div><a id=\"addinvitee\" class=\"cursor\" onclick=\"document.getElementById('invitees').innerHTML=((document.getElementById('invitees').innerHTML*1)+1); toggle('inviteerow'+document.getElementById('invitees').innerHTML);if(document.getElementById('invitees').innerHTML >= $teller) { toggle('addinvitee'); }\">Add invitee</a>";
            } elseif ($arrayVeldType[$key] == "SKIP_LIST_FILES") {
                $list = "";
                $doctype = 'Deed of pledge';
                if ($tabel == "contracten") {
                    $doctype = 'Contract';
                } elseif ($tabel == "bedrijf") {
                    $doctype = 'Articles of association';
                } elseif ($tabel == "oft_annual_accounts") {
                    $doctype = 'Annual account';
                } elseif ($tabel == "oft_tax") {
                    $doctype = 'Tax filing';
                }
                $queryDocument = $pdo->prepare('SELECT ID, BESTANDSNAAM FROM documentbeheer WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid AND DOCTYPE = :doctype ORDER BY BESTANDSNAAM;');
                $queryDocument->bindValue('bedrijfsid', $bedrijfsid);
                $queryDocument->bindValue('doctype', $doctype);
                $queryDocument->execute();

                foreach ($queryDocument->fetchAll() as $data_2) {
                    $list .= "<a href=\"content.php?SITE=entity_getFileCloudVPS_ajax&ID=" . ($data_2["ID"] * 1) . "\">" . $data_2["BESTANDSNAAM"] . "</a><br/>";
                }
                $templateContent = str_replace($tabel . "_" . $kolom . "_value", $list, $templateContent);
            } elseif ($arrayVeldType[$key] == "SKIP_EXISTING") {
                $list = array();
                $query = $pdo->prepare('SELECT * FROM oft_bedrijf_adres GROUP BY ADRES ORDER BY ADRES;');
                $query->execute();

                foreach ($query->fetchAll() as $data_2) {
                    $list[($data_2["ID"] * 1)] = trim(stripslashes($data_2["ADRES"]) . ", " . stripslashes($data_2["PLAATS"]));
                }
                $replacement = selectbox($list, "", true);
            } elseif ($arrayVeldType[$key] == "SKIP_ADD_FILES_TYPE") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", "checked=\"true\"",
                    $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", "", $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_3", "", $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_4", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "rechtentot") {
                $rechtenTot = rechtenTot('new', $bedrijfsid, '');
                if (isset($rechtenTot[0])) {
                    $replacement .= "<br/><br/>Select the entities the user is authorized to<br/>" . $rechtenTot[0];
                }
            } elseif ($arrayVeldType[$key] == "SIGNATORY") {
                $oft_proxyholders = selectbox_tabel("oft_proxyholders", "ID", "USERID", $bedrijfsid, true,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");

                $capicities = array("Proxyholder" => "Proxyholder");
                $query = $pdo->prepare('SELECT * FROM oft_capacity GROUP BY NAAM;');
                $query->execute();

                foreach ($query->fetchAll() as $data_2) {
                    $capicities[trim($data_2["NAAM"])] = trim($data_2["NAAM"]);
                }

                $replacement .= "<table class=\"oft_table\"><tr><th>Signatory</th><th>Capacity</th></tr>";
                $replacement .= "<tr id=\"PH1\"><td><select name=\"PH1\" class=\"field\">$oft_proxyholders</select></td><td><select name=\"PH_CAPACITY1\" class=\"fieldHalf\">" . selectbox($capicities,
                        "Proxy holder", false) . "</select></td></tr>";
                $replacement .= "<tr id=\"PH2\" style=\"display: none;\"><td><select name=\"PH2\" class=\"field\">$oft_proxyholders</select></td><td><select name=\"PH_CAPACITY2\" class=\"fieldHalf\">" . selectbox($capicities,
                        "Proxy holder", false) . "</select></td></tr>";
                $replacement .= "<tr id=\"PH3\" style=\"display: none;\"><td><select name=\"PH3\" class=\"field\">$oft_proxyholders</select></td><td><select name=\"PH_CAPACITY3\" class=\"fieldHalf\">" . selectbox($capicities,
                        "Proxy holder", false) . "</select></td></tr>";
                $replacement .= "<tr id=\"PH4\" style=\"display: none;\"><td><select name=\"PH4\" class=\"field\">$oft_proxyholders</select></td><td><select name=\"PH_CAPACITY4\" class=\"fieldHalf\">" . selectbox($capicities,
                        "Proxy holder", false) . "</select></td></tr>";
                $replacement .= "<tr id=\"PH5\" style=\"display: none;\"><td><select name=\"PH5\" class=\"field\">$oft_proxyholders</select></td><td><select name=\"PH_CAPACITY5\" class=\"fieldHalf\">" . selectbox($capicities,
                        "Proxy holder", false) . "</select></td></tr>";
                $replacement .= "</table><div id=\"signatories\" style=\"display: none;\">1</div><a id=\"addsig\" class=\"cursor\" onclick=\"document.getElementById('signatories').innerHTML=((document.getElementById('signatories').innerHTML*1)+1); toggle('PH'+document.getElementById('signatories').innerHTML);if(document.getElementById('signatories').innerHTML == 5) { toggle('addsig'); }\">Add</a>";
            } elseif ($arrayVeldType[$key] == "AMOUNTPERSENTAGE") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", "checked=\"true\"",
                    $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "SKIP_RENEW") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "SKIP_CANCELPERIOD") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "mena_status") {
                $options = getListOptions('mena_status');
                $replacement = selectbox($options, "Pending", false);
            } elseif ($arrayVeldType[$key] == "service_provider") {
                $options = getListOptions('service_provider');
                $replacement = selectbox($options, "Type", false);
            } elseif ($arrayVeldType[$key] == "numbers_types") {
                $options = getListOptions('numbers_types');
                $replacement = selectbox($options, "Type", false);
            } elseif ($arrayVeldType[$key] == "mena_category") {
                $options = getListOptions('mena_category');
                $replacement = selectbox($options, "Pending", false);
            } elseif ($arrayVeldType[$key] == "mena_services") {
                $options = getListOptions('mena_services');
                $replacement = selectbox($options, "Administration", false);
            } elseif ($arrayVeldType[$key] == "MEMBERTYPE") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", "checked=\"true\"",
                    $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", "", $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_3", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "rechten") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_1", "", $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_2", "checked=\"true\"",
                    $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_3", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "selectjanee") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_ja", "checked=\"true\"",
                    $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_nee", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "newuser") {
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_ja", "checked=\"true\"",
                    $templateContent);
                $templateContent = str_replace($tabel . "_" . $kolom . "_value_nee", "", $templateContent);
            } elseif ($arrayVeldType[$key] == "PROFIEL") {
                $options = getListOptions('PROFIEL');
                $replacement = selectbox($options, "Administrator", false);
            } elseif ($arrayVeldType[$key] == "AUTHORIZATION" || $arrayVelden[$key] == "AUTHORIZATION") {
                $options = getListOptions('AUTHORIZATION');
                $replacement = selectbox($options, "Solely", false);
            } elseif ($arrayVeldType[$key] == "maanden") {
                $options = getListOptions('maanden');
                $replacement = selectbox($options, date("m"), true);
            } elseif ($arrayVeldType[$key] == "PRIORITY") {
                $options = getListOptions('PRIORITY');
                $replacement = selectbox($options, "Medium", false);
            } elseif ($arrayVeldType[$key] == "jaar") {
                $jarenArray = array();
                for ($j = date("Y") + 1; $j >= (date("Y") - 10); $j--) {
                    $jarenArray[$j] = $j;
                }

                $replacement = selectbox($jarenArray, date("Y"), false);
            } elseif ($arrayVeldType[$key] == "SKIP_EXISTING") {
                $replacement = selectbox(array(), "", true);
            } elseif ($arrayVeldType[$key] == "DOCTYPE") {
                if (isset($_REQUEST["DOCTYPE"])) {
                    $doctypes = array();
                    $doctype = $_REQUEST["DOCTYPE"];
                    $tDocType = explode(',', $_REQUEST["DOCTYPE"]);
                    foreach ($tDocType as $kDoc => $vDoc) {
                        $doctype = lb($vDoc);
                        $doctypes[$doctype] = $doctype;
                    }

                    $replacement = selectbox($doctypes, $doctype, false);
                } else {
                    $sectie = "";
                    if (isset($_REQUEST["SECTIE"])) {
                        $sectie = "AND SECTIE like '" . ps($_REQUEST["SECTIE"]) . "'";
                    }

                    $replacement = selectbox_tabel("stam_documenttype", "NAAM", "NAAM", "Articles of association",
                        false, "WHERE (NOT DELETED = 'Ja' OR DELETED is null) $sectie");
                    /*
                $replacement     = selectbox(array("Annual accounts" => "Annual accounts",
                "Articles of association" => "Articles of association",
                "Bank documents" => "Bank documents",
                "Board resolution" => "Board resolution",
                //"Contract" => "Contract",
                //"Corporate Income Tax filing" => "Corporate Income Tax filing",
                "Deed of transfer" => "Deed of transfer",
                "Deed of pledge" => "Deed of pledge",
                "Extract trade register" => "Extract trade register",
                "Financial documents" => "Financial documents",
                "Loan agreements" => "Loan agreements",
                "Procedures" => "Procedures",
                "Proxies" => "Proxies",
                "Service agreements" => "Service agreements",
                "Share transfer agreements" => "Share transfer agreements",
                "Shareholders register" => "Shareholders register",
                "Shareholders resolution" => "Shareholders resolution",
                "Tax filing" => "Tax filing",
                "VAT filing" => "VAT filing",
                "CPEC" => "CPEC",
                "Registers" => "Registers",
                "Other" => "Other",
                ), "Articles of association", false);
                 */
                }
            } elseif ($arrayVeldType[$key] == "reg_incidenttype") {
                $options = getListOptions('reg_incidenttype');
                $replacement = selectbox($options, "Internal fraud (ie: insider trading, market timing)", false);
            } elseif ($arrayVeldType[$key] == "valuta") {
                $replacement = selectbox_tabel("valuta", "NAAM", "NAAM", "EUR", false,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
            } elseif ($arrayVeldType[$key] == "status") {
                $options = getListOptions('status');
                $replacement = selectbox($options, "Draft", true);
            } elseif ($arrayVeldType[$key] == "reg_status") {
                $options = getListOptions('reg_status');
                $replacement = selectbox($options, "Open", false);
            } elseif ($arrayVeldType[$key] == "reg_recoverable") {
                $options = getListOptions('reg_recoverable');
                $replacement = selectbox($options, "D&O", false);
            } elseif ($arrayVeldType[$key] == "reg_typeofaction") {
                $options = getListOptions('reg_typeofaction');
                $replacement = selectbox($options, "Advisory", false);
            } elseif ($arrayVeldType[$key] == "termijn") {
                $options = getListOptions('termijn');
                $replacement = selectbox($options, "year", false);
            } elseif ($arrayVeldType[$key] == "owner") {
                $options = array();
                $options["OrangeField"] = "OrangeField";
                $options["Vistra"] = "Vistra";
                $replacement = selectbox($options, "OrangeField", false);
            } elseif ($arrayVeldType[$key] == "adres_type") {
                $options = getListOptions('adres_type');
                $replacement = selectbox($options, "Registered", false);
            } elseif ($arrayVeldType[$key] == "oft_department") {
                $replacement = selectbox_tabel("oft_department", "ID", "NAAM", '', false,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
            } elseif ($arrayVeldType[$key] == "CLASSTYPE") {
                if ($_REQUEST["SITE"] == "oft_entitie_transactions_add") {
                    $classTypes = "<option value=\"\" amount=\"\"></option>";
                    if (isset($_REQUEST["BID"])) {
                        $queryClasses = $pdo->prepare('select * from oft_classes WHERE (NOT DELETED = "Ja" OR DELETED is null) AND BEDRIJFSID = :bedrijfsid;');
                        $queryClasses->bindValue('bedrijfsid', $_REQUEST["BID"]);
                        $queryClasses->execute();

                        foreach ($queryClasses->fetchAll() as $dClassTypes) {
                            $nrOfSharesLeft = $dClassTypes["ISSUEDQUANTITY"];
                            $nrOfPercentageLeft = 100;

                            $queryShareHoldersPerType = $pdo->prepare('select *
                                               from oft_shareholders
                                              WHERE (NOT DELETED = "Ja" OR DELETED is null)
                                                AND CLASSTYPE = :classtype
                                                AND ADMINIDFROM = :adminidfrom;');
                            $queryShareHoldersPerType->bindValue('classtype', $dClassTypes["TYPE"]);
                            $queryShareHoldersPerType->bindValue('adminidfrom', $_REQUEST["BID"]);
                            $queryShareHoldersPerType->execute();

                            foreach ($queryShareHoldersPerType->fetchAll() as $dShareHoldersPerType) {
                                $nrOfSharesLeft -= $dShareHoldersPerType["QUANTITY"];
                                $nrOfPercentageLeft -= $dShareHoldersPerType["PERSENTAGE"];
                            }

                            $classTypes .= "<option value=\"" . $dClassTypes["TYPE"] . "\" amount=\"" . $nrOfSharesLeft . "\" percentageLeft=\"" . $nrOfPercentageLeft . "\">Type: " . $dClassTypes["TYPE"] . "  / Number issued not allocated: " . $nrOfSharesLeft . "</option>";
                        }
                        if ($classTypes == "<option value=\"\" amount=\"\"></option>") {
                            $classTypes = "<option value=\"Common\" amount=\"0\">Please first complete the capital</option>";
                        }
                    } else {
                        $classTypes = selectbox_tabel("oft_transaction_types", "NAAM", "NAAM", "Common", false,
                            "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
                    }

                    $replacement = $classTypes;
                } else {
                    $replacement = selectbox_tabel("oft_transaction_types", "NAAM", "NAAM", "Common", false,
                        "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
                }
            } elseif ($arrayVeldType[$key] == "valuta") {
                $replacement = selectbox_tabel("valuta", "NAAM", "NAAM", 'EUR', false,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
            } elseif ($arrayVeldType[$key] == "land") {
                $replacement = selectbox_tabel("country", "NAAM", "NAAM", 'The Netherlands', false,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null) AND ZICHTBAAR='Ja'");
            } elseif ($arrayVeldType[$key] == "bedrijf" || $arrayVeldType[$key] == "newentity") {
                $replacement = selectbox_tabel("bedrijf", "ID", "BEDRIJFSNAAM", $bedrijfsid, false,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
            } elseif ($arrayVeldType[$key] == "managementType") {
                $replacement = "Non-executive Board";
                if (isset($_REQUEST["TYPE"])) {
                    $replacement = $_REQUEST["TYPE"];
                }
            } elseif ($arrayVeldType[$key] == "personeel") {
                $replacement = selectbox_tabel("personeel", "ID", "INITIALEN, TUSSENVOEGSEL, ACHTERNAAM", '', false,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
            } elseif (substr($arrayVeldType[$key], 0, 5) == "stam_") {
                $replacement = selectbox_tabel($arrayVeldType[$key], "ID", "NAAM", '', true,
                    "WHERE (NOT DELETED = 'Ja' OR DELETED is null)");
            } elseif ($arrayVeldType[$key] == "SKIP_INCIDENT_ACTIONS") {
                $replacement = "<tr><td class=\"oft_label\">Actions taken - text</td>
                                  <td></td>
                                  <td><textarea name=\"INCIDENT_ACTIONS_n\" class=\"textbox\"></textarea></td>
                               </tr>
                               <tr><td class=\"oft_label\">Actions taken - status</td>
                                  <td></td>
                                  <td><select name=\"INCIDENT_ACTIONS_STATUS_n\" class=\"field\">" . selectbox(array(
                        "Open" => "Open",
                        "Closed" => "Closed"
                    ), "Open", false) . "</select></td>
                               </tr>
                               <tr><td class=\"oft_label\">Actions taken - last update</td>
                                   <td></td>
                                   <td>
                                      <input type=\"text\" class=\"field3\" value=\"\" name=\"INCIDENT_ACTIONS_DATUM_n\" onClick=\"$(this).datepicker().datepicker( 'show' );\" />
                                   </td>
                               </tr>";
            } elseif ($arrayVeldType[$key] == "boardmeetings") {
                $lijst = array();
                for ($j = 1; $j <= 4; $j++) {
                    $lijst[$j] = $j;
                }
                $replacement = selectbox($lijst, 1, false);
            } elseif ($arrayVeldType[$key] == "jarenlijst") {
                $jaren = array();
                for ($j = date("Y") + 1; $j >= date("Y") - 11; $j--) {
                    $jaren["$j-01-01"] = $j;
                }
                $replacement = selectbox($jaren, date("Y"), false);
            } elseif ($arrayVeldType[$key] == "alleenjarenlijst") {
                $jaren = array();
                for ($j = date("Y") + 1; $j >= date("Y") - 11; $j--) {
                    $jaren[$j] = $j;
                }
                $replacement = selectbox($jaren, date("Y"), false);
            } elseif ($arrayVeldType[$key] == "endfinyear") {
                $maanden[date("Y") . "-01-31"] = "31 Jan";
                $maanden[date("Y") . "-02-28"] = "28 Feb";
                $maanden[date("Y") . "-03-31"] = "31 Mrt";
                $maanden[date("Y") . "-04-30"] = "30 Apr";
                $maanden[date("Y") . "-05-31"] = "31 May";
                $maanden[date("Y") . "-06-30"] = "30 Jun";
                $maanden[date("Y") . "-07-31"] = "31 Jul";
                $maanden[date("Y") . "-08-31"] = "31 Aug";
                $maanden[date("Y") . "-09-30"] = "30 Sep";
                $maanden[date("Y") . "-10-31"] = "31 Oct";
                $maanden[date("Y") . "-11-30"] = "30 Nov";
                $maanden[date("Y") . "-12-31"] = "31 Dec";
                $replacement = selectbox($maanden, date("Y-12-31"), false);
            }
            $templateContent = str_replace($tabel . "_" . $kolom . "_value", $replacement, $templateContent);
        }

        $templateContent = $templateContent;
    }

    return $templateContent;
}

function oft_validatie_form($userid, $bedrijfsid, $tabel)
{
    /*
     *  Reuse validate form for ajax posting data to server. When error show this
     */

    //Validatie
    $validatie = <<<EOD
<script type="text/javascript">

window.oftButtonClikkedName = "OPSLAAN";
window.oftButtonClikkedValue = "Save";

function validate_form(element) {

  var uri = $(element).attr('action');
  if(uri=="") {
    uri = window.location.href;
  }
  var met = $(element).attr('method');
  var fd = new FormData(element);
  if(window.XMLHttpRequest) {
    var xhr = new XMLHttpRequest();
  } else {
    var xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }

  fd.append(window.oftButtonClikkedName, window.oftButtonClikkedValue);
  xhr.open(met, uri, true);
  xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 ) {
          if(xhr.status == 200) {
            if(typeof xhr.responseURL == 'undefined') {
              window.location.reload();
            } else {
              window.location.href = xhr.responseURL;
            }
          } else {
            validate_error_message(xhr.responseText,element);
          }
      }
  };

  xhr.send(fd);

  //reset buttonstate.
  window.oftButtonClikkedName = "OPSLAAN";
  window.oftButtonClikkedValue = "Save";

  return false;
}

function validate_error_message(text,form) {
  if ($(form).find(".error_message").length == 0) {
    $(form).prepend("<div class='error_message'>Form not submitted due to error: <div class='error_message_text'></div> Please validate form.</div>");
  }

  $(form).find(".error_message_text").text(text);
  window.scrollTo(0,0);
}
</script>
EOD;

    return $validatie;
}

function oft_alertFoutmeldingValidatie($veld, $naam)
{
    if ($veld == "") {
        $veld = $naam;
    }

    return "\n     alert(\"The field `$naam` is empty. Please check your input.\");
\n     return (false);";
//\n     theForm.$naam.focus();
}

function replace_template_algemeen($userid, $bedrijfsid, $tabel, $id, $templateContent)
{
    global $db;

    $templateContent = str_replace("-TABEL-", $tabel, $templateContent);
    $templateContent = str_replace("-SAVE-",
        "<input type=\"submit\" name=\"OPSLAAN\" value=\"Save\" class=\"button\" />", $templateContent);
    $templateContent = str_replace("-PREVIOUS-",
        "<input type=\"submit\" name=\"PREVIOUS\" value=\"Back\" class=\"button\" />", $templateContent);
    $templateContent = str_replace("-NEXT-", "<input type=\"submit\" name=\"NEXT\" value=\"Next\" class=\"button\" />",
        $templateContent);
    $templateContent = str_replace("-ANNULEREN-",
        "<input type=\"button\" class=\"cancelbutton\" onClick=\"oft2ClosePopup();\" value=\"Cancel\" name=\"ANNULEREN\" />",
        $templateContent);

    $templateContent = str_replace("-SEND-", "<input type=\"submit\" name=\"SEND\" value=\"Send\" class=\"button\" />",
        $templateContent);
    if ($id == "") {
        $templateContent = str_replace("-PRINT-", "", $templateContent);
    } else {
        $templateContent = str_replace("-PRINT-",
            "<input type=\"button\" class=\"cancelbutton\" onClick=\"window.open('content.php?SITE=" . $tabel . "_1_print&PRINT=true&ID=" . $id . "');\" value=\"Print\" name=\"PRINT\" />",
            $templateContent);
    }

    return $templateContent;
}
